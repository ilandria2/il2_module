﻿
namespace PortraitIndicatorGenerator
{
    #region Namespaces

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;

    #endregion

    class Program
    {
        #region constants

        const string DIR_SOURCE = @"c:\_mydocs\repositories\il2_module\dev\il2_indicators\src\";
        const string DIR_OUTPUT = @"c:\_mydocs\repositories\il2_module\dev\il2_indicators\out\";
        const int FINAL_WIDTH = 64;
        const int FINAL_HEIGHT = 128;
        const int FINAL_HEIGHT_USED = 100;

        #endregion

        #region enums

        private enum Indicator
        {
            Stamina,
            Cooldown,
            Food,
            Water,
            Energy,
            Progress,
            Other
        }

        #endregion

        #region files

        private static readonly string[] _filesStamina = {
          Path.Combine(DIR_SOURCE, "il2port_x_s0.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s1.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s2.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s3.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s4.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s5.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s6.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s7.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s8.bmp"),
          Path.Combine(DIR_SOURCE, "il2port_x_s9.bmp"),
        };

        private static readonly string[] _filesCooldown = {
            Path.Combine(DIR_SOURCE, "il2port_x_c0.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_c1.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_c2.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_c3.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_c4.bmp"),
        };

        private static readonly string[] _filesFood = {
            Path.Combine(DIR_SOURCE, "il2port_x_f0.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_f1.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_f2.bmp"),
        };

        private static readonly string[] _filesWater = {
            Path.Combine(DIR_SOURCE, "il2port_x_w0.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_w1.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_w2.bmp"),
        };

        private static readonly string[] _filesEnergy = {
            Path.Combine(DIR_SOURCE, "il2port_x_e0.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_e1.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_e2.bmp"),
        };

        private static readonly string[] _filesProgress = {
            Path.Combine(DIR_SOURCE, "il2port_x_p0.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_p1.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_p2.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_p3.bmp"),
            Path.Combine(DIR_SOURCE, "il2port_x_p4.bmp"),
        };

        private static readonly string _fileUnused = Path.Combine(DIR_SOURCE, "il2port_x.bmp");

        #endregion

        static void Main(string[] args)
        {

            Bitmap finalImage = null;
            int step = 0;

            try
            {
                finalImage = new Bitmap(FINAL_WIDTH, FINAL_HEIGHT);

                //get a graphics object from the image so we can draw on it
                using (Graphics g = Graphics.FromImage(finalImage))
                {
                    //set background color
                    g.Clear(Color.Black);

                    #region Loop init

                    using (var bmpOther = new Bitmap(_fileUnused))

                        for (int s = 0; s < _filesStamina.Length; s++)
                        {
                            using (var bmpStamina = new Bitmap(_filesStamina[s]))

                                for (int c = 0; c < _filesCooldown.Length; c++)
                                {
                                    using (var bmpCooldown = new Bitmap(_filesCooldown[c]))

                                        for (int w = 0; w < _filesWater.Length; w++)
                                        {
                                            using (var bmpWater = new Bitmap(_filesWater[w]))

                                                for (int f = 0; f < _filesFood.Length; f++)
                                                {
                                                    using (var bmpFood = new Bitmap(_filesFood[f]))

                                                        for (int e = 0; e < _filesEnergy.Length; e++)
                                                        {
                                                            using (var bmpEnergy = new Bitmap(_filesEnergy[e]))

                                                                for (int p = 0; p < _filesProgress.Length; p++)
                                                                {
                                                                    using (var bmpProgress = new Bitmap(_filesProgress[p]))
                                                                    {
                    #endregion

                                                                        Console.SetCursorPosition(0, 0);
                                                                        Console.WriteLine(GetProgress(++step, s, c, w, f, e, p));

                                                                        DrawImage(Indicator.Stamina, bmpStamina, g);
                                                                        DrawImage(Indicator.Cooldown, bmpCooldown, g);
                                                                        DrawImage(Indicator.Water, bmpWater, g);
                                                                        DrawImage(Indicator.Food, bmpFood, g);
                                                                        DrawImage(Indicator.Energy, bmpEnergy, g);
                                                                        DrawImage(Indicator.Progress, bmpProgress, g);
                                                                        DrawImage(Indicator.Other, bmpOther, g);

                                                                        finalImage.Save(Path.Combine(DIR_OUTPUT, string.Format(@"il2ind_x_{0}{1}{2}{3}{4}{5}_m.bmp", s, c, w, f, e, p)), ImageFormat.Bmp);

                    #region Loop end
                                                                    }
                                                                } // progress
                                                        } // energy
                                                } // water
                                        } // food
                                } // cooldown
                        } // stamina

                    #endregion
                }

            }

            #region Error handling

            catch (Exception ex)
            {
                if (finalImage != null)
                    finalImage.Dispose();

                Console.WriteLine(ex.GetType().FullName);
                Console.WriteLine(ex.Message);

                if (ex.InnerException != null)
                {
                    Console.WriteLine("Inner exception:");
                    Console.WriteLine(ex.InnerException.GetType().FullName);
                    Console.WriteLine(ex.InnerException.Message);
                }

                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
            }

            #endregion
        }

        #region Methods.Helpers

        private static string GetProgress(int current, int s, int c, int w, int f, int e, int p)
        {
            int total = _filesStamina.Length * _filesCooldown.Length * _filesFood.Length * _filesWater.Length * _filesEnergy.Length * _filesProgress.Length;
            int percent = (int)(((float)current / total) * 100);
            //if (percent == 0) System.Diagnostics.Debugger.Break();
            var barC = new string('*', percent / 4);
            var barX = new string('-', (100 - percent) / 4);

            if (barC.Length + barX.Length < 25)
                barX += "-";

            var bar = string.Format("{0}{1}", barC, barX);
            var msg = string.Format("Progress [{0}] {1}% (s{2}-c{3}-w{4}-f{5}-e{6}-p{7})                   ", bar, percent, s, c, w, f, e, p);

            return msg;
        }

        static void DetermineOffsetForIndicatorImage(Indicator indicator, out int top, out int left)
        {
            switch (indicator)
            {
                case Indicator.Stamina:
                    left = 0;
                    top = 0;
                    break;
                case Indicator.Cooldown:
                    left = 8;
                    top = 0;
                    break;
                case Indicator.Water:
                    left = 17;
                    top = 0;
                    break;
                case Indicator.Food:
                    left = 17;
                    top = 32;
                    break;
                case Indicator.Energy:
                    left = 17;
                    top = 65;
                    break;
                case Indicator.Progress:
                    left = 17;
                    top = 95;
                    break;
                case Indicator.Other:
                    left = 0;
                    top = FINAL_HEIGHT_USED;
                    break;
                default:
                    top = 0;
                    left = 0;
                    break;
            }
        }

        private static void DrawImage(Indicator indicator, Bitmap image, Graphics graphics)
        {
            int x = 0; int y = 0;

            DetermineOffsetForIndicatorImage(indicator, out y, out x);
            graphics.DrawImage(image, new Rectangle(x, y, image.Width, image.Height));
        }

        #endregion
    }
}
