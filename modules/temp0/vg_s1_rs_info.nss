// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Sys info                                      |
// | File    || vg_s1_rs_info.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 13-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu RS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_rs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_RS_PREFIX + "_OBJECT");

    sTemp   =   gls(OBJECT_SELF, SYSTEM_RS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    int bFloat  =   TRUE;

    if  (sCase  ==  "ENERGY_X")
    {
        sMessage    =   C_NEGA + "-- Insufficient amount of energy --";
        bFloat      =   FALSE;
    }

    else

    if  (sCase  ==  "CRITICAL")
    {
        sMessage    =   C_NEGA + "!!! Extreeme fatigue !!!";
    }

    else

    if  (sCase  ==  "NO_REST")
    {
        sMessage    =   C_NEGA + "Your character cannot rest at this place";
    }

    else

    if  (sCase  ==  "OFF")
    {
        sMessage    =   C_NEGA + "The rest system is offline at this moment";
    }

    else

    if  (sCase  ==  "STATUS")
    {
        int     nValue  =   gli(OBJECT_SELF, SYSTEM_RS_PREFIX + "_FATIGUE");

        sMessage    =  C_NORM + "[" + GetDiagram(nValue, RS_FATIGUE_EDGE_FSLEEP, -2, TRUE) + C_NORM + "] Energy";
    }

    else

    if  (sCase  ==  "RESTING")
    {
        sMessage    =   C_NORM + "Your character started to " + C_BOLD + "rest";
    }

    else

    if  (sCase  ==  "REST_CANCELLED")
    {
        //int     nRest   =   stoi(GetNthSubString(sTemp, 1));
        //string  sRest   =   gls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", nRest);

        sMessage    =   C_NEGA + "Rest interrupted";
    }

    else

    if  (sCase  ==  "NOT_TIRED")
    {
        sMessage    =   C_NORM + "Your character cannot rest while not being " + C_NEGA + "tired";
    }

    else

    if  (sCase  ==  "REST_UNARMED")
    {
        sMessage    =   C_NORM + "Your character cannot rest with " + C_NEGA + "something equipped in the hand" + C_NORM + " or with an " + C_NEGA + "armor";
    }

    else

    if  (sCase  ==  "WAKED_UP")
    {
        sMessage    =   C_POSI + "Your character wakes up from a sleep";
    }

    else

    if  (sCase  ==  "RESURRECTED")
    {
        sMessage    =   C_NEGA + "Your character was resurrected hence is weak now";
    }

    else

    if  (sCase  ==  "UNPACK")
    {
        sMessage    =   C_NORM + "You are unpacking the sleepbag";
    }

    if  (sCase  ==  "SYSTEM_SKILLS")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_RS_PREFIX + "_OBJECT");
        object  oTarget =   glo(OBJECT_SELF, SYSTEM_RS_PREFIX + "_SYSTEM_SKILLS_OBJECT", -1, SYS_M_DELETE);
        int     nValue  =   gli(oTarget, SYSTEM_RS_PREFIX + "_FATIGUE");
        int     nRest   =   gli(oTarget, SYSTEM_RS_PREFIX + "_REST_STATE");
        string  sRest   =   gls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", nRest);

        sRest   =   (!nRest)    ?   "None" :   sRest;

        sMessage    +=  C_NORM + "[" + GetDiagram(nValue, RS_FATIGUE_EDGE_FSLEEP, -2, TRUE, 175, 0, 255) + C_NORM + "] Energy";
        sMessage    +=  "\n" + C_NORM + "Active rest: " + C_BOLD + sRest;
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}
