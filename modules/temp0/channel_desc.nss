// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_desc.nss                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "DESC"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "DESC";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    string  sMessage;

    if  (sText  ==  "0")
    {
        sMessage    =   C_INTE + "Appearance indicator" + C_NORM + " of your character was " + C_INTE + "deleted";

        sls(oPC, SYSTEM_PIS_PREFIX + "_DESC", "", -1, SYS_M_DELETE);
    }

    else
    {
        sText   =   left(sText, 40);
        sText   =   ConvertTokens(sText, oPC, FALSE);
        sMessage    =   C_INTE + "Appearance indicator" + C_NORM + " of your character was set to:\n" + sText;

        sls(oPC, SYSTEM_PIS_PREFIX + "_DESC", sText);
    }

    msg(oPC, sMessage, FALSE, FALSE);
    PIS_RefreshIndicator(oPC, oPC, TRUE);
    //SetDescription(oPC, sText);
    //AssignCommand(oPC, ActionExamine(oPC));

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
