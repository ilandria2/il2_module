// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Insert item script               |
// | File    || vg_s1_tis_insert.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Umelo pusti udalost "OnPlayerChat" za cilem pouziti struktury LPD dialog
    a nastaveni TIS menu pro kontrolu podminek vykonani cinnosti vlozeni
    predmetu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oItem   =   GetSpellTargetObject();
    object  oPC     =   OBJECT_SELF;
    object  oTIS    =   glo(oPC, SYSTEM_TIS_PREFIX + "_OBJECT", gli(oPC, SYSTEM_TIS_PREFIX + "_OBJECT_USED"));
    object  oAssoc  =   glo(oTIS, SYSTEM_TIS_PREFIX + "_ASSOC");
    string  sMessage;

    //  neplatny cil
    if  (GetItemPossessor(oItem)    !=  oPC)
    {
        sMessage    =   C_NEGA + "Mus� vybrat p�edm�t z invent��e tv� postavy.";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    sMessage    =   C_NORM + "Z menu interakc� vyber �innost [" + C_BOLD + "Invnet��" + C_NORM + "] pro vlo�en� p�edm�tu.";

    msg(oPC, sMessage, TRUE, FALSE);
    slo(oPC, SYSTEM_TIS_PREFIX + "_INSERT", oItem);
}

