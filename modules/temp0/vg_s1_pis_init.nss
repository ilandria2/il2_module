// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Init script                      |
// | File    || vg_s1_pis_init.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system PIS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_d"
#include    "vg_s0_oes_const"
#include    "vg_s0_pis_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_PIS_PREFIX;
    string  sName       =   SYSTEM_PIS_NAME;
    string  sVersion    =   SYSTEM_PIS_VERSION;
    string  sAuthor     =   SYSTEM_PIS_AUTHOR;
    string  sDate       =   SYSTEM_PIS_DATE;
    string  sUpDate     =   SYSTEM_PIS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_pis_info");
    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_pis_data_c");
    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_pis_data_s");
    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_pis_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_pis_names", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACQUIRE_ITEM) + "_SCRIPT", "vg_s1_pis_acquir", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CUSTOM_LOOP) + "_SCRIPT", "vg_s1_pis_loop", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_COMBAT_ROUND_END) + "_SCRIPT", "vg_s1_pis_hpicre", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_PHYSICAL_ATTACKED) + "_SCRIPT", "vg_s1_pis_hpiplc", -2);

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}

