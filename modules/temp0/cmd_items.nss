// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_items.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "ITEMS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_acs_const"
#include "vg_s0_sys_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void FilterRecipeByPattern(object player, string pattern, int bAll = FALSE, int n = 1, int nMatch = 0, string sCatCur = "", string out_message = "");

object oACS = GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");


void    main()
{
    string      sCmd    =   "ITEMS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sParam  =   CCS_GetConvertedString(sLine, 1, FALSE);
    string  sMessage;

    /*

    ;items;                     -- vypise seznam ctg + recipes (all)

        --- priklad:
        Recepty podle patternu **AXE**:

        Nastroje:
        117 - [PickAxe] / Krumpac
        124 - [WoodsmanAxe] / Drevorubecka sekyra

        Zbrane:
        175 - [WeaponAxe] / Sekyra

    ;items pochoden;            -- vypise seznam ctg + recipes kde ConvertText("label" | "category" | "name", FALSE, TRUE) like **pochoden**
    */

    sParam  =   upper(sParam);

    oACS    =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    int     nRecipe =   stoi(sParam);
    int     bAll    =   sParam ==  "" || sParam == "ALL" || sParam == "*";
    int     nCount  =   gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_N_ROWS");
    int     n, bMatch, nMatch;
    string  sLabel, sCat, sCatCur, sName, sResRef;

    //  zadany parametr nebylo cislo >= 100 (cislo receptu)
    if  (nRecipe    <   100
    ||  bAll)
    {
        sParam  =   ConvertText(sParam);

        FilterRecipeByPattern(oPC, sParam, bAll);
    }

    //  zadany parametr je cislo >= 100 (detail receptu)
    else
    {
        /*
        ;items 109;                 -- vypise podrobne info o rid 109

            --- priklad:
            Recept:             109
            Kategorie:          Zdroj svetla
            Nazev:              Pochoden
            ResRef:             vg_i_tr_torch_01
            Mnozstvi:           1
            Zakladni vaha:      640.5g
            Zakladni DB:        3140.0j

            Upresneni surovin:
            - Hornina (x50):
                1-Med           0.8 [g] / 0.4 [db]
                2-Zelezo        1.2 [g] / 1.6 [db]
                3-Zlato         1.1 [g] / 0.8 [db]
                4-Stribro       1.4 [g] / 1.2 [db]

            - Drevo (x500):
                1-Buk           0.8 [g] / 0.9 [db]
                2-Dub           1.8 [g] / 1.4 [db]

            - Horlavina (x10):
                1-Luj           1.4 [g] / 250 [db]
                2-Olej          1.2 [g] / 630 [db]
                3-Zarivy prach  0.6 [g] / 150 [db]

        */

        //SpawnScriptDebugger();
        sLabel  =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_LABEL");

        //  neplatne ID receptu
        if  (sLabel ==  "")
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid recipe ID: " + R1 + itos(nRecipe);

            msg(oPC, sMessage, TRUE, FALSE);
            return;
        }

        string  sQty, sBaseW, sBaseDB, sBaseCT;

        sCat    =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_CATEGORY");
        sResRef =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_RESREF");
        sName   =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_DISPLAY");
        sQty    =   itos(gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_QTY"));
        sBaseW  =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_MODWEIGHT");
        sBaseDB =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_MODDURAB");
        sBaseCT =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_MODCOST");
        sName   =   (sName      ==  "" || sName     == C_NORM) ?   W1 + "<Empty>" : sName;
        sResRef =   (sResRef    ==  "" || sResRef   == C_NORM) ?   W1 + "<Empty>" : sResRef;
        sBaseW  =   left(sBaseW, len(sBaseW) - 2);
        sBaseDB =   left(sBaseDB, len(sBaseDB) - 2);
        sBaseCT =   left(sBaseCT, len(sBaseCT) - 2);

        sMessage    =   Y1 + "Recipe information:";
        sMessage    +=  "\n" + W2 + "Recipe: " + Y2 + itos(nRecipe);
        sMessage    +=  "\n" + W2 + "Category: " + Y2 + sCat;
        sMessage    +=  "\n" + W2 + "Label: " + Y2 + sLabel;
        sMessage    +=  "\n" + W2 + "Name: " + Y2 + sName;
        sMessage    +=  "\n" + W2 + "ResRef: " + Y2 + sResRef;
        sMessage    +=  "\n" + W2 + "Quantity: " + Y2 + sQty;
        sMessage    +=  "\n" + W2 + "Base weight: " + Y2 + sBaseW;
        sMessage    +=  "\n" + W2 + "Base durability: " + Y2 + sBaseDB;
        sMessage    +=  "\n" + W2 + "Base cost: " + Y2 + sBaseCT;
        sMessage    +=  "\n\n" + Y1 + "Material specification:";

        int g, c;
        int nGroup, nChild, nQty;

        nGroup  =   gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++g);

        //  projde vsechny skupiny pro dany recept
        while   (nGroup >=  100)
        {
            sName       =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nGroup) + "_DISPLAY");
            nQty        =   gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_QTY", g);
            sMessage    +=  (g > 1) ? "\n" : "";
            sMessage    +=  "\n" + Y2 + itos(g) + "-" + W2 + sName + W2 + " [" + itos(nGroup) + "] (x" + Y2 + itos(nQty) + W2 + "):";

            nChild      =   gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", ++c);

            //  projde vsechny materialy v dane skupine
            while   (nChild >=  100)
            {
                sBaseW      =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODWEIGHT");
                sBaseDB     =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODDURAB");
                sBaseCT     =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODCOST");
                sName       =   gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_DISPLAY");
                sName       =   (sName      ==  "" || sName     == C_NORM) ?   W1 + "<Empty>" : sName;
                sBaseW      =   left(sBaseW, len(sBaseW) - 2);
                sBaseDB     =   left(sBaseDB, len(sBaseDB) - 2);
                sBaseCT     =   left(sBaseCT, len(sBaseCT) - 2);
                sMessage    +=  "\n  " + Y2 + itos(g) + "." + itos(c) + "-" + W2 + sName + W2 + " [" + itos(nChild) + "] - " + Y2 + sBaseW + "g" + W2 + ", " + Y2 + sBaseDB + "db";
                nChild      =   gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", ++c);
            }

            nGroup  =   gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++g, c=0);
        }

        if  (g  ==  1)
        sMessage    +=  "\n" + W1 + "<Empty>";
    }

    ExamineMessage(oPC, sMessage);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

void FilterRecipeByPattern(object player, string pattern, int bAll = FALSE, int n = 1, int nMatch = 0, string sCatCur = "", string out_message = "")
{
    if (!GetIsObjectValid(player) || (pattern == "" && !bAll)) return;
    if (gli(player, "items"))SpawnScriptDebugger();
    int nRecipe = gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE", n);
    string sResRef = gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_RESREF");

    // ignore pseudo recipes
    while (nRecipe > 100 && sResRef == "")
    {
        nRecipe = gli(oACS, SYSTEM_ACS_PREFIX + "_RECIPE", ++n);
        sResRef = gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_RESREF");
    }

    // min recipe id is 100, so it means we reached the end of the list - print the final list
    if (nRecipe < 100)
    {
        out_message =   "\n" + W2 + "List of recipes filtered by pattern " + Y1 + "**" + pattern + "**" + W2 + ":" + out_message;
        out_message +=  "\n" + Y1 + "----------------------------------------";
        out_message +=  "\n" + W2 + "Found " + Y1 + itos(nMatch) + W2 + " recipes";
        out_message +=  "\n" + W1 + "(Viewing recipes with non-empty resrefs only)";

        ExamineMessage(player, out_message);
        return;
    }

    // find match by testing name, label and category against pattern from the parameter
    int bMatch = bAll;
    string sCat = gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_CATEGORY");
    string sLabel = gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_LABEL");
    string sName = gls(oACS, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_DISPLAY");
    sName = (sName == "" || sName == C_NORM) ? W1 + "<Empty>" : sName;

    if (!bAll)
        bMatch = TestStringAgainstPattern("**" + pattern + "**", sCat + ";" + sLabel + ";" + sName + ";" + ConvertText(sCat + ";" + sLabel + ";" + sName));

    if (bMatch)
    {
        // new category in list
        if  (sCat != sCatCur)
        {
            sCatCur = sCat;
            out_message += "\n\n" + Y1 + sCatCur + ":";
        }

        nMatch++;
        out_message += "\n" + Y2 + itos(nRecipe) + " - " + W1 + sLabel + " / " + sName;
    }

    DelayCommand(0.001, FilterRecipeByPattern(player, pattern, bAll, ++n, nMatch, sCatCur, out_message));
}
