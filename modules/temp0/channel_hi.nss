// script handler of the channel "HI"

#include    "vg_s0_ccs_core_t"
#include    "vg_s0_lpd_core_c"

void main()
{
    string sChannel = "HI";
    string sLine = GetPCChatMessage();
    object sSystem = GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object oPC = OBJECT_SELF;
    string sText = CCS_GetChannelText(sLine);

    // user must provide channel text, can't greet with an empty sentence on RP server
    if (sText == "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    if (gli(oPC, "ccs_hi"))SpawnScriptDebugger();

    int n = 1;
    object oNPC = GetNearestObject(OBJECT_TYPE_CREATURE, oPC, n);
    object oNPCFirst;
    int found = FALSE;

    while (GetIsObjectValid(oNPC))
    {
        // no nearby NPC available to converse with
        if (GetDistanceBetween(oPC, oNPC) > LPD_DEFAULT_MAX_DISTANCE)
        {
            // NPC is busy message
            if (GetIsObjectValid(oNPCFirst))
                SYS_Info(oPC, SYSTEM_LPD_PREFIX, "OCCUPIED");
            else
                SYS_Info(oPC, SYSTEM_LPD_PREFIX, "NO_TARGET");
            return;
        }

        object G_NPC = LPD_GetConversator(oNPC);

        // npc is busy
        if (GetIsObjectValid(G_NPC))
        {
            // player already talks to this NPC - ignore this time
            if (G_NPC == oPC)
                return;

            // else remember this NPC as available but busy as we first try to get hold of another near NPC
            oNPCFirst = oNPC;
        }

        // npc is not busy and is valid
        else
        {
            found = TRUE;
            break;
        }

        oNPC = GetNearestObject(OBJECT_TYPE_CREATURE, oPC, ++n);
    }

    // not found - info player
    if (!found)
    {
        // NPC is busy message
        if (GetIsObjectValid(oNPCFirst))
            SYS_Info(oPC, SYSTEM_LPD_PREFIX, "OCCUPIED");

        // noone to converse with
        else
            SYS_Info(oPC, SYSTEM_LPD_PREFIX, "NO_TARGET");
        return;
    }

    // got valid NPC
    sText = ConvertTokens(sText, oPC, FALSE, TRUE, FALSE, FALSE);

    SetPCChatMessage(sText);
    LPD_StartConversation(oPC, oNPC);
}

