// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_item.nss                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "ITEM"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_acs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "ITEM";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sRecipe =   CCS_GetConvertedString(sLine, 1, FALSE);
    int     nAmount =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    string  sSpec   =   CCS_GetConvertedString(sLine, 3, FALSE);
    object  oTarget =   CCS_GetConvertedObject(sLine, 4, FALSE);
    location    lTarget=CCS_GetConvertedLocation(sLine, 4, FALSE);
    int     nRecipe, n1, n2, n3, n4, n5;
    string  sMessage;

    nRecipe =   stoi(sRecipe);
    sSpec   =   left(sSpec + "11111", 5);
    n1      =   stoi(sub(sSpec, 0, 1));
    n2      =   stoi(sub(sSpec, 1, 1));
    n3      =   stoi(sub(sSpec, 2, 1));
    n4      =   stoi(sub(sSpec, 3, 1));
    n5      =   stoi(sub(sSpec, 4, 1));

    /*
        ;item vg_i_ci_abx123_01;    -- vytvori predmet s resrefem vg_i_ci_abx123_01 u cile
        ;item 109;                  -- vytvori predmet s rid 109 u cile
    */

    //  podle resrefu
    if  (nRecipe    <   100)
    {
        if  (!GetHasInventory(oTarget))
        CreateObject(OBJECT_TYPE_ITEM, sRecipe, lTarget);

        else
        CreateItemOnObject(sRecipe, oTarget, nAmount);
    }

    //  podle receptu
    else
    {
        if (gli(oPC, "acs_rcr"))SpawnScriptDebugger();
        ACS_CreateItemUsingRecipe(nRecipe, nAmount, lTarget, oTarget, FALSE, n1, n2, n3, n4, n5);

        sMessage    =   G1 + "( ! ) " + W2 + "Creating recipe: " + itos(nRecipe) + " (x" + itos(nAmount) + ") on object: " + GetName(oTarget) + "/" + GetTag(oTarget) + "/" + GetPCPlayerName(oTarget);
    }

    msg(oPC, sMessage, TRUE, FALSE);
    return;

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

