// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Impact script                           |
// | File    || vg_s3_sys_useitm.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-05-2009                                                     |
// | Updated || 16-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript pro kouzlo "Pouzit" na predmetu

    Zhodnoti vlastnosti predmetu a na zaklade nich aplikuje
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    ExecImpact(object oItem)
{
    itemproperty    ipTemp;
    int             nType, nSubType;
    string          sImpact;

    ipTemp  =   GetFirstItemProperty(oItem);

    while   (GetIsItemPropertyValid(ipTemp))
    {
        nType   =   GetItemPropertyType(ipTemp);

        if  (nType  ==  ITEM_PROPERTY_SPELL)
        {
            nSubType    =   GetItemPropertySubType(ipTemp);
            sImpact     =   Get2DAString("spells", "ImpactScript", stoi(Get2DAString("iprp_spells", "SpellIndex", nSubType)));

            ExecuteScript(sImpact, OBJECT_SELF);
        }

        ipTemp  =   GetNextItemProperty(oItem);
    }
}



void    main()
{
    object  oPC     =   OBJECT_SELF;

    //  hrac musi byt schopny vykonavat cinnosti
    if  (!GetCommandable(oPC))  return;

    int     nSpell  =   GetSpellId();
    object  oTarget =   GetSpellTargetObject();

    //  use item
    if  (nSpell ==  SPELL_TIS_TARGET)
    {
        if  (GetObjectType(oTarget) !=  OBJECT_TYPE_ITEM)
        {
            SYS_Info(oPC, SYSTEM_SYS_PREFIX + "READ_DESC");
            return;
        }

        int nSlot   =   gli(oTarget, SYSTEM_SYS_PREFIX + "_INV_SLOT");

        //  predmet, kt. byl vybran k pouziti neni equipnutej
        if  (nSlot  <   100)
        {
            SYS_Info(oPC, SYSTEM_SYS_PREFIX, "EQUIP_FIRST_&" + GetName(oTarget) + "&");
            return;
        }

        //  vybranej predmet ma vlastnost "Pouzitelny na cil"
        if  (GetItemHasItemProperty(oTarget, ITEM_PROPERTY_USE_TARGET))
        {
            SYS_Info(oPC, SYSTEM_SYS_PREFIX, "ITEM_USE_TARGET_&" + GetName(oTarget) + "&");
            slo(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USED", oTarget);
            sls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "&vg_s3_sys_useitm&");
        }

        else

        //  vybranej predmet ma vlastnost "Pouzitelny na sebe"
        if  (GetItemHasItemProperty(oTarget, ITEM_PROPERTY_USE_SELF))
        {
            //  casove omezeni
            int bProg   =   gli(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USAGE_P");

            if  (bProg) return;

            sli(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USAGE_P", TRUE);
            DelayCommand(6.0f, sli(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USAGE_P", FALSE));

            //  signalizace OnActivateItem event v module
            SignalEvent(GetModule(), EventActivateItem(oTarget, GetSpellTargetLocation(), oTarget));

            //  pusti impact skripty pro vlastnosti typu "Ucinek" (custom CastSpell)
            ExecImpact(oTarget);
        }

        //  vybranej predmet nelze pouzit
        else
        {
            SYS_Info(oPC, SYSTEM_SYS_PREFIX + "ITEM_UNUSEABLE_&" + GetName(oTarget) + "&");
            return;
        }
    }

    else

    //  target
    if  (nSpell ==  SPELL_TIS_TARGET)
    {
        object  oUsed   =   glo(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USED");
        int     nSlot   =   gli(oUsed, SYSTEM_SYS_PREFIX + "_INV_SLOT");

        //  pouzitej predmet neni ten co se nachazi v leve ruce
        if  (nSlot  <   100)    return;

        //  casove omezeni
        int bProg   =   gli(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USAGE_P");

        if  (bProg) return;

        sli(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USAGE_P", TRUE);
        DelayCommand(6.0f, sli(oPC, SYSTEM_SYS_PREFIX + "_ITEM_USAGE_P", FALSE));

        //  signalizace OnActivateItem event v module
        SignalEvent(GetModule(), EventActivateItem(oUsed, GetSpellTargetLocation(), oTarget));

        //  pusti impact skripty pro vlastnosti typu "Ucinek" (custom CastSpell)
        DelayCommand(1.0f, ExecImpact(oUsed));
    }   //  target
}   //  main
