// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Persistent data save script             |
// | File    || vg_s1_sys_data_s.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-09-2009                                                     |
// | Updated || 05-09-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "x0_i0_position"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!GetIsObjectValid(OBJECT_SELF))
        return;

    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sParam  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_SAVE_PARAM", -1, SYS_M_DELETE);
    string  sQuery;

    sParam  =   upper(sParam);
    sParam  =   (sParam ==  "") ?   "POS"   :   sParam;

    //  ulozeni pozice
    if  (sParam ==  "POS")
    {
        /*//  ulozeni pozice
        string  sArea   =   sub(GetTag(GetArea(OBJECT_SELF)), 4, 1);

        if  (sArea  ==  "X"
        ||  sArea   ==  "S")    return;
        */
        sQuery  =   "SELECT PlayerID " +
                    "FROM sys_players " +
                    "WHERE PlayerID = '" +  itos(nID) + "'";

        SQLExecDirect(sQuery);

        //string  sValue  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_REGION");
        string  sValue  =   APSLocationToString(GetLocation(OBJECT_SELF));

        if  (SQLFetch() ==  SQL_SUCCESS)
        {
            sQuery  =   "UPDATE sys_players " +
                        "SET PlayerPOS = '" + sValue + "'" +
                        "WHERE PlayerID = '" + itos(nID) + "'";

            SQLExecDirect(sQuery);
            //SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "POS_SUCCESS");
        }
    }

    else

    //  datum a cas
    if  (sParam ==  "SETTINGS")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
        string  sDate   =   GetCalendarDate();
        int lastId = gli(oSystem, SYSTEM_SYS_PREFIX + "_LAST_OBJECT_ID");

        sDate   =   left(sDate, 13) + ":00:00.000";
        sQuery  =   "UPDATE sys_settings SET GameDate = '" + sDate + "', LastObjectId = " + itos(lastId) + ", date = NOW()";

        SQLExecDirect(sQuery);
    }

    else

    if  (sParam ==  "VAR")
    {
        string  sObjDef     =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_VAR_OBJDEF", -1, SYS_M_DELETE);
        string  sObjType    =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_VAR_OBJTYPE", -1, SYS_M_DELETE);
        string  sDataType   =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_VAR_DATATYPE", -1, SYS_M_DELETE);
        string  sDataVal    =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_VAR_DATAVAL", -1, SYS_M_DELETE);

        //  nastaveni promenne v db
        if  (sDataVal   !=  "")
        sQuery  =   "INSERT INTO " +
                        "sys_vars " +
                    "VALUES ('" +
                        sObjDef + "','" +
                        sObjType + "','" +
                        sDataType + "','" +
                        sDataVal + "'," +
                        "NULL" +
                    ") ON DUPLICATE KEY UPDATE " +
                        "DataVal = '" + sDataVal + "'," +
                        "date = NULL";

        //  smazani promenne z db
        else
        sQuery  =   "DELETE FROM " +
                        "sys_vars " +
                    "WHERE " +
                        "ObjDef = '" + sObjDef + "' and " +
                        "ObjType = '" + sObjType + "' and " +
                        "DataType = '" + sDataType + "'";

        SQLExecDirect(sQuery);
    }

    else

    //  registrace postavy
    if  (sParam ==  "REG")
    {
        object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
        object player = OBJECT_SELF;
        int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
        int charHead = GetCreatureBodyPart(CREATURE_PART_HEAD, player);
        int charColorSkin = GetColor(player, COLOR_CHANNEL_SKIN);
        int charColorHair = GetColor(player, COLOR_CHANNEL_HAIR);
        int charGender = GetGender(player);
        int charPheno = GetPhenoType(player);
        int charRace = GetRacialType(player);
        string charDesc = GetDescription(player, TRUE);
        int charAge = GetAge(player);
        int charHP = GetCurrentHitPoints(player);
        int charHPMax = GetMaxHitPoints(player);

        string query =
            "UPDATE sys_players SET" +
            " CharHeadType = " + itos(charHead) +
            ",CharSkinColor = " + itos(charColorSkin) +
            ",CharHairColor = " + itos(charColorHair) +
            ",CharGender = " + itos(charGender) +
            ",CharPhenotype = " + itos(charPheno) +
            ",CharRace = " + itos(charRace) +
            ",CharSoundSet = ''" +
            ",CharDescription = '" + SQLEncodeSpecialChars(charDesc) + "'" +
            ",CharAge = " + itos(charAge) +
            ",CharHitPoints = " + itos(charHP) +
            ",CharHitPointsMax = " + itos(charHPMax) +
            ",RegisteredOn = NOW() " +
            "WHERE PlayerID = " + itos(playerId);

        logentry("Registering character: " + query);
        SQLExecDirect(query);
        logentry("Registration result: " + itos(SQLFetch()));


        sli(player, SYSTEM_SYS_PREFIX + "_REGISTERED", TRUE);
    }

    else

    //  ochrana uctu verejnym cd-klicem
    if  (sParam ==  "KEY")
    {
        string  sKey    =   GetPCPublicCDKey(OBJECT_SELF);
        string  sAcc    =   GetPCPlayerName(OBJECT_SELF);
        string  sPass   =   "";
        string  sChars  =   "abcdefghkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ0123456789";
        int     nLen    =   len(sChars);
        int     i       =   1;

        for (i = 1; i <= 10; i++)
        sPass   +=  sub(sChars, Random(nLen), 1);


        sQuery  =   "INSERT INTO sys_keys " +
                    "(" +
                        "PlayerACC, " +
                        "PlayerCD," +
                        "PlayerPWD" +
                    ") VALUES " +
                    "('" +
                        sAcc + "','" +
                        sKey + "','" +
                        sPass +
                    "')";

        SQLExecDirect(sQuery);
        sli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_KEY_EXISTS", TRUE);
        DelayCommand(3.0f, SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "KEY_&" + sAcc + "&_&" + sKey + "&_&" + sPass + "&"));
    }
}
