// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Persistent Data Check script     |
// | File    || vg_s1_pis_data_c.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro system PIS
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_pis_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;

    //  pis_names tabble
    sLog    =   "[" + SYSTEM_PIS_PREFIX + "] Checking table [pis_names]";
    sQuerry =   "DESCRIBE pis_names";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_PIS_PREFIX + "] Table [pis_names] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_PIS_PREFIX + "] Table [pis_names] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE pis_names " +
        "(" +
            "pc1 INT NOT NULL," +
            "pc2 INT NOT NULL," +
            "name VARCHAR(50) NOT NULL," +
            "date TIMESTAMP DEFAULT NOW()," +
            "PRIMARY KEY (pc1, pc2)" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE pis_names";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_PIS_PREFIX + "] Table [pis_names] created successfully"; else
        sLog    =   "[" + SYSTEM_PIS_PREFIX + "] Table [pis_names] not created - error";

        logentry(sLog);
    }
}
