#include "vg_s0_ids_core_c"

void main()
{
    object player = GetPCItemLastUnequippedBy();

    IDS_ComposeClothes(player);
    IDS_CleanInventory(player);
}
