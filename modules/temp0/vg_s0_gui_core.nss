// +---------++----------------------------------------------------------------+
// | Name    || Generic User Interface (GUI) Core                              |
// | File    || vg_s0_gui_core.nss                                             |
// | Version || 1.0                                                            |
// | Author  || VirgiL                                                         |
// | Created || 2017-02-22                                                     |
// +---------++----------------------------------------------------------------+

#include "vg_s0_gui_const"
#include "vg_s0_sys_core_o"
#include "nwnx_funcsext"
#include "x0_i0_position"

// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+

// adds Layout object for gui of typeId
void GUI_LayoutAddObject(int typeId, float width, float height, float posX, float posY, int layer, int vfxId, string layoutResRef);

// returns a refrence to a waypoint object that represents the definition of a GUI menu identified by a unique GUI typeId
object GUI_GetDefinitionObject(int typeId);

// returns a reference to an object that owns the layoutObject
object GUI_GetOwner(object layoutObject);

// sets the owner of the layoutObject to owner
// ** if owner is OBJECT_INVALID then the owner will be reset (DELETED) instead
void GUI_SetOwner(object layoutObject, object owner = OBJECT_INVALID);

// returns tech name of a menu for player or empty string if non active
string GUI_GetMenuName(object player);

// sets tech name of a menu for player to menu
// ** if menu is "" then the menu will be reset (DELETED) instead
void GUI_SetMenuName(object player, string menu);

// returns a reference to a placeable object that was clicked on by player
object GUI_GetLayoutObjectClicked(object player);

// sets the reference to a placeable object that wa clicked on by player to layoutObject
// ** if layoutObject is OBJECT_INVALID then the reference will be reset (DELETED) instead
void GUI_SetLayoutObjectClicked(object player, object layoutObject);

// returns the type of a GUI menu identified by guiTypeId
string GUI_GetType(int guiTypeId);

// returns the type ID of a GUI menu identified by guiType
int GUI_GetTypeId(string guiType);

// sets the guiType of a GUI menu identified by guiTypeId
void GUI_SetType(int guiTypeId, string guiType);

// sets the guiTypeId of a GUI menu identified by guiType
void GUI_SetTypeId(string guiType, int guiTypeId);

// returns the Layout ID of the layoutObject
int GUI_GetLayoutObjectId(object layoutObject);

// sets the Layout ID of the layoutObject
void GUI_SetLayoutObjectId(object layoutObject, int layoutId);

// Stops menu mode for player
void GUI_StopMenu(object player, int abort = FALSE);

// Starts a new guiType menu for player (stops any previously active menu)
void GUI_StartMenu(object player, string guiType);

// adds Layout grid objects for gui of typeId
void GUI_LayoutAddGridObjects(int guiTypeId, float width, float height, float distX, float distY, int rows, int columns, float margin, int layer, string layoutResRef);

// sets the width of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectWidth(int guiTypeId, int layoutId, float width);

// gets the width of the object identified by layoutId for guiTypeId menu
float GUI_GetLayoutObjectWidth(int guiTypeId, int layoutId);

// sets the height of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectHeight(int guiTypeId, int layoutId, float height);

// gets the height of the object identified by layoutId for guiTypeId menu
float GUI_GetLayoutObjectHeight(int guiTypeId, int layoutId);

// sets the X coordinate of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectPosX(int guiTypeId, int layoutId, float posX);

// gets the X coordinate of the object identified by layoutId for guiTypeId menu
float GUI_GetLayoutObjectPosX(int guiTypeId, int layoutId);

// sets the Y coordinate of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectPosY(int guiTypeId, int layoutId, float posY);

// gets the Y coordinate of the object identified by layoutId for guiTypeId menu
float GUI_GetLayoutObjectPosY(int guiTypeId, int layoutId);

// sets the layer of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectLayer(int guiTypeId, int layoutId, int layerId);

// gets the layer of the object identified by layoutId for guiTypeId menu
int GUI_GetLayoutObjectLayer(int guiTypeId, int layoutId);

// sets the VFX ID of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectVfxId(int guiTypeId, int layoutId, int vfxId);

// gets the VFX ID of the object identified by layoutId for guiTypeId menu
int GUI_GetLayoutObjectVfxId(int guiTypeId, int layoutId);

// sets the LayoutResRef of the object identified by layoutId for guiTypeId menu
void GUI_SetLayoutObjectLayoutResRef(int guiTypeId, int layoutId, string layoutResRef);

// gets the LayoutResRef of the object identified by layoutId for guiTypeId menu
string GUI_GetLayoutObjectLayoutResRef(int guiTypeId, int layoutId);

// Returns pixels / 100
float GUI_PixelsToMeters(float pixels);

// calculates XY offset based on the Z coordinate and angle of the model's orientation
// if degAngle = -1 then 50 degree is used
vector GUI_Offset(vector pos, float height, float side, int layerLevel = 0, float degAngle = GUI_CAMERA_ANGLE);

// renders and returns gui layout object with vfx at player's (pc) position adjusted by width, height, layerLevel and a base layer level (and a hardcoded degAngle mdl orientation)
vector GUI_RenderObject(int layoutId, int xGui, object pc, int baseLayer);

// gets the cached position of the GUI object at guiRow for player's active menu
vector GUI_MenuGetObjectPosition(object player, int guiRow);

// sets the cached position of the GUI object at guiRow for player's active menu
// if pos is vi() then deletes the position instead
void GUI_MenuSetObjectPosition(object player, int guiRow, vector pos);

// Locks player's camera and movement
void GUI_LockCamera(object player);

// Unlocks player's camera and movement
void GUI_UnlockCamera(object player);

// Gets the current sequence id for player
int GUI_GetSequence(object pc);

// Sets the current sequence id for player
// -1 will use current id + 1 instead
void GUI_SetSequence(object pc, int id = 0);

// enters menu rendering loop if not in any yet
void GUI_RenderMenuSequence(object pc, string menu = "", int id = 0);

// sets the max LayerID for source object
void GUI_SetMaxLayerId(object source, int layerId = -1);

// gets the max LayerID for source object
int GUI_GetMaxLayerId(object source);

// gets the cached VfxId of the GUI object at guiRow for player's active menu
int GUI_MenuGetObjectVfxId(object player, int guiRow);

// sets the cached VfxId of the GUI object at guiRow for player's active menu or deletes it if value = GUI_VFX_EMPTY
void GUI_MenuSetObjectVfxId(object player, int guiRow, int value = GUI_VFX_EMPTY);

// gets the cached Name of the GUI object at guiRow for player's active menu
string GUI_MenuGetObjectName(object player, int guiRow);

// sets the cached Name of the GUI object at guiRow for player's active menu or deletes it if value = ""
void GUI_MenuSetObjectName(object player, int guiRow, string value = "");

// gets the cached LayoutObject of the GUI object at guiRow for player's active menu
object GUI_MenuGetObjectLayout(object player, int guiRow);

// sets the cached LayoutObject of the GUI object at guiRow for player's active menu or deletes it if value = OBJECT_INVALID
void GUI_MenuSetObjectLayout(object player, int guiRow, object value = OBJECT_INVALID);

// destroys layout object on guiRow for player
void GUI_DestroyLayoutObject(object player, int guiRow);

// disables layout object on guiRow for player
void GUI_DisableLayoutObject(object player, int guiRow);

// enables layout object on guiRow for player
void GUI_EnableLayoutObject(object player, int guiRow);

// Creates a layout object for owner by layoutId of resRef with name on specific position
object GUI_CreateLayoutObject(object owner, int layoutId, string resRef, vector position);

// Returns TRUE if vfxId is GUI_VFX_MISSING or GUI_VFX_EMPTY
int GUI_GetIsVfxMissingOrEmpty(int vfxId);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+

void GUI_LayoutAddObject(int typeId, float width, float height, float posX, float posY, int layer, int vfxId, string layoutResRef)
{
    object gui = GUI_GetDefinitionObject(typeId);
    int newId = gli(gui, GUI_ + "OBJECT_I_ROWS") + 1;
    logentry("[" + GUI + "] Adding new layout object (" + itos(newId) + ") for GuiTypeID " + itos(typeId) + ": VFX=" + itos(vfxId) + " layoutResRef='" + layoutResRef + "'...");

    GUI_SetLayoutObjectWidth(typeId, newId, width);
    GUI_SetLayoutObjectHeight(typeId, newId, height);
    GUI_SetLayoutObjectPosX(typeId, newId, posX);
    GUI_SetLayoutObjectPosY(typeId, newId, posY);
    GUI_SetLayoutObjectLayer(typeId, newId, layer);
    GUI_SetLayoutObjectVfxId(typeId, newId, vfxId);
    GUI_SetLayoutObjectLayoutResRef(typeId, newId, layoutResRef);

    sli(gui, GUI_ + "OBJECT", 1, newId);
}



object GUI_GetDefinitionObject(int typeId)
{
    return GetObjectByTag(GUI_ + itos(typeId));
}



object GUI_GetOwner(object layoutObject)
{
    return glo(layoutObject, GUI_ + "OWNER");
}



void GUI_SetOwner(object layoutObject, object owner = OBJECT_INVALID)
{
    if (owner == OBJECT_INVALID)
        slo(layoutObject, GUI_ + "OWNER", OBJECT_INVALID, -1, SYS_M_DELETE);
    else
        slo(layoutObject, GUI_ + "OWNER", owner);
}



string GUI_GetMenuName(object player)
{
    return gls(player, GUI_ + "MENU");
}



void GUI_SetMenuName(object player, string menu)
{
    string activeMenu = GUI_GetMenuName(player);

    if (menu == activeMenu)
        return;

    if (menu == "")
        sls(player, GUI_ + "MENU", "", -1, SYS_M_DELETE);
    else
        sls(player, GUI_ + "MENU", menu);
}



object GUI_GetLayoutObjectClicked(object player)
{
    return glo(player, GUI_ + "OBJECT_CLICKED");
}



void GUI_SetLayoutObjectClicked(object player, object layoutObject)
{
    if (layoutObject == OBJECT_INVALID)
        slo(player, GUI_ + "OBJECT_CLICKED", OBJECT_INVALID, -1, SYS_M_DELETE);
    else
        slo(player, GUI_ + "OBJECT_CLICKED", layoutObject);
}



string GUI_GetType(int guiTypeId)
{
    return gls(__GUI(), GUI_ + "TYPE", guiTypeId);
}



int GUI_GetTypeId(string guiType)
{
    return gli(__GUI(), GUI_ + "TYPE_" + guiType);
}



void GUI_SetType(int guiTypeId, string guiType)
{
    sls(__GUI(), GUI_ + "TYPE", guiType, guiTypeId);
}



void GUI_SetTypeId(string guiType, int guiTypeId)
{
    sli(__GUI(), GUI_ + "TYPE_" + guiType, guiTypeId);
}



int GUI_GetLayoutObjectId(object layoutObject)
{
    return gli(layoutObject, GUI_ + "LAYOUT");
}



void GUI_SetLayoutObjectId(object layoutObject, int layoutId)
{
    sli(layoutObject, GUI_ + "LAYOUT", layoutId);
}



void GUI_StopMenu(object player, int abort = FALSE)
{
    string menu = GUI_GetMenuName(player);
    GUI_SetLayoutObjectClicked(player, OBJECT_INVALID);
    GUI_SetMenuName(player, "");
    GUI_SetMaxLayerId(player, -1);
    GUI_UnlockCamera(player);

    if (menu == "")
        return;

    // get list of gui layout objects
    int guiObjects = gli(GUI_GetDefinitionObject(GUI_GetTypeId(menu)), GUI_ + "OBJECT_I_ROWS");
    int guiObject;
    for (guiObject = 1; guiObject <= guiObjects; guiObject++)
    {
        GUI_DestroyLayoutObject(player, guiObject);
        GUI_MenuSetObjectVfxId(player, guiObject, GUI_VFX_EMPTY);
        GUI_MenuSetObjectName(player, guiObject, "");
        GUI_MenuSetObjectPosition(player, guiObject, vi());
    }

    AssignCommand(player, ClearAllActions());

    if (abort)
        ExecuteScript(GUI_ + "END_" + menu, player);
}



void GUI_StartMenu(object player, string guiType)
{
    // camera still locked - nothing should happen
    if (gli(player, GUI_ + "CAMERA_LOCKED"))
        return;

    // empty guiType means active menu type
    if (guiType == "")
        guiType = GUI_GetMenuName(player);

    // empty guiType and empty active menu - invalid state
    if (guiType == "")
        return;

    GUI_LockCamera(player);

    // new menu type - stop current menu and start new sequence loop
    if (GUI_GetMenuName(player) != guiType)
    {
        GUI_SetMenuName(player, guiType);

        // execute the render handler associated to the active menu
        ExecuteScript(GUI_ + "rnd_" + guiType, player);

        GUI_RenderMenuSequence(player);
    }
}



void GUI_LayoutAddGridObjects(int guiTypeId, float width, float height, float distX, float distY, int rows, int columns, float margin, int layer, string layoutResRef)
{
//    SpawnScriptDebugger();
    int row, column, index;
    object defObject = GUI_GetDefinitionObject(guiTypeId);
    float winWidth = GUI_GetLayoutObjectWidth(guiTypeId, GUI_LAYOUT_WINDOW);
    float winHeight = GUI_GetLayoutObjectHeight(guiTypeId, GUI_LAYOUT_WINDOW);
    float cornerX = -winWidth / 2;
    float cornerY = -winHeight / 2;

    logentry("[" + GUI + "] Adding new layout grid objects for GuiTypeID " + itos(guiTypeId) + ": layoutResRef ='" + layoutResRef + "'...");

    for (row = 1; row <= rows; row++)
    {
        for (column = 1; column <= columns; column++)
        {
            index++;
            logentry("[" + GUI + "] Adding new layout grid object (" + itos(row) + "x" + itos(column) + ") for GuiTypeID " + itos(guiTypeId));

            //=IF(MOD(Id,Gridx)=0,Gridx-1,MOD(Id,Gridx)-1)
            int offsetX = index % columns;
            if (offsetX == 0)
                offsetX = columns;
            offsetX -= 1;

            //=Gridy-ROUNDDOWN((Id-1)/Gridy,0)-1
            int offsetY = rows - (index - 1) / rows - 1;

            //=CBLx + Dx + Gx/2 + OffsetX * Gx + OffsetX * Gridm
            float guiX = cornerX + distX + width / 2 + offsetX * width + offsetX * margin;

            //=CBLy + Dy + Gy/2 + OffsetY * Gy + OffsetY * Gridm
            float guiY = cornerY + distY + height / 2 + offsetY * height + offsetY * margin;

            // add GUI nth object
            GUI_LayoutAddObject(guiTypeId, width, height, guiX, guiY, layer, GUI_VFX_EMPTY, layoutResRef);
        }
    }
}



void GUI_SetLayoutObjectWidth(int guiTypeId, int layoutId, float width)
{
    slf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "WIDTH", width, layoutId);
}

float GUI_GetLayoutObjectWidth(int guiTypeId, int layoutId)
{
    return glf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "WIDTH", layoutId);
}

void GUI_SetLayoutObjectHeight(int guiTypeId, int layoutId, float height)
{
    slf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "HEIGHT", height, layoutId);
}

float GUI_GetLayoutObjectHeight(int guiTypeId, int layoutId)
{
    return glf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "HEIGHT", layoutId);
}

void GUI_SetLayoutObjectPosX(int guiTypeId, int layoutId, float posX)
{
    slf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "POSX", posX, layoutId);
}

float GUI_GetLayoutObjectPosX(int guiTypeId, int layoutId)
{
    return glf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "POSX", layoutId);
}

void GUI_SetLayoutObjectPosY(int guiTypeId, int layoutId, float posY)
{
    slf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "POSY", posY, layoutId);
}

float GUI_GetLayoutObjectPosY(int guiTypeId, int layoutId)
{
    return glf(GUI_GetDefinitionObject(guiTypeId), GUI_ + "POSY", layoutId);
}

void GUI_SetLayoutObjectLayer(int guiTypeId, int layoutId, int layerId)
{
    object defObject = GUI_GetDefinitionObject(guiTypeId);
    sli(defObject, GUI_ + "LAYER", layerId, layoutId);

    if (layerId > GUI_GetMaxLayerId(defObject))
        GUI_SetMaxLayerId(defObject, layerId);
}

int GUI_GetLayoutObjectLayer(int guiTypeId, int layoutId)
{
    return gli(GUI_GetDefinitionObject(guiTypeId), GUI_ + "LAYER", layoutId);
}

void GUI_SetLayoutObjectVfxId(int guiTypeId, int layoutId, int vfxId)
{
    sli(GUI_GetDefinitionObject(guiTypeId), GUI_ + "VFX", vfxId, layoutId);
}

int GUI_GetLayoutObjectVfxId(int guiTypeId, int layoutId)
{
    return gli(GUI_GetDefinitionObject(guiTypeId), GUI_ + "VFX", layoutId);
}

void GUI_SetLayoutObjectLayoutResRef(int guiTypeId, int layoutId, string layoutResRef)
{
    sls(GUI_GetDefinitionObject(guiTypeId), GUI_ + "RESREF", layoutResRef, layoutId);
}

string GUI_GetLayoutObjectLayoutResRef(int guiTypeId, int layoutId)
{
    return gls(GUI_GetDefinitionObject(guiTypeId), GUI_ + "RESREF", layoutId);
}



vector GUI_Offset(vector pos, float height, float side, int layerLevel = 0, float degAngle = GUI_CAMERA_ANGLE)
{
    /*

       (targetDist)

        C   a    B
         x------x
          \     |
           \    |
            \   | c (targetZ)
  (height) b \  |
              \ |
               \|
                x
                A
    beta= 90
    gama = 70
    alfa = 180-90-70 = 20

        a            b
    ---------  = ----------
    sin(alfa)     sin(beta)

         b * sin(alfa)
    a = ----------------
            sin(beta)

    b^2 = a^2 + c^2
    c^2 = b^2 - a^2
    c = square_root(b^2 - a^2)

    */


    float targetDist = 0.0f;
    float targetZ = 0.0f;
    float degBeta = 90.0f;
    float degAlfa = degBeta - degAngle;

if (gli(GetFirstPC(), "offset"))
SpawnScriptDebugger();
    targetDist = (height) * sin(degAlfa) / sin(degBeta);
    targetDist -= layerLevel * GUI_LAYER_THICKNESS;
    targetZ = sqrt(pow(height, 2.0) - pow(targetDist, 2.0));

    if (height < 0.0f) targetZ = -targetZ;

    vector targetPos = Vector(pos.x, pos.y, pos.z + targetZ);
    targetPos = GetChangedPosition(targetPos, targetDist, GUI_CAMERA_DIRECTION);
    targetPos = GetChangedPosition(targetPos, side, 0.0f);
    return targetPos;
}



float GUI_PixelsToMeters(float pixels)
{
    return pixels / 100.0f;
}



vector GUI_MenuGetObjectPosition(object player, int guiRow)
{
    return GetPositionFromLocation(gll(player, GUI_ + "MENU_OBJECT_POS", guiRow));
}



void GUI_MenuSetObjectPosition(object player, int guiRow, vector pos)
{
    if (pos == vi())
        sll(player, GUI_ + "MENU_OBJECT_POS", li(), guiRow, SYS_M_DELETE);
    else
        sll(player, GUI_ + "MENU_OBJECT_POS", Location(GetArea(player), pos, 0.0f), guiRow);
}



vector GUI_RenderObject(int layoutId, int xGui, object pc, int baseLayer)
{
    // 1. get the cached position of guiRow from player's active menu
    vector guiObjectPos = GUI_MenuGetObjectPosition(pc, layoutId);

    // position not cached yet
    if (guiObjectPos == vi())
    {
        // 1.1 get base position = player's position + GUI_HEIGHT for the Z axis
        vector pos = GetPosition(pc);
        pos = Vector(pos.x, pos.y, pos.z + GUI_HEIGHT);

        // 1.2. calculate absolute coordinates based on height, size, layer level and orientation
        int guiTypeId = GUI_GetTypeId(GUI_GetMenuName(pc));
        float posY = GUI_GetLayoutObjectPosY(guiTypeId, layoutId);
        float posX = GUI_GetLayoutObjectPosX(guiTypeId, layoutId);
        int layer = GUI_GetLayoutObjectLayer(guiTypeId, layoutId);
        guiObjectPos = GUI_Offset(pos, posY, posX, baseLayer + layer, GUI_CAMERA_ANGLE);

        // 1.3. cache the calculated position on player
        GUI_MenuSetObjectPosition(pc, layoutId, guiObjectPos);
    }

    // 3. apply location-based VFX as a visual representation of the layout object
    AreaVisualEffectForPC(pc, xGui, guiObjectPos);

    // for local testing (the above plugin function doesn't work in windows nwnx2)
    if (gli(GetModule(), "DEBUG"))
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(xGui), Location(GetArea(pc), guiObjectPos, 0.0f));

    return guiObjectPos;
}

// helper function for GUI_LockCamera()
void _locationLoop(object player)
{
    // invalid player ref
    if (!GetIsObjectValid(player))
        return;

    // menu stopped separately
    if (!gli(player, GUI_ + "CAMERA_LOCKED"))
        return;

    location locOrig = gll(player, GUI_ + "CAMERA_LOC");
    location locPlayer = GetLocation(player);

    // player is dead/dying or moved - interrupt current menu
    if (GetCurrentHitPoints(player) <= 0 ||
        (locOrig != locPlayer && GetPositionFromLocation(locOrig) != GetPositionFromLocation(locPlayer)))
    {
        GUI_StopMenu(player, TRUE);
        return;
    }

    DelayCommand(1.0f, _locationLoop(player));
}

void GUI_LockCamera(object player)
{
    if (gli(player, GUI_ + "CAMERA_LOCKED"))
        return;

    sli(player, GUI_ + "CAMERA_LOCKED", TRUE);
    sll(player, GUI_ + "CAMERA_LOC", GetLocation(player));
    AssignCommand(player, SetCameraFacing(GUI_CAMERA_DIRECTION, GUI_CAMERA_DISTANCE, GUI_CAMERA_ANGLE));
    AssignCommand(player, LockCameraDirection(player, TRUE));
    AssignCommand(player, LockCameraPitch(player, TRUE));
    SetCameraHeight(player, GUI_CAMERA_HEIGHT);

    DelayCommand(1.0f, _locationLoop(player));
}



void GUI_UnlockCamera(object player)
{
    SetCameraHeight(player, 0.0f);
    LockCameraDirection(player, FALSE);
    LockCameraPitch(player, FALSE);
    AssignCommand(player, ClearAllActions());
    sli(player, GUI_ + "CAMERA_LOCKED", FALSE, -1, SYS_M_DELETE);
    sll(player, GUI_ + "CAMERA_LOC", li(), -1, SYS_M_DELETE);
}



int GUI_GetSequence(object pc)
{
    return gli(pc, GUI_ + "SEQUENCE");
}



void GUI_SetSequence(object pc, int id = 0)
{
    int seqId = GUI_GetSequence(pc);

    if (id > 0)
        seqId = id;

    else if (id == -1)
        seqId++;

    else if (id == -2)
        seqId = -1;

    if (seqId == -1)
        sli(pc, GUI_ + "SEQUENCE", -1, -1, SYS_M_DELETE);
    else
        sli(pc, GUI_ + "SEQUENCE", seqId);
}



void GUI_RenderMenuSequence(object pc, string menu = "", int id = 0)
{
    // empty menu equals active menmu
    if (menu == "")
        menu = GUI_GetMenuName(pc);

    // menu was stopped
    else if (GUI_GetMenuName(pc) == "")
        return;

    int seqId = GUI_GetSequence(pc);
msg(pc, "SEQ(" + itos(id) + ") started, existing(" + itos(seqId) + ")");

    int guiTypeId = GUI_GetTypeId(menu);
    int clickedSeq = id == -1; // sequence clicked from layout
    object defObject = GUI_GetDefinitionObject(guiTypeId);

    // manual event - adjust sequence ID and base layer level
    int baseLayer;
    if (id <= 0)
    {
        // manually clicked seq
        if (id == -1)
        {
            baseLayer = GUI_GetMaxLayerId(pc) + 1;
            if (baseLayer == 1)
                baseLayer += GUI_GetMaxLayerId(defObject) + 1;
        }

        id = seqId + 1;
    }

    // set active menu sequence id (if -1 then increase)
    if (id != seqId)
        GUI_SetSequence(pc, id);
    if (baseLayer > 0)
        GUI_SetMaxLayerId(pc, baseLayer);

    // get list of objects defined by menu
    int guiObjects = gli(defObject, GUI_ + "OBJECT_I_ROWS");
    int guiObject;
    for (guiObject = 1; guiObject <= guiObjects; guiObject++)
    {
        // first determine whether there is a cached or default VFX id associated to the guiObject
        int vfxId = GUI_MenuGetObjectVfxId(pc, guiObject);

        // no cached VFX id
        if (GUI_GetIsVfxMissingOrEmpty(vfxId))
        {
            vfxId = GUI_GetLayoutObjectVfxId(guiTypeId, guiObject);

            // no default vfx id - skip rendering this object
            if (GUI_GetIsVfxMissingOrEmpty(vfxId))
            {
                GUI_DisableLayoutObject(pc, guiObject);
                continue;
            }
        }

        // reset cached position if using base layer
        if (baseLayer > 0)
            GUI_MenuSetObjectPosition(pc, guiObject, vi());

        // render object by VFX id
        vector guiObjectPos = GUI_RenderObject(guiObject, vfxId, pc, baseLayer);
        string layoutResRef = GUI_GetLayoutObjectLayoutResRef(guiTypeId, guiObject);

        // object doesn't have clickable layout thus no display name
        if (layoutResRef == "")
            continue;

        // get existing layout object
        object layoutObject = GUI_MenuGetObjectLayout(pc, guiObject);

        // reuse existing layout objett
        if (GetIsObjectValid(layoutObject))
            GUI_EnableLayoutObject(pc, guiObject);

        // create new layout object
        else
        {
            guiObjectPos.z += GUI_LAYOUT_Z_OFFSET;
            layoutObject = GUI_CreateLayoutObject(pc, guiObject, layoutResRef, guiObjectPos);

            // error during creation
            if (!GetIsObjectValid(layoutObject))
                continue;
        }

        string label = GUI_MenuGetObjectName(pc, guiObject);
        label = label == "" ? " " : Y1 + label + _C;

        SetName(layoutObject, label);
    }

    if (!clickedSeq)
        DelayCommand(GUI_LIFETIME - 0.05, GUI_RenderMenuSequence(pc, menu, id));
}



int GUI_GetMaxLayerId(object source)
{
    return gli(source, GUI_ + "LAYER_MAX");
}



void GUI_SetMaxLayerId(object source, int layerId = -1)
{
    if (layerId == -1)
        sli(source, GUI_ + "LAYER_MAX", -1, -1, SYS_M_DELETE);
    else
        sli(source, GUI_ + "LAYER_MAX", layerId);
}



int GUI_MenuGetObjectVfxId(object player, int guiRow)
{
    return gli(player, GUI_ + "MENU_OBJECT_VFX", guiRow);
}



void GUI_MenuSetObjectVfxId(object player, int guiRow, int value = GUI_VFX_EMPTY)
{
    if (value == GUI_VFX_EMPTY)
        sli(player, GUI_ + "MENU_OBJECT_VFX", -1, guiRow, SYS_M_DELETE);
    else
        sli(player, GUI_ + "MENU_OBJECT_VFX", value, guiRow);
}



string GUI_MenuGetObjectName(object player, int guiRow)
{
    return gls(player, GUI_ + "MENU_OBJECT_NAME", guiRow);
}



void GUI_MenuSetObjectName(object player, int guiRow, string value = "")
{
    if (value == "")
        sls(player, GUI_ + "MENU_OBJECT_NAME", "", guiRow, SYS_M_DELETE);
    else
        sls(player, GUI_ + "MENU_OBJECT_NAME", value, guiRow);
}



object GUI_MenuGetObjectLayout(object player, int guiRow)
{
    return glo(player, GUI_ + "MENU_OBJECT_LAYOUT", guiRow);
}



void GUI_MenuSetObjectLayout(object player, int guiRow, object value = OBJECT_INVALID)
{
    if (value == OBJECT_INVALID)
        slo(player, GUI_ + "MENU_OBJECT_LAYOUT", OBJECT_INVALID, guiRow, SYS_M_DELETE);
    else
        slo(player, GUI_ + "MENU_OBJECT_LAYOUT", value, guiRow);
}



void GUI_DestroyLayoutObject(object player, int guiRow)
{
    object layoutObject = GUI_MenuGetObjectLayout(player, guiRow);

    if (GetIsObjectValid(layoutObject))
    {
        GUI_SetOwner(layoutObject);
        GUI_SetLayoutObjectId(layoutObject, -1);
        SetPlotFlag(layoutObject, FALSE);
        SetUseableFlag(layoutObject, FALSE);
        DestroyObject(layoutObject, 0.05);
    }
}



object GUI_CreateLayoutObject(object owner, int layoutId, string resRef, vector position)
{
    object layoutObject = CreateObject(OBJECT_TYPE_PLACEABLE, resRef, Location(GetArea(owner), position, GUI_CAMERA_DIRECTION));

    // ERROR creating layout object
    if (!GetIsObjectValid(layoutObject))
        return OBJECT_INVALID;

    GUI_SetLayoutObjectId(layoutObject, layoutId);
    GUI_SetOwner(layoutObject, owner);
    GUI_MenuSetObjectLayout(owner, layoutId, layoutObject);

    return layoutObject;
}



int GUI_GetIsVfxMissingOrEmpty(int vfxId)
{
    return vfxId == GUI_VFX_EMPTY || vfxId == GUI_VFX_MISSING;
}



void GUI_DisableLayoutObject(object player, int guiRow)
{
    object layoutObject = GUI_MenuGetObjectLayout(player, guiRow);

    if (GetIsObjectValid(layoutObject) && GetUseableFlag(layoutObject))
    {
        SetUseableFlag(layoutObject, FALSE);
        SetName(layoutObject, _C);
    }
}



void GUI_EnableLayoutObject(object player, int guiRow)
{
    object layoutObject = GUI_MenuGetObjectLayout(player, guiRow);

    if (GetIsObjectValid(layoutObject) && !GetUseableFlag(layoutObject))
        SetUseableFlag(layoutObject, TRUE);
}

