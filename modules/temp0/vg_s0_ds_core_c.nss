// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Core C                                       |
// | File    || vg_s0_ds_core_c.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 02-07-2008                                                     |
// | Updated || 29-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu DS typu "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_const"
#include    "vg_s0_sys_core_o"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || DS_CreateCorpse                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Vytvori duplikat bytosti oTarget (NPC) a ulozi pozici do databazi.
//
// -----------------------------------------------------------------------------
object  DS_CreateCorpse(object oTarget, location lTarget);



// +---------++----------------------------------------------------------------+
// | Name    || DS_JumpToSphere                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Presune cil oTarget do sfery smrti
//
// -----------------------------------------------------------------------------
void    DS_JumpToSphere(object oTarget, int bCorpse = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || DS_ReturnFromSphere                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati cil oTarget ze sfery smrti
//
// -----------------------------------------------------------------------------
void    DS_ReturnFromSphere(object oTarget, int bToCorpse = FALSE);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                              DS_CreateCorpse                              |
// +---------------------------------------------------------------------------+



object  DS_CreateCorpse(object oTarget, location lTarget)
{
    if  (!GetIsObjectValid(oTarget))    return  OBJECT_INVALID;
    if  (!GetIsPC(oTarget))             return  OBJECT_INVALID;
    if  (GetIsDM(oTarget))              return  OBJECT_INVALID;

    object  oCorpse =   DuplicateCreature(oTarget, lTarget, FALSE, TRUE, FALSE, DS_TAG_DEATH_CORPSE);

    sll(oTarget, SYSTEM_DS_PREFIX + "_CORPSE_LOCATION", GetLocation(oCorpse));
    slo(oTarget, SYSTEM_DS_PREFIX + "_CORPSE", oCorpse);
    slo(oCorpse, SYSTEM_DS_PREFIX + "_CORPSE", oTarget);
    SavePersistentData(oTarget, SYSTEM_DS_PREFIX);
    DelayCommand(0.3f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(FALSE, FALSE), oCorpse));

    return  oCorpse;
}



// +---------------------------------------------------------------------------+
// |                              DS_JumpToSphere                              |
// +---------------------------------------------------------------------------+



void    DS_JumpToSphere(object oTarget, int bCorpse = FALSE)
{
    if  (!GetIsObjectValid(oTarget))    return;
    if  (!GetIsPC(oTarget))             return;
    if  (GetIsDM(oTarget))              return;

    if  (GetIsDead(oTarget))
    {
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), oTarget);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oTarget)), oTarget);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_DEATH_L), oTarget);

        effect  eEffect =   GetFirstEffect(oTarget);

        while   (GetIsEffectValid(eEffect))
        {
            RemoveEffect(oTarget, eEffect);

            eEffect =   GetNextEffect(oTarget);
        }
    }

    location    lLocation   =   GetLocation(GetWaypointByTag(DS_TAG_SPHERE_JUMP));
    location    lTarget     =   GetLocation(oTarget);

    AssignCommand(oTarget, JumpToLocation(lLocation));
    //DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectVisualEffect(VFX_DUR_GLOW_WHITE)), oTarget));

    if  (bCorpse)
    {
        object  oCorpse =   DS_CreateCorpse(oTarget, lTarget);

        DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(FALSE, FALSE), oCorpse));
    }
}



// +---------------------------------------------------------------------------+
// |                          DS_ReturnFromSphere                              |
// +---------------------------------------------------------------------------+



void    DS_ReturnFromSphere(object oTarget, int bToCorpse = FALSE)
{
    if  (!GetIsObjectValid(oTarget))    return;
    if  (!GetIsPC(oTarget))             return;
    if  (GetIsDM(oTarget))              return;

    object  oTemple =   GetWaypointByTag(DS_TAG_SPHERE_RETURN);

    SavePersistentData(oTarget, SYSTEM_DS_PREFIX, "RETURN");
    AssignCommand(oTarget, ClearAllActions(TRUE));
    DelayCommand(0.05f, AssignCommand(oTarget, JumpToLocation(GetLocation(oTemple))));

    /*
    object  oCorpse =   glo(oTarget, SYSTEM_DS_PREFIX + "_CORPSE");
    string  sRegion =   GetSubString(GetTag(GetArea(oCorpse)), 6, 2);

    string  sArea;
    int     n;
    object  oTemp   =   GetObjectByTag(DS_TAG_RETURN_FROM_SPHERE, n);

    while   (GetIsObjectValid(oTemp))
    {
        sArea   =   GetTag(GetArea(oTemp));

        if  (GetSubString(sArea, 6, 2)  ==  sRegion)    break;

        oTemp   =   GetObjectByTag(DS_TAG_RETURN_FROM_SPHERE, ++n);
    }

    object  oArea   =   GetObjectByTag(sArea);
    object  oTemple =   GetNearestObjectByTag(DS_TAG_RETURN_FROM_SPHERE, GetFirstObjectInArea(oArea));

    sll(oTarget, SYSTEM_DS_PREFIX + "_CORPSE_LOCATION", GetLocation(oCorpse), -1, SYS_M_DELETE);
    sli(oTarget, SYSTEM_DS_PREFIX + "_KILLER", FALSE, -1, SYS_M_DELETE);
    slo(oTarget, SYSTEM_DS_PREFIX + "_CORPSE", OBJECT_INVALID, -1, SYS_M_DELETE);
    slo(oCorpse, SYSTEM_DS_PREFIX + "_CORPSE", OBJECT_INVALID, -1, SYS_M_DELETE);
    SavePersistentData(oTarget, SYSTEM_DS_PREFIX);

    if  (!bToCorpse)    AssignCommand(oTarget, JumpToLocation(GetLocation(oTemple)));
    else                AssignCommand(oTarget, JumpToLocation(GetLocation(oCorpse)));

    DelayCommand(0.1f, SetPlotFlag(oCorpse, FALSE));
    DelayCommand(0.1f, AssignCommand(oCorpse, SetIsDestroyable(TRUE, FALSE, FALSE)));
    DelayCommand(0.5f, DestroyObject(oCorpse));

    effect  eEffect =   GetFirstEffect(oTarget);

    while   (GetIsEffectValid(eEffect))
    {
        if  (GetEffectSubType(eEffect)  ==  SUBTYPE_SUPERNATURAL
        &&  GetEffectType(eEffect)      ==  EFFECT_TYPE_VISUALEFFECT)
        {
            RemoveEffect(oTarget, eEffect);
        }

        eEffect =   GetNextEffect(oTarget);
    }
    */
}
