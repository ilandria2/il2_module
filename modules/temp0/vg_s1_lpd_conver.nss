// on creature's conversation script handler

#include  "vg_s0_lpd_core_c"

void main()
{
    // not clicked on npc - ignore
    if (GetListenPatternNumber() != -1)
        return;

    object oPC = GetLastSpeaker();

    if (gli(oPC, "lpd_start")) SpawnScriptDebugger();

    object oNPC = OBJECT_SELF;
    object oDialog = LPD_GetDialog(oNPC);

    //  pokud ma NPC zadefinovany LPD rozhovor, pak se ma zacit
    if  (GetIsObjectValid(oDialog))
    {
        object  oG_SRC  =   LPD_GetConversator(oNPC);

        //  NPC je zaneprazdnen
        if  (GetIsObjectValid(oG_SRC)
        &&  oG_SRC  !=  oPC)
        {
            SYS_Info(oPC, SYSTEM_LPD_PREFIX, "OCCUPIED");
            return;
        }

        object  oG_PC   =   LPD_GetConversator(oPC);

        //  hrac jiz je v rozhovoru (nemuze mit 2)
        if  (GetIsObjectValid(oG_PC)
        &&  oG_PC   !=  oNPC)
        {
            SYS_Info(oPC, SYSTEM_LPD_PREFIX, "SECOND_DIALOG");
            return;
        }

        SYS_Info(oPC, SYSTEM_LPD_PREFIX, "START_CONV");
        //  pokus o zacatek LPD rozhovoru (kliknuti na NPC)
        //LPD_StartConversation(oPC, oNPC, oDialog);
        return;
    }

    else
    {
        //  dostupny obchod
        object  oStore  =   GetNearestObject(OBJECT_TYPE_STORE);

        if  (GetIsObjectValid(oStore)
        &&  GetDistanceBetween(oNPC, oStore)    <=  5.0f)
        {
            OpenStore(oStore, oPC);
            return;
        }

        //  klasicky rozhovor, ktery je defaultne nastaveny na bytosti
        if  (GetCommandable(oNPC))
        {
            ClearAllActions();
            BeginConversation();
            return;
        }
    }
}
