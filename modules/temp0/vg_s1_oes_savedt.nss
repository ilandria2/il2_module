// +---------++----------------------------------------------------------------+
// | Name    || Objects Event System (OES) Save data loop script               |
// | File    || vg_s1_oes_savedt.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-05-2009                                                     |
// | Updated || 20-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pusti smycku pro ukladani dat do SQL
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_d"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    SaveData(object oPC)
{
    object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int     nSystems    =   gli(GetModule(), "SYSTEM_S_ROWS");
    int     bSQL        =   gli(oSystem, SYSTEM_SYS_PREFIX + "_SQL");
    int     nRow        =   1;
    int     bGlobal;
    string  sSystem;

    while   (nRow   <=  nSystems)
    {
        sSystem =   gls(GetModule(), "SYSTEM", nRow);
        bGlobal =   gli(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT");

        if  (bGlobal)
        SavePersistentData(oPC, sSystem);
        nRow++;
    }

    ExportSingleCharacter(oPC);
}



void    main()
{
    object  oPC =   GetFirstPC();

    while   (GetIsObjectValid(oPC))
    {
        if  (!GetIsDM(oPC)) DelayCommand(ran(0.0f, 5.0f, 2), SaveData(oPC));

        oPC =   GetNextPC();
    }
}
