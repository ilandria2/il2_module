// Text Interaction System (TIS) Interaction script - MINE

#include "vg_s0_tis_inter"
#include "vg_s0_acs_core_c"

const int ACTION_TYPE  = 101;

// helper functions
void ApplyMiningAudioVisual();
void ApplyFellingAudioVisual();
string RandomSound();
void FinishAction();

//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    string sMessage;
    int typeTool = (this.ObjectTech == "TREE") ? BASE_ITEM_WOODSMAN_AXE :
                   (this.ObjectTech == "ROCK") ? BASE_ITEM_PICKAXE : -1;

    // equip the right tool first
    if (GetBaseItemType(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, this.Player)) != typeTool)
    {
        string sItem = (this.ObjectTech == "TREE") ? "woodsman axe" :
                       (this.ObjectTech == "ROCK") ? "pickaxe" : "";
        sMessage = C_NORM + "You need to equip a " + C_INTE + sItem + " to your right hand";
    }

    // left hand must be free
    else if (GetIsObjectValid(GetItemInSlot(INVENTORY_SLOT_LEFTHAND, this.Player)))
        sMessage = C_NORM + "You need to free your " + C_INTE + "left hand!";

    else
    {
        object user = TIS_GetUser(this.Object);
        if (GetIsObjectValid(user) && user != this.Player)
            sMessage = C_BOLD + "Someone else is using it.";

        // success
        else
            return TRUE;
    }

    sMessage = ConvertTokens(sMessage, this.Player, FALSE, FALSE, FALSE, FALSE);
    msg(this.Player, sMessage, TRUE, FALSE);
    return FALSE;
}

//////// ACTION'S BODY
void Do()
{
    string sMessage;
    string sSTATE = gls(this.Object, SYSTEM_TIS_PREFIX + "_STATE");

    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        // rock
        if  (this.ObjectTech == "ROCK")
        {
            sSTATE = (sSTATE == "MINE" || sSTATE == "") ? "MINE" : "CRUSH";
            AssignCommand(this.Player, ApplyMiningAudioVisual());
        }

        // tree
        else if (this.ObjectTech == "TREE")
        {
            sSTATE = (sSTATE == "FELL" || sSTATE == "") ? "FELL" : "CUT";
            AssignCommand(this.Player, ApplyFellingAudioVisual());
        }

        sMessage = (sSTATE == "MINE") ? "*Mines a rock*" :
                   (sSTATE == "CRUSH") ? "*Crushes a rock*" :
                   (sSTATE == "FELL") ? "*Fells a tree*" :
                   (sSTATE == "CUT") ? "*Cuts a wood*" : "";
        sMessage = ConvertTokens(sMessage, this.Player, FALSE, FALSE, FALSE, FALSE);
        this.DelaySequence = TIS_INTERACT_SEQUENCE_DELAY * 2;

        AssignCommand(this.Player, SpeakString(sMessage));
        AssignCommand(this.Player, PlayAnimation(ANIMATION_LOOPING_CUSTOM19, 1.5f, this.DelaySequence));
        sls(this.Object, TIS_ + "STATE", sSTATE);
    }

    // delayed sequence
    else
    {
        // audio-visual effects
        if (this.ObjectTech == "ROCK")
            AssignCommand(this.Player, ApplyMiningAudioVisual());
        else if (this.ObjectTech == "TREE")
            AssignCommand(this.Player, ApplyFellingAudioVisual());

        int bTree = sSTATE == "CUT" || sSTATE == "FELL";
        StopAnimation(this.Player);
        AssignCommand(this.Player, PlayAnimation(ANIMATION_LOOPING_CUSTOM19, 1.5f, 3.0f));

        float delayMenu = 2.3f + !bTree * 1.0f;
        DelayCommand(delayMenu, FinishAction());

        this.DelaySequence = -delayMenu;

        object oRight = GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, this.Player);
        int DBCUR = IDS_GetItemValue(oRight, FALSE);
        int DBNEW = DBCUR - TIS_DURABILITY_DECREASE_MINE;

        IDS_UpdateItem(this.Player, oRight, DBNEW, -1, TRUE);
    }
}

void ApplyMiningAudioVisual()
{
    vector vPos = GetAwayVector(GetPosition(this.Player), 0.75f, GetFacing(this.Player));
    string sSound = "cb_ht_daggrston";

    //DelayCommand(0.2f, AssignCommand(this.Player, PlaySound("cb_ht_daggrston")));
    AssignCommand(this.Object, DelayCommand(1.4f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(3.0f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(4.4f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(6.0f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(7.4f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(9.0f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(10.4f, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(12.0f, PlaySound(sSound)));

    if (Random(100) <= 40)
    {
        int nVoice;

        switch (Random(3))
        {
            case 0: nVoice = VOICE_CHAT_GATTACK1; break;
            case 1: nVoice = VOICE_CHAT_GATTACK2; break;
            case 2: nVoice = VOICE_CHAT_GATTACK3; break;
        }

        DelayCommand(1.1f, AssignCommand(this.Player, PlayVoiceChat(nVoice, this.Player)));
        DelayCommand(1.4f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_SPARKS_PARRY), Location(GetArea(this.Player), Vector(vPos.x, vPos.y, vPos.z + 0.5f), itof(Random(360)))));
    }
}

void ApplyFellingAudioVisual()
{
    //DelayCommand(0.2f, AssignCommand(this.Player, PlaySound(sSound)));
    AssignCommand(this.Object, DelayCommand(1.4f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(3.0f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(4.4f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(6.0f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(7.4f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(9.0f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(10.4f, PlaySound(RandomSound())));
    AssignCommand(this.Object, DelayCommand(12.0f, PlaySound(RandomSound())));

    if (Random(100)    <=  25)
    {
        int nVoice;
        switch  (Random(3))
        {
            case 0: nVoice  =   VOICE_CHAT_GATTACK1;    break;
            case 1: nVoice  =   VOICE_CHAT_GATTACK2;    break;
            case 2: nVoice  =   VOICE_CHAT_GATTACK3;    break;
        }

        AssignCommand(this.Player, PlayVoiceChat(nVoice, this.Player));
    }
}

string RandomSound()
{
    string sSound;

    switch (Random(6))
    {
        case 0: sSound = "cb_ht_bladewood1"; break;
        case 1: sSound = "cb_ht_daggrwood"; break;
        case 2: sSound = "cb_ht_metblwood1"; break;
        case 3: sSound = "cb_ht_metblwood2"; break;
        case 4: sSound = "cb_ht_wodblwood1"; break;
        case 5: sSound = "cb_ht_wodblwood2"; break;
    }

    return sSound;
}

void FinishAction()
{
    string objectState = gls(this.Object, TIS_ + "STATE");
    string sMessage;

    // finished mining / felling
    if  (objectState == "MINE"
    ||  objectState == "FELL")
    {
        string sVisual = gls(__TIS(), TIS_ + "SET_AT_" + itos(this.ActionType) + "_TAG_" + this.ObjectTech);;

        // mining
        if (objectState == "MINE")
        {
            objectState = "CRUSH";
            sMessage = "*Mined a rock*";

            ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_CHUNK_STONE_SMALL), GetLocation(this.Object));
        }

        // felling
        else if (objectState == "FELL")
        {
            objectState = "CUT";
            sMessage = "*Felled a tree*";

            ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_DUST_EXPLOSION), GetLocation(this.Object));
        }

        if (GetDistanceBetween(this.Player, this.Object) > TIS_INTERACT_DISTANCE)
            AssignCommand(this.Player, ActionMoveToObject(this.Object, FALSE, 0.5f));

        vector vPos = GetPosition(this.Object);
        object oNewVisual = CreateObject(OBJECT_TYPE_PLACEABLE, sVisual, Location(GetArea(this.Object), Vector(vPos.x, vPos.y, (objectState == "CUT") ? vPos.z - 0.05f : vPos.z), ran(0.0f, 360.0f, 0)), FALSE, GetTag(this.Object));

        sls(oNewVisual, TIS_ + "STATE", objectState);
        slo(this.Player, TIS_ + "OBJECT", oNewVisual, gli(this.Player, TIS_ + "OBJECT_USED"));
        SetDescription(oNewVisual, GetDescription(this.Object));
        SetPlotFlag(this.Object, FALSE);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(TRUE, FALSE), this.Object);
    }

    // finised crushing / cutting
    else
    {
        // crushing
        if (objectState == "CRUSH")
        {
            sMessage = "*Crushed a rock*";

            ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_CHUNK_STONE_SMALL), GetLocation(this.Object));
        }

        // cutting
        else if (objectState == "CUT")
        {
            sMessage = "*Cut a wood*";

            AssignCommand(this.Player, PlaySound("cb_bu_woodlrg"));
        }

        int nAmount = gli(this.Object, TIS_ + "A_" + itos(this.ActionSubType) + "_AMOUNT");
        if  (!nAmount)
            nAmount = gli(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_AS_" + itos(this.ActionSubType) + "_AMOUNT");
        int nRecipe = gli(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_AS_" + itos(this.ActionSubType) + "_RECIPE");
        int nQty = gli(__TIS(), TIS_ + "SET_AS_" + itos(this.ActionSubType) + "_QTY");
        location lLoc = GetRandomLocation(GetArea(this.Player), this.Object, 2.0f);

        // random 75-100% of original quantity
        nQty = ftoi(nQty * itof(100 - Random(26)) / 100);
        nAmount = (!nAmount) ? 1 : nAmount;
        this.DelaySequence = -999.0f; // last sequence no gui

        sli(this.Object, TIS_ + "A_" + itos(this.ActionSubType) + "_AMOUNT", --nAmount);
        ACS_CreateItemUsingRecipe(nRecipe, nQty, lLoc, OBJECT_INVALID, TRUE);

        if (!nAmount)
        {
            SetPlotFlag(this.Object, FALSE);
            DestroyObject(this.Object, 0.1);
        }

        // last sequence with reopen gui
        if (nAmount > -1)
            this.DelaySequence = 0.0;
    }

    sMessage = ConvertTokens(sMessage, this.Player, FALSE, FALSE, FALSE, FALSE);

    AssignCommand(this.Player, SpeakString(sMessage));
    StopAnimation(this.Player);
}


/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

