// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_getstatus.nss                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "GETSTATUS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_t"
#include    "vg_s0_bcs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "GETSTATUS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    object  oTarget =   CCS_GetConvertedObject(sLine, 1, FALSE);
    string  sType   =   CCS_GetConvertedString(sLine, 2, FALSE);
    string  sMessage;

    sType   =   upper(sType);


    /*
        energy, liquid, food, stamina, hp, weight, skills, all

        //examples - ;GETINFO o="t";s="ALL";
        ;getstatus t;               - info o cieli
        ;getstatus s;               - info o mojej postave
        ;getstatus p2;energy;       - info o RS_FATIGUE u hraca #2

        //examples - ;SETINFO s="";n="80%";o="t";
        ;setstatus energy;137;t;    - nastavit RS_FATIGUE na 137 u cile
        ;setstatus liquid;34%;t;    - nastavit FDS_THIRST na 66% u cile
        ;setstatus all;100%;p3;     - nastavit vsechny potreby na 100% u hraca #3
        ;setstatus all;74;p3;       - nastavit vsechny potreby na 74% u hraca #3 (nastavit na ciselnu hodnotu tu nebude fungovat)

    */

    //SpawnScriptDebugger();
    string  sInfo, sName, sPerc, sValue;
    int     nValue, nMax, bAll;

    bAll    =   sType == "ALL";
    sInfo   =   "\n\n" + Y1 + "~~~~~~~~~~~~~~~~~~~~~~\n" + W2 + "Status of the need [" + Y1 + sType + W2 + "] on target:";

    //  vybrany cil je bytost
    if  (GetObjectType(oTarget) ==  OBJECT_TYPE_CREATURE)
    {
        vector  vPos    =   GetPosition(oTarget);

        sInfo   +=  "\n" + W2 + "Name: " + Y1 + GetName(oTarget);

        if  (GetIsPC(oTarget))
        {
            sInfo   +=  "\n" + W2 + "Account: " + Y1 + GetPCPlayerName(oTarget);
            sInfo   +=  "\n" + W2 + "ID: " + Y1 + itos(gli(oTarget, SYSTEM_SYS_PREFIX + "_PLAYER_ID"));
            sInfo   +=  "\n" + W2 + "Public CD-key: " + Y1 + GetPCPublicCDKey(oTarget);
        }

        if  (!GetIsPC(oTarget))
        sInfo   +=  "\n" + W2 + "TAG: " + Y1 + GetTag(oTarget);
        sInfo   +=  "\n" + W2 + "Area: " + Y1 + GetTag(GetArea(oTarget)) + " / " + GetName(GetArea(oTarget));
        sInfo   +=  "\n" + W2 + "Position: " + Y1 + "x" + itos(ftoi(vPos.x)) + " y" + itos(ftoi(vPos.y)) + " z" + itos(ftoi(vPos.z));
        sInfo   +=  "\n" + W2 + "Angle: " + Y1 + itos(ftoi(GetFacing(oTarget))) + "s / " + itos(ftoi(GetFacing(oTarget)) / 30) + "h " + Y2 + "(0H/12H = East, 3H = North, 6H = West, 9H = South)";

        //  Vlastnosti (AC, AB, BAB, ...)
        sInfo   +=  "\n\n" + Y1 + "Properties:";
        sInfo   +=  "\n" + W2 + "AC(" + Y1 + itos(GetAC(oTarget)) + W2 + ")";
        sInfo   +=  ", AB(" + Y1 + "???" + W2 + ")";
        sInfo   +=  ", BAB(" + Y1 + itos(GetBaseAttackBonus(oTarget)) + W2 + ")";
        sInfo   +=  ", Class(" + Y1 + Get2DAString("classes", "Label", GetClassByPosition(1, oTarget)) + W2 + ")";

        //  potreby
        sInfo   +=  "\n\n" + Y1 + "Needs:";

        //  hp
        if  (bAll
        ||  sType   ==  "HP")
        {
            nValue  =   GetCurrentHitPoints(oTarget);
            nMax    =   GetMaxHitPoints(oTarget);
            sValue  =   itos(nValue) + " / " + itos(nMax);
            sPerc   =   itos(ftoi(itof(nValue) / nMax * 100)) + "%";
            sName   =   "Health";
            sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, FALSE) + C_NORM + " " + sName + " " + sValue;
        }

        //  zjisti statusy potreb hracskych postav
        if  (GetIsPC(oTarget) && !GetIsDM(oTarget))
        {
            //  energie
            if  (bAll
            ||  sType   ==  "ENERGY")
            {
                nValue  =   gli(oTarget, SYSTEM_RS_PREFIX + "_FATIGUE");
                nMax    =   RS_FATIGUE_EDGE_FSLEEP;
                sValue  =   itos(nMax - nValue) + " / " + itos(nMax);
                sPerc   =   itos(100 - ftoi(itof(nValue) / nMax * 100)) + "%";
                sName   =   "Energy";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;
            }

            //  hydratace
            if  (bAll
            ||  sType   ==  "LIQUID")
            {
                nValue  =   gli(oTarget, SYSTEM_FDS_PREFIX + "_THIRST");
                nMax    =   FDS_DEADLY_EDGE_THIRST;
                sValue  =   itos(nMax - nValue) + " / " + itos(nMax);
                sPerc   =   itos(100 - ftoi(itof(nValue) / nMax * 100)) + "%";
                sName   =   "Hydratation";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;
            }

            //  jidlo
            if  (bAll
            ||  sType   ==  "FOOD")
            {
                nValue  =   gli(oTarget, SYSTEM_FDS_PREFIX + "_HUNGER");
                nMax    =   FDS_DEADLY_EDGE_HUNGER;
                sValue  =   itos(nMax - nValue) + " / " + itos(nMax);
                sPerc   =   itos(100 - ftoi(itof(nValue) / nMax * 100)) + "%";
                sName   =   "Food";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;
            }

            //  vydrz/stamina
            if  (bAll
            ||  sType   ==  "STAMINA")
            {
                nValue  =   gli(oTarget, SYSTEM_BCS_PREFIX + "_STAMINA");
                nMax    =   gli(oTarget, SYSTEM_BCS_PREFIX + "_STAMINA_MAX");
                sValue  =   itos(nValue) + " / " + itos(nMax);
                sPerc   =   itos(ftoi(itof(nValue) / nMax * 100)) + "%";
                sName   =   "Stamina";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, FALSE) + C_NORM + " " + sName + " " + sValue;
            }

            //  power
            if  (bAll
            ||  sType   ==  "POWER")
            {
                nValue  =   gli(oTarget, SYSTEM_BCS_PREFIX + "_POWER");
                nMax    =   gli(oTarget, SYSTEM_BCS_PREFIX + "_POWER_MAX");
                sValue  =   itos(nValue) + " / " + itos(nMax);
                sPerc   =   itos(ftoi(itof(nValue) / nMax * 100)) + "%";
                sName   =   "Power";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, FALSE) + C_NORM + " " + sName + " " + sValue;
            }

            //  progress
            if  (bAll
            ||  sType   ==  "PROGRESS")
            {
                nValue  =   gli(oTarget, SYSTEM_BCS_PREFIX + "_PROGRESS_ACTION");
                nMax    =   100;
                sValue  =   itos(nValue) + " / " + itos(nMax);
                sPerc   =   itos(nValue) + "%";
                sName   =   "Action progress";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, FALSE) + C_NORM + " " + sName + " " + sValue;
            }

            //  nosnost
            if  (bAll
            ||  sType   ==  "WEIGHT")
            {
                nValue  =   GetWeight(oTarget) / 10;
                nMax    =   GetAbilityScore(oTarget, ABILITY_STRENGTH) * 3000;
                sValue  =   itos(nMax - nValue) + " / " + itos(nMax);
                sPerc   =   itos(100 - ftoi(itof(nValue) / nMax * 100)) + "%";
                sName   =   "Weight capacity";
                sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;
            }

            //  znalosti
            if  (bAll
            ||  sType   ==  "SKILLS")
            {
                sInfo   +=  "\n\n" + Y1 + "Skills:\n" + W2;
                sInfo   +=  "Hunter(" + itos(gli(oTarget, "BODY_LEVEL")) + ")";
                sInfo   +=  ", Alchemist(" + itos(gli(oTarget, "PLANT_LEVEL")) + ")";
                //sInfo   +=  ", Huba�(" + itos(gli(oTarget, "MUSHROOM_LEVEL")) + ")";
                sInfo   +=  ", Miner(" + itos(gli(oTarget, "ROCK_LEVEL")) + ")";
                sInfo   +=  ", Woodsman(" + itos(gli(oTarget, "TREE_LEVEL")) + ")";
            }
        }// player
    }// creature

    msg(oPC, sInfo);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

