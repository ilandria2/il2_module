// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) Sys info                           |
// | File    || vg_s1_fds_info.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 18-06-2008                                                     |
// | Updated || 05-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu FDS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_fds_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_FDS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "HUNGER_C")
    {
        sMessage    =   C_NEGA + "!!! Extreeme hunger !!!";
    }

    else

    if  (sCase  ==  "THIRST_C")
    {
        sMessage    =   C_NEGA + "!!! Extreeme thirst !!!";
    }

    else

    if  (sCase  ==  "HUNGER_D")
    {
        sMessage    =   C_NEGA + "Your character died starving from hunger";
    }

    else

    if  (sCase  ==  "THIRST_D")
    {
        sMessage    =   C_NEGA + "Your character died starving from thirst";
    }

    else

    if  (sCase  ==  "SPHERE")
    {
        sMessage    =   C_NORM + "Your character cannot eat or drink in this area";
    }

    msg(OBJECT_SELF, sMessage, TRUE, FALSE);
}
