// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_help.nss                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "HELP"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "HELP";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    string  sMessage;
    string  sQuery  =   "";
    int     nPCID   =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

    sQuery  =   "INSERT INTO psi_pcreports VALUES (" +
                itos(nPCID) + ",'" +
                GetPCPlayerName(oPC) + "','" +
                GetName(oPC) + "','" +
                GetPCIPAddress(oPC) +
                "','HELP','N/A','" +
                SQLEncodeSpecialChars(sText) + "','" +
                LocationToString(GetLocation(oPC)) +
                "','NEW','N/A',now(), now())";

    SQLExecDirect(sQuery);

    sMessage    =   C_NEGA + "[" + sChannel + " - " + C_BOLD + GetPCPlayerName(oPC) + " - " + GetName(GetArea(oPC)) + C_NEGA + "]: " + C_NORM + sText;

    object  oTemp   =   GetFirstPC();
    int     nCount;

    while   (GetIsObjectValid(oTemp))
    {
        if  (GetIsDM(oTemp)
        ||  CCS_GetHasAccess(oTemp, "PM"))
        {
            if  (oPC    !=  oTemp)
            {
                nCount++;

                msg(oTemp, sMessage, TRUE, FALSE);
            }
        }

        oTemp   =   GetNextPC();
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "Added a new help entry to the database:\n" + Y2 + sText;

    msg(oPC, sMessage, FALSE, FALSE);
    logentry("[" + sChannel + " - " + GetPCPlayerName(oPC) + " - " + GetTag(GetArea(oPC)) + " - " + GetName(GetArea(oPC)) + "]: " + sText, TRUE);

    /*
    if  (nCount >   0)
    {
        sMessage    =   C_POSI + "Zpr�va o probl�mu byla doru�ena [" + C_BOLD + itos(nCount) + C_POSI + "] DM";

        msg(oPC, sMessage, TRUE, FALSE);
    }

    else
    {
        sMessage    =   C_NEGA + "Zpr�va o probl�mu nemohla b�t doru�ena proto�e ve h�e nen� ��dn� DM";

        msg(oPC, sMessage, TRUE, FALSE);
    }
    */

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
