
#include "aps_include"
#include "vg_s0_ids_core_c"

void main()
{
    if (gli(GetFirstPC(), "itempers"))SpawnScriptDebugger();

    string sParam = gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_SAVE_PARAM", -1, SYS_M_DELETE);
    string query;

    // if SELF is IDS placed_list object then we have to deal with a destroyed placed_item from this list
    if (left(GetTag(OBJECT_SELF), 8) == IDS_ + "AREA")
    {
        object placed_list = OBJECT_SELF;
        int item_oid = stoi(sParam);
        query = "DELETE FROM ids_placed_items WHERE item_oid = " + itos(item_oid);

        IDS_SetPlacedItemStateById(item_oid, IDS_PLACED_ITEM_STATE_DESTROYED, placed_list);
        logentry("[" + IDS + "] Destroying placed_item in DB by id: " + itos(item_oid));
    }

    // if SELF is placeable we have to deal with a modified placed_item
    else if (GetObjectType(OBJECT_SELF) == OBJECT_TYPE_PLACEABLE)
    {
        object placed_item = OBJECT_SELF;
        string item_tech = gls(placed_item, IDS_ + "ITEM_TECH");

        if (item_tech == "")
            return;

        int item_oid = GetTechPart(item_tech, TECH_PART_OBJECT_ID);
        string item_tag = gls(placed_item, IDS_ + "ITEM_TAG");
        if (item_oid <= 0)
        {
            logentry("[" + IDS + "] Attempted to persist placed item [tag: " + item_tag + "] with invalid object_id [" + itos(item_oid) + "]");
            return;
        }

        int item_state = stoi(sParam);

        if (item_state == IDS_PLACED_ITEM_STATE_DEFAULT ||
            item_state == IDS_PLACED_ITEM_STATE_SPAWNED)
            return;

        // item was changed = insert new or update existing
        if (item_state == IDS_PLACED_ITEM_STATE_CHANGED)
        {
            int item_weight = gli(placed_item, IDS_ + "ITEM_WEIGHT");
            int item_dbmax = gli(placed_item, IDS_ + "ITEM_DBMAX");
            int item_dbcur = gli(placed_item, IDS_ + "ITEM_DBCUR"); /// xxxx wrong dbcur when onDamaged
            int item_stack = gli(placed_item, IDS_ + "ITEM_STACK");
            int item_type = gli(placed_item, IDS_ + "ITEM_TYPE");
            string item_loc = APSLocationToString(GetLocation(placed_item));
            string item_name = gls(placed_item, IDS_ + "ITEM_NAME");
            string item_desc = gls(placed_item, IDS_ + "ITEM_DESC");
            item_name = SQLEncodeSpecialChars(item_name);
            item_desc = SQLEncodeSpecialChars(item_desc);

            query = "INSERT INTO ids_placed_items values (" +
                "" + itos(item_oid) + "" +
                ",'" + item_tag + "'" +
                "," + itos(item_type) + "" +
                ",'" + item_tech + "'" +
                "," + itos(item_stack) + "" +
                "," + itos(item_weight) + "" +
                "," + itos(item_dbcur) + "" +
                "," + itos(item_dbmax) + "" +
                ",'" + item_loc + "'" +
                ",'" + item_name + "'" +
                ",'" + item_desc + "'" +
                ",default" + // created
                ",default" +// updated
                ") ON DUPLICATE KEY UPDATE " +
                "" + "item_tag = " + "'" + item_tag + "'" +
                "," + "item_tech = " + "'" + item_tech + "'" +
                "," + "item_stack = " + "" + itos(item_stack) + "" +
                "," + "item_weight = " + "" + itos(item_weight) + "" +
                "," + "item_dbcur = " + "" + itos(item_dbcur) + "" +
                "," + "item_dbmax = " + "" + itos(item_dbmax) + "" +
                "," + "item_loc = " + "'" + item_loc + "'" +
                "," + "date_mod = default"
                ;

            object placed_list = IDS_GetPlacedItemList(GetTag(GetArea(placed_item)));
            IDS_SetPlacedItemState(placed_item, IDS_PLACED_ITEM_STATE_SPAWNED, placed_list);
            logentry("[" + IDS + "] Inserting/updating placed_item in DB by id: " + itos(item_oid));
        }
    }

    // invalid SELF reference
    else
    {
        logentry("[" + IDS + "] Invalid self reference - unable to handle SAVE data event");
        return;
    }

    SQLExecDirect(query);
}

