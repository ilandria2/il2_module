// persistent data check script for ETS system

#include "aps_include"
#include "vg_s0_sys_core_t"
#include "vg_s0_ets_const"

void main()
{
    // ets_store tabble
    logentry("[" + ETS + "] Checking table [ets_store]");
    SQLExecDirect("DESCRIBE ets_store");

    if (SQLFetch() == SQL_SUCCESS)
        logentry("[" + ETS + "] Table [ets_store] already exist");
    else
    {
        logentry("[" + ETS + "] Table [ets_store] does not exist -> Creating it");
        SQLExecDirect("CREATE TABLE ets_store " +
        "(" +
            "store_id int primary key," +
            "store_resref varchar(16) not null," +
            "store_name varchar(100) not null," +
            "store_loc varchar(200) not null," +
            "store_owner varchar(32) not null," +
            "date timestamp default current_timestamp," +
            "date_mod timestamp default current_timestamp" +
        ")");

        SQLExecDirect("DESCRIBE ets_store");

        if (SQLFetch() == SQL_SUCCESS)
            logentry("[" + ETS + "] Table [ets_store] created successfully");
        else
            logentry("[" + ETS + "] Table [ets_store] not created - error");
    }



    //  ets_store_items tabble
    logentry("[" + ETS + "] Checking table [ets_store_items]");
    SQLExecDirect("DESCRIBE ets_store_items");

    if (SQLFetch() == SQL_SUCCESS)
        logentry("[" + ETS + "] Table [ets_store_items] already exist");
    else
    {
        logentry("[" + ETS + "] Table [ets_store_items] does not exist -> Creating it");
        SQLExecDirect("CREATE TABLE ets_store_items " +
        "(" +
            "store_item_id int primary key," +
            "store_id int," +
            "player_id int null," +
            "item_oid int," +
            "item_tag varchar(32)," +
            "item_tech varchar(100)," +
            "item_stack INT NOT NULL," +
            "item_weight INT NOT NULL," +
            "item_dbcur INT NOT NULL," +
            "item_dbmax INT NOT NULL," +
            "item_name NVARCHAR(100) NOT NULL," +
            "item_desc NVARCHAR(500) NOT NULL," +
            "date TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
            "date_mod TIMESTAMP DEFAULT CURRENT_TIMESTAMP" +
        ")");

        SQLExecDirect("DESCRIBE ets_store_items");

        if (SQLFetch() == SQL_SUCCESS)
            logentry("[" + ETS + "] Table [ets_store_items] created successfully");
        else
            logentry("[" + ETS + "] Table [ets_store_items] not created - error");
    }



    // ets_store_transaction tabble
    logentry("[" + ETS + "] Checking table [ets_store_transaction]");
    SQLExecDirect("DESCRIBE ets_store_transaction");

    if (SQLFetch() == SQL_SUCCESS)
        logentry("[" + ETS + "] Table [ets_store_transaction] already exist");
    else
    {
        logentry("[" + ETS + "] Table [ets_store_transaction] does not exist -> Creating it");
        SQLExecDirect("CREATE TABLE ets_store_transaction " +
        "(" +
            "transaction_id int primary key," +
            "store_id int not null," +
            "player_id int not null," +
            "items_bought int not null," +
            "items_sold int not null," +
            "balance_cleared int not null," +
            "balance_required int not null," +
            "date timestamp default current_timestamp," +
            "date_mod timestamp default current_timestamp" +
        ")");

        SQLExecDirect("DESCRIBE ets_store_transaction");

        if (SQLFetch() == SQL_SUCCESS)
            logentry("[" + ETS + "] Table [ets_store_transaction] created successfully");
        else
            logentry("[" + ETS + "] Table [ets_store_transaction] not created - error");
    }
}
