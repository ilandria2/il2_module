// +---------++----------------------------------------------------------------+
// | Name    || Object Events System (OES) Door event script                   |
// | File    || vg_s4_oes_d_open.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-12-2007                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Udalost oblasti "OnOpen"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_oes_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    OES_ActionEvent(OES_EVENT_D_ON_OPEN);
}
