// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) Constants                          |
// | File    || vg_s0_fds_const.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 18-06-2008                                                     |
// | Updated || 05-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu FDS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_FDS_NAME                     =   "Foods & Drinks System";
const string    SYSTEM_FDS_PREFIX                   =   "FDS";
const string    SYSTEM_FDS_VERSION                  =   "2.00";
const string    SYSTEM_FDS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_FDS_DATE                     =   "18-06-2008";
const string    SYSTEM_FDS_UPDATE                   =   "05-07-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const int       FDS_HUNGER_THIRST_HEARTBEATS        =   10;

//  vaha
const int       FDS_WEIGHT_PERCENT_EDGE_1           =   25;
const int       FDS_WEIGHT_PERCENT_EDGE_2           =   50;
const int       FDS_WEIGHT_PERCENT_EDGE_3           =   75;
const int       FDS_WEIGHT_PERCENT_VALUE_1          =   1;
const int       FDS_WEIGHT_PERCENT_VALUE_2          =   2;
const int       FDS_WEIGHT_PERCENT_VALUE_3          =   3;

/*
//  boj
const int       FDS_ATTACKS_COUNT_EDGE_1            =   10;
const int       FDS_ATTACKS_COUNT_EDGE_2            =   30;
const int       FDS_ATTACKS_COUNT_EDGE_3            =   50;
const int       FDS_ATTACKS_COUNT_VALUE_1           =   1;
const int       FDS_ATTACKS_COUNT_VALUE_2           =   2;
const int       FDS_ATTACKS_COUNT_VALUE_3           =   3;

//  kouzleni
const int       FDS_CONJURE_COUNT_EDGE_1            =   30;
const int       FDS_CONJURE_COUNT_EDGE_2            =   60;
const int       FDS_CONJURE_COUNT_EDGE_3            =   90;
const int       FDS_CONJURE_COUNT_VALUE_1           =   1;
const int       FDS_CONJURE_COUNT_VALUE_2           =   2;
const int       FDS_CONJURE_COUNT_VALUE_3           =   3;
*/

const int       FDS_DEADLY_EDGE_HUNGER              =   350;
const int       FDS_DEADLY_EDGE_THIRST              =   200;
const int       FDS_CRITICAL_EDGE_HUNGER            =   300;
const int       FDS_CRITICAL_EDGE_THIRST            =   175;

