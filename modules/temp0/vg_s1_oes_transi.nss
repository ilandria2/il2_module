// +---------++----------------------------------------------------------------+
// | Name    || Object Events System (OES) Area Transition script              |
// | File    || vg_s1_oes_transi.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 25-10-2009                                                     |
// | Updated || 25-10-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ked je zadefinovany prechod pro dvere / spoust, pak bude bytost presunuta
    k tomuto objektu.

    (manualni prechod nefunguje kdyz je zadefinovany skript)
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oCreature   =   GetClickingObject();
    object  oTarget     =   GetTransitionTarget(OBJECT_SELF);

    if  (!GetIsObjectValid(oTarget))    return;

    AssignCommand(oCreature, ActionJumpToLocation(GetLocation(oTarget)));
}
