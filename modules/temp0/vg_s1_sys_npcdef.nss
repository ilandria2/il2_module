// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Commoner definition script              |
// | File    || vg_s1_sys_npcdef.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 12-09-2009                                                     |
// | Updated || 12-09-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Definice NPC pro generator podle regionu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");

//  ----------------------------------------------------------------------------
//  Definice zdroju

    //  jmena - muzi
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HUMAN) + "_NAME", "�lov�k (Mu�)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HALFELF) + "_NAME", "P�lelf (Mu�)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HALFLING) + "_NAME", "�otek (Mu�)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HALFORC) + "_NAME", "P�lork (Mu�)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_GNOME) + "_NAME", "Gn�m (Mu�)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_ELF) + "_NAME", "Elf (Mu�)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_DWARF) + "_NAME", "Trpasl�k (Mu�)");

    //  jmena - zeny
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HUMAN) + "_NAME", "�lov�k (�ena)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HALFELF) + "_NAME", "P�lelf (�ena)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HALFLING) + "_NAME", "�otek (�ena)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HALFORC) + "_NAME", "P�lork (�ena)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_GNOME) + "_NAME", "Gn�m (�ena)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_ELF) + "_NAME", "Elf (�ena)");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_DWARF) + "_NAME", "Trpasl�k (�ena)");

    //  nazvy sloupcu z 2da
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "HUMAN", RACIAL_TYPE_HUMAN);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "HALFELF", RACIAL_TYPE_HALFELF);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "HALFING", RACIAL_TYPE_HALFLING);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "HALFORC", RACIAL_TYPE_HALFORC);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "ELF", RACIAL_TYPE_ELF);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "GNOME", RACIAL_TYPE_GNOME);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", "DWARF", RACIAL_TYPE_DWARF);

    //  zdroje - casti TAGu
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_HUMAN) + "_PART", "VG_C_S1_NPCHU");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_HALFELF) + "_PART", "VG_C_S1_NPCHE");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_HALFLING) + "_PART", "VG_C_S1_NPCHI");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_HALFORC) + "_PART", "VG_C_S1_NPCHO");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_GNOME) + "_PART", "VG_C_S1_NPCGN");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_ELF) + "_PART", "VG_C_S1_NPCEL");
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(RACIAL_TYPE_DWARF) + "_PART", "VG_C_S1_NPCDW");

    //  pocet zdroju - muzi
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HUMAN) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HALFELF) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HALFLING) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_HALFORC) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_GNOME) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_ELF) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_M" + itos(RACIAL_TYPE_DWARF) + "_COUNT", 1);

    //  pocet zdroju - zeny
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HUMAN) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HALFELF) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HALFLING) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_HALFORC) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_GNOME) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_ELF) + "_COUNT", 1);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_F" + itos(RACIAL_TYPE_DWARF) + "_COUNT", 1);

    //  vzhled - hlavy
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HUMAN) + "_M_HEAD", 117);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HUMAN) + "_F_HEAD", 104);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFELF) + "_M_HEAD", 8);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFELF) + "_F_HEAD", 30);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFLING) + "_M_HEAD", 30);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFLING) + "_F_HEAD", 28);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFORC) + "_M_HEAD", 27);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFORC) + "_F_HEAD", 14);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_ELF) + "_M_HEAD", 34);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_ELF) + "_F_HEAD", 55);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_GNOME) + "_M_HEAD", 25);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_GNOME) + "_F_HEAD", 10);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_DWARF) + "_M_HEAD", 24);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_DWARF) + "_F_HEAD", 23);

    //  vzhled - kuze
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HUMAN) + "_SKIN", 13);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFELF) + "_SKIN", 8);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFLING) + "_SKIN", 5);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFORC) + "_SKIN", 27);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_ELF) + "_SKIN", 8);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_GNOME) + "_SKIN", 5);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_DWARF) + "_SKIN", 5);

    //  vzhled - vlasy
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HUMAN) + "_HAIR", 68);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFELF) + "_HAIR", 68);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFLING) + "_HAIR", 68);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_HALFORC) + "_HAIR", 68);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_ELF) + "_HAIR", 68);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_GNOME) + "_HAIR", 68);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(RACIAL_TYPE_DWARF) + "_HAIR", 68);

//  Definice zdroju
//  ----------------------------------------------------------------------------

    string  sRegion;

//  ----------------------------------------------------------------------------
//  Region 00 (SYS / TEST)

    sRegion =   "00";

    //  skupiny
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", "VILLAGER", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 60, -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", "CITIZEN", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 20, -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_", "NOBLEMAN", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 10, -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", "BEGGAR", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 10, -2);

    //  rasy
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 40, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);

    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 50, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 5, -2);

    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 70, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 7, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 2, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 3, -2);

    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 55, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 5, -2);

    //  pohlavi
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_GENDER_CHANCE", 30);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_GENDER_CHANCE", 40);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_GENDER_CHANCE", 50);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_GENDER_CHANCE", 35);

    //  phenotype
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_PHENOTYPE_CHANCE", 20);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_PHENOTYPE_CHANCE", 50);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_PHENOTYPE_CHANCE", 70);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_PHENOTYPE_CHANCE", 5);

    //  obleceni - muzi
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_M_CLOTHES", "NK_I_CC_ALBEGM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_M_CLOTHES", "NK_I_CC_ALBEGM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_M_CLOTHES", "NK_I_CC_ALBEGM03", -2);

    //  obleceni - zeny
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_F_CLOTHES", "NK_I_CC_ALBEGF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_F_CLOTHES", "NK_I_CC_ALBEGF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_F_CLOTHES", "NK_I_CC_ALBEGF01", -2);

    //  zlato
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_GOLD_MAX", 5);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_GOLD_MAX", 15);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_GOLD_MAX", 30);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_GOLD_MAX", 0);

//  Region 00 (SYS / TEST)
//  ----------------------------------------------------------------------------


//  ----------------------------------------------------------------------------
//  Region AL (Alazor)

    sRegion =   "AL";

    //  skupiny
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", "VILLAGER", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 60, -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", "CITIZEN", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 20, -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_", "NOBLEMAN", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 10, -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", "BEGGAR", -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", 10, -2);

    //  rasy
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 40, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_RACE_CHANCE", 10, -2);

    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 50, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_RACE_CHANCE", 5, -2);

    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 70, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 7, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 2, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_RACE_CHANCE", 3, -2);

    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HUMAN, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 55, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HALFELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HALFLING, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_HALFORC, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_GNOME, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 10, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_ELF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 5, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_ID", RACIAL_TYPE_DWARF, -2);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_RACE_CHANCE", 5, -2);

    //  pohlavi
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_GENDER_CHANCE", 30);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_GENDER_CHANCE", 40);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_GENDER_CHANCE", 50);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_GENDER_CHANCE", 35);

    //  phenotype
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_PHENOTYPE_CHANCE", 20);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_PHENOTYPE_CHANCE", 50);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_PHENOTYPE_CHANCE", 70);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_PHENOTYPE_CHANCE", 5);

    //  obleceni - muzi
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_M_CLOTHES", "NK_I_CC_ALCOMM06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_M_CLOTHES", "NK_I_CC_ALCOMM06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_M_CLOTHES", "NK_I_CC_ALNOBM04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_M_CLOTHES", "NK_I_CC_ALBEGM01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_M_CLOTHES", "NK_I_CC_ALBEGM02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_M_CLOTHES", "NK_I_CC_ALBEGM03", -2);

    //  obleceni - zeny
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_F_CLOTHES", "NK_I_CC_ALCOMF06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_F_CLOTHES", "NK_I_CC_ALCOMF06", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF02", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF03", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF04", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_F_CLOTHES", "NK_I_CC_ALNOBF05", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_F_CLOTHES", "NK_I_CC_ALBEGF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_F_CLOTHES", "NK_I_CC_ALBEGF01", -2);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_F_CLOTHES", "NK_I_CC_ALBEGF01", -2);

    //  zlato
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_VILLAGER_GOLD_MAX", 5);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_CITIZEN_GOLD_MAX", 15);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_NOBLEMAN_GOLD_MAX", 30);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_GOLD_MIN", 0);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_BEGGAR_GOLD_MAX", 0);

//  Region AL (Alazor)
//  ----------------------------------------------------------------------------
}
