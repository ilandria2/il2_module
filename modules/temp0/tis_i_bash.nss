// Text Interaction System (TIS) Interaction script - BASH

//this = TIS_ConstructInteraction();
#include "vg_s0_tis_inter"

const int ACTION_TYPE  = 114;

// Interaction-specific helper functions
void ApplyPlantAudio(object player);
void ApplyPlantDestroyAudio(object player);


//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    return TRUE;
}

//////// ACTION'S BODY
void Do()
{
    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        string  sMessage;

        //  plants and mushrooms are to be stepped on and no attack needed
        if  (this.ObjectTech  ==  "PLANT"
        ||  this.ObjectTech   ==  "MUSHROOM")
        {
            sMessage = "*Steps on something*";
            sMessage = ConvertTokens(sMessage, this.Player, FALSE, TRUE, FALSE, FALSE);

            AssignCommand(this.Player, ActionMoveToLocation(GetLocation(this.Object), FALSE));
            AssignCommand(this.Player, SpeakString(sMessage));
            DelayCommand(0.5f, ApplyPlantDestroyAudio(this.Player));
            DelayCommand(1.0f, ApplyPlantDestroyAudio(this.Player));
            DelayCommand(1.5f, ApplyPlantDestroyAudio(this.Player));
            DelayCommand(1.5f, DestroyObject(this.Object));
            DelayCommand(1.7f, AssignCommand(this.Player, SetFacing(GetFacing(this.Player) - 180.0f)));
        }

        //  everything else is solved by a standard attack command
        else
        {
            sMessage    =   "*Bashes something*";
            sMessage    =   ConvertTokens(sMessage, this.Player, FALSE, TRUE, FALSE, FALSE);

            AssignCommand(this.Player, SpeakString(sMessage));
            DelayCommand(0.3f, AssignCommand(this.Player, ActionAttack(this.Object)));
        }
    }

    // delayed sequences
    else
    {
        // last sequence
        if (TRUE)
            this.DelaySequence = 0.0;
    }
}



void    ApplyPlantAudio(object player)
{
    int     nDice;
    string  sSound;

    nDice   =   Random(5);

    switch  (nDice)
    {
    case 0: sSound  =   "as_na_grassmove1"; break;
    case 1: sSound  =   "as_na_grassmove2"; break;
    case 2: sSound  =   "as_na_leafmove1";  break;
    case 3: sSound  =   "as_na_leafmove2";  break;
    case 4: sSound  =   "as_na_leafmove3";  break;
    }

    AssignCommand(player, PlaySound(sSound));
}



void    ApplyPlantDestroyAudio(object player)
{
    int     nDice;
    string  sSound;

    nDice   =   Random(7);

    switch  (nDice)
    {
    case 0: sSound  =   "as_na_twigsnap1";  break;
    case 1: sSound  =   "as_na_twigsnap2";  break;
    case 2: sSound  =   "as_na_twigsnap3";  break;
    case 3: sSound  =   "as_na_branchsnp1"; break;
    case 4: sSound  =   "as_na_branchsnp2"; break;
    case 5: sSound  =   "as_na_branchsnp3"; break;
    case 6: sSound  =   "as_na_branchsnp4"; break;
    }

    DelayCommand(0.2f, AssignCommand(player, PlaySound(sSound)));
}



/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

