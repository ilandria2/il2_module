// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Sys info                           |
// | File    || vg_s1_acs_info.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu ACS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_acs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "XSTORE_FINISH")
    {

        sMessage    =   C_POSI + "Store transaction successfull!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "TEACH")
    {
        sMessage    =   C_POSI + "Learning successfull!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "STORE_BOUGHT")
    {
        //object  oItem   =   glo(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_STORE_BOUGHT", -1, SYS_M_DELETE);
        //int     nCost   =   gli(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_STORE_PRICE", -1, SYS_M_DELETE);

        //sMessage    =   G2 + "Koupen p�edm�t " + G1 + GetName(oItem) + G2 + " za " + G1 + itos(nCost) + "M";
        sMessage    =   G1 + "Item purchased successfully!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "STORE_SOLD")
    {
        //object  oItem   =   glo(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_STORE_BOUGHT", -1, SYS_M_DELETE);
        //int     nCost   =   gli(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_STORE_PRICE", -1, SYS_M_DELETE);

        //sMessage    =   G2 + "Koupen p�edm�t " + G1 + GetName(oItem) + G2 + " za " + G1 + itos(nCost) + "M";
        sMessage    =   G1 + "Item sold successfully!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "STORE_NOT_ENOUGH_MONEY")
    {
        sMessage    =   R1 + "Insufficient amount of money!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "STORE_NOT_ENOUGH_ITEMS")
    {
        sMessage    =   R1 + "Insufficient amount of items for transaction!";
        bFloat      =   TRUE;
    }

    else
    {

    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}
