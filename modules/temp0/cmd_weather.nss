// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_weather.nss                                                |
// | Author  || VirgiL                                                         |
// | Created || 26-07-2009                                                     |
// | Updated || 26-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "WEATHER
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "WEATHER";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nWeather    =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oArea       =   CCS_GetConvertedObject(sLine, 2, FALSE);
    int     nOldValue   =   GetWeather(oArea);
    int     bOutDoor    =   GetIsAreaNatural(oArea);
    string  sMessage;

    if  (!GetIsObjectValid(oArea))
    {
        sMessage    =   R1 + "( ! ) Invalid area";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (GetIsAreaInterior(oArea))
    {
        sMessage    =   R1 + "( ! ) Cannot change the weather in an interior area";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    SetWeather(oArea, nWeather);

    int     nNewValue   =   GetWeather(oArea);
    string  sWeather;

    switch  (nNewValue)
    {
        case    WEATHER_CLEAR:              sWeather    =   "Clear";    break;
        case    WEATHER_RAIN:               sWeather    =   "Storm";   break;
        case    WEATHER_SNOW:               sWeather    =   "Snow";     break;
        case    WEATHER_USE_AREA_SETTINGS:  sWeather    =   "Default";   break;
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "Changing the weather to [" + G2 + sWeather + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
