// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Impact script                          |
// | File    || vg_s3_scs_conjur.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-05-2009                                                     |
// | Updated || 14-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nastavuje promennou urcujici vybranej mod kouzleni
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void main()
{
    int     nLast       =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT"), SYSTEM_SCS_PREFIX + "_CONJURE_MODE_S_ROWS");
    int     nCurrent    =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONJURE_MODE");
    int     nNew;

    if  (nCurrent   >=  (nLast - 1))    nNew    =   0;
    else                                nNew    =   nCurrent + 1;

    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONJURE_MODE", nNew);
    SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "CONJURE_MODE_SWITCH_&" + itos(nNew)+"&");
}
