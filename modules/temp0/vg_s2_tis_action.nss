// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) - Action skript                  |
// | File    || vg_s2_tis_action.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Provede cinnost TIS vlaken
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"
#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oSource =   OBJECT_SELF;
    object  oPC     =   LPD_GetPC();
    string  sParam  =   upper(gls(oSource, SYS_VAR_PARAMETER, -1, SYS_M_DELETE));

    if(gli(oPC, "tis_debug"))SpawnScriptDebugger();



//  ----------------------------------------------------------------------------
//  ACT - Vykonani vybrane interakce s vybranym objektem

    if  (sParam ==  "ACT")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");          //  system object
        int     nCurAct =   gli(oPC, SYSTEM_TIS_PREFIX + "_ACTION_USED");                       //  p.c. action
        string  sAS     =   itos(gli(oPC, SYSTEM_TIS_PREFIX + "_ACTION", nCurAct));             //  Interaction SubType
        string  sTech   =   gls(oSystem, SYSTEM_TIS_PREFIX + "_AS_" + sAS + "_TECH");           //  Interaction TECH

        ExecuteScript(lower(SYSTEM_TIS_PREFIX + "_I_" + sTech), oPC);
    }

//  ACT - Vykonani vybrane interakce s vybranym objektem
//  ----------------------------------------------------------------------------
//  NOT - No objects found

    if  (sParam ==  "NOT")
        SYS_Info(oPC, SYSTEM_TIS_PREFIX, "NO_OBJECTS");

//  NOT - No objects found
//  ----------------------------------------------------------------------------

}
