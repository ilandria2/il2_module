// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Core M                                 |
// | File    || vg_s0_scs_core_m.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu SCS typu "Math"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_const"
#include    "vg_s0_scs_core_o"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CalculateMaxMana                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte a navrati maximalni uroven many bytosti oCreature na zaklade jeji
//  vlastnosti a faktoru, ktere na ni vplyvaji
//
// -----------------------------------------------------------------------------
int     SCS_CalculateMaxMana(object oCreature);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CalculateRegeneratedMana                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte a navrati uroven obnovene many bytosti oCreature na zaklade jeji
//  vlastnosti a faktoru, ktere na ni vplyvaji
//
// -----------------------------------------------------------------------------
int     SCS_CalculateRegeneratedMana(object oCreature, int bBonus = TRUE, int nBase = -1);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CalculateUsedMana                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte a navrati mnozstvi spotrebovane many pro seslani kouzla nSpell
//  bytosti oCreature
//
// -----------------------------------------------------------------------------
int     SCS_CalculateUsedMana(object oCreature, int nSpell);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetSpellHasMetamagicType                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 pokud kouzlo nSpell lze seslat v metamagii nMetaMagic
//
// -----------------------------------------------------------------------------
int     SCS_GetSpellHasMetamagicType(int nMetaMagic, int nSpell, int bIsHex = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CalculateSpellFailure                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati % jaka velka je sance ze se nepodari seslat kouzlo z urovne skoly
//  kouzel nTarget, kdyz se pouziva uroven skoly nCurrent
//
// -----------------------------------------------------------------------------
int     SCS_CalculateSpellFailure(int nCurrent, int nTarget);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetDomainSpellRowUsed                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati cislo radku z tabulky promennych "SCS_DOMAIN_SPELL_&", ktere udava
//  radek obsahujici ID domeny a pouzity okruh seslaneho kouzla
//  Navrati 0 kdyz kouzlo nebylo seslano jako 'domenni kouzlo' ale jako kouzlo
//  od povolani
//  Navrati -1 kdyz bytost oCreature nema dostatek urovne sesilatele na seslani
//  kouzla nSpell ani u jedne z domen ani bez nich
//  Navrati -2 pri jakekoliv chybe
//
// -----------------------------------------------------------------------------
int     SCS_GetDomainSpellRowUsed(int nSpell, object oCreature = OBJECT_SELF);




// +---------++----------------------------------------------------------------+
// | Name    || SCS_CalculateCasterLevel                                       |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Vypocita kolik urovni sesilatele je potrebnych na seslani kouzla z urovne
//  kouzel nSpellClass
//
// -----------------------------------------------------------------------------
int     SCS_CalculateCasterLevel(int nSpellClass);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CalculateSpellLevel                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati pouzity okruh kouzel pri kouzleni (kvuli DomainSpells)
//
// -----------------------------------------------------------------------------
int     SCS_CalculateSpellLevel(int nSpell, object oCreature);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_ResistSpell                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Do a Spell Resistance check between oCaster and oTarget, returning TRUE if
//  the spell was resisted.
//  * Return value if oCaster or oTarget is an invalid object: FALSE
//  * Return value if spell cast is not a player spell: - 1
//  * Return value if spell resisted: 1
//  * Return value if spell resisted via magic immunity: 2
//  * Return value if spell resisted via spell absorption: 3
//
//  **  TAKEN FROM ResistSpell() function description
//
// -----------------------------------------------------------------------------
int     SCS_ResistSpell(object oCaster, object oTarget, float fDelay = 0.0f);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetSpellSaveDC                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Vypocita DC kuzla (10 + okruh + CasterAbilityMod)
//
// -----------------------------------------------------------------------------
int     SCS_GetSpellSaveDC();



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetMetaMagicFeat                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati pouzitej typ metamagie
//
// -----------------------------------------------------------------------------
int     SCS_GetMetaMagicFeat();



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetSpellId                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati ID originalniho kouzla
//
// -----------------------------------------------------------------------------
int     SCS_GetSpellId();



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                            SCS_SummoningCircle                            |
// +---------------------------------------------------------------------------+



int     SCS_CalculateMaxMana(object oCreature)
{
    int     nResult, nAbilityMod, nClass, n2da, nCasterLevel, i, bSpellCaster, nMaxManaValue, nMaxManaPercent;
    float   fFactor;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    i               =   1;
    nClass          =   GetClassByPosition(i, oCreature);
    nAbilityMod     =   gli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_" + Get2DAString("classes", "PrimaryAbil", nClass));
    bSpellCaster    =   StringToInt(Get2DAString("classes", "SpellCaster", nClass));
    n2da            =   StringToInt(GetStringRight(Get2DAString("classes", "AttackBonusTable", nClass), 1));
    nCasterLevel    =   GetLevelByClass(nClass, oCreature);
    nMaxManaValue   =   gli(oCreature, SYSTEM_SCS_PREFIX + "_MAX_MANA_BONUS_VALUE");
    nMaxManaPercent =   gli(oCreature, SYSTEM_SCS_PREFIX + "_MAX_MANA_BONUS_PERCENT");

    while   (i  <=  3)
    {
        if  (bSpellCaster)
        {
            if  (nClass ==  CLASS_TYPE_SORCERER)    fFactor =   SCS_MANA_MAX_FACTOR_SORCERER;
            else
            {
                if  (n2da   ==  1)  fFactor =   SCS_MANA_MAX_FACTOR_LOW_CASTER; else
                if  (n2da   ==  2)  fFactor =   SCS_MANA_MAX_FACTOR_CASTER;     else
                if  (n2da   ==  3)  fFactor =   SCS_MANA_MAX_FACTOR_HIGH_CASTER;
            }

            nResult     +=  ftoi((itof(SCS_MANA_MAX_BASE_VALUE * nCasterLevel)) * (1.0 + (IntToFloat(nAbilityMod) * SCS_MANA_MAX_FACTOR_ABILITY)) * fFactor);
        }

        i++;
        nClass          =   GetClassByPosition(i, oCreature);
        nAbilityMod     =   gli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_" + Get2DAString("classes", "PrimaryAbil", nClass));
        bSpellCaster    =   StringToInt(Get2DAString("classes", "SpellCaster", nClass));
        n2da            =   StringToInt(GetStringRight(Get2DAString("classes", "AttackBonusTable", nClass), 1));
        nCasterLevel    =   GetLevelByClass(nClass, oCreature);
    }

    nResult +=  nMaxManaValue;
    nResult +=  ftoi(itof(nResult) * (itof(nMaxManaPercent) / 100));

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                        SCS_CalculateRegeneratedMana                       |
// +---------------------------------------------------------------------------+



int     SCS_CalculateRegeneratedMana(object oCreature, int bBonus = TRUE, int nBase = -1)
{
    if  (!GetIsObjectValid(oCreature))  return  -1;
    if  (GetIsDead(oCreature))          return  -1;

    int nResult;

    if  (nBase  ==  -1)
    {
        int     nClass, nAbilityMod, bSpellCaster, n2da, nCasterLevel, i,
                nSpellCraft;
        float   fFactor;

        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

        i                   =   1;
        nClass              =   GetClassByPosition(i, oCreature);
        nAbilityMod         =   gli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_" + Get2DAString("classes", "PrimaryAbil", nClass));
        bSpellCaster        =   stoi(Get2DAString("classes", "SpellCaster", nClass));
        n2da                =   stoi(GetStringRight(Get2DAString("classes", "AttackBonusTable", nClass), 1));
        nCasterLevel        =   GetLevelByClass(nClass, oCreature);
        nSpellCraft         =   GetSkillRank(SKILL_SPELLCRAFT, oCreature, FALSE);

        while   (i)
        {
            if  (bSpellCaster)
            {
                fFactor =   (nClass ==  CLASS_TYPE_SORCERER)    ?   SCS_MANA_REGEN_FACTOR_SORCERER      :
                            (n2da   ==  1)                      ?   SCS_MANA_REGEN_FACTOR_LOW_CASTER    :
                            (n2da   ==  2)                      ?   SCS_MANA_REGEN_FACTOR_CASTER        :
                                                                    SCS_MANA_REGEN_FACTOR_HIGH_CASTER;

                nResult +=  FloatToInt((IntToFloat(SCS_MANA_REGEN_BASE_VALUE * nCasterLevel)) * (1.0 + (IntToFloat(nSpellCraft) * SCS_MANA_REGEN_FACTOR_SKILL)) * fFactor);
            }

            if  (i  ==  3)
            break;

            i++;
            nClass          =   GetClassByPosition(i, oCreature);
            nAbilityMod     =   gli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_" + Get2DAString("classes", "PrimaryAbil", nClass));
            bSpellCaster    =   stoi(Get2DAString("classes", "SpellCaster", nClass));
            n2da            =   stoi(GetStringRight(Get2DAString("classes", "AttackBonusTable", nClass), 1));
            nCasterLevel    =   GetLevelByClass(nClass, oCreature);
        }
    }

    else
    {
        nResult =   nBase;
    }

    if  (bBonus)
    {
        int nRestState          =   gli(oCreature, "RS_REST_STATE");
        int nManaRegenValue     =   gli(oCreature, SYSTEM_SCS_PREFIX + "_REGEN_MANA_BONUS_VALUE");
        int nManaRegenPercent   =   gli(oCreature, SYSTEM_SCS_PREFIX + "_REGEN_MANA_BONUS_PERCENT");
        int nAdjustment;

        nResult +=  nManaRegenValue;
        nResult +=  ftoi(itof(nResult) * (itof(nManaRegenPercent) / 100));

        switch  (nRestState)
        {
            case    1:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_MEDITATE;    break;
            case    2:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_SIT;         break;
            case    3:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_NAP;         break;
            case    4:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_SLEEP;       break;
            case    5:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_FSLEEP;      break;
        }

        nAdjustment =   ftoi((itof(nResult) / 100) * nAdjustment);
        nResult     +=  nAdjustment;
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                          SCS_CalculateUsedMana                            |
// +---------------------------------------------------------------------------+



int     SCS_CalculateUsedMana(object oCreature, int nSpell)
{
    if  (!GetIsObjectValid(oCreature))    return  -1;

    if  (nSpell >   SCS_SPELL_SUMMONING_CIRCLE
    &&  nSpell  <=  (SCS_SPELL_SUMMONING_CIRCLE + 8))
    return  (gli(oCreature, SYSTEM_SCS_PREFIX + "_MANA_MAX") / SCS_MANA_COST_SC_COEFFICIENT);

    int     nResult, nAdjustment, nFactor, nMetaMagic, nMode, nInnate;
    object  oSystem;

    int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
    int nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));

    nSpell  =   nSpellOrig;
    oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    //nSpell  -=  (nSpell >=  SCS_FIRST_CONJURE_SPELL)    ?   SCS_FIRST_CONJURE_SPELL :   0;
    nMode   =   gli(oCreature, SYSTEM_SCS_PREFIX + "_CONJURE_MODE");
    nInnate =   SCS_CalculateSpellLevel(nSpell, oCreature);

    if  (nMode  ==  SCS_CONJURE_MODE_FAKE)  return  ((nInnate * 2) + 1);    else
    if  (nMode  ==  SCS_CONJURE_MODE_LEARN) return  (nInnate * 5);

    nMetaMagic  =   gli(oCreature, SYSTEM_SCS_PREFIX + "_METAMAGIC");
    nFactor     =   gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", nMetaMagic);
    nAdjustment =   StringToInt(Get2DAString("scs_spells", "ManaCost", nSpell));

    if  (nInnate    ==  0)  nResult =   SCS_MANA_BASECOST_CANTRIPS;
    else                    nResult =   ftoi(itof(SCS_MANA_BASECOST_HIGHER_LEVELS) * (pow(SCS_MANA_COST_COEFFICIENT, itof(nInnate + nFactor - 1)))) + nAdjustment;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                       SCS_GetSpellHasMetaMagicType                        |
// +---------------------------------------------------------------------------+



int     SCS_GetSpellHasMetamagicType(int nMetaMagic, int nSpell, int bIsHex = FALSE)
{
    int     nResult, nInt;
    string  sHexInt;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    if  (!bIsHex)   nMetaMagic  =   gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", nMetaMagic);
    int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
    int nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));

    nSpell  =   nSpellOrig;
    //nSpell  =   (nSpell >=  SCS_FIRST_CONJURE_SPELL)    ?   nSpell - SCS_FIRST_CONJURE_SPELL    :   nSpell;
    sHexInt =   Get2DAString("spells", "MetaMagic", nSpell);
    nInt    =   htoi(sHexInt);
    nResult =   ((nMetaMagic & nInt)    ==  nMetaMagic) ?   TRUE    :   FALSE;
    nResult =   (nMetaMagic ==  METAMAGIC_NONE  ||  nMetaMagic  ==  METAMAGIC_ANY)  ?   TRUE    :   nResult;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                        SCS_CalculateSpellCastChance                       |
// +---------------------------------------------------------------------------+



int     SCS_CalculateSpellFailure(int nCurrent, int nTarget)
{
    int     nDiff   =   nTarget - nCurrent;
    int     nResult;

    nDiff   =   (nDiff  <   0)  ?   0   :   nDiff;
    nResult =   nDiff * SCS_SPELL_FAILURE_COEFFICIENT;
    nResult =   (nResult    >   100)    ?   100 :   nResult;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                         SCS_CalculateCasterLevel                          |
// +---------------------------------------------------------------------------+



int     SCS_CalculateCasterLevel(int nSpellClass)
{
    int     nResult;

    if  (nSpellClass    <=  1)  nResult =   2;
    else                        nResult =   (nSpellClass * 3) - 2;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                         SCS_CalculateSpellLevel                           |
// +---------------------------------------------------------------------------+



int     SCS_CalculateSpellLevel(int nSpell, object oCreature)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int     nDSRow  =   SCS_GetDomainSpellRowUsed(nSpell, oCreature);
    int     nResult =   (nDSRow <=  0)  ?   stoi(Get2DAString("spells", "Innate", nSpell))  :   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_LEVEL", nDSRow);

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                            SCS_ResistSpell                                |
// +---------------------------------------------------------------------------+



int     SCS_ResistSpell(object oCaster, object oTarget, float fDelay = 0.0f)
{
    if  (GetIsDM(oTarget)   ||  GetIsDead(oTarget)  ||  GetObjectType(oTarget)  !=  OBJECT_TYPE_CREATURE)   return  FALSE;
    if  (fDelay >   0.5f)   fDelay  -=  0.1f;

    int     nResult;
    effect  eVisual;
    string  sMessage;

    nResult =   ResistSpell(oCaster, oTarget);

    if  (nResult    ==  2)
    {
        eVisual     =   EffectVisualEffect(VFX_IMP_GLOBE_USE);

        SYS_Info(oCaster, SYSTEM_SCS_PREFIX, "RESIST_SPELL_&" + GetName(oTarget) + "&_&" + itos(nResult) + "&");
        DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eVisual, oTarget));
    }

    else

    if  (nResult    ==  3)
    {
        if  (fDelay >   0.5f)   fDelay  -=  0.1f;

        eVisual     =   EffectVisualEffect(VFX_IMP_SPELL_MANTLE_USE);

        SYS_Info(oCaster, SYSTEM_SCS_PREFIX, "RESIST_SPELL_&" + GetName(oTarget) + "&_&" + itos(nResult) + "&");
        DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eVisual, oTarget));
    }

    else
    {
        int nSpellResistance    =   GetSpellResistance(oTarget);

        if  (nSpellResistance   >   0)
        {
            int nPenetration;
            int nCasterLevel    =   GetLevelByClass(SCS_GetSpellCastClass(),oCaster);
            int nDice           =   1 + Random(20);

            nPenetration    =   (GetHasFeat(FEAT_SPELL_PENETRATION, oCaster))           ?   2   :   0;
            nPenetration    =   (GetHasFeat(FEAT_GREATER_SPELL_PENETRATION, oCaster))   ?   4   :   nPenetration;
            nPenetration    =   (GetHasFeat(FEAT_EPIC_SPELL_PENETRATION, oCaster))      ?   6   :   nPenetration;

            if  (nDice  ==  1)
            {
                nResult     =   TRUE;
                eVisual     =   EffectVisualEffect(VFX_IMP_MAGIC_RESISTANCE_USE);

                SYS_Info(oCaster, SYSTEM_SCS_PREFIX, "RESIST_SPELL_&" + GetName(oTarget) + "&_&" + itos(nResult) + "&_&" + itos(nDice) + "&_&" + itos(nCasterLevel) + "&_&" + itos(nPenetration) + "&_&" + itos(nSpellResistance) + "&");
                DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eVisual, oTarget));
            }

            else
            {
                int nRoll   =   nCasterLevel + nDice + nPenetration;

                if  (nRoll  <   nSpellResistance)
                {
                    nResult     =   TRUE;
                    eVisual     =   EffectVisualEffect(VFX_IMP_MAGIC_RESISTANCE_USE);

                    DelayCommand(fDelay, ApplyEffectToObject(DURATION_TYPE_INSTANT, eVisual, oTarget));
                }

                else    nResult =   FALSE;

                SYS_Info(oCaster, SYSTEM_SCS_PREFIX, "RESIST_SPELL_&" + GetName(oTarget) + "&_&" + itos(nResult) + "&_&" + itos(nDice) + "&_&" + itos(nCasterLevel) + "&_&" + itos(nPenetration) + "&_&" + itos(nSpellResistance) + "&");
            }
        }
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                             SCS_GetSpellSaveDC                            |
// +---------------------------------------------------------------------------+



int     SCS_GetSpellSaveDC()
{
    int     nResult, nAbilMod, nSpell, nSpellLevel, nClass;
    object  oPC;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    oPC         =   OBJECT_SELF;
    nSpell      =   GetSpellId();
    nSpellLevel =   SCS_CalculateSpellLevel(nSpell, oPC);
    nClass      =   SCS_GetSpellCastClass();
    nAbilMod    =   GetAbilityModifier(gli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_" + Get2DAString("classes", "PrimaryAbil", nClass)), oPC);
    nResult     =   10 + nSpellLevel + nAbilMod;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                            SCS_GetMetaMagicFeat                           |
// +---------------------------------------------------------------------------+



int     SCS_GetMetaMagicFeat()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    //return  gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_METAMAGIC"));
    int nSpell      =   GetSpellId();
    int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;
    int nMeta       =   gli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", nSpellGroup);

    return  nMeta;
}



// +---------------------------------------------------------------------------+
// |                              SCS_GetSpellId                               |
// +---------------------------------------------------------------------------+



int     SCS_GetSpellId()
{
    int nSpell      =   GetSpellId();
    int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;
    int nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800);

    return  nSpellOrig;
}



// +---------------------------------------------------------------------------+
// |                       SCS_GetDomainSpellRowUsed                           |
// +---------------------------------------------------------------------------+



int     SCS_GetDomainSpellRowUsed(int nSpell, object oCreature = OBJECT_SELF)
{
    if  (!GetIsObjectValid(oCreature))                          return  -2;
    if  (GetObjectType(oCreature)   !=  OBJECT_TYPE_CREATURE)   return  -2;

    int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
    int nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));

    nSpell  =   nSpellOrig;
    //if  (nSpell >=  SCS_FIRST_CONJURE_SPELL)    nSpell  -=  SCS_FIRST_CONJURE_SPELL;

    int nResult;
    int nDomain, nLevel, nRow, nFeat;

    object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int     nSources    =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_DOMAIN_I_ROWS");
    int     nCleric     =   GetLevelByClass(CLASS_TYPE_CLERIC, oCreature);

    while   (++nRow <=  nSources)
    {
        nDomain =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_DOMAIN", nRow);
        nLevel  =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_LEVEL", nRow);
        nFeat   =   stoi(Get2DAString("Domains", "GrantedFeat", nDomain));

        if  (GetHasFeat(nFeat, oCreature)
        &&  nCleric >=  SCS_CalculateCasterLevel(nLevel))
        {
            nResult =   nRow;
            break;
        }
    }

    if  (!nResult)
    {
        string  s2da    =   Get2DAString("spells", "Cleric", nSpell);

        //  kouzlo lze seslat pouze kdyz ma domenu
        if  (s2da   ==  "")
        {
            nResult =   -1;
        }

        //  kouzlo lze seslat od povolani ale take od domeny
        else
        {
            nLevel  =   stoi(s2da);
            nResult =   (nCleric    >=  SCS_CalculateCasterLevel(nLevel))   ?   0   :   -1;
        }
    }

    return  nResult;
}
