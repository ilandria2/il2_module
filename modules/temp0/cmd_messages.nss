// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_messages.nss                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "MESSAGES"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "MESSAGES";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    object  oPlayer =   CCS_GetConvertedObject(sLine, 1, FALSE);
    string  sMessage;

    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   C_NEGA + "( ! ) " + W2 + "You must select a player character";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    int bValue;

    bValue  =   gli(oPlayer, SYSTEM_SYS_PREFIX + "_MESSAGES");
    bValue  =   !bValue;

    if  (bValue)
    sMessage    =   Y1 + "( ? ) " + W2 + "System messages on player [" + Y1 + GetPCPlayerName(oPlayer) + W2 + "]: " + G1 + "Enabled";

    else
    sMessage    =   Y1 + "( ? ) " + W2 + "System messages on player [" + Y1 + GetPCPlayerName(oPlayer) + W2 + "]: " + R1 + "Disabled";

    sli(oPlayer, SYSTEM_SYS_PREFIX + "_MESSAGES", bValue);
    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

