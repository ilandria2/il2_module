// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Mana potion script                     |
// | File    || vg_s3_scs_potion.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-05-2009                                                     |
// | Updated || 16-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Na zaklade typu lektvaru many se zvysi uroven many hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC         =   OBJECT_SELF;
    int     nSpell      =   GetSpellId();
    int     nCurrent    =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA");
    int     nMax        =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX");
    int     nRestore;

    switch  (nSpell)
    {
        case    SCS_SPELL_MANA_RESTORE_LIGHT:       nRestore    =   5 + (1 + Random(5));            break;
        case    SCS_SPELL_MANA_RESTORE_MEDIUM:      nRestore    =   15 + (1 + Random(10));          break;
        case    SCS_SPELL_MANA_RESTORE_GREATER:     nRestore    =   35 + (1 + Random(15));          break;
        case    SCS_SPELL_MANA_RESTORE_STRONG:      nRestore    =   60 + (1 + Random(20));          break;
        case    SCS_SPELL_MANA_RESTORE_MONSTROUS:   nRestore    =   100 + (1 + Random(25));         break;
        case    SCS_SPELL_MANA_RESTORE:             nRestore    =   nMax / 3 + (1 + Random(30));    break;
    }

    if  ((nCurrent + nRestore)  >   nMax)   nRestore    =   nMax - nCurrent;

    sli(oPC, SYSTEM_SCS_PREFIX + "_MANA", nRestore, -1, SYS_M_INCREMENT);
    SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&" + itos(nCurrent) + "&_&" + itos(nCurrent + nRestore) + "&");
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEALING_G), oPC);
    AssignCommand(oPC, ActionDoCommand(PlaySound("sce_positive")));
    SignalEvent(oPC, EventSpellCastAt(oPC, nSpell, FALSE));
}
