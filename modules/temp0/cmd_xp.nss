// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_xp.nss                                                     |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "XP"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_xps_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "XP";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oXPS    =   GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nAmount =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     nClass  =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    int     bSet    =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    object  oPlayer =   CCS_GetConvertedObject(sLine, 4, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� objekt";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neni to bytost
    if  (GetObjectType(oPlayer) !=  OBJECT_TYPE_CREATURE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� typ objektu";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neni to hrac
    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Nen� to hr��";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    string  sClass;

    //  seznam trid
    if  (nClass ==  -1)
    {
        int n   =   0;

        sClass      =   gls(oXPS, SYSTEM_XPS_PREFIX + "_CLASS", n + 1);
        sMessage    =   Y1 + "( ? ) " + W2 + "Seznam t��d zku�enosti: ";

        while   (n  <=  XPS_M_CLASS_LAST)
        {
            if  (n  >   0)
            sMessage    +=  W2 + ", ";
            sMessage    +=  G2 + itos(n) + " - " + sClass;

            n++;
            sClass  =   gls(oXPS, SYSTEM_XPS_PREFIX + "_CLASS", n + 1);
        }

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sClass  =   gls(oXPS, SYSTEM_XPS_PREFIX + "_CLASS", nClass + 1);

    if  (!bSet) sMessage    =   Y1 + "( ? ) " + W2 + "�prava zku�enost� t��dy [" + G2 + sClass + W2 + "] bytosti [" + G2 + GetName(oPlayer) + W2 + "] o hodnotu [" + G2 + itos(nAmount) + W2 + "]";
    else        sMessage    =   Y1 + "( ? ) " + W2 + "Nastaven� zku�enost� t��dy [" + G2 + sClass + W2 + "] bytosti [" + G2 + GetName(oPlayer) + W2 + "] na hodnotu [" + G2 + itos(nAmount) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);
    XPS_AlterXP(oPlayer, nClass, nAmount, bSet, TRUE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
