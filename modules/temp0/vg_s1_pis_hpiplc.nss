// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) HP Indicator Placeable           |
// | File    || vg_s1_pis_hpiplc.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pri OnPhysicalAttacked u kazdeho paceable se pusti tento skript, ktery
    pravidelne informuje utocnika (hrace) o stavu poskozeni daneho placeable
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_pis_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetLastAttacker(OBJECT_SELF);

    if  (!GetIsPC(oPC)) return;

    int nPlayer =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    int bBusy   =   gli(OBJECT_SELF, SYSTEM_PIS_PREFIX + "_HPI_" + itos(nPlayer) + "_BUSY");

    if  (bBusy) return;

    sli(OBJECT_SELF, SYSTEM_PIS_PREFIX + "_HPI_" + itos(nPlayer) + "_BUSY", TRUE);
    msg(oPC, PIS_GetHealthIndicator(OBJECT_SELF), TRUE, FALSE);
    DelayCommand(12.0f, sli(OBJECT_SELF, SYSTEM_PIS_PREFIX + "_HPI_" + itos(nPlayer) + "_BUSY", FALSE));
}

