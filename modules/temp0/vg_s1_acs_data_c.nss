// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Persistent data check script       |
// | File    || vg_s1_acs_data_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_acs_const"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;

    //  cargo_complete tabble
    sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Checking table [cargo_complete]";
    sQuerry =   "DESCRIBE cargo_complete";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [cargo_complete] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [cargo_complete] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE cargo_complete " +
        "(" +
            "CargoID INT NOT NULL primary key," +
            "EndDate datetime not null," +
            "Completed smallint not null," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE cargo_complete";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [cargo_complete] created successfully"; else
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [cargo_complete] not created - error";

        logentry(sLog);
    }

    //  psi_xstore tabble
    sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Checking table [psi_xstore]";
    sQuerry =   "DESCRIBE psi_xstore";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_xstore] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_xstore] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE psi_xstore " +
        "(" +
            "PCID int not null," +
            "XSI int not null," +
            "RID int not null," +
            "RQTY int not null," +
            "RS1 int null," +
            "RS2 int null," +
            "RS3 int null," +
            "RS4 int null," +
            "RS5 int null," +
            "date TIMESTAMP NOT NULL default now()" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE psi_xstore";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_xstore] created successfully"; else
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_xstore] not created - error";

        logentry(sLog);
    }

    //  psi_pcreports tabble
    sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Checking table [psi_pcreports]";
    sQuerry =   "DESCRIBE psi_pcreports";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_pcreports] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_pcreports] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE psi_pcreports " +
        "(" +
            "PCID int not null," +
            "PCACC varchar(50) not null," +
            "PCNAME varchar(50) not null," +
            "PCIP varchar(50) not null," +
            "TYPE varchar(32) not null," +
            "CATEGORY varchar(32) not null," +
            "MESSAGE varchar(255) not null," +
            "LOCATION varchar(100) not null," +
            "STATUS varchar(32) not null," +
            "COMMENTS varchar(255) not null," +
            "SUBMIT_DATE datetime not null," +
            "MODIFY_DATE datetime not null" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE psi_pcreports";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_pcreports] created successfully"; else
        sLog    =   "[" + SYSTEM_ACS_PREFIX + "] Table [psi_pcreports] not created - error";

        logentry(sLog);
    }
}
