// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Persistent data check script           |
// | File    || vg_s1_scs_data_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_scs_const"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;
    int     nTmp;

    //  scs_spells
    sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Checking table [" + SCS_DB_TABLE_SPELLS + "]";
    sQuerry =   "DESCRIBE " + SCS_DB_TABLE_SPELLS;

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_SPELLS + "] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_SPELLS + "] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE " + SCS_DB_TABLE_SPELLS + " " +
        "(" +
            SCS_DB_COLUMN_CHARACTER_ID + " INT(6) NOT NULL," +
            SCS_DB_COLUMN_SPELL_ID + " INT(4) DEFAULT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE " + SCS_DB_TABLE_SPELLS;

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_SPELLS + "] created successfully"; else
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_SPELLS + "] error in creation";

        logentry(sLog);
    }

    //  scs_mana
    sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Checking table [" + SCS_DB_TABLE_MANA + "]";
    sQuerry =   "DESCRIBE " + SCS_DB_TABLE_MANA;

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_MANA + "] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_MANA + "] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE " + SCS_DB_TABLE_MANA + " " +
        "(" +
            SCS_DB_COLUMN_CHARACTER_ID + " INT(6) NOT NULL," +
            SCS_DB_COLUMN_MANA_VALUE + " INT(4) DEFAULT 0," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE " + SCS_DB_TABLE_MANA;

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_MANA + "] created successfully"; else
        sLog    =   "[" + SYSTEM_SCS_PREFIX + "] Table [" + SCS_DB_TABLE_MANA + "] error in creation";

        logentry(sLog);
    }
}
