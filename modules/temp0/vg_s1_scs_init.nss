// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Sys init                               |
// | File    || vg_s0_scs_init.nss                                             |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-05-2009                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system SCS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_SCS_PREFIX;
    string  sName       =   SYSTEM_SCS_NAME;
    string  sVersion    =   SYSTEM_SCS_VERSION;
    string  sAuthor     =   SYSTEM_SCS_AUTHOR;
    string  sDate       =   SYSTEM_SCS_DATE;
    string  sUpDate     =   SYSTEM_SCS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_scs_info");
    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_scs_data_c");
    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_scs_data_s");
    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_scs_data_l");
    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACTIVATE_ITEM) + "_SCRIPT", "vg_s1_scs_spbook", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_scs_manamx", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_DEATH) + "_SCRIPT", "vg_s1_scs_death", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_EQUIP_ITEM) + "_SCRIPT", "vg_s1_scs_equip", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_UNEQUIP_ITEM) + "_SCRIPT", "vg_s1_scs_unequi", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_LEVEL_UP) + "_SCRIPT", "vg_s1_scs_level", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_scs_regene", -2);
/*
    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 1",
        -2
    );

    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 2",
        -2
    );
*/

    //  Typy VK
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2276), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2277), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2278), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2279), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2280), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2281), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2282), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", GetStringByStrRef(2283), -2);

    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_A", 1);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_C", 2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_D", 3);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_E", 4);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_V", 5);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_I", 6);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_N", 7);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_T", 8);

    //  Typy metamagie
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Normaln�", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Pos�len�", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Prodlou�en�", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Maximalizace", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Rychl�", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Tich�", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", "Klidn�", -2);

    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Normaln�", 1 + METAMAGIC_NONE);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Pos�len�", 1 + METAMAGIC_EMPOWER);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Prodlou�en�", 1 + METAMAGIC_EXTEND);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Maximalizace", 1 + METAMAGIC_MAXIMIZE);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Rychl�", 1 + METAMAGIC_QUICKEN);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Tich�", 1 + METAMAGIC_SILENT);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", "Klidn�", 1 + METAMAGIC_STILL);

    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "N", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "E", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "X", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "M", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "Q", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "S", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", "T", -2);

    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 0, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 1, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 2, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 4, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 8, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 16, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC", 32, -2);

    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", -1, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", FEAT_EMPOWER_SPELL, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", FEAT_EXTEND_SPELL, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", FEAT_MAXIMIZE_SPELL, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", FEAT_QUICKEN_SPELL, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", FEAT_SILENCE_SPELL, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", FEAT_STILL_SPELL, -2);

    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", -1, 1 + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", FEAT_EMPOWER_SPELL, 1 + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", FEAT_EXTEND_SPELL, 1 + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", FEAT_MAXIMIZE_SPELL, 1 + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", FEAT_QUICKEN_SPELL, 1 + METAMAGIC_QUICKEN);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", FEAT_SILENCE_SPELL, 1 + METAMAGIC_SILENT);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", FEAT_STILL_SPELL, 1 + METAMAGIC_STILL);

    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 0, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 2, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 1, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 3, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 4, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 1, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_MODIFIER", 1, -2);

    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_STILL + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_STILL + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_STILL + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_STILL + METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_MAXIMIZE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_NONE, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EMPOWER, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EXTEND, -2);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_MAXIMIZE, -2);

//    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 1, 1 + METAMAGIC_NONE);    //  conjure
//    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 2, 1 + METAMAGIC_EMPOWER);
//    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 3, 1 + METAMAGIC_EXTEND);
//    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 4, 1 + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 5, 1 + METAMAGIC_NONE);    //  cast
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 6, 1 + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 7, 1 + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 8, 1 + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 9, 1 + METAMAGIC_QUICKEN + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 10, 1 + METAMAGIC_QUICKEN + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 11, 1 + METAMAGIC_QUICKEN + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 12, 1 + METAMAGIC_QUICKEN + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 13, 1 + METAMAGIC_STILL + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 14, 1 + METAMAGIC_STILL + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 15, 1 + METAMAGIC_STILL + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 16, 1 + METAMAGIC_STILL + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 17, 1 + METAMAGIC_SILENT + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 18, 1 + METAMAGIC_SILENT + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 19, 1 + METAMAGIC_SILENT + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 20, 1 + METAMAGIC_SILENT + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 21, 1 + METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 22, 1 + METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 23, 1 + METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 24, 1 + METAMAGIC_QUICKEN + METAMAGIC_STILL + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 25, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 26, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 27, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 28, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 29, 1 + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 30, 1 + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 31, 1 + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 32, 1 + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_MAXIMIZE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 33, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_NONE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 34, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EMPOWER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 35, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_EXTEND);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 36, 1 + METAMAGIC_QUICKEN + METAMAGIC_SILENT + METAMAGIC_STILL + METAMAGIC_MAXIMIZE);

    //  Typy modu kouzleni
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CONJURE_MODE", GetStringByStrRef(16821259), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CONJURE_MODE", GetStringByStrRef(16821260), -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_CONJURE_MODE", GetStringByStrRef(16821261), -2);

    //  Typy vlastnosti
    sli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_STR", ABILITY_STRENGTH);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_DEX", ABILITY_DEXTERITY);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_CON", ABILITY_CONSTITUTION);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_INT", ABILITY_INTELLIGENCE);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_WIS", ABILITY_WISDOM);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_CHA", ABILITY_CHARISMA);

    sls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", "S�la", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", "Obratnost", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", "Odolnost", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", "Inteligence", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", "Moudrost", -2);
    sls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", "Charizma", -2);

    //  ClassToBit
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 1, CLASS_TYPE_BARD);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 2, CLASS_TYPE_CLERIC);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 4, CLASS_TYPE_DRUID);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 8, CLASS_TYPE_PALADIN);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 16, CLASS_TYPE_RANGER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 32, CLASS_TYPE_SORCERER);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", 32, CLASS_TYPE_WIZARD);

    //  Domenni kouzla
    string  s2da;
    int     nSpell, nDomain, nLevel;

    while   (nLevel <=  8)
    {
        nLevel++;
        nDomain =   0;

        while   (nDomain    <=  21) //  21 = posledni radek v Domains.2da (water)
        {
            s2da    =   Get2DAString("Domains", "Level_" + itos(nLevel), nDomain);

            if  (s2da   !=  "")
            {
                nSpell  =   stoi(s2da);

                sli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_DOMAIN", nDomain, -2);
                sli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_LEVEL", nLevel, -2);
            }

            nDomain++;
        }
    }

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
