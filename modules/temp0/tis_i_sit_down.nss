// Text Interaction System (TIS) Interaction script - SIT_DOWN

//this = TIS_ConstructInteraction();
#include "vg_s0_tis_inter"

const int ACTION_TYPE  = 113;

//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    return TRUE;
}

//////// ACTION'S BODY
void Do()
{
    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        object oSit = GetSittingCreature(this.Object);
        string sMessage;

        // sit place is busy
        if  (GetIsObjectValid(oSit))
        {
            sMessage = C_NORM + "This sitplace is " + C_INTE + "busy";
            msg(this.Player, sMessage, TRUE, FALSE);
        }

        // sit place is free
        else
        {
            GUI_StopMenu(this.Player);

            sMessage = "*Sits down*";
            sMessage = ConvertTokens(sMessage, this.Player, FALSE, TRUE, FALSE, FALSE);

            AssignCommand(this.Player, ClearAllActions());
            AssignCommand(this.Player, ActionSit(this.Object));
            AssignCommand(this.Player, SpeakString(sMessage));
        }
    }

    // delayed sequence
    else
    {
        // ......

        // last sequence
        if (TRUE)
            this.DelaySequence = 0.0;
    }
}

/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

