// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Persistent data save script                   |
// | File    || vg_s1_rs_data_s.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-08-2009                                                     |
// | Updated || 16-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace systemu RS
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_rs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!GetIsObjectValid(OBJECT_SELF))    return;

    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    int     nValue  =   gli(OBJECT_SELF, SYSTEM_RS_PREFIX + "_FATIGUE");
    string  sQuerry;

    sQuerry =   "INSERT INTO " +
                    "rs_fatigue " +
                "VALUES " +
                "('" +
                    itos(nID) + "','" +
                    itos(nValue) + "'," +
                    "NULL" +
                ") ON DUPLICATE KEY UPDATE " +
                    "Energy = '" + itos(nValue) + "'," +
                    "date = NULL";

    SQLExecDirect(sQuerry);
}
