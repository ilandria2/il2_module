// +---------++----------------------------------------------------------------+
// | Name    || Placeable Location Editor (PLE) System init                    |
// | File    || vg_s1_ple_init.nss                                             |
// +---------++----------------------------------------------------------------+

#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_sys_core_d"
#include "vg_s0_oes_const"
#include "vg_s0_ple_const"

void main()
{
    string sName       = SYSTEM_PLE_NAME;
    string sVersion    = SYSTEM_PLE_VERSION;
    string sAuthor     = SYSTEM_PLE_AUTHOR;
    string sDate       = SYSTEM_PLE_DATE;
    string sUpDate     = SYSTEM_PLE_UPDATE;

    sli(__PLE(), "SYSTEM" + _PLE_ + "STATE", 2);
    logentry("[" + PLE + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", PLE, -2);
    sli(__PLE(), "SYSTEM" + _PLE_ + "ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(__PLE(), "SYSTEM" + _PLE_ + "NAME", sName);
    sls(__PLE(), "SYSTEM" + _PLE_ + "PREFIX", PLE);
    sls(__PLE(), "SYSTEM" + _PLE_ + "VERSION", sVersion);
    sls(__PLE(), "SYSTEM" + _PLE_ + "AUTHOR", sAuthor);
    sls(__PLE(), "SYSTEM" + _PLE_ + "DATE", sDate);
    sls(__PLE(), "SYSTEM" + _PLE_ + "UPDATE", sUpDate);

    sls(__PLE(), PLE_ + "NCS_SYSINFO_RESREF", "vg_s1_ple_info");
//    sls(__PLE(), PLE_ + "NCS_DB_CHECK_RESREF", "vg_s1_PLE_data_c");
//    sls(__PLE(), PLE_ + "NCS_DB_SAVE_RESREF", "vg_s1_PLE_data_s");
//    sls(__PLE(), PLE_ + "NCS_DB_LOAD_RESREF", "vg_s1_PLE_data_l");
//    sli(__PLE(), PLE_ + "DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(__PLE(), PLE_ + "DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(__PLE(), PLE_ + "DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(__PLE(), PLE_ + "DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(PLE);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_*) + "_SCRIPT", "<event_script>", -2);

/*
    // <INIT_CODE>
*/
    sli(__PLE(), "SYSTEM" + _PLE_ + "STATE", TRUE);
    logentry("[" + PLE + "] init done", TRUE);
}

