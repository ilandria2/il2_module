// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Core T                           |
// | File    || vg_s0_pis_core_t.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Textova knihovna funkci systemu PIS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_pis_const"
#include    "vg_s0_fds_const"
#include    "vg_s0_rs_const"
#include    "vg_s0_ids_core_c"
#include    "vg_s0_bcs_const"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetMergedIndicator                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati textovy retezec indikujici jak postava oViewer vidi postavu oSee
//
// -----------------------------------------------------------------------------
string  PIS_GetMergedIndicator(object oViewer, object oSee);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetInventorySizeIndicator                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati text reprezentujici indikator objemu inventare u hrace oPC
//
// -----------------------------------------------------------------------------
//string  PIS_GetInventorySizeIndicator(object oPC);



/*
// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetHealthIndicator                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati effect reprezentujici indikator zdravi u bytosti oCreature
//
// -----------------------------------------------------------------------------
effect  PIS_GetHealthIndicator(object oCreature);




// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetHungerIndicator                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati effect reprezentujici indikator hladu u hrace oPC
//
// -----------------------------------------------------------------------------
effect  PIS_GetHungerIndicator(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetThirstIndicator                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati effect reprezentujici indikator zizne u hrace oPC
//
// -----------------------------------------------------------------------------
effect  PIS_GetThirstIndicator(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetFatigueIndicator                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati effect reprezentujici indikator unavy u hrace oPC
//
// -----------------------------------------------------------------------------
effect  PIS_GetFatigueIndicator(object oPC);
*/



// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetInventoryIndicator                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati text reprezentujici indikator kapacity objemu a vahy u oCreature
//
// -----------------------------------------------------------------------------
string  PIS_GetInventoryIndicator(object oCreature);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_GetHealthIndicator                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati text reprezentujici indikator zdravi/poskozeni u objektu oObject
//
// -----------------------------------------------------------------------------
string  PIS_GetHealthIndicator(object oObject);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



/*
// +---------------------------------------------------------------------------+
// |                           PIS_GetHealthIndicator                          |
// +---------------------------------------------------------------------------+



effect  PIS_GetHealthIndicator(object oCreature)
{
    int     nPerc;
    effect  eStatus;

    nPerc   =   abs(ftoi(itof(GetCurrentHitPoints(oCreature)) / GetMaxHitPoints(oCreature) * 100));
    eStatus =   (nPerc  >=  91) ?   EffectAbilityIncrease(ABILITY_WISDOM, 1)    :
                (nPerc  >=  51) ?   EffectAbilityDecrease(ABILITY_WISDOM, 1)    :
                (nPerc  >=  11) ?   EffectSavingThrowIncrease(SAVING_THROW_FORT, 1, SAVING_THROW_TYPE_NONE) :
                (nPerc  >=  1)  ?   EffectSpellFailure(1)   :   eStatus;

    return  eStatus;
}
*/



// +---------------------------------------------------------------------------+
// |                           PIS_GetHungerIndicator                          |
// +---------------------------------------------------------------------------+



effect  PIS_GetHungerIndicator(object oPC)
{
    int     nPerc;
    effect  eStatus;

    nPerc   =   100 - abs(ftoi(itof(gli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER")) / FDS_DEADLY_EDGE_HUNGER * 100));
    eStatus =   (nPerc  >=  91) ?   EffectAbilityIncrease(ABILITY_CHARISMA, 1)  :
                (nPerc  >=  51) ?   EffectAbilityDecrease(ABILITY_CHARISMA, 1)  :
                (nPerc  >=  11) ?   EffectSavingThrowIncrease(SAVING_THROW_WILL, 1, SAVING_THROW_TYPE_NONE) :
                (nPerc  >=  1)  ?   EffectSavingThrowDecrease(SAVING_THROW_ALL, 1, SAVING_THROW_TYPE_NONE) :   eStatus;

    return  eStatus;
}



// +---------------------------------------------------------------------------+
// |                           PIS_GetThirstIndicator                          |
// +---------------------------------------------------------------------------+



effect  PIS_GetThirstIndicator(object oPC)
{
    int     nPerc;
    effect  eStatus;

    nPerc   =   100 - abs(ftoi(itof(gli(oPC, SYSTEM_FDS_PREFIX + "_THIRST")) / FDS_DEADLY_EDGE_THIRST * 100));
    eStatus =   (nPerc  >=  91) ?   EffectAbilityIncrease(ABILITY_INTELLIGENCE, 1)  :
                (nPerc  >=  51) ?   EffectAbilityDecrease(ABILITY_INTELLIGENCE, 1)  :
                (nPerc  >=  11) ?   EffectSavingThrowIncrease(SAVING_THROW_REFLEX, 1, SAVING_THROW_TYPE_NONE)   :
                (nPerc  >=  1)  ?   EffectSpellLevelAbsorption(1)   :   eStatus;

    return  eStatus;
}



// +---------------------------------------------------------------------------+
// |                           PIS_GetFatigueIndicator                         |
// +---------------------------------------------------------------------------+



effect  PIS_GetFatigueIndicator(object oPC)
{
    int     nPerc;
    effect  eStatus;

    nPerc   =   100 - abs(ftoi(itof(gli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE")) / RS_FATIGUE_EDGE_FSLEEP * 100));
    eStatus =   (nPerc  >=  91) ?   EffectAbilityIncrease(ABILITY_DEXTERITY, 1) :
                (nPerc  >=  51) ?   EffectAbilityDecrease(ABILITY_DEXTERITY, 1) :
                (nPerc  >=  11) ?   EffectSkillIncrease(0, 1)   :
                (nPerc  >=  1)  ?   EffectSkillDecrease(0, 1)   :   eStatus;

    return  eStatus;
}



/*
// +---------------------------------------------------------------------------+
// |                           PIS_GetWeightIndicator                          |
// +---------------------------------------------------------------------------+



effect  PIS_GetWeightIndicator(object oCreature)
{
    int     nPerc;
    effect  eStatus;

    nPerc   =   100 - abs(ftoi(itof(GetWeight(oCreature) / 10) / (GetAbilityScore(oCreature, ABILITY_STRENGTH) * 3000) * 100));
    eStatus =   (nPerc  >=  91) ?   EffectAbilityIncrease(ABILITY_CONSTITUTION, 1)  :
                (nPerc  >=  51) ?   EffectAbilityDecrease(ABILITY_CONSTITUTION, 1)  :
                (nPerc  >=  11) ?   EffectSpellResistanceIncrease(1)    :
                (nPerc  >=  1)  ?   EffectSpellResistanceDecrease(1)    :   eStatus;

    return  eStatus;
}
*/


// +---------------------------------------------------------------------------+
// |                         PIS_GetInventoryIndicator                         |
// +---------------------------------------------------------------------------+



string  PIS_GetInventoryIndicator(object oCreature)
{
    int     nPerc, nWValue, nWMax, nCValue, nCMax;
    string  sStatus;

    nWValue =   GetWeight(oCreature) / 10 / 1000;
    nWMax   =   GetAbilityScore(oCreature, ABILITY_STRENGTH) * 3000 / 1000;
    nCValue = gli(oCreature, IDS_ + "INV_SLOTS");
    nCMax = gli(oCreature, IDS_ + "INV_SLOTS_MAX");
    sStatus = W2 + "Inventory: " + Y1 + itos(nCValue) + W2 + " of max " + Y2 + itos(nCMax) + W2 + " slots\nWeight: " + Y1 + itos(nWValue) + W2 + " of max " + Y2 + itos(nWMax) + W2 + " kg";

    return  sStatus;
}



// +---------------------------------------------------------------------------+
// |                          PIS_GetMergedIndicator                           |
// +---------------------------------------------------------------------------+



string  PIS_GetMergedIndicator(object oViewer, object oSee)
{
    int     nViewer =   gli(oViewer, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    int     nSee    =   gli(oSee, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    int     bSHide  =   gli(oSee, SYSTEM_PIS_PREFIX + "_HIDDEN");
    int     bVHide  =   gli(oViewer, SYSTEM_PIS_PREFIX + "_HIDDEN");
    string  AFK, NAME, NAME2, NAMEX, DESC, HIDDEN, SLOTS;
    string  sResult;

//    if  (GetIsDM(oViewer) || oViewer == oSee)
    if  (oViewer == oSee)
    {
        HIDDEN  =   (bVHide)    ?   W0 + "- Hidden identity -"   :   "";
        //NAME    =   C_NORM + "[" + GetPCPlayerName(oSee) + "] ";
        //NAME    +=  (GetIsDM(oViewer)) ? itos(nViewer) + " - " : "";
        //NAME    +=  itos(nViewer) + " - ";
        NAME    =  C_NORM + gls(oSee, SYSTEM_PIS_PREFIX + "_NAME");
        //NAME    +=  W1 + " (" + itos(GetAge(oSee)) + " let)";
        //HP      =   PIS_GetHealthIndicator(oSee);
        //HUNGER  =   PIS_GetHungerIndicator(oSee);
        //THIRST  =   PIS_GetThirstIndicator(oSee);
        //FATIGUE =   PIS_GetFatigueIndicator(oSee);
        //WEIGHT  =   PIS_GetWeightIndicator(oSee);
        //SLOTS   =   PIS_GetInventorySizeIndicator(oSee);
    }

    else
    {
        if  (!bSHide)
        {
            NAME2   =   gls(oSee, SYSTEM_PIS_PREFIX + "_PC_" + itos(nViewer) + "_NAME");
            NAME2   =   (NAME2  !=  "") ?   C_NORM + NAME2   :   "";
        }

        if  (!bVHide)
        {
            NAMEX   =   gls(oViewer, SYSTEM_PIS_PREFIX + "_PC_" + itos(nSee) + "_NAME");
            NAMEX   =   (NAMEX  !=  "") ?   C_NORM + "Recognizes you as: " + NAMEX   :   "";
        }
    }

    AFK     =   (gli(oSee, SYSTEM_PIS_PREFIX + "_AFK"))     ?   C_INTE + "- AFK -"          :   "";
    DESC    =   gls(oSee, SYSTEM_PIS_PREFIX + "_DESC");
    DESC    =   (DESC   !=  "") ?   ConvertTokens(DESC, oSee, FALSE, TRUE, FALSE, FALSE) : "";

    sResult +=  (AFK    != "") ? "\n" + AFK     : "";
    sResult +=  (NAME   != "") ? "\n" + NAME    : "";
    sResult +=  (NAME2  != "") ? "\n" + NAME2   : "";
    //sResult +=  (HP     != "") ? "\n" + HP      : "";

    //sResult +=  (sResult!= "") ? "\n"           : "";

    //sResult +=  (HUNGER != "") ? "\n" + HUNGER  : "";
    //sResult +=  (THIRST != "") ? "\n" + THIRST  : "";
    //sResult +=  (FATIGUE!= "") ? "\n" + FATIGUE : "";
    //sResult +=  (WEIGHT != "") ? "\n" + WEIGHT  : "";
    //sResult +=  (SLOTS  != "") ? "\n" + SLOTS   : "";

    //sResult +=  (sResult!= "") ? "\n"           : "";

    sResult +=  (HIDDEN != "") ? "\n" + HIDDEN  : "";
    sResult +=  (DESC   != "") ? "\n" + DESC    : "";
    sResult +=  (NAMEX  != "") ? "\n" + NAMEX   : "";

//    msg(
    sResult =   (sResult== "") ? "<c000> </c>"  : sResult;

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                           PIS_GetHealthIndicator                          |
// +---------------------------------------------------------------------------+



string  PIS_GetHealthIndicator(object oObject)
{
    string  sStatus;
    int     nValue, nMax;

    nValue  =   GetCurrentHitPoints(oObject);
    nMax    =   GetMaxHitPoints(oObject);
    nValue  =   ftoi(itof(nValue) / nMax * 100);
    sStatus =   Y2 + "Target's health: " + W2 + itos(nValue) + "%";

    return  sStatus;
}

