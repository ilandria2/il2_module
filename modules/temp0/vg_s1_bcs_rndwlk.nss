// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Random Walk script                    |
// | File    || vg_s1_bcs_rndwlk.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Checks for the current action - if no valid action then plays performs the
    ActionRandomWalk()
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (GetCurrentAction(OBJECT_SELF)  ==  ACTION_INVALID)
    AssignCommand(OBJECT_SELF, ActionRandomWalk());
}

