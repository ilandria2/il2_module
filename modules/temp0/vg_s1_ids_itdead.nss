#include "vg_s0_ids_core_c"

void main()
{
    object area = GetArea(OBJECT_SELF);

    if (!GetIsObjectValid(area))
    {
        logentry("[" + IDS + "] invalid area for placedItem death event");
        return;
    }

    IDS_CheckPlacedItemsInArea(GetTag(area));
}
