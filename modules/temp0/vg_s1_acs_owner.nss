// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Event exit dialog script           |
// | File    || vg_s1_acs_evexit.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Skript pro dialog "Event exit"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_acs_core_c"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    SpawnScriptDebugger();
    object  oSource =   LPD_GetSource();
    object  oPC     =   LPD_GetPC();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    string  sParam  =   gls(OBJECT_SELF, SYSTEM_LPD_PREFIX + "_SCRIPT_PARAMETER", -1, SYS_M_DELETE);
    object  oArea   =   GetArea(oPC);
    object  oStart  =   glo(oArea, SYSTEM_ACS_PREFIX + "_START");
    object  oNPC    =   glo(oStart, SYSTEM_ACS_PREFIX + "_NPC");
    int     bIndex  =   gli(oStart, SYSTEM_ACS_PREFIX + "_EVENT");
    int     bReady  =   gli(oSource, SYSTEM_ACS_PREFIX + "_EVENT_READY");
    int     bWave   =   gli(oSource, SYSTEM_ACS_PREFIX + "_WAVE_READY");
    int     bResult;

    if  (sParam ==  "EA_START")
    {
        if  (bReady)
        ACS_StartEvent(oNPC, bIndex);

        else

        if  (bWave)
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_TIME", 9999);
    }

    else

    if  (sParam ==  "EA_EXIT")
    {
        //  dokonceni eventu - odejit s odmenou
        if  (bReady ==  2)
        {
            int     n       =   1;
            object  oPlayer =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, oStart, n = 1);

            //  za x vterin portne vsechny hrace zpet k NPC
            while   (GetIsObjectValid(oPlayer))
            {
                DelayCommand(1.0f, AssignCommand(oPlayer, JumpToObject(oNPC)));

                oPlayer =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, oStart, ++n);
            }

            SetPlotFlag(oSource, FALSE);
            DestroyObject(oSource, 7.0f);

            //  dokonceni - odmeny
            int     nRecipe =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RID", n = 1);
            int     nS1, nS2, nS3, nS4, nS5, nQty;

            while   (nRecipe    >=  100)
            {
                nS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RSPEC1", n);
                nS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RSPEC2", n);
                nS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RSPEC3", n);
                nS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RSPEC4", n);
                nS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RSPEC5", n);
                nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_AMOUNT", n);

                DelayCommand(2.0f, ACS_CreateItemUsingRecipeVoid(nRecipe, nQty, li(), oPC, FALSE, nS1, nS2, nS3, nS4, nS5));

                nRecipe =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_RW_RID", ++n);
            }

            DelayCommand(2.0f, sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1));
            return;
        }

        int nPC     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_PLAYERS");
        int nMin    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_PCMIN");
        int nMax    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_PCMAX");
        int bFull;

        nPC     -=  1;
        bFull   =   (nPC    <   nMin)   ?   -1  :
                    (nPC    ==  nMax)   ?   1   :   0;
        bResult =   TRUE;

        if  (!nPC)
        {
            SetPlotFlag(oSource, FALSE);
            DestroyObject(oSource, 1.0f);
        }

        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_PLAYERS", nPC);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_FULL", bFull);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_WAVE_READY", !bFull);
        sli(oSource, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1);
        sli(oSource, SYSTEM_ACS_PREFIX + "_EVENT_FULL", bFull);
        sli(oSource, SYSTEM_ACS_PREFIX + "_WAVE_READY", !bFull);
        //ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oStart));
        AssignCommand(oPC, JumpToObject(oNPC));
        DelayCommand(2.0f, AssignCommand(oPC, SetFacingPoint(GetPosition(oNPC))));
    }

    else
    {
        if  (bReady ==  2)
        {
            AssignCommand(oSource, PlaySound("il2_event_done"));
        }
    }

    bResult =   (bResult    !=  0)  ?   TRUE    :   FALSE;

    sli(OBJECT_SELF, SYS_VAR_RETURN, bResult);
}

