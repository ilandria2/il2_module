// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Check condition script                 |
// | File    || vg_s1_scs_spchec.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 14-05-2009                                                     |
// | Updated || 14-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skontroluje zda hrac splna vsechny podminky pro specificky mod kouzleni pri
    sesilani daneho kouzla
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_c"
#include    "vg_s0_scs_core_m"
#include    "vg_s0_scs_core_o"
#include    "vg_s0_xps_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    //SpawnScriptDebugger();
    int     bCondition;

    //  Polymorphed?
    bCondition  =   !CheckEffect(EFFECT_TYPE_POLYMORPH, SYS_C_EFFECT_CREATOR_ANY, OBJECT_SELF);

    if  (!bCondition)
    {
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_POLYMORPH");
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", bCondition);
        return;
    }

    int bTarget         =   FALSE;
    int nSpellCast      =   GetSpellId();
    int nSpell, nSpellGroup, nSpellOrig;

    //if  (nSpellCast ==  SYS_SPELL_TARGET)
    {
        string  sTemp   =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS");

        nSpellCast  =   stoi(GetNthSubString(sTemp, 2));

        if  (GetNthSubString(sTemp) ==  SCS_NCS_SPELL_SUMMONING_CIRCLE)
        nSpell  =   nSpellCast + SCS_SPELL_SUMMONING_CIRCLE;
        bTarget =   TRUE;
    }

    nSpellGroup     =   (nSpellCast <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpellCast - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
    nSpellOrig      =   (nSpellCast <   SCS_FIRST_CONJURE_SPELL)    ?   nSpellCast  :   nSpellCast - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));
    nSpell          =   (!nSpell)   ?   nSpellOrig  :   nSpell;

    //  Spell known?
    string  sMaster =   Get2DAString("spells", "Master", nSpell);

    if  (sMaster    !=  "") nSpell  =   stoi(sMaster);

    bCondition  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", nSpell);

    if  (!bCondition)
    {
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_SPELL_&" + itos(nSpell) + "&");
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", bCondition);
        return;
    }

    //  -- domain-spell known?
    object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int     nSources    =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_DOMAIN_I_ROWS");
    int     nClass      =   SCS_GetSpellCastClass();

    if  (nSources   >   0
    &&  nClass      ==  CLASS_TYPE_CLERIC)
    {
        int nDSRow  =   SCS_GetDomainSpellRowUsed(nSpell, OBJECT_SELF);

        //  nedostatek urovne / chybi domena
        bCondition  =   (nDSRow ==  -1) ?   FALSE   :
                        (nDSRow >=  0)  ?   TRUE    :   FALSE;

        if  (!bCondition)
        {
            SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_DOMAIN_SPELL_&" + itos(nSpell) + "&");
            sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", bCondition);
            return;
        }
    }

    //  Permitted by powers? (sfera atd)
    ExecuteScript(SCS_NCS_PERMITTED_RESREF, OBJECT_SELF);

    bCondition  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_PERMITTED_RESULT", -1, SYS_M_DELETE);

    if  (!bCondition)
    {
        //integrovane v skripte SCS_NCS_PERMITTED_RESREF
        //SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_PERMITTED");
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", bCondition);
        return;
    }

    int nInnate         =   SCS_CalculateSpellLevel(nSpell, OBJECT_SELF);
    int nManaNeeded     =   SCS_CalculateUsedMana(OBJECT_SELF, nSpell);
    int nManaCurrent    =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA");
    int nManaMax        =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA_MAX");
    int nMode           =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONJURE_MODE");
    int nMeta           =   gli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", nSpellGroup);
    int nSchool         =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_" + Get2DAString("spells", "School", nSpell));
    int nAbilityUsed    =   gli(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY_" + Get2DAString("classes", "PrimaryAbil", nClass));
    int nAbilityScore   =   GetAbilityScore(OBJECT_SELF, nAbilityUsed);
    int bResult         =   TRUE;
    int bCircle         =   TRUE;
    int bMetaMagic      =   TRUE;
    int bMetaMagicFeat  =   TRUE;
    int nAbilityNeeded, nMetaMagic;

    //  Specialni podminky pro vyvolavaci kruhy
    if  (nSpell >   SCS_SPELL_SUMMONING_CIRCLE
    &&  nSpell  <=  (SCS_SPELL_SUMMONING_CIRCLE + 8))
    {
        nManaNeeded     =   nManaMax / SCS_MANA_COST_SC_COEFFICIENT;
        nAbilityNeeded  =   12;
    }

    else
    {
        switch  (nMode)
        {
            case    SCS_CONJURE_MODE_FAKE:
                nAbilityNeeded  =   10 + (nInnate / 2);
            break;

            case    SCS_CONJURE_MODE_LEARN:
                nAbilityNeeded  =   10 + nInnate;

                //  In summoning circle?
                bCircle         =   SCS_GetIsInSummoningCircle(OBJECT_SELF, nSchool);
                bResult         =   bCircle;
            break;

            default:
                nAbilityNeeded  =   10 + nInnate;
                nMetaMagic      =   (nMeta & METAMAGIC_EMPOWER)     ?   METAMAGIC_EMPOWER   :
                                    (nMeta & METAMAGIC_EXTEND)      ?   METAMAGIC_EXTEND    :
                                    (nMeta & METAMAGIC_MAXIMIZE)    ?   METAMAGIC_MAXIMIZE  :   METAMAGIC_NONE;
                bMetaMagic      =   SCS_GetSpellHasMetamagicType(nMetaMagic, nSpell, TRUE);

                //  Has metamagic feat?
                bMetaMagicFeat  =   GetHasFeat(gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", 1 + nMetaMagic), OBJECT_SELF);

                //  Metamagic possible?
                bMetaMagicFeat  =   (nMetaMagic ==  METAMAGIC_NONE) ?   TRUE    :   bMetaMagicFeat;
                bResult         =   bMetaMagic && bMetaMagicFeat;
            break;
        }
    }
    //  Enough mana?
    int bMana       =   ci(nManaCurrent, nManaNeeded, SYS_M_HIGHER_OR_EQUAL);

    //  Enough ability score?
    int bAbility    =   ci(nAbilityScore, nAbilityNeeded, SYS_M_HIGHER_OR_EQUAL);

    if  (!bMana)            {SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_MANA_&" + itos(nManaCurrent) + "&_&" + itos(nManaMax) + "&_&" + itos(nManaNeeded) + "&"); bResult = FALSE;}
    if  (!bAbility)         {SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_ABILITY_&" + itos(nAbilityUsed) + "&_&" + itos(nAbilityScore) + "&_&" + itos(nAbilityNeeded) + "&"); bResult = FALSE;}
    if  (!bCircle)          SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_CIRCLE_&" + itos(nSchool) + "&");  else
    if  (!bMetaMagicFeat)   SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_METAMAGIC_FEAT_&" + itos(nMetaMagic) + "&");   else
    if  (!bMetaMagic)       SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_CHECK_FAILED_METAMAGIC_&" + itos(nMetaMagic) + "&");

    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", bResult);
    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT_MANA", bMana);

    if  (!bTarget)
    {
        int     nCurrent    =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nSchool) + "_LEVEL");
        int     nTarget     =   (!nInnate)  ?   0   :   (nInnate * 5);
        int     nFailure    =   SCS_CalculateSpellFailure(nCurrent, nTarget);
        int     nDice       =   1 + Random(100);

        switch  (nMode)
        {
            case    SCS_CONJURE_MODE_FAKE:
                nFailure    =   nFailure / 3;
            break;

            case    SCS_CONJURE_MODE_LEARN:
                nFailure    =   nFailure / 2;
            break;
        }

        bResult =   (nDice  >   nFailure)   ?   bResult :   2;

        if  (bResult    !=  2)
        {
            switch  (nClass)
            {
            case    CLASS_TYPE_SORCERER:
            case    CLASS_TYPE_WIZARD:
            case    CLASS_TYPE_BARD:
                if  (d100() <=  GetArcaneSpellFailure(OBJECT_SELF))
                {
                    bResult =   3;
                }
            break;
            }
        }

        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", bResult);
    }
}
