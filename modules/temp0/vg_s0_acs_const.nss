// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Constants                          |
// | File    || vg_s0_acs_const.nss                                            |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu ACS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_ACS_NAME                     =   "Advanced Craft System";
const string    SYSTEM_ACS_PREFIX                   =   "ACS";
const string    SYSTEM_ACS_VERSION                  =   "1.0";
const string    SYSTEM_ACS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_ACS_DATE                     =   "29-03-2012";
const string    SYSTEM_ACS_UPDATE                   =   "29-03-2012";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    ACS_TAG_DIALOG_COMMON               =   "VG_P_S1_COMMON01";
const string    ACS_TAG_DIALOG_EVENT_OWNER          =   "VG_P_S1_ACS_EVEX";
const string    ACS_TAG_WAYPOINT_SPAWN_NPC          =   "VG_W_SY_ACSSPAWN";
const string    ACS_TAG_WAYPOINT_SPAWN_PC           =   "VG_W_SY_ACSSTART";
const string    ACS_TAG_WAYPOINT_SPAWN_OWNER        =   "VG_W_SY_ACSSPNPC";

const int       ACS_RECIPE_MONEY                    =   187;

const int       ACS_CURRENCY_COIN_TOTAL             =   0;
const int       ACS_CURRENCY_COIN_COPPER            =   1;
const int       ACS_CURRENCY_COIN_SILVER            =   2;
const int       ACS_CURRENCY_COIN_GOLD              =   3;
const int       ACS_CURRENCY_COIN_4                 =   4;
const int       ACS_CURRENCY_COIN_5                 =   5;

const int       ACS_STORE_MODE_BUY                  =   0;
const int       ACS_STORE_MODE_SELL                 =   1;

const int       ACS_MAXIMUM_STACK_MODIFIER          =   999999;

const int       ACS_MODIFIER_WEIGHT                 =   0;
const int       ACS_MODIFIER_DURABILITY             =   1;
const int       ACS_MODIFIER_COST                   =   2;

