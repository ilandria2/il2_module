// +---------++----------------------------------------------------------------+
// | Name    || Placeable Location Editor (PLE) Core                           |
// | File    || vg_s0_ple_core                                                 |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 2018-02-14                                                     |
// | Updated || 2018-02-14                                                     |
// +---------++----------------------------------------------------------------+

#include "vg_s0_ple_const"
#include "vg_s0_sys_core_c"
#include "vg_s0_ids_core_c"
#include "nwnx_funcsext"

// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+


// gets the PLE_LOOP flag on player to state value
int PLE_GetLoopFlag(object player);

// sets the PLE_LOOP flag on player to state value
void PLE_SetLoopFlag(object player, int state);

// Starts the editor loop sequence for player
void PLE_StartLoop(object player, object target);

// gets the PLE_TARGET object reference for player
object PLE_GetTarget(object player);

// sets the PLE_TARGET object reference for player to target
void PLE_SetTarget(object player, object target);

// calculates angle [degrees] between loc_pc and loc_target
float PLE_CalculateOrientation(location loc_pc, location loc_target);

// sets adjust mode for player to mode
void PLE_SetAdjustMode(object player, int mode = PLE_MODE_TERRAIN);

// gets PLE_MODE_* value indicating current adjust mode of player
int PLE_GetAdjustMode(object player);

// switches the next adjust mode for player and returns its id
int PLE_SwitchNextMode(object player);

// handles string command
void PLE_HandleChatCommand(object player, string command);

// relocates target object by player to a new locNew location
object PLE_RelocateObject(object player, object target, location locNew);


// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+


object PLE_RelocateObject(object player, object target, location locNew)
{
    if (!GetIsObjectValid(player) ||
        !GetIsObjectValid(target) ||
        !GetIsObjectValid(GetAreaFromLocation(locNew)) ||
        !PLE_GetLoopFlag(player))
    {
        logentry("[" + PLE + "] Unable to relocate target - invalid state.");
        return OBJECT_INVALID;
    }


    // duplicate placeable
    string plcResRef = GetResRef(target);
    object plcTarget = CreateObject(OBJECT_TYPE_PLACEABLE, plcResRef, locNew, FALSE, GetTag(target));

    if (!GetIsObjectValid(plcTarget))
    {
        msg(player, R1 + "ERROR: Unable to duplicate plc!");
        logentry("[" + PLE + "] Unable to duplicate plc by resref: " + plcResRef);
        return OBJECT_INVALID;
    }

    // adjust health
    SetHardness(GetHardness(target), plcTarget);
    int damage = GetMaxHitPoints(target) - GetCurrentHitPoints(target);
    if (damage > 0)
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(damage, DAMAGE_TYPE_MAGICAL, DAMAGE_POWER_ENERGY), plcTarget);

    // copy vars
    CopyVariables(target, plcTarget);

    // set name, description and other visual properties
    SetName(plcTarget, GetName(target));
    SetDescription(plcTarget, GetDescription(target));
    SetDescription(plcTarget, GetDescription(target, FALSE, FALSE), FALSE);
    PLE_SetTarget(player, plcTarget);
    IDS_SetPlacedItemState(plcTarget, IDS_PLACED_ITEM_STATE_CHANGED, IDS_GetPlacedItemList(GetTag(GetArea(plcTarget))));
    Destroy(target, 0.1);

    return plcTarget;
}


void PLE_HandleChatCommand(object player, string command)
{
    command = upper(command);
    object target = PLE_GetTarget(player);
    vector posTarget = GetPosition(target);
    vector posTargetPlayerZ = Vector(posTarget.x, posTarget.y, GetPosition(player).z);
    location locNew;
    float number = 0.0f;

    // pure number = set Z axis
    if (TestStringAgainstPattern("*n", command))
    {
        number = stof(command) / 100;
        locNew = Location(GetArea(player), Vector(posTarget.x, posTarget.y, posTargetPlayerZ.z + number), GetFacing(target));
    }

    // unary plus/minus with number = offset Z axis relative to current
    else if (TestStringAgainstPattern("-*n|+*n", command))
    {
        number = stof(command) / 100;
        locNew = Location(GetArea(player), Vector(posTarget.x, posTarget.y, posTarget.z + number), GetFacing(target));
    }

    // number ending with O = orientation
    else if (right(command, 1) == "A")
    {
        number = stof(left(command, len(command)-1));
        locNew = Location(GetArea(player), posTarget, number);
        DelayCommand(0.5, SYS_Info(player, PLE, "TARGET_ORIENT"));
    }
    else
        SYS_Info(player, PLE, "INVALID_CMD");

    location locTargetPlayerZ = Location(GetArea(player), posTargetPlayerZ, 0.0f);
    if (GetDistanceBetweenLocations(locTargetPlayerZ, locNew) > PLE_INTERACT_DISTANCE)
    {
        SYS_Info(player, PLE, "MAX_DIST");
        return;
    }

    PLE_RelocateObject(player, target, locNew);
}


int PLE_SwitchNextMode(object player)
{
    int mode = PLE_GetAdjustMode(player);
    int newMode = -1;
    switch (mode)
    {
        case PLE_MODE_TERRAIN:
            newMode = PLE_MODE_SIDEWAYS;
            break;

        case PLE_MODE_SIDEWAYS:
            newMode = PLE_MODE_ORIENTATION;
            break;

        case PLE_MODE_ORIENTATION:
            newMode = PLE_MODE_TERRAIN;
            break;
    }

    PLE_SetAdjustMode(player, newMode);
    return newMode;
}

void PLE_SetAdjustMode(object player, int mode = PLE_MODE_TERRAIN)
{
    sli(player, PLE_ + "MODE", mode);
    SetTargetParameters(player, PLE_TARGET_SCRIPT);
    SYS_Info(player, PLE, "MODE");
}

int PLE_GetAdjustMode(object player)
{
    return gli(player, PLE_ + "MODE");
}

float PLE_CalculateOrientation(location loc_pc, location loc_target)
{
    float orient = 0.0;
    vector pos = GetPositionFromLocation(loc_pc);
    vector posTarget = GetPositionFromLocation(loc_target);
    float distTarget = GetDistanceBetweenLocations(loc_pc, loc_target);
    float baseOrient =
        posTarget.x>=pos.x && posTarget.y>=pos.y ? 0.0 :
        posTarget.x>=pos.x && posTarget.y<pos.y ? 270.0 :
        posTarget.x<pos.x && posTarget.y<pos.y ? 180.0 : 90.0;
    orient = asin(fabs(posTarget.x - pos.x)/distTarget);
    orient = (baseOrient == 0.0 || baseOrient == 180.0) ? 90.0 - orient : orient;
    orient = baseOrient + orient;
        /*
        msg(oPC,
            C2 + "----------------------\n" +
            "\nposPC: X=" + ftos(pos.x) +
            "\nposPC: Y=" + ftos(pos.y) +
            "\nposTG: X=" + ftos(posTarget.x) +
            "\nposTG: Y=" + ftos(posTarget.y) +
            "\ndistTG: " + ftos(distTarget) +
            "\nbase=" + itos(ftoi(baseOrient)) +
            "\norientCur: " + ftos(GetFacing(oPC)) +
            "\norientNew: " + ftos(orient));
        */
    return orient;
}


object PLE_GetTarget(object player)
{
    return glo(player, PLE_ + "TARGET");
}


void PLE_SetTarget(object player, object target)
{
    slo(player, PLE_ + "TARGET", target);
}


int PLE_GetLoopFlag(object player)
{
    return gli(player, PLE_ + "LOOP");
}


void PLE_SetLoopFlag(object player, int state)
{
    sli(player, PLE_ + "LOOP", state);
}


int PLE_ValidateSequenceState(object player, object target)
{
    if (!GetIsObjectValid(player)) return FALSE;
    if (!PLE_GetLoopFlag(player)) return FALSE;

    int currAct = GetCurrentAction(player);
    if (GetCurrentHitPoints(player) < 1 ||
        (currAct != ACTION_INVALID && currAct != ACTION_USEOBJECT) ||
        GetDistanceBetweenLocations(GetLocation(player), gll(player, PLE_ + "LASTLOC")) != 0.0)
    {
        return FALSE;
    }

    if (!GetIsObjectValid(target))
        return FALSE;
    return TRUE;
}


void PLE_StartLoopSequence(object player)
{
    object target = PLE_GetTarget(player);
    if (!PLE_ValidateSequenceState(player, target))
    {
        PLE_SetTarget(player, OBJECT_INVALID);
        PLE_SetLoopFlag(player, FALSE);
        SYS_Info(player, PLE, "LOOP_END");
        return;
    }

    vector posTarget = GetPosition(target);
    vector posTargetPlayerZ = Vector(posTarget.x, posTarget.y, GetPosition(player).z);
    vector posOrient = Vector(posTarget.x, posTarget.y, posTarget.z + gli(player, "offset"));
    vector posOrientPoint = GetAwayVector(Vector(posOrient.x, posOrient.y, posOrient.z + 0.01), itof(PLE_VFX_AXIS_O_PIXELS - 20 /*margin*/) / 200, GetFacing(target));
    AreaVisualEffectForPC(player, PLE_VFX_AXIS_Z, posTargetPlayerZ);
    AreaVisualEffectForPC(player, PLE_VFX_AXIS_O, posOrient);
    AreaVisualEffectForPC(player, PLE_VFX_AXIS_OP, posOrientPoint);

    // for local testing (the above plugin function doesn't work in windows nwnx2)
    if (gli(GetModule(), "DEBUG"))
    {
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(PLE_VFX_AXIS_Z), Location(GetArea(player), posTargetPlayerZ, 0.0f));
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(PLE_VFX_AXIS_O), Location(GetArea(player), posOrient, 0.0f));
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(PLE_VFX_AXIS_OP), Location(GetArea(player), posOrientPoint, 0.0f));
    }

    sll(player, PLE_ + "LASTLOC", GetLocation(player));
    SetTargetParameters(player, PLE_TARGET_SCRIPT);
    DelayCommand(PLE_LOOP_SEQUENCE_DELAY, PLE_StartLoopSequence(player));
}


void PLE_StartLoop(object player, object target)
{
    if (PLE_GetLoopFlag(player))
        return;

    SYS_Info(player, PLE, "LOOP_START");
    PLE_SetTarget(player, target);
    PLE_SetLoopFlag(player, TRUE);
    SYS_Info(player, PLE, "TARGET_ORIENT");
    PLE_SetAdjustMode(player, PLE_MODE_TERRAIN);
    AssignCommand(player, ClearAllActions());
    sll(player, PLE_ + "LASTLOC", GetLocation(player));
    PLE_StartLoopSequence(player);
}

