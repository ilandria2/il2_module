// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_areas.nss                                                  |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-12-2009                                                     |
// | Updated || 05-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "AREAINFO"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



void    msgex(object oPC, string sMessage)
{
    location    loc =   GetLocation(oPC);
    vector      vec =   GetPositionFromLocation(loc);
    object  oExam   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

    SetDescription(oExam, sMessage);
    AssignCommand(oPC, ClearAllActions(TRUE));
    AssignCommand(oPC, ActionExamine(oExam));
    DelayCommand(2.0f, DestroyObject(oExam));
}



int GetNumPC(object oArea)
{
    object  oPC =   GetFirstPC();
    int     nResult;

    while   (GetIsObjectValid(oPC))
    {
        if  (GetArea(oPC)   ==  oArea
        ||  GetModule()     ==  oArea)
        nResult++;

        oPC =   GetNextPC();
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "AREAS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nAreaID =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oSYS    =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    string  sMessage;

    //  neplatne ID
    if  (nAreaID    <   -2)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid ID";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  seznam oblasti
    if  (nAreaID    <   0)
    {
        int nID, nPlayers;
        string  sArea, sColor, sCharColor, sActiveArea, sAllArea;
        //string  s2da    =   GetStringUpperCase(Get2DAString("ccs_areainfo", "AREATAG", nID));
        object  oArea   =   glo(oSYS, SYSTEM_SYS_PREFIX + "_AREA", ++nID);

        //while   (s2da   !=  "")
        while   (GetIsObjectValid(oArea))
        {
            //oArea       =   GetObjectByTag(s2da);
            sArea       =   (GetIsObjectValid(oArea))   ?   Y2 + GetTag(oArea) + " - " + GetName(oArea)  :   R1 + "<Invalid TAG>";
            nPlayers    =   GetNumPC(oArea);
            sColor      =   (nPlayers   >   0)  ?   G1  :   W1;
            sCharColor  =   (nPlayers   >   0)  ?   W2  :   W1;
            sMessage    =   "\n" + sCharColor + "A" + itos(nID) + " - " + sColor + sArea + sCharColor;
            sMessage    +=  (!nPlayers) ?   ""  :   " - " + sColor + itos(nPlayers) + " player(s)";
            sActiveArea +=  (!nPlayers) ?   ""  :   sMessage;
            sAllArea    +=  sMessage;

            //s2da    =   GetStringUpperCase(Get2DAString("ccs_areainfo", "AREATAG", ++nID));
            oArea       =   glo(oSYS, SYSTEM_SYS_PREFIX + "_AREA", ++nID);
        }

        //  seznam vsech oblasti
        if  (nAreaID    ==  -1)
        {
            sMessage    =   Y1 + "( ? ) " + W2 + "List of areas:";
            sMessage    +=  "\n" + W2 + "~~~~~~~~~~~~~~~~~~~~~~";
            sMessage    +=  sAllArea + "\n";

            //msg(oPC, sMessage, FALSE, FALSE);
        }

        //  seznam jen aktivnich oblasti
        sMessage    +=  Y1 + "( ? ) " + W2 + "List of areas with players:";
        sMessage    +=  "\n" + W2 + "~~~~~~~~~~~~~~~~~~~~~~";
        sMessage    +=  sActiveArea;

        sMessage    +=  "\n\n" + W1 + "<TIP: Type " + Y2 + ";JUMP A7;t;" + W1 + " to move the target to the area #7>";

        msgex(oPC, sMessage);
    }

    //  konkretni ID
    else
    {
        //string  sTAG        =   GetStringUpperCase(Get2DAString("ccs_areainfo", "AREATAG", nAreaID));
        //object  oArea       =   GetObjectByTag(sTAG);
        object  oArea   =   glo(oSYS, SYSTEM_SYS_PREFIX + "_AREA", nAreaID);
        string  sTAG    =   GetTag(oArea);

        if  (!GetIsObjectValid(oArea))
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid area";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        string  sArea       =   GetName(oArea);
        int     nPlayers    =   GetNumPC(oArea);

        sMessage    =   Y1 + "( ? ) " + W2 + "Area information:";
        sMessage    +=  "\n" + W2 + "Name: " + G1 + sArea;
        sMessage    +=  "\n" + W2 + "TAG: " + G1 + sTAG;
        sMessage    +=  "\n" + W2 + "Players: " + G1 + itos(nPlayers);

        //msg(oPC, sMessage, FALSE, FALSE);
        msgex(oPC, sMessage);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
