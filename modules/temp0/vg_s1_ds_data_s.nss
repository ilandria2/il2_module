// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Persistent data save script                  |
// | File    || vg_s1_ds_data_s.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ds_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    object  oCorpse =   glo(OBJECT_SELF, SYSTEM_DS_PREFIX + "_CORPSE");
    string  sParam  =   gls(OBJECT_SELF, SYSTEM_DS_PREFIX + "_DB_SAVE_PARAM", -1, SYS_M_DELETE);
    string  sQuery;

    sParam  =   upper(sParam);

    int bDied   =   gli(OBJECT_SELF, SYSTEM_DS_PREFIX + "_DIED");

    if  (!GetIsDead(OBJECT_SELF)
    &&  bDied)
    {
        sParam  =   "RETURN";
    }

    if  (sParam ==  "DEAD")
    {
        int nID =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        sQuery  =   "INSERT INTO " +
                        "ds_corpses " +
                    "VALUES ('" +
                        itos(nID) + "'," +
                        "NULL" +
                    ") ON DUPLICATE KEY UPDATE date = NULL";

        SQLExecDirect(sQuery);
    }

    else

    if  (sParam ==  "RETURN")
    {
        int nID =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        sQuery  =   "DELETE FROM " +
                        "ds_corpses " +
                    "WHERE " +
                        "PlayerID = '" + itos(nID) + "'";

        SQLExecDirect(sQuery);
    }

    /*
    string  sQuerry;

    sQuerry =   "SELECT " + DS_DB_COLUMN_PLAYER_ID +
                " FROM " + DS_DB_TABLE_CORPSES +
                " WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" +  itos(nID) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        //  save data
        if  (GetIsObjectValid(oCorpse))
        {
            string  sCorpse =   APSLocationToString(GetLocation(oCorpse));

            sQuerry =   "UPDATE " + DS_DB_TABLE_CORPSES +
                        " SET " + DS_DB_COLUMN_CORPSE + " = '" + sCorpse + "'" +
                        " WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" + itos(nID) + "'";
        }

        //  delete data
        else
        {
            sQuerry =   "DELETE FROM " + DS_DB_TABLE_CORPSES +
                        " WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" + itos(nID) + "'";
        }

        SQLExecDirect(sQuerry);
    }

    else
    {
        string  sCorpse =   APSLocationToString(GetLocation(oCorpse));

        sQuerry =   "INSERT INTO " + DS_DB_TABLE_CORPSES + " " +
                    "(" +
                        DS_DB_COLUMN_PLAYER_ID + ", " +
                        DS_DB_COLUMN_CORPSE +
                    ") VALUES " +
                    "('" +
                        itos(nID) + "','" +
                        sCorpse +
                    "')";

        SQLExecDirect(sQuerry);
    }
    */
}
