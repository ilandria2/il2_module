// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Informacni skript                       |
// | File    || vg_s1_sys_info.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 11-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript pro posilani zprav systemu SYS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_colors"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");

    sTemp   =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (gli(OBJECT_SELF, "sys_info"))SpawnScriptDebugger();
    if  (sCase  ==  "MODE_SWITCH")
    {
        int     nCurVal =   stoi(GetNthSubString(sTemp, 1));
        int     nPrevID =   stoi(GetNthSubString(sTemp, 2));
        int     bSecondary= stoi(GetNthSubString(sTemp, 3));
        int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_MODE");
        string  sName   =   gls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_NAME" + (bSecondary ? "2" : ""), nID);

        bFloat = TRUE;
        sMessage = C_NORM + "You are now " + C_BOLD + sName;
    }
/*
    else

    if  (sCase  ==  "SEARCH_DIALOG")
    {
        sMessage    =   C_NORM + "Nelze prohled�vat okol� v " + C_NEGA + "aktivn�m rozhovoru.";
        bFloat      =   TRUE;
    }
*/
    else

    if  (sCase  ==  "READ_DESC")
    {
        sMessage    =   C_NEGA + "Please read the description of this ability.";
    }

    else if (sCase == "PROMPT")
    {
        string prompt = GetNthSubString(sTemp);

        if (prompt == "")
            prompt = "Respond with a chat message:";
        sMessage = C_BOLD + prompt;
        bFloat = TRUE;
    }

    else if (sCase == "ITEM_MARKED")
    {
        sMessage = C_BOLD + "Mark this item again to split it.\nMark other item to merge it";
        bFloat = TRUE;
    }

    else if (sCase == "MERGE_INCOMPAT")
    {
        sMessage = C_BOLD + "The second item is of different type than the first one.";
        bFloat = TRUE;
    }

    else if (sCase == "ITEMS_MERGED")
    {
        sMessage = C_BOLD + "The two marked items were merged together.";
        bFloat = TRUE;
    }

    else if (sCase == "ITEM_SPLIT")
    {
        sMessage = C_BOLD + "The marked item was split in two.";
        bFloat = TRUE;
    }

    else if (sCase == "EXCEEDED_MAX")
    {
        sMessage = C_BOLD + "The amount exceeds max limit. Please retry.";
        bFloat = TRUE;
    }

    else if (sCase == "EXCEEDED_MIN")
    {
        sMessage = C_BOLD + "The amount must be at least 1. Please retry.";
        bFloat = TRUE;
    }

    else

    if  (sCase  ==  "MESSAGE")
    {
        string  sMsg    =   GetNthSubString(sTemp, 1);

        sMessage    =   C_BOLD + "[" + GetName(OBJECT_SELF) + "] " + C_NORM + sMsg;
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "EQUIP_FIRST")
    {
        string  sName   =   GetNthSubString(sTemp, 1);

        sMessage    =   C_NEGA + "You need to equip the item first";
    }

    else

    if  (sCase  ==  "ITEM_UNUSEABLE")
    {
        //string  sName   =   GetNthSubString(sTemp, 1);

        sMessage    =   C_NEGA + "Your character doesn't know how to use this item";
    }

    else

    if  (sCase  ==  "ITEM_USE_TARGET")
    {
        string  sName   =   GetNthSubString(sTemp, 1);

        sMessage    =   "Select a target for " + C_BOLD + sName;
    }

    else

    if  (sCase  ==  "SPLIT")
    {
        string  sName   =   GetNthSubString(sTemp, 1);

        sMessage    =   C_NEGA + "You can equip only one item of " + sName;
    }

    else

    if  (sCase  ==  "INVALID_PLAYERID")
    {
        sMessage    =   C_NEGA + "Invalid player character - please contact the developers";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "RELOCATIION")
    {
        sMessage    =   C_NORM + "Moving to a last saved location";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "REGISTER_FAILURE")
    {
        sMessage    =   C_NEGA + "Registration failure due to the NWNx2 is inactive";
        bFloat      =   TRUE;
    }
    /*
    else

    if  (sCase  ==  "REGISTER_SUCCESS")
    {
        sMessage    =   C_POSI + "Registrace tv� postavy prob�hla �sp�n�";
        bFloat      =   TRUE;
    }*/

    else

    if  (sCase  ==  "KEY")
    {
        string  sAcc    =   GetNthSubString(sTemp, 1);
        string  sKey    =   GetNthSubString(sTemp, 2);
        string  sPass   =   GetNthSubString(sTemp, 3);

        sMessage    =   C_POSI + "Account protection [" + C_BOLD + sAcc + C_POSI + "] active";

        msg(OBJECT_SELF, sMessage, TRUE, FALSE);

        sMessage    =   C_NORM + "The following cd-key can be changed by developers [" +
                        C_BOLD + sKey + C_NORM + "] login details:\n" +
                        "Account: '" + C_BOLD + sAcc + C_NORM + "'\n" +
                        "Password: '" + C_BOLD + sPass + C_NORM + "'\n" +
                        C_NORM + "Hide this information somewhere for possible problems with your account";
    }

    else

    if  (sCase  ==  "KEY_EXISTS")
    {
        sMessage    =   C_NORM + "Login under existing account";
    }

    else

    if  (sCase  ==  "SKILLS")
    {
        int     nSystem =   stoi(GetNthSubString(sTemp, 1));
        string  sSystem =   gls(GetModule(), "SYSTEM", nSystem);
        object  oSystem =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");
        string  sName   =   gls(oSystem, "SYSTEM_" + sSystem + "_NAME");

        sMessage    =   C_NORM + "System abilities [" + C_BOLD + sSystem + " - " + sName + C_NORM + "]";
    }

    else

    if  (sCase  ==  "SKILLS_INFO")
    {
        string  sSystem =   GetNthSubString(sTemp, 1);
        object  oTarget =   glo(OBJECT_SELF, sSystem + "_SYSTEM_SKILLS_OBJECT");
        object  oSystem =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");
        string  sName   =   gls(oSystem, "SYSTEM_" + sSystem + "_NAME");

        sMessage    =   C_NORM + "System abilities [" + C_BOLD + sSystem + " - " + sName + C_NORM + "] at player [" + C_BOLD + GetPCPlayerName(oTarget) + ", " + GetName(oTarget) + C_NORM + "]:";
    }

    else

    if  (sCase  ==  "SKILLS_NOT_ACCESS")
    {
        sMessage    =   C_NEGA + "You cannot view the abilities of foreign player characters";
        bFloat      =   TRUE;
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}
