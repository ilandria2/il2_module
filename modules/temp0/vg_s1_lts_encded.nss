// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasure System (LTS) OnDeath event skript for EncCre |
// | File    || vg_s1_lts_encded                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    <DESCRIPTION>
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oCreature   =   OBJECT_SELF;
    int     bEnc        =   GetIsEncounterCreature(oCreature);

    //  pustit pouze pro bytosti spawnute ze setkani
    if  (!bEnc) return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_LTS_PREFIX + "_OBJECT");
    string  sDung   =   GetSubString(GetTag(GetArea(oCreature)), 6, 10);
    string  sSQL, sDate;
    int     nNth;

    sSQL    =   "SELECT NOW()";

    SQLExecDirect(sSQL);
    SQLFetch();

    sDate   =   SQLGetData(1);
    nNth    =   gli(oCreature, SYSTEM_LTS_PREFIX + "_C_SPAWN");

    sls(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_DEATH", sDate, nNth);

    int nTotalD =   gli(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_DEATH_S_ROWS");
    int nTotalS =   gli(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_SPAWN_S_ROWS");

    //  smrt posledni spawnute bytosti - upraveni casove metriky
    if  (nTotalD    ==  nTotalS)
    {
        int     nTotal  =   gli(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_TIME");
        string  sFirst  =   gls(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_SPAWN", 1);

        sSQL    =   "SELECT TIMESTAMPDIFF(SECOND, '" + sFirst + "', '" + sDate + "')";

        SQLExecDirect(sSQL);
        SQLFetch();

        int nTime   =   stoi(SQLGetData(1));

        sli(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_TIME", nTotal + nTime);

        int n;

        while   (n  <   nTotalS)
        {
            sls(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_SPAWN", "", ++n, SYS_M_DELETE);
            sls(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_DEATH", "", n, SYS_M_DELETE);
        }

        //AssignCommand(GetModule(), SpeakString("+ " + itos(nTime) + " seconds proggress time (total " + itos(nTotal + nTime) + ")", TALKVOLUME_SHOUT));
    }
}
