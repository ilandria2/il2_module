#include "vg_s0_ids_core_c"

void main()
{
    string param = gls(OBJECT_SELF, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);
    DelayCommand(0.5, IDS_CheckPlacedItemsInAreaCheck(GetTag(OBJECT_SELF), stoi(param)));
}
