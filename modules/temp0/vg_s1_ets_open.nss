#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_ets_const"

void main()
{
    object player = GetLastOpenedBy();
    int playerCarts = gli(player, ETS_ + "CART_O_ROWS");
    int storeCarts = gli(OBJECT_SELF, ETS_ + "CART_O_ROWS");

    if (!playerCarts && !storeCarts)
    {
        slo(player, ETS_ + "TARGET", OBJECT_SELF);
        slo(OBJECT_SELF, ETS_ + "TARGET", player);
    }

    logentry("[" + ETS + "] Player " + GetPCPlayerName(player) + " opened store " + GetTag(OBJECT_SELF));
}
