// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) Persistent data load script        |
// | File    || vg_s1_fds_data_l.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacteni perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_fds_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sQuerry;

    sQuerry =   "SELECT " +
                    "Hunger, " +
                    "Thirst " +
                "FROM " +
                    "fds_stomach " +
                "WHERE " +
                    "PlayerID = '" + itos(nID) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sli(OBJECT_SELF, SYSTEM_FDS_PREFIX + "_HUNGER", stoi(SQLGetData(1)));
        sli(OBJECT_SELF, SYSTEM_FDS_PREFIX + "_THIRST", stoi(SQLGetData(2)));
    }
}
