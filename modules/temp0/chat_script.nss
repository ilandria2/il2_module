// +---------++----------------------------------------------------------------+
// | Name    || chat_script                                                    |
// +---------++----------------------------------------------------------------+
/*
    Zablokuje tell mezi hrac<>hrac, povoli pouze hrac<>DM nebo DM<>hrac
*/
// -----------------------------------------------------------------------------



#include    "nwnx_chat"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;

    if  (GetIsDM(oPC))  return;

    string  sText   =   NWNXChat_GetMessageText();
    int     nChan   =   stoi(left(sText, 2));
    int     nTo     =   stoi(sub(sText, 2, 10));

    if  (nChan  ==  CHAT_CHANNEL_PRIVATE)
    {
        object  oRecip  =   NWNXChat_GetPCByPlayerID(nTo);

        if  (GetIsDM(oRecip))   return;

        string  sMessage;

        sMessage    =   C_NEGA + "The TELL channel can be used only for communication with DM.";
        //sMessage    +=  "\n" + C_NORM + "Pomoc� menu odpo�inku m��e� " + C_BOLD + "vyhledat hr��e" + C_NORM + ", kte�� maj� " + C_BOLD + "z�jem o RP";

        msg(oPC, sMessage, TRUE, FALSE);
        NWNXChat_SuppressMessage();
    }

    else

    if  (left(sText, 1) ==  " " ||
        right(sText, 1) ==  " ")    return;
}


