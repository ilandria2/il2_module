// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Player Skills script                    |
// | File    || vg_s3_sys_skills.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-11-2009                                                     |
// | Updated || 05-11-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Napise informace o dovednostech ciloveho hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nSpell  =   GetSpellId();

    if  (nSpell ==  SPELL_TIS_TARGET)
    {
        int     nLast       =   gli(GetModule(), "SYSTEM_S_ROWS");
        int     nCurrent    =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_SYSTEM");
        int     nNew;

        if  (nCurrent   >=  nLast)  nNew    =   1;
        else                        nNew    =   nCurrent + 1;

        sli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_SYSTEM", nNew);
        sls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "&vg_s3_sys_skills&_&" + itos(nNew) + "&");
        SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "SKILLS_&" + itos(nNew)+ "&");
    }

    else

    if  (nSpell ==  SPELL_TIS_TARGET)
    {
        object  oTarget     =   GetSpellTargetObject();

        if  (!GetIsObjectValid(oTarget)
        ||  !GetIsPC(oTarget)
        ||  GetIsDM(oTarget))   return;

        int     bAccess     =   CCS_GetHasAccess(OBJECT_SELF, "PM");

        if  (!bAccess
        &&  !GetIsDM(OBJECT_SELF)
        &&  oTarget !=  OBJECT_SELF)
        {
            SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "SKILLS_NOT_ACCESS");
            return;
        }

        string  sParameter  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS");
        int     nSystem     =   stoi(GetNthSubString(sParameter, 2));
        string  sSystem     =   gls(GetModule(), "SYSTEM", nSystem);

        slo(OBJECT_SELF, sSystem + "_SYSTEM_SKILLS_OBJECT", oTarget);
        SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "SKILLS_INFO_&" + sSystem + "&");
        SYS_Info(OBJECT_SELF, sSystem, "SYSTEM_SKILLS");
    }
}
