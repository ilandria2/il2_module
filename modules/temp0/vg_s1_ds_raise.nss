// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Ressurect corpse script                      |
// | File    || vg_s1_ds_raise.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 01-07-2008                                                     |
// | Updated || 30-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery ozivi mrtvolu hrace a prenese hrace ze sfery smrti do sveta
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oTarget =   OBJECT_SELF;
    string  sTag    =   GetTag(oTarget);

    if  (sTag   ==  DS_TAG_DEATH_CORPSE)
    {
        object  oPC =   glo(oTarget, SYSTEM_DS_PREFIX + "_CORPSE");

        int nAnimation  =   (Random(2)) ?   ANIMATION_LOOPING_DEAD_FRONT    :   ANIMATION_LOOPING_DEAD_BACK;

        DS_ReturnFromSphere(oPC, TRUE);
        DelayCommand(0.5f, AssignCommand(oPC, SetFacing(GetFacing(oTarget))));
        DelayCommand(1.0f, AssignCommand(oPC, ActionPlayAnimation(nAnimation, 0.5f, 5.0f)));
        DelayCommand(2.0f, SYS_Info(oPC, SYSTEM_DS_PREFIX, "RESURRECTED"));
    }

    else
    {
        ExecuteScript("vg_s1_ds_resurre", oTarget);
    }
}
