// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) System init                    |
// | File    || vg_s1_lts_init.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 25-07-2009                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system LTS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_lts_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_LTS_PREFIX;
    string  sName       =   SYSTEM_LTS_NAME;
    string  sVersion    =   SYSTEM_LTS_VERSION;
    string  sAuthor     =   SYSTEM_LTS_AUTHOR;
    string  sDate       =   SYSTEM_LTS_DATE;
    string  sUpDate     =   SYSTEM_LTS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_lts_info");
//    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_<short>_data_c");
//    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_<short>_data_s");
//    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_<short>_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

/*    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_CLOSE) + "_SCRIPT", "vg_s1_lts_closed", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_OPEN) + "_SCRIPT", "vg_s1_lts_open", -2);
//    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_DISTURBED) + "_SCRIPT", "vg_s1_lts_modify", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_DEATH) + "_SCRIPT", "vg_s1_lts_death", -2);
//    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_DEATH) + "_SCRIPT", "vg_s1_lts_encded", -2);
//    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_SPAWN) + "_SCRIPT", "vg_s1_lts_encspw", -2);
//    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_lts_encdes", -2);

*/
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_M_ON_OPEN_STORE) + "_SCRIPT", "vg_s1_lts_stopen", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_M_ON_CLOSE_STORE) + "_SCRIPT", "vg_s1_lts_stclos", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_UNACQUIRE_ITEM) + "_SCRIPT", "vg_s1_lts_stunac", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACQUIRE_ITEM) + "_SCRIPT", "vg_s1_lts_stacqu", -2);
/*
    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 1",
        -2
    );

    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 2",
        -2
    );
*/

//    ExecuteScript("vg_s1_lts_count", OBJECT_SELF);

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
