// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) Encounter Despawn script       |
// | File    || vg_s1_lts_encdes.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    <DESCRIPTION>
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!GetIsEncounterCreature()) return;

    int nTimes  =   gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEATS");
    int nAction =   GetCurrentAction(OBJECT_SELF);

    //  kdyz nestoji nebo se nehybe, tak se vynuluje pocet HB
    if  (nAction    !=  ACTION_INVALID
    &&  nAction     !=  ACTION_MOVETOPOINT
    &&  nAction     !=  ACTION_RANDOMWALK)
    {
        sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEATS", 0);
        return;
    }

    //  po presazeni hranice 'nic-nedelani' nastane despawn
    if  (nTimes ==  LTS_DESPAWN_HEARTBEATS)
    {
        SetPlotFlag(OBJECT_SELF, FALSE);
        DestroyObject(OBJECT_SELF);
        return;
    }

    //  pripocte HB pro pristi udalosti HB
    sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEATS", ++nTimes);
}
