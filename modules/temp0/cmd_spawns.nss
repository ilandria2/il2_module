// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_spawns.nss                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SPAWNS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_tis_core_c"



void    msgex(object oPC, string sMessage)
{
    location    loc =   GetLocation(oPC);
    vector      vec =   GetPositionFromLocation(loc);
    object  oExam   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

    sMessage    =   R1 + "<The text length is limited in this window>\n\n" + sMessage;

    //msg(oPC, sMessage);
    SetDescription(oExam, sMessage);
    AssignCommand(oPC, ClearAllActions(TRUE));
    AssignCommand(oPC, ActionExamine(oExam));
    DelayCommand(2.0f, DestroyObject(oExam));
}



//  projde vsechny oblasti ve kterych ma master-spawn objekt nejake spawn-pointy
void    LoopAreasInDelay(object oPC, string sMessage = "", int n = 0, int nMaster = 100)
{
    //SpawnScriptDebugger();
    object  oMaster, oArea, oSpawn, oTIS, oSYS;
    int     nArea, nSpawn, nProg, s, nTimer, nTime, nCount, nActive, nRespawn, row;
    string  sSpawn, sGroup, sName, sResRef;

    oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");
    sGroup  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nMaster) + "_NAME");
    oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sGroup);
    oArea   =   glo(oMaster, SYSTEM_TIS_PREFIX + "_AREA", ++n);

    //  "dalsi" oblast existuje
    if  (GetIsObjectValid(oArea))
    {
        oSYS    =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
        nTime   =   gli(oSYS, SYSTEM_SYS_PREFIX + "_HBS");
        nTimer  =   gli(oMaster, SYSTEM_TIS_PREFIX + "_HBS");
        nCount  =   gli(oMaster, SYSTEM_TIS_PREFIX + "_COUNT");
        oSpawn  =   glo(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN", s = 1);
        nArea   =   gli(oArea, SYSTEM_SYS_PREFIX + "_AREA");
        nSpawn  =   0;

        //  projde vsechny spawn-point objekty v dane oblasti
        while   (GetIsObjectValid(oSpawn))
        {
            sSpawn  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP");

            //  spawn-point objekt patri k danemu master-spawn objektu
            if  (sSpawn ==  sGroup)
            {
                //  pocet spawn-point objektu v dane oblasti u daneho master-spawn objektu
                if  (GetArea(oSpawn)    ==  oArea)
                {
                    nSpawn++;
                }
            }

            oSpawn  =   glo(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN", ++s);
        }

        sMessage    +=  "\n\n" + Y1 + "A" + itos(nArea) + "-" + Y2 + GetTag(oArea) + " (" + GetName(oArea) + Y2 + ")";
        s           =   0;

        //  projde vsechny spawnute objekty pro dany master-spawn objekt
        while   (++s    <=  nCount)
        {
            nProg   =   gli(oMaster, SYSTEM_TIS_PREFIX + "_HBS", s);

            //  s-ty spawnpoint je nastaven jako "spawned"
            if  (nProg  ==  -1)
            {
                oSpawn  =   glo(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN_OBJECT", s);

                //  s-ty spawnuty objekt je spawnutej
                if  (GetIsObjectValid(oSpawn))
                {
                    //  ignoruje samotne spawn-point objekty (nastane kdyz se rozhodlo, ze se ma spawnout bytosti v jine oblasti (instant creature spawn on trigger))
                    if  (GetTag(oSpawn) !=  TIS_TAG_TIS_SPAWN_POINT)
                    {
                        //  projde pouze ty spawnute objekty, ktere se nachazi v dane oblasti
                        if  (GetArea(oSpawn)    ==  oArea)
                        {
                            nActive++;
                            row         =   gli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW");
                            sResRef     =   GetTag(oSpawn);
                            sResRef     =   (right(sResRef, 7) == "_WP_REV") ? left(sResRef, len(sResRef) - 7) : sResRef;
                            sName       =   gls(oTIS, SYSTEM_TIS_PREFIX + "_RF_" + upper(sResRef) + "_NAME");
                            //sMessage    +=  "\n" + Y2 + itos(nActive) + " - " + W2 + GetTag(oSpawn) + " (" + Y2 + sName + W2 + ")";
                            //sMessage    +=  "\n" + Y2 + itos(nActive) + " - " + W2 + lower(sResRef) + " - " + W1 + sName;
                            //sMessage    +=  "\n" + Y2 + itos(nActive) + W2 + " - " + sName; //24-08-2014 - nActive -> s, kvuli prikazu ;jump $sp7;
                            sMessage    +=  "\n" + Y2 + itos(row) + W2 + " - " + sName;

                            slo(oPC, "SP" + itos(row), oSpawn);
                        }
                    }
                }
            }
        }

        nCount  =   gli(oArea, SYSTEM_TIS_PREFIX + "_SPAWN_RESREF_S_ROWS");
        s       =   0;

        //  projde vsechny spawn-point objekty oznacene jako "instant creature spawn" v dane oblasti
        while   (++s    <=  nCount)
        {
            sResRef =   gls(oArea, SYSTEM_TIS_PREFIX + "_SPAWN_RESREF", s);
            oSpawn  =   glo(oArea, SYSTEM_TIS_PREFIX + "_SPAWN_SP", s);
            sSpawn  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP");

            //  pouze spawn-point objekty pro dany master-spawn point objekt
            if  (sSpawn ==  sGroup)
            {
                row         =   gli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW");
                sName       =   gls(oTIS, SYSTEM_TIS_PREFIX + "_RF_" + upper(sResRef) + "_NAME");
                //sMessage    +=  "\n" + Y2 + itos(++nActive) + "* - " + W2 + lower(sResRef) + " - " + W1 + sName;
                //sMessage    +=  "\n" + Y2 + itos(++nActive) + "*" + W2 + " - " + sName;   //24-08-2014 - nActive -> s, kvuli prikazu ;jump $sp7;
                sMessage    +=  "\n" + Y2 + itos(row) + "*" + W2 + " - " + sName;
                nActive++;

                slo(oPC, "SP" + itos(row), oSpawn);
            }
        }

        if  (nActive)
        sMessage    +=  "\n" + W1 + "<These objects spawned \nnear " + Y1 + itos(nSpawn) + W1 + " spawn-point objects>";

        else
        sMessage    +=  "\n" + W1 + "<No objects>";

        //  spozdeny loop
        DelayCommand(0.001f, LoopAreasInDelay(oPC, sMessage, n, nMaster));
    }

    //  oblast jiz neexistuje - zaslani zpravy
    else
    {
        sMessage    =   "\n" + Y1 + "----------------------------------------" + sMessage;
        sMessage    =   "\n" + Y1 + "Master-spawn object information " + sMessage;
        sMessage    +=  "\n\n" + W1 + "<Objects with " + Y2 + "*" + W1 + " behind the number are creatures who will instantly spawn after some player character enters the area>";
        sMessage    +=  "\n\n" + W1 + "<TIP: For moving selected creature to a specific spawned object from this list with number " + Y2 + "17" + W1 + " type this command: " + Y2 + ";JUMP o17;t;";
        sMessage    +=  "\n" + Y1 + "----------------------------------------";

        msgex(oPC, sMessage);
    }
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SPAWNS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sParam  =   CCS_GetConvertedString(sLine, 1, FALSE);
    string  sMessage;
    int     nMaster, bAll;

    sParam  =   ConvertText(upper(sParam));
    nMaster =   stoi(sParam);
    bAll    =   sParam == "ALL";

    /*

    ;spawns;                --- vypise seznam vsech spawn-master objektu (id, name, display) a ich resrefy (ALL)
    ;spawns houby;          --- vypise seznam vsech spawn-master objektu (id, name, display) a ich resrefy podle patternu **HOUBY**

    priklad:

    Seznam master-spawn objektu podle patternu **HOUBY**

    107 - P_MUSHPLNT - Houby a rostliny
      1 - vg_p_up_plants04 - Rostlina (Mlhovnik)
      2 - vg_p_up_plants07 - Rostlina (Mlhovnik mlady)

    109 - P_MUSHROMS - Houby
      1 - vg_p_up_plants04 - Rostlina (Mlhovnik)
      2 - vg_p_up_plants07 - Rostlina (Mlhovnik mlady)

    125 - C_WOLVES2 - Houby

    <Nalezeno objektu: 3>

    ;spawns a23;            --- vypise seznam vsech spawn-master objektu (id, name, display) a ich resrefy ktere zahrnaji oblast 23 (;areainfo;)
    ;spawns a;              --- vypise seznam vsech spawn-master objektu (id, name, display) a ich resrefy ktere zahrnaji aktualni oblast (;areainfo;)
    //xxxxxxxxxxxxxxxxxxxx

    */


    //        nSpawn  =   gli(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN_OBJECT_O_ROWS");     //  pocet spawnutych objektu pro oMaster



    object  oTIS, oSYS, oMaster, oSpawn;
    string  sGroup, sResRef, sName, sLabel, sMaster;
    int     n, s, nID, nTime, bMatch, nFound;

    oSYS    =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    //  textovy parametr - vypsani seznamu objektu podle patternu sParam
    if  (nMaster    <   100)
    {
        nID     =   gli(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN", ++n);

        //  projde vsechny master-spawn objekty
        while   (nID)
        {
            sGroup  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nID) + "_NAME");
            sName   =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nID) + "_DISPLAY");
            sLabel  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nID) + "_LABEL");
            oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sGroup);
            sResRef =   gls(oMaster, SYSTEM_TIS_PREFIX + "_RESREF", s = 1);
            sName   =   (sName == "") ? W1 + "<Empty>" : sName;
            sMaster =   "\n\n" + Y1 + itos(nID) + Y2 + " - " + sGroup + " - " + sName + ":";
            bMatch  =   bAll;
            bMatch  =   (!bMatch) ? TestStringAgainstPattern("**" + sParam + "**", sGroup + ";" + sLabel + ";" + sName + ";" + ConvertText(sGroup + ";" + sLabel + ";" + sName)) : bMatch;

            //  projde vsechny resrefy u kazdeho master-spawn objektu
            while   (sResRef    !=  "")
            {
                sName   =   gls(oTIS, SYSTEM_TIS_PREFIX + "_RF_" + upper(sResRef) + "_NAME");
                sName   =   (sName == "") ? W1 + "<Empty>" : sName;
                bMatch  =   (!bMatch) ? TestStringAgainstPattern("**" + sParam + "**", sResRef + ";" + sName + ";" + ConvertText(sResRef + ";" + sName)) : bMatch;
                nID     =   gli(oTIS, SYSTEM_TIS_PREFIX + "_SPRF_" + upper(sResRef) + "_ID");
                //sMaster +=  "\n" + Y2 + itos(s) + W2 + " - " + itos(nID) + "-" + sResRef + " - " + W1 + sName;
                sMaster +=  "\n  " + Y2 + itos(nID) + W1 + " - " + sResRef + W2 + " - " + sName;

                sResRef =   gls(oMaster, SYSTEM_TIS_PREFIX + "_RESREF", ++s);
            }

            sMessage    +=  (bMatch) ? sMaster : "";
            nFound      +=  (bMatch) ? 1 : 0;

            nID     =   gli(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN", ++n);
        }

        sMessage    =   "\n" + Y1 + "----------------------------------------" + sMessage;
        sMessage    =   "\n" + W2 + "List of master-spawn objects filtered by a pattern " + Y1 + "**" + sParam + "**" + W2 + ":" + sMessage;
        sMessage    +=  "\n" + Y1 + "----------------------------------------";
        sMessage    +=  "\n" + W1 + "<Found master-spawn objects: " + itos(nFound) + ">";

        msgex(oPC, sMessage);
    }

    else



    /*

    ;spawns 107;            --- vypise podrobne info o master objekte 104

    priklad:

    Informace o master spawne:

    ID:     107
    Label:  plc_mushrooms_plants
    Tech:   P_MUSHPLANT
    Nazev:  Huby a rostliny
    Pocet:  132
    Respawn: 320 vterin
    Active: 16:00 - 05:00 (Ted je 17:49)
    Active: Neustale

    Seznam objektu, ktere se spawnuji:
    1 - P - vg_p_up_plants04
    2 - P - vg_p_um_mushro02
    13 - C - vg_c_ax_wolfs_01

    <Nalezeno resrefu: 13>


    Stav spawn-pointu v oblastech:
    A4 - SLVISL_EXTN01E00 (Ostrov):
      1 - VG_P_UP_PLANTS04_WP_REV (Spawnuty)
      2 - VG_P_UP_PLANTS01_WP_REV (Spawnuty)
      3 - VG_P_UP_PLANTS01_WP_REV (Timer: 17 / 340 [s])
      4 - VG_P_UP_PLANTS04_WP_REV (Spawnuty)
      5 - VG_P_UP_MUSHRO01_WP_REV (Spawnuty)
      6 - VG_P_UP_MUSHRO03_WP_REV (Timer: 230 / 340 [s])
      7 - VG_P_UP_PLANTS04_WP_REV (Timer: 98 / 340 [s])
      8 - VG_P_UP_PLANTS01_WP_REV (Spawnuty)
      9 - VG_P_UP_MUSHRO01_WP_REV (Spawnuty)

    */


    //  ciselni parametr - detajlni info o master-spawn objekt
    if  (nMaster    >=  100)
    {
        sGroup  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nMaster) + "_NAME");

        //  master-spawn objekt s ID nMaster neexistuje
        if  (sGroup ==  "")
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid master-spawn object ID: " + R2 + itos(nMaster);

            msg(oPC, sMessage, TRUE, FALSE);
            return;
        }

        int     nCount, nTimeS, nTimeE, nTimer, bActive, nProg, nRespawn, nWait;

        oMaster     =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sGroup);
        nCount      =   gli(oMaster, SYSTEM_TIS_PREFIX + "_COUNT");
        nTimeS      =   gli(oMaster, SYSTEM_TIS_PREFIX + "_TIME_S");
        nTimeE      =   gli(oMaster, SYSTEM_TIS_PREFIX + "_TIME_E");
        nTimer      =   gli(oMaster, SYSTEM_TIS_PREFIX + "_HBS");
        sLabel      =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nMaster) + "_LABEL");
        sName       =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nMaster) + "_DISPLAY");
        sName       =   (sName == "") ? W1 + "<Empty>" : sName;
        sMaster     =   "\n\n" + Y1 + itos(nID) + W2 + " - " + sGroup + " - " + sName + ":";
        nTime       =   gli(oSYS, SYSTEM_SYS_PREFIX + "_HBS");

        sMessage    +=  "\n" + W2 + "ID: " + Y2 + itos(nMaster);
        sMessage    +=  "\n" + W2 + "Label: " + Y2 + sLabel;
        sMessage    +=  "\n" + W2 + "TechName: " + Y2 + sGroup;
        sMessage    +=  "\n" + W2 + "Name: " + Y2 + sName;
        sMessage    +=  "\n" + W2 + "Amount: " + Y2 + itos(nCount) + " spawns";
        sMessage    +=  "\n" + W2 + "Timer: " + Y2 + itos(6 * nTimer) + " seconds";
        sMessage    +=  "\n" + W2 + "Status: ";

        if  (
            ((nTimeS    <   nTimeE)   &&  ((GetTimeHour() <   nTimeS) ||  (GetTimeHour()  >   nTimeE))) ||
            ((nTimeS    >=  nTimeE)   &&  ((GetTimeHour() <   nTimeS) &&  (GetTimeHour()  >   nTimeE))))
        bActive =   FALSE;

        else
        bActive =   TRUE;

        sMessage    +=  (bActive)   ?   G2 + "Active" : R2 + "Inactive";

        sMessage    +=  "\n" + W2 + "Active in a time: " + Y2;
        sMessage    +=  (nTimeS == -1) ? "<Nonstop>" : "";
        sMessage    +=  (nTimeS != -1) ? right("00" + itos(nTimeS), 2) + ":00-" + right("00" + itos(nTimeE), 2) + ":00" : "";
        sMessage    +=  (nTimeS != -1) ? " (Now it's " + right("00" + itos(GetTimeHour()), 2) + ":" + right("00" + itos(GetTimeMinute()), 2) + ")" : "";

        sMessage    +=  "\n\n" + Y1 + "List of master-spawn object resrefs";
        sMessage    +=  "\n" + Y1 + "----------------------------------------";

        sResRef     =   gls(oMaster, SYSTEM_TIS_PREFIX + "_RESREF", s = 1);

        //  projde vsechny resrefy u kazdeho master-spawn objektu
        while   (sResRef    !=  "")
        {
            sName       =   gls(oTIS, SYSTEM_TIS_PREFIX + "_RF_" + upper(sResRef) + "_NAME");
            sName       =   (sName == "") ? W1 + "<Empty>" : sName;
            //sMessage    +=  "\n" + Y2 + itos(s) + W2 + " - " + sResRef + " - " + W1 + sName;
            sMessage    +=  "\n" + Y2 + itos(s) + W2 + " - " + sName;

            sResRef     =   gls(oMaster, SYSTEM_TIS_PREFIX + "_RESREF", ++s);
        }

        //  ukaze seznam respawnujicich-se / spawnutych objektu pouze kdyz je dany master spawn-objekt aktivni (neustale / zohledneni start/end date)
        if  (bActive)
        {
            sMessage    +=  "\n\n" + Y1 + "List of respawning objects";
            sMessage    +=  "\n" + Y1 + "----------------------------------------";
            s           =   0;

            SpawnScriptDebugger();
            //  projde vsechny aktivni objekty
            while   (++s    <=  nCount)
            {
                nProg   =   gli(oMaster, SYSTEM_TIS_PREFIX + "_HBS", s);

                //  s-ty spawnpoint je nastaven jako "respawning"
                if  (nProg  !=  -1)
                {
//msg(oPC, "Time: " + itos(nTime) + " Timer: " + itos(nTimer) + " Prog: " + itos(nProg) + " >> 6 * (Timer - (Time - Prog))");
                    nWait       =   nTimer - (nTime - nProg);

                    if  (nWait  <=  0)
                    sName       =   Y2 + "<Ready for respawn>";

                    else
                    sName       =   W2 + "<Respawn in " + Y2 + itos(6 * nWait) + " seconds" + W2 + ">";
                    sMessage    +=  "\n" + Y2 + itos(++nRespawn) + " - " + sName;
                }
            }

            sMessage    +=  "\n" + W1 + "<Found respawning objects: " + Y2 + itos(nRespawn) + W1 + ">";

            sMessage    +=  "\n\n" + Y1 + "List of areas and their spawns";
            sMessage    +=  "\n" + Y1 + "----------------------------------------";

            LoopAreasInDelay(oPC, sMessage, 0, nMaster);
        }

        else
        msgex(oPC, sMessage);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

