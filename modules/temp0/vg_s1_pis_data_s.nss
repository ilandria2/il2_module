// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Persistent Data Save script      |
// | File    || vg_s1_pis_data_s.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_pis_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!GetIsObjectValid(OBJECT_SELF))    return;

    string  sQuerry;
    string  sID     =   itos(gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID"));
    string  sPC     =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_SAVE_PARAM", -1, SYS_M_DELETE);
    string  sName   =   gls(OBJECT_SELF, SYSTEM_PIS_PREFIX + "_PC_" + sPC + "_NAME");

    sQuerry =   (sName  ==  "0")    ?
        //  smazani
        "DELETE FROM pis_names " +
        "WHERE pc1 = " + sID + " AND pc2 = " + sPC

    :
        //  vlozeni
        "INSERT INTO " +
            "pis_names " +
        "VALUES " +
        "('" +
            sID + "','" +
            sPC + "','" +
            sName + "'," +
            "DEFAULT" +
        ") ON DUPLICATE KEY UPDATE " +
            "name = '" + sName + "'," +
            "date = DEFAULT";

    SQLExecDirect(sQuerry);
}
