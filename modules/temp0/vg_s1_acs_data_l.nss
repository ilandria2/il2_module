// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Persistent data load script        |
// | File    || vg_s1_acs_data_l.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Nacteni perzistentni data receptu, zakazek, eventu
    (mimo receptu se vetsinu jedna o data souvisejici s "NPC Common dialog")
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include "vg_s0_acs_core_c"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_o"
#include    "vg_s0_acs_const"
#include    "vg_s0_tis_const"
#include    "vg_s0_fds_const"
#include    "vg_s0_lts_const"
#include "vg_s0_lpd_core_c"
#include "vg_s0_ets_const"


////////////////////////////////
//  Finish (final setup)
void    LoadDataFinish()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    //  Predpoklad spravne funkcnosti:
    //  Nasledujici nastavovane objekty budou muset jiz v tomto momentu
    //  existovat v nejake oblasti.
    //
    //  Pokud se vytvori az v prubehu hry, pak tyto promenne nebudou nastaveny
    //  ale musi byt nastaveny pokazde, co se dany objekt vytvori.
    //
    //  Napriklad pro NPC se pouziva LPD rozhovor "Common dialog", ktery na
    //  zacatku overuje, zda dane NPC bylo nastaveno - kdyz ne, pak se tak stane
    //  v inicializacnim skriptu na zacatku rozhovoru.

    //  1 - nastavi popis objektu s tagem v board_messages z db
    int     n, x, y;
    string  sID;
    string  sTag, sMsg;
    object  oObject;

    sID =   itos(gli(oSystem, SYSTEM_ACS_PREFIX + "_MSG", n = 1));

    while   (sID    !=  "0")
    {
        sTag    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_MSG_" + sID + "_TAG");
        sMsg    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_MSG_" + sID + "_MESSAGE");
        sMsg    =   ConvertTokens(sMsg, OBJECT_INVALID, TRUE);
        oObject =   GetObjectByTag(sTag, x = 0);

        if  (!gli(oObject, SYSTEM_ACS_PREFIX + "_MSG_SET"))
        {
            if  (GetDescription(oObject) !=  "")
            SetDescription(oObject, GetDescription(oObject) + "\n");
            sli(oObject, SYSTEM_ACS_PREFIX + "_MSG_SET", TRUE);
        }

        while   (GetIsObjectValid(oObject))
        {
            SetDescription(oObject, GetDescription(oObject) + "- " + sMsg + "\n");

            oObject =   GetObjectByTag(sTag, ++x);
        }

        sID =   itos(gli(oSystem, SYSTEM_ACS_PREFIX + "_MSG", ++n));
    }

    //  2 - nastavi seznam waypointu v eventech a seznam eventu pro TAGy NPC
    object  oArea;

    sID =   itos(gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT", n = 1));

    while   (sID    !=  "0")
    {
        sTag    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_AREATAG");
        oArea   =   GetObjectByTag(sTag);
        oObject =   GetFirstObjectInArea(oArea);

        sli(oArea, SYSTEM_ACS_PREFIX + "_EVENT", stoi(sID), -2);

        while   (GetIsObjectValid(oObject))
        {
            if  (GetTag(oObject)    ==  ACS_TAG_WAYPOINT_SPAWN_NPC)
            {
                slo(oArea, SYSTEM_ACS_PREFIX + "_WAYPOINT", oObject, -2);
                slo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WAYPOINT", oObject, -2);
            }

            else

            if  (GetTag(oObject)    ==  ACS_TAG_WAYPOINT_SPAWN_PC)
            {
                slo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_START", oObject);
                slo(oArea, SYSTEM_ACS_PREFIX + "_START", oObject);
            }

            else

            if  (GetTag(oObject)    ==  ACS_TAG_WAYPOINT_SPAWN_OWNER)
            {
                slo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_NPCLOC", oObject);
                slo(oArea, SYSTEM_ACS_PREFIX + "_NPCLOC", oObject);
            }

            oObject =   GetNextObjectInArea(oArea);
        }

        sTag    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_TAG");

        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_TAG_" + sTag + "_EVENT", stoi(sID));

        sID =   itos(gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT", ++n));
    }

    sli(oSystem, "SYSTEM_" + SYSTEM_ACS_PREFIX + "_STATE", TRUE);
    logentry("[" + SYSTEM_ACS_PREFIX + "] init done", TRUE);
}



////////////////////////////////////////////////
//  STEP 30 - Exchange trade store items

void LoadDataStep30Loop(int nCount = 0)
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if (SQLFetch() == SQL_SUCCESS)
    {
        int store_item_id = stoi(SQLGetData(1));
        int store_id = stoi(SQLGetData(2));

        object store = glo(__ETS(), ETS_ + "STORE", store_id);
        //SpawnScriptDebugger();

        if (!GetIsObjectValid(store))
            logentry("[" + SYSTEM_ACS_PREFIX + "] Unable to locate store object id " + itos(store_id));
        else
        {
            int item_oid = stoi(SQLGetData(3));
            string item_tag = SQLGetData(4);
            string item_tech = SQLGetData(5);
            int item_stack = stoi(SQLGetData(6));
            int item_weight = stoi(SQLGetData(7));
            int item_dbcur = stoi(SQLGetData(8));
            int item_dbmax = stoi(SQLGetData(9));
            //string item_name = SQLGetData(10);
            //string item_desc = SQLGetData(11);

            string item_resref = left(item_tag, 16);
            int item_recipe = GetTechPart(item_tech, TECH_PART_RECIPE_ID);
            int item_type = ResRefToBaseItemType(item_resref);
            int isWeighted = IsWeightBasedItemType(item_type);
            int amount = !isWeighted ? item_stack : ACS_GetItemStackSize(item_weight, item_resref);

            object item = ACS_CreateItemUsingRecipe(item_recipe, amount, li(), store, FALSE);

            if (!GetIsObjectValid(item))
                logentry("[" + SYSTEM_ACS_PREFIX + "] Unable to create store item id " + itos(store_item_id) + " [" + item_tag + "] for store id [" + itos(store_id) + "]");
            else
            {
                IDS_UpdateItem(OBJECT_INVALID, item, item_dbcur, item_dbmax, FALSE);
                SetTechPart(item, TECH_PART_OBJECT_ID, item_oid);
                sli(item, ETS_ + "STORE_ITEM", store_item_id);

                nCount++;
            }
        }
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " exchange trade store items loaded.");
        LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep30Loop(nCount));
}

void LoadDataStep30()
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading exchange trade store items...");

        string sQuery =
            "select store_item_id, store_id, item_oid, item_tag, item_tech, item_stack, item_weight, item_dbcur, item_dbmax, item_name, item_desc " +
            "from ets_store_items " +
            "where player_id is null " +
            "order by store_id, item_oid asc";

        SQLExecDirect(sQuery);
        LoadDataStep30Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep30());
}



////////////////////////////////////////////////
//  STEP 29 - Exchange trade store definitions

void LoadDataStep29Loop(int nCount = 0)
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if (SQLFetch() == SQL_SUCCESS)
    {
        int store_id = stoi(SQLGetData(1));
        string store_resref = SQLGetData(2);
        string store_name = SQLGetData(3);
        location store_loc = APSStringToLocation(SQLGetData(4));
        string store_owner = SQLGetData(5);
        object store = CreateObject(OBJECT_TYPE_STORE, store_resref, store_loc);

        sli(__ETS(), ETS_ + "STORE", store_id, -2);
        sls(__ETS(), ETS_ + "STORE_RESREF", store_resref, store_id);
        sli(__ETS(), ETS_ + "STORE_" + store_resref + "_ID", store_id);
        sls(__ETS(), ETS_ + "STORE_NAME", store_name, store_id);
        sll(__ETS(), ETS_ + "STORE_LOC", store_loc, store_id);
        sls(__ETS(), ETS_ + "STORE_OWNER", store_owner, store_id);
        slo(__ETS(), ETS_ + "STORE", store, store_id);

        sli(store, ETS_ + "STORE_ID", store_id);
        sls(store, ETS_ + "STORE_OWNER", store_owner);

        if (GetIsObjectValid(store))
            logentry("[" + SYSTEM_ACS_PREFIX + "] Creating store object id " + itos(store_id) + " [" + store_resref + "] at " + APSLocationToString(store_loc));
        else
            logentry("[" + SYSTEM_ACS_PREFIX + "] ERROR: Unable to create store object id " + itos(store_id) + " [" + store_resref + "]");

        nCount++;

        //SpawnScriptDebugger();
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " exchange trade store definitions loaded.");
        LoadDataStep30();
        return;
    }

    DelayCommand(0.001f, LoadDataStep29Loop(nCount));
}

void LoadDataStep29()
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading exchange trade store definitions...");

        string sQuery =
            "select store_id, store_resref, store_name, store_loc, store_owner " +
            "from ets_store " +
            "order by store_loc asc";

        SQLExecDirect(sQuery);
        LoadDataStep29Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep29());
}



/////////////////////////////////////////
//  STEP 28 - Conversation structures

void    LoadDataStep28Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string tag = SQLGetData(1);
        string node = SQLGetData(2);
        string child = SQLGetData(3);
        string keyword = SQLGetData(4);
        string text = SQLGetData(5);

        nCount++;

        /* conv struct example
        +------------------+------+-------+---------+-----------------------+
        | tag              | node | child | keyword | text                  |
        +------------------+------+-------+---------+-----------------------+
        | VG_C_NH_M_LEADER | S1   | E1    | NULL    | NULL                  |
        | VG_C_NH_M_LEADER | E1   | E10   | hi      | Greetings to you.     |
        +------------------+------+-------+---------+-----------------------+*/

        //SpawnScriptDebugger();
        object source = LPD_GetDialogByTag(tag);
        string type = left(node, 1);

        if (type == "S")
            sls(source, SYSTEM_LPD_PREFIX + "_START", node, -2);
        sls(source, SYSTEM_LPD_PREFIX + "_NODE", node, -2);

        sls(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_CHILD", child, -2); // list of children

        if (keyword != "")
            sls(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_KEYWORD", keyword);

        if (text != "")
            sls(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_TEXT", text);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " conversation structures loaded.");
        LoadDataStep29();
        return;
    }

    DelayCommand(0.001f, LoadDataStep28Loop(nCount));
}

void    LoadDataStep28()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading conversation structures...");

        string sQuery =
            "select tag, node, child, keyword, text " +
            "from lpd_convstruct as s " +
            "join lpd_convdef as d on d.id = s.npc " +
            "where npc is not null " +
            "order by npc asc";

        SQLExecDirect(sQuery);
        LoadDataStep28Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep28());
}



/////////////////////////////////////////
//  STEP 27 - Conversation definitions

void    LoadDataStep27Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oLPD    =   GetObjectByTag("SYSTEM_" + SYSTEM_LPD_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sTag    =   SQLGetData(3);

        nCount++;

        //SpawnScriptDebugger();
        logentry("[" + SYSTEM_ACS_PREFIX + "] initializing conversation definition for: " + sTag);
        vector vCreate = Vector(1.0f + nCount, 4.0f, 1.0f);
        location lCreate = Location(GetObjectByTag(SYS_TAG_SYSTEMS_AREA), vCreate, 0.0f);
        object source = CreateObject(OBJECT_TYPE_PLACEABLE, LPD_TAG_DIALOG_SOURCE, lCreate, FALSE, SYSTEM_LPD_PREFIX + "_SOURCE_" + sTag);

        sli(oLPD, SYSTEM_LPD_PREFIX + "_NPC", stoi(sID), -2);

        sls(oLPD, SYSTEM_LPD_PREFIX + "_TD_" + sID + "_LABEL", SQLGetData(2));
        sls(oLPD, SYSTEM_LPD_PREFIX + "_TD_" + sID + "_TAG", sTag);

        sli(oLPD, SYSTEM_LPD_PREFIX + "_TAG_" + sTag + "_TD", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " conversation definitions loaded.");
        LoadDataStep28();
        return;
    }

    DelayCommand(0.001f, LoadDataStep27Loop(nCount));
}

void    LoadDataStep27()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading conversation definitions...");

        string  sQuery  =   "select id, label, tag " +
                            "from lpd_convdef " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep27Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep27());
}



////////////////////////////////
//  STEP 26 - Treasure objects

void    LoadDataStep26Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oLTS    =   GetObjectByTag("SYSTEM_" + SYSTEM_LTS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sTag    =   SQLGetData(3);

        nCount++;

        //SpawnScriptDebugger();
        sli(oLTS, SYSTEM_LTS_PREFIX + "_TREASURE", stoi(sID), -2);

        sls(oLTS, SYSTEM_LTS_PREFIX + "_TD_" + sID + "_LABEL", SQLGetData(2));
        sls(oLTS, SYSTEM_LTS_PREFIX + "_TD_" + sID + "_TAG", sTag);
        sli(oLTS, SYSTEM_LTS_PREFIX + "_TD_" + sID + "_VALMIN", stoi(SQLGetData(4)));
        sli(oLTS, SYSTEM_LTS_PREFIX + "_TD_" + sID + "_VALMAX", stoi(SQLGetData(5)));

        sli(oLTS, SYSTEM_LTS_PREFIX + "_TAG_" + sTag + "_TD", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " treasure objects loaded.");
        LoadDataStep27();
        return;
    }

    DelayCommand(0.001f, LoadDataStep26Loop(nCount));
}

void    LoadDataStep26()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading treasure objects...");

        string  sQuery  =   "select id, label, tag, valmin, valmax " +
                            "from lts_treasure " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep26Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep26());
}



////////////////////////////////////
//  STEP 25 - Store items

void    LoadDataStep25Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sSI =   SQLGetData(1);
        string  sID =   SQLGetData(2);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_STORE_" + sID + "_SI", stoi(sSI), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RLABEL", SQLGetData(3));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RID", stoi(SQLGetData(4)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RQTY", stoi(SQLGetData(5)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RSPEC1", stoi(SQLGetData(6)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RSPEC2", stoi(SQLGetData(7)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RSPEC3", stoi(SQLGetData(8)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RSPEC4", stoi(SQLGetData(9)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_RSPEC5", stoi(SQLGetData(10)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + sSI + "_COST", stoi(SQLGetData(11)));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " store items loaded.");
        LoadDataStep26();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep25Loop(nCount));
}

void    LoadDataStep25()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading store items...");

        string  sQuery  =   "select id, xid, rlabel, rid, rqty, rspec1, rspec2, rspec3, rspec4, rspec5, cost " +
                            "from xstore_items " +
                            "where xid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep25Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep25());
}



/////////////////////////////////////////
//  STEP 24 - Store definitions

void    LoadDataStep24Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sTag    =   SQLGetData(2);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_STORE", stoi(sID), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_SD_" + sID + "_TAG", sTag);

        sli(oSystem, SYSTEM_ACS_PREFIX + "_SD_" + sTag + "_ID", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " store definitions loaded.");
        LoadDataStep25();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep24Loop(nCount));
}

void    LoadDataStep24()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading store definitions...");

        string  sQuery  =   "select id, tag " +
                            "from xstore_defs " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep24Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep24());
}



////////////////////////////////////
//  STEP 23 - Recipe units

void    LoadDataStep23Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_RUNIT", stoi(sID), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_RUNIT_" + sID + "_LABEL", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RUNIT_" + sID + "_UNIT1U", SQLGetData(3));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RUNIT_" + sID + "_UNIT1K", SQLGetData(4));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RUNIT_" + sID + "_UNIT1M", SQLGetData(5));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " recipe units loaded.");
        LoadDataStep24();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep23Loop(nCount));
}

void    LoadDataStep23()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading recipe units...");

        string  sQuery  =   "select id, label, base1u, base1k, base1m " +
                            "from acs_units " +
                            "where label is not null";

        SQLExecDirect(sQuery);
        LoadDataStep23Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep23());
}



////////////////////////////////////
//  STEP 22 - Recipe categories

void    LoadDataStep22Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_RCAT", stoi(sID), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_RCAT_" + sID + "_LABEL", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RCAT_" + sID + "_NAME", SQLGetData(3));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " recipe categories loaded.");
        LoadDataStep23();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep22Loop(nCount));
}

void    LoadDataStep22()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading recipe categories...");

        string  sQuery  =   "select id, label, name " +
                            "from acs_cats " +
                            "where name is not null";

        SQLExecDirect(sQuery);
        LoadDataStep22Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep22());
}



////////////////////////////////////
//  STEP 21 - FDS nutrition

void    LoadDataStep21Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oFDS    =   GetObjectByTag("SYSTEM_" + SYSTEM_FDS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sTag    =   SQLGetData(2);

        nCount++;

        sli(oFDS, SYSTEM_FDS_PREFIX + "_NITEM", stoi(sID), -2);

        sls(oFDS, SYSTEM_FDS_PREFIX + "_NI_" + sID + "_TAG", sTag);
        sli(oFDS, SYSTEM_FDS_PREFIX + "_NI_" + sID + "_FOOD", stoi(SQLGetData(3)));
        sli(oFDS, SYSTEM_FDS_PREFIX + "_NI_" + sID + "_LIQUID", stoi(SQLGetData(4)));

        sli(oFDS, SYSTEM_FDS_PREFIX + "_NI_" + sTag + "_NID", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " fds nutrition items loaded.");
        LoadDataStep22();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep21Loop(nCount));
}

void    LoadDataStep21()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading fds nutrition items...");

        string  sQuery  =   "select id, tag, food, liquid " +
                            "from fds_nutrition " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep21Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep21());
}



////////////////////////////////////
//  STEP 20 - Exchange store items

void    LoadDataStep20Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sXI =   SQLGetData(1);
        string  sID =   SQLGetData(2);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSTORE_" + sID + "_XSI", stoi(sXI), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RLABEL", SQLGetData(3));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RID", stoi(SQLGetData(4)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RQTY", stoi(SQLGetData(5)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RSPEC1", stoi(SQLGetData(6)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RSPEC2", stoi(SQLGetData(7)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RSPEC3", stoi(SQLGetData(8)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RSPEC4", stoi(SQLGetData(9)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + sXI + "_RSPEC5", stoi(SQLGetData(10)));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " exchange store items loaded.");
        LoadDataStep21();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep20Loop(nCount));
}

void    LoadDataStep20()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading exchange store items...");

        string  sQuery  =   "select id, xid, rlabel, rid, rqty, rspec1, rspec2, rspec3, rspec4, rspec5 " +
                            "from xchange_items " +
                            "where xid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep20Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep20());
}



/////////////////////////////////////////
//  STEP 19 - Exchange store definitions

void    LoadDataStep19Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sTag    =   SQLGetData(3);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSTORE", stoi(sID), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + sID + "_MENU", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + sID + "_TAG", sTag);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + sID + "_INFO", SQLGetData(4));

        sli(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + sTag + "_ID", stoi(sID), -2);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " exchange store definitions loaded.");
        LoadDataStep20();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep19Loop(nCount));
}

void    LoadDataStep19()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading exchange store definitions...");

        string  sQuery  =   "select id, menu, tag, info " +
                            "from xchange_defs " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep19Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep19());
}



////////////////////////////////////
//  STEP 18 - Teacher requirements

void    LoadDataStep18Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sRI =   SQLGetData(1);
        string  sID =   SQLGetData(2);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_TRID", stoi(sRI), -2);

        sls(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RLABEL", SQLGetData(3));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RID", stoi(SQLGetData(4)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RQTY", stoi(SQLGetData(5)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RSPEC1", stoi(SQLGetData(6)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RSPEC2", stoi(SQLGetData(7)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RSPEC3", stoi(SQLGetData(8)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RSPEC4", stoi(SQLGetData(9)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + sRI + "_RSPEC5", stoi(SQLGetData(10)));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " teacher requirements loaded.");
        LoadDataStep19();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep18Loop(nCount));
}

void    LoadDataStep18()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading teacher requirements...");

        string  sQuery  =   "select trid, tdid, rlabel, rid, rqty, rspec1, rspec2, rspec3, rspec4, rspec5 " +
                            "from teachers_reqs " +
                            "where tdid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep18Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep18());
}



////////////////////////////////////
//  STEP 17 - Teacher definitions

void    LoadDataStep17Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER", stoi(sID), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_LABEL", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_NPCTAG", SQLGetData(3));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_VARNAME", SQLGetData(4));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_VARVALUE", stoi(SQLGetData(5)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_MREQUEST", SQLGetData(6));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_TREQUIRE", SQLGetData(7));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sID + "_TTEACH", SQLGetData(8));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + SQLGetData(3) + "_TDID", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " teacher definitions loaded.");
        LoadDataStep18();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep17Loop(nCount));
}

void    LoadDataStep17()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading teacher definitions...");

        string  sQuery  =   "select id, label, tag, vname, vvalue, mrequest, trequire, tteach " +
                            "from teachers_defs " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep17Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep17());
}



////////////////////////////////////
//  STEP 16 - Distinct spawn list

void    LoadDataStep16Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sli(oTIS, SYSTEM_TIS_PREFIX + "_SPRF", 100 + nCount, -2);
        sls(oTIS, SYSTEM_TIS_PREFIX + "_SPRF_" + itos(100 + nCount) + "_TYPE", SQLGetData(1));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_SPRF_" + itos(100 + nCount) + "_RESREF", SQLGetData(2));
        sli(oTIS, SYSTEM_TIS_PREFIX + "_SPRF_" + upper(SQLGetData(2)) + "_ID", 100 + nCount);
        nCount++;
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " distinct spawn list entries loaded.");
        LoadDataStep17();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep16Loop(nCount));
}

void    LoadDataStep16()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading distinct spawn list...");

        string  sQuery  =   "select distinct otype, resref " +
                            "from tis_spawnlist " +
                            "where resref is not null " +
                            "order by 1 desc, 2 asc";

        SQLExecDirect(sQuery);
        LoadDataStep16Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep16());
}



////////////////////////////////////
//  STEP 15 - Spawn list

void    LoadDataStep15Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sSI     =   SQLGetData(1);
        string  sID     =   SQLGetData(2);
        string  sType   =   SQLGetData(3);
        string  sResRef =   SQLGetData(4);
        string  sSpawn  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + sID + "_NAME");
        object  oSpawn  =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sSpawn);
        object  oCreate;
        string  sName;

        nCount++;

        sls(oSpawn, SYSTEM_TIS_PREFIX + "_TYPE", sType, -2);
        sls(oSpawn, SYSTEM_TIS_PREFIX + "_RESREF", sResRef, -2);

        if  (sType  ==  "P")
        oCreate =   CreateObject(OBJECT_TYPE_PLACEABLE, sResRef, GetLocation(oTIS));

        else

        if  (sType  ==  "C")
        oCreate =   CreateObject(OBJECT_TYPE_CREATURE, sResRef, GetLocation(oTIS));
        sName   =   GetName(oCreate, TRUE);

        sls(oTIS, SYSTEM_TIS_PREFIX + "_RF_" + upper(sResRef) + "_NAME", sName);
        sls(oSpawn, SYSTEM_TIS_PREFIX + "_NAME", sName, -2);
        DestroyObject(oCreate, 0.001);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " spawn list entries loaded.");
        LoadDataStep16();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep15Loop(nCount));
}

void    LoadDataStep15()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading spawn list entries...");

        string  sQuery  =   "select id, sid, otype, resref " +
                            "from tis_spawnlist " +
                            "where resref is not null " +
                            "order by 2";

        SQLExecDirect(sQuery);
        LoadDataStep15Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep15());
}



////////////////////////////////////
//  STEP 14 - Spawn definitions

void    LoadDataStep14Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sSpawn  =   SQLGetData(4);
        object  oSpawn  =   CreateObject(OBJECT_TYPE_WAYPOINT, TIS_TAG_TIS_SPAWN_MASTER, Location(GetObjectByTag(SYS_TAG_SYSTEMS_AREA), Vector(2.0f + nCount, 0.0f, 0.0f), 0.0f), FALSE, TIS_TAG_TIS_SPAWN_MASTER + "_" + sSpawn);

        nCount++;

        sli(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN", stoi(sID), -2);
        sls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + sID + "_NAME", sSpawn);
        sls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + sID + "_LABEL", SQLGetData(2));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + sID + "_DISPLAY", SQLGetData(3));

        sli(oSpawn, SYSTEM_TIS_PREFIX + "_COUNT", stoi(SQLGetData(5)));
        sli(oSpawn, SYSTEM_TIS_PREFIX + "_HBS", stoi(SQLGetData(6)));
        sli(oSpawn, SYSTEM_TIS_PREFIX + "_TIME_S", stoi(SQLGetData(7)));
        sli(oSpawn, SYSTEM_TIS_PREFIX + "_TIME_E", stoi(SQLGetData(8)));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " spawn definitions loaded.");
        LoadDataStep15();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep14Loop(nCount));
}

void    LoadDataStep14()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading spawn definitions...");

        string  sQuery  =   "select id, label, display, name, spawn, respawn, actstart, actend " +
                            "from tis_spawndef " +
                            "where name is not null";

        SQLExecDirect(sQuery);
        LoadDataStep14Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep14());
}


///////////////////////////////////
//  STEP 13 - Interaction settings

void    slv(string sType, object oObject, string sVarName, string sValue)
{
    sType   =   lower(sType);

    if  (sType  ==  "s")    sls(oObject, sVarName, sValue);         else
    if  (sType  ==  "n")    sli(oObject, sVarName, stoi(sValue));   else
    if  (sType  ==  "f")    slf(oObject, sVarName, stof(sValue));
}


void    LoadDataStep13Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sOS     =   SQLGetData(2);
        string  sAS     =   SQLGetData(3);
        string  sPost   =   SQLGetData(4);
        string  sType   =   SQLGetData(5);
        string  sValue  =   SQLGetData(6);

        nCount++;

        //  manualni zadani nazvu promenne do POSTFIX-u (bez OS a AS)
        if  (sOS    ==  "-1")
        slv(sType, oTIS, SYSTEM_TIS_PREFIX + "_SET_" + sPost, sValue);

        else

        if  (sAS    ==  "-1")
        slv(sType, oTIS, SYSTEM_TIS_PREFIX + "_SET_OS_" + sOS + "_" + sPost, sValue);

        else
        {
            //  neprima kombinace OS + AS (seznamy)
            sli(oTIS, SYSTEM_TIS_PREFIX + "_SET_OS_" + sOS + "_AS_" + sAS + "_ID", stoi(sID), -2);
            sls(oTIS, SYSTEM_TIS_PREFIX + "_SET_" + sID + "_NAME", sPost);
            sls(oTIS, SYSTEM_TIS_PREFIX + "_SET_" + sID + "_TYPE", sType);
            sls(oTIS, SYSTEM_TIS_PREFIX + "_SET_" + sID + "_VALUE", sValue);

            //  prima kombinace OS + AS + POST
            slv(sType, oTIS, SYSTEM_TIS_PREFIX + "_SET_OS_" + sOS + "_AS_" + sAS + "_" + sPost, sValue);
        }
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " interaction settings loaded.");
        LoadDataStep14();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep13Loop(nCount));
}

void    LoadDataStep13()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading Interaction settings...");

        string  sQuery  =   "select id, osid, asid, post, typ, val " +
                            "from tis_settings " +
                            "where post is not null";

        SQLExecDirect(sQuery);
        LoadDataStep13Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep13());
}



///////////////////////////////////
//  STEP 12 - Interaction subtypes

void    LoadDataStep12Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(2);
        string  sAS =   SQLGetData(1);
        string  sAT =   SQLGetData(3);

        nCount++;

        sli(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_AS", stoi(sAS), -2);
        sli(oTIS, SYSTEM_TIS_PREFIX + "_AS_" + sAS + "_TID", stoi(sID));
        sli(oTIS, SYSTEM_TIS_PREFIX + "_AS_" + sAS + "_IID", stoi(sAT));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_AS_" + sAS + "_DISPLAY", SQLGetData(4));
        sli(oTIS, SYSTEM_TIS_PREFIX + "_AS_" + sAS + "_VFX", stoi(SQLGetData(5)));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_AS_" + sAS + "_TECH", gls(oTIS, SYSTEM_TIS_PREFIX + "_AT_" + sAT + "_TECH"));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " interaction subtypes loaded.");
        LoadDataStep13();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep12Loop(nCount));
}

void    LoadDataStep12()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading Interaction subtypes...");

        string  sQuery  =   "select id, tid, iid, display, vfx " +
                            "from tis_asubs " +
                            "where tid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep12Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep12());
}



////////////////////////////////
//  STEP 11 - Interaction types

void    LoadDataStep11Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oTIS, SYSTEM_TIS_PREFIX + "_AT", stoi(sID), -2);
        sls(oTIS, SYSTEM_TIS_PREFIX + "_AT_" + sID + "_DISPLAY", SQLGetData(2));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_AT_" + sID + "_TECH", SQLGetData(3));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " interaction types loaded.");
        LoadDataStep12();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep11Loop(nCount));
}

void    LoadDataStep11()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading Interaction types...");

        string  sQuery  =   "select id, display, tech " +
                            "from tis_atypes " +
                            "where tech is not null";

        SQLExecDirect(sQuery);
        LoadDataStep11Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep11());
}



///////////////////////////////////////////
//  STEP 10 - Interactive object subtypes

void    LoadDataStep10Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(3);
        string  sSI     =   SQLGetData(1);
        string  sTAG    =   SQLGetData(4);

        nCount++;

        //  ID -> info.*
        sli(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_OS", stoi(sSI), -2);
        sls(oTIS, SYSTEM_TIS_PREFIX + "_OS_" + sSI + "_DISPLAY", SQLGetData(2));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_OS_" + sSI + "_TAG", sTAG);

        //  info.TAG -> ID
        sli(oTIS, SYSTEM_TIS_PREFIX + "_TAG_" + sTAG + "_OT", stoi(sID));
        sli(oTIS, SYSTEM_TIS_PREFIX + "_TAG_" + sTAG + "_OS", stoi(sSI));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_TAG_" + sTAG + "_TECH", gls(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_TECH"));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " interactive object subtypes loaded.");
        LoadDataStep11();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep10Loop(nCount));
}

void    LoadDataStep10()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading Interactive object subtypes...");

        string  sQuery  =   "select id, display, tid, tag " +
                            "from tis_osubs " +
                            "where id is not null and tid is not null;";

        SQLExecDirect(sQuery);
        LoadDataStep10Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep10());
}



///////////////////////////////////////
//  STEP 9 - Interactive object types

void    LoadDataStep9Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sTECH   =   SQLGetData(3);

        nCount++;

        //  ID -> info.*
        sli(oTIS, SYSTEM_TIS_PREFIX + "_OT", stoi(sID), -2);
        sls(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_DISPLAY", SQLGetData(2));
        sli(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_VFX", stoi(SQLGetData(4)));
        sli(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_VFX_SC", stoi(SQLGetData(5)));
        sls(oTIS, SYSTEM_TIS_PREFIX + "_OT_" + sID + "_TECH", sTECH);

        //  info.TECH -> ID
        sli(oTIS, SYSTEM_TIS_PREFIX + "_TECH_" + sTECH + "_OT", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " interactive object types loaded.");
        LoadDataStep10();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep9Loop(nCount));
}

void    LoadDataStep9()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading Interactive object types...");

        string  sQuery  =   "select id, display, tech, vfx, vfx_sc " +
                            "from tis_otypes " +
                            "where tech is not null;";

        SQLExecDirect(sQuery);
        LoadDataStep9Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep9());
}



////////////////////////////////
//  STEP 8 - Event rewards

void    LoadDataStep8Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_LABEL", SQLGetData(2),-2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_RID", stoi(SQLGetData(3)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_AMOUNT", stoi(SQLGetData(4)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_RSPEC1", stoi(SQLGetData(5)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_RSPEC2", stoi(SQLGetData(6)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_RSPEC3", stoi(SQLGetData(7)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_RSPEC4", stoi(SQLGetData(8)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_RW_RSPEC5", stoi(SQLGetData(9)), -2);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " event rewards loaded.");
        LoadDataStep9();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep8Loop(nCount));
}

void    LoadDataStep8()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading event rewards...");

        string  sQuery  =   "select eid, label, rid, amount, rspec1, rspec2, rspec3, rspec4, rspec5 " +
                            "from events_rewards " +
                            "where id is not null and eid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep8Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep8());
}



////////////////////////////////
//  STEP 7 - Event waves

void    LoadDataStep7Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        int     nWave   =   stoi(SQLGetData(2));

        nCount++;

        if  (nWave > gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WAVES"))
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WAVES", nWave);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WV_" + itos(nWave) + "_LABEL", SQLGetData(3), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WV_" + itos(nWave) + "_RESREF", SQLGetData(4), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WV_" + itos(nWave) + "_AMOUNT", stoi(SQLGetData(5)), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WV_" + itos(nWave) + "_SOUND", SQLGetData(6)); // bez nRow (jen 1 zvuk za vlnu)
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_WV_" + itos(nWave) + "_TEXT", SQLGetData(7)); // bez nRow (jen 1 text za vlnu)
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " event waves loaded.");
        LoadDataStep8();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep7Loop(nCount));
}

void    LoadDataStep7()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading event waves...");

        string  sQuery  =   "select eid, wave, label, resref, amount, sound, tWave " +
                            "from events_waves " +
                            "where id is not null and eid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep7Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep7());
}



////////////////////////////////
//  STEP 6 - Event definitions

void    LoadDataStep6Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT", stoi(sID), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_LABEL", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_TAG", SQLGetData(3));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_TYPE", stoi(SQLGetData(4)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_AREATAG", SQLGetData(5));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_PAUSE", stoi(SQLGetData(6)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_LENGTH", stoi(SQLGetData(7)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_PCMIN", stoi(SQLGetData(8)));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_PCMAX", stoi(SQLGetData(9)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_T_NONE", SQLGetData(10));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_T_PROGRESS", SQLGetData(11));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_T_DETAIL", SQLGetData(12));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + sID + "_T_DONE", SQLGetData(13));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + SQLGetData(3) + "_EVENT", stoi(sID), -2);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " events loaded.");
        LoadDataStep7();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep6Loop(nCount));
}

void    LoadDataStep6()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading events...");

        string  sQuery  =   "select id, label, tag, style, area, pause, waveln, pcmin, pcmax, tNone, tProg, tDetail, tDone " +
                            "from events_defs " +
                            "where id is not null and tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep6Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep6());
}



////////////////////////////////
//  STEP 5 - Board messages

void    LoadDataStep5Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_MSG", stoi(sID), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_MSG_" + sID + "_LABEL", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_MSG_" + sID + "_TAG", SQLGetData(3));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_MSG_" + sID + "_MESSAGE", SQLGetData(4));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " board messages loaded.");
        LoadDataStep6();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep5Loop(nCount));
}

void    LoadDataStep5()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading board messages...");

        string  sQuery  =   "select id, label, tag, msg " +
                            "from board_messages " +
                            "where id is not null";

        SQLExecDirect(sQuery);
        LoadDataStep5Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep5());
}



////////////////////////////////
//  STEP 4 - Cargo items

void    LoadDataStep4Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RID", stoi(SQLGetData(2)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RQTY", stoi(SQLGetData(3)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RSPEC1", stoi(SQLGetData(4)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RSPEC2", stoi(SQLGetData(5)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RSPEC3", stoi(SQLGetData(6)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RSPEC4", stoi(SQLGetData(7)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_ITEM_RSPEC5", stoi(SQLGetData(8)), -2);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " cargo items loaded.");
        LoadDataStep5();
        //LoadDataFinish();
        return;
    }

    DelayCommand(0.001f, LoadDataStep4Loop(nCount));
}

void    LoadDataStep4()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading cargo items...");

        string  sQuery  =   "select cid, rid, rqty, rspec1, rspec2, rspec3, rspec4, rspec5 " +
                            "from cargo_items " +
                            "where cid is not null";

        SQLExecDirect(sQuery);
        LoadDataStep4Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep4());
}



////////////////////////////////
//  STEP 3 - Cargos

void    LoadDataStep3Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO", stoi(sID), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_DISPLAY", ConvertTokens(SQLGetData(2), OBJECT_INVALID, TRUE));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_TAG", SQLGetData(3));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_DURATION", SQLGetData(4));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_GREET", SQLGetData(5));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_INFO_S", SQLGetData(6));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_INFO_A", SQLGetData(7));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_CARGO_N", SQLGetData(8));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_CARGO_S", SQLGetData(9));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_CARGO_I", SQLGetData(10));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_T_CARGO_D", SQLGetData(11));
        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_COMPLETE", stoi(SQLGetData(12)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sID + "_DATE_END", SQLGetData(13));

        if  (SQLGetData(4)  ==  "") sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + SQLGetData(3) + "_WISH", stoi(sID));
        else                        sli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + SQLGetData(3) + "_CARGO", stoi(sID), -2);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " cargos loaded.");
        LoadDataStep4();
        return;
    }

    DelayCommand(0.001f, LoadDataStep3Loop(nCount));
}

void    LoadDataStep3()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading cargos...");

        string  sQuery  =   "select id, display, tag, dur, tGreet, tInfoS, tInfoA, tCargoN, tCargoS, tCargoI, tCargoD, c.Completed, c.EndDate " +
                            "from cargo_defs as d left join cargo_complete as c on d.id = c.CargoID " +
                            "where tag is not null";

        SQLExecDirect(sQuery);
        LoadDataStep3Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep3());
}



////////////////////////////////
//  STEP 2 - Recipe Meta-relations

void    LoadDataStep2Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_REL_GROUP", stoi(SQLGetData(2)), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_REL_QTY", stoi(SQLGetData(3)), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_REL_ADDRESS", SQLGetData(4), -2);
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " recipe meta-relations loaded.");
        LoadDataStep3();
        return;
    }

    DelayCommand(0.001f, LoadDataStep2Loop(nCount));
}

void    LoadDataStep2()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading recipe meta-relations...");

        string  sQuery  =   "select rid, rid_group, rid_qty, rid_address " +
                            "from acs_rels_m " +
                            "where rid is not null and rid_group != -1";

        SQLExecDirect(sQuery);
        LoadDataStep2Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep2());
}



////////////////////////////////
//  STEP 1 - Recipe Definitions (base + meta)

void    LoadDataStep1Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object oSys = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID     =   SQLGetData(1);
        string  sResRef =   SQLGetData(5);
        string  sResRefPlc =SQLGetData(6);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE", stoi(sID), -2);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_CATEGORY", SQLGetData(2));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_LABEL", SQLGetData(3));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_DISPLAY", ConvertTokens(SQLGetData(4), OBJECT_INVALID, TRUE));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_DISPLAY_NOCOLOR", SQLGetData(4));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_RESREF", sResRef);
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_RESREFPLC", sResRefPlc);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_QTY", stoi(SQLGetData(7)));
        slf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_MODWEIGHT", stof(SQLGetData(8)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_MODWEIGHT", SQLGetData(8));
        slf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_MODDURAB", stof(SQLGetData(9)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_MODDURAB", SQLGetData(9));
        slf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_MODCOST", stof(SQLGetData(10)));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_MODCOST", SQLGetData(10));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_UNIT1U", SQLGetData(11));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_UNIT1K", SQLGetData(12));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + sID + "_UNIT1M", SQLGetData(13));

        sli(oSystem, SYSTEM_ACS_PREFIX + "_RESREF_" + sResRef + "_RID", stoi(sID));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RESREF_" + sResRef + "_RESREFPLC", sResRefPlc);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_RESREFPLC_" + sResRefPlc + "_RID", stoi(sID));
        sls(oSystem, SYSTEM_ACS_PREFIX + "_RESREFPLC_" + sResRefPlc + "_RESREF", sResRef);

        if (sResRef != "")
        {
            int baseType = ResRefToBaseItemType(sResRef);
            int width = stoi(Get2DAString("baseitems", "InvSlotWidth", baseType));
            int height = stoi(Get2DAString("baseitems", "InvSlotHeight", baseType));
            string slots = Get2DAString("baseitems", "EquipableSlots", baseType);

            //sli(oSys, SYSTEM_SYS_PREFIX + "_ITRF_" + upper(sResRef) + "_ID", baseType); // ---- already set in ResRefToBaseItemType() call
            //sli(oSystem, SYSTEM_SYS_PREFIX + "_ITRF_" + upper(resRef) + "_INVBONUS", invBonus); // ---- already set in ResRefToBaseItemType() call
            sli(oSys, SYSTEM_SYS_PREFIX + "_BITM_" + itos(baseType) + "_WIDTH", width);
            sli(oSys, SYSTEM_SYS_PREFIX + "_BITM_" + itos(baseType) + "_HEIGHT", height);
            sls(oSys, SYSTEM_SYS_PREFIX + "_BITM_" + itos(baseType) + "_SLOTS", slots);
        }
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " recipe meta-definitions loaded.");
        LoadDataStep2();
        return;
    }

    DelayCommand(0.001f, LoadDataStep1Loop(nCount));
}

void    LoadDataStep1()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading recipe meta-definitions...");

        string  sQuery  =   "select d.id, c.name, d.label, d.display, d.resref, d.resrefplc, d.qty, m.modweight, m.moddurab, m.modcost, u.base1u, u.base1k, u.base1m " +
                            "from acs_defs as d left join acs_defs_m as m on d.id = m.id left join acs_cats as c on c.id = d.cid left join acs_units as u on u.id = d.uid " +
                            "where d.label is not null " +
                            "order by 2 asc, 4 asc";

        SQLExecDirect(sQuery);
        LoadDataStep1Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep1());
}




////////////////////////////////
//  STEP 0 - Recipe Groups

void    LoadDataStep0Loop(int nCount = 0)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        string  sID =   SQLGetData(1);
        string  sSI =   SQLGetData(2);

        nCount++;

        sli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + sID + "_CHILD", stoi(sSI), -2);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_CHILD_" + sSI + "_GROUP", stoi(sID));
    }

    else
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", FALSE, -1, SYS_M_DELETE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] " + itos(nCount) + " recipe groups loaded.");
        LoadDataStep1();
        return;
    }

    DelayCommand(0.001f, LoadDataStep0Loop(nCount));
}

void    LoadDataStep0()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    if  (!gli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING"))
    {
        sli(oSystem, SYSTEM_ACS_PREFIX + "_DATA_LOADING", TRUE);
        logentry("[" + SYSTEM_ACS_PREFIX + "] Loading recipe groups...");

        string  sQuery  =   "select r.rid, r.rid_src " +
                            "from acs_defs as d inner join acs_rels as r on d.id = r.rid " +
                            "where d.display is not null and d.qty is null " +
                            "order by r.rid, r.id";

        SQLExecDirect(sQuery);
        LoadDataStep0Loop();
        return;
    }

    DelayCommand(3.0f, LoadDataStep0());
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sQuerry;
    string  sParam  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_LOAD_PARAM", -1, SYS_M_DELETE);
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    //SpawnScriptDebugger();
    //  recepty, zakazky
    if  (sParam ==  "INIT")
    {
        //  recipe defs
        LoadDataStep0();
    }

    else
    {

    }
}
