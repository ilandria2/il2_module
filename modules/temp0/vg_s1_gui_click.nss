// +---------++-----------------------------------------------------------------+
// | Name    || Generic User Interface (GUI) Layout click event handler         |
// | File    || vg_s1_gui_click.nss                                             |
// +---------++-----------------------------------------------------------------+

#include "vg_s0_gui_core"

void main()
{
    object player = GetPlaceableLastClickedBy();
    object plcLayout = OBJECT_SELF;

    AssignCommand(player, ClearAllActions());

    // ignore players clicking on foreign player's gui layouts
    object owner = GUI_GetOwner(plcLayout);
    if (owner != player)
        return;

    // ignore players clicking on their GUI's after GUI Menu was already stopped
    string playerMenu = GUI_GetMenuName(player);
    if (playerMenu == "")
        return;

    // pass a reference to the layout object clicked
    GUI_SetLayoutObjectClicked(player, plcLayout);

    // execute the click handler associated to the active menu
    ExecuteScript(GUI_ + "clk_" + playerMenu, player);

    // execute the render handler associated to the active menu
    ExecuteScript(GUI_ + "rnd_" + playerMenu, player);

    // render sequence manually
    GUI_RenderMenuSequence(player, playerMenu, -1);
}

