#include "vg_s0_sys_core_t"
#include "nwnx_funcsext"

void main()
{
    vector vPos = GetPosition(OBJECT_SELF);
    vPos = Vector(vPos.x, vPos.y, vPos.z + 2.0f);

    msg(OBJECT_SELF, "Timing cancelled - applying some other effect", TRUE, FALSE);
    AssignCommand(OBJECT_SELF, PlayAnimation(0, 1.0f, 0.0f));
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEAD_EVIL), OBJECT_SELF);
}
