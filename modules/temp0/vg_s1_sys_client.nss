// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Player login script                     |
// | File    || vg_s1_sys_client.nss                                           |
// +---------++----------------------------------------------------------------+

#include "aps_include"
#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_sys_core_d"
#include "vg_s0_oes_const"

// helper functions

int CheckPlayerCdKey(object player);
int IsRegistered(int playerId);
int GetAndCheckPlayerIdAndAccount(object player);
string GetPlayerAcc(int playerId);

object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");


// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void main()
{
    object player = GetEnteringObject();
//TODO: prevent this script from executing when entering standard areas / after login

    // TEMPORARY DEATH SYSTEM SOLUTION
    if (GetIsDead(player))
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), player);

    // account protection
    if (!CheckPlayerCdKey(player))
        return;

    // init and check player id and account
    int playerId = GetAndCheckPlayerIdAndAccount(player);
    if (!playerId)
        return;

    // EXISTING player charactger
    if (IsRegistered(playerId))
    {
        sli(player, SYSTEM_SYS_PREFIX + "_REGISTERED", TRUE);
        slo(GetModule(), "USERDEFINED_EVENT_OBJECT", player);
        SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_RELOG));
    }
}



int GetAndCheckPlayerIdAndAccount(object player)
{
    int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    if (!playerId)
    {
        playerId = stoi(GetTag(player));

        // player without ID - ignore
        if (!playerId)
        {
            logentry("Missing player ID [" + itos(playerId) + "] on player [" + GetPCPlayerName(player) + " / " + GetName(player) + " / " + GetTag(player) + "]");
            return FALSE;
        }

        // get account associated to this ID
        string account = GetPlayerAcc(playerId);

        // ID not found or is associated to a foreign account
        if (account == "" || account != GetPCPlayerName(player))
        {
            string sLog = "Invalid or foreign player ID [" + itos(playerId) + "] on player [" + GetPCPlayerName(player) + " / " + GetName(player) + "], db acc [" + account + "]";
            logentry(sLog, TRUE);
            SYS_Info(player, SYSTEM_SYS_PREFIX, "INVALID_PLAYERID");
            return FALSE;
        }

        // set player id
        sli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID", playerId);
    }

    return playerId;
}

string GetPlayerAcc(int playerId)
{
    SQLExecDirect("select PlayerACC from sys_players where playerId = " + itos(playerId));
    if (SQLFetch() == SQL_SUCCESS)
        return SQLGetData(1);
    return "";
}


int IsRegistered(int playerId)
{
    SQLExecDirect("select registeredOn is not null from sys_players where playerId = " + itos(playerId));
    if (SQLFetch() == SQL_SUCCESS)
        return SQLGetData(1) == "1";
    return FALSE;
}



int CheckPlayerCdKey(object player)
{
    // account protection should only be invoked when nwnx and mysql is responding
    int bSQL = gli(oSystem, SYSTEM_SYS_PREFIX + "_SQL");
    if (!bSQL)
    {
        sli(player, SYSTEM_SYS_PREFIX + "_KEY_EXISTS", TRUE);
        return TRUE;
    }

    string sKey = GetPCPublicCDKey(player);
    string sAcc = GetPCPlayerName(player);
    string sQuery, sData;

    sQuery = "SELECT PlayerACC FROM sys_keys WHERE PlayerCD = '" + sKey + "'";
    SQLExecDirect(sQuery);

    //  found ACCOUNT in db
    if  (SQLFetch() == SQL_SUCCESS)
    {
        sData = SQLGetData(1);

        //  using foreign KEY
        if  (sAcc != sData)
        {
            logentry("[" + SYSTEM_SYS_PREFIX + "] Player [" + GetName(player) + " / " + GetPCPlayerName(player) + "] connected with foreign key [" + sKey + "] - booting player", TRUE);
            BootPC(player);
            return FALSE;
        }
    }

    sQuery = "SELECT PlayerCD FROM sys_keys WHERE PlayerACC = '" + sAcc + "'";
    SQLExecDirect(sQuery);

    // found CD-KEY in db
    if (SQLFetch() == SQL_SUCCESS)
    {
        sData = SQLGetData(1);

        // using foreign ACCOUNT
        if (sKey != sData)
        {
            logentry("[" + SYSTEM_SYS_PREFIX + "] Player [" + GetName(player) + " / " + GetPCPlayerName(player) + "] connected with foreign account [" + sKey + "] - booting player...", TRUE);
            BootPC(player);
            return FALSE;
        }
    }

    sli(player, SYSTEM_SYS_PREFIX + "_KEY_EXISTS", TRUE);
    return TRUE;
}
