// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) System init                      |
// | File    || vg_s1_ccs_init.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-08-2008                                                     |
// | Updated || 17-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system CCS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_ccs_core_t"
#include    "nwnx_funcs"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_CCS_PREFIX;
    string  sName       =   SYSTEM_CCS_NAME;
    string  sVersion    =   SYSTEM_CCS_VERSION;
    string  sAuthor     =   SYSTEM_CCS_AUTHOR;
    string  sDate       =   SYSTEM_CCS_DATE;
    string  sUpDate     =   SYSTEM_CCS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_ccs_info");
//    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_ccs_data_c");
//    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_ccs_data_s");
//    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_ccs_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_ccs_public", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_CHAT) + "_SCRIPT", "vg_s1_ccs_chat", -2);
/*
    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 1",
        -2
    );

    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 2",
        -2
    );
*/
    sls(oSystem, sSystem + "_DATATYPE", "N", CCS_DATATYPE_INTEGER + 1);
    sls(oSystem, sSystem + "_DATATYPE", "F", CCS_DATATYPE_FLOAT + 1);
    sls(oSystem, sSystem + "_DATATYPE", "S", CCS_DATATYPE_STRING + 1);
    sls(oSystem, sSystem + "_DATATYPE", "O", CCS_DATATYPE_OBJECT + 1);
    sls(oSystem, sSystem + "_DATATYPE", "L", CCS_DATATYPE_LOCATION + 1);

    //  inicializace skupin a prikazu
    ExecuteScript("vg_s1_ccs_cmd", GetModule());

    //  inicializace kanalu mluvy
    ExecuteScript("vg_s1_ccs_chanel", GetModule());

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
