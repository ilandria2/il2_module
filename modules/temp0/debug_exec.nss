#include    "vg_s0_tis_core_c"

void main()
{
    SpawnScriptDebugger();
    object  oPC = OBJECT_SELF;//GetFirstPC();
    object  oTIS, oSYS, oMaster, oSpawn;
    string  sGroup;
    int     n, s, nID, nSpawn, nProg, nHBs, nTime, nRow;

    oSYS    =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");
    nID     =   gli(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN", ++n);
    nHBs    =   gli(oSYS, SYSTEM_SYS_PREFIX + "_HBS");

    msg(oPC, "System time: " + itos(nHBs));

    while   (nID)
    {
        sGroup  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN_" + itos(nID) + "_NAME");
        oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sGroup);
        nSpawn  =   gli(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN_OBJECT_O_ROWS");
        nTime   =   gli(oMaster, SYSTEM_TIS_PREFIX + "_HBS");
        s       =   0;

        msg(oPC, "Master " + itos(nID) + ": " + sGroup + " (" + itos(nTime) + ")");

        while   (++s    <=  nSpawn)
        {
            oSpawn  =   glo(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN_OBJECT", s);
            nProg   =   gli(oMaster, SYSTEM_TIS_PREFIX + "_HBS", s);
            nRow    =   gli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW");
            sGroup  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP");

            msg(oPC, "- S" + itos(s) + ": " + GetTag(oSpawn) + " / " + GetName(GetArea(oSpawn)) + " (" + itos(nProg) + ") " + itos(nRow) + "-" + sGroup);

        }

        nID     =   gli(oTIS, SYSTEM_TIS_PREFIX + "_SPAWN", ++n);
    }

    object  oArea   =   GetObjectByTag("VG_AI_00_TESTARE");
    string  sResRef;
    int x, y;

    while   (GetIsObjectValid(oArea))
    {
        nSpawn  =   gli(oArea, SYSTEM_TIS_PREFIX + "_SPAWN_RESREF_S_ROWS");
        x       =   0;
        y++;

        if  (nSpawn)
        msg(oPC, "Area " + GetName(oArea));

        while   (++x    <=  nSpawn)
        {
            sResRef =   gls(oArea, SYSTEM_TIS_PREFIX + "_SPAWN_RESREF", x);
            oSpawn  =   glo(oArea, SYSTEM_TIS_PREFIX + "_SPAWN_SP", x);
            nRow    =   gli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW");
            sGroup  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP");

            if  (sResRef    !=  "")
            msg(oPC, "CreSL." + itos(x) + " " + sResRef + " / " + GetName(oSpawn) + " r." + itos(nRow) + "-" + sGroup);
        }

        oSpawn  =   GetNearestObjectByTag(TIS_TAG_TIS_SPAWN_POINT, GetFirstObjectInArea(oArea), x = 1);

        while   (GetIsObjectValid(oSpawn))
        {
            nRow    =   gli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW");
            sGroup  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP");

            if  (nRow)
            msg(oPC, "CreSP." + itos(x) + " r." + itos(nRow) + "-" + sGroup);
            oSpawn  =   GetNearestObjectByTag(TIS_TAG_TIS_SPAWN_POINT, GetFirstObjectInArea(oArea), ++x);
        }

        if  (y==2)break;
        oArea   =   GetObjectByTag("VG_AD_00_DUNG_01_TESTDNG");
    }
}
