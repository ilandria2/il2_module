// STORES console command script

#include "vg_s0_ccs_core_t"
#include "vg_s0_ets_core"
#include "vg_s0_sys_core_d"
#include "vg_s0_sys_core_c"

void FilterStoresByPattern(object player, string pattern, string areaCat = "", int row = 0, string out_message = "");

void main()
{
    string sCmd = "STORES";
    string sLine = CCS_GetCommandLine(TRUE);
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int bLog = gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object player = OBJECT_SELF;

    if (bLog)
    {
        int nId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

    string sPattern = CCS_GetConvertedString(sLine, 1, FALSE);
    sPattern = upper(sPattern);
    int storeId = stoi(sPattern);

    /*  Search by PATTERN:

        Island [SLVISL_EXTN01E00]:
        100 - VG_M_HC_HUNTER01 - Hunter's needs
        104 - VG_M_HC_SMITH_01 - Strange smithy
        105 - VG_M_HC_HERBAL01 - Must have herbs

        Island [SLVISL_EXTS00E01]:
        101 - VG_M_HC_FENCE_01 - Shady pocket
    */

    if (gli(player, "ets"))SpawnScriptDebugger();

    // search stores by pattern
    if (storeId < 100)
    {
        FilterStoresByPattern(player, sPattern);
    }

    /*  Search by ID

        Store details:
        ID - 104
        Tag - VG_M_HC_SMITH_01
        Name - Strange smithy
        AreaTag - SLVISL_EXTN01E00
        AreaName - Island
        Owner - VG_C_NH_M_HCHUNT

        Merchandise:
        3x Torch
        164x Copper coins
        2x Cloth bandages
        7x Clean water
    */

    // search store by ID
    else
    {
        object store = glo(__ETS(), ETS_ + "STORE", storeId);

        if (!GetIsObjectValid(store))
        {
            SYS_Info(player, ETS, "INVALID_STORE_ID");
            return;
        }

        string name = GetName(store);
        string tag = GetTag(store);
        string areaTag = GetTag(GetArea(store));
        string areaName = GetName(GetArea(store));
        string owner = gls(store, ETS_ + "STORE_OWNER");
        string message = Y1 + "Store details:\n\n";

        message += Y2 + "ID: " + G1 + itos(storeId) + "\n";
        message += Y2 + "Tag: " + G1 + tag + "\n";
        message += Y2 + "Name: " + G1 + name + "\n";
        message += Y2 + "AreaTag: " + G1 + areaTag + "\n";
        message += Y2 + "AreaName: " + G1 + areaName + "\n";
        message += Y2 + "OwnerTag: " + G1 + owner + "\n";

        message += "\n" + Y1 + "Merchandise:\n\n";

        //TODO:

        ExamineMessage(player, message);
    }
}

void FilterStoresByPattern(object player, string pattern, string areaCat = "", int row = 0, string out_message = "")
{
    int storeId = gli(__ETS(), ETS_ + "STORE", ++row);

    if (storeId >= 100)
    {
        object store = glo(__ETS(), ETS_ + "STORE", storeId);
        string name = GetName(store);
        string tag = GetTag(store);
        string areaTag = GetTag(GetArea(store));
        string areaName = GetName(GetArea(store));

        if (row == 1)
        {
            pattern = "**" + pattern + "**";
            out_message = Y1 + "Stores by pattern: " + G1 + pattern + Y1 + ":\n\n";
        }

        if (areaCat != areaTag)
        {
            out_message += "\n" + W2 + areaName + " (" + areaTag + W2 + "):\n";
            areaCat = areaTag;
        }

        if (pattern == "**ALL**" || TestStringAgainstPattern(pattern, tag + ";" + name + ";" + areaTag + ";" + areaName + ";"))
            out_message += Y1 + itos(storeId) + " - " + G2 + tag + Y1 + " - " + name + "\n";

        DelayCommand(0.01, FilterStoresByPattern(player, pattern, areaCat, row, out_message));
    }

    ExamineMessage(player, out_message);
}

