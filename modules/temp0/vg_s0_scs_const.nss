// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Constants                              |
// | File    || vg_s0_scs_const.nss                                            |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu SCS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_SCS_NAME                     =   "SpellCraft System";
const string    SYSTEM_SCS_PREFIX                   =   "SCS";
const string    SYSTEM_SCS_VERSION                  =   "3.00";
const string    SYSTEM_SCS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_SCS_DATE                     =   "20-03-2008";
const string    SYSTEM_SCS_UPDATE                   =   "13-05-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    SCS_NCS_CONDITION_RESREF            =   "vg_s1_scs_spchec";
const string    SCS_NCS_PERMITTED_RESREF            =   "vg_s1_scs_spperm";
const string    SCS_NCS_SPELL_FAILURE_RESREF        =   "vg_s1_scs_spfail";
const string    SCS_NCS_SPELL_FAILURE_M_RESREF      =   "vg_s1_scs_spfaim";
const string    SCS_NCS_LEARN_SCHOOL_RESREF         =   "vg_s1_scs_learsc";
const string    SCS_NCS_LEARN_SPELL_RESREF          =   "vg_s1_scs_learsp";
const string    SCS_NCS_GAIN_XP_RESREF              =   "vg_s1_scs_gainxp";
const string    SCS_NCS_SPELL_EVENT_CONJURE         =   "vg_s3_scs_spconj";
const string    SCS_NCS_SPELL_EVENT_CAST            =   "vg_s3_scs_spcast";
const string    SCS_NCS_PLACE_SUMMONING_CIRCLE      =   "vg_s1_scs_scopen";
const string    SCS_NCS_DESTROY_SUMMONING_CIRCLE    =   "vg_s1_scs_scclos";
const string    SCS_NCS_SPELL_SUMMONING_CIRCLE      =   "vg_s3_scs_circle";
const string    SCS_NCS_REGEN_MANA_LOOP             =   "vg_s1_scs_regene";
const string    SCS_NCS_DOMAIN_POWER_SCRIPT         =   "vg_s3_scs_domain";

const string    SCS_TAG_SUMMONING_CIRCLE            =   "VG_P_VE_S_CIRCLE";

const string    SCS_DB_TABLE_SPELLS                 =   "scs_spells";
const string    SCS_DB_TABLE_MANA                   =   "scs_mana";
const string    SCS_DB_COLUMN_CHARACTER_ID          =   "id_char";
const string    SCS_DB_COLUMN_SPELL_ID              =   "id_spell";
const string    SCS_DB_COLUMN_MANA_VALUE            =   "mana";

const int       SCS_SPELL_SUMMONING_CIRCLE          =   1628;
const int       SCS_SPELL_CONJURE_MODE              =   1627;
const int       SCS_SPELL_MANA_RESTORE_LIGHT        =   1100;
const int       SCS_SPELL_MANA_RESTORE_MEDIUM       =   1101;
const int       SCS_SPELL_MANA_RESTORE_GREATER      =   1102;
const int       SCS_SPELL_MANA_RESTORE_STRONG       =   1103;
const int       SCS_SPELL_MANA_RESTORE_MONSTROUS    =   1104;
const int       SCS_SPELL_MANA_RESTORE              =   1105;

const int       SCS_FIRST_CONJURE_SPELL             =   2000;
const int       SCS_FIRST_CONJURE_FEAT              =   1700;
const int       SCS_FIRST_CONJURE_ABILITY           =   1600;

const int       SCS_IPRP_TYPE_SPELLBOOK             =   152;
const int       SCS_IPRP_TYPE_ADDITIONAL            =   87;
const int       SCS_IPRP_SUBTYPE_BONUS_MANA_V       =   3;
const int       SCS_IPRP_SUBTYPE_BONUS_MANA_P       =   4;
const int       SCS_IPRP_SUBTYPE_BONUS_REGEN_MANA_V =   5;
const int       SCS_IPRP_SUBTYPE_BONUS_REGEN_MANA_P =   6;

const int       SCS_CONJURE_MODE_NORMAL             =   0;
const int       SCS_CONJURE_MODE_LEARN              =   1;
const int       SCS_CONJURE_MODE_FAKE               =   2;

const int       SCS_MANA_REGEN_REST_BONUS_MEDITATE  =   100;
const int       SCS_MANA_REGEN_REST_BONUS_SIT       =   5;
const int       SCS_MANA_REGEN_REST_BONUS_NAP       =   10;
const int       SCS_MANA_REGEN_REST_BONUS_SLEEP     =   15;
const int       SCS_MANA_REGEN_REST_BONUS_FSLEEP    =   0;

const int       SCS_MANA_MAX_BASE_VALUE             =   10;

const float     SCS_MANA_MAX_FACTOR_ABILITY         =   0.2f;
const float     SCS_MANA_MAX_FACTOR_SORCERER        =   0.2f;
const float     SCS_MANA_MAX_FACTOR_LOW_CASTER      =   0.5f;
const float     SCS_MANA_MAX_FACTOR_CASTER          =   0.75f;
const float     SCS_MANA_MAX_FACTOR_HIGH_CASTER     =   1.0f;

const int       SCS_MANA_REGEN_BASE_VALUE           =   2;

const float     SCS_MANA_REGEN_FACTOR_SKILL         =   0.02f;
const float     SCS_MANA_REGEN_FACTOR_SORCERER      =   1.5f;
const float     SCS_MANA_REGEN_FACTOR_LOW_CASTER    =   0.25f;
const float     SCS_MANA_REGEN_FACTOR_CASTER        =   0.5f;
const float     SCS_MANA_REGEN_FACTOR_HIGH_CASTER   =   0.75f;

const int       SCS_METAMAGIC_MODIFIER_EXTEND       =   1;
const int       SCS_METAMAGIC_MODIFIER_EMPOWER      =   2;
const int       SCS_METAMAGIC_MODIFIER_STILL        =   3;
const int       SCS_METAMAGIC_MODIFIER_SILENT       =   3;
const int       SCS_METAMAGIC_MODIFIER_MAXIMIZE     =   4;
const int       SCS_METAMAGIC_MODIFIER_QUICKEN      =   4;

const int       SCS_SPELL_FAILURE_COEFFICIENT       =   6;
const int       SCS_MANA_BASECOST_CANTRIPS          =   2;
const int       SCS_MANA_BASECOST_HIGHER_LEVELS     =   5;
const int       SCS_MANA_REGEN_HEARTBEATS           =   5;
const float     SCS_MANA_COST_COEFFICIENT           =   1.51f;
const int       SCS_MANA_COST_SC_COEFFICIENT        =   8;
