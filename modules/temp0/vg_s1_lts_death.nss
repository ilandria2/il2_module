// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) OnDeath                        |
// | File    || vg_s1_lts_death.nss                                            |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosti "OnDeath" pro poklad
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_core_c"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sPost   =   gls(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_SOURCE");

    if  (sPost  ==  "") return;

    object  oPC =   GetLastKiller();

    //  neni to hrac
    if  (GetIsDM(oPC)
    ||  (!GetIsPC(oPC)
    &&  !GetIsPC(GetMaster(oPC))))  return;

    if  (gli(oPC, "lts_test"))SpawnScriptDebugger();
    int     nTimes  =   gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEAT");
    object  oSource =   GetObjectByTag("VG_P_TC_LTS_CONT_" + sPost);

    //  progress
    if  (nTimes !=  -2)
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
        int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");
        int     nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_HEARTBEATS");

        //  jiz dosahlo maximum
        if  (nHBs - nTimes  >=  nMax)
        nTimes  =   -2;

        //  poklad otevren pred jeho spawnutim - reset
        else
        {
            //SYS_Info(oPC, SYSTEM_LTS_PREFIX, "SEQUENCE_RESET");
            return;
        }
    }

    /*
    int     nItems  =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_COUNT");

    //  poklad neni nastaven
    if  (!nItems)
    {
        SYS_Info(oPC, SYSTEM_LTS_PREFIX, "TREASURE_NOT_SET");
        return;
    }
    */

    object  oItem   =   GetFirstItemInInventory(OBJECT_SELF);

    //  poklad je jiz spawnut ale nebyl vzat - nic se nestane
    if  (GetIsObjectValid(oItem))   return;

    int nChance =   gli(oSource, SYSTEM_LTS_PREFIX + "_SPAWN_CHANCE");

    nChance =   (!nChance)  ?   100 :   nChance;

    //  neuspech pri hodu
    if  (Random(100)    >=  nChance)    return;

    object  oAttacker   =   GetLastAttacker(OBJECT_SELF);
    object  oCaster     =   GetLastSpellCaster();
    int     nAmount, nMin, nMax;

    //  fyzicke zniceni
    if  (oPC    ==  oAttacker)
    {
        int nMod    =   GetAbilityModifier(ABILITY_DEXTERITY, oPC);
        int nDC     =   GetHardness(OBJECT_SELF);
        int nDice   =   1 + Random(10);

        //  uspech pri fyzickem zniceni
        if  ((nDice + nMod) >=  nDC)
        {
            nMin    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MIN");
            nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MAX");

            nAmount =   nMin + (Random(nMax - nMin));

            SYS_Info(oPC, SYSTEM_LTS_PREFIX, "PHYSICAL_DESTROY_SUCCESS_&" + itos(nDice) + "&_&" + itos(nMod) + "&_&" + itos(nDC) + "&");
        }

        //  neuspech pri fyzickem zniceni - poklad bude poskozen / znicen
        else
        {
            //  kriticky neuspech - poklad bude znicen
            if  (nDice  ==  1)
            {
                SYS_Info(oPC, SYSTEM_LTS_PREFIX, "PHYSICAL_DESTROY_CRITICAL_FAILURE_&" + itos(nDice) + "&_&" + itos(nMod) + "&_&" + itos(nDC) + "&");
                return;
            }

            //  neuspech - poklad bude poskozen
            else
            {
                nMin    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MIN") / 2;
                nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MAX") / 2;

                nAmount =   nMin + (Random(nMax - nMin));

                SYS_Info(oPC, SYSTEM_LTS_PREFIX, "PHYSICAL_DESTROY_FAILURE_&" + itos(nDice) + "&_&" + itos(nMod) + "&_&" + itos(nDC) + "&");
            }
        }
    }

    else

    //  magicke zniceni
    if  (oPC    ==  oCaster)
    {
        int nMod    =   GetSkillRank(SKILL_CONCENTRATION, oPC, FALSE);
        int nDC     =   GetHardness(OBJECT_SELF);
        int nDice   =   1 + Random(20);

        //  uspech pri magickem zniceni
        if  ((nDice + nMod) >=  nDC)
        {
            nMin    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MIN");
            nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MAX");

            nAmount =   nMin + (Random(nMax - nMin));

            SYS_Info(oPC, SYSTEM_LTS_PREFIX, "MAGICAL_DESTROY_SUCCESS_&" + itos(nDice) + "&_&" + itos(nMod) + "&_&" + itos(nDC) + "&");
        }

        //  neuspech pri magickem - poklad bude poskozen / znicen
        else
        {
            //  kriticky neuspech - poklad bude znicen
            if  (nDice  ==  1)
            {
                SYS_Info(oPC, SYSTEM_LTS_PREFIX, "MAGICAL_DESTROY_CRITICAL_FAILURE_&" + itos(nDice) + "&_&" + itos(nMod) + "&_&" + itos(nDC) + "&");
                return;
            }

            //  neuspech - poklad bude poskozen
            else
            {
                nMin    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MIN") / 2;
                nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MAX") / 2;

                nAmount =   nMin + (Random(nMax - nMin));

                SYS_Info(oPC, SYSTEM_LTS_PREFIX, "MAGICAL_DESTROY_FAILURE_&" + itos(nDice) + "&_&" + itos(nMod) + "&_&" + itos(nDC) + "&");
            }
        }
    }

    //  jine zniceni (DM / neco)
    else    return;

    int bMain   =   gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_MAIN_TREASURE");

    nChance =   gli(oSource, SYSTEM_LTS_PREFIX + "_GOLD_CHANCE");
    nMin    =   gli(oSource, SYSTEM_LTS_PREFIX + "_GOLD_MIN");
    nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_GOLD_MAX");

    LTS_SpawnTreasure(OBJECT_SELF, oSource, bMain, nAmount, nChance, nMin, nMax);
}
