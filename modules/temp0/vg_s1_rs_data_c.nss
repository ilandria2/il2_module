// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Persistent data check script                  |
// | File    || vg_s1_rs_data_c.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-08-2009                                                     |
// | Updated || 16-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro hrace systemu RS
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_rs_const"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;

    sLog    =   "[" + SYSTEM_RS_PREFIX + "] Checking table [rs_fatigue]";
    sQuerry =   "DESCRIBE rs_fatigue";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_RS_PREFIX + "] Table [rs_fatigue] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_RS_PREFIX + "] Table [rs_fatigue] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE rs_fatigue " +
        "(" +
            "PlayerID INT(6) NOT NULL PRIMARY KEY," +
            "Energy VARCHAR(3) NOT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE rs_fatigue";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_RS_PREFIX + "] Table [rs_fatigue] created successfully"; else
        sLog    =   "[" + SYSTEM_RS_PREFIX + "] Table [rs_fatigue] error in creation";

        logentry(sLog);
    }
}
