// +---------++-----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Interactions ability              |
// | File    || vg_s1_tis_intera.nss                                            |
// +---------++-----------------------------------------------------------------+
// Performs the Primary or Secondary effect of the Interactions ability

#include "vg_s0_tis_core_c"
#include "vg_s0_gui_core"
#include "vg_s0_ple_core"

void main()
{
    object oPC = OBJECT_SELF;
    string menu = GUI_GetMenuName(oPC);

    // active menu - interupt
    if (menu != "")
    {
        GUI_StopMenu(oPC, TRUE);
        return;
    }

    // Interact ability is used to switch among Placeable Location Editor modes
    if (PLE_GetLoopFlag(oPC))
    {
        PLE_SwitchNextMode(oPC);
    }

    // primary or secondary interaction
    else
    {
        int bSecondary = gli(oPC, SYSTEM_SYS_PREFIX + "_USE_SECONDARY", -1, SYS_M_DELETE);

        if (!bSecondary)
            TIS_PrimaryInteraction(oPC);
        else
            TIS_DetectObjectsFor(oPC, TRUE, -1);
    }
}
