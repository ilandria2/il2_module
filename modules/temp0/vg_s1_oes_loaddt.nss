// +---------++----------------------------------------------------------------+
// | Name    || Objects Event System (OES) Load data loop script               |
// | File    || vg_s1_oes_loaddt.nss                                           |
// -----------------------------------------------------------------------------

#include "vg_s0_sys_core_m"
#include "vg_s0_sys_core_d"
#include "vg_s0_sys_core_v"
#include "vg_s0_oes_const"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void LoadData(object oPC, int nRow = 1, int wait = FALSE)
{
    if (!GetIsObjectValid(oPC)) return;

    int nSystems = gli(GetModule(), "SYSTEM_S_ROWS");

    while (nRow <= nSystems)
    {
        string sSystem = gls(GetModule(), "SYSTEM", nRow);
        int bGlobal = gli(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT");

        if (bGlobal)
        {
            if (!wait)
                LoadPersistentData(oPC, sSystem);

            int loading = gli(oPC, sSystem + "_LOADING_DATA");

            while (loading)
            {
                DelayCommand(1.0f, LoadData(oPC, nRow, TRUE));
                return;
            }

            wait = FALSE;
        }

        nRow++;
    }
}



void main()
{
    if (GetUserDefinedEventNumber() != OES_EVENT_NUMBER_O_PLAYER_LOGIN)
        return;

    object oPC = glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    LoadData(oPC);
}
