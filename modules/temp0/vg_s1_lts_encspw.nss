// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasure System (LTS) OnSpawn event skript for EncCre |
// | File    || vg_s1_lts_encspw                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    <DESCRIPTION>
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_m"
#include    "vg_s0_sys_core_t"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oCreature   =   OBJECT_SELF;
    int     bEnc        =   GetIsEncounterCreature(oCreature);

    //  pustit pouze pro bytosti spawnute ze setkani
    if  (!bEnc) return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_LTS_PREFIX + "_OBJECT");
    string  sDung   =   GetSubString(GetTag(GetArea(oCreature)), 6, 10);
    string  sSQL, sDate;
    int     nNth;

    sSQL    =   "SELECT NOW()";

    SQLExecDirect(sSQL);
    SQLFetch();

    sDate   =   SQLGetData(1);

    sls(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_SPAWN", sDate, -2);

    nNth    =   gli(oSystem, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_C_SPAWN_S_ROWS");

    sli(oCreature, SYSTEM_LTS_PREFIX + "_C_SPAWN", nNth);
}
