// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Core C                           |
// | File    || vg_s0_tis_core_c.nss                                           |
// | Author  || VirgiL                                                         |
// | Version || 1.00                                                           |
// | Created || 17-01-2011                                                     |
// | Updated || 17-01-2011                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu TIS typu COMMAND
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_const"
#include    "vg_s0_sys_core_c"
#include    "vg_s0_lpd_core_c"
#include    "x0_i0_position"
#include "vg_s0_gui_core"


// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || TIS_CheckCurrentObjectAndAction                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Checks whether player oPC is still able to detect selected object and action
//
// -----------------------------------------------------------------------------
int TIS_CheckCurrentObjectAndAction(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_CheckCurrentObjectAndDetectActions                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Checks the current object (detect object) and detects actions for it
//
// -----------------------------------------------------------------------------
int TIS_CheckCurrentObjectAndDetectActions(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_SetActionMenuItems                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Sets names and icons for the TIS actions currently listed on the player
//
// -----------------------------------------------------------------------------
void TIS_SetActionMenuItems(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_SetObjectMenuItems                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Sets names and icons for the TIS objects currently listed on the player
//
// -----------------------------------------------------------------------------
void TIS_SetObjectMenuItems(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_DetectObjectsFor                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Detects nAmount of interactive objects for the oObject object
//  ** nAmount - if -1 then no limit is applied
//
// -----------------------------------------------------------------------------
void TIS_DetectObjectsFor(object oObject, int startGui = FALSE, int nAmount = -1, object oDebug = OBJECT_INVALID);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_ClearObjectList                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Clears the list of Interaction menu objects stored on a player character
//
// -----------------------------------------------------------------------------
void TIS_ClearObjectList(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_ClearActionList                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Clears the list of Interaction menu actions stored on a player character
//
// -----------------------------------------------------------------------------
void TIS_ClearActionList(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_DetectActionsFor                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prozkouma vybrany objekt a podle podminek detekce nacte seznam cinnosti
//  do kolekce, ktere muze postava hrace vykonavat
//
// -----------------------------------------------------------------------------
void    TIS_DetectActionsFor(object oPC, object oDebug = OBJECT_INVALID);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_DetectAction                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Overi detekci specificke cinnosti
//
// -----------------------------------------------------------------------------
int TIS_DetectAction(object oPC, object oObject, int nAction = -1, object oDebug = OBJECT_INVALID);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_DetectObject                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Overi detekci specifickeho objektu
//
// -----------------------------------------------------------------------------
int     TIS_DetectObject(object oPC, object oObject, object oDebug = OBJECT_INVALID);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_UpdateSpawnGroup                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Update casy vsech spawn pointu u skupiny sGroup
//  V pripade, ze nektery ze spawnpointu jiz presahne svuj respawn cas, pak bude
//  tenhle spawnpoint respawnout.
//  Kdyz bForce je TRUE, pak se ignoruje cas u spawnpointu, u kt. se nyni pocita
//  cas respawnu.
//
// -----------------------------------------------------------------------------
void    TIS_UpdateSpawnGroup(string sGroup, int bForce = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || TIS_PrimaryInteraction                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Player performs a primary interaction with his/her environment nearby
//
// -----------------------------------------------------------------------------
void    TIS_PrimaryInteraction(object oPC);


// Holds fields that represent the Interaction event structure
struct TIS_Interaction
{
    object Player;
    object Object;

    //int ObjectType;
    int ObjectSubType;
    string ObjectTech;

    int ActionType;
    int ActionSubType;
    string ActionTech;

    float DelaySequence;
    location SequenceLocation;

};

// Constructs and returns a new instance of the structure of type Interaction which holds the following fields:
// Player, Object, ObjectSubType, ObjectTech, ActionType, ActionSubType, ActionTech, DelaySequence [f], SequenceLocation
struct TIS_Interaction TIS_ConstructInteraction(int actionType, object player = OBJECT_SELF);

// Handles the Interaction logic when player attempts to perform an action on an interactive object
struct TIS_Interaction TIS_HandleInteraction(struct TIS_Interaction instance);

// sets reference to a user object of Object to value
void TIS_SetUser(object Object, object value);

// gets reference to a user object of Object
object TIS_GetUser(object Object);

// both interactive Object and Player should link each other during sequence interactions and unlink at the end or when invalid)
int TIS_IsUserValid(object Object);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



void TIS_SetUser(object Object, object value)
{
    if (!GetIsObjectValid(value))
        slo(Object, TIS_ + "USERLINK", OBJECT_INVALID, -1, SYS_M_DELETE);
    else
        slo(Object, TIS_ + "USERLINK", value);
}

object TIS_GetUser(object Object)
{
    return glo(Object, TIS_ + "USERLINK");
}

int TIS_IsUserValid(object Object)
{
    if (!GetIsObjectValid(Object))
        return FALSE;

    // determine which side is Object and which Player
    object Player = OBJECT_INVALID;
    object OtherLink = OBJECT_INVALID;
    if (GetIsPC(Object))
    {
        Player = Object;
        Object = TIS_GetUser(Player);
        OtherLink = TIS_GetUser(Object);

        if (!GetIsObjectValid(OtherLink))
            return FALSE;

        if (OtherLink != Player)
            return FALSE;
    }
    else
    {
        Player = TIS_GetUser(Object);
        OtherLink = TIS_GetUser(Player);

        if (!GetIsObjectValid(OtherLink))
            return FALSE;

        if (OtherLink != Object)
            return FALSE;
    }

    // either Object or Player link is invalid
    if (!GetIsObjectValid(Player) || GetIsObjectValid(Object))
        return FALSE;

    if (GetCurrentHitPoints(Player) <= 0)
        return FALSE;

    return TRUE;
}


// +---------------------------------------------------------------------------+
// |                             TIS_HandleInteraction                         |
// +---------------------------------------------------------------------------+

void TIS_HandleInteractionVoid(struct TIS_Interaction instance)
{
    TIS_HandleInteraction(instance);
}

struct TIS_Interaction TIS_HandleInteraction(struct TIS_Interaction instance)
{
    AssignCommand(instance.Player, ClearAllActions());
    StopAnimation(instance.Player);
    string script = TIS_ + "I_" + instance.ActionTech;

    // begining of the interaction
    if (instance.DelaySequence == 0.0)
    {
        // special conditions (such as Item equipped needed) before begining of interaction were not met
        if (!CheckScript(script, instance.Player, TIS_PARAMETER_CHECK))
        {
            if (instance.DelaySequence == 0.0) return instance;

            //DelayCommand(0.1f, LPD_ResumeConversation(instance.Player));
            return instance;
        }

        // execute DO section of the tis_i_xxx interaction script
        RunScript(script, instance.Player, TIS_PARAMETER_DO);
        instance.DelaySequence = glf(instance.Player, TIS_ + "SEQ_DELAY", -1, SYS_M_DELETE);

        // begining of a sequence interaction
        if (instance.DelaySequence > 0.0)
        {
            instance.SequenceLocation = GetLocation(instance.Player);

            sll(instance.Player, SYSTEM_TIS_PREFIX + "_SEQLOC", instance.SequenceLocation);
            TIS_SetUser(instance.Object, instance.Player);
            TIS_SetUser(instance.Player, instance.Object);
            TimingBarStart(instance.Player, ftoi(instance.DelaySequence * 1000), FALSE, "", GUI_ + "END_" + GUI_GetType(GUI_TYPE_INTERACT));
            GUI_StopMenu(instance.Player);
        }

        // end of instant interaction
        else return instance;
    }

    // progress of interaction
    else
    {
        int bResult = CheckScript("vg_s2_tis_check", instance.Player, "COA");

        // sequence interact interupted
        if  (!bResult)
        {
            TimingBarCancel(instance.Player);
            return instance;
        }

        // execute DO section of the tis_i_xxx interaction script
        RunScript(script, instance.Player, TIS_PARAMETER_DO);
        instance.DelaySequence = glf(instance.Player, TIS_ + "SEQ_DELAY", -1, SYS_M_DELETE);
    }

    // end of sequence interaction
    if  (instance.DelaySequence <= 0.0 &&
        instance.DelaySequence != -999.0)
    {
        // additional time to ensure GUI is rendered after in-game object disappearance takes action
        if (instance.DelaySequence < 0.0)
            instance.DelaySequence -= 2.0;

        TIS_SetUser(instance.Object, OBJECT_INVALID);
        TIS_SetUser(instance.Player, OBJECT_INVALID);
        DelayCommand(0.05 + (-instance.DelaySequence), TIS_DetectActionsFor(instance.Player));
        DelayCommand(0.1f + (-instance.DelaySequence), TIS_SetObjectMenuItems(instance.Player));
        DelayCommand(0.1f + (-instance.DelaySequence), TIS_SetActionMenuItems(instance.Player));
        DelayCommand(0.2f + (-instance.DelaySequence), GUI_StartMenu(instance.Player, GUI_GetType(GUI_TYPE_INTERACT)));
        instance.DelaySequence = 0.0;
        return instance;
    }

    if (instance.DelaySequence > 0.0f)
        DelayCommand(instance.DelaySequence, TIS_HandleInteractionVoid(instance));
    return instance;
}



// +---------------------------------------------------------------------------+
// |                          TIS_ConstructInteraction                         |
// +---------------------------------------------------------------------------+

struct TIS_Interaction TIS_ConstructInteraction(int actionType, object player = OBJECT_SELF)
{
    struct TIS_Interaction ret;
    object Object = glo(player, TIS_ + "OBJECT", gli(player, TIS_ + "OBJECT_USED"));
    string subTag = left(GetTag(Object), 16);

    ret.SequenceLocation = gll(player, TIS_ + "SEQLOC");
    ret.ActionType = actionType;
    ret.ActionSubType = gli(player, TIS_ + "ACTION", gli(player, TIS_ + "ACTION_USED"));
    ret.ActionTech = gls(__TIS(), TIS_ + "AS_" + itos(ret.ActionSubType) + "_TECH");
    ret.Player = player;
    ret.Object = Object;
    ret.ObjectSubType = gli(__TIS(), TIS_ + "TAG_" + subTag + "_OS");
    ret.ObjectTech = gls(__TIS(), TIS_ + "TAG_" + subTag + "_TECH");

    // assign default delay when constructing interaction object in middle of sequence
    if (GetIsObjectValid(GetAreaFromLocation(ret.SequenceLocation)))
        ret.DelaySequence = TIS_INTERACT_SEQUENCE_DELAY;

    return ret;
}

// +---------------------------------------------------------------------------+
// |                             TIS_DetectAction                              |
// +---------------------------------------------------------------------------+



int TIS_DetectAction(object oPC, object oObject, int nAction = -1, object oDebug = OBJECT_INVALID)
{

//  ----------------------------------------------------------------------------
//  Detekce interakci

    sli(oPC, TIS_ + "ACTION_CHECK", nAction);   //  action check
    slo(oPC, TIS_ + "OBJECT", oObject);         //  of object

    if (GetIsObjectValid(oDebug))
        slo(oPC, TIS_ + "DEBUG_OBJECT", oDebug);

    if (nAction == -1)
        TIS_ClearActionList(oPC);

    string sCond = lower(TIS_ + "A_CHECK");     //  format "tis_a_check"
    int bResult = CheckScript(sCond, oPC);      //  check

    return  bResult;

//  Detekce interakci
//  ----------------------------------------------------------------------------

}



// +---------------------------------------------------------------------------+
// |                            TIS_ClearActionList                            |
// +---------------------------------------------------------------------------+

void TIS_ClearActionList(object oPC)
{
    int n;
    for (n = 1; n <= TIS_AREA_DETECT_ACTIONS; n++)
    {
        sli(oPC, TIS_ + "ACTION", -1, n, SYS_M_DELETE);
        sls(oPC, TIS_ + "N_A" + itos(n), "", -1, SYS_M_DELETE);
    }

    sli(oPC, TIS_ + "ACTION_I_ROWS", -1, -1, SYS_M_DELETE);
    sli(oPC, TIS_ + "ACTION_USED", -1, -1, SYS_M_DELETE);

}



// +---------------------------------------------------------------------------+
// |                           TIS_SetActionMenuItems                          |
// +---------------------------------------------------------------------------+

void TIS_SetActionMenuItems(object oPC)
{
    if (!GetIsObjectValid(oPC) || !GetIsPC(oPC))
        return;

    int nActions = gli(oPC, TIS_ + "ACTION_I_ROWS");

    if (nActions == 0) return;
    if (gli(oPC, "names2"))SpawnScriptDebugger();

    object oTIS = glo(oPC, TIS_ + "OBJECT", gli(oPC, TIS_ + "OBJECT_USED"));
    string sSub = left(GetTag(oTIS), 16);
    string sOS = itos(gli(__TIS(), TIS_ + "TAG_" + sSub + "_OS"));
    string sTECH = gls(__TIS(), TIS_ + "TAG_" + sSub + "_TECH");
    string sSTATE = gls(oTIS, TIS_ + "STATE");

    int n;
    for (n = 1; n <= nActions; n++)
    {
        string sAS = itos(gli(oPC, TIS_ + "ACTION", n));                              // Interaction SubTypeID
        string sAT = itos(gli(__TIS(), TIS_ + "AS_" + sAS + "_IID"));                 // Interaction Type

        if (sSTATE == "")
            sSTATE = gls(__TIS(), TIS_ + "SET_OS_" + sOS + "_AS_" + sAS + "_TYPE");

        if (sSTATE == "")
            sSTATE = gls(__TIS(), TIS_ + "SET_AT_" + sAT + "_DEFST_" + sTECH);        // default state from TECH

        string sName = gls(__TIS(), TIS_ + "SET_AT_" + sAT + "_NAME_" + sSTATE);      // Interaction name from settings (object state specific)

        if (sName == "")
            sName = gls(__TIS(), TIS_ + "SET_OS_" + sOS + "_AS_" + sAS + "_DISPLAY"); // Interaction name from settings (OS & AS specific)

        if (sName == "")
            sName = gls(__TIS(), TIS_ + "AS_" + sAS + "_DISPLAY");                    // Interaction name from subtypes (OT & AS specific)

        sName = C_BOLD + sName + _C;
        sls(oPC, TIS_ + "N_A" + itos(n), sName);
        sSTATE = "";
    }
}



// +---------------------------------------------------------------------------+
// |                            TIS_DetectActionsFor                           |
// +---------------------------------------------------------------------------+

void TIS_DetectActionsFor(object oPC, object oDebug = OBJECT_INVALID)
{
    object oTIS = glo(oPC, TIS_ + "OBJECT", gli(oPC, TIS_ + "OBJECT_USED"));

    if (gli(oPC, "tis_actions"))SpawnScriptDebugger();

    //AssignCommand(oPC, SetFacingPoint(GetPosition(oTIS)));
    TIS_DetectAction(oPC, oTIS, -1, oDebug);
    TIS_SetActionMenuItems(oPC);

    sll(oPC, TIS_ + "SEQLOC", li(), -1, SYS_M_DELETE);
}



// +---------------------------------------------------------------------------+
// |                            TIS_DetectObject                               |
// +---------------------------------------------------------------------------+



int     TIS_DetectObject(object oPC, object oObject, object oDebug = OBJECT_INVALID)
{
    // invalid object
    if (!GetIsObjectValid(oObject))
    {
        /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): Invalid object >");
        return FALSE;
    }

    // invalid player
    if (!GetIsObjectValid(oPC))
    {
        /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): Invalid player >");
        return FALSE;
    }

    // not a player character
    if (!GetIsPC(oPC))
    {
        /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): Not a player character >");
        return FALSE;
    }

    // player is dead or is dying
    if (GetCurrentHitPoints(oPC) < 1)
    {
        /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): Player is dead or is dying >");
        return FALSE;
    }

    // alive creature
    if (GetObjectType(oObject) == OBJECT_TYPE_CREATURE && !GetIsDead(oObject))
    {
        /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): Creature is still alive >");
        return FALSE;
    }

    string sTech = gls(__TIS(), TIS_ + "TAG_" + left(GetTag(oObject), 16) + "_TECH");

    /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): TECH: " + sTech + " >");

    //  neni to interakcni TIS objekt
    if  (sTech  ==  "") return  FALSE;

    //  kontrola specifickych podminek detekce objektu v skriptu tis_o_<OT_TECH>.nss
    slo(oPC, TIS_ + "OBJECT", oObject);

    string  sCond   =   lower(TIS_ + "O_" + sTech);
    int     bCheck  =   CheckScript(sCond, oPC);

    /**/__dbgmsg(FALSE, oDebug, "< DetectObject(): Script check: " + sCond + " result: " + itos(bCheck) + " >");

    return  bCheck;
}




// +---------------------------------------------------------------------------+
// |                           TIS_SetObjectMenuItems                          |
// +---------------------------------------------------------------------------+

void TIS_SetObjectMenuItems(object oPC)
{
    if (!GetIsObjectValid(oPC) || !GetIsPC(oPC))
        return;

    int nObjects = gli(oPC, TIS_ + "OBJECT_O_ROWS");

    if (nObjects == 0) return;

    if (gli(oPC, "names"))SpawnScriptDebugger();

    int n;
    for (n = 1; n <= nObjects; n++)
    {
        object oTIS = glo(oPC, TIS_ + "OBJECT", n);
        string sName = GetName(oTIS);
        string sOS = itos(gli(__TIS(), TIS_ + "TAG_" + left(GetTag(oTIS), 16) + "_OS"));

        sName = (sName == _C) ? "" : sName;

        if  (GetObjectType(oTIS) == OBJECT_TYPE_CREATURE)
            sName = gls(__TIS(), TIS_ + "OS_" + sOS + "_DISPLAY");

        sName = C_BOLD + sName + _C;
        sls(oPC, TIS_ + "N_O" + itos(n), sName);
    }
}


// +---------------------------------------------------------------------------+
// |                            TIS_UpdateSpawnGroup                           |
// +---------------------------------------------------------------------------+



int GetNumPC(object oArea)
{
    object  oPC =   GetFirstPC();
    int     nResult;

    while   (GetIsObjectValid(oPC))
    {
        if  (GetArea(oPC)   ==  oArea
        ||  GetModule()     ==  oArea)
        nResult++;

        oPC =   GetNextPC();
    }

    return  nResult;
}

void    TIS_UpdateSpawnGroup(string sGroup, int bForce = FALSE)
{
    object  oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + upper(sGroup));

    if  (!GetIsObjectValid(oMaster))    return;

    //if(!gli(GetFirstPC(), "tis_spawn"))    SpawnScriptDebugger();

    int     nHour   =   GetTimeHour();
    int     nStart  =   gli(oMaster, TIS_ + "TIME_S");
    int     nEnd    =   gli(oMaster, TIS_ + "TIME_E");
    int     n, nSpawn;
    object  oSpawn;

    //  spawnovani ve specificky cas
    if  (nStart !=  -1)
    {
        //  sekvence mimo validniho casovyho rozpeti - despawn a vycisteni seznamu
        if  (
        ((nStart    <   nEnd)   &&  ((nHour <   nStart) ||  (nHour  >   nEnd))) ||
        ((nStart    >=  nEnd)   &&  ((nHour <   nStart) &&  (nHour  >   nEnd))))
        {
            n       =   0;
            nSpawn  =   gli(oMaster, TIS_ + "SPAWN_OBJECT_O_ROWS", -1, SYS_M_DELETE);

            for (n = 1; n <= nSpawn; n++)
            {
                oSpawn  =   glo(oMaster, TIS_ + "SPAWN_OBJECT", n, SYS_M_DELETE);

                SetPlotFlag(oSpawn, FALSE);
                DestroyObject(oSpawn, 0.0f);
                sli(oMaster, TIS_ + "HBS", -1, n, SYS_M_DELETE);
            }

            object  oArea;
            int     a;

            oArea   =   glo(oMaster, TIS_ + "AREA_SP", a = 1);

            while   (GetIsObjectValid(oArea))
            {
                nSpawn  =   gli(oArea, TIS_ + "SPAWN_SP_O_ROWS");

                for (n = 1; n <= nSpawn; n++)
                {
                    slo(oArea, TIS_ + "SPAWN_SP", OBJECT_INVALID, n, SYS_M_DELETE);
                    sls(oArea, TIS_ + "SPAWN_RESREF", "", n, SYS_M_DELETE);
                }

                sli(oArea, TIS_ + "SPAWN_SP_O_ROWS", -1, -1, SYS_M_DELETE);
                sli(oArea, TIS_ + "SPAWN_RESREF_S_ROWS", -1, -1, SYS_M_DELETE);
                sli(oArea, TIS_ + "GROUP_" + sGroup + "_AREA_SP", -1, -1, SYS_M_DELETE);

                oArea   =   glo(oMaster, TIS_ + "AREA_SP", ++a);
            }

            sli(oMaster, TIS_ + "AREA_SP_O_ROWS", -1, -1, SYS_M_DELETE);
            return;
        }
    }

    int     nCount  =   gli(oMaster, TIS_ + "COUNT");

    if  (nCount)
    {
        int     nHBs    =   gli(__TIS(), SYSTEM_SYS_PREFIX + "_HBS");
        int     nTime   =   gli(oMaster, TIS_ + "HBS");
        int     nTotal  =   gli(oMaster, TIS_ + "RESREF_S_ROWS");
        int     nRows   =   gli(oMaster, TIS_ + "SPAWN_O_ROWS");
        string  sObject, sType;
        int     nProg, nType, nResRef, bDo;
        float   fDist;

        n       =   0;
        nTime   =   (!nTime)    ?   TIS_SPAWN_TIME  :   nTime;

        while   (++n    <=  nCount)
        {
            nProg   =   gli(oMaster, TIS_ + "HBS", n);

            //  n-ty spawnpoint je nastaveny, ze objekt je jiz spawnutej
            if  (nProg  ==  -1)
            {
                oSpawn  =   glo(oMaster, TIS_ + "SPAWN_OBJECT", n);

                //  neexistuje - resetnuti promenne aby pocitala odznovu
                if  (!GetIsObjectValid(oSpawn))
                nProg   =   nHBs;
            }

            if  (nProg  >   -1)
            {
                bForce  =   (bForce)    ?   bForce  :   gli(oMaster, TIS_ + "FSPAWN", -1, SYS_M_DELETE);

                //  cas n-tyho spawnpointu dosahl respawn time
                if  (bForce
                ||  nHBs - nProg   >=  nTime)
                {
                    nResRef =   1 + Random(nTotal);
                    sType   =   gls(oMaster, TIS_ + "TYPE", nResRef);
                    oSpawn  =   glo(oMaster, TIS_ + "SPAWN", 1 + Random(nRows));
                    sObject =   gls(oMaster, TIS_ + "RESREF", nResRef);
                    bDo     =   (GetNumPC(GetArea(oSpawn)) || sType !=  "C")    ?   TRUE    :   FALSE;
                    nProg   =   -1;

                    slo(oMaster, TIS_ + "SPAWN_OBJECT", oSpawn, n);

                    //  nenastane spawn - jenom se spawnpoint oznaci jako pripraveny ke spawnuti bytosti
                    if  (!bDo)
                    {
                        object  oArea   =   GetArea(oSpawn);
                        int     nSPRow  =   gli(oArea, TIS_ + "GROUP_" + sGroup + "_AREA_SP");

                        if  (!nSPRow)
                        {
                            //  seznam oblasti, ve kterych se maji instantne spawnout bytosti (OnTrigger)
                            slo(oMaster, TIS_ + "AREA_SP", oArea, -2);

                            //  p.c. oblasti v ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            sli(oArea, TIS_ + "GROUP_" + sGroup + "_AREA_SP", gli(oMaster, TIS_ + "AREA_SP_O_ROWS"));
                        }

                        slo(oArea, TIS_ + "SPAWN_SP", oSpawn, -2);            //  seznam spawn-point objektu, ktere jsou oznacene jako "instant creature spawn"
                        sls(oArea, TIS_ + "SPAWN_RESREF", sObject, -2);       //  seznam resrefu bytosti pro ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                    }

                    //  nastane spawn
                    else
                    {
                        nType   =   (sType  ==  "C")    ?   OBJECT_TYPE_CREATURE    :   OBJECT_TYPE_PLACEABLE;
                        fDist   =   glf(oSpawn, TIS_ + "RADIUS");
                        fDist   =   (fDist  ==  0.0f)   ?   TIS_SPAWN_RADIUS    :   fDist;
                        fDist   =   ran(0.0f, fDist);

                        oSpawn  =   CreateObject(nType, sObject, GetRandomLocation(GetArea(oSpawn), oSpawn, fDist));

                        int nMin    =   gli(oSpawn, TIS_ + "MONEY_MIN");
                        int nMax    =   gli(oSpawn, TIS_ + "MONEY_MAX");

                        //  spawn pokladu s penezi
                        if  (nMax   >   0)
                        {
                            //ExecuteScript("vg_s1_acs_treas", oSpawn);//xxxxxxxxxxx
                        }

                        slo(oMaster, TIS_ + "SPAWN_OBJECT", oSpawn, n);
                    }

                    sls(oSpawn, TIS_ + "GROUP", sGroup);
                    sli(oSpawn, TIS_ + "ROW", n);
                }

                else
                nProg   =   nHBs;

                sli(oMaster, TIS_ + "HBS", nProg, n);
            }
        }   //  while
    }
}



// +---------------------------------------------------------------------------+
// |                         TIS_PrimaryInteraction                            |
// +---------------------------------------------------------------------------+

void    TIS_PrimaryInteraction(object oPC)
{
    if (gli(oPC, "primint"))SpawnScriptDebugger();

    GUI_StopMenu(oPC, TRUE);
    TIS_DetectObjectsFor(oPC, FALSE, 1);

    object oTIS = glo(oPC, TIS_ + "OBJECT", 1);

    // no objects found
    if (!GetIsObjectValid(oTIS))
    {
        SYS_Info(oPC, TIS, "NO_OBJECTS");
        return;
    }

    sli(oPC, TIS_ + "OBJECT_USED", 1);
    TIS_DetectActionsFor(oPC);

    // determine primary action
    string sAction = itos(gli(oPC, TIS_ + "ACTION", 1));

    // no actions detected
    if (sAction == "0")
    {
        SYS_Info(oPC, TIS, "NO_ACTIONS");
        return;
    }

    sli(oPC, TIS_ + "ACTION_USED", 1);
    if (!TIS_CheckCurrentObjectAndAction(oPC)) return;

    if (GetDistanceBetween(oPC, oTIS) > 1.0f)
        AssignCommand(oPC, ActionMoveToObject(oTIS, FALSE, 0.9f));


    // execute action
    string sTech = gls(__TIS(), TIS_ + "AS_" + sAction + "_TECH");

    //AssignCommand(oPC, ActionDoCommand(SetFacingPoint(GetPosition(oTIS))));
    AssignCommand(oPC, ActionDoCommand(ExecuteScript(lower(TIS_ + "I_" + sTech), oPC)));
}



// +---------------------------------------------------------------------------+
// |                            TIS_ClearObjectList                            |
// +---------------------------------------------------------------------------+

void TIS_ClearObjectList(object oPC)
{
    int n;

    for (n = 1; n <= TIS_AREA_DETECT_OBJECTS; n++)
    {
        slo(oPC, TIS_ + "OBJECT", OBJECT_INVALID, n, SYS_M_DELETE);
        sls(oPC, TIS_ + "N_O" + itos(n), "", -1, SYS_M_DELETE);
    }

    sli(oPC, TIS_ + "OBJECT_O_ROWS", -1, -1, SYS_M_DELETE);
    sli(oPC, TIS_ + "OBJECT_USED", -1, -1, SYS_M_DELETE);
}



// +---------------------------------------------------------------------------+
// |                            TIS_DetectObjectsFor                           |
// +---------------------------------------------------------------------------+

void TIS_DetectObjectsFor(object oObject, int startGui = FALSE, int nAmount = -1, object oDebug = OBJECT_INVALID)
{
    /**/__dbgmsg(FALSE, oDebug, "< DETECT STARTED >");
    /**/__dbgmsg(FALSE, oDebug, "< Source creature: " + GetName(oObject) + " / " + GetPCPlayerName(oObject) + " / " + GetTag(oObject) + ", AmountToDetect: " + itos(nAmount) + ">");

    /**/__dbgmsg(FALSE, oDebug, "< Clearing object list... >");
    TIS_ClearObjectList(oObject);

    int n;
    object oTIS = GetNearestObject(OBJECT_TYPE_CREATURE | OBJECT_TYPE_DOOR | OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_TRIGGER | OBJECT_TYPE_WAYPOINT, oObject, n = 1);
    int nFound = 0;

    // search objects
    while (GetIsObjectValid(oTIS))
    {
        /**/__dbgmsg(FALSE, oDebug, "< obj " + itos(n) + ": " + GetTag(oTIS) + " / " + GetName(oTIS) + " >");
        /**/__dbgmsg(FALSE, oDebug, "< dist: " + ftos(GetDistanceBetween(oObject, oTIS)) + " vs max " + ftos(TIS_INTERACT_DISTANCE) + " >");

        if (GetDistanceBetween(oObject, oTIS) > TIS_INTERACT_DISTANCE) break;

        if (TIS_DetectObject(oObject, oTIS, oDebug))
        {
            nFound++;
            slo(oObject, TIS_ + "OBJECT", oTIS, -2);

            if (nFound == nAmount)
            {
                /**/__dbgmsg(FALSE, oDebug, "< Max amount wanted reached >");
                break;
            }
        }

        oTIS = GetNearestObject(OBJECT_TYPE_CREATURE | OBJECT_TYPE_DOOR | OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_TRIGGER | OBJECT_TYPE_WAYPOINT, oObject, ++n);
    }

    // single object found - select it immediately
    if (nFound == 1)
        sli(oObject, TIS_ + "OBJECT_USED", 1);

    TIS_DetectActionsFor(oObject);
    TIS_SetObjectMenuItems(oObject);

    if (startGui)
    {
        if (nFound > 0)
            GUI_StartMenu(oObject, GUI_GetType(GUI_TYPE_INTERACT));
        else
            SYS_Info(oObject, TIS, "NO_OBJECTS");
    }

    /**/__dbgmsg(FALSE, oDebug, "< DETECT ENDED >");
}



// +---------------------------------------------------------------------------+
// |                   TIS_CheckCurrentObjectAndDetectActions                  |
// +---------------------------------------------------------------------------+

void    Move(object oPC, object oMoveTo)
{
    if  (GetDistanceBetween(oPC, oMoveTo)   <   1.0f)   return;

    AssignCommand(oPC, ActionMoveToObject(oMoveTo, FALSE, 0.9f));
}



int TIS_CheckCurrentObjectAndDetectActions(object oPC)
{
    int bResult = FALSE;
    int     nCurObj =   gli(oPC, TIS_ + "OBJECT_USED");
    object  oCurObj;

    if  (!nCurObj)
    {
        nCurObj = 1;
        sli(oPC, TIS_ + "OBJECT_USED", nCurObj);
    }

    oCurObj =   glo(oPC, TIS_ + "OBJECT", nCurObj);   //  aktualni TIS objekt
    bResult =   TIS_DetectObject(oPC, oCurObj);                     //  opetovni detekce aktualniho TIS objektu

    //  neuspech -- smazani aktualniho vyberu objektu / cinnosti
    if  (!bResult)
        GUI_StopMenu(oPC, TRUE);

    //  uspech -- detekce dostupnych interakci
    else
    {
        int nCurAct =   gli(oPC, TIS_ + "ACTION_USED");

        //  priblizeni k cili a otoceni na spravnim smerem
        if  (!nCurAct)
        {
            //DelayCommand(0.1f, Move(oPC, oCurObj));
            //AssignCommand(oPC, ActionDoCommand(SetFacingPoint(GetPosition(oCurObj))));
        }

        //  detekce cinnosti
        TIS_DetectActionsFor(oPC);
        sli(oPC, TIS_ + "CHECK_RESULT", -1, -1, SYS_M_DELETE);
    }

    return bResult;
}



// +---------------------------------------------------------------------------+
// |                     TIS_CheckCurrentObjectAndAction                       |
// +---------------------------------------------------------------------------+

int TIS_CheckCurrentObjectAndAction(object oPC)
{
    int bResult = FALSE;
    int     nCurAct =   gli(oPC, TIS_ + "ACTION_USED");                                           //  p.c. current action
    int     nCurObj =   gli(oPC, TIS_ + "OBJECT_USED");                                           //  current object row
    object  oCurObj =   glo(oPC, TIS_ + "OBJECT", nCurObj);                                       //  current object
    location lSeq   =   gll(oPC, TIS_ + "SEQLOC");
    location locPC = GetLocation(oPC);
    int curAct = GetCurrentAction(oPC);

    //  hrac v prubehu smycky sekvence zmenil lokaci
    if  (GetIsObjectValid(GetAreaFromLocation(lSeq))
    &&  lSeq   != locPC
    && GetPositionFromLocation(lSeq) != GetPositionFromLocation(locPC))
    {
        sli(oPC, TIS_ + "CHECK_RESULT", TIS_CHECK_RESULT_ACTION_VECTOR);
        SYS_Info(oPC, TIS, "CHECK_RESULT_&" + itos(TIS_CHECK_RESULT_ACTION_VECTOR) + "&");

        bResult =   FALSE;
    }

    else

    //  hrac musi byt v interakcni vzdalenosti od TIS objektu
    if  (GetDistanceBetween(oPC, oCurObj)   >   TIS_INTERACT_DISTANCE)
    {
        sli(oPC, TIS_ + "CHECK_RESULT", TIS_CHECK_RESULT_ACTION_DIST);
        SYS_Info(oPC, TIS, "CHECK_RESULT_&" + itos(TIS_CHECK_RESULT_ACTION_DIST) + "&");
        //DelayCommand(0.1f, Move(oPC, oCurObj));
        //AssignCommand(oPC, ActionDoCommand(SetFacingPoint(GetPosition(oCurObj))));

        bResult =   FALSE;
    }

    else

    //  hrac nesmi vykonavat zadnou cinnost
    if  (curAct != ACTION_INVALID &&
        curAct != ACTION_USEOBJECT)
    {
        sli(oPC, TIS_ + "CHECK_RESULT", TIS_CHECK_RESULT_ACTION_ACTION);
        SYS_Info(oPC, TIS, "CHECK_RESULT_&" + itos(TIS_CHECK_RESULT_ACTION_ACTION) + "&");

        bResult =   FALSE;
    }

    else
    {
        //  opetovni detekce aktualniho objektu
        bResult =   TIS_DetectObject(oPC, oCurObj);

        //  neuspech - vraceni zpet do vyberu objektu v menu
        if  (!bResult)
        {
            //SpawnScriptDebugger();
            int n       =   nCurObj;
            int nLast   =   gli(oPC, TIS_ + "OBJECT_O_ROWS");

            //  smazani aktualniho objektu ze seznamu a posunuti vsech dalsich objektu o 1 zpet v seznamu
            for (; n < nLast; n++)
            slo(oPC, TIS_ + "OBJECT",
            glo(oPC, TIS_ + "OBJECT", n + 1), n);
            slo(oPC, TIS_ + "OBJECT", OBJECT_INVALID, n, SYS_M_DELETE);
            sli(oPC, TIS_ + "OBJECT_O_ROWS", nLast - 1);
            sli(oPC, TIS_ + "OBJECT_USED", -1, -1, SYS_M_DELETE);
            sli(oPC, TIS_ + "ACTION_USED", -1, -1, SYS_M_DELETE);

            SYS_Info(oPC, TIS, "CHECK_RESULT_&" + itos(TIS_CHECK_RESULT_ACTION_OBJECT) + "&");
        }

        //  uspech
        else
        {
            bResult =   TIS_DetectAction(oPC, oCurObj, nCurAct);

            //  neplatni cinnost (postava hrace jiz nedokaze detekovat vybranou cinnost)
            if  (!bResult)
            {
                sli(oPC, TIS_ + "CHECK_RESULT", TIS_CHECK_RESULT_ACTION_INVALID);
                SYS_Info(oPC, TIS, "CHECK_RESULT_&" + itos(TIS_CHECK_RESULT_ACTION_INVALID) + "&");
            }

            //AssignCommand(oPC, SetFacingPoint(GetPosition(oCurObj)));
        }
    }

    return bResult;
}
