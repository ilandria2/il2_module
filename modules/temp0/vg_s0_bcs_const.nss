// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Constants                             |
// | File    || vg_s0_bcs_const.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 18-07-2009                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu BCS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_BCS_NAME                     =   "BattleCraft System";
const string    SYSTEM_BCS_PREFIX                   =   "BCS";
const string    SYSTEM_BCS_VERSION                  =   "2.00";
const string    SYSTEM_BCS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_BCS_DATE                     =   "26-12-2007";
const string    SYSTEM_BCS_UPDATE                   =   "18-07-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const int       BCS_ATTACKS_WEAPON_SIZE_UNARMED     =   6;  //  BCS_ATTACKS_WEAPON_SIZE_TINY + 1
const int       BCS_ATTACKS_WEAPON_SIZE_TINY        =   5;  //  baseitems.2da -> size = 1; result = 6 - size = 6 - 1 = 5
const int       BCS_ATTACKS_WEAPON_SIZE_SMALL       =   4;  //  baseitems.2da -> size = 2; result = 6 - size = 6 - 2 = 4
const int       BCS_ATTACKS_WEAPON_SIZE_MEDIUM      =   3;  //  baseitems.2da -> size = 3; result = 6 - size = 6 - 3 = 3
const int       BCS_ATTACKS_WEAPON_SIZE_LARGE       =   2;  //  baseitems.2da -> size = 4; result = 6 - size = 6 - 4 = 2

const float     BCS_COOLDOWN_DURATION               =   6.0f;   //  static duration for the abilities-cooldown effect

const int       BCS_STAMINA_COST_ADVHIT_PRIMARY     =   10;
const int       BCS_STAMINA_COST_ADVHIT_SECONDARY   =   30;
const int       BCS_POWER_COST_ADVHIT_PRIMARY       =   20;
const int       BCS_POWER_COST_ADVHIT_SECONDARY     =   40;
const int       BCS_FATIGUE_COST_ADVHIT_PRIMARY     =   5;
const int       BCS_FATIGUE_COST_ADVHIT_SECONDARY   =   10;

