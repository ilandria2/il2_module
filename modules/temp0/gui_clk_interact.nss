
#include "vg_s0_gui_core"
#include "vg_s0_tis_core_c"

//////////////// 6. tis gui clicked handler
///// actions taken by the system using the GUI

const int _GUI_TYPE = GUI_TYPE_INTERACT;

void main()
{
    object player = OBJECT_SELF;
    object layout = GUI_GetLayoutObjectClicked(player);
    int layoutId = GUI_GetLayoutObjectId(layout);

    ///////////////////////////////////////////////////////////
    // copy TIS Menu LPD dialog interaction logic
    /////////////////////////////////////////////////////////////

    // close button clicked
    if (layoutId == GUI_LAYOUT_CLOSE_BUTTON)
    {
        GUI_StopMenu(player, TRUE);
        return;
    }

    // grid buttons clicked
    else if (layoutId >= GUI_LAYOUT_INTERACT_GRID &&
    layoutId <= GUI_LAYOUT_INTERACT_GRID + GUI_LAYOUT_INTERACT_GRID_OBJECTS - 1)
    {
        int objId = gli(player, TIS_ + "OBJECT_USED");
        int gridId = layoutId - GUI_LAYOUT_INTERACT_GRID + 1;
        int checkResult = 0;

        // object clicked
        if (objId <= 0)
        {
            sli(player, TIS_ + "OBJECT_USED", gridId);

            // detect current object and actions
            checkResult = CheckScript("vg_s2_tis_check", player, "ACT");

            // player's character lost its target
            if (!checkResult)
            {
                sli(player, TIS_ + "OBJECT_USED", -1, 0, SYS_M_DELETE);
                sli(player, TIS_ + "LOST_OBJECT", TRUE);
            }
        }

        // action clicked
        else
        {
            sli(player, TIS_ + "ACTION_USED", gridId);

            // detect current object and action
            checkResult = CheckScript("vg_s2_tis_check", player, "COA");

            // player's character lost its target / invalid action performed
            if (!checkResult)
            {
                sli(player, TIS_ + "ACTION_USED", -1, 0, SYS_M_DELETE);
                return;
            }

            RunScript("vg_s2_tis_action", player, "ACT");
        }
    }
}


