// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) NPC Sit down script                     |
// | File    || vg_s1_sys_npcsit.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-10-2009                                                     |
// | Updated || 17-10-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pokud se pri zjeveni NPC v okoli 2m nachazi objekt kde se da usadit, sedne
    si na nej
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



void    SitDown()
{
    if  (!GetIsObjectValid(OBJECT_SELF)
    ||  GetIsDead(OBJECT_SELF)
    ||  GetIsInCombat())
    return;

    int     nNth    =   1;
    object  oTemp   =   GetNearestObject(OBJECT_TYPE_PLACEABLE, OBJECT_SELF, nNth);
    float   fDist;
    string  sSitVar;

    while   (GetIsObjectValid(oTemp))
    {
        fDist   =   GetDistanceBetween(OBJECT_SELF, oTemp);

        if  (fDist  >   2.0f)   break;

        sSitVar =   gls(oTemp, "OES_P_USED_0_NAME");

        if  (sSitVar    ==  "SIT")
        {
            if  (!GetIsObjectValid(GetSittingCreature(oTemp)))
            {
                DelayCommand(0.4f, ClearAllActions());
                DelayCommand(0.5f, ActionSit(oTemp));
                break;
            }
        }

        oTemp   =   GetNearestObject(OBJECT_TYPE_PLACEABLE, OBJECT_SELF, ++nNth);
    }
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    DelayCommand(1.0f, AssignCommand(OBJECT_SELF, SitDown()));
}
