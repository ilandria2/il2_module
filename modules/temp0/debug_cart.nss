#include "vg_s0_ets_core"

void main()
{
    object player = OBJECT_SELF;
    object store = glo(player, ETS_ + "TARGET");
    string param = gls(player, "debug_param", -1, SYS_M_DELETE);

    if (gli(player, "ets"))SpawnScriptDebugger();

    if (param == "ACCEPTCART")
        ETS_AcceptCarts(player, store);

    else if (param == "RESETCART")
        ETS_ResetCarts(player, store);
}
