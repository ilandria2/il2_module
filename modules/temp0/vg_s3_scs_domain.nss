// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Domain Powers impact script            |
// | File    || vg_s3_scs_domain.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pusti skript domenniho kouzla
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nSpell  =   GetSpellId();
    int nSource;

    switch  (nSpell)
    {
        case    1637:   nSource =   380;    break;  //  IL2_BATTLE_MASTERY_SPELL
        case    1638:   nSource =   381;    break;  //  IL2_DIVINE_STRENGTH
        case    1639:   nSource =   382;    break;  //  IL2_DIVINE_PROTECTION
        case    1640:   nSource =   383;    break;  //  IL2_NEGATIVE_PLANE_AVATAR
        case    1641:   nSource =   384;    break;  //  IL2_DIVINE_TRICKERY
    }

    if  (GetHasFeat(stoi(Get2DAString("spells", "FeatID", nSource))))
    {
        AssignCommand(OBJECT_SELF, ActionCastSpellAtObject(nSource, OBJECT_SELF, METAMAGIC_ANY, TRUE));
    }

    else
    {
        IncrementRemainingFeatUses(OBJECT_SELF, stoi(Get2DAString("spells", "FeatID", nSpell)));
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "UNKNOWN_DOMAIN_SPELL_&" + itos(nSpell) + "&");
    }
}
