// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Constants                        |
// | File    || vg_s0_pis_const.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Konstanty system PIS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_PIS_NAME                 =   "Player Indicator System";
const string    SYSTEM_PIS_PREFIX               =   "PIS";
const string    SYSTEM_PIS_VERSION              =   "1.0";
const string    SYSTEM_PIS_AUTHOR               =   "VirgiL";
const string    SYSTEM_PIS_DATE                 =   "12-10-2013";
const string    SYSTEM_PIS_UPDATE               =   "12-10-2013";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



//const int       <SHORT>_CONST_<CONSTNAME>        =   1;

