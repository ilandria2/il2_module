// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Start rest script                             |
// | File    || vg_s1_rs_strrest.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-07-2009                                                     |
// | Updated || 16-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Hrac zacne odpocivat
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"
#include    "vg_s0_rs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oRest   =   GetNearestObjectByTag(RS_TAG_RESTAREA, OBJECT_SELF, 1);

    //  na tomhle miste nelze spat
    if  (!GetIsObjectValid(oRest))
    {
        SYS_Info(OBJECT_SELF, SYSTEM_RS_PREFIX, "NO_REST");
        return;
    }

    float   fDist   =   glf(oRest, SYSTEM_RS_PREFIX + "_DIST");

    //  nejblizsi misto odpocinku je prilis daleko
    if  (fDist  >   0.0f
    &&  GetDistanceBetween(oRest, OBJECT_SELF)  >   fDist)
    {
        SYS_Info(OBJECT_SELF, SYSTEM_RS_PREFIX, "NO_REST");
        return;
    }

    int     nValue  =   gli(OBJECT_SELF, SYSTEM_RS_PREFIX + "_FATIGUE");

    //  nedostatek unavy pro zacati spanku
    if  (nValue     <   RS_REST_FATIGUE_NEEDED)
    {
        SYS_Info(OBJECT_SELF, SYSTEM_RS_PREFIX, "NOT_TIRED");
        return;
    }

    //  preruseni rozhovoru
    LPD_EndConversation(OBJECT_SELF, TRUE);

    //  odpocinek
    RS_Rest(OBJECT_SELF);
}
