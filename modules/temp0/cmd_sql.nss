// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_sql.nss                                                    |
// | Author  || VirgiL                                                         |
// | Created || 22-07-2009                                                     |
// | Updated || 22-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SQL"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SQL";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sQuerry =   CCS_GetConvertedString(sLine, 1, FALSE);
    int     bActive =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_SQL");
    string  sMessage;

    //  NwNX2 neni aktivni
    if  (!bActive)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "SQL database not responding";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  nebyl zadan zadnej querry
    if  (sQuerry    ==  "")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "No SQL query entered";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "SQL querry [" + G2 + sQuerry + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);
    SQLExecDirect(sQuerry);

    sMessage    =   "";

    int     nColumn, nRow;
    string  sData;

    while   (SQLFetch() ==  SQL_SUCCESS)
    {
        if  (!nRow)
        sMessage    +=  W2 + "Row " + G1 + "#" + itos(++nRow);    else
        sMessage    +=  W2 + "\nRow " + G1 + "#" + itos(++nRow);
        sData       =   SQLGetData(++nColumn);

        while   (sData  !=  "")
        {
            sMessage    +=  W2 + "\n| " + G1 + "#" + itos(nColumn) + W2 + " [" + G2 + sData + W2 + "]";
            sData       =   SQLGetData(++nColumn);
        }

        nColumn =   0;
    }

    if  (sMessage   !=  "")
    {
        location    loc =   GetLocation(oPC);
        vector      vec =   GetPositionFromLocation(loc);
        object  oExam   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

        sMessage    =   W2 + "Results of SQL statement:\n" + Y2 + sQuerry + "\n" + Y1 + "-------------------------" + sMessage;
        SetDescription(oExam, sMessage);
        AssignCommand(oPC, ClearAllActions(TRUE));
        AssignCommand(oPC, ActionExamine(oExam));
        DelayCommand(2.0f, DestroyObject(oExam));
        //msg(oPC, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
