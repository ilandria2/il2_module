// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Spell failure event                    |
// | File    || vg_s1_scs_spfail.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Aplikuje efekty selhani kouzla
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_scs_const"
#include    "vg_s0_scs_core_o"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nEffect;
    int     nSpell  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_FAILURE_PARAMETERS", -1, SYS_M_DELETE);
    string  sAnim   =   Get2DAString("spells", "ConjAnim", nSpell);

    if  (sAnim  ==  "head") nEffect =   292;
    else                    nEffect =   293;

    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(nEffect), OBJECT_SELF);
    //DrawSpring(DURATION_TYPE_INSTANT, 194 + Random(11), GetLocation(OBJECT_SELF), 2.0f, 0.5f, 0.0f, 3.0f, 0.0f, 20, 2.0f, 1.0f, 0.0f, "z");

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int     nSchool =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_" + Get2DAString("spells", "School", nSpell));
    int     bCircle =   SCS_GetIsInSummoningCircle(OBJECT_SELF, nSchool);

    //  add penalty
    if  (!bCircle)
    {
        int nDice   =   d10();
        int nAmount =   d2();
        int nVFX, nType;

        switch  (nDice)
        {
            case    1:  //  acid
            nType   =   DAMAGE_TYPE_ACID;
            nVFX    =   VFX_IMP_HEAD_ACID;
            break;

            case    2:  //  cold
            nType   =   DAMAGE_TYPE_COLD;
            nVFX    =   VFX_IMP_HEAD_COLD;
            break;

            case    3:  //  divine
            nType   =   DAMAGE_TYPE_DIVINE;
            nVFX    =   VFX_IMP_HEAD_HOLY;
            break;

            case    4:  //  electricity
            nType   =   DAMAGE_TYPE_ELECTRICAL;
            nVFX    =   VFX_IMP_HEAD_ELECTRICITY;
            break;

            case    5:  //  fire
            nType   =   DAMAGE_TYPE_FIRE;
            nVFX    =   VFX_IMP_HEAD_FIRE;
            break;

            case    6:  //  magical
            nType   =   DAMAGE_TYPE_MAGICAL;
            nVFX    =   VFX_IMP_HEAD_MIND;
            break;

            case    7:  //  negative
            nType   =   DAMAGE_TYPE_NEGATIVE;
            nVFX    =   VFX_IMP_HEAD_EVIL;
            break;

            case    8:  //  positive
            nType   =   DAMAGE_TYPE_POSITIVE;
            nVFX    =   VFX_IMP_HEAD_HEAL;
            break;

            case    9:  //  sonic
            nType   =   DAMAGE_TYPE_SONIC;
            nVFX    =   VFX_IMP_HEAD_SONIC;
            break;

            case    10: //  natural
            nType   =   DAMAGE_TYPE_BLUDGEONING;
            nVFX    =   VFX_IMP_HEAD_NATURE;
            break;
        }

        if  (nSchool    ==  7)  //  necromancy
        nAmount *=  2;

        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(nVFX), OBJECT_SELF);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(nAmount, nType, DAMAGE_POWER_ENERGY), OBJECT_SELF);
    }
}
