// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) JumpToSphere script                          |
// | File    || vg_s1_ds_jump.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 01-07-2008                                                     |
// | Updated || 30-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery prenese hrace do sfery smrti
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetLastRespawnButtonPresser();

    //DS_JumpToSphere(oPC, TRUE);
    //DS_JumpToSphere(oPC, FALSE);
    //DelayCommand(4.0f, SYS_Info(oPC, SYSTEM_DS_PREFIX, "JUMP"));
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), oPC);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oPC)), oPC);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_DEATH_L), oPC);
}
