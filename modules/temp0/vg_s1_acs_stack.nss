// merging weighted stackable items when acquired

#include "vg_s0_sys_core_o"
#include "vg_s0_acs_core_c"

void main()
{
    object oCreature = GetModuleItemAcquiredBy();

    if (!GetIsPC(oCreature)) return;
    if (!gli(oCreature, SYSTEM_SYS_PREFIX + "_PLAYER_ID")) return;

    object oAcquired = GetModuleItemAcquired();

    if (GetPlotFlag(oAcquired)) return;
    if (IsSystemItem(oAcquired)) return;
    if (!IsWeightBasedItem(oAcquired)) return;

    // do not merge when flagged for skip
    int skipMerge = gli(oAcquired, SYSTEM_ACS_PREFIX + "_SKIP_MERGE", -1, SYS_M_DELETE);
    if (skipMerge)
        return;

    int stackAcq = 0;
    float modWeight = 0.0;
    int baseType = GetBaseItemType(oAcquired);

    if (baseType != BASE_ITEM_MONEY)
    {
        stackAcq = ACS_GetItemStackSizeForItem(oAcquired);
        modWeight = ACS_GetRecipeModifier(ACS_GetRecipeForResRef(GetResRef(oAcquired)), ACS_MODIFIER_WEIGHT);
    }

    object oItem = GetFirstItemInInventory(oCreature);

    while (GetIsObjectValid(oItem))
    {
        if (oItem != oAcquired && GetTag(oItem) == GetTag(oAcquired))
        {
            // special behavior for money that is stacking until infinity
            if (baseType == BASE_ITEM_MONEY)
            {
                ACS_GiveMoney(oItem, ACS_GetMoney(oAcquired));
                ACS_RefreshNameAndDescription(oItem);
                break;
            }

            else
            {
                if (gli(oCreature, "acs_stack"))SpawnScriptDebugger();

                int stackItem = ACS_GetItemStackSizeForItem(oItem);
                int stackFree = ACS_MAXIMUM_STACK_MODIFIER - stackItem;

                // stack acquired item into existing item
                if (stackFree > 0)
                {
                    stackItem += stackAcq;

                    // over maximum
                    if (stackItem > ACS_MAXIMUM_STACK_MODIFIER)
                    {
                        stackAcq = stackItem - ACS_MAXIMUM_STACK_MODIFIER;
                        stackItem = ACS_MAXIMUM_STACK_MODIFIER;
                    }

                    // up to maximum
                    else
                        stackAcq = 0;

                    IPS_SetItemWeight(oItem, ftoi(modWeight * stackItem));
                    ACS_RefreshNameAndDescription(oItem);

                    // no stack left in the new item - finished
                    if (stackAcq == 0) break;
                }
            }
        }

        oItem = GetNextItemInInventory(oCreature);
    }

    if (stackAcq > 0)
    {
        IPS_SetItemWeight(oAcquired, ftoi(modWeight * stackAcq));
        ACS_RefreshNameAndDescription(oAcquired);
    }

    if (stackAcq == 0)
    {
        SetPlotFlag(oAcquired, FALSE);
        Destroy(oAcquired);
    }
}

