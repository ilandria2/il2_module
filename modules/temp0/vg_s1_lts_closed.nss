// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) OnClosed                       |
// | File    || vg_s1_lts_closed.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosi "OnClosed" pro poklad
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sPost   =   gls(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_SOURCE");

    if  (sPost  ==  "") return;

    object  oPC =   glo(OBJECT_SELF, "TIS_CLOSED_BY");

    oPC =   (!GetIsObjectValid(oPC))    ?   GetLastClosedBy()   :   oPC;

    if  (GetIsDM(oPC)
    ||  (!GetIsPC(oPC)
    &&  !GetIsPC(GetMaster(oPC))))  return;

    if  (gli(oPC, "lts_test"))SpawnScriptDebugger();
    if  (!GetIsObjectValid(GetFirstItemInInventory(OBJECT_SELF)))
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
        int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");

        sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEAT", nHBs);
    }
}
