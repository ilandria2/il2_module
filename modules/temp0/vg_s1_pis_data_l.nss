// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Persistent Data Load script      |
// | File    || vg_s1_pis_data_l.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Nacte perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_pis_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sQuerry, sPC, sName;

    sQuerry =   "SELECT pc2, name " +
                "FROM pis_names " +
                "WHERE pc1 = '" + itos(nID) + "'";

    SQLExecDirect(sQuerry);

    while   (SQLFetch() ==  SQL_SUCCESS)
    {
        sPC     =   SQLGetData(1);
        sName   =   SQLGetData(2);

        sls(OBJECT_SELF, SYSTEM_PIS_PREFIX + "_PC_" + sPC + "_NAME", sName);
    }
}
