// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) PopUpDeathGUIPanel script                    |
// | File    || vg_s1_ds_pop_up.nss                                            |
// | Author  || VirgiL                                                         |
// | Created || 01-07-2008                                                     |
// | Updated || 30-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery otevre GUI panel pri smrti hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_oes_const"
#include    "vg_s0_ds_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+


void    ress(object oPC = OBJECT_SELF)
{
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), oPC);
    SignalEvent(GetModule(), EventUserDefined(OES_EVENT_O_ON_PLAYER_RESURRECT));
}


void    main()
{
    object  oPC     =   GetLastPlayerDied();

    sli(oPC, SYSTEM_DS_PREFIX + "_DIED", TRUE);
    //SavePersistentData(oPC, SYSTEM_DS_PREFIX, "DEAD");
    DelayCommand(3.0f, ress(oPC));
    DelayCommand(3.1f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(ftoi(GetMaxHitPoints(oPC) * 0.5)), oPC));
    DelayCommand(3.2f, AssignCommand(oPC, JumpToLocation(GetLocation(GetWaypointByTag(DS_DIE_WAYPOINT)))));
    DelayCommand(3.3f, AssignCommand(oPC, ActionUnequipItem(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC))));
    DelayCommand(3.31f, AssignCommand(oPC, ActionUnequipItem(GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oPC))));
    DelayCommand(3.5f, AssignCommand(oPC, ActionPlayAnimation(ANIMATION_LOOPING_DEAD_BACK, 0.8f, 10.0f)));


    /*
    object  oCorpse =   glo(oPC, SYSTEM_DS_PREFIX + "_CORPSE");

    if  (!GetIsObjectValid(oCorpse))
    {
        int     bLogin  =   gli(oPC, SYSTEM_DS_PREFIX + "_LOGIN_DEATH", -1, SYS_M_DELETE);
        object  oKiller =   GetLastHostileActor(oPC);
        int     bKiller =   (GetIsPC(oKiller))  ?   2   :   1;
        int     nID     =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        if  (!bLogin)
        {
            string  sSQL;

            sSQL    =   "SELECT " + DS_DB_COLUMN_PLAYER_ID + " " +
                        "FROM " + DS_DB_TABLE_CORPSES + " " +
                        "WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" + itos(nID) + "'";

            SQLExecDirect(sSQL);

            //  update
            if  (SQLFetch() ==  SQL_SUCCESS)
            {
                sSQL    =   "UPDATE " + DS_DB_TABLE_CORPSES + " " +
                            "SET " + DS_DB_COLUMN_KILLER + " = '" + itos(bKiller) + "' " +
                            "WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" + itos(nID) + "'";

                SQLExecDirect(sSQL);
            }

            //  insert
            else
            {
                sSQL    =   "INSERT INTO " + DS_DB_TABLE_CORPSES + " " +
                            "(" +
                                DS_DB_COLUMN_PLAYER_ID + ", " +
                                DS_DB_COLUMN_CORPSE + "," +
                                DS_DB_COLUMN_KILLER +
                            ") VALUES " +
                            "('" +
                                itos(nID) + "'," +
                                "DEFAULT" + ",'" +
                                itos(bKiller) +
                            "')";
            }

            SQLExecDirect(sSQL);
            sli(oPC, SYSTEM_DS_PREFIX + "_KILLER", bKiller);

            string  sLog, sMsg;
            object  oArea   =   GetArea(oPC);

            if  (bKiller    ==  2)
            {
                sLog    =   "[" + SYSTEM_DS_PREFIX + "] PC [" +
                            GetPCPlayerName(oPC) + " (" + itos(nID) + ")] killed by PC [" +
                            GetPCPlayerName(oKiller) + " (" + itos(gli(oKiller, SYSTEM_SYS_PREFIX + "_PLAYER_ID")) + ")] in area [" +
                            GetTag(oArea) + " (" + GetName(oArea) + ")]";

                sMsg    =   W2 + "Hr�� [" + C2 + GetPCPlayerName(oPC) + " (" + itos(nID) + ")" + W2 + "]" +
                            "\nzem�el hr��em [" + C2 + GetPCPlayerName(oKiller) + " (" + itos(gli(oKiller, SYSTEM_SYS_PREFIX + "_PLAYER_ID")) + ")" + W2 + "]" +
                            "\nv oblasti [" + C2 + GetTag(oArea) + " (" + GetName(oArea) + ")" + W2 + "]";
            }

            else
            {
                sLog    =   "[" + SYSTEM_DS_PREFIX + "] PC [" +
                            GetPCPlayerName(oPC) + " (" + itos(nID) + ")] killed by object [" +
                            GetTag(oKiller) + " (" + GetName(oKiller) + ")] in area [" +
                            GetTag(oArea) + " (" + GetName(oArea) + ")]";

                sMsg    =   W2 + "Hr�� [" + C2 + GetPCPlayerName(oPC) + " (" + itos(nID) + ")" + W2 + "]" +
                            "\nzem�el objektem [" + C2 + GetTag(oKiller) + " (" + GetName(oKiller) + ")" + W2 + "]" +
                            "\nv oblasti [" + C2 + GetTag(oArea) + " (" + GetName(oArea) + ")" + W2 + "]";
            }

            object  oPlayer =   GetFirstPC();

            while   (GetIsObjectValid(oPlayer))
            {
                if  (GetIsDM(oPlayer)
                ||  CCS_GetHasAccess(oPlayer, "PM", FALSE))
                msg(oPlayer, sMsg, TRUE, FALSE);

                oPlayer =   GetNextPC();
            }

            logentry(sLog, TRUE);
        }

        DelayCommand(3.0f, PopUpDeathGUIPanel(oPC));
    }

    else
    {
        SYS_Info(oPC, SYSTEM_DS_PREFIX, "ALREADY_DIED");
        DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), oPC));
        DelayCommand(0.2f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oPC)), oPC));
    }
    */
}
