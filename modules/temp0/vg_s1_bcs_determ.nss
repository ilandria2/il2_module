// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) OnPhysicalAttacked event script       |
// | File    || vg_s1_bcs_determ.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-01-2008                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pusti smycku na kontrolu stavu fyzickeho utoku
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"
#include    "vg_s0_xps_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oTarget =   OBJECT_SELF;
    object  oPC     =   GetLastAttacker(oTarget);

    if  (GetObjectType(oTarget) ==  OBJECT_TYPE_PLACEABLE
    ||  GetObjectType(oTarget)  ==  OBJECT_TYPE_DOOR)
    {
        string  sTag    =   (GetIsPC(oTarget))  ?   GetPCPlayerName(oTarget)    :   GetTag(oTarget);
        object  oWeapon =   GetLastWeaponUsed(oPC);
        int     nClass  =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT"), SYSTEM_XPS_PREFIX + "_BASEITEM_CLASS", GetBaseItemType(oWeapon));
        int     nHits   =   gli(oPC, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_CLASS", nClass);

        if  (!nHits)
        sli(oPC, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS", nClass, -2, SYS_M_SET);
        sli(oPC, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_CLASS", 1, nClass, SYS_M_INCREMENT);
    }

    BCS_DetermineCombatRound(oPC, oTarget);
}
