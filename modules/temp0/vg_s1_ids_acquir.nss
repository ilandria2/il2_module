#include "vg_s0_ids_core_c"

void main()
{
    object player = GetModuleItemAcquiredBy();

    if (!GetIsPC(player))
        return;

    if (!gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID"))
        return;

    IDS_CleanInventory(player);
}
