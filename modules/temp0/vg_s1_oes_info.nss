// +---------++----------------------------------------------------------------+
// | Name    || Object Event System (OES) Sys info                             |
// | File    || vg_s1_oes_info                                                 |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 12-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu OES
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_colors"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_OES_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "MESSAGE")
    {
        int     nVarInt     =   stoi(GetNthSubString(sTemp, 1));
        string  sVarString  =   GetNthSubString(sTemp, 2);

        if  (nVarInt    ==  0)  SendMessageToPC(OBJECT_SELF, sVarString);   else
        if  (nVarInt    ==  1)  SpeakString(sVarString);                    else
        {
            sMessage    =   GetIsDawn() ?   "*Venku svit�*"       :
                            GetIsDay()  ?   "*Venku je den*"      :
                            GetIsDusk() ?   "*Venku se stm�v�*"   :
                                            "*Venku je noc*";

            if  (nVarInt    ==  2)  SendMessageToPC(OBJECT_SELF, B2 + sMessage + _C);   else
            if  (nVarInt    ==  3)  SpeakString(sMessage);
        }
        if  (nVarInt    ==  4)  FloatingTextStringOnCreature(sVarString, OBJECT_SELF, FALSE);
    }

    else
    {
        // <CODE>
    }
}

