// +---------++----------------------------------------------------------------+
// | Name    || Generic User Interface (PLE) Constants                         |
// | File    || vg_s0_PLE_const.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+

// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_PLE_NAME                 =   "Placeable Location Editor";
const string    SYSTEM_PLE_PREFIX               =   "PLE";
const string    SYSTEM_PLE_VERSION              =   "1.0";
const string    SYSTEM_PLE_AUTHOR               =   "VirgiL";
const string    SYSTEM_PLE_DATE                 =   "2018-02-14";
const string    SYSTEM_PLE_UPDATE               =   "2018-02-14";
string          PLE                             =   SYSTEM_PLE_PREFIX;
string          PLE_                            =   PLE + "_";
string          _PLE                            =   "_" + PLE;
string          _PLE_                           =   "_" + PLE + "_";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const float PLE_LOOP_SEQUENCE_DELAY     = 3.00f;
const int   PLE_VFX_AXIS_Z              = 8300;
const int   PLE_VFX_AXIS_O              = 8301;
const int   PLE_VFX_AXIS_O_PIXELS       = 260; // it's 280, but 20 is without transparent margin at each side
const int   PLE_VFX_AXIS_OP             = 8302;

const int   PLE_MODE_TERRAIN            = 0;
const int   PLE_MODE_SIDEWAYS           = 1;
const int   PLE_MODE_ORIENTATION        = 2;

const float PLE_INTERACT_DISTANCE       = 3.0f;

const string PLE_TARGET_SCRIPT          = "vg_s1_ple_target";


object __ple;
object __PLE();
object __PLE()
{
    if (!GetIsObjectValid(__ple))
        __ple = GetObjectByTag("SYSTEM" + _PLE_ + "OBJECT");

    return __ple;
}

