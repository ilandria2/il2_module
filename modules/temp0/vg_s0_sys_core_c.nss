// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Core C                                  |
// | File    || vg_s0_sys_core_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 10-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Knihovna systemovych funkci Ilandrie 2 kategorie "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_const"
#include    "vg_s0_sys_core_m"
#include    "vg_s0_sys_core_o"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+

// Creates invisobj useable placeable for player, sets its description to message, shows examine window on it
void ExamineMessage(object player, string message);

// Gets player's script parameter on the prompt script
string GetPromptParameter(object player);

// Gets player's response on the prompt script
string GetPromptAnswer(object player);

// Sets player's response on the prompt script
// if answer = "" then it's cleared
void SetPromptAnswer(object player, string answer);

// Gets the script name for player that will be executed once plaer inputs a value by the next chat message
string GetPromptScript(object player);

// Sets the script name for player that will be executed once player inputs a value by the next chat message
// if script = "" it will be unset
void SetPromptScript(object player, string script, string param = "", string prompt = "");

// Gets the script name set for Target ability for player
string GetTargetScript(object player);

// Gets the parameter value set for Target ability for player
string GetTargetParameters(object player);

// Sets the a parametrized script name for player that will be executed by the Target ability
// if script is "" then it will be unset
void SetTargetParameters(object player, string script, string params = "");

// Stops the current animation played by creature
void StopAnimation(object creature);

// Starts a timing bar (like when resting) for oPC for nMiliSeconds duration and
// executes onComplete when finished
// Additionally calls cancels the existing timing bar before starting the a one
void TimingBarStart(object oPC, int miliSeconds, int bAllowMove = FALSE, string onComplete = "", string onCancel = "");

// Cancels the current timing bar and if set then executes it's associated cancel-script
void TimingBarCancel(object oPC);

// +---------++----------------------------------------------------------------+
// | Name    || SwitchRunState                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zapne / vypne behani pro bytost oCreature
//
// -----------------------------------------------------------------------------
void    SwitchRunState(object oCreature, int bState = TRUE, int nPercentChange = 99);



// +---------++----------------------------------------------------------------+
// | Name    || SetCustomActionMode                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Aktivuje vlastni mod nMode pro bytost oCreature
//
// -----------------------------------------------------------------------------
void    SetCustomActionMode(object oCreature, int nMode = SYS_MODE_WALK, int bSecondary = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || DelayActionMode                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Delays the nMode action mode for later determination of Secondary effect
//
// -----------------------------------------------------------------------------
void DelayActionMode(object oPC, int nMode);



// +---------++----------------------------------------------------------------+
// | Name    || DelayActionMode                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Delays the sScript action mode for later determination of Secondary effect
//
// -----------------------------------------------------------------------------
void DelayAbility(object oPC, string sScript);



// +---------++----------------------------------------------------------------+
// | Name    || DetermineSecondaryAbility                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Gets number of attempts for a script execution and determines whether the
//  Secondary effect should be performed instead
//
// -----------------------------------------------------------------------------
void DetermineSecondaryAbility(object oPC, string sScript);



// +---------++----------------------------------------------------------------+
// | Name    || DetermineSecondaryActionMode                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Gets number of attempts for action modes use and determines whether the
//  Secondary effect should be performed instead
//
// -----------------------------------------------------------------------------
void DetermineSecondaryActionMode(object oPC, int nMode);



// +---------++----------------------------------------------------------------+
// | Name    || ActionEffect                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Aplikuje nebo odebere definovany efekt sEffect na/z oTarget
//
// -----------------------------------------------------------------------------
void    ActionEffect(int nDurationType, string sEffect, location lTarget, object oTarget, int bApply = TRUE, int nSubType = SUBTYPE_MAGICAL, float fDuration = 0.0f);



// +---------++----------------------------------------------------------------+
// | Name    || ActionGold                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prida / odebere nCount zlatych bytosti oTarget
//
// -----------------------------------------------------------------------------
void    ActionGold(int nCount, object oTarget, int bType = SYS_M_INCREMENT);



// +---------++----------------------------------------------------------------+
// | Name    || ActionItem                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prida / odebere (pocet) predmetu definovanych nDefType a sDefinition u
//  oTarget
//
// -----------------------------------------------------------------------------
void    ActionItem(int nCount, int nDefType, string sDefinition, int nSlot, object oTarget, int bCreate = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || ActionVariable                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Smaze / nastavi promennou sName typu nType u objektu oTarget na sValue
//
// -----------------------------------------------------------------------------
void    ActionVariable(int nType, object oObject, string sName, string sValue, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || ActionJump                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Premistni bytost oObject do lokace lTarget
//
// -----------------------------------------------------------------------------
void    ActionJump(object oObject, location lTarget);



// +---------++----------------------------------------------------------------+
// | Name    || CheckAssociate                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 pokud bytost oTarget splna podminky pro meni nCount pritelicku
//  typu nType
//
// -----------------------------------------------------------------------------
int     CheckAssociate(int nCount, int nType, object oTarget, int bType = SYS_M_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckEffect                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 pokud bytost oTarget je ovlyvnena efektem typu nType od typu
//  puvodce efektu nCreator
//
// -----------------------------------------------------------------------------
int     CheckEffect(int nType, int nCreator, object oTarget, int bType = SYS_M_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckFeat                                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 pokud bytost oTarget ma / neba odbornost nFeat
//
// -----------------------------------------------------------------------------
int     CheckFeat(int nFeat, object oTarget, int bType = SYS_M_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckGold                                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 pokud bytost oTarget ma / nema nGold zlatych
//
// -----------------------------------------------------------------------------
int     CheckGold(int nGold, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckItem                                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkontroluje vyskyt definovaneho predmetu nDefType s parametrem sDefinition
//  u inventare oSource
//
// -----------------------------------------------------------------------------
int     CheckItem(int nCount, int nDefType, string sDefinition, int nSlot, object oSource = OBJECT_SELF, int bType = SYS_M_HIGHER_OR_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckJournal                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 kdyz dennik sJournal je v urovni nJournal u bytosti oTarget
//
// -----------------------------------------------------------------------------
int     CheckJournal(int nJournal, string sJournal, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckParty                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkontroluje pocet hracu v druzine hrace oTarget s povolanim nClass
//
// -----------------------------------------------------------------------------
int     CheckParty(int nCount, int nClass, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckHitPoints                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkontroluje jestli bytost oTarget ma dostatek zivotu
//
// -----------------------------------------------------------------------------
int     CheckHitPoints(int nPoints, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckScript                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkontroluje podminky definovane v skriptu sScript
//
// -----------------------------------------------------------------------------
int     CheckScript(string sScript, object oSource, string sParam = "");

// executes script on source with parameter
void RunScript(string script, object source, string param = "");



// +---------++----------------------------------------------------------------+
// | Name    || CheckThrow                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Provede hod XkY + <nCategory.nType> vs DC pro bytost oCreature
//
// -----------------------------------------------------------------------------
int     CheckThrow(object oCreature, int nCategory, int nType, int nXky, int nxkY, int nDifficulty, int bMessage = TRUE, int bType = SYS_M_HIGHER_OR_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || CheckVariable                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Overi hodnotu lokalni promenne sName typu nType u objektu oObject
//
// -----------------------------------------------------------------------------
int     CheckVariable(int nType, object oObject, string sName, string sValue, int nRow = -1, int bType = SYS_M_EQUAL);



// +---------------------------------------------------------------------------+
// |                        I M P L E M E N T A T I O N                        |
// +---------------------------------------------------------------------------+

void ExamineMessage(object player, string message)
{
    location loc = GetLocation(player);
    vector vec = GetPositionFromLocation(loc);
    object oExam = CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(player), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

    SetDescription(oExam, message);
    AssignCommand(player, ClearAllActions(TRUE));
    AssignCommand(player, ActionExamine(oExam));
    DelayCommand(2.0f, DestroyObject(oExam));
}

string GetPromptParameter(object player)
{
    return gls(player, SYSTEM_SYS_PREFIX + "_PROMPT_PARAM", -1, SYS_M_DELETE);
}

string GetPromptAnswer(object player)
{
    return gls(player, SYSTEM_SYS_PREFIX + "_PROMPT_RESPONSE", -1, SYS_M_DELETE);
}

void SetPromptAnswer(object player, string answer)
{
    if (answer == "")
        sls(player, SYSTEM_SYS_PREFIX + "_PROMPT_RESPONSE", "", -1, SYS_M_DELETE);
    else
        sls(player, SYSTEM_SYS_PREFIX + "_PROMPT_RESPONSE", answer);
}

string GetPromptScript(object player)
{
    return gls(player, SYSTEM_SYS_PREFIX + "_PROMPT_SCRIPT");
}

void SetPromptScript(object player, string script, string param = "", string prompt = "")
{
    if (script == "")
    {
        sls(player, SYSTEM_SYS_PREFIX + "_PROMPT_SCRIPT", "", -1, SYS_M_DELETE);
        sls(player, SYSTEM_SYS_PREFIX + "_PROMPT_PARAM", "", -1, SYS_M_DELETE);
    }
    else
    {
        sls(player, SYSTEM_SYS_PREFIX + "_PROMPT_SCRIPT", script);
        sls(player, SYSTEM_SYS_PREFIX + "_PROMPT_PARAM", param);
        SYS_Info(player, SYSTEM_SYS_PREFIX, "PROMPT_&" + prompt + "&");
    }
}

string GetTargetScript(object player)
{
    return GetNthSubString(gls(player, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS"), 1);
}

string GetTargetParameters(object player)
{
    return GetNthSubString(gls(player, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS"), 2);
}

void SetTargetParameters(object player, string script, string params = "")
{
    if (script == "")
        sls(player, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "", -1, SYS_M_DELETE);
    else
        sls(player, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "&" + script + "&&" + params + "&");
}



void StopAnimation(object creature)
{
    AssignCommand(creature, PlayAnimation(0, 1.0f, -1.0f));
}

// +---------------------------------------------------------------------------+
// |                         TimingBar helper functions                        |
// +---------------------------------------------------------------------------+



void TimingBarGuiShow(object oPC, int miliSeconds)
{
    sls(oPC, "NWNX!FUNCSEXT!STARTTIMINGBAR", itos(miliSeconds)+"    ");
    sls(oPC, "NWNX!FUNCSEXT!STARTTIMINGBAR", "", 0, SYS_M_DELETE);
}

void TimingBarGuiHide(object oPC)
{
    SetLocalString(oPC, "NWNX!FUNCSEXT!STOPTIMINGBAR", "    ");
    DeleteLocalString(oPC, "NWNX!FUNCSEXT!STOPTIMINGBAR");
}

void TimingBarComplete(object oPC, int ID)
{
    // ignore the complete event of a previously cancelled timing bar
    if (gli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID") != ID) return;
    TimingBarGuiHide(oPC);

    string onComplete = gls(oPC, SYSTEM_SYS_PREFIX + "_TBAR_F", -1, SYS_M_DELETE);
    if (onComplete != "")
        ExecuteScript(onComplete, oPC);

    sls(oPC, SYSTEM_SYS_PREFIX + "_TBAR_C", "", -1, SYS_M_DELETE);
    sli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID", 1, -1, SYS_M_INCREMENT);
    sli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_MOVE", FALSE, ID, SYS_M_DELETE);
    sll(oPC, SYSTEM_SYS_PREFIX + "_TBAR_LOC", li(), -1, SYS_M_DELETE);
}

void TimingBarMoveCheckLoop(object oPC, int ID)
{
    // stop old sequence because new will start in parallel
    int IDcur = gli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID");
    if (IDcur > ID)
        return;

    location lSeq = gll(oPC, SYSTEM_SYS_PREFIX + "_TBAR_LOC");
    location lPC = GetLocation(oPC);

    vector posSeq = GetPositionFromLocation(lSeq);
    vector posPc = GetPositionFromLocation(lPC);

    // character is dying or was moved
    if  (GetCurrentHitPoints(oPC) < 0 ||
    (GetIsObjectValid(GetAreaFromLocation(lSeq)) && lSeq != lPC && posSeq != posPc))
    {
        TimingBarCancel(oPC);
        return;
    }

    DelayCommand(1.0f, TimingBarMoveCheckLoop(oPC, ID));
}

void TimingBarMoveCheck(object oPC)
{
    // ignore an attempt to start the check loop for existing ID
    int ID = gli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID");
    if (gli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_MOVE", ID))
        return;

    sll(oPC, SYSTEM_SYS_PREFIX + "_TBAR_LOC", GetLocation(oPC));
    sli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_MOVE", TRUE, ID);
    DelayCommand(1.0f, TimingBarMoveCheckLoop(oPC, ID));
}


// +---------------------------------------------------------------------------+
// |                                TimingBarStart                             |
// +---------------------------------------------------------------------------+

void TimingBarStart(object oPC, int miliSeconds, int bAllowMove = FALSE, string onComplete = "", string onCancel = "")
{
    TimingBarCancel(oPC);
    TimingBarGuiShow(oPC, miliSeconds);
    int ID = gli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID");

    if (onComplete != "")
        sls(oPC, SYSTEM_SYS_PREFIX + "_TBAR_F", onComplete);

    if (onCancel != "")
        sls(oPC, SYSTEM_SYS_PREFIX + "_TBAR_C", onCancel);

    if (!bAllowMove)
        TimingBarMoveCheck(oPC);

    DelayCommand(itof(miliSeconds) / 1000, TimingBarComplete(oPC, ID));
}



// +---------------------------------------------------------------------------+
// |                                TimingBarCancel                            |
// +---------------------------------------------------------------------------+

void TimingBarCancel(object oPC)
{
    TimingBarGuiHide(oPC);

    string onCancel = gls(oPC, SYSTEM_SYS_PREFIX + "_TBAR_C", -1, SYS_M_DELETE);
    if (onCancel != "")
        ExecuteScript(onCancel, oPC);

    int ID = gli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID");

    sls(oPC, SYSTEM_SYS_PREFIX + "_TBAR_F", "", -1, SYS_M_DELETE);
    sli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_ID", 1, -1, SYS_M_INCREMENT);
    sli(oPC, SYSTEM_SYS_PREFIX + "_TBAR_MOVE", FALSE, ID, SYS_M_DELETE);
    sll(oPC, SYSTEM_SYS_PREFIX + "_TBAR_LOC", li(), -1, SYS_M_DELETE);
}



// +---------------------------------------------------------------------------+
// |                               SwitchRunState                              |
// +---------------------------------------------------------------------------+



void    SwitchRunState(object oCreature, int bState = TRUE, int nPercentChange = 99)
{
    effect  eEffect =   GetFirstEffect(oCreature);
    int     nType;

    while   (GetIsEffectValid(eEffect))
    {
        nType   =   GetEffectType(eEffect);

        if  (nType  ==  EFFECT_TYPE_SLOW
        ||  nType   ==  EFFECT_TYPE_MOVEMENT_SPEED_INCREASE)
        {
            if  (GetEffectSubType(eEffect)  ==  SUBTYPE_SUPERNATURAL)
            RemoveEffect(oCreature, eEffect);
        }

        eEffect =   GetNextEffect(oCreature);
    }

    if  (!bState)
    {
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectSlow()), oCreature);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectMovementSpeedIncrease(nPercentChange)), oCreature);
    }

    // run
    else if (bState == 1)
    {
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectMovementSpeedIncrease(SYS_MOVEMENT_BONUS_RUN)), oCreature);
    }

    // sprint
    else if (bState == 2)
    {
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectMovementSpeedIncrease(SYS_MOVEMENT_BONUS_SPRINT)), oCreature);
    }
}



// +---------------------------------------------------------------------------+
// |                            SetCustomActionMode                            |
// +---------------------------------------------------------------------------+



void    SetCustomActionMode(object oCreature, int nMode = SYS_MODE_WALK, int bSecondary = FALSE)
{
    if  (!GetIsObjectValid(oCreature))  return;

    int     nCur    =   gli(oCreature, SYSTEM_SYS_PREFIX + "_MODE");
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    string  sScript;

    //  Preruseni aktualniho
    if  (nCur   >   SYS_MODE_WALK)
    {
        sScript =   gls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_SCRIPT", nCur);

        sli(oCreature, SYSTEM_SYS_PREFIX + "_MODE", -nCur);
        ExecuteScript(SYS_MODE_SCRIPT_PREFIX + sScript, oCreature);
        sli(oCreature, SYSTEM_SYS_PREFIX + "_MODE", nCur);
        sli(oCreature, SYSTEM_SYS_PREFIX + "_MODE_SECONDARY", -1, -1, SYS_M_DELETE);
    }

    //  Aktivace novyho
    nMode   =   (nMode  ==  nCur)   ?   SYS_MODE_WALK   :   nMode;
    sScript =   gls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_SCRIPT", nMode);

    sli(oCreature, SYSTEM_SYS_PREFIX + "_USE_SECONDARY", bSecondary);
    ExecuteScript(SYS_MODE_SCRIPT_PREFIX + sScript, oCreature);
}



// +---------------------------------------------------------------------------+
// |                               DelayAbility                                |
// +---------------------------------------------------------------------------+



void DelayAbility(object oPC, string sScript)
{
    //  track attempts within a short period of time
    int nLoop = gli(oPC, SYSTEM_SYS_PREFIX + "_SCRIPT_IN_LOOP");

    sli(oPC, SYSTEM_SYS_PREFIX + "_SCRIPT_IN_LOOP", ++nLoop);

    if (nLoop == 1)
        DelayCommand(SYS_MODE_LOOP_DELAY, DetermineSecondaryAbility(oPC, sScript));
}



// +---------------------------------------------------------------------------+
// |                               DelayActionMode                             |
// +---------------------------------------------------------------------------+



void DelayActionMode(object oPC, int nMode)
{
    //  track attempts within a short period of time
    int nLoop = gli(oPC, SYSTEM_SYS_PREFIX + "_MODE_IN_LOOP");

    sli(oPC, SYSTEM_SYS_PREFIX + "_MODE_IN_LOOP", ++nLoop);

    if (nLoop == 1)
        DelayCommand(SYS_MODE_LOOP_DELAY, DetermineSecondaryActionMode(oPC, nMode));
}



// +---------------------------------------------------------------------------+
// |                         DetermineSecondaryAbility                         |
// +---------------------------------------------------------------------------+



void DetermineSecondaryAbility(object oPC, string sScript)
{
    //if (gli(oPC, "2ndary"))SpawnScriptDebugger();
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int nLoop = gli(oPC, SYSTEM_SYS_PREFIX + "_SCRIPT_IN_LOOP", -1, SYS_M_DELETE);

    sli(oPC, SYSTEM_SYS_PREFIX + "_USE_SECONDARY", nLoop > 1);
    ExecuteScript(sScript, oPC);
}



// +---------------------------------------------------------------------------+
// |                         DetermineSecondaryActionMode                      |
// +---------------------------------------------------------------------------+



void DetermineSecondaryActionMode(object oPC, int nMode)
{
    //if (gli(oPC, "2ndary"))SpawnScriptDebugger();
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int nLoop = gli(oPC, SYSTEM_SYS_PREFIX + "_MODE_IN_LOOP", -1, SYS_M_DELETE);

    SetCustomActionMode(oPC, nMode, nLoop > 1);
}



// +---------------------------------------------------------------------------+
// |                               ActionEffect                                |
// +---------------------------------------------------------------------------+



void    ActionEffect(int nDurationType, string sEffect, location lTarget, object oTarget, int bApply = TRUE, int nSubType = SUBTYPE_MAGICAL, float fDuration = 0.0f)
{
    effect  eEffect =   GetDefinedEffect(sEffect);

    switch  (nSubType)
    {
        case    SUBTYPE_EXTRAORDINARY:  eEffect = ExtraordinaryEffect(eEffect); break;
        case    SUBTYPE_SUPERNATURAL:   eEffect = SupernaturalEffect(eEffect);  break;
    }

    if  (bApply ==  FALSE)
    {
        effect  eLoop   =   GetFirstEffect(oTarget);
        int     nType   =   GetEffectType(eLoop);

        while   (GetIsEffectValid(eLoop)    ==  TRUE)
        {
            if  (eLoop  ==  eEffect)
            {
                RemoveEffect(oTarget, eLoop);
            }

            eLoop   =   GetNextEffect(oTarget);
        }
    }

    else
    {
        if  (!GetIsObjectValid(oTarget))    ApplyEffectAtLocation(nDurationType, eEffect, lTarget, fDuration);
        else                                ApplyEffectToObject(nDurationType, eEffect, oTarget, fDuration);
    }
}



// +---------------------------------------------------------------------------+
// |                               ActionGold                                  |
// +---------------------------------------------------------------------------+



void    ActionGold(int nCount, object oTarget, int bType = SYS_M_INCREMENT)
{
    if  (!GetIsObjectValid(oTarget))    return;

    int     nGold, nNew;

    nGold   =   GetGold(oTarget);
    nNew    =   ei(nGold, nCount, bType);
    nNew    =   nNew    <   0   ?   0   :   nNew;

    if  (nNew   >   nGold)  GiveGoldToCreature(oTarget, nNew - nGold);  else
    if  (nNew   <   nGold)  TakeGoldFromCreature(nGold - nNew, oTarget, TRUE);
}



// +---------------------------------------------------------------------------+
// |                               ActionItem                                  |
// +---------------------------------------------------------------------------+



void    ActionItem(int nCount, int nDefType, string sDefinition, int nSlot, object oTarget, int bCreate = TRUE)
{
    int     nItemType, i, bItem;
    object  oItem, oNew;
    string  sValue;

    if  (bCreate    ==  TRUE)
    {
        oNew    =   CreateItemOnObject(sDefinition, oTarget, nCount, "");

        SetIdentified(oNew, TRUE);
    }

    if  (nSlot  <   NUM_INVENTORY_SLOTS)
    {
        if  (bCreate    ==  FALSE)
        {
            oItem   =   GetItemInSlot(nSlot, oTarget);
            bItem   =   CompareItem(nDefType, sDefinition, oItem);

            if  (bItem  ==  TRUE)
            {
                SetPlotFlag(oItem, FALSE);
                Destroy(oItem);
            }
        }

        else
        {
            AssignCommand(oTarget, ActionEquipItem(oItem, nSlot));
        }
    }

    else

    if  (nSlot  ==  SYS_O_ITEM_SLOT_INVENTORY)
    {
        if  (bCreate    ==  FALSE)
        {
            oItem   =   GetFirstItemInInventory(oTarget);

            while   (oItem  !=  OBJECT_INVALID)
            {
                bItem   =   CompareItem(nDefType, sDefinition, oItem);

                if  (bItem  ==  TRUE)
                {
                    SetPlotFlag(oItem, FALSE);
                    Destroy(oItem);
                }

                oItem   =   GetNextItemInInventory(oTarget);
            }
        }

        else
        {
            //  nic sa nestane, protoze byli definovany vsechny predmety v
            //  inventari
        }
    }

    else

    if  (nSlot  ==  SYS_O_ITEM_SLOT_EQUIPPED)
    {
        if  (bCreate    ==  FALSE)
        {
            i       =   0;

            while   (i  <   NUM_INVENTORY_SLOTS)
            {
                oItem   =   GetItemInSlot(i, oTarget);
                bItem   =   CompareItem(nDefType, sDefinition, oItem);

                if  (bItem  ==  TRUE)
                {
                    SetPlotFlag(oItem, FALSE);
                    Destroy(oItem);
                }

                i++;
            }
        }

        else
        {
            //  nic sa nestane, protoze byli definovany vsechny vybavene
            //  predmety v inventari
        }
    }
}



// +---------------------------------------------------------------------------+
// |                             ActionVariable                                |
// +---------------------------------------------------------------------------+



void    ActionVariable(int nType, object oObject, string sName, string sValue, int nRow = -1, int bType = SYS_M_SET)
{
    switch  (nType)
    {
        case    SYS_V_STRING:   sls(oObject, sName, sValue, nRow, bType);   break;
        case    SYS_V_INTEGER:  sli(oObject, sName, StringToInt(sValue), nRow, bType);      break;
        case    SYS_V_FLOAT:    slf(oObject, sName, StringToFloat(sValue), nRow, bType);    break;
        case    SYS_V_LOCATION: sll(oObject, sName, GetLocalLocation(oObject, sValue), nRow, bType);    break;
        case    SYS_V_OBJECT:   slo(oObject, sName, GetLocalObject(oObject, sValue), nRow, bType);      break;
    }
}



// +---------------------------------------------------------------------------+
// |                              CheckAssociate                               |
// +---------------------------------------------------------------------------+



int     CheckAssociate(int nCount, int nType, object oTarget, int bType = SYS_M_EQUAL)
{
    int     bResult, n;
    object  oAssociate;

    oAssociate  =   GetAssociate(nType, oTarget, 1);

    while   (oAssociate !=  OBJECT_INVALID)
    {
        n++;
        oAssociate  =   GetAssociate(nType, oTarget, n);
    }

    bResult =   ci(n, nCount, bType);

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                               CheckEffect                                 |
// +---------------------------------------------------------------------------+



int     CheckEffect(int nType, int nCreator, object oTarget, int bType = SYS_M_EQUAL)
{
    effect  eEffect;
    int     bValid, bDead, bCompare, bCreator, bResult;
    int     nEffect;
    object  oCreator;

    eEffect     =   GetFirstEffect(oTarget);
    nEffect     =   GetEffectType(eEffect);
    oCreator    =   GetEffectCreator(eEffect);

    if  (nType  ==  EFFECT_TYPE_INVALIDEFFECT)
    bCompare    =   TRUE;   else
    bCompare    =   ci(nEffect, nType, bType);

    bResult     =   bCompare  &&  bCreator;
    bValid      =   GetIsEffectValid(eEffect);
    bDead       =   GetIsDead(oCreator);

    while
    (
        bValid  !=  FALSE   &&
        bResult ==  FALSE
    )
    {
        switch  (nCreator)
        {
            case    SYS_C_EFFECT_CREATOR_ANY:           bCreator    =   GetIsObjectValid(oCreator); break;
            case    SYS_C_EFFECT_CREATOR_SELF:          bCreator    =   GetIsObjectValid(oCreator)  &&  oCreator    ==  oTarget;    break;
            case    SYS_C_EFFECT_CREATOR_NOT_SELF:      bCreator    =   GetIsObjectValid(oCreator)  &&  oCreator    !=  oTarget;    break;
            case    SYS_C_EFFECT_CREATOR_IS_ALIVE:      bCreator    =   GetIsObjectValid(oCreator)  &&  !bDead; break;
            case    SYS_C_EFFECT_CREATOR_IS_NOT_ALIVE:  bCreator    =   GetIsObjectValid(oCreator)  &&  bDead;  break;
            case    SYS_C_EFFECT_CREATOR_IS_INVALID:    bCreator    =   !GetIsObjectValid(oCreator);    break;
        }

        eEffect     =   GetNextEffect(oTarget);
        nEffect     =   GetEffectType(eEffect);
        oCreator    =   GetEffectCreator(eEffect);

        if  (nType  ==  EFFECT_TYPE_INVALIDEFFECT)
        bCompare    =   TRUE;   else
        bCompare    =   ci(nEffect, nType, bType);

        bResult     =   bCompare  &&  bCreator;
        bValid      =   GetIsEffectValid(eEffect);
        bDead       =   GetIsDead(oCreator);
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                               CheckFeat                                   |
// +---------------------------------------------------------------------------+



int     CheckFeat(int nFeat, object oTarget, int bType = SYS_M_EQUAL)
{
    int     bResult, bFeat;

    bFeat   =   GetCreatureHasTalent(TalentFeat(nFeat), oTarget);
    bResult =   ci(TRUE, bFeat, bType);

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                                CheckGold                                  |
// +---------------------------------------------------------------------------+



int     CheckGold(int nGold, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL)
{
    int     bResult, nCount;

    nCount  =   GetGold(oTarget);
    bResult =   ci(nCount, nGold, bType);

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                                 CheckItem                                 |
// +---------------------------------------------------------------------------+



int     CheckItem(int nCount, int nDefType, string sDefinition, int nSlot, object oSource = OBJECT_SELF, int bType = SYS_M_HIGHER_OR_EQUAL)
{
     int    bResult, nValue;

     nValue     =   GetItemCount(nDefType, sDefinition, nSlot, oSource);
     bResult    =   ci(nValue, nCount, bType);

     return bResult;
}



// +---------------------------------------------------------------------------+
// |                               CheckJournal                                |
// +---------------------------------------------------------------------------+



int     CheckJournal(int nJournal, string sJournal, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL)
{
    int     bResult, nJrn;

    nJrn    =   GetLocalInt(oTarget, SYS_VAR_JOURNAL_PREFIX + sJournal);
    bResult =   ci(nJrn, nJournal, bType);

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                                CheckParty                                 |
// +---------------------------------------------------------------------------+



int     CheckParty(int nCount, int nClass, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL)
{
    int     bResult, nMembers, nType;
    object  oMember;

    oMember =   GetFirstFactionMember(oTarget, TRUE);

    while   (oMember   !=  OBJECT_INVALID)
    {
        if  (nClass !=  100)
        {
            if  (GetLevelByClass(nClass, oMember) !=  0)
            {
                nMembers++;
            }
        }

        else
        {
            nMembers++;
        }

        oMember =   GetNextFactionMember(oTarget, TRUE);
    }

    bResult =   ci(nMembers, nCount, bType);

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                            CheckHitPoints                                 |
// +---------------------------------------------------------------------------+



int     CheckHitPoints(int nPoints, object oTarget, int bType = SYS_M_HIGHER_OR_EQUAL)
{
    int     bResult;

    bResult =   ci(GetCurrentHitPoints(oTarget), nPoints, bType);

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                              CheckScript                                  |
// +---------------------------------------------------------------------------+



int     CheckScript(string sScript, object oSource, string sParam = "")
{
    if  (sScript    ==  "") return  TRUE;

    int     bResult;

    DeleteLocalInt(oSource, SYS_VAR_RETURN);
    SetLocalString(oSource, SYS_VAR_PARAMETER, sParam);
    ExecuteScript(sScript, oSource);

    bResult =   GetLocalInt(oSource, SYS_VAR_RETURN);

    DeleteLocalInt(oSource, SYS_VAR_RETURN);

    return  bResult;
}



void RunScript(string script, object source, string param = "")
{
    SetLocalString(source, SYS_VAR_PARAMETER, param);
    ExecuteScript(script, source);
}


// +---------------------------------------------------------------------------+
// |                              CheckThrow                                   |
// +---------------------------------------------------------------------------+



int     CheckThrow(object oCreature, int nCategory, int nType, int nXky, int nxkY, int nDifficulty, int bMessage = TRUE, int bType = SYS_M_HIGHER_OR_EQUAL)
{
    int     nRow, nValue, nDice, nSum, bImpossible, bResult;

    if
    (
        nXky    !=  0   &&
        nxkY    !=  0
    )
    {
        nDice   =   nXky * (1 + Random(nxkY));
    }

    switch  (nCategory)
    {
        case    SYS_M_THROW_CATEGORY_DICE:          nValue  =   0;  break;
        case    SYS_M_THROW_CATEGORY_SKILL:         nValue  =   GetSkillRank(nType, oCreature, FALSE);  break;
        case    SYS_M_THROW_CATEGORY_ABILITY:       nValue  =   GetAbilityScore(oCreature, nType, FALSE);   break;
        case    SYS_M_THROW_CATEGORY_BAB:           nValue  =   GetBaseAttackBonus(oCreature);  break;
        case    SYS_M_THROW_CATEGORY_AC:            nValue  =   GetAC(oCreature);   break;
        case    SYS_M_THROW_CATEGORY_SR:            nValue  =   GetSpellResistance(oCreature);  break;
        case    SYS_M_THROW_CATEGORY_SAVING_THROW:
            if  (nType  ==  0){nValue   =   GetFortitudeSavingThrow(oCreature);}    else
            if  (nType  ==  1){nValue   =   GetReflexSavingThrow(oCreature);}       else
            if  (nType  ==  2){nValue   =   GetWillSavingThrow(oCreature);}
        break;
    }

    nSum        =   nDice + nValue;
    bResult     =   ci(nSum, nDifficulty, bType);

    if  (nDice  ==  1){bResult  =   FALSE;}

    //  Posle spravu
    if  (bMessage   ==  TRUE)
    {
    //  nDifficulty, bResult, bImpossible, nDice, nValue
        string  sParameters;

        bImpossible =   ci(nXky * nxkY + nValue, nDifficulty, SYS_M_LOWER);
        sParameters =   "CHECK_THROW_" +
                        "&" + IntToString(nDice) + "&_" +
                        "&" + IntToString(nValue) + "&_" +
                        "&" + IntToString(nDifficulty) + "&_" +
                        "&" + IntToString(bResult) + "&_" +
                        "&" + IntToString(bImpossible) + "&_" +
                        "&" + IntToString(nCategory + 1) + "&_" +
                        "&" + IntToString(nType + 1) + "&";

        SYS_Info(oCreature, SYSTEM_SYS_PREFIX, sParameters);
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                              CheckVariable                                |
// +---------------------------------------------------------------------------+



int     CheckVariable(int nType, object oObject, string sName, string sValue, int nRow = -1, int bType = SYS_M_EQUAL)
{
    int bResult;

    switch  (nType)
    {
        case    SYS_V_INTEGER:  bResult =   ci(gli(oObject, sName, nRow), StringToInt(sValue), bType);      break;
        case    SYS_V_FLOAT:    bResult =   cf(glf(oObject, sName, nRow), StringToFloat(sValue), bType);    break;
        case    SYS_V_STRING:
            bResult =   TestStringAgainstPattern(sValue, gls(oObject, sName, nRow));

            if  (bType  !=  SYS_M_EQUAL)
            bResult =   !bResult;
        break;
        case    SYS_V_LOCATION: bResult =   gll(oObject, sName, nRow)   ==  gll(oObject, sValue);   break;
        case    SYS_V_OBJECT:   bResult =   glo(oObject, sName, nRow)   ==  glo(oObject, sValue);   break;
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                                ActionJump                                 |
// +---------------------------------------------------------------------------+



void    ActionJump(object oObject, location lTarget)
{
    AssignCommand(oObject, ClearAllActions(TRUE));
    AssignCommand(oObject, JumpToLocation(lTarget));
}
