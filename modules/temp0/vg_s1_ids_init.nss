// +---------++----------------------------------------------------------------+
// | Name    || Item Durability System (IDS) System init                       |
// | File    || vg_s1_ids_init.nss                                             |
// +---------++----------------------------------------------------------------+
// Initializes the IDS system
// -----------------------------------------------------------------------------

#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_sys_core_d"
#include "vg_s0_oes_const"
#include "vg_s0_ids_const"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void WaitForLoadData()
{
    while (gli(__IDS(), IDS_ + "LOADING_DATA"))
    {
        DelayCommand(0.1, WaitForLoadData());
        return;
    }

    sli(__IDS(), "SYSTEM" + _IDS_ + "STATE", TRUE);
    logentry("[" + IDS + "] init done", TRUE);
}

void main()
{
    string  sName       =   SYSTEM_IDS_NAME;
    string  sVersion    =   SYSTEM_IDS_VERSION;
    string  sAuthor     =   SYSTEM_IDS_AUTHOR;
    string  sDate       =   SYSTEM_IDS_DATE;
    string  sUpDate     =   SYSTEM_IDS_UPDATE;

    sli(__IDS(), "SYSTEM" + _IDS_ + "STATE", 2);
    logentry("[" + IDS + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", IDS, -2);
    sli(__IDS(), "SYSTEM" + _IDS_ + "ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(__IDS(), "SYSTEM" + _IDS_ + "NAME", sName);
    sls(__IDS(), "SYSTEM" + _IDS_ + "PREFIX", IDS);
    sls(__IDS(), "SYSTEM" + _IDS_ + "VERSION", sVersion);
    sls(__IDS(), "SYSTEM" + _IDS_ + "AUTHOR", sAuthor);
    sls(__IDS(), "SYSTEM" + _IDS_ + "DATE", sDate);
    sls(__IDS(), "SYSTEM" + _IDS_ + "UPDATE", sUpDate);

    sls(__IDS(), IDS_ + "NCS_SYSINFO_RESREF", "vg_s1_ids_info");
    sls(__IDS(), IDS_ + "NCS_DB_CHECK_RESREF", "vg_s1_ids_data_c");
    sls(__IDS(), IDS_ + "NCS_DB_SAVE_RESREF", "vg_s1_ids_data_s");
    sls(__IDS(), IDS_ + "NCS_DB_LOAD_RESREF", "vg_s1_ids_data_l");
//    sli(__IDS(), IDS_ + "DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(__IDS(), IDS_ + "DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(__IDS(), IDS_ + "DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(__IDS(), IDS_ + "DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(IDS);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACQUIRE_ITEM) + "_SCRIPT", "vg_s1_ids_acquir", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_UNACQUIRE_ITEM) + "_SCRIPT", "vg_s1_ids_unacqu", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_EQUIP_ITEM) + "_SCRIPT", "vg_s1_ids_equip", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_UNEQUIP_ITEM) + "_SCRIPT", "vg_s1_ids_unequi", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_DAMAGED) + "_SCRIPT", "vg_s1_ids_itdamg", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_DEATH) + "_SCRIPT", "vg_s1_ids_itdead", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CUSTOM_LOOP) + "_SCRIPT", "vg_s1_ids_durab", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CUSTOM_LOOP) + "_SCRIPT", "vg_s1_ids_itpers", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_ids_colors", -2);

    // create waypoints for IDS system for each area in the world that will hold list of placed item id's for persistence purposes
    int n = 1;
    object SysObj = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    object area = glo(SysObj, SYSTEM_SYS_PREFIX + "_AREA", n);

    while (GetIsObjectValid(area))
    {
        logentry("[" + IDS + "] Creating PlacedItems list for area " + GetTag(area));
        vector vCreate = Vector(1.0f + n, 6.0f, 1.0f);
        location lCreate = Location(area, vCreate, 0.0f);
        object placedList = CreateObject(OBJECT_TYPE_WAYPOINT, IDS_TAG_PLACED_ITEMS_AREA_OBJECT, lCreate, FALSE, IDS_ + "AREA_" + GetTag(area));

        slo(placedList, IDS_ + "AREA", area);
        area = glo(SysObj, SYSTEM_SYS_PREFIX + "_AREA", ++n);
    }

    LoadPersistentData(OBJECT_SELF, IDS);
    WaitForLoadData();
}

