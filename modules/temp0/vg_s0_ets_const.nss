// +---------++----------------------------------------------------------------+
// | Name    || Exchange Trade System (ETS) Constants                          |
// | File    || vg_s0_ets_const.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+

// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_ETS_NAME                 =   "Exchange Trade System";
const string    SYSTEM_ETS_PREFIX               =   "ETS";
const string    SYSTEM_ETS_VERSION              =   "1.0";
const string    SYSTEM_ETS_AUTHOR               =   "VirgiL";
const string    SYSTEM_ETS_DATE                 =   "2018-03-31";
const string    SYSTEM_ETS_UPDATE               =   "2018-03-31";
string          ETS                                 =   SYSTEM_ETS_PREFIX;
string          ETS_                                =   ETS + "_";
string          _ETS                                =   "_" + ETS;
string          _ETS_                               =   "_" + ETS + "_";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+





object __ets;
object __ETS();
object __ETS()
{

    if (!GetIsObjectValid(__ets))
        __ets = GetObjectByTag("SYSTEM" + _ETS_ + "OBJECT");

    return __ets;
}

