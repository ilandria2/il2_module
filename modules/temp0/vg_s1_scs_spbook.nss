// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Spellbook script                       |
// | File    || vg_s1_scs_spbook.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-05-2009                                                     |
// | Updated || 16-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Zkontroluje se jestli hrac ma dostatecnou uroven potrebneho povolani a pokud
    ma, pak se nauci kouzlo ktere je zadefinovano v pouzite knize kouzel
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetItemActivator();
    object  oItem   =   GetItemActivated();
    string  sTag    =   GetTag(oItem);
    string  sPart   =   GetSubString(sTag, 5, 6);

    //  VG_I_BK_SCS_A_00
    //  _____^^^^^^_____
    if  (sPart  ==  "BK_SCS")
    {
        itemproperty    ipTemp  =   GetFirstItemProperty(oItem);
        int             nType, nSubType, nSpell, nFeat, bKnown, nCount, bSpellBook;

        while   (GetIsItemPropertyValid(ipTemp))
        {
            nType   =   GetItemPropertyType(ipTemp);

            if  (nType  ==  SCS_IPRP_TYPE_SPELLBOOK)
            {
                nCount++;
                bSpellBook  =   TRUE;
                nSubType    =   GetItemPropertySubType(ipTemp);
                nSpell      =   stoi(Get2DAString("scs_spellbooks", "Spell_ID", nSubType));
                nFeat       =   SCS_FIRST_CONJURE_FEAT + nSpell;

                if  (GetHasFeat(nFeat, oPC)
                ||  (nSpell >=  SCS_FIRST_CONJURE_ABILITY))
                {
                    bKnown  =   gli(oPC, SYSTEM_SCS_PREFIX + "_SPELL", nSpell);

                    if  (!bKnown)
                    {
                        sli(oPC, SYSTEM_SCS_PREFIX + "_LEARN_SPELL_ID", nSpell);
                        slo(oPC, SYSTEM_SCS_PREFIX + "_LEARN_SPELL_SPBOOK", oItem);
                        ExecuteScript(SCS_NCS_LEARN_SPELL_RESREF, oPC);
                        RemoveItemProperty(oItem, ipTemp);
                        nCount--;
                    }

                    else
                    {
                        SYS_Info(oPC, SYSTEM_SCS_PREFIX, "LEARN_SPELL_INTERRUPTED_&" + itos(nSpell) + "&");
                    }
                }

                else
                {
                    SYS_Info(oPC, SYSTEM_SCS_PREFIX, "LEARN_SPELL_FAILED_&" + itos(nSpell) + "&");
                }
            }

            ipTemp  =   GetNextItemProperty(oItem);
        }

        if  (bSpellBook)
        {
            if  (!nCount)
            {
                SetPlotFlag(oItem, FALSE);
                DestroyObject(oItem, 0.1f);
            }
        }
    }
}
