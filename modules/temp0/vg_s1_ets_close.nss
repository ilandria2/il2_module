#include "vg_s0_ets_core"

void main()
{
    object player = GetLastClosedBy();

    int playerCarts = gli(player, ETS_ + "CART_O_ROWS");
    int storeCarts = gli(OBJECT_SELF, ETS_ + "CART_O_ROWS");

    if (!playerCarts && !storeCarts)
    {
        slo(player, ETS_ + "TARGET", OBJECT_INVALID, -1, SYS_M_DELETE);
        slo(OBJECT_SELF, ETS_ + "TARGET", OBJECT_INVALID, -1, SYS_M_DELETE);
    }

    logentry("[" + ETS + "] Player " + GetPCPlayerName(player) + " closed store " + GetTag(OBJECT_SELF));
}
