// +---------++----------------------------------------------------------------+
// | Name    || Object Event System (OES) Core                                 |
// | File    || vg_s0_oes_core_c.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 26-12-2007                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pomoci lokalnich promennych lze zadefinovat chovani udalosti. Tohle je
    knihovna typu "parser"
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_c"
#include    "vg_s0_oes_const"
#include    "x0_i0_position"
#include "vg_s0_ccs_const"


// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || OES_ActionEvent                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Podle typu udalosti pusti integrovane skripty v OES jeste pred tim, nez
//  prohledne prikazy definovane pomoci lokalnich promennych
//
// -----------------------------------------------------------------------------
void    OES_ActionEvent(int bEventType, object oObject = OBJECT_SELF);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



void    OES_LoopEventUntilSystemsInitialized(object oObject, int bEventType)
{
    int bInit   =   gli(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT_STATUS");

    if  (bInit  !=  -1)
    {
        DelayCommand(10.0f, OES_LoopEventUntilSystemsInitialized(oObject, bEventType));
        return;
    }

    OES_ActionEvent(bEventType, oObject);
}



// +---------------------------------------------------------------------------+
// |                            OES_ActionEvent                                |
// +---------------------------------------------------------------------------+



void    OES_ActionEvent(int bEventType, object oObject = OBJECT_SELF)
{
    int bInit   =   gli(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT_STATUS");

    if  (bInit  !=  -1)
    {
        switch  (bEventType)
        {
            case    OES_EVENT_O_ON_MODULE_LOAD: break;
            case    OES_EVENT_A_ON_HEARTBEAT:
            case    OES_EVENT_A_ON_ENTER:
            case    OES_EVENT_C_ON_HEARTBEAT:
            case    OES_EVENT_C_ON_PERCEPTION:
            case    OES_EVENT_D_ON_HEARTBEAT:
            case    OES_EVENT_E_ON_HEARTBEAT:
            case    OES_EVENT_O_ON_HEARTBEAT:
            case    OES_EVENT_P_ON_HEARTBEAT:
            case    OES_EVENT_R_ON_HEARTBEAT:
            return;
            break;

            default:
            DelayCommand(5.0f, OES_LoopEventUntilSystemsInitialized(OBJECT_SELF, bEventType));
            return;
            break;
        }
    }

    object  oSelf   =   oObject;
    int     bType   =   GetObjectType(oSelf);
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");
    string  sEvent  =   gls(oSystem, SYSTEM_OES_PREFIX + "_EVENT", bEventType + 1);

    int bDebug  =   gli(GetModule(), "oes_test");

    if  (bDebug ==  2)  SpawnScriptDebugger();

    else

    if  (bDebug ==  1)
    {
        string  sMessage    =   C0 + "Self [" + W1 + GetTag(OBJECT_SELF) + C0 + " / " + W1 + GetName(OBJECT_SELF) +
                                C0 + "] Loc [" + W1 + GetTag(GetArea(OBJECT_SELF)) + C0 + " / " + W1 + GetName(GetArea(OBJECT_SELF)) + C0 +
                                C0 + "] Event ID [" + W1 + sEvent + C0 + "]";

        AssignCommand(GetModule(), SpeakString(sMessage, TALKVOLUME_SHOUT));
    }

    /*
    if  (bInit  !=  -1)
        {
        int bExist  =   gli(oSystem, "oes_event_" + itos(bEventType) + "_ran");

        if  (!bExist)
        {
            sli(oSystem, "oes_event_" + itos(bEventType) + "_ran", TRUE);
            sls(oSystem, "oes_event_ran", sEvent, -2);
            DelayCommand(10.0f, logentry("oes_event_ran: " + itos(bEventType) + " - " + sEvent, FALSE));
        }
    }
    */

    if  (bType  ==  OBJECT_TYPE_CREATURE)
    {
        switch  (bEventType)
        {
            case    OES_EVENT_C_ON_BLOCKED:
                ExecuteScript("nw_c2_defaulte", oSelf);
            break;

            case    OES_EVENT_C_ON_COMBAT_ROUND_END:
                ExecuteScript("nw_c2_default3", oSelf);
            break;

            case    OES_EVENT_C_ON_CONVERSATION:
                ExecuteScript("nw_c2_default4", oSelf);
            break;

            case    OES_EVENT_C_ON_DAMAGED:
                ExecuteScript("nw_c2_default6", oSelf);
            break;

            case    OES_EVENT_C_ON_DEATH:
                ExecuteScript("nw_c2_default7", oSelf);
            break;

            case    OES_EVENT_C_ON_DISTURBED:
                ExecuteScript("nw_c2_default8", oSelf);
            break;

            case    OES_EVENT_C_ON_HEARTBEAT:
                ExecuteScript("nw_c2_default1", oSelf);

                if  (gls(OBJECT_SELF, "LPD_DIALOG") !=  "")
                {
                    object  oSource =   glo(OBJECT_SELF, "LPD_CONVERSATOR");

                    if  (!GetIsObjectValid(oSource))
                    {
                        if  (GetCurrentAction(OBJECT_SELF)  ==  ACTION_INVALID)
                        {
                            location    lSpawn  =   gll(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_SPAWN");
                            object      oArea   =   GetAreaFromLocation(lSpawn);

                            if  (GetIsObjectValid(oArea))
                            {
                                AssignCommand(OBJECT_SELF, ClearAllActions(TRUE));

                                if  (GetArea(OBJECT_SELF)   !=  oArea)
                                AssignCommand(OBJECT_SELF, JumpToLocation(lSpawn));

                                else
                                {
                                    if  (GetDistanceBetweenLocations(lSpawn, GetLocation(OBJECT_SELF))  >=  3.0f)
                                    AssignCommand(OBJECT_SELF, ActionMoveToLocation(lSpawn, FALSE));

                                    else
                                    AssignCommand(OBJECT_SELF, SetFacing(GetFacingFromLocation(lSpawn)));
                                }
                            }
                        }
                    }
                }

                //  object created by the spawn system
                if  (gls(OBJECT_SELF, "TIS_GROUP")  !=  "")
                {
                    DelayCommand(1.0f, ExecuteScript("vg_s1_bcs_rndwlk", OBJECT_SELF));
                }
            break;

            case    OES_EVENT_C_ON_PERCEPTION:
                ExecuteScript("nw_c2_default2", oSelf);

                {
                object  oPC =   GetLastPerceived();

                if  (GetIsPC(oPC))
                RemoveFromParty(oPC);
                }
            break;

            case    OES_EVENT_C_ON_PHYSICAL_ATTACKED:
                ExecuteScript("nw_c2_default5", oSelf);
            break;

            case    OES_EVENT_C_ON_RESTED:
                ExecuteScript("nw_c2_defaulta", oSelf);
            break;

            case    OES_EVENT_C_ON_SPAWN:
                ExecuteScript("nw_c2_default9", oSelf);
                sll(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_SPAWN", GetLocation(OBJECT_SELF));
            break;

            case    OES_EVENT_C_ON_SPELL_CAST_AT:
                ExecuteScript("nw_c2_defaultb", oSelf);
            break;

            case    OES_EVENT_C_ON_USER_DEFINED:
                ExecuteScript("nw_c2_defaultd", oSelf);
            break;
        }
    }

    else

    if  (bType  ==  OBJECT_TYPE_DOOR)
    {
        switch  (bEventType)
        {
            case    OES_EVENT_D_ON_AREA_TRANSITION:

            break;

            case    OES_EVENT_D_ON_CLOSE:


            break;

            case    OES_EVENT_D_ON_DAMAGED:


            break;

            case    OES_EVENT_D_ON_DEATH:


            break;

            case    OES_EVENT_D_ON_FAIL_TO_OPEN:


            break;

            case    OES_EVENT_D_ON_HEARTBEAT:


            break;

            case    OES_EVENT_D_ON_LOCK:


            break;

            case    OES_EVENT_D_ON_OPEN:


            break;

            case    OES_EVENT_D_ON_PHYSICAL_ATTACKED:

            break;

            case    OES_EVENT_D_ON_SPELL_CAST_AT:


            break;

            case    OES_EVENT_D_ON_UNLOCK:


            break;

            case    OES_EVENT_D_ON_USER_DEFINED:

            break;
        }
    }

    else

    if  (bType  ==  OBJECT_TYPE_ENCOUNTER)
    {
        switch  (bEventType)
        {
            case    OES_EVENT_E_ON_ENTER:

            break;

            case    OES_EVENT_E_ON_EXHAUSTED:

            break;

            case    OES_EVENT_E_ON_EXIT:

            break;

            case    OES_EVENT_E_ON_HEARTBEAT:

            break;

            case    OES_EVENT_E_ON_USER_DEFINED:

            break;
        }
    }

    else

    if  (bType  ==  OBJECT_TYPE_STORE)
    {
        switch  (bEventType)
        {
            case    OES_EVENT_M_ON_CLOSE_STORE:

            break;

            case    OES_EVENT_M_ON_OPEN_STORE:

            break;
        }
    }

    else

    if  (bType  ==  OBJECT_TYPE_PLACEABLE)
    {
        switch  (bEventType)
        {
            case    OES_EVENT_P_ON_CLICK:

            break;

            case    OES_EVENT_P_ON_CLOSE:

            break;

            case    OES_EVENT_P_ON_DAMAGED:

            break;

            case    OES_EVENT_P_ON_DEATH:

            if  (!GetUseableFlag(OBJECT_SELF))
            {
                object  oItem   =   GetFirstItemInInventory(OBJECT_SELF);

                while   (GetIsObjectValid(oItem))
                {
                    SetIdentified(
                        CopyObject(
                            oItem,
                            Location(
                                GetArea(OBJECT_SELF),
                                GetPositionFromLocation(GetRandomLocation(GetArea(OBJECT_SELF), OBJECT_SELF, 2.0f)),
                                ran(0.0f, 360.0f, 0)
                            )
                        ),
                        TRUE
                    );

                    oItem   =   GetNextItemInInventory(OBJECT_SELF);
                }
            }
            break;

            case    OES_EVENT_P_ON_DISTURBED:

            break;

            case    OES_EVENT_P_ON_HEARTBEAT:

            break;

            case    OES_EVENT_P_ON_LOCK:

            break;

            case    OES_EVENT_P_ON_OPEN:

            break;

            case    OES_EVENT_P_ON_PHYSICAL_ATTACKED:

            break;

            case    OES_EVENT_P_ON_SPELL_CAST_AT:

            break;

            case    OES_EVENT_P_ON_UNLOCK:

            break;

            case    OES_EVENT_P_ON_USED:

            break;

            case    OES_EVENT_P_ON_USER_DEFINED:

            break;
        }
    }

    else

    if  (bType  ==  OBJECT_TYPE_TRIGGER)
    {
        switch  (bEventType)
        {/*
            case    OES_EVENT_R_ON_AREA_TRANSITION:

            break;
         */
            case    OES_EVENT_R_ON_CLICK:

            break;

            case    OES_EVENT_R_ON_ENTER:

            break;

            case    OES_EVENT_R_ON_EXIT:

            break;

            case    OES_EVENT_R_ON_HEARTBEAT:

            break;

            case    OES_EVENT_R_ON_USER_DEFINED:

            break;

            case    OES_EVENT_T_ON_DISARM:

            break;

            case    OES_EVENT_T_ON_TRAP_TRIGGERED:

            break;
        }
    }

    else
    {
        if  (GetModule()    ==  oSelf)
        {
            switch  (bEventType)
            {
                case    OES_EVENT_O_ON_ACQUIRE_ITEM:
                    if  (!GetIsPC(GetModuleItemAcquiredBy()))   break;
                    if  (GetBaseItemType(GetModuleItemAcquired())   ==  BASE_ITEM_DIALOGOPTION)    break;

                    logentry(
                        "[" + SYSTEM_OES_PREFIX + "] "  +
                        "Acquire item: Item [" + GetName(GetModuleItemAcquired()) + " / " + GetTag(GetModuleItemAcquired()) +
                        " (x" + itos(GetModuleItemAcquiredStackSize()) +
                        ")] Obj [" + GetName(GetModuleItemAcquiredBy()) +
                        "] in [" + GetName(GetArea(GetModuleItemAcquiredBy())) +
                        "] from [" + GetName(GetModuleItemAcquiredFrom()) + "]");
                break;


                case    OES_EVENT_O_ON_ACTIVATE_ITEM:

                    //ExecuteScript("cnr_module_onact", oSelf);

                break;

                case    OES_EVENT_O_ON_CLIENT_ENTER:

                    //ExecuteScript("cnr_module_oce", oSelf);

                break;

                case    OES_EVENT_O_ON_CLIENT_LEAVE:

                break;

                case    OES_EVENT_O_ON_PLAYER_LEAVING:

                    logentry("[" + SYSTEM_OES_PREFIX + "] Player [" + GetName(OBJECT_SELF) + " / " + GetPCPlayerName(OBJECT_SELF) + "] leaving module");

                break;

                case    OES_EVENT_O_ON_CUTSCENE_ABORT:


                break;

                case    OES_EVENT_O_ON_HEARTBEAT:

                    if  (gli(oSystem, SYSTEM_OES_PREFIX + "_HEARTBEAT") ==  OES_CUSTOM_LOOP_HEARTBEATS)
                    {
                        sli(oSystem, SYSTEM_OES_PREFIX + "_HEARTBEAT", 0);
                        OES_ActionEvent(OES_EVENT_O_ON_CUSTOM_LOOP);
                    }

                    else
                    sli(oSystem, SYSTEM_OES_PREFIX + "_HEARTBEAT", 1, -1, SYS_M_INCREMENT);

                break;

                case    OES_EVENT_O_ON_MODULE_LOAD:

                    sls(GetModule(), "NWNX!INIT", "1");
                    SQLInit();
                    SQLExecDirect("set names cp1250");
                    AssignCommand(OBJECT_SELF, ExecuteScript("vg_s1_oes_loadsy", oSelf));

                break;

                case    OES_EVENT_O_ON_PLAYER_DEATH:

                break;

                case    OES_EVENT_O_ON_PLAYER_DYING:


                break;

                case    OES_EVENT_O_ON_PLAYER_EQUIP_ITEM:

                break;

                case    OES_EVENT_O_ON_PLAYER_CHAT:

                break;

                case    OES_EVENT_O_ON_PLAYER_LEVEL_UP:

                break;

                case    OES_EVENT_O_ON_PLAYER_RESPAWN:

                break;

                case    OES_EVENT_O_ON_PLAYER_REST:

                break;

                case    OES_EVENT_O_ON_UNACQUIRE_ITEM:
                {
                    if  (!GetIsPC(GetModuleItemLostBy()))   break;
                    if  (GetBaseItemType(GetModuleItemLost())   ==  BASE_ITEM_DIALOGOPTION)    break;

                    object  oNew    =   GetItemPossessor(GetModuleItemLost());

                    logentry(
                        "[" + SYSTEM_OES_PREFIX + "] "  +
                        "Unacquire item: Item [" + GetName(GetModuleItemLost()) + " / " + GetTag(GetModuleItemLost()) +
                        "] Obj [" + GetName(GetModuleItemLostBy()) +
                        "] in [" + GetName(GetArea(GetModuleItemLostBy())) +
                        "] new [" + GetName(oNew) + " / " + GetTag(oNew) + "]");
                }
                break;

                case    OES_EVENT_O_ON_PLAYER_UNEQUIP_ITEM:

                break;

                case    OES_EVENT_O_ON_USER_DEFINED:


                break;

                case    OES_EVENT_O_ON_CUSTOM_LOOP:

                break;
            }
        }

        else
        {
            switch  (bEventType)
            {
                case    OES_EVENT_A_ON_ENTER:
                    if  (!GetIsPC(OBJECT_SELF)) break;
                    logentry(
                        "[" + SYSTEM_OES_PREFIX + "] "  +
                        "Area Enter: Entering [" + GetName(GetEnteringObject()) +
                        " / " + GetTag(GetEnteringObject()) +
                        " / " + GetPCPlayerName(GetEnteringObject()) +
                        "] Area [" + GetName(OBJECT_SELF) + "]");
                break;

                case    OES_EVENT_A_ON_EXIT:

                break;

                case    OES_EVENT_A_ON_HEARTBEAT:

                break;

                case    OES_EVENT_A_ON_USER_DEFINED:

                break;
            }
        }
    }

    if (bEventType == OES_EVENT_O_ON_PLAYER_CHAT)
    {
        object player = GetPCChatSpeaker();
        string script = GetPromptScript(player);

        if (script != "")
        {
            string answer = GetPCChatMessage();

            if (left(answer, 1) != CCS_COMMAND_SEPARATOR)
            {
                SetPromptAnswer(player, answer);
                RunScript(script, player);
                SetPromptScript(player, "");
                SetPCChatMessage("");
                return;
            }
        }
    }

    if  (bEventType ==  OES_EVENT_C_ON_CONVERSATION
    &&  GetListenPatternNumber()    <=  100
    &&  GetListenPatternNumber()    !=  -1) return;

    int     nRow    =   1;
    string  sScript =   gls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(bEventType) + "_SCRIPT", nRow);
    int bLog = gli(oSystem, SYSTEM_OES_PREFIX + "_EVENT_LOG");

    while   (sScript    !=  "")
    {
        if (bLog) logentry("[" + SYSTEM_OES_PREFIX + "] LogEvent [" + itos(bEventType) + "] script [" + sScript + "] - begin");

        ExecuteScript(sScript, oSelf);

        if (bLog) logentry("[" + SYSTEM_OES_PREFIX + "] LogEvent [" + itos(bEventType) + "] script [" + sScript + "] - end");

        sScript =   gls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(bEventType) + "_SCRIPT", ++nRow);
    }

    sScript =   gls(OBJECT_SELF, SYSTEM_OES_PREFIX + "_EVENT_" + itos(bEventType) + "_SCRIPT");

    if  (sScript    !=  "")
    {
        ExecuteScript(sScript, OBJECT_SELF);
    }
}
