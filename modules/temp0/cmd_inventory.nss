// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_inventory.nss                                              |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 12-12-2009                                                     |
// | Updated || 12-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "INVENTORY"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "INVENTORY";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sType   =   CCS_GetConvertedString(sLine, 1, FALSE);
    object  oTarget =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;

    sType   =   GetStringUpperCase(sType);

    //  zly typ inventare se nahradi 'GENERIC'
    if  (sType  !=  "EQUIPPED"
    &&  sType   !=  "NATURAL")
    sType   =   "GENERIC";

    //  neplatny cil
    if  (!GetIsObjectValid(oTarget))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    object  oView   =   glo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_TARGET");

    //  jiz prohlizi inventar
    if  (GetIsObjectValid(oView))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "You are already viewing some inventory";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    object  oViewer =   glo(oTarget, SYSTEM_CCS_PREFIX + "_INVENTORY_VIEWER");

    //  tento inventar jiz nekdo prohlizi
    if  (GetIsObjectValid(oViewer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Inventory of the selected target is currently being viewed by other PM";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int bInventory  =   GetHasInventory(oTarget);

    //  cil nema inventar
    if  (!bInventory)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "The target does not have any invnetory";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  pokus o prohlizeni typu inventare, ktere maji jenom bytosti, kde vybrany
    //  objekt neni bytost
    if  (sType  !=  "GENERIC")
    {
        if  (GetObjectType(oTarget) !=  OBJECT_TYPE_CREATURE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Only creatures have equip inventory";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }
    }

    //  cil nesmi byt vyvolavatel s vyjimkou pro natural
    if  (oPC    ==  oTarget)
    {
        if  (sType  !=  "NATURAL")
        {
            sMessage    =   R1 + "( ! ) " + W2 + "On your character you can only view the natural items inventory";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }
    }

    object  oContainer  =   CreateObject(OBJECT_TYPE_PLACEABLE, "VG_P_S1_CCS_INV1", Location(GetArea(oPC), GetPosition(oPC), GetFacing(oPC) + 180.0f));

    if  (!GetIsObjectValid(oContainer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Unable to create invisible container";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    string  sInventory  =   (sType  ==  "GENERIC")  ?   "Generic inventory"   :
                            (sType  ==  "EQUIPPED") ?   "Equip inventory"   :
                            (sType  ==  "NATURAL")  ?   "Natural items inventory"  :   "";
    string  sName       =   GetName(oTarget);
    string  sContainer  =   W2 + sInventory + " (" + G1 + sName + W2 + ")";

    ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectVisualEffect(VFX_DUR_CUTSCENE_INVISIBILITY), oContainer);
    SetName(oContainer, sContainer);
    slo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_CONTAINER", oContainer);
    slo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_TARGET", oTarget);
    slo(oTarget, SYSTEM_CCS_PREFIX + "_INVENTORY_VIEWER", oPC);
    sls(oContainer, SYSTEM_CCS_PREFIX + "_INVENTORY_TYPE", sType);
    AssignCommand(oPC, ClearAllActions(TRUE));
    DelayCommand(0.15f, AssignCommand(oPC, ActionInteractObject(oContainer)));

    //  predmety z obecnyho inventare
    if  (sType  ==  "GENERIC")
    {
        object  oTemp   =   GetFirstItemInInventory(oTarget);
        int     nCount;

        while   (GetIsObjectValid(oTemp))
        {
            if  (!GetHasInventory(oTemp))
            {
                object  oCopy   =   CopyItem(oTemp, oContainer, TRUE);

                if  (GetIsObjectValid(oCopy))
                {
                    slo(oCopy, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oTemp);
                    SetDroppableFlag(oCopy, FALSE);

                    nCount++;
                }

                else
                {
                    sMessage    =   R1 + "( ! ) " + W2 + "Unable to copy item [" + R2 + GetName(oTemp) + W2 + "]";

                    msg(oPC, sMessage, FALSE, FALSE);
                }
            }

            else
            {
                sMessage    =   R1 + "( !!! ) " + W2 + "Found item [" + R2 + GetName(oTemp) + W2 + "] with its own inventory - cannot copy the item itself but the items inside will be copied";

                msg(oPC, sMessage, FALSE, FALSE);
            }

            oTemp   =   GetNextItemInInventory(oTarget);
        }

        if  (!nCount)   sMessage    =   Y1 + "( ? ) " + W2 + "Target does not have any items in the inventory";
        else            sMessage    =   G1 + "( ! ) " + W2 + "Loaded " + G2 + itos(nCount) + W2 + " items from generic inventory";

        msg(oPC, sMessage, FALSE, FALSE);
    }

    else

    //  vybavene predmety
    if  (sType  ==  "EQUIPPED")
    {
        int     nCount, nSlot;
        object  oTemp;

        while   (nSlot  <   NUM_INVENTORY_SLOTS)
        {
            oTemp   =   GetItemInSlot(nSlot, oTarget);

            if  (GetIsObjectValid(oTemp))
            {
                object  oCopy   =   CopyItem(oTemp, oContainer, TRUE);

                slo(oCopy, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oTemp);
                SetDroppableFlag(oCopy, FALSE);

                nCount++;
            }

            nSlot++;
        }

        if  (!nCount)   sMessage    =   Y1 + "( ? ) " + W2 + "Target does not have any equipped items";
        else            sMessage    =   G1 + "( ! ) " + W2 + "Loaded " + G2 + itos(nCount) + W2 + " equipped items";

        msg(oPC, sMessage, FALSE, FALSE);
    }

    else

    //  vrozene predmety
    if  (sType  ==  "NATURAL")
    {
        object  oCWeapon_R  =   GetItemInSlot(INVENTORY_SLOT_CWEAPON_R, oTarget);
        object  oCWeapon_L  =   GetItemInSlot(INVENTORY_SLOT_CWEAPON_L, oTarget);
        object  oCWeapon_B  =   GetItemInSlot(INVENTORY_SLOT_CWEAPON_B, oTarget);
        object  oCArmour    =   GetItemInSlot(INVENTORY_SLOT_CARMOUR, oTarget);
        int nCount;

        if  (GetIsObjectValid(oCWeapon_R))
        {
            object  oCopy   =   CopyItem(oCWeapon_R, oContainer, TRUE);

            slo(oCopy, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oCWeapon_R);
            SetDroppableFlag(oCopy, FALSE);

            nCount++;
        }

        if  (GetIsObjectValid(oCWeapon_L))
        {
            object  oCopy   =   CopyItem(oCWeapon_L, oContainer, TRUE);

            slo(oCopy, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oCWeapon_L);
            SetDroppableFlag(oCopy, FALSE);

            nCount++;
        }

        if  (GetIsObjectValid(oCWeapon_B))
        {
            object  oCopy   =   CopyItem(oCWeapon_B, oContainer, TRUE);

            slo(oCopy, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oCWeapon_B);
            SetDroppableFlag(oCopy, FALSE);

            nCount++;
        }

        if  (GetIsObjectValid(oCArmour))
        {
            object  oCopy   =   CopyItem(oCArmour, oContainer, TRUE);

            slo(oCopy, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oCArmour);
            SetDroppableFlag(oCopy, FALSE);

            nCount++;
        }

        if  (!nCount)   sMessage    =   Y1 + "( ? ) " + W2 + "Target does not have any natural items";
        else            sMessage    =   G1 + "( ! ) " + W2 + "Loaded " + G2 + itos(nCount) + W2 + " natural items";

        msg(oPC, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
