// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Info script                      |
// | File    || vg_s1_pis_info.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu PIS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_pis_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_PIS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "NAME_ON")
    {
        string  sParam     =   GetNthSubString(sTemp, 1);

        //sMessage    =   C_NORM + "Pomoc� prvku " + C_INTE + "'Z�kladn� interakce'" + C_NORM + " vyber ciz� hr��skou postavu";
        return;
    }

    else

    if  (sCase  ==  "HIDE_SWITCH")
    {
        sMessage    =   C_NORM + "Mode " + C_INTE + "Hidden identity" + C_NORM + " switched";
    }

    else

    if  (sCase  ==  "USE_NAME")
    {
        sMessage    =   C_NORM + "Use the " + C_INTE + "'Target'" + C_NORM + " ability to select a foreign player character";
    }

    else

    if  (sCase  ==  "NAMES_OFF")
    {
        sMessage    =   C_NORM + "All " + C_INTE + "dynamic names" + C_NORM + " stored for your character were " + C_NEGA + "deleted";
    }

    else

    if  (sCase  ==  "TARGET_UNSET")
    {
        sMessage    =   C_NORM + "The " + C_INTE + "'Target'" + C_NORM + " ability is now unset";
    }

    else

    if  (sCase  ==  "VALID_TARGET")
    {
        sMessage    =   C_NORM + "You need to select a " + C_INTE + "visible foreign player character";
    }

    else

    if  (sCase  ==  "AFK")
    {
        sMessage    =   C_NORM + "Mode " + Y1 + "AFK" + C_NORM + " switched";
    }

    msg(OBJECT_SELF, sMessage, TRUE, FALSE);
}

