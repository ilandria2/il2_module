// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_spawnshow.nss                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SPAWNSHOW"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SPAWNSHOW";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sMessage;
    object  oTIS, oSpawn;
    int     n, nSpawn;

    oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");
    oSpawn  =   GetNearestObject(OBJECT_TYPE_WAYPOINT, oPC, ++n);

    //  projde vsechny placeable v aktualni oblasti
    while   (GetIsObjectValid(oSpawn))
    {
        //msg(oPC, GetTag(oSpawn));
        //  pouze placeable s TAG-em _WP_REV na konci (indikator zahaleneho objektu)
        if  (right(GetTag(oSpawn), 7)   ==  "_WP_REV")
        {
            nSpawn++;
            oSpawn  =   CreateObject(OBJECT_TYPE_PLACEABLE, "vg_p_ve_lights01", GetLocation(oSpawn), FALSE, "SPAWN_MARKER");

            DelayCommand(2 * 60.0f, SetPlotFlag(oSpawn, FALSE));
            DelayCommand(2 * 60.0f + itof(nSpawn), DestroyObject(oSpawn));
        }

        oSpawn  =   GetNearestObject(OBJECT_TYPE_WAYPOINT, oPC, ++n);
    }

    sMessage    =   G1 + "( ! ) " + W2 + "Highlighted " + G2 + itos(nSpawn) + W2 + " spawned placeable in this arrea for the time of 2 minutes";

    msg(oPC, sMessage, TRUE, FALSE);


//  Kod skriptu
//  ----------------------------------------------------------------------------
}

