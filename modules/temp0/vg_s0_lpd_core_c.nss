// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialog System (LPD) Core C                       |
// | File    || vg_s0_lpd_core_c.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-12-2006                                                     |
// | Updated || 11-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu LPD typu "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_const"
#include    "vg_s0_scs_const"
#include    "vg_s0_sys_core_c"
#include    "vg_s0_xps_core_m"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || LPD_GetPC                                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati objekt hrace z rozhovoru
//
// -----------------------------------------------------------------------------
object  LPD_GetPC();



// +---------++----------------------------------------------------------------+
// | Name    || LPD_GetSource                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati objekt zdroje rozhovoru
//
// -----------------------------------------------------------------------------
object  LPD_GetSource();



// +---------++----------------------------------------------------------------+
// | Name    || LPD_GetDialog                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati zdroj rozhovoru pro NPC
//
// -----------------------------------------------------------------------------
object  LPD_GetDialog(object oSource = OBJECT_SELF);


// gets the dialog source object identified by npcTag
object LPD_GetDialogByTag(string npcTag);


// +---------++----------------------------------------------------------------+
// | Name    || LPD_DetermineConditions                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkontroluje podminky rozhovoru (Text Appears When) definovane v retezci
//  sTAW pro zdroj rozhovoru oSource (NPC)
//
// -----------------------------------------------------------------------------
int     LPD_DetermineConditions(string sTAW, object oSource);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_ActionsTaken                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Provede "Actions taken" sACT pro zdroj rozhovoru oSource
//
// -----------------------------------------------------------------------------
void    LPD_ActionsTaken(string sACT, object oSource);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_StartConversation                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Spusti rozhovor oDialog hrace oPC se zdrojem rozhovoru oSource
//  ** pokud oDialog neni definovan, jako rozhovor se pouziva lokalni promenna
//  urcujici TAG tohoto rozhovoru
//
// -----------------------------------------------------------------------------
void    LPD_StartConversation(object oPC, object oSource, object oDialog = OBJECT_INVALID);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_EndConversation                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skonci rozhovor hrace oPC
//
// -----------------------------------------------------------------------------
void    LPD_EndConversation(object oPC, int bAbort = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_OpenSourceNode                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Otevre vsechny uzle v rozhovoru od definovaneho az dokud nenarazi na hracovu
//  povinnou interakci nebo do skonceni rozhovoru
//
// -----------------------------------------------------------------------------
void    LPD_OpenSource(object oPC, string node, int bResume = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_GetBase                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati zaklad z LPN:
//  ** kdyz LPN je 1234, navrati 1000
//
// -----------------------------------------------------------------------------
int     LPD_GetBase(int nLPN);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_GetSpokenText                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati text kteri hrac rekl (GetMatchedSubString(0, 1, ..., n))
//  ** pokud bLP je FALSE, navrati GetPCChatMessage()
//
// -----------------------------------------------------------------------------
string  LPD_GetSpokenText(int bLP = TRUE, int bConvert = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_LoopFacing                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Bytosti oCreature se otaci na bytost oFaceTo rekurzivne po intervalech
//
// -----------------------------------------------------------------------------
void    LPD_LoopFacing(object oCreature, object oFaceTo);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_PauseConversation                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Pozastavi rozhovor hrace oPC
//
// -----------------------------------------------------------------------------
void    LPD_PauseConversation(object oPC = OBJECT_SELF);



// +---------++----------------------------------------------------------------+
// | Name    || LPD_ResumeConversation                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Pokracuje v rozhovoru hrace oPC
//
// -----------------------------------------------------------------------------
void    LPD_ResumeConversation(object oPC = OBJECT_SELF);


// gets amount of start node links registered for source
int LPD_GetStartNodeLinkCount(object source);

// gets link to a start node by row
string LPD_GetStartNodeLink(object source, int row);

// gets amount of node links registered for source
int LPD_GetNodeLinkCount(object source);

// gets link to a node by row
string LPD_GetNodeLink(object source, int row);

// gets amount of node links registered for source
int LPD_GetChildNodeLinkCount(object source, string node);

// gets link to a child node of parent node by row
string LPD_GetChildNodeLink(object source, string node, int row);

// gets the keyword value registered for source on a specific node
string LPD_GetNodeKeyword(object source, string node);

// gets the spoken text value registered for source on a specific node
string LPD_GetNodeText(object source, string node);

// gets the target conversator of speaker
object LPD_GetConversator(object speaker);

// sets the target conversator of speaker
void LPD_SetConversator(object speaker, object target);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                         LPD_DetermineConditions                           |
// +---------------------------------------------------------------------------+



int     LPD_DetermineConditions(string sTAW, object oSource)
{
    if  (!GetIsObjectValid(oSource))    return  FALSE;
    if  (sTAW   ==  "")                 return  TRUE;

    int     bResult =   TRUE;
    string  sType   =   GetStringLeft(sTAW, 4);

    //  ------------------------------------------------------------------------
    //  TAW - SCRIPT

    if  (sType  ==  LPD_STRING_FUNCTION_SCRIPT)
    {
        string  sScript =   GetNthSubString(sTAW, 1);
        string  sParam  =   GetNthSubString(sTAW, 2);

        sls(oSource, SYSTEM_LPD_PREFIX + "_SCRIPT_PARAMETER", sParam);

        bResult =   CheckScript(sScript, oSource);
    }

    //  TAW - SCRIPT
    //  ------------------------------------------------------------------------

    else

    //  ------------------------------------------------------------------------
    //  TAW - LOCAL VARIABLE Check

    if  (sType  ==  LPD_STRING_FUNCTION_LOCAL_VAR)
    {
        int     nType   =   stoi(GetNthSubString(sTAW, 1));
        int     nObjDef =   stoi(GetNthSubString(sTAW, 2));
        string  sObjDef =   GetNthSubString(sTAW, 3);
        string  sName   =   GetNthSubString(sTAW, 4);
        string  sValue  =   GetNthSubString(sTAW, 5);
        int     nRow    =   stoi(GetNthSubString(sTAW, 6));
        int     bComp   =   stoi(GetNthSubString(sTAW, 7));
        object  oTarget =   GetDefinedObject(nObjDef, sObjDef, oSource);

        bResult =   CheckVariable(nType, oTarget, sName, sValue, nRow, bComp);
    }

    //  TAW - LOCAL VARIABLE Check
    //  ------------------------------------------------------------------------

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                             LPD_ActionsTaken                              |
// +---------------------------------------------------------------------------+



void    LPD_ActionsTaken(string sACT, object oSource)
{
    if  (!GetIsObjectValid(oSource))    return;
    if  (sACT   ==  "")                 return;

    string  sType   =   GetStringLeft(sACT, 4);

    if  (sType  ==  LPD_STRING_FUNCTION_SCRIPT)
    {
        string  sScript =   GetNthSubString(sACT, 1);
        string  sParam  =   GetNthSubString(sACT, 2);

        sls(oSource, SYSTEM_LPD_PREFIX + "_SCRIPT_PARAMETER", sParam);
        /*DelayCommand(0.03, */ExecuteScript(sScript, oSource)/*)*/;
    }

    else

    if  (sType  ==  LPD_STRING_FUNCTION_LOCAL_VAR)
    {
        int     nType   =   stoi(GetNthSubString(sACT, 1));
        int     nObjDef =   stoi(GetNthSubString(sACT, 2));
        string  sObjDef =   GetNthSubString(sACT, 3);
        string  sName   =   GetNthSubString(sACT, 4);
        string  sValue  =   GetNthSubString(sACT, 5);
        int     nRow    =   stoi(GetNthSubString(sACT, 6));
        int     bComp   =   stoi(GetNthSubString(sACT, 7));
        object  oTarget =   GetDefinedObject(nObjDef, sObjDef, oSource);

        ActionVariable(nType, oTarget, sName, sValue, nRow, bComp);
    }
}



// +---------------------------------------------------------------------------+
// |                                LPD_GetPC                                  |
// +---------------------------------------------------------------------------+



object  LPD_GetPC()
{
    object  oConv   =   LPD_GetConversator(OBJECT_SELF);

    if  (GetIsObjectValid(oConv)
    &&  GetIsPC(oConv))
    return  oConv;

    if  (GetIsPC(OBJECT_SELF))
    return  OBJECT_SELF;
    return  OBJECT_INVALID;
}



// +---------------------------------------------------------------------------+
// |                               LPD_GetSource                               |
// +---------------------------------------------------------------------------+



object  LPD_GetSource()
{
    object  oConv   =   LPD_GetConversator(OBJECT_SELF);

    if  (GetIsObjectValid(oConv)
    &&  !GetIsPC(oConv))
    return  oConv;

    if  (!GetIsPC(OBJECT_SELF))
    return  OBJECT_SELF;
    return  OBJECT_INVALID;
}



int LPD_GetStartNodeLinkCount(object source)
{
    return gli(source, SYSTEM_LPD_PREFIX + "_START_S_ROWS");
}

string LPD_GetStartNodeLink(object source, int row)
{
    return gls(source, SYSTEM_LPD_PREFIX + "_START", row);
}

int LPD_GetNodeLinkCount(object source)
{
    return gli(source, SYSTEM_LPD_PREFIX + "_NODE_S_ROWS");
}

string LPD_GetNodeLink(object source, int row)
{
    return gls(source, SYSTEM_LPD_PREFIX + "_NODE", row);
}

int LPD_GetChildNodeLinkCount(object source, string node)
{
    return gli(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_CHILD_S_ROWS");
}

string LPD_GetChildNodeLink(object source, string node, int row)
{
    return gls(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_CHILD", row);
}

string LPD_GetNodeKeyword(object source, string node)
{
    return gls(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_KEYWORD");
}

string LPD_GetNodeText(object source, string node)
{
    return gls(source, SYSTEM_LPD_PREFIX + "_NODE_" + node + "_TEXT");
}



// +---------------------------------------------------------------------------+
// |                            LPDS_StartConversation                         |
// +---------------------------------------------------------------------------+



void LPD_StartConversation(object oPC, object oSource, object oDialog = OBJECT_INVALID)
{
    if (!GetIsObjectValid(oPC)) return;
    if (!GetIsObjectValid(oSource)) return;
    if (!GetIsObjectValid(oDialog))
        oDialog = LPD_GetDialog(oSource);
    if (!GetIsObjectValid(oDialog)) return;
    if (!GetCommandable(oPC))  return;
    if (GetIsInCombat(oPC))    return;
    if (GetIsDead(oPC))        return;

    object oG_PC = LPD_GetConversator(oPC);

    // restart dialog if clicked npc the pc is already conversating with
    if (oG_PC == oSource)
        LPD_EndConversation(oPC, TRUE);

    // link conversation targets between pc and npc
    LPD_SetConversator(oSource, oPC);
    LPD_SetConversator(oPC, oSource);

    int row;
    int links = LPD_GetStartNodeLinkCount(oDialog);
    int bValid = FALSE;
    string child = "";

    if (gli(OBJECT_SELF, "lpd_start"))
        SpawnScriptDebugger();

    // loop through all starts and pick first valid (for now, take only first)
    for (row = 1; row <= links; row++)
    {
        child = LPD_GetStartNodeLink(oDialog, row);
        bValid = child != "";
        //TAW     =   gls(oDialog, SYSTEM_LPD_PREFIX + "_ROOT_" + itos(nSource) + "_TAW");
        //bValid  =   (TAW    ==  "") ?   TRUE    :   LPD_DetermineConditions(TAW, oSource);

        if (bValid)
            break;
    }

    // at least one node found
    if  (bValid)
    {
        // non private dialogs should force the NPC to keep looking at player
        if  (!GetIsPC(oSource))
            DelayCommand(ran(0.0f, 1.0f, 1), LPD_LoopFacing(oSource, oPC));

        // when player started a monologue then he should have the same list of vars as if he is npc
        else
            sls(oPC, SYSTEM_LPD_PREFIX + "_DIALOG", GetTag(oDialog));

        DelayCommand(0.03f, AssignCommand(oSource, ClearAllActions()));
        SYS_Info(oPC, SYSTEM_LPD_PREFIX, "STARTED");
        LPD_OpenSource(oPC, child);
    }
}



// +---------------------------------------------------------------------------+
// |                               LPD_GetBase                                 |
// +---------------------------------------------------------------------------+



int     LPD_GetBase(int nLPN)
{
    int     nResult, nCount, nLength;
    string  sConvert;

    sConvert    =   itos(nLPN);
    nLength     =   GetStringLength(sConvert);
    nCount      =   ftoi(pow(10.0f, itof(nLength - 1)));
    nResult     =   stoi(left(sConvert, 1)) * nCount;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                         LPD_EndConversation                               |
// +---------------------------------------------------------------------------+



void    LPD_EndConversation(object oPC, int bAbort = FALSE)
{
    if  (!GetIsObjectValid(oPC))        return;

    object  oSource =   LPD_GetConversator(oPC);

    if  (!GetIsObjectValid(oSource))    return;

    object  oDialog =   LPD_GetDialog(oSource);
    int     nLast   =   gli(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_S_ROWS");
    int     n       =   1;
    string validNode;

    while (n <= nLast)
    {
        validNode = gls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE", n++, SYS_M_DELETE);
        sls(oSource, SYSTEM_LPD_PREFIX + validNode + "_KEYWORD", "", -1, SYS_M_DELETE);
    }

    sli(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_S_ROWS", -1, -1, SYS_M_DELETE);

    n       =   1;
    nLast   =   gli(oSource, SYSTEM_LPD_PREFIX + "_TALK_S_ROWS");

    while   (n  <=  nLast)
    sls(oSource, SYSTEM_LPD_PREFIX + "_TALK", "", n++, SYS_M_DELETE);
    sli(oSource, SYSTEM_LPD_PREFIX + "_TALK_S_ROWS", -1, -1, SYS_M_DELETE);

    n       =   1;
    nLast   =   gli(oSource, SYSTEM_LPD_PREFIX + "_TEXT_S_ROWS");

    while   (n  <=  nLast)
    {
        sls(oSource, SYSTEM_LPD_PREFIX + "_TEXT", "", n, SYS_M_DELETE);
        sli(oSource, SYSTEM_LPD_PREFIX + "_VFX", -1, n, SYS_M_DELETE);
        n++;
    }

    sli(oSource, SYSTEM_LPD_PREFIX + "_TEXT_S_ROWS", -1, -1, SYS_M_DELETE);
    sli(oSource, SYSTEM_LPD_PREFIX + "_ICON_S_ROWS", -1, -1, SYS_M_DELETE);

    string  END_A   =   gls(oDialog, SYSTEM_LPD_PREFIX + "_END_A");
    string  END_N   =   gls(oDialog, SYSTEM_LPD_PREFIX + "_END_N");
    string  sScript =   (bAbort)    ?   END_A   :   END_N;

    if  (sScript    !=  "")
    {
        AssignCommand(oSource, ClearAllActions());
        ExecuteScript(sScript, oSource);
    }

    object  oGUI    =   glo(oPC, SYSTEM_LPD_PREFIX + "_GUI");
    object  oItem   =   GetFirstItemInInventory(oGUI);

    while   (GetIsObjectValid(oItem))
    {
        SetPlotFlag(oItem, FALSE);
        DestroyObject(oItem, 0.1f);

        oItem   =   GetNextItemInInventory(oGUI);
    }

    SetPlotFlag(oGUI, FALSE);
    DestroyObject(oGUI, 0.15f);
    SetCutsceneMode(oPC, TRUE, TRUE);
    SetCutsceneMode(oPC, FALSE, TRUE);
    sls(oPC, SYSTEM_LPD_PREFIX + "_DIALOG", "", -1, SYS_M_DELETE);
    sli(oSource, SYSTEM_LPD_PREFIX + "_IS_READY", FALSE, -1, SYS_M_DELETE);
    sli(oSource, SYSTEM_LPD_PREFIX + "_IS_PAUSED", FALSE, -1, SYS_M_DELETE);
    sls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_CUR", "", -1, SYS_M_DELETE);
    slo(oPC, SYSTEM_LPD_PREFIX + "_GUI", OBJECT_INVALID, -1, SYS_M_DELETE);
    LPD_SetConversator(oPC, OBJECT_INVALID);
    LPD_SetConversator(oSource, OBJECT_INVALID);

    if (bAbort)
        SYS_Info(oPC, SYSTEM_LPD_PREFIX, "ABORTED");
    else
        SYS_Info(oPC, SYSTEM_LPD_PREFIX, "ENDED");
    //sli(oSource, SYSTEM_LPD_PREFIX + "_RESPONSE", 0, -1, SYS_M_DELETE);
}



// +---------------------------------------------------------------------------+
// |                              LPD_OpenSource                               |
// +---------------------------------------------------------------------------+



void LPD_OpenSource(object oPC, string node, int bResume = FALSE)
{
    if (!GetIsObjectValid(oPC))
        return;

    object oSource = LPD_GetConversator(oPC);
    if (!GetIsObjectValid(oSource))
        return;

    object oDialog = LPD_GetDialog(oSource);

    if (!GetIsObjectValid(oDialog)) return;
    if (!GetCommandable(oPC)) return;
    if (GetCurrentHitPoints(oPC) < 0) return;
    if (gli(oPC, "lpd_debug"))SpawnScriptDebugger();

    // remember current source node and type
    sls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_CUR", node);

    // Sxxx = StartNode, Exxx = NPCEntry, Rxxx = PCReply
    int bEntry = left(node, 1) == "E";
    int validNode;
    float fDelay;
    object oActor;

    if (!bResume)
    {
//  ----------------------------------------------------------------------------
//  delete previous temp variables

        int n = 1;
        int nLast = gli(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_S_ROWS");
        int validNode;

        // list of valid nodes and keywords
        while (n <= nLast)
        {
            string sourceNode = gls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE", n++, SYS_M_DELETE);
            sls(oSource, SYSTEM_LPD_PREFIX + "_" + sourceNode + "_KEYWORD", "", -1, SYS_M_DELETE);
        }

        sli(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_S_ROWS", -1, -1, SYS_M_DELETE);

//  delete previous temp variables
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  load node properties and perform actions

        oActor = (bEntry) ? oSource : oPC;

        // NPC speaking
        if (bEntry)
        {
            string TEXT = LPD_GetNodeText(oDialog, node);

            if (TEXT != "")
            {
                // support $VarName as variable
                if (left(TEXT, 1) == "$")
                    TEXT = gls(oSource, sub(TEXT, 1, len(TEXT) - 1));
                TEXT = ConvertTokens(TEXT, oPC);

                // npc talk
                //if (!GetIsPC(oSource))
                //    AssignCommand(oSource, SpeakString(TEXT));

                // monologue message in combat log
                if (GetIsPC(oSource))
                    msg(oSource, TEXT, TRUE, FALSE);

                // save spoken text for NPC due to later usage by possible GUI system
                sls(oSource, SYSTEM_LPD_PREFIX + "_TALK", TEXT, -2);
            }

            // mark NPC as busy
            sli(oSource, SYSTEM_LPD_PREFIX + "_IS_READY", FALSE);
        }

        // player speaking
        else
        {
            // delete previous spoken text by NPC
            int n = 1;
            int nLast = gli(oSource, SYSTEM_LPD_PREFIX + "_TALK_S_ROWS");

            while (n  <=  nLast)
                sls(oSource, SYSTEM_LPD_PREFIX + "_TALK", "", n++, SYS_M_DELETE);
            sli(oSource, SYSTEM_LPD_PREFIX + "_TALK_S_ROWS", -1, -1, SYS_M_DELETE);
        }

        //  Actions taken
        //string ACT = gls(oDialog, SYSTEM_LPD_PREFIX + "_" + ((bEntry) ? "E" : "R") + "_" + itos(nID) + "_ACT");
        //if (ACT != "")
        //    LPD_ActionsTaken(ACT, oActor);

        //  in monologue the player shouldn't visibly say anything
        //if  (bPM)   SetPCChatMessage("");

        // dialogue interupted from within action script - interupt execution
        object oG_PC = LPD_GetConversator(oPC);
        if (!GetIsObjectValid(oG_PC))
            return;

//  load node properties and perform actions
//  ----------------------------------------------------------------------------
    }

    // NPC is flagged as paused from dialogue - do not continue
    int bPaused = gli(oSource, SYSTEM_LPD_PREFIX + "_IS_PAUSED");
    if (bPaused)
        return;

//  ----------------------------------------------------------------------------
//  load new Entry (NPC) node

    if (bEntry)
    {
        int children = LPD_GetChildNodeLinkCount(oDialog, node);
        int validCount = 0;

        if (gli(oPC, "lpd_src"))SpawnScriptDebugger();
        for (validNode = 1; validNode <= children; validNode++)
        {
            string child = LPD_GetChildNodeLink(oDialog, node, validNode);
            int CONDITION = child != "";
            //TAW = gls(oDialog, SYSTEM_LPD_PREFIX + "_E_" + itos(nID) + "_S_" + itos(validNode) + "_TAW");
            //int CONDITION = (TAW == "") ? TRUE : LPD_DetermineConditions(TAW, oSource);

            if (CONDITION)
            {
                bEntry = left(child, 1) == "E";

                // continue with another Entry (NPC) node
                if  (bEntry)
                {
                    DelayCommand(fDelay, LPD_OpenSource(oPC, child));
                    return;
                }
                fDelay += 0.03f;
                validCount++;

                sls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE", child, -2);
            }
        }

        int n;
        string talk = gls(oSource, SYSTEM_LPD_PREFIX + "_TALK", ++n);
        string message;

        while (talk != "")
        {
            if (message != "")
                message += "\n\n";
            message += talk;
            talk = gls(oSource, SYSTEM_LPD_PREFIX + "_TALK", ++n);
        }

        if (message != "")
        {
            message = ConvertTokens(message, oSource, FALSE, TRUE, FALSE, FALSE);
            AssignCommand(oSource, SpeakString(message));
        }

        // at least onde node was found valid
        if (validCount > 0)
        {
            DelayCommand(fDelay, SYS_Info(oPC, SYSTEM_LPD_PREFIX, "MENU_LIST"));
            DelayCommand(fDelay, sli(oSource, SYSTEM_LPD_PREFIX + "_IS_READY", TRUE));
            return;
        }
    }

//  load new Entry (NPC) node
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  load new Reply (PC) node

    else
    {
        // reset timeout response counter
        sli(oSource, SYSTEM_LPD_PREFIX + "_RESPONSE", 0, -1, SYS_M_DELETE);

        int children = LPD_GetChildNodeLinkCount(oDialog, node);
        for (validNode = 1; validNode <= children; validNode++)
        {
            string child = LPD_GetChildNodeLink(oDialog, node, validNode);
            int CONDITION = child != "";
            //string TAW = gls(oDialog, SYSTEM_LPD_PREFIX + "_R_" + itos(nID) + "_S_" + itos(validNode) + "_TAW");
            //CONDITION = LPD_DetermineConditions(TAW, oPC);

            if (CONDITION)
            {
                LPD_OpenSource(oPC, child);
                return;
            }
        }
    }

//  load new Reply (PC) node
//  ----------------------------------------------------------------------------

    //  no valid node left - end dialogue
    DelayCommand(fDelay + 0.1, LPD_EndConversation(oPC, FALSE));
}



// +---------------------------------------------------------------------------+
// |                            LPD_GetDialog                                  |
// +---------------------------------------------------------------------------+



object LPD_GetDialog(object oSource = OBJECT_SELF)
{
    if (!GetIsObjectValid(oSource))
        return OBJECT_INVALID;

    string sTAG = gls(oSource, SYSTEM_LPD_PREFIX + "_DIALOG");

    if (sTAG != "")
        return GetObjectByTag(sTAG);

    sTAG = SYSTEM_LPD_PREFIX + "_SOURCE_" + GetTag(oSource);
    return GetObjectByTag(sTAG);
}

object LPD_GetDialogByTag(string npcTag)
{
    npcTag = SYSTEM_LPD_PREFIX + "_SOURCE_" + npcTag;
    return GetObjectByTag(npcTag);
}



// +---------------------------------------------------------------------------+
// |                          LPD_GetSpokenText                                |
// +---------------------------------------------------------------------------+



string  LPD_GetSpokenText(int bLP = TRUE, int bConvert = TRUE)
{
    if  (!bLP)
    {
        if  (!bConvert)
        return  GetPCChatMessage();
        return  ConvertText(GetPCChatMessage());
    }

    string  sResult;

    int n;
    int nCount  =   GetMatchedSubstringsCount();

    while   (n  <=  nCount)
    sResult +=  GetMatchedSubstring(n++);

    if  (bConvert)  sResult =   ConvertText(sResult);

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                             LPD_LoopFacing                                |
// +---------------------------------------------------------------------------+



void    loop(object oCreature, object oFaceTo)
{
    if  (!GetIsObjectValid(oCreature))  return;
    if  (!GetIsObjectValid(oFaceTo))
    {
        sli(oCreature, SYSTEM_LPD_PREFIX + "_FLOOP", FALSE, -1, SYS_M_DELETE);
        sli(oCreature, SYSTEM_LPD_PREFIX + "_RESPONSE", FALSE, -1, SYS_M_DELETE);
        return;
    }

    object  oG_Cre  =   LPD_GetConversator(oCreature);
    object  oG_Face =   LPD_GetConversator(oFaceTo);

    if  (!GetIsObjectValid(oG_Cre)
    ||  !GetIsObjectValid(oG_Face)
    ||  GetIsInCombat(oCreature)
    ||  GetIsDead(oCreature)
    ||  oG_Cre  !=  oFaceTo
    ||  oG_Face !=  oCreature)
    {
        sli(oCreature, SYSTEM_LPD_PREFIX + "_FLOOP", FALSE, -1, SYS_M_DELETE);
        sli(oCreature, SYSTEM_LPD_PREFIX + "_RESPONSE", FALSE, -1, SYS_M_DELETE);
        return;
    }

    int nResponse   =   gli(oCreature, SYSTEM_LPD_PREFIX + "_RESPONSE");

    if  (nResponse  >=  LPD_DEFAULT_MAX_NOT_RESPONDING)
    {
        LPD_EndConversation(oFaceTo, TRUE);
        sli(oCreature, SYSTEM_LPD_PREFIX + "_FLOOP", FALSE, -1, SYS_M_DELETE);
        sli(oCreature, SYSTEM_LPD_PREFIX + "_RESPONSE", FALSE, -1, SYS_M_DELETE);
        return;
    }

    vector  vFTo    =   GetPosition(oFaceTo);
    location lFTo   =   Location(GetArea(oCreature), Vector(vFTo.x, vFTo.y, GetPosition(oCreature).z), GetFacing(oFaceTo));
    float   fDist   =   GetDistanceBetweenLocations(GetLocation(oCreature), lFTo);

    if  (fDist  >=  LPD_DEFAULT_MAX_DISTANCE)
    {
        LPD_EndConversation(oFaceTo, TRUE);
        sli(oCreature, SYSTEM_LPD_PREFIX + "_FLOOP", FALSE, -1, SYS_M_DELETE);
        sli(oCreature, SYSTEM_LPD_PREFIX + "_RESPONSE", FALSE, -1, SYS_M_DELETE);
        return;
    }

    if  (gli(oCreature, SYSTEM_LPD_PREFIX + "_IS_READY")    ==  TRUE)
    sli(oCreature, SYSTEM_LPD_PREFIX + "_RESPONSE", 1, -1, SYS_M_INCREMENT);
    AssignCommand(oCreature, SetFacingPoint(GetPosition(oFaceTo)));
    DelayCommand(LPD_DEFAULT_FACING_INTERVAL, loop(oCreature, oFaceTo));
}



void    LPD_LoopFacing(object oCreature, object oFaceTo)
{
    if  (gli(oCreature, SYSTEM_LPD_PREFIX + "_FLOOP"))  return;

    sli(oCreature, SYSTEM_LPD_PREFIX + "_FLOOP", TRUE);
    DelayCommand(LPD_DEFAULT_FACING_INTERVAL, loop(oCreature, oFaceTo));
}



// +---------------------------------------------------------------------------+
// |                            LPD_PauseConversation                          |
// +---------------------------------------------------------------------------+



void    LPD_PauseConversation(object oPC = OBJECT_SELF)
{
    object  oG_PC   =   LPD_GetConversator(oPC);

    if  (!GetIsObjectValid(oG_PC))  return;

    int bPaused =   gli(oG_PC, SYSTEM_LPD_PREFIX + "_IS_PAUSED");

    if  (bPaused)   return;

    sli(oG_PC, SYSTEM_LPD_PREFIX + "_IS_PAUSED", TRUE);
    SetCutsceneMode(oPC, TRUE, TRUE);
    SetCutsceneMode(oPC, FALSE, TRUE);
    //SYS_Info(oPC, SYSTEM_LPD_PREFIX, "STATE_PAUSE");
}



// +---------------------------------------------------------------------------+
// |                           LPD_ResumeConversation                          |
// +---------------------------------------------------------------------------+



void LPD_ResumeConversation(object oPC = OBJECT_SELF)
{
    object oG_PC = LPD_GetConversator(oPC);

    if (!GetIsObjectValid(oG_PC))
        return;

    int bPaused = gli(oG_PC, SYSTEM_LPD_PREFIX + "_IS_PAUSED");

    if (!bPaused)
        return;

    sli(oG_PC, SYSTEM_LPD_PREFIX + "_IS_PAUSED", FALSE, -1, SYS_M_DELETE);
    //SYS_Info(oPC, SYSTEM_LPD_PREFIX, "STATE_RESUME");

    string current = gls(oG_PC, SYSTEM_LPD_PREFIX + "_SOURCE_CUR");
    LPD_OpenSource(oPC, current, TRUE);
}

object LPD_GetConversator(object speaker)
{
    return glo(speaker, SYSTEM_LPD_PREFIX + "_CONVERSATOR");
}

void LPD_SetConversator(object speaker, object target)
{
    if (!GetIsObjectValid(target))
        slo(speaker, SYSTEM_LPD_PREFIX + "_CONVERSATOR", OBJECT_INVALID, -1, SYS_M_DELETE);
    else
        slo(speaker, SYSTEM_LPD_PREFIX + "_CONVERSATOR", target);
}

