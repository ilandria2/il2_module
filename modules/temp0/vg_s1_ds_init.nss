// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) System init                                  |
// | File    || vg_s1_ds_init.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 29-07-2009                                                     |
// | Updated || 29-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system DS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_ds_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_DS_PREFIX;
    string  sName       =   SYSTEM_DS_NAME;
    string  sVersion    =   SYSTEM_DS_VERSION;
    string  sAuthor     =   SYSTEM_DS_AUTHOR;
    string  sDate       =   SYSTEM_DS_DATE;
    string  sUpDate     =   SYSTEM_DS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_ds_info");
    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_ds_data_c");
    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_ds_data_s");
    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_ds_data_l");
    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", FALSE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_ds_corpsec", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CLIENT_LEAVE) + "_SCRIPT", "vg_s1_ds_corpsed", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_DEATH) + "_SCRIPT", "vg_s1_ds_pop_up", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_DYING) + "_SCRIPT", "vg_s1_ds_bleed", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_RESPAWN) + "_SCRIPT", "vg_s1_ds_jump", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_RESURRECT) + "_SCRIPT", "vg_s1_ds_resurre", -2);


/*

    // <INIT_CODE>

*/


    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
