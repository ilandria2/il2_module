// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Constants                        |
// | File    || vg_s1_tis_init.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system TIS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_oes_const"
#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{

    string  sName       =   SYSTEM_TIS_NAME;
    string  sVersion    =   SYSTEM_TIS_VERSION;
    string  sAuthor     =   SYSTEM_TIS_AUTHOR;
    string  sDate       =   SYSTEM_TIS_DATE;
    string  sUpDate     =   SYSTEM_TIS_UPDATE;

    sli(__TIS(), "SYSTEM" + _TIS + "_STATE", 2);
    logentry("[" + TIS + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", TIS, -2);
    sli(__TIS(), "SYSTEM" + _TIS + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(__TIS(), "SYSTEM" + _TIS + "_NAME", sName);
    sls(__TIS(), "SYSTEM" + _TIS + "_PREFIX", TIS);
    sls(__TIS(), "SYSTEM" + _TIS + "_VERSION", sVersion);
    sls(__TIS(), "SYSTEM" + _TIS + "_AUTHOR", sAuthor);
    sls(__TIS(), "SYSTEM" + _TIS + "_DATE", sDate);
    sls(__TIS(), "SYSTEM" + _TIS + "_UPDATE", sUpDate);

    sls(__TIS(), TIS + "_NCS_SYSINFO_RESREF", "vg_s1_tis_info");

    if  (gli(__TIS(), TIS + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(TIS);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_DEATH) + "_SCRIPT", "vg_s1_tis_dspawn", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_DEATH) + "_SCRIPT", "vg_s1_tis_dspawn", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_A_ON_ENTER) + "_SCRIPT", "vg_s1_tis_spawn", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_R_ON_ENTER) + "_SCRIPT", "vg_s1_tis_portal", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_A_ON_EXIT) + "_SCRIPT", "vg_s1_tis_exit", -2);

    //  ------------------------------------------------------------------------
    //  inicializacni kod

    //  aktivace existujicich TIS objektu v module
    //ExecuteScript("vg_s1_tis_activa", OBJECT_SELF);

    //  seskupeni oblasti pro spawn system
    DelayCommand(5.0f, ExecuteScript("vg_s1_tis_groups", OBJECT_SELF));

    //  hned po nacteni modulu se nastavi vsechny spawn objekty jako pripravene
    DelayCommand(7.0f, ExecuteScript("vg_s1_tis_fspawn", OBJECT_SELF));

    //  inicializacni kod
    //  ------------------------------------------------------------------------

    sli(__TIS(), "SYSTEM" + _TIS + "_STATE", TRUE);
    logentry("[" + TIS + "] init done", TRUE);
}
