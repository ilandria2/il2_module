// executed when player buys an item

#include "vg_s0_ets_const"
#include "vg_s0_ets_core"

void main()
{
    object store = GetModuleItemAcquiredFrom();
    object item = GetModuleItemAcquired();
    object player = GetModuleItemAcquiredBy();

    if (!GetIsObjectValid(store))
    {
        if (GetStolenFlag(item) && !GetPlotFlag(item))
        {
            DestroyObject(item, 0.01);
            logentry("[" + ETS + "] Destroying old cart item: " + GetTag(item) + " from player: " + GetPCPlayerName(player));
        }
    }

    if (GetObjectType(store) != OBJECT_TYPE_STORE) return;

    ETS_InteractWithStoreItem("BUY", store, player, item);
}
