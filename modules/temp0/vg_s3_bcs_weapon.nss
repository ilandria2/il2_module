// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) OnWeaponHit impact script             |
// | File    || vg_s3_bcs_weapon.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 18-07-2009                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Upravuje pocet zasahu hracovi
*/
// -----------------------------------------------------------------------------



#include    "x2_inc_switches"
#include    "x2_inc_spellhook"
#include    "vg_s0_bcs_core_c"
#include    "vg_s0_xps_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!X2PreSpellCastCode()) return;

    object  oTarget =   GetSpellTargetObject();
    int     bPC     =   GetIsPC(oTarget);

    if  (GetIsPC(OBJECT_SELF))
    {
        string  sTag    =   (GetIsPC(oTarget))  ?   GetPCPlayerName(oTarget)    :   GetTag(oTarget);
        object  oWeapon =   GetLastWeaponUsed(OBJECT_SELF);
        int     nClass  =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT"), SYSTEM_XPS_PREFIX + "_BASEITEM_CLASS", GetBaseItemType(oWeapon));
        int     nHits   =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_CLASS", nClass);

        if  (!nHits)
        sli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS", nClass, -2, SYS_M_SET);
        sli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_CLASS", 1, nClass, SYS_M_INCREMENT);
    }

    if  (bPC)   BCS_DetermineCombatRound(OBJECT_SELF, oTarget);
}
