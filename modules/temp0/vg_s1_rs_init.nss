// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) System init                                   |
// | File    || vg_s1_rs_init.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 13-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system RS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_rs_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_RS_PREFIX;
    string  sName       =   SYSTEM_RS_NAME;
    string  sVersion    =   SYSTEM_RS_VERSION;
    string  sAuthor     =   SYSTEM_RS_AUTHOR;
    string  sDate       =   SYSTEM_RS_DATE;
    string  sUpDate     =   SYSTEM_RS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_rs_info");
    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_rs_data_c");
    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_rs_data_s");
    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_rs_data_l");
    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_REST) + "_SCRIPT", "vg_s1_rs_rest", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CUSTOM_LOOP) + "_SCRIPT", "vg_s1_rs_tired", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACTIVATE_ITEM) + "_SCRIPT", "vg_s1_rs_bedroll", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_DEATH) + "_SCRIPT", "vg_s1_rs_death", -2);

/*
    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 1",
        -2
    );

    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 2",
        -2
    );
*/
/*
    sli(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_EDGE_NONE, -2);
    sli(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_EDGE_LOW, -2);
    sli(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_EDGE_MEDIUM, -2);
    sli(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_EDGE_WEAK_PC, -2);
    sli(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_EDGE_HEAVY_BODY, -2);
    sli(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_EDGE_FORCE_SLEEP, -2);

    sls(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", "��dn� �nava", -2);
    sls(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", "Slab� �nava", -2);
    sls(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", "�nava", -2);
    sls(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", "Vy�erpanost", -2);
    sls(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", "Ospalost", -2);
    sls(oSystem, SYSTEM_RS_PREFIX + "_TIRED_EDGE", "Vynucen� sp�nek", -2);
*/

    //sls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", "Z�jem o RP", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", "Z�jem o akci", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", "Z�jem o quest", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", "Z�jem o v�pravu", -2);

    //sls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", "Meditace", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", "Sed�n�", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", "D��m�n�", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", "Sp�nek", -2);
    //sls(oSystem, SYSTEM_RS_PREFIX + "_REST_STATE", "Vynucen� sp�nek", -2);

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
