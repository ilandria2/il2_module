// on player send message script for conversation's dialogue options selection

#include "vg_s0_lpd_core_c"
#include "vg_s0_ccs_core_t"

void main()
{
    string sMsg = GetPCChatMessage();

    // ignore blanks
    if (sMsg == "")
        return;

    object player = GetPCChatSpeaker();
    if (gli(player, "lpd_pcm"))SpawnScriptDebugger();
    // ignore out of character
    if (left(sMsg, 2) == "//")
        return;

    // ignore message not starting with channel character
    if (left(sMsg, 1) != CCS_CHANNEL_IDENTIFIER)
        return;

    object oSource = LPD_GetConversator(player);

    // not in dialogue - ignore
    // to start a dialogue, player must write /hi xxx channel command first
    if (!GetIsObjectValid(oSource))
        return;

    object oDialog = LPD_GetDialog(oSource);

    // if player speaks to an NPC then dinstance (or for future, field of view) should be considered
    if (oSource != player)
        if (GetDistanceBetween(player, oSource) > LPD_DEFAULT_MAX_DISTANCE)
            return;

    int nSource = 1;
    int bMatch = FALSE;
    string node = gls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE", nSource);
    string chanName = CCS_GetChannel(sMsg);
    string chanText = CCS_GetChannelText(sMsg);
    string keyword, text;

    while (node != "")
    {
        keyword = LPD_GetNodeKeyword(oDialog, node);
        bMatch = upper(chanName) == upper(keyword);

        if (bMatch)
        {
            text = chanText;

            if (text == "")
                text = LPD_GetNodeText(oDialog, node);
            text = ConvertTokens(text, player, FALSE, TRUE, FALSE, FALSE);

            SetPCChatMessage(text);
            break;
        }

        node = gls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE", ++nSource);
    }

    if (bMatch)
        DelayCommand(0.5f, LPD_OpenSource(player, node, FALSE));
}
