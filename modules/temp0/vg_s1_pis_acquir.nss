// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Item acquire script              |
// | File    || vg_s1_pis_acquir.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Obnovi indikator pro hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+


void    acq(object oPC)
{
    PIS_RefreshIndicator(oPC, OBJECT_INVALID, FALSE);
    sli(oPC, SYSTEM_PIS_PREFIX + "_ACQUIRED", FALSE, -1, SYS_M_DELETE);
}



void    main()
{
    object  oPC     =   GetModuleItemAcquiredBy();

    if  (!GetIsPC(oPC)) return;

    int bAcq    =   gli(oPC, SYSTEM_PIS_PREFIX + "_ACQUIRED");

    if  (bAcq)  return;

    sli(oPC, SYSTEM_PIS_PREFIX + "_ACQUIRED", TRUE);
    //DelayCommand(3.0f, acq(oPC));
}

