// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialogs (LPD) Death Abort script                 |
// | File    || vg_s1_lpd_dthabo.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kdyz hrac zemre, prerusi se jeho aktualni rozhovor
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetLastPlayerDied();

    LPD_EndConversation(oPC, TRUE);
}
