#include "vg_s0_sys_core_v"
#include "nwnx_funcs"

void main()
{
    object source = OBJECT_SELF;
    object target = glo(source, SYSTEM_SYS_PREFIX + "_COPY_VARS_TARGET", -1, SYS_M_DELETE);
    struct LocalVariable lvar = GetFirstLocalVariable(source);
    while (GetIsVariableValid(lvar))
    {
        //msg(oPC, C2 + "Copying var [" + lvar.name + "]");
        switch (lvar.type)
        {
            case VARIABLE_TYPE_FLOAT:
                slf(target, lvar.name, glf(source, lvar.name));
                break;
            case VARIABLE_TYPE_INT:
                sli(target, lvar.name, gli(source, lvar.name));
                break;
            case VARIABLE_TYPE_STRING:
                sls(target, lvar.name, gls(source, lvar.name));
                break;
            case VARIABLE_TYPE_OBJECT:
                slo(target, lvar.name, glo(source, lvar.name));
                break;
            case VARIABLE_TYPE_LOCATION:
                sll(target, lvar.name, gll(source, lvar.name));
                break;
        }
        lvar = GetNextLocalVariable(lvar);
    }
}
