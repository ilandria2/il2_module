// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Constants                                     |
// | File    || vg_s0_rs_const.nss                                             |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 13-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu RS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_RS_NAME                      =   "Rest System";
const string    SYSTEM_RS_PREFIX                    =   "RS";
const string    SYSTEM_RS_VERSION                   =   "2.10";
const string    SYSTEM_RS_AUTHOR                    =   "VirgiL";
const string    SYSTEM_RS_DATE                      =   "04-06-2008";
const string    SYSTEM_RS_UPDATE                    =   "30-11-2010";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



//  zdroje
const string    RS_TAG_REST_DIALOG                  =   "VG_P_S1_RS_REST";
const string    RS_TAG_RESTAREA                     =   "VG_W_RS_RESTAREA";
const string    RS_RSRF_BEDROLL                     =   "vg_p_s1_rs_bed";

//  zajem o..
const int       RS_PCFIND_MODE_NONE                 =   0;
const int       RS_PCFIND_MODE_ROLEPLAY             =   1;
const int       RS_PCFIND_MODE_ACTION               =   2;
const int       RS_PCFIND_MODE_QUEST                =   3;
const int       RS_PCFIND_MODE_EXPEDITION           =   4;

//  stavy spanku
const int       RS_REST_STATE_NONE                  =   0;
const int       RS_REST_STATE_MEDITATE              =   1;
const int       RS_REST_STATE_SIT                   =   2;
const int       RS_REST_STATE_NAP                   =   3;
const int       RS_REST_STATE_SLEEP                 =   4;
const int       RS_REST_STATE_FORCE_SLEEP           =   5;
const int       RS_REST_STATE_INVALID               =   255;

//  unava za vahu
const int       RS_WEIGHT_PERCENT_EDGE_1            =   25;
const int       RS_WEIGHT_PERCENT_EDGE_2            =   50;
const int       RS_WEIGHT_PERCENT_EDGE_3            =   75;
const int       RS_WEIGHT_PERCENT_VALUE_1           =   1;
const int       RS_WEIGHT_PERCENT_VALUE_2           =   2;
const int       RS_WEIGHT_PERCENT_VALUE_3           =   3;

const int       RS_WEIGHT_TOLERANCE                 =   150;

//  unava za boj
const int       RS_ATTACKS_COUNT_EDGE_1             =   10;
const int       RS_ATTACKS_COUNT_EDGE_2             =   30;
const int       RS_ATTACKS_COUNT_EDGE_3             =   50;
const int       RS_ATTACKS_COUNT_VALUE_1            =   1;
const int       RS_ATTACKS_COUNT_VALUE_2            =   2;
const int       RS_ATTACKS_COUNT_VALUE_3            =   3;

//  unava za kouzleni
const int       RS_CONJURE_COUNT_EDGE_1             =   30;
const int       RS_CONJURE_COUNT_EDGE_2             =   60;
const int       RS_CONJURE_COUNT_EDGE_3             =   90;
const int       RS_CONJURE_COUNT_VALUE_1            =   1;
const int       RS_CONJURE_COUNT_VALUE_2            =   2;
const int       RS_CONJURE_COUNT_VALUE_3            =   3;

//  unavy
const int       RS_TIRED_SEQUENCE_VALUE_NONE        =   1;
const int       RS_TIRED_SEQUENCE_VALUE_MEDITATE    =   0;
const int       RS_TIRED_SEQUENCE_VALUE_SIT         =   -1;
const int       RS_TIRED_SEQUENCE_VALUE_NAP         =   -3;
const int       RS_TIRED_SEQUENCE_VALUE_SLEEP       =   -3;
const int       RS_TIRED_SEQUENCE_VALUE_FORCE_SLEEP =   -2;

//  animace
const int       RS_ANIMATION_REST_SLEEP             =   40;
const int       RS_ANIMATION_REST_FORCE_SLEEP       =   ANIMATION_LOOPING_DEAD_FRONT;
const int       RS_ANIMATION_REST_NAP               =   ANIMATION_LOOPING_DEAD_BACK;
const int       RS_ANIMATION_REST_MEDITATE          =   ANIMATION_LOOPING_MEDITATE;
const int       RS_ANIMATION_REST_SIT               =   ANIMATION_LOOPING_SIT_CROSS;

//  konec odpocinku
const int       RS_REST_SEQUENCES                   =   5;
const int       RS_REST_FATIGUE_NEEDED              =   80;
const int       RS_ON_RESTED_HP_HEALED_PERCENT      =   10;

//  hranice urovni unavy
const int       RS_FATIGUE_EDGE_FSLEEP              =   300;

