// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Constants                        |
// | File    || vg_s0_tis_const.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu TIS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_TIS_NAME                     =   "Text Interaction System";
const string    SYSTEM_TIS_PREFIX                   =   "TIS";
const string    SYSTEM_TIS_VERSION                  =   "1.00";
const string    SYSTEM_TIS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_TIS_DATE                     =   "17-01-2011";
const string    SYSTEM_TIS_UPDATE                   =   "17-01-2011";
string          TIS                                 =   SYSTEM_TIS_PREFIX;
string          TIS_                                =   TIS + "_";
string          _TIS                                =   "_" + TIS;
string          _TIS_                               =   "_" + TIS + "_";




// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string TIS_PARAMETER_CHECK                    = "CHECK";
const string TIS_PARAMETER_DO                       = "DO";

const string    TIS_TAG_TIS_OBJECT                  =   "VG_W_SY_TISOBJCT";
const string    TIS_TAG_TIS_SPAWN_MASTER            =   "VG_W_SY_TISMASTR";
const string    TIS_TAG_TIS_SPAWN_POINT             =   "VG_W_SY_TISSPAWN";
const string    TIS_TAG_DIALOG_INTERACTION_MENU     =   "VG_P_S1_TIS_CORE";
const string    TIS_RESREF_INTERACTION_POINT        =   "vg_w_sy_tisinter";
const float     TIS_SPAWN_RADIUS                    =   3.0f;
const int       TIS_SPAWN_TIME                      =   5;
const int       TIS_AREA_DETECT_OBJECTS             =   7;
const int       TIS_AREA_DETECT_ACTIONS             =   7;
const int       TIS_TOKEN_BASE                      =   700;
const float     TIS_INTERACT_DISTANCE               =   3.0f;
const float     TIS_INTERACT_SEQUENCE_DELAY         =   6.0f;

const int       TIS_DURABILITY_DECREASE_MINE        =   5;
const int       TIS_DURABILITY_DECREASE_ACQUIRE     =   1;

const int       TIS_CHECK_RESULT_ACTION_INVALID     =   1;
const int       TIS_CHECK_RESULT_ACTION_ACTION      =   5;
const int       TIS_CHECK_RESULT_ACTION_VECTOR      =   6;
const int       TIS_CHECK_RESULT_ACTION_DIST        =   7;
const int       TIS_CHECK_RESULT_ACTION_OBJECT      =   8;

// +---------------------------------------------------------------------------+
// |                             F U N C T I O N S                             |
// +---------------------------------------------------------------------------+

object __tis;
object __TIS();
object __TIS()
{
    if (!GetIsObjectValid(__tis))
        __tis = GetObjectByTag("SYSTEM" + _TIS_ + "OBJECT");

    return __tis;
}

