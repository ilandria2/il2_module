// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_getapp.nss                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "GETAPP"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    List2daValues(object oPC, string s2da, string sColumn, string sPattern, int nMaxRow = 2000, int nRow = 0, int nFound = 0, string sMessage = "")
{
    string  sValue;

    if  (sMessage   ==  "")
    sMessage    =  Y1 + "--------------------\n" + W2 + "Appearances in " + Y1 + s2da + ".2da" + W2 + " that match pattern [" + Y1 + sPattern + W2 + "]:";
    sValue      =   Get2DAString(s2da, sColumn, nRow);

    if  (sValue !=  "")
    {
        if  (TestStringAgainstPattern(sPattern, sValue))
        {
            nFound++;
            sMessage    +=  "\n" + Y1 + itos(nRow) + W2 + " - " + sValue;
        }
    }

//    else
    if  (nRow   >   nMaxRow)
    {
        if  (!nFound)
        sMessage    +=  "\n" + Y1 + "<No appearance found>";

        location    loc =   GetLocation(oPC);
        vector      vec =   GetPositionFromLocation(loc);
        object  oExam   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

        //msg(oPC, sMessage);
        SetDescription(oExam, sMessage);
        AssignCommand(oPC, ClearAllActions(TRUE));
        AssignCommand(oPC, ActionExamine(oExam));
        DelayCommand(2.0f, DestroyObject(oExam));
        return;
    }

    DelayCommand(0.001f, List2daValues(oPC, s2da, sColumn, sPattern, nMaxRow, ++nRow, nFound, sMessage));
}




void    main()
{
    string      sCmd    =   "GETAPP";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sValue      =   CCS_GetConvertedString(sLine, 1, FALSE);
    object  oCreature   =   CCS_GetConvertedObject(sLine, 1, FALSE);
    int     bValue      =   FALSE;
    string  sMessage;


    /*
        ;getapp t;
        ;getapp snake;

        ;setapp 23;body;t;
        ;setapp 14;tail;t;
    */


    //  neplatny objekt
    if  (!GetIsObjectValid(oCreature))
    {
        if  (len(sValue) < 3)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid target / to search for an appearance you have to write " + R1 + "at least 3 chars";

            msg(oPC, sMessage, TRUE, FALSE);
            return;
        }

        bValue  =   TRUE;
        sValue  =   "**" + sValue + "**";
    }

    else

    //  neplatny typ objektu
    if  (GetObjectType(oCreature)   !=  OBJECT_TYPE_CREATURE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Target must be a " + R1 + "creature";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    //  vypsani vzhledu cilove bytosti
    if  (!bValue)
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Creature appearance [" + G2 + GetName(oCreature) + ", " + GetTag(oCreature) + W2 + "]:";
        sMessage    +=  "\n" + W2 + "Body: " + G2 + itos(GetAppearanceType(oCreature)) + " - " + Get2DAString("appearance", "LABEL", GetAppearanceType(oCreature));
        sMessage    +=  "\n" + W2 + "Tail: " + G2 + itos(GetCreatureTailType(oCreature)) + " - " + Get2DAString("tailmodel", "LABEL", GetCreatureTailType(oCreature));
        sMessage    +=  "\n" + W2 + "Wings: " + G2 + itos(GetCreatureWingType(oCreature));
        sMessage    +=  "\n" + W2 + "Head: " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_HEAD, oCreature));
        sMessage    +=  "\n" + W2 + "Neck: " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_NECK, oCreature));
        sMessage    +=  "\n" + W2 + "Torso: " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_TORSO, oCreature));
        sMessage    +=  "\n" + W2 + "Pelvis: " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_PELVIS, oCreature));
        sMessage    +=  "\n" + W2 + "Biceps (R): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_RIGHT_BICEP, oCreature)) + W2 + " / (L): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_LEFT_BICEP, oCreature));
        sMessage    +=  "\n" + W2 + "Forearms (R): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_RIGHT_FOREARM, oCreature)) + W2 + " / (L): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_LEFT_FOREARM, oCreature));
        sMessage    +=  "\n" + W2 + "Hands (R): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_RIGHT_HAND, oCreature)) + W2 + " / (L): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_LEFT_HAND, oCreature));
        sMessage    +=  "\n" + W2 + "Thighs (R): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_RIGHT_THIGH, oCreature)) + W2 + " / (L): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_LEFT_THIGH, oCreature));
        sMessage    +=  "\n" + W2 + "Shins (R): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_RIGHT_SHIN, oCreature)) + W2 + " / (L): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_LEFT_SHIN, oCreature));
        sMessage    +=  "\n" + W2 + "Foots (R): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_RIGHT_FOOT, oCreature)) + W2 + " / (L): " + G2 + itos(GetCreatureBodyPart(CREATURE_PART_LEFT_FOOT, oCreature));
    }

    //  hledani klicoveho slova v appearance.2da
    else
    {
        List2daValues(oPC, "appearance", "LABEL", sValue, 2000);
    }

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
