// +---------++----------------------------------------------------------------+
// | Name    || Object Events System (OES) System init                         |
// | File    || vg_s1_oes_init.nss                                             |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-12-2007                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system OES
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_OES_PREFIX;
    string  sName       =   SYSTEM_OES_NAME;
    string  sVersion    =   SYSTEM_OES_VERSION;
    string  sAuthor     =   SYSTEM_OES_AUTHOR;
    string  sDate       =   SYSTEM_OES_DATE;
    string  sUpDate     =   SYSTEM_OES_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_oes_info");
//    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_oes_data_c");
//    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_oes_data_s");
//    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_oes_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
        CheckPersistentTables(sSystem);

    sls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_D_ON_AREA_TRANSITION) + "_SCRIPT", "vg_s1_oes_transi", -2);
    sls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_R_ON_CLICK) + "_SCRIPT", "vg_s1_oes_transi", -2);
    sls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CUSTOM_LOOP) + "_SCRIPT", "vg_s1_oes_savedt", -2);
    sls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_oes_loaddt", -2);

    //  Area
    sls(oSystem, sSystem + "_EVENT", "A_ENTER", -2);
    sls(oSystem, sSystem + "_EVENT", "A_EXIT", -2);
    sls(oSystem, sSystem + "_EVENT", "A_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "A_USER_DEFINED", -2);

    //  Creature
    sls(oSystem, sSystem + "_EVENT", "C_BLOCKED", -2);
    sls(oSystem, sSystem + "_EVENT", "C_COMBAT_ROUND_END", -2);
    sls(oSystem, sSystem + "_EVENT", "C_CONVERSATION", -2);
    sls(oSystem, sSystem + "_EVENT", "C_DAMAGED", -2);
    sls(oSystem, sSystem + "_EVENT", "C_DEATH", -2);
    sls(oSystem, sSystem + "_EVENT", "C_DISTURBED", -2);
    sls(oSystem, sSystem + "_EVENT", "C_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "C_PERCEPTION", -2);
    sls(oSystem, sSystem + "_EVENT", "C_PHYSICAL_ATTACKED", -2);
    sls(oSystem, sSystem + "_EVENT", "C_RESTED", -2);
    sls(oSystem, sSystem + "_EVENT", "C_SPAWN", -2);
    sls(oSystem, sSystem + "_EVENT", "C_SPELL_CAST_AT", -2);
    sls(oSystem, sSystem + "_EVENT", "C_USER_DEFINED", -2);

    //  Door
    sls(oSystem, sSystem + "_EVENT", "D_AREA_TRANSITION_CLICK", -2);
    sls(oSystem, sSystem + "_EVENT", "D_CLOSED", -2);
    sls(oSystem, sSystem + "_EVENT", "D_DAMAGED", -2);
    sls(oSystem, sSystem + "_EVENT", "D_DEATH", -2);
    sls(oSystem, sSystem + "_EVENT", "D_FAIL_TO_OPEN", -2);
    sls(oSystem, sSystem + "_EVENT", "D_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "D_LOCK", -2);
    sls(oSystem, sSystem + "_EVENT", "D_OPEN", -2);
    sls(oSystem, sSystem + "_EVENT", "D_PHYSICAL_ATTACKED", -2);
    sls(oSystem, sSystem + "_EVENT", "D_SPELL_CAST_AT", -2);
    sls(oSystem, sSystem + "_EVENT", "D_UNLOCK", -2);
    sls(oSystem, sSystem + "_EVENT", "D_USER_DEFINED", -2);

    //  Encounter
    sls(oSystem, sSystem + "_EVENT", "E_ENTER", -2);
    sls(oSystem, sSystem + "_EVENT", "E_EXHAUSTED", -2);
    sls(oSystem, sSystem + "_EVENT", "E_EXIT", -2);
    sls(oSystem, sSystem + "_EVENT", "E_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "E_USER_DEFINED", -2);

    //  Merchant
    sls(oSystem, sSystem + "_EVENT", "M_OPEN", -2);
    sls(oSystem, sSystem + "_EVENT", "M_CLOSED", -2);

    //  Module
    sls(oSystem, sSystem + "_EVENT", "O_ACQUIRE_ITEM", -2);
    sls(oSystem, sSystem + "_EVENT", "O_ACTIVATE_ITEM", -2);
    sls(oSystem, sSystem + "_EVENT", "O_CLIENT_ENTER", -2);
    sls(oSystem, sSystem + "_EVENT", "O_CLIENT_LEAVE", -2);
    sls(oSystem, sSystem + "_EVENT", "O_CUTSCENE_ABORT", -2);
    sls(oSystem, sSystem + "_EVENT", "O_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "O_MODULE_LOAD", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_CHAT", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_DEATH", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_DYING", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_EQUIP_ITEM", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_LEVEL_UP", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_RESPAWN", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_REST", -2);
    sls(oSystem, sSystem + "_EVENT", "O_PLAYER_UNEQUIP_ITEM", -2);
    sls(oSystem, sSystem + "_EVENT", "O_UNACQUIRE_ITEM", -2);
    sls(oSystem, sSystem + "_EVENT", "O_USER_DEFINED", -2);

    //  Placeable
    sls(oSystem, sSystem + "_EVENT", "P_CLICK", -2);
    sls(oSystem, sSystem + "_EVENT", "P_CLOSED", -2);
    sls(oSystem, sSystem + "_EVENT", "P_DAMAGED", -2);
    sls(oSystem, sSystem + "_EVENT", "P_DEATH", -2);
    sls(oSystem, sSystem + "_EVENT", "P_DISTURBED", -2);
    sls(oSystem, sSystem + "_EVENT", "P_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "P_LOCK", -2);
    sls(oSystem, sSystem + "_EVENT", "P_OPEN", -2);
    sls(oSystem, sSystem + "_EVENT", "P_PHYSICAL_ATTACKED", -2);
    sls(oSystem, sSystem + "_EVENT", "P_SPELL_CAST_AT", -2);
    sls(oSystem, sSystem + "_EVENT", "P_UNLOCK", -2);
    sls(oSystem, sSystem + "_EVENT", "P_USED", -2);
    sls(oSystem, sSystem + "_EVENT", "P_USER_DEFINED", -2);

    //  Trap
    sls(oSystem, sSystem + "_EVENT", "T_DISARM", -2);
    sls(oSystem, sSystem + "_EVENT", "T_TRIGGERED", -2);

    //  Trigger
    sls(oSystem, sSystem + "_EVENT", "R_AREA_TRANSITION_CLICK", -2);
    sls(oSystem, sSystem + "_EVENT", "R_CLICK", -2);
    sls(oSystem, sSystem + "_EVENT", "R_ENTER", -2);
    sls(oSystem, sSystem + "_EVENT", "R_EXIT", -2);
    sls(oSystem, sSystem + "_EVENT", "R_HEARTBEAT", -2);
    sls(oSystem, sSystem + "_EVENT", "R_USER_DEFINED", -2);

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}

