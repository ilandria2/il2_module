// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_description.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-07-2009                                                     |
// | Updated || 17-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "DESCRIPTION"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "DESCRIPTION";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sDesc   =   CCS_GetConvertedString(sLine, 1, FALSE);
    int     bIdent  =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    object  oObject =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sMessage;

    //  Neplatny objekt
    if  (!GetIsObjectValid(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    string  sOldDesc    =   GetDescription(oObject, FALSE, bIdent);
    string  sType       =   (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)   ?   "Object description" :
                            (bIdent)    ?   "Identified item description" :   "Unidentified item description";

    if  (sDesc  ==  "GET")
    sMessage    =   Y1 + "( ? ) " + W2 + sType + " [" + G2 + GetTag(oObject) + " / " + GetName(oObject) + W2 + "] is [" + G2 + sOldDesc + W2 + "]";

    else
    {
        SetDescription(oObject, sDesc, bIdent);

        string  sNewDesc    =   GetDescription(oObject, FALSE, bIdent);

        sMessage    =   Y1 + "( ? ) " + W2 + sType + " [" + G2 + GetName(oObject) + W2 + "] changed from [" + G2 + sOldDesc + W2 + "] to [" + G2 + sNewDesc + W2 + "]";
    }

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
