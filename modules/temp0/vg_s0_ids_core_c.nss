// +---------++----------------------------------------------------------------+
// | Name    || Item Durability System (IDS) Core C                            |
// | File    || vg_s0_ids_core_c.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//    Jadro systemu IDS typu C (command)
// -----------------------------------------------------------------------------

#include    "vg_s0_ids_const"
#include    "vg_s0_sys_core_c"
#include    "x2_inc_itemprop"
#include "vg_s0_sys_core_d"

// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+

// Checks placed items within area and removes them from DB if they don't exist anymore
void IDS_CheckPlacedItemsInArea(string areaTag, int onlyDead = TRUE);

// Converts item to simplier structure used for creating placed items
struct IDS_RawItem IDS_GetRawItem(object item);

// Creates placed item based on raw structure
object IDS_CreatePlacedItemRaw(struct IDS_RawItem i);

// Gets the object reference to the placed item instance identified by itemId within the placedList
object IDS_GetPlacedItem(int itemId, object placedList);

// Gets itemId stored on the placedList at a specific row number
int IDS_GetPlacedItemByRow(object placedList, int row);

// Gets the row position within the list of placed items
int IDS_GetPlacedItemRow(object placedList, int itemId);

// Adds or sets itemId to the list of placed items and returns its row position from the list
int IDS_AddOrSetPlacedItem(object placedList, int itemId);

// Handles OnDamaged event for placed_item
void IDS_HandleDamagedEventForPlacedItem(object placed_item);

// Gets the object that holds the list of placed items for area identified by areaTag
object IDS_GetPlacedItemList(string areaTag);

// Gets the IDS_PLACED_ITEM_STATE_* value of a placed item identified by it's object_id and areaTag
int IDS_GetPlacedItemState(int itemId, object placedList);

// Sets the IDS_PLACED_ITEM_STATE_* value of a placed item
void IDS_SetPlacedItemState(object placedItem, int itemState, object placedList);

// Sets the IDS_PLACED_ITEM_STATE_* value of a placed item identified by it's object_id and areaTag
void IDS_SetPlacedItemStateById(int itemId, int itemState, object placedList);

// Gets resref of placeable that should represent baseItem or resRefItem when they are dropped on ground
string IDS_GetPlacedResRef(int baseItem = -1, string resRefItem = "");

// If the item is dropped (owner is invalid) it will be replaced by an interactive placeable object
void IDS_ItemToInteractiveObject(object item);

// Gets the INVENTORY_SLOT_* constant identified by row
int IDS_GetSupportedSlotForComposedClothes(int row);

// Get's the XnnnXnnn part from item's TAG (16-32 chars)
string IDS_GetComposedAppearance(object item, int pos);

// Get's the X part from item's composed appearance string
string IDS_GetComposedAppearanceType(string app);

// Get's the nnn part from item's composed appearance string
int IDS_GetComposedAppearanceValue(string app);

// Determines whether creature has some item equipped that alters model's appearance
int IDS_IsArmorPartCovered(object creature, int model);

// Gets default appearance type of a specific model of an armor identified by armorTag
int IDS_GetArmorPartDefaultAppearance(string armorTag, int model);

// Restores default armor item appearance for a specific model part
object IDS_RestoreDefaultAppearancePart(object armor, int model);

// Restores default armor item appearance
// if full = FALSE then only restores those parts not hidden by other equipped items
object IDS_RestoreDefaultAppearance(object armor, int full = TRUE);

// Destroys the naked armor instance creature has equipped or posseses in his/her inventory
void IDS_DestroyNakedArmor(object creature);

// Ensures that the creature has the naked body armor equipped
void IDS_EquipNakedArmor(object creature);

// Modifies armor's model part appearance and destroys the original
object IDS_ModifyArmorAppearance(object armor, int type, int model, int value);

// Translates ITEM_APPR_ARMOR_MODEL_* constant into letter representation (A=neck, etc.)
string IDS_ModelToAppType(int model);

// Translates ITEM_APPR_ARMOR_MODEL_* constant into it's string version (RSHIN, LFOOT, etc.)
string IDS_ModelToString(int type, int model);

// Translates AppType (letter representation (A=neck, etc.)) into ITEM_APPR_ARMOR_MODEL_* constant
int IDS_AppTypeToModel(string appType);

// With a small delay fires a procedure that composes equipped clothes appearance based on equipped items
void IDS_ComposeClothes(object creature);

// Copies oItem on oCreature's location and destroys the original afterwards
object IDS_DropItem(object oCreature, object oItem, float delay = 0.0f, int alsoMessage = TRUE);

// Gets the inventory capacity - slots used
int IDS_GetFreeSpace(object oPC);

// Refreshes the oPC's inventory capacity:
// ** sets INV_SLOTS to current size
// ** sets INV_SLOTS_MAX to current capacity given by equiped items with bonus on inventory capacity
// ** returns size - capacity
int IDS_RefreshInventoryCapacity(object oPC);

// Gets the inventory capacity amount that the item gives to possessor
// ** if oItem is invalid then uses resRef param to lookup the meta property
int IDS_GetInventoryCapacityBonusOfItem(object oItem, string resRef = "");

// Gets the inventory capacity amount that received from items
int IDS_GetInventoryCapacityBonus(object oPC);

// Gets the inventory slots used by items in oPC's inventory
int IDS_GetInventorySlotsUsed(object oPC);

// delays an inventory check and drops items exceeding player limits
void IDS_CleanInventory(object player);

// determines inventory capcity limit for player
int IDS_GetItemsLimit(object player);



// +---------++----------------------------------------------------------------+
// | Name    || IDS_UpdateItem                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Aktualizuje durabilitu u predmetu
//  nValue - nynejsi hodnota durability
//  nMax - maximalni hodnota durability
//
// -----------------------------------------------------------------------------
void    IDS_UpdateItem(object oPC, object oItem, int nValue, int nMax, int bMessage = TRUE);

// +---------++----------------------------------------------------------------+
// | Name    || IDS_UpdateItemsInInventory                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Projde vsechny/equipnute predmety v inventari oPC a snizi jejich durabilitu
//  (zatim jenom pochodne a jidlo)
//
// -----------------------------------------------------------------------------
void    IDS_UpdateItemsInInventory(object oPC, int bAll = TRUE);

// +---------++----------------------------------------------------------------+
// | Name    || IDS_GetItemValue                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zjistil aktualni/maximalni hodnotu durability predmetu oItem (0-1000[-])
//
// -----------------------------------------------------------------------------
int     IDS_GetItemValue(object oItem, int bMax = FALSE);

// +---------++----------------------------------------------------------------+
// | Name    || IDS_GetItemStatus                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zjistil aktualni hodnotu stavu durability (0-100[%])
//
// -----------------------------------------------------------------------------
int     IDS_GetItemStatus(object oItem);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+


void IDS_CheckPlacedItemsInAreaPerform(object placedList, int onlyDead = TRUE)
{
    if (gli(GetModule(), "itsavex")){sli(GetModule(),"itsavex",0);SpawnScriptDebugger();}
    int item_row = 1;
    int item_id = IDS_GetPlacedItemByRow(placedList, item_row);

    if (item_id)
    {
        if (gli(GetFirstPC(), "itsave"))SpawnScriptDebugger();
        // loop through all placed items
        while (item_id)
        {
            object item_placed = IDS_GetPlacedItem(item_id, placedList);
            int item_state = IDS_GetPlacedItemState(item_id, placedList);
            //msg(GetFirstPC(), "VAR.SX1: " + itos(var) + ": " + itos(item_id) + "=" + itos(item_state) + ": ex=" + itos(GetIsObjectValid(item_placed)) + " (" + GetName(item_placed) + _C + ")");

            // placed item still exists
            if (GetIsObjectValid(item_placed))
            {
                // item was changed - persist it
                if (!onlyDead &&
                item_state != IDS_PLACED_ITEM_STATE_SPAWNED &&
                item_state != IDS_PLACED_ITEM_STATE_DEFAULT)
                    DelayCommand(0.01 * item_row, SavePersistentData(item_placed, IDS, itos(item_state)));
            }

            // placed item doesn't exist anymore - delete from DB - use IDS system object as caller
            else if (item_state != IDS_PLACED_ITEM_STATE_DESTROYED)
                DelayCommand(0.01 * item_row, SavePersistentData(placedList, IDS, itos(item_id)));

            item_id = IDS_GetPlacedItemByRow(placedList, ++item_row);
        }
    }

    sli(placedList, IDS_ + "CHECKING_AREA", FALSE);
}

void IDS_CheckPlacedItemsInAreaCheck(string areaTag, int onlyDead = TRUE)
{
    object placedList = IDS_GetPlacedItemList(areaTag);
    if (gli(placedList, IDS_ + "CHECKING_AREA"))
        return;

    sli(placedList, IDS_ + "CHECKING_AREA", TRUE);
    DelayCommand(3.0, IDS_CheckPlacedItemsInAreaPerform(placedList, onlyDead));
}


void IDS_CheckPlacedItemsInArea(string areaTag, int onlyDead = TRUE)
{
    RunScript("vg_s1_ids_arechk", GetObjectByTag(areaTag), itos(onlyDead));
}


void IDS_HandleDamagedEventForPlacedItem(object placed_item)
{
    if (gli(GetFirstPC(), "itdmg"))SpawnScriptDebugger();
    // ignore non placed-items
    if (!gli(placed_item, IDS_ + "ITEM"))
        return;

    ////// item 'sack pants' example:
    // dbmax = 150
    int item_dbmax = gli(placed_item, IDS_ + "ITEM_DBMAX");

    // dbcur = 80
    int item_dbcur = gli(placed_item, IDS_ + "ITEM_DBCUR");

    int plc_hpmin = 0;
    int plc_hpnew = GetCurrentHitPoints(placed_item);

    // weight-based items
    int bWeight = IsWeightBasedItemType(gli(placed_item, IDS_ + "ITEM_TYPE"));
    if (bWeight)
        plc_hpmin = IDS_PLACEABLE_MAX_HITPOINTS - gli(placed_item, IDS_ + "ITEM_WEIGHT") / IDS_ITEM_DURABILITY_HP_RATIO;

    // stack-based items
    // hpmin = hpmax - dbmax / hprat = 10000 - 150 / 10 = 10000 - 15 = 9985 (death border)
    else
        plc_hpmin = IDS_PLACEABLE_MAX_HITPOINTS - ftoi(itof(item_dbmax) / IDS_ITEM_DURABILITY_HP_RATIO);

    // item ID will be marked as either changed or destroyed and later a massive IDS custom loop will handle database synchronization
    string item_tech = gls(placed_item, IDS_ + "ITEM_TECH");
    int item_id = GetTechPart(item_tech, TECH_PART_OBJECT_ID);
    string area_tag = GetTag(GetArea(placed_item));

    // flag itemID as changed (default)
    int item_flag = IDS_PLACED_ITEM_STATE_SPAWNED;

    // placed item didnt survive damage - flag as destroyed for later DB sequence and physically destroy it
    if (plc_hpnew < plc_hpmin)
        Destroy(placed_item, 0.1);

    // placed item survived damage - update durab
    else
    {
        item_flag = IDS_PLACED_ITEM_STATE_CHANGED;

        // for weighted items decrease weight
        if (bWeight)
        {
            int damage = GetTotalDamageDealt();
            int item_wnew = gli(placed_item, IDS_ + "ITEM_WEIGHT") - damage * IDS_ITEM_DURABILITY_HP_RATIO;
            sli(placed_item, IDS_ + "ITEM_WEIGHT", item_wnew);
        }

        // for stack items decrease durability
        else
        {
            int item_dbnew = item_dbmax - (IDS_PLACEABLE_MAX_HITPOINTS - plc_hpnew) * IDS_ITEM_DURABILITY_HP_RATIO;
            sli(placed_item, IDS_ + "ITEM_DBCUR", item_dbnew);
        }
    }

    // set the itemID flag for persistance updating
    IDS_SetPlacedItemState(placed_item, item_flag, IDS_GetPlacedItemList(area_tag));
    IDS_CheckPlacedItemsInArea(area_tag, FALSE);
}

object IDS_GetPlacedItemList(string areaTag)
{
    return GetObjectByTag(IDS_ + "AREA_" + areaTag);
}

int IDS_GetPlacedItemByRow(object placedList, int row)
{
    return gli(placedList, IDS_ + "ITEM", row);
}

int IDS_GetPlacedItemRow(object placedList, int itemId)
{
    return gli(placedList, IDS_ + "ITEM_ROW", itemId);
}

int IDS_AddOrSetPlacedItem(object placedList, int itemId)
{
    int item_row = IDS_GetPlacedItemRow(placedList, itemId);
    if (!item_row)
    {
        sli(placedList, IDS_ + "ITEM", itemId, -2);
        item_row = gli(placedList, IDS_ + "ITEM_I_ROWS");
    }

    sli(placedList, IDS_ + "ITEM_ROW", item_row, itemId);
    return item_row;
}

object IDS_GetPlacedItem(int itemId, object placedList)
{
    int item_row = IDS_GetPlacedItemRow(placedList, itemId);
    return glo(placedList, IDS_ + "ITEM_PLACED", item_row);
}

int IDS_GetPlacedItemState(int itemId, object placedList)
{
    int item_row = IDS_GetPlacedItemRow(placedList, itemId);
    return gli(placedList, IDS_ + "ITEM_STATE", item_row);
}

void IDS_SetPlacedItemStateById(int itemId, int itemState, object placedList)
{
    int item_row = IDS_AddOrSetPlacedItem(placedList, itemId);
    sli(placedList, IDS_ + "ITEM_STATE", itemState, item_row);
}

void IDS_SetPlacedItemState(object placedItem, int itemState, object placedList)
{
    int itemId = GetTechPart(gls(placedItem, IDS_ + "ITEM_TECH"), TECH_PART_OBJECT_ID);
    int item_rowold = IDS_GetPlacedItemRow(placedList, itemId);
    int item_row = IDS_AddOrSetPlacedItem(placedList, itemId);
    sli(placedList, IDS_ + "ITEM_STATE", itemState, item_row);
    slo(placedList, IDS_ + "ITEM_PLACED", placedItem, item_row);

    // new item - persist asap
    if (!item_rowold && item_row && gli(placedList, IDS_ + "LOADED"))
        DelayCommand(0.5, SavePersistentData(placedItem, IDS, itos(itemState)));
}

int IDS_GetArmorDefaultsInitialized(string armorTag)
{
    return gli(__IDS(), IDS_ + "ARMOR_" + armorTag + "_INIT");
}

void IDS_InitArmorDefaults(string armorTag)
{
    location loc = Location(GetObjectByTag(SYS_TAG_SYSTEMS_AREA), Vector(2.0, 3.0, 1.0), 0.0);
    object armor = CreateObject(OBJECT_TYPE_ITEM, armorTag, loc);

    if (!GetIsObjectValid(armor))
        return;

    string tag = GetTag(armor);
    int model = 0;
    for (model = 0; model < ITEM_APPR_ARMOR_NUM_MODELS; model++)
    {
        int curValue = GetItemAppearance(armor, ITEM_APPR_TYPE_ARMOR_MODEL, model);
        sli(__IDS(), IDS_ + "ARMOR_" + tag + "_APP", curValue, model + 1);
    }

    sli(__IDS(), IDS_ + "ARMOR_" + tag + "_INIT", TRUE);
    Destroy(armor, 1.0);
}

int IDS_GetSupportedSlotForComposedClothes(int row)
{
    switch (row)
    {
        case 0: return INVENTORY_SLOT_BOOTS;
        case 1: return INVENTORY_SLOT_BELT;
        case 2: return INVENTORY_SLOT_ARMS;
        case 3: return INVENTORY_SLOT_CLOAK;
    }

    return -1;
}


int IDS_IsArmorPartCovered(object creature, int model)
{
    if (!GetIsObjectValid(creature))
        return -1;

    // loop through all supported slots
    int row = 0;
    int slot = IDS_GetSupportedSlotForComposedClothes(row++);
    while (slot != -1)
    {
        object item = GetItemInSlot(slot, creature);
        slot = IDS_GetSupportedSlotForComposedClothes(row++);

        if (!GetIsObjectValid(item))
            continue;

        // loop through all defined Apps on current item slot
        int pos = 0;
        string app = IDS_GetComposedAppearance(item, pos);
        while (app != "")
        {
            string appType = IDS_GetComposedAppearanceType(app);
            int appModel = IDS_AppTypeToModel(appType);
            int bothSides = appModel < 0 && appModel > -255;
            appModel = abs(appModel);
            if (appModel == model)
                return TRUE;

            if (bothSides && (appModel + 1) == model)
                return TRUE;

            app = IDS_GetComposedAppearance(item, ++pos);
        }
    }

    return FALSE;
}

int IDS_GetArmorPartDefaultAppearance(string armorTag, int model)
{
    if (armorTag == IDS_TAG_ITEM_NAKED_BODY)
        return 1;

    if (!IDS_GetArmorDefaultsInitialized(armorTag))
        IDS_InitArmorDefaults(armorTag);
    return gli(__IDS(), IDS_ + "ARMOR_" + armorTag + "_APP", model + 1);
}

object IDS_RestoreDefaultAppearancePart(object armor, int model)
{
    int defValue = IDS_GetArmorPartDefaultAppearance(GetTag(armor), model);
    int curValue = GetItemAppearance(armor, ITEM_APPR_TYPE_ARMOR_MODEL, model);

    if (curValue != defValue)
        return IDS_ModifyArmorAppearance(armor, ITEM_APPR_TYPE_ARMOR_MODEL, model, defValue);
    return armor;
}

object IDS_RestoreDefaultAppearance(object armor, int full = TRUE)
{
    string tag = GetTag(armor);
    if (tag == "")
        return OBJECT_INVALID;

    object creature = GetItemPossessor(armor);

     // initialize defaults if needed
    if (tag != IDS_TAG_ITEM_NAKED_BODY)
    {
        if (!IDS_GetArmorDefaultsInitialized(tag))
            IDS_InitArmorDefaults(tag);

        // save last used colors from current armor on player
        // so when naked armor is equiped it can be recolored to match the previous armor
        if (GetIsObjectValid(creature))
        {
            int c;
            for (c = 0; c < ITEM_APPR_ARMOR_NUM_COLORS; c++)
                sli(creature, SYSTEM_IDS_PREFIX + "_LAST_COLOR", GetItemAppearance(armor, ITEM_APPR_TYPE_ARMOR_COLOR, c), c);
        }
    }

    // detect whether it was originally equipped
    int wasEquipped = GetItemInSlot(INVENTORY_SLOT_CHEST, creature) == armor;

    // loop through all armor parts
    int model = 0;
    for (model = 0; model < ITEM_APPR_ARMOR_NUM_MODELS; model++)
    {
        if (!full)
        {
            int covered = IDS_IsArmorPartCovered(creature, model);
            if (covered)
                continue;
        }

        // restore default part if not set yet
        armor = IDS_RestoreDefaultAppearancePart(armor, model);
    }

    // re-equip
    //if (wasEquipped && GetItemInSlot(INVENTORY_SLOT_CHEST, creature) != armor)
    //    DelayCommand(0.1, AssignCommand(creature, ActionEquipItem(armor, INVENTORY_SLOT_CHEST)));

    return armor;
}

void IDS_DestroyNakedArmor(object creature)
{
    object armor = GetItemInSlot(INVENTORY_SLOT_CHEST, creature);

    if (GetIsObjectValid(armor))
    {
        string tag = GetTag(armor);
        if (tag == IDS_TAG_ITEM_NAKED_BODY)
        {
            Destroy(armor);
            return;
        }
    }

    armor = GetItemPossessedBy(creature, IDS_TAG_ITEM_NAKED_BODY);
    if (!GetIsObjectValid(armor))
        return;

    Destroy(armor);
}

void IDS_EquipNakedArmor(object creature)
{
    IDS_DestroyNakedArmor(creature);
    object naked = CreateItemOnObject(IDS_TAG_ITEM_NAKED_BODY, creature);
    AssignCommand(creature, ActionEquipItem(naked, INVENTORY_SLOT_CHEST));
}

string IDS_GetComposedAppearance(object item, int pos)
{
    return sub(GetTag(item), 16 + pos * 4, 4);
}

string IDS_GetComposedAppearanceType(string app)
{
    return left(app, 1);
}

int IDS_GetComposedAppearanceValue(string app)
{
    if (app == "")
        return -1;
    return stoi(sub(app, 1, 3));
}

string IDS_ModelToString(int type, int model)
{
    if (type == ITEM_APPR_TYPE_ARMOR_MODEL)
    {
        switch (model)
        {
            case ITEM_APPR_ARMOR_MODEL_RFOOT: return "RFOOT";
            case ITEM_APPR_ARMOR_MODEL_LFOOT: return "LFOOT";
            case ITEM_APPR_ARMOR_MODEL_RSHIN: return "RSHIN";
            case ITEM_APPR_ARMOR_MODEL_LSHIN: return "LSHIN";
            case ITEM_APPR_ARMOR_MODEL_LTHIGH: return "LTHIGH";
            case ITEM_APPR_ARMOR_MODEL_RTHIGH: return "RTHIGH";
            case ITEM_APPR_ARMOR_MODEL_PELVIS: return "PELVIS";
            case ITEM_APPR_ARMOR_MODEL_TORSO: return "TORSO";
            case ITEM_APPR_ARMOR_MODEL_BELT: return "BELT";
            case ITEM_APPR_ARMOR_MODEL_NECK: return "NECK";
            case ITEM_APPR_ARMOR_MODEL_RFOREARM: return "RFOREARM";
            case ITEM_APPR_ARMOR_MODEL_LFOREARM: return "LFOREARM";
            case ITEM_APPR_ARMOR_MODEL_RBICEP: return "RBICEP";
            case ITEM_APPR_ARMOR_MODEL_LBICEP: return "LBICEP";
            case ITEM_APPR_ARMOR_MODEL_RSHOULDER: return "RSHOULDER";
            case ITEM_APPR_ARMOR_MODEL_LSHOULDER: return "LSHOULDER";
            case ITEM_APPR_ARMOR_MODEL_RHAND: return "RHAND";
            case ITEM_APPR_ARMOR_MODEL_LHAND: return "LHAND";
            case ITEM_APPR_ARMOR_MODEL_ROBE: return "ROBE";
        }
    }
    else if (type == ITEM_APPR_TYPE_ARMOR_COLOR)
    {
        switch (model)
        {
            case ITEM_APPR_ARMOR_COLOR_CLOTH1: return "CLOTH1";
            case ITEM_APPR_ARMOR_COLOR_CLOTH2: return "CLOTH2";
            case ITEM_APPR_ARMOR_COLOR_LEATHER1: return "LEATHER1";
            case ITEM_APPR_ARMOR_COLOR_LEATHER2: return "LEATHER2";
            case ITEM_APPR_ARMOR_COLOR_METAL1: return "METAL1";
            case ITEM_APPR_ARMOR_COLOR_METAL2: return "METAL2";
        }
    }
    return "";
}

int IDS_AppTypeToModel(string appType)
{
    string appTypes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int appTypeId = FindSubString(appTypes, appType);
    switch (appTypeId)
    {
        case 0: return ITEM_APPR_ARMOR_MODEL_NECK;
        case 1: return ITEM_APPR_ARMOR_MODEL_TORSO;
        case 2: return ITEM_APPR_ARMOR_MODEL_BELT;
        case 3: return ITEM_APPR_ARMOR_MODEL_PELVIS;
        case 4: return ITEM_APPR_ARMOR_MODEL_RSHOULDER;
        case 5: return ITEM_APPR_ARMOR_MODEL_RBICEP;
        case 6: return ITEM_APPR_ARMOR_MODEL_RFOREARM;
        case 7: return ITEM_APPR_ARMOR_MODEL_RHAND;
        case 8: return ITEM_APPR_ARMOR_MODEL_LSHOULDER;
        case 9: return ITEM_APPR_ARMOR_MODEL_LBICEP;
        case 10: return ITEM_APPR_ARMOR_MODEL_LFOREARM;
        case 11: return ITEM_APPR_ARMOR_MODEL_LHAND;
        case 12: return ITEM_APPR_ARMOR_MODEL_RTHIGH;
        case 13: return ITEM_APPR_ARMOR_MODEL_RSHIN;
        case 14: return ITEM_APPR_ARMOR_MODEL_RFOOT;
        case 15: return ITEM_APPR_ARMOR_MODEL_LTHIGH;
        case 16: return ITEM_APPR_ARMOR_MODEL_LSHIN;
        case 17: return ITEM_APPR_ARMOR_MODEL_LFOOT;
        case 18: return ITEM_APPR_ARMOR_MODEL_ROBE;

        // below are for both Left and Right variants (negative is indicator of increment by +1 after modified and repeat)
        case 19: return -ITEM_APPR_ARMOR_MODEL_RSHOULDER;
        case 20: return -ITEM_APPR_ARMOR_MODEL_RBICEP;
        case 21: return -ITEM_APPR_ARMOR_MODEL_RFOREARM;
        case 22: return -ITEM_APPR_ARMOR_MODEL_RHAND;
        case 23: return -ITEM_APPR_ARMOR_MODEL_LTHIGH;
        case 24: return -ITEM_APPR_ARMOR_MODEL_RSHIN;
        case 25: return -ITEM_APPR_ARMOR_MODEL_RFOOT;
    }

    // some invalid
    return -255;
}

string IDS_ModelToAppType(int model)
{
    switch (model)
    {
        case ITEM_APPR_ARMOR_MODEL_NECK: return "A";
        case ITEM_APPR_ARMOR_MODEL_TORSO: return "B";
        case ITEM_APPR_ARMOR_MODEL_BELT: return "C";
        case ITEM_APPR_ARMOR_MODEL_PELVIS: return "D";
        case ITEM_APPR_ARMOR_MODEL_RSHOULDER: return "E";
        case ITEM_APPR_ARMOR_MODEL_RBICEP: return "F";
        case ITEM_APPR_ARMOR_MODEL_RFOREARM: return "G";
        case ITEM_APPR_ARMOR_MODEL_RHAND: return "H";
        case ITEM_APPR_ARMOR_MODEL_LSHOULDER: return "I";
        case ITEM_APPR_ARMOR_MODEL_LBICEP: return "J";
        case ITEM_APPR_ARMOR_MODEL_LFOREARM: return "K";
        case ITEM_APPR_ARMOR_MODEL_LHAND: return "L";
        case ITEM_APPR_ARMOR_MODEL_RTHIGH: return "M";
        case ITEM_APPR_ARMOR_MODEL_RSHIN: return "N";
        case ITEM_APPR_ARMOR_MODEL_RFOOT: return "O";
        case ITEM_APPR_ARMOR_MODEL_LTHIGH: return "P";
        case ITEM_APPR_ARMOR_MODEL_LSHIN: return "Q";
        case ITEM_APPR_ARMOR_MODEL_LFOOT: return "R";
        case ITEM_APPR_ARMOR_MODEL_ROBE: return "S";
    }
    return "";
}

object IDS_ModifyArmorAppearance(object armor, int type, int model, int value)
{
    int curVal = GetItemAppearance(armor, type, model);
    //msg(GetItemPossessor(armor), C2 + GetName(armor) + ": Changing type " + itos(type) + " model " + IDS_ModelToString(type, model) + " from " + itos(curVal) + " to " + itos(value));
    object armorNew = CopyItemAndModify(armor, type, model, value, TRUE);

    if (!GetIsObjectValid(armorNew))
        return armor;

    Destroy(armor);
    return armorNew;
}


void IDS_ComposeClothesCheck(object creature)
{
    if (gli(creature, "compose"))SpawnScriptDebugger();
    if (!GetIsObjectValid(creature) ||
        GetCurrentHitPoints(creature) <= 0 ||
        !GetHasInventory(creature))
        return;

    // if naked, equip naked armor
    object armor = GetItemInSlot(INVENTORY_SLOT_CHEST, creature);
    if (!GetIsObjectValid(armor))
    {
        IDS_EquipNakedArmor(creature);

        // restart the sequence in a while and interupt this
        DelayCommand(0.1, IDS_ComposeClothesCheck(creature));
        return;
    }

    // if got some armor equiped
    else
    {
        // if not naked armor then destroy any naked armors left to rot in inventory
        if (GetTag(armor) != IDS_TAG_ITEM_NAKED_BODY)
            IDS_DestroyNakedArmor(creature);

        // restore armor's parts not covered by composite items
        armor = IDS_RestoreDefaultAppearance(armor, FALSE);
    }

    // loop through all supported inventory slots
    int row = 0;
    int slot = IDS_GetSupportedSlotForComposedClothes(row);
    while (slot != -1)
    {
        object item = GetItemInSlot(slot, creature);

        // loop through all appearance types defined on item's TAG
        int pos = 0;
        string app = IDS_GetComposedAppearance(item, pos);
        if (app != "")
        {
            while (app != "")
            {
                // parse model part and appearance value
                string appType = IDS_GetComposedAppearanceType(app);
                int appValue = IDS_GetComposedAppearanceValue(app);
                int model = IDS_AppTypeToModel(appType);
                int bothSides = model < 0 && model > -255;
                model = abs(model);
                int side;
                for (side = 0; side < 2; side++)
                {
                    string name = IDS_ModelToString(ITEM_APPR_TYPE_ARMOR_MODEL, model);
                    int curValue = GetItemAppearance(armor, ITEM_APPR_TYPE_ARMOR_MODEL, model);

                    if (curValue != appValue)
                        armor = IDS_ModifyArmorAppearance(armor, ITEM_APPR_TYPE_ARMOR_MODEL, model, appValue);

                    if (!bothSides)
                        break;

                    model++;
                }

                app = IDS_GetComposedAppearance(item, ++pos);
            }
        }

        slot = IDS_GetSupportedSlotForComposedClothes(++row);
    }

    // copy last used colors from previous armor to the naked armor
    if (GetTag(armor) == IDS_TAG_ITEM_NAKED_BODY)
    {
        int c;
        for (c = 0; c < ITEM_APPR_ARMOR_NUM_COLORS; c++)
        {
            int color = gli(creature, SYSTEM_IDS_PREFIX + "_LAST_COLOR", c);
            if (GetItemAppearance(armor, ITEM_APPR_TYPE_ARMOR_COLOR, c) != color)
                armor = IDS_ModifyArmorAppearance(armor, ITEM_APPR_TYPE_ARMOR_COLOR, c, color);
        }
    }
    // at least once modified - equip copied armor
    if (GetItemInSlot(INVENTORY_SLOT_CHEST, creature) != armor)
    {
        DelayCommand(0.09, AssignCommand(creature, ClearAllActions()));
        DelayCommand(0.10, AssignCommand(creature, ActionEquipItem(armor, INVENTORY_SLOT_CHEST)));
    }

    // indicate finished composing
    DelayCommand(0.15, sli(creature, IDS_ + "COMPOSING", FALSE));
}

void IDS_ComposeClothes(object creature)
{
    if (!GetIsObjectValid(creature) ||
        GetCurrentHitPoints(creature) <= 0 ||
        !GetHasInventory(creature))
        return;

    int inCheck = gli(creature, IDS_ + "COMPOSING");
    if (inCheck)
        return;

    sli(creature, IDS_ + "COMPOSING", TRUE);
    AssignCommand(creature, ClearAllActions());
    AssignCommand(creature, ActionPlayAnimation(ANIMATION_LOOPING_GET_LOW, 0.75, 3.0f));
    TimingBarStart(creature, 3000);
    DelayCommand(3.0, IDS_ComposeClothesCheck(creature));
}

string IDS_GetPlacedResRef(int baseItem = -1, string resRefItem = "")
{
    // placeable resref by itemResRef
    string itemModel = resRefItem;
    if (itemModel != "")
        itemModel = gls(GetObjectByTag("SYSTEM_ACS_OBJECT"), "ACS_RESREF_" + lower(itemModel) + "_RESREFPLC");
    if (itemModel != "")
        return itemModel;

    // placeable resref by baseItemId
    if (baseItem > -1)
    {
        itemModel = gls(__IDS(), IDS_ + "DEFAULT_MODEL", baseItem);

        if (itemModel == "")
        {
            itemModel = Get2DAString("baseitems", "DefaultModel", baseItem);
            if (itemModel == "")
                itemModel = "it_bag";
            sls(__IDS(), IDS_ + "DEFAULT_MODEL", itemModel, baseItem);
        }
        return IDS + itemModel;
    }

    return IDS + "it_bag";
}

struct IDS_RawItem
{
    int ID;
    int TYPE;
    string TAG;
    string TECH;
    int WEIGHT;
    int DBCUR;
    int DBMAX;
    int STACK;
    string NAME;
    string DESC;
    location LOC;
};

object IDS_CreatePlacedItemRaw(struct IDS_RawItem i)
{
    // convert baseItemType to placeable resref
    string placedResRef = IDS_GetPlacedResRef(i.TYPE, i.TAG);
    if (placedResRef == "")
    {
        logentry("[" + IDS + "] unable to get placedResRef for baseItemType " + itos(i.TYPE));
        return OBJECT_INVALID;
    }

    // create placeable at items location
    object placedItem = CreateObject(OBJECT_TYPE_PLACEABLE, placedResRef, i.LOC, FALSE, IDS_TAG_PLACED_ITEM);
    if (!GetIsObjectValid(placedItem))
    {
        logentry("[" + IDS + "] unable to create placedItem for with resRef " + placedResRef);
        return OBJECT_INVALID;
    }

    // mark the item as PLACED_ITEM for generic recognition
    sli(placedItem, IDS_ + "ITEM", TRUE);
    sli(placedItem, IDS_ + "ITEM_WEIGHT", i.WEIGHT);
    sli(placedItem, IDS_ + "ITEM_DBMAX", i.DBMAX);
    sli(placedItem, IDS_ + "ITEM_DBCUR", i.DBCUR);
    sls(placedItem, IDS_ + "ITEM_TECH", i.TECH);
    sls(placedItem, IDS_ + "ITEM_TAG", i.TAG);
    sli(placedItem, IDS_ + "ITEM_STACK", i.STACK);
    sli(placedItem, IDS_ + "ITEM_TYPE", i.TYPE);
    sls(placedItem, IDS_ + "ITEM_NAME", i.NAME);
    sls(placedItem, IDS_ + "ITEM_DESC", i.DESC);

    // spawned placeable that represents placed items from palette has max possible HP (10k)
    // we must lower it to match item's current durability
    int plc_damage = 0;
    int itemHardness = 0;

    if (gli(GetFirstPC(), "itplace"))SpawnScriptDebugger();

    // stack-based items
    if (!IsWeightBasedItemType(i.TYPE))
    {
        // the stronger max durability, the more resistant the item is
        itemHardness = i.DBMAX / IDS_ITEM_DURABILITY_HD_RATIO;
        plc_damage = (i.DBMAX - i.DBCUR) / IDS_ITEM_DURABILITY_HP_RATIO;
    }

    AssignCommand(GetModule(), ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(plc_damage, DAMAGE_TYPE_MAGICAL, DAMAGE_POWER_ENERGY), placedItem));
    SetHardness(itemHardness, placedItem);

    // finally visible name and description
    SetName(placedItem, i.NAME);
    SetDescription(placedItem, i.DESC);

    // flag the item on area as changed so it will be persisted on the next save_data loop
    IDS_SetPlacedItemState(placedItem, IDS_PLACED_ITEM_STATE_CHANGED, IDS_GetPlacedItemList(GetTag(GetAreaFromLocation(i.LOC))));

    return placedItem;
}

struct IDS_RawItem IDS_GetRawItem(object item)
{
    struct IDS_RawItem ri;

    ri.TAG = GetTag(item);
    ri.TECH = GetDescription(item, FALSE, FALSE);
    ri.ID = GetTechPart(ri.TECH, TECH_PART_OBJECT_ID);
    ri.WEIGHT = IPS_GetItemWeight(item);
    ri.DBCUR = IDS_GetItemValue(item, FALSE);
    ri.DBMAX = IDS_GetItemValue(item, TRUE);
    ri.STACK = GetItemStackSize(item);
    ri.TYPE = GetBaseItemType(item);
    ri.NAME = GetName(item);
    ri.DESC = GetDescription(item);
    ri.LOC = GetLocation(item);

    return ri;
}

void IDS_ItemToInteractiveObjectPerform(object item)
{
    if (gli(item, "convert"))SpawnScriptDebugger();
    // item was destroyed or picked up by someone else
    if (!GetIsObjectValid(item) ||
        GetIsObjectValid(GetItemPossessor(item)))
        return;

    // copy important setting from the original item to placed item
    object placed_item = IDS_CreatePlacedItemRaw(IDS_GetRawItem(item));

    // finally get rid of the original item on ground
    Destroy(item);
}

void IDS_ItemToInteractiveObject(object item)
{
    if (!GetIsObjectValid(item) ||
        GetIsObjectValid(GetItemPossessor(item)))
        return;

    int inCheck = gli(item, IDS_ + "CONVERTING_ITEM");
    if (inCheck)
        return;

    sli(item, IDS_ + "CONVERTING_ITEM", TRUE);
    DelayCommand(0.5, IDS_ItemToInteractiveObjectPerform(item));
}



// +---------------------------------------------------------------------------+
// |                                IDS_DropItem                               |
// +---------------------------------------------------------------------------+

object IDS_DropItem(object oCreature, object oItem, float delay = 0.0f, int alsoMessage = TRUE)
{
    object oCopy = CopyObject(oItem, GetLocation(oCreature));
    if (!GetIsObjectValid(oCopy))
        return OBJECT_INVALID;

    IDS_ItemToInteractiveObject(oCopy);
    Destroy(oItem, delay);

    if (GetIsPC(oCreature))
    {
        sli(oCreature, IDS_ + "DROPPING", TRUE);

        if (alsoMessage)
            SYS_Info(oCreature, IDS, "ITEM_DROPPED_&" + GetName(oCopy) + "&");
    }

    return oCopy;
}



// +---------------------------------------------------------------------------+
// |                               IDS_GetFreeSpace                            |
// +---------------------------------------------------------------------------+

int IDS_GetFreeSpace(object oPC)
{
    int slots = gli(oPC, IDS_ + "INV_SLOTS");
    int capacity = gli(oPC, IDS_ + "INV_SLOTS_MAX");

    return capacity - slots;
}



// +---------------------------------------------------------------------------+
// |                        IDS_RefreshInventoryCapacity                       |
// +---------------------------------------------------------------------------+

int IDS_RefreshInventoryCapacity(object oPC)
{
    if (!GetIsPC(oPC)) return 0;

    int nBonus = IDS_GetInventoryCapacityBonus(oPC);
    int nSlots = IDS_GetInventorySlotsUsed(oPC);

    sli(oPC, IDS_ + "INV_SLOTS", nSlots);
    sli(oPC, IDS_ + "INV_SLOTS_MAX", nBonus);
    SYS_Info(oPC, IDS, "SLOTS_STATUS");

    return nBonus - nSlots;
}



// +---------------------------------------------------------------------------+
// |                        IDS_GetInventoryCapacityBonus                      |
// +---------------------------------------------------------------------------+

int IDS_GetInventoryCapacityBonus(object oPC)
{
    if (!GetIsPC(oPC)) return -1;

    int slot = 0;
    int nBonus = 0;

    for (slot = 0; slot < NUM_INVENTORY_SLOTS; slot++)
    {
        object oItem = GetItemInSlot(slot, oPC);
        nBonus += IDS_GetInventoryCapacityBonusOfItem(oItem);
    }

    return nBonus;
}



// +---------------------------------------------------------------------------+
// |                     IDS_GetInventoryCapacityBonusOfItem                   |
// +---------------------------------------------------------------------------+

int IDS_GetInventoryCapacityBonusOfItem(object oItem, string resRef = "")
{
    resRef = upper(GetIsObjectValid(oItem) ? GetResRef(oItem) : resRef);
    return gli(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_ITRF_" + resRef + "_INVBONUS");
}



// +---------------------------------------------------------------------------+
// |                                  IDS_Update                               |
// +---------------------------------------------------------------------------+



void    IDS_UpdateItem(object oPC, object oItem, int nValue, int nMax, int bMessage = TRUE)
{
    if (nMax == 0)
        return;

    int nStatus;

    if  (nMax   !=  -1)
    {
        IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_MAX, nMax), 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);
        sli(oItem, IDS_ + "VALMAX", nMax);
    }

    nValue  =   (nValue <   0)  ?   0   :   nValue;
    nMax    =   (nMax   ==  -1) ?   IDS_GetItemValue(oItem, TRUE)   :   nMax;
    nStatus =   ftoi(itof(nValue) / nMax * 100);

    IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_CURRENT, nValue), 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);
    IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_STATUS, nStatus), 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);

    sli(oItem, IDS_ + "VALCUR", nValue);
    sli(oItem, IDS_ + "STATUS", nStatus);

    if  (!nValue)
    {
        SetPlotFlag(oItem, FALSE);
        Destroy(oItem);
        //msg(oPC, "destroying: " + GetName(oItem), TRUE, FALSE);
    }

    else
    {
        SetTechPart(oItem, TECH_PART_DURABILITY_CURRENT, nValue);
        SetTechPart(oItem, TECH_PART_DURABILITY_MAX, nMax);
    }

    if  (bMessage)
    {
        //if  (1 + Random(100)    <=  IDS_NOTIFY_CHANCE
        if  (nStatus    <   25
        ||  !nValue)
        SYS_Info(oPC, IDS, "STATUS_&" + GetName(oItem) + "&_&" + itos(nStatus) + "&");
    }
}



// +---------------------------------------------------------------------------+
// |                         IDS_UpdateItemsInInventory                        |
// +---------------------------------------------------------------------------+



void    IDS_UpdateItemsInInventory(object oPC, int bAll = TRUE)
{
    if  (!GetIsPC(oPC)) return;
    if  (GetIsDM(oPC))  return;

    int     nType, nValue;
    object  oItem;

    //  equipnute predmety

    //  leva ruka
    oItem   =   GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oPC);

    if  (GetItemHasItemProperty(oItem, ITEM_PROPERTY_STATUS))
    {
        nValue  =   IDS_GetItemValue(oItem, FALSE);
        nValue  -=  (GetBaseItemType(oItem) ==  BASE_ITEM_TORCH) ? IDS_PENALTY_LOOP_TORCH : IDS_PENALTY_LOOP_LEFTHAND;

        IDS_UpdateItem(oPC, oItem, nValue, -1, TRUE);
    }

    //  prava ruka
    oItem   =   GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC);

    if  (GetItemHasItemProperty(oItem, ITEM_PROPERTY_STATUS))
    {
        nValue  =   IDS_GetItemValue(oItem, FALSE);

        IDS_UpdateItem(oPC, oItem, nValue - IDS_PENALTY_LOOP_RIGHTHAND, -1, TRUE);
    }

    //  hrudnik
    oItem   =   GetItemInSlot(INVENTORY_SLOT_CHEST, oPC);

    if  (GetItemHasItemProperty(oItem, ITEM_PROPERTY_STATUS))
    {
        nValue  =   IDS_GetItemValue(oItem, FALSE);

        IDS_UpdateItem(oPC, oItem, nValue - IDS_PENALTY_LOOP_TORSO, -1, TRUE);
    }

    //  obecny inventar
    if  (bAll)
    {
        oItem   =   GetFirstItemInInventory(oPC);

        while   (GetIsObjectValid(oItem))
        {
            if  (GetItemHasItemProperty(oItem, ITEM_PROPERTY_STATUS))
            {
                nValue  =   IDS_GetItemValue(oItem, FALSE);

                IDS_UpdateItem(oPC, oItem, nValue - IDS_PENALTY_LOOP_INVENTORY, -1, FALSE);
            }

            oItem   =   GetNextItemInInventory(oPC);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                              IDS_GetItemValue                             |
// +---------------------------------------------------------------------------+



int     IDS_GetItemValue(object oItem, int bMax = FALSE)
{
    return  gli(oItem, IDS_ + "VAL" + ((bMax) ? "MAX" : "CUR"));
    /*
    int nType   =   (bMax)  ?   ITEM_PROPERTY_MAX   :   ITEM_PROPERTY_CURRENT;

    if  (!GetItemHasItemProperty(oItem, nType)) return  -1;

    itemproperty    ipTemp  =   GetFirstItemProperty(oItem);

    while   (GetIsItemPropertyValid(ipTemp))
    {
        if  (GetItemPropertyType(ipTemp)    ==  nType)  return  GetItemPropertySubType(ipTemp);

        ipTemp  =   GetNextItemProperty(oItem);
    }

    return  -1;
    */
}



// +---------------------------------------------------------------------------+
// |                              IDS_GetItemStatus                            |
// +---------------------------------------------------------------------------+



int     IDS_GetItemStatus(object oItem)
{
    return  gli(oItem, IDS_ + "STATUS");
    /*
    if  (!GetItemHasItemProperty(oItem, ITEM_PROPERTY_STATUS))  return  -1;

    itemproperty    ipTemp  =   GetFirstItemProperty(oItem);

    while   (GetIsItemPropertyValid(ipTemp))
    {
        if  (GetItemPropertyType(ipTemp)    ==  ITEM_PROPERTY_STATUS)   return  GetItemPropertySubType(ipTemp);

        ipTemp  =   GetNextItemProperty(oItem);
    }

    return  -1;
    */
}



// +---------------------------------------------------------------------------+
// |                           IDS_GetInventorySlotsUsed                       |
// +---------------------------------------------------------------------------+

int IDS_GetInventorySlotsUsed(object oPC)
{
    if (!GetHasInventory(oPC)) return 0;

    object oItem = GetFirstItemInInventory(oPC);
    int nSize, nResult;

    while (GetIsObjectValid(oItem))
    {
        if (!IsSystemItem(oItem))
            nResult += GetBaseItemSize(GetBaseItemType(oItem));
        oItem = GetNextItemInInventory(oPC);
    }

    return nResult;
}

int IDS_GetItemsLimit(object player)
{
    //TODO: implement attribute/skill based inventory capacity limit
    return IDS_INVENTORY_ITEMS_LIMIT_BASE;
}

void IDS_CleanInventoryCheck(object player)
{
    if (!GetIsObjectValid(player) ||
        GetCurrentHitPoints(player) <= 0 ||
        !GetHasInventory(player))
        return;

    object item = GetFirstItemInInventory(player);

    if (!GetIsObjectValid(player))
        return;

    // count items
    int count = 0;
    while (GetIsObjectValid(item))
    {
        // ignore plot/system items
        if (!GetPlotFlag(item))
            count++;
        item = GetNextItemInInventory(player);
    }

    // inventory items count exceeds limit
    int limit = IDS_GetItemsLimit(player);
    count = count - limit;

    if (count > 0)
    {
        item = GetFirstItemInInventory(player);
        for (; count > 0; count--)
        {
            slo(player, IDS_ + "DROP_ITEM", item, -2);
            item = GetNextItemInInventory(player);
        }

        SYS_Info(player, IDS, "ITEMS_DROPPED");
    }

    // copy listed items
    int dropCount = gli(player, IDS_ + "DROP_ITEM_O_ROWS", -1, SYS_M_DELETE);
    for (count = dropCount; count > 0; count--)
    {
        item = glo(player, IDS_ + "DROP_ITEM", count, SYS_M_DELETE);
        IDS_DropItem(player, item);
    }

    // indicate finished checking
    sli(player, IDS_ + "CHECKING", FALSE);
}

void IDS_CleanInventory(object player)
{
    if (!GetIsObjectValid(player) ||
        GetCurrentHitPoints(player) <= 0 ||
        !GetHasInventory(player))
        return;

    int inCheck = gli(player, IDS_ + "CHECKING");

    if (inCheck)
        return;

    sli(player, IDS_ + "CHECKING", TRUE);
    DelayCommand(0.5, IDS_CleanInventoryCheck(player));
}
