// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_support.nss                                                |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-07-2009                                                     |
// | Updated || 17-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SUPPORT"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SUPPORT";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sTag    =   CCS_GetConvertedString(sLine, 1, FALSE);
    string  sDesc   =   CCS_GetConvertedString(sLine, 2, FALSE);
    string  sMessage;

    //  Neplatny TAG
    if  (sTag   ==  "")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� zna�ka";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  Neplatny objekt
    if  (GetStringLength(sDesc) <   5)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� popis - mus� b�t minim�ln� 5 znak�";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sMessage    =   V1 + "POZOR: " + W2 + "Hr�� [" + V2 + GetName(oPC) + W2 + "] v oblasti [" + V2 + GetName(GetArea(oPC)) + W2 + "] ��d� o podporu DM/PM:";
    sMessage    +=  "\n" + V1 + "Probl�m: " + W2 + sTag;
    sMessage    +=  "\n" + V1 + "Popis: " + W2 + sDesc;

    object  oTemp   =   GetFirstPC();
    int     nCount;

    while   (GetIsObjectValid(oTemp))
    {
        if  (GetIsDM(oTemp)
        ||  CCS_GetHasAccess(oTemp, "PM"))
        {
            nCount++;

            msg(oTemp, sMessage, FALSE, FALSE);
        }

        oTemp   =   GetNextPC();
    }

    if  (nCount >   0)
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Zpr�va o probl�mu byla doru�ena [" + G2 + itos(nCount) + W2 + "] DM/PM";

        msg(oPC, sMessage, FALSE, FALSE);
    }

    else
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Zpr�va o probl�mu nemohla b�t doru�ena proto�e ve h�e nejsou " + R2 + "��dn�" + W2 + " DM/PM";

        msg(oPC, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
