// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) OnPlayerResurrected script                    |
// | File    || vg_s1_rs_raise.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 15-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Po oziveni bude hrac slaby a unaveny
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   OBJECT_SELF;

    if  (!GetIsPC(oPC)) return;

    /*string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

    if  (sArea  ==  "S"
    ||  sArea   ==  "X")    return;
    */
    //int nEdge   =   RS_FATIGUE_EDGE_FSLEEP - 15; //gli(GetObjectByTag("SYSTEM_" + SYSTEM_RS_PREFIX + "_OBJECT"), SYSTEM_RS_PREFIX + "_TIRED_EDGE", RS_TIRED_LEVEL_WEAK_PC);

    //sli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE", nEdge);
    SYS_Info(oPC, SYSTEM_RS_PREFIX, "RESURRECTED");
}
