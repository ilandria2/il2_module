// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) OnPlayerDeath script                    |
// | File    || vg_s1_sys_death.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    "Resetne mod chuze" zemrelemu hracske postave
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetLastPlayerDied();

    if  (!GetIsPC(oPC)) return;

    //sli(oPC, SYSTEM_SYS_PREFIX + "_MODE", -1, -1, SYS_M_DELETE);
}
