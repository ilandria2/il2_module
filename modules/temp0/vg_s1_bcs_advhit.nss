// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Perform advanced hit                  |
// | File    || vg_s1_bcs_advhit.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Performs the "Advanced hit" combat ability
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"
#include    "vg_s0_pis_core_c"
#include    "vg_s0_rs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+





void main()
{
    object oPC = OBJECT_SELF;
    object oTarget = GetAttackTarget(oPC);
    int bSecondary = gli(oPC, SYSTEM_SYS_PREFIX + "_USE_SECONDARY", -1, SYS_M_DELETE);

    //SpawnScriptDebugger();
    //  cooldown
    if  (gli(oPC, SYSTEM_BCS_PREFIX + "_COOLDOWN"))
    {
        SYS_Info(oPC, SYSTEM_BCS_PREFIX, "COOLDOWN");
        return;
    }

    //  pc s nikym nebojuje
    if  (!GetIsObjectValid(oTarget))
    {
        SYS_Info(oPC, SYSTEM_BCS_PREFIX, "NO_ATTACK");
        return;
    }

    int nStamina    =   gli(oPC, SYSTEM_BCS_PREFIX + "_STAMINA");

    //  nedostatek vydrze
    if  ((!bSecondary && nStamina < BCS_STAMINA_COST_ADVHIT_PRIMARY)
    ||  (bSecondary && nStamina < BCS_STAMINA_COST_ADVHIT_SECONDARY))
    {
        SYS_Info(oPC, SYSTEM_BCS_PREFIX, "STAMINA_X");
        return;
    }

    int nPower = gli(oPC, SYSTEM_BCS_PREFIX + "_POWER");

    if  ((!bSecondary && nPower < BCS_POWER_COST_ADVHIT_PRIMARY)
    ||  (bSecondary && nPower < BCS_POWER_COST_ADVHIT_SECONDARY))
    {
        SYS_Info(oPC, SYSTEM_BCS_PREFIX, "POWER_X");
        return;
    }

    int nFatigue = gli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE");

    //  nedostatek energie
    if  ((!bSecondary && RS_FATIGUE_EDGE_FSLEEP - nFatigue < BCS_FATIGUE_COST_ADVHIT_PRIMARY)
    ||  (bSecondary && RS_FATIGUE_EDGE_FSLEEP - nFatigue < BCS_FATIGUE_COST_ADVHIT_SECONDARY))
    {
        SYS_Info(oPC, SYSTEM_RS_PREFIX, "ENERGY_X");
        return;
    }

    int nRight  =   GetBaseItemType(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC));
    int nSize   =   stoi(Get2DAString("baseitems", "WeaponSize", nRight));

    /*
    //  nema equipnutou stredni zbran
    if  (nSpellID   ==  SPELL_BCS_ADVANCED_HIT_STRONG
    &&  nSize       !=  2)
    {
        SYS_Info(oPC, SYSTEM_BCS_PREFIX, "WEAPON_SIZE_2_X");
        return;
    }

    //  nema equipnutou velkou zbran
    if  (nSpellID   ==  SPELL_BCS_ADVANCED_HIT_MIGHTY
    &&  nSize       <   3)
    {
        SYS_Info(oPC, SYSTEM_BCS_PREFIX, "WEAPON_SIZE_3P_X");
        return;
    }
    */

    int nType   =   stoi(Get2DAString("baseitems", "WeaponType", nRight));
    int nDType  =   0;

    if  (nType  ==  4)  //  pierce-slash
    nDType  =   (Random(2)) ?   DAMAGE_TYPE_SLASHING    :   DAMAGE_TYPE_PIERCING;

    else

    if  (nType  ==  5)  //  bludg-pierce
    nDType  =   (Random(2)) ?   DAMAGE_TYPE_SLASHING    :   DAMAGE_TYPE_BLUDGEONING;

    else
    nDType  =   (nType  ==  1)  ?   DAMAGE_TYPE_PIERCING    :
                (nType  ==  2)  ?   DAMAGE_TYPE_BLUDGEONING :
                (nType  ==  3)  ?   DAMAGE_TYPE_SLASHING    :   -1;

    int nDamage;

    if  (!bSecondary)
    {
        nDamage     =   15;
        nStamina    -=  10;
        nPower      -=  20;
        nFatigue    +=  5;

        PlayVoiceChat(VOICE_CHAT_GATTACK1 + Random(3), oPC);
    }

    else
    {
        nDamage     =   40;
        nStamina    -=  30;
        nPower      -=  40;
        nFatigue    +=  10;

        PlayVoiceChat(VOICE_CHAT_BATTLECRY1 + Random(3), oPC);
    }

    nDamage += nSize * 2;

    BCS_StartCooldownEffect(oPC);
    DelayCommand(0.5f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(nDamage, nDType, DAMAGE_POWER_NORMAL), oTarget));
    sli(oPC, SYSTEM_BCS_PREFIX + "_STAMINA", nStamina);
    sli(oPC, SYSTEM_BCS_PREFIX + "_POWER", nPower);
    sli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE", nFatigue);
    SYS_Info(oPC, SYSTEM_BCS_PREFIX, "USE_ABILITY_&" + itos(FEAT_BCS_ADVANCED_HIT_STRONG) + "&&" + itos(bSecondary) + "&");
    DelayCommand(0.5f, BCS_RegDrainStamina(oPC, 1));
    DelayCommand(0.6f, PIS_RefreshIndicatorPortrait(oPC));
}
