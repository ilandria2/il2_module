// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Learn school script                    |
// | File    || vg_s1_scs_learsc.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 14-06-2009                                                     |
// | Updated || 14-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript pro uceni magicke skoly
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nSpell      =   GetSpellId();
    int nClass      =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT"), SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_" + Get2DAString("spells", "School", nSpell));
    int nInnate     =   stoi(Get2DAString("spells", "Innate", nSpell));
    int nGainedXP   =   1 + (nInnate * XPS_M_XP_LEARN_SCHOOL_MULTIPLIER) / 2 + Random(nInnate * XPS_M_XP_LEARN_SCHOOL_MULTIPLIER);
    int nMode       =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONJURE_MODE");

    if  (nMode  ==  SCS_CONJURE_MODE_LEARN)
    nGainedXP   *=  2;

    XPS_AlterAbsorbXP(OBJECT_SELF, nClass, nGainedXP);
    XPS_AlterAbsorbXP(OBJECT_SELF, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nGainedXP / 2);
}
