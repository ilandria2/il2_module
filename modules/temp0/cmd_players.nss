// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_players.nss                                                |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-12-2009                                                     |
// | Updated || 05-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "PLAYERS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



int GetNumPC(object oArea)
{
    object  oPC =   GetFirstPC();
    int     nResult;

    while   (GetIsObjectValid(oPC))
    {
        if  (GetArea(oPC)   ==  oArea
        ||  GetModule()     ==  oArea)
        nResult++;

        oPC =   GetNextPC();
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "PLAYERS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nPlayerID   =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    string  sMessage;

    //  neplatne ID
    if  (nPlayerID  <   -1)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid ID";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int nRow, bDM, bPM, nPCID;
    string  sAcc, sName, sDM_PC, sPlayers;
    object  oPlayer;

    //  seznam hracu
    if  (nPlayerID  ==  -1)
    {
        oPlayer =   GetFirstPC();

        while   (GetIsObjectValid(oPlayer))
        {
            sAcc    =   GetPCPlayerName(oPlayer);
            sName   =   GetName(oPlayer);
            bDM     =   GetIsDM(oPlayer);
            bPM     =   CCS_GetHasAccess(oPlayer, "PM");
            nPCID   =   gli(oPlayer, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
            sDM_PC  =   (bDM    ||  bPM)    ?   R2  :   G2;

            sPlayers    +=  "\n" + W2 + "#" + itos(nRow) + " - " + sDM_PC + sAcc + W2 + " (" + sDM_PC + sName + W2 + ") - Persistent ID " + sDM_PC + itos(nPCID);

            nRow++;
            oPlayer =   GetNextPC();
        }

        sMessage    =   Y1 + "( ? ) " + W2 + "Player list:";
        sMessage    +=  "\n" + W2 + "~~~~~~~~~~~~~~~~~";
        sMessage    +=  sPlayers;

        msg(oPC, sMessage, FALSE, FALSE);
    }

    //  informace o konkretnim hraci
    else
    {
        object  oPlayer =   GetFirstPC();
        int     nRow;

        while   (GetIsObjectValid(oPlayer))
        {
            if  (nRow   ==  nPlayerID)  break;

            nRow++;

            oPlayer =   GetNextPC();
        }

        string  sIP, sArea, sLeader, sClasses;
        int     nLevel, nClass1, nClass2, nClass3, nLevel1, nLevel2, nLevel3;

        sIP     =   GetPCIPAddress(oPlayer);
        sAcc    =   GetPCPlayerName(oPlayer);
        sName   =   GetName(oPlayer);
        bDM     =   GetIsDM(oPlayer);
        bPM     =   CCS_GetHasAccess(oPlayer, "PM");
        sArea   =   GetName(GetArea(oPlayer));
        sLeader =   GetPCPlayerName(GetFactionLeader(oPlayer));
        nPCID   =   gli(oPlayer, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
        nLevel  =   GetHitDice(oPlayer);
        nClass1 =   GetClassByPosition(1, oPlayer);
        nClass2 =   GetClassByPosition(2, oPlayer);
        nClass3 =   GetClassByPosition(3, oPlayer);
        nLevel1 =   GetLevelByClass(nClass1, oPlayer);
        nLevel2 =   GetLevelByClass(nClass2, oPlayer);
        nLevel3 =   GetLevelByClass(nClass3, oPlayer);

        string  sDM         =   (bDM)   ?   R2 + "YES"  :   G2 + "NO";
        string  sPM         =   (bPM)   ?   R2 + "YES"  :   G2 + "NO";

        sClasses    =   GetStringByStrRef(stoi(Get2DAString("classes", "Name", nClass1))) + "(" + itos(nLevel1) + ")";

        if  (nClass2    !=  CLASS_TYPE_INVALID)
        sClasses    +=  ", " + GetStringByStrRef(stoi(Get2DAString("classes", "Name", nClass2))) + "(" + itos(nLevel2) + ")";

        if  (nClass3    !=  CLASS_TYPE_INVALID)
        sClasses    +=  ", " + GetStringByStrRef(stoi(Get2DAString("classes", "Name", nClass3))) + "(" + itos(nLevel3) + ")";

        sMessage    =   Y1 + "( ? ) " + W2 + "Player info:";
        sMessage    +=  "\n" + W2 + "~~~~~~~~~~~~~~~~~";
        sMessage    +=  "\n" + W2 + "ID: " + G2 + itos(nRow);
        sMessage    +=  "\n" + W2 + "IP Address: " + G2 + sIP;
        sMessage    +=  "\n" + W2 + "Account: " + G2 + sAcc;
        sMessage    +=  "\n" + W2 + "Name: " + G2 + sName;
        sMessage    +=  "\n" + W2 + "PM: " + sPM;
        sMessage    +=  "\n" + W2 + "DM: " + sDM;
        sMessage    +=  "\n" + W2 + "Area: " + G2 + sArea;
        sMessage    +=  "\n" + W2 + "In a group of: " + G2 + sLeader;
        sMessage    +=  "\n" + W2 + "Persistent ID: " + G2 + itos(nPCID);
        sMessage    +=  "\n" + W2 + "Level: " + G2 + itos(nLevel) + W2 + " [" + C2 + sClasses + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
