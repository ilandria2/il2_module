// +---------++----------------------------------------------------------------+
// | Name    || ExperienceSystem (XPS) OnModuleHeartbeat script                |
// | File    || vg_s1_xps_hrbeat.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 30-04-2009                                                     |
// | Updated || 07-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosti OnModuleHeartbeat systemu XPS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    //ExecuteScript("vg_s1_xps_rpx", OBJECT_SELF);    // odmeny za RP a casove XP
    //ExecuteScript("vg_s1_xps_absorb", OBJECT_SELF); // vstrebavaci mod
}
