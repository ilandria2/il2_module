// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_alignment.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 26-07-2009                                                     |
// | Updated || 26-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "ALIGNMENT"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "ALIGNMENT";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nAlignment  =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     nShift      =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    object  oCreature   =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sMessage;

    if  (!GetIsObjectValid(oCreature))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (GetObjectType(oCreature)   !=  OBJECT_TYPE_CREATURE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type - needs to be a creature";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    string  sTag    =   (GetIsPC(oCreature))    ?   GetPCPlayerName(oCreature)  :   GetTag(oCreature);

    int     nLawChaosOld    =   GetLawChaosValue(oCreature);
    int     nGoodEvilOld    =   GetGoodEvilValue(oCreature);
    string  sLawChaosOld    =   (nLawChaosOld   <=  30) ?   R2 + "Chaotic"    :
                                (nLawChaosOld   <   70) ?   V2 + "Neutral"    :
                                                            B2 + "Lawful";
    string  sGoodEvilOld    =   (nGoodEvilOld   <=  30) ?   R2 + "Evil" :
                                (nGoodEvilOld   <   70) ?   V2 + "Neutral" :
                                                            G2 + "Good";

    AdjustAlignment(oCreature, nAlignment, nShift);

    int     nLawChaosNew    =   GetLawChaosValue(oCreature);
    int     nGoodEvilNew    =   GetGoodEvilValue(oCreature);
    string  sLawChaosNew    =   (nLawChaosNew   <=  30) ?   R1 + "Chaotic" :
                                (nLawChaosNew   <   70) ?   V1 + "Neutral" :
                                                            B1 + "Lawful";
    string  sGoodEvilNew    =   (nGoodEvilNew   <=  30) ?   R1 + "Evil" :
                                (nGoodEvilNew   <   70) ?   V1 + "Neutral" :
                                                            B1 + "Good";

    sMessage    =   Y1 + "( ? ) " + W2 + "Alignment of the creature [" + G2 + GetName(oCreature) + " (" + sTag + ")" + W2 + "] was changed:";
    sMessage    +=  "\nfrom [" + Y2 + sLawChaosOld + " (" + itos(nLawChaosOld) + ") " + sGoodEvilOld + " (" + itos(nGoodEvilOld) + ")" + W2 + "]";
    sMessage    +=  "\nto [" + G2 + sLawChaosNew + " (" + itos(nLawChaosNew) + ") " + sGoodEvilNew + " (" + itos(nGoodEvilNew) + ")" + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
