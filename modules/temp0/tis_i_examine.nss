// Text Interaction System (TIS) Interaction script - EXAMINE

//this = TIS_ConstructInteraction();
#include "vg_s0_tis_inter"

const int ACTION_TYPE  = 112;

//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    return TRUE;
}

//////// ACTION'S BODY
void Do()
{
    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        string oldDesc = GetDescription(this.Object);
        string oldName = GetName(this.Object);
        string sDesc = gls(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_AS_" + itos(this.ActionSubType) + "_DESC");
        string sName = gls(__TIS(), TIS_ + "OS_" + itos(this.ObjectSubType) + "_DISPLAY");
        sDesc = ConvertTokens(sDesc, this.Player, TRUE, TRUE, FALSE, TRUE);

        SetName(this.Object, sName);
        SetDescription(this.Object, sDesc);
        DelayCommand(0.1f, AssignCommand(this.Player, ActionExamine(this.Object)));
        DelayCommand(0.3f, SetName(this.Object, oldName));
        DelayCommand(0.3f, SetDescription(this.Object, oldDesc));
        this.DelaySequence = -999.0; // return to menu
    }

    // delayed sequence
    else
    {
        // ......

        // last sequence
        if (TRUE)
            this.DelaySequence = 0.0;
    }
}

/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

