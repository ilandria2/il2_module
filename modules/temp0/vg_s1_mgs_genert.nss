// +---------++----------------------------------------------------------------+
// | Name    || Music Generator System (MGS) Music generator script            |
// | File    || vg_s1_mgs_genert.nss                                           |
// +---------++----------------------------------------------------------------+
/*
    Vygeneruje hudbu v oblasti, pokud se v ni nikdo nenachazi.

    Nastaveni:
    MGS_BATTLE - string - <odkaz na skupinu, kt. se ma generovat>
    MGS_BACKGROUND - string - <odkaz na skupinu, kt. se ma generovat>

    priklad:
    MGS_BATTLE="battle"         // budou se generovat hudby ze skupiny 'battle'
    MGS_BACKROUND="temple"      // budou se generovat hudby ze skupiny 'temple'

    Hudby se definuji v souboru mus_categories.2da
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_mgs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    //SpawnScriptDebugger();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    object  oMGS    =   GetObjectByTag("SYSTEM_" + SYSTEM_MGS_PREFIX + "_OBJECT");
    int     nLast   =   gli(oMGS, SYSTEM_MGS_PREFIX + "_HBS");
    int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");

    //  kod ma nastat pouze pri prekroceni limitu
    if  (nLast &&
    nHBs - nLast   <   MGS_HEARTBEATS_MUSIC)   return;

    int     nArea, bPC;
    object  oArea, oPC;
    string  sBattle, sBack;

    oArea   =   glo(oSystem, SYSTEM_SYS_PREFIX + "_AREA", nArea = 1);
    oArea   =   (!GetIsObjectValid(oArea))  ?   GetObjectByTag("VG_AE_FO_HUMCAMP")  :   oArea;

    while   (GetIsObjectValid(oArea))
    {
        bPC     =   FALSE;
        oPC     =   GetFirstPC();

        while   (GetIsObjectValid(oPC))
        {
            if  (GetArea(oPC)   ==  oArea)
            {
                bPC =   TRUE;
                break;
            }

            oPC =   GetNextPC();
        }

        if  (bPC || !nLast)
        {
            sBattle =   gls(oArea, SYSTEM_MGS_PREFIX + "_BATTLE");
            sBack   =   gls(oArea, SYSTEM_MGS_PREFIX + "_BACKGROUND");

            if  (sBattle    !=  "")
            {
                int nTracks =   gli(oMGS, SYSTEM_MGS_PREFIX + "_" + GetStringUpperCase(sBattle));

                if  (!nTracks)
                {
                    logentry("[" + SYSTEM_MGS_PREFIX + "] Area [" + GetTag(oArea) + "] has invalid category set up [" + sBattle + "] for BATTLE music");
                }

                else
                {
                    MusicBattleChange(oArea, stoi(Get2DAString("mus_categories", GetStringUpperCase(sBattle + "_id"), Random(nTracks))));
                }
            }

            if  (sBack !=  "")
            {
                int nTracks =   gli(oMGS, SYSTEM_MGS_PREFIX + "_" + GetStringUpperCase(sBack));

                if  (!nTracks)
                {
                    logentry("[" + SYSTEM_MGS_PREFIX + "] Area [" + GetTag(oArea) + "] has invalid category set up [" + sBack + "] for BACKGROUND music");
                }

                else
                {
                    MusicBackgroundChangeDay(oArea, stoi(Get2DAString("mus_categories", GetStringUpperCase(sBack + "_id"), Random(nTracks))));
                    MusicBackgroundChangeNight(oArea, stoi(Get2DAString("mus_categories", GetStringUpperCase(sBack + "_id"), Random(nTracks))));
                }
            }
        }

        oArea   =   glo(oSystem, SYSTEM_SYS_PREFIX + "_AREA", ++nArea);
    }

    sli(oMGS, SYSTEM_MGS_PREFIX + "_HBS", nHBs);
}

