// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Sys info                               |
// | File    || vg_s0_scs_info.nss                                             |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-05-2009                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu SCS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_core_m"
#include    "vg_s0_scs_const"
#include    "vg_s0_xps_const"
#include    "vg_s0_sys_colors"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    sTemp   =   gls(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "CIRCLE_SWITCH")
    {
        int     nSchool =   stoi(GetNthSubString(sTemp, 1));

        if  (!nSchool)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Nem� ��dn� znalosti vyvol�vac�ch kruh�";
            sMessage    +=  "\n" + Y1 + "( ? ) " + W2 + "Mus� naj�t knihu kouzel pro �koly kouzel";
        }

        else
        {
            string  sSchool =   gls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", nSchool);
            int     bKnown  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", SCS_SPELL_SUMMONING_CIRCLE + nSchool);
            string  sColor;

            if  (bKnown)
            sColor  =   V2; else
            sColor  =   R1;

            sMessage    =   W2 + "Vyvol�vac� kruh [" + sColor + sSchool + W2 + "]";
        }
    }

    else

    if  (sCase  ==  "SPELL_SWITCH")
    {   //SpawnScriptDebugger();
        int nSpell      =   stoi(GetNthSubString(sTemp, 1));    // spell
        int nSub        =   stoi(GetNthSubString(sTemp, 2));    // sub
        int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
        int nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));
        string  sMaster =   Get2DAString("spells", "Master", nSpellOrig);

        if  (sMaster    !=  "")
        {
            nSpell      =   stoi(sMaster);
            nSpellGroup =   0;
        }

        int nMetaMagic  =   gli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", nSpellGroup);
        int nSchool     =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_" + Get2DAString("spells", "School", nSpell));
        int nMana       =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA");
        int nCost       =   SCS_CalculateUsedMana(OBJECT_SELF, nSpell);
        int nInnate     =   SCS_CalculateSpellLevel(nSpell, OBJECT_SELF);
        int nCurrent    =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nSchool) + "_LEVEL");
        int nMode       =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONJURE_MODE");
        int nChance     =   SCS_CalculateSpellFailure(nCurrent, nInnate * 5);

        nChance /=  (nMode  ==  SCS_CONJURE_MODE_FAKE)  ?   3   :
                    (nMode  ==  SCS_CONJURE_MODE_LEARN) ?   2   :   1;

        string  sChance     =   itos(100 - nChance);

        /* [NEXMQST] ***** odstaveno
        int     nLast       =   gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER_S_ROWS");
        int     n           =   1;

        while   (n  <=  nLast)
        {
            string  sLetter =   gls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_LETTER", n);
            int     bMeta   =   SCS_GetSpellHasMetamagicType(n, nSpell);
            int     bFeat   =   GetHasFeat(gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGIC_FEAT", n), OBJECT_SELF);

            bFeat   =   bFeat || (n == 1);

            sMetaColor  =   (!bMeta || !bFeat)      ?   W0  :
                            (n  ==  stoi(sParam2))  ?   C1  :   W2;

            sMetaMagic  +=  sMetaColor + sLetter;
            n++;
        }*/

        int     bMeta       =   SCS_GetSpellHasMetamagicType(nMetaMagic, nSpellOrig, TRUE);
        int     bFeat       =   GetHasFeat(gli(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX_FEAT", 1 + nMetaMagic), OBJECT_SELF);
        string  sMetaColor  =   (!nMetaMagic)       ?   V2  :
                                (!bMeta || !bFeat)  ?   R1  :   V2;
        string  sCostColor  =   (nCost  >   nMana)  ?   R1  :   C1;
        string  sSpell      =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", SCS_FIRST_CONJURE_SPELL + nSpellOrig + 800)));
        string  sMode, sSub, sMetaMagic;

        if  (nSub   !=  0)
        {
            sSpell  =   "-> " + GetStringByStrRef(stoi(Get2DAString("spells", "Name", stoi(Get2DAString("spells", "SubRadSpell" + itos(nSub), nSpellOrig)))));
        }

        else
        {
            sSpell  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", nSpellOrig)));
        }

        if  (nMode  >   SCS_CONJURE_MODE_NORMAL)
        {
            sMode   =   W1 + gls(oSystem, SYSTEM_SCS_PREFIX + "_CONJURE_MODE", 1 + nMode) + ": ";
        }

        if  (nMetaMagic !=  METAMAGIC_NONE)
        {
            sMetaMagic  =   gls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", 1 + nMetaMagic);
            sMetaMagic  =   " [" + sMetaColor + sMetaMagic + W2 + "]";
        }

        int nSources    =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpellOrig) + "_DOMAIN_I_ROWS");
        int bKnown      =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", nSpellOrig);
        int nDSRow      =   SCS_GetDomainSpellRowUsed(nSpellOrig, OBJECT_SELF);
        string  sSpellColor;

        if  (bKnown)
        {
            if  (!nSources  ||  (nSources   >   0  &&  nDSRow   >=  0))
            sSpellColor =   W2;

            else
            sSpellColor =   R1;
        }

        else
        sSpellColor =   R1;

        sMessage    =   sMode + sSpellColor + sSpell + sSub + W2 + sMetaMagic + " [" + sCostColor + itos(nCost) + W2 + " Mana " + C1 + sChance + W2 + "%]";
    }

    else

    if  (sCase  ==  "CONJURE_MODE_SWITCH")
    {
        string  sParam1 =   GetNthSubString(sTemp);
        string  sMode   =   gls(oSystem, SYSTEM_SCS_PREFIX + "_CONJURE_MODE", stoi(sParam1) + 1);

        sMessage    =   W2 + "M�d kouzlen� [" + V2 + sMode + W2 + "]";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_POLYMORPH")
    {
        sMessage    =   R1 + "( ! ): " + W2 + "V prom�n� nelze ses�lat kouzla";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_SPELL")
    {
        string  sParam1 =   GetNthSubString(sTemp);
        string  sSpell  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", stoi(sParam1))));

        sMessage    =   R1 + "( ! ) " + W2 + "Nezn�me kouzlo [" + C1 + sSpell + W2 + "]";
        sMessage    +=  "\n" + Y1 + "( ? ) " + W2 + "Mus� naj�t knihu kouzel pro toto kouzlo";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_DOMAIN_SPELL")
    {
        int     nSpell  =   stoi(GetNthSubString(sTemp));
        string  sSpell  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", nSpell)));

        sMessage    =   R1 + "( ! ) " + W2 + "Nedostatek �rovn� sesilatele pro kouzlo [" + C1 + sSpell + W2 + "]:";

        int     nSources    =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_DOMAIN_I_ROWS");
        int     nRow, nDomain, nLevel;
        string  sDomain, s2da;

        while   (++nRow <=  nSources)
        {
            nDomain =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_DOMAIN", nRow);
            nLevel  =   gli(oSystem, SYSTEM_SCS_PREFIX + "_DOMAIN_SPELL_" + itos(nSpell) + "_LEVEL", nRow);
            sDomain =   GetStringByStrRef(stoi(Get2DAString("Domains", "Name", nDomain)));

            sMessage    +=  "\n" + W2 + "Klerik s dom�nou [" + C1 + sDomain + W2 + "] �rove� (" + C2 + itos(SCS_CalculateCasterLevel(nLevel)) + W2 + ")";
        }

        s2da        =   Get2DAString("spells", "Cleric", nSpell);
        s2da        =   (s2da   ==  "") ?   R1 + "#N/A" :   C1 + s2da;
        sMessage    +=  "\n" + W2 + "Klerik (" + s2da + W2 + ")";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_MANA")
    {
        string  sMana   =   GetNthSubString(sTemp);
        string  sMax    =   GetNthSubString(sTemp, 2);
        string  sNeeded =   GetNthSubString(sTemp, 3);

        sMessage    =   R1 + "( ! ) " + W2 + "Nedostatek many [" + C1 + sMana + W2 + " / " + C1 + sMax + W2 + " vs " + R1 + sNeeded + W2 + "]";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_ABILITY")
    {
        string  sAbility    =   GetNthSubString(sTemp);
        string  sScore      =   GetNthSubString(sTemp, 2);
        string  sNeeded     =   GetNthSubString(sTemp, 3);
        string  sName       =   gls(oSystem, SYSTEM_SCS_PREFIX + "_ABILITY", stoi(sAbility) + 1);

        sMessage    =   R1 + "( ! ) " + W2 + "Nedostatek vlastnosti [" + C1 + sName + W2 + " = " + C1 + sScore + W2 + " vs " + R1 + sNeeded + W2 + "]";
    }

    else

    if  (sCase  ==  "SPELL_PERMITTED_AREASWITCH")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "V t�hle oblasti nelze ses�lat kouzla";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_CIRCLE")
    {
        string  sSchool =   gls(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", stoi(GetNthSubString(sTemp)));

        sMessage    =   R1 + "( ! ) " + W2 + "Mus� b�t ve vyvol�vac�m kruhu [" + C1 + sSchool + W2 + "]";
    }

    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_METAMAGIC")
    {
        string  sMetaMagic  =   gls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", 1 + stoi(GetNthSubString(sTemp)));

        sMessage    =   R1 + "( ! ) " + W2 + "Nelze seslat kouzlo v metamagii [" + C1 + sMetaMagic + W2 + "]";
    }


    else

    if  (sCase  ==  "SPELL_CHECK_FAILED_METAMAGIC_FEAT")
    {
        string  sMetaMagic  =   gls(oSystem, SYSTEM_SCS_PREFIX + "_METAMAGICX", 1 + stoi(GetNthSubString(sTemp)));

        sMessage    =   R1 + "( ! ) " + W2 + "Neovl�d� odbornost metamagii [" + C1 + sMetaMagic + W2 + "]";
    }

    else

    if  (sCase  ==  "UNKNOWN_DOMAIN_SPELL")
    {
        int     nDomainSpell    =   stoi(GetNthSubString(sTemp));
        string  sSpell          =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", nDomainSpell)));

        sMessage    =   R1 + "( ! ) " + W2 + "Nezn�m� dom�nn� kouzlo [" + C1 + sSpell + W2 + "]";
        sMessage    +=  "\n" + Y1 + "( ? ) " + W2 + "Mus� m�t pot�ebnou dom�nu kouzel na sesl�n� tohoto kouzla";
    }

    else

    if  (sCase  ==  "CHANGE_MANA")
    {
        int     nManaOld    =   stoi(GetNthSubString(sTemp));
        int     nManaNew    =   stoi(GetNthSubString(sTemp, 2));
        int     nManaMax    =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA_MAX");
        string  sChar       =   (nManaNew   >=  nManaOld)   ?   "+" :   "-";

        sMessage    =   B1 + "[" + GetDiagram(nManaNew, nManaMax, FALSE, 0, 0, 255) + B1 + "] " + sChar + itos(abs(nManaNew - nManaOld)) + B1 + " Mana [" + B2 + itos(nManaNew) + B1 + " / " + itos(nManaMax) + " (" + B2 + itos(ftoi(itof(nManaNew) / itof(nManaMax) * 100)) + B1 + "%)]";
    }

    else

    if  (sCase  ==  "CHANGE_MANA_BONUS")
    {
        int     nSubType    =   stoi(GetNthSubString(sTemp));
        int     nCurrent    =   stoi(GetNthSubString(sTemp, 2));
        int     nNew        =   stoi(GetNthSubString(sTemp, 3));
        string  sName       =   gls(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_ADDPROP", nSubType + 1);
        string  sChar       =   (nNew   >=  nCurrent)   ?   "+" :   "-";

        sMessage    =   W2 + sChar + C1 + itos(abs(nNew - nCurrent)) + V2 + " " + sName + W2 + " [" + C1 + itos(nNew) + W2 + "]";
    }

    else

    if  (sCase  ==  "CHANGE_MANA_MAX")
    {
        int     nManaMaxOld =   stoi(GetNthSubString(sTemp));
        int     nManaMaxNew =   stoi(GetNthSubString(sTemp, 2));
        string  sChar       =   (nManaMaxNew    >=  nManaMaxOld)    ?   "+" :   "-";

        sMessage    =   W2 + sChar + C1 + itos(abs(nManaMaxNew - nManaMaxOld)) + V2 + " MaxMana";
    }

    else

    if  (sCase  ==  "SPELL_FAILURE")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Selh�n� kouzla";
    }

    else

    if  (sCase  ==  "SPELL_FAILURE_M")
    {
        sMessage    =   R1 + "( ! ) " + V1 + "Mystick� " + W2 +"selh�n� kouzla";
    }

    else

    if  (sCase  ==  "LEARN_SPELL_FAILED")
    {
        int     nSpell      =   stoi(GetNthSubString(sTemp));
        string  sSpell      =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", nSpell)));
        string  sBard       =   Get2DAString("spells", "Bard", nSpell);
        string  sWizard     =   Get2DAString("spells", "Wiz_Sorc", nSpell);
        string  sCleric     =   Get2DAString("spells", "Cleric", nSpell);
        string  sDruid      =   Get2DAString("spells", "Druid", nSpell);
        string  sRanger     =   Get2DAString("spells", "Ranger", nSpell);
        string  sPal        =   Get2DAString("spells", "Paladin", nSpell);
        string  sSorc       =   (stoi(sWizard)  <=  6)  ?   sWizard :   "";
        string  sCBard      =   (sBard      !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_BARD)))       :   "";
        string  sCWizard    =   (sWizard    !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_WIZARD)))     :   "";
        string  sCCleric    =   (sCleric    !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_CLERIC)))     :   "";
        string  sCDruid     =   (sDruid     !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_DRUID)))      :   "";
        string  sCRanger    =   (sRanger    !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_RANGER)))     :   "";
        string  sCPal       =   (sPal       !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_PALADIN)))    :   "";
        string  sCSorc      =   (sSorc      !=  "") ?   GetStringByStrRef(stoi(Get2DAString("classes", "Name", CLASS_TYPE_SORCERER)))   :   "";
        string  sClasses;

        if  (sBard      !=  "") sBard   =   itos(SCS_CalculateCasterLevel(stoi(sBard)));
        if  (sWizard    !=  "") sWizard =   itos(SCS_CalculateCasterLevel(stoi(sWizard)));
        if  (sCleric    !=  "") sCleric =   itos(SCS_CalculateCasterLevel(stoi(sCleric)));
        if  (sDruid     !=  "") sDruid  =   itos(SCS_CalculateCasterLevel(stoi(sDruid)));
        if  (sRanger    !=  "") sRanger =   itos(SCS_CalculateCasterLevel(stoi(sRanger)));
        if  (sPal       !=  "") sPal    =   itos(SCS_CalculateCasterLevel(stoi(sPal)));
        if  (sSorc      !=  "") sSorc   =   itos(SCS_CalculateCasterLevel(stoi(sSorc)));

        sClasses    +=  (sBard      !=  "") ?   ("\n" + W2 + sCBard   + " [" + C1 + sBard   + W2 + "]") :   "";
        sClasses    +=  (sWizard    !=  "") ?   ("\n" + W2 + sCWizard + " [" + C1 + sWizard + W2 + "]") :   "";
        sClasses    +=  (sCleric    !=  "") ?   ("\n" + W2 + sCCleric + " [" + C1 + sCleric + W2 + "]") :   "";
        sClasses    +=  (sDruid     !=  "") ?   ("\n" + W2 + sCDruid  + " [" + C1 + sDruid  + W2 + "]") :   "";
        sClasses    +=  (sRanger    !=  "") ?   ("\n" + W2 + sCRanger + " [" + C1 + sRanger + W2 + "]") :   "";
        sClasses    +=  (sPal       !=  "") ?   ("\n" + W2 + sCPal    + " [" + C1 + sPal    + W2 + "]") :   "";
        sClasses    +=  (sSorc      !=  "") ?   ("\n" + W2 + sCSorc   + " [" + C1 + sSorc   + W2 + "]") :   "";

        sMessage    =   R1 + "( ! ) " + W2 + "Nedostatek �rovn� sesilatele [" + C1 + sSpell + W2 + "]:" + sClasses;
    }

    else

    if  (sCase  ==  "LEARN_SPELL_INTERRUPTED")
    {
        string  sSpell  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", stoi(GetNthSubString(sTemp)))));

        sMessage    =   R1 + "( ! ) " + W2 + "Ji� zn� kouzlo [" + C1 + sSpell + W2 + "]";
    }

    else

    if  (sCase  ==  "LEARN_SPELL_SUCCESS")
    {
        string  sSpell  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", stoi(GetNthSubString(sTemp)))));

        sMessage    =   G1 + "( ! ) " + W2 + "Z�sk�v� znalosti kouzla [" + C1 + sSpell + W2 + "]";
    }

    else

    if  (sCase  ==  "RESIST_SPELL")
    {
        string  sName   =   GetNthSubString(sTemp);
        int     nResult =   stoi(GetNthSubString(sTemp, 2));
        int     nDice   =   stoi(GetNthSubString(sTemp, 3));
        int     nLevel  =   stoi(GetNthSubString(sTemp, 4));
        int     nValue  =   stoi(GetNthSubString(sTemp, 5));
        int     nSR     =   stoi(GetNthSubString(sTemp, 6));
        string  sState, sColor;

        switch  (nResult)
        {
            case    2:  sMessage    =   V2 + sName + W2 + " je im�nn� v��i tomuhle kouzlu.";  break;
            case    3:  sMessage    =   V2 + sName + W2 + " pohltil ��inky tohoto kouzla.";   break;
            default:
                if  (nDice  ==  1)  {sState =   "KRITICK� CHYBA"; sColor = R1;}
                else
                {
                    if  ((nDice + nLevel + nValue)  <   nSR)
                    {
                        sState  =   "NE�SP�CH";
                        sColor  =   R1;
                    }

                    else
                    {
                        sState  =   "�SP�CH";
                        sColor  =   G1;
                    }
                }

                sMessage    =   V2 + sName + ": " + W2 + "Prolomen� MO: " + sColor + sState + " " +
                                W2 + "(" +
                                itos(nDice) + " + " +
                                itos(nLevel) + " + " +
                                itos(nValue) + " = " +
                                sColor + itos(nDice + nLevel + nValue) +
                                W2 + " vs " +
                                C1 + itos(nSR) +
                                W2 + ")";
            break;
        }
    }

    else

    if  (sCase  ==  "SYSTEM_SKILLS")
    {
        object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
        object  oTarget     =   glo(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SYSTEM_SKILLS_OBJECT", -1, SYS_M_DELETE);
        int     nMana       =   gli(oTarget, SYSTEM_SCS_PREFIX + "_MANA");
        int     nManaMax    =   gli(oTarget, SYSTEM_SCS_PREFIX + "_MANA_MAX");
        int     nMaxVal     =   gli(oTarget, SYSTEM_SCS_PREFIX + "_MAX_MANA_BONUS_VALUE");
        int     nMaxPer     =   gli(oTarget, SYSTEM_SCS_PREFIX + "_MAX_MANA_BONUS_PERCENT");
        int     nRegenVal   =   gli(oTarget, SYSTEM_SCS_PREFIX + "_REGEN_MANA_BONUS_VALUE");
        int     nRegenPer   =   gli(oTarget, SYSTEM_SCS_PREFIX + "_REGEN_MANA_BONUS_PERCENT");
        int     nRestState  =   gli(oTarget, "RS_REST_STATE");
        int     nRegenBase  =   SCS_CalculateRegeneratedMana(oTarget, FALSE);
        int     nRegen      =   SCS_CalculateRegeneratedMana(oTarget, TRUE, nRegenBase);
        int     nMaxBase    =   nManaMax - ftoi(nManaMax * (itof(nMaxPer) / 100)) - nMaxVal;

        int nAdjustment;

        switch  (nRestState)
        {
            case    1:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_MEDITATE;    break;
            case    2:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_SIT;         break;
            case    3:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_NAP;         break;
            case    4:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_SLEEP;       break;
            case    5:  nAdjustment =   SCS_MANA_REGEN_REST_BONUS_FSLEEP;      break;
        }

        sMessage    =   Y1 + "( ? ) " + W2 + "Vlastnosti many:";
        sMessage    +=  "\n" + W2 + "Mana: " + B2 + "[" + GetDiagram(nMana, nManaMax, FALSE, 0, 0, 255) + B1 + "] " + B1 + " Mana [" + B2 + itos(nMana) + B1 + " / " + itos(nManaMax) + " (" + B2 + itos(ftoi(itof(nMana) / itof(nManaMax) * 100)) + B1 + "%)]";
        sMessage    +=  "\n" + W2 + "MaxMana: " + B1 + "Z�klad: " + B2 + itos(nMaxBase) + B1 + ", + " + B2 + itos(nMaxVal) + B1 + ", % " + B2 + itos(nMaxPer) + B1 + " = " + B2 + itos(nManaMax);
        sMessage    +=  "\n" + W2 + "Obnova many: " + B1 + "Z�klad: " + B2 + itos(nRegenBase) + B1 + ", + " + B2 + itos(nRegenVal) + B1 + ", % " + B2 + itos(nRegenPer) + B1 + ", Rest " + B2 + itos(nAdjustment) + B1 + " = " + B2 + itos(nRegen);

        string  lc, l1, l2, l3, l4, l5, l6, l7, l8, l9, sSpell;
        int     loop    =   1;
        int     nSpell  =   gli(oTarget, SYSTEM_SCS_PREFIX + "_SPELL_ROW", loop);
        int     bKnown  =   gli(oTarget, SYSTEM_SCS_PREFIX + "_SPELL", nSpell);
        int     nInnate;

        while   (bKnown)
        {
            sSpell  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", nSpell)));
            nInnate =   SCS_CalculateSpellLevel(nSpell, OBJECT_SELF);

            switch  (nInnate)
            {
                case    0:  lc  +=  B2 + sSpell + W2 + ", ";    break;
                case    1:  l1  +=  B2 + sSpell + W2 + ", ";    break;
                case    2:  l2  +=  B2 + sSpell + W2 + ", ";    break;
                case    3:  l3  +=  B2 + sSpell + W2 + ", ";    break;
                case    4:  l4  +=  B2 + sSpell + W2 + ", ";    break;
                case    5:  l5  +=  B2 + sSpell + W2 + ", ";    break;
                case    6:  l6  +=  B2 + sSpell + W2 + ", ";    break;
                case    7:  l7  +=  B2 + sSpell + W2 + ", ";    break;
                case    8:  l8  +=  B2 + sSpell + W2 + ", ";    break;
                case    9:  l9  +=  B2 + sSpell + W2 + ", ";    break;
            }

            nSpell  =   gli(oTarget, SYSTEM_SCS_PREFIX + "_SPELL_ROW", ++loop);
            bKnown  =   gli(oTarget, SYSTEM_SCS_PREFIX + "_SPELL", nSpell);
        }

        sMessage    +=  "\n" + W2 + "Znalosti kouzel [" + B2 + itos(loop - 1) + W2 + "]";
        sMessage    +=  (lc ==  "") ?   ""  :   "\n    " + B1 + "Triky: " + lc;
        sMessage    +=  (l1 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 1: " + l1;
        sMessage    +=  (l2 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 2: " + l2;
        sMessage    +=  (l3 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 3: " + l3;
        sMessage    +=  (l4 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 4: " + l4;
        sMessage    +=  (l5 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 5: " + l5;
        sMessage    +=  (l6 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 6: " + l6;
        sMessage    +=  (l7 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 7: " + l7;
        sMessage    +=  (l8 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 8: " + l8;
        sMessage    +=  (l9 ==  "") ?   ""  :   "\n    " + B1 + "Okruh 9: " + l9;
    }

    msg(OBJECT_SELF, sMessage, FALSE, FALSE);
}
