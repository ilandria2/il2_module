// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Persistent data load script            |
// | File    || vg_s1_xps_data_l.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-06-2009                                                     |
// | Updated || 13-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacteni perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_xps_core_m"
#include    "vg_s0_xps_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nID =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sQuerry;
    string  sData, sRow;
    int     nClass, nXP, nAbsorb, nPrev, nFind;

    sQuerry =   "SELECT " +
                    "XPS " +
                "FROM " +
                    "xps_xp " +
                "WHERE " +
                    "PlayerID = '" + itos(nID) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sData   =   SQLGetData(1);
        nClass  =   0;
        nPrev   =   nFind;
        nFind   =   FindSubString(sData, "~", nPrev);

        while   (nFind  !=  -1)
        {
            nXP     =   stoi(GetSubString(sData, nPrev, nFind));
            nPrev   =   nFind;
            nFind   =   FindSubString(sData, "~", nPrev + 1);
            nAbsorb =   stoi(GetSubString(sData, nPrev + 1, nFind));
            nPrev   =   nFind + 1;
            nFind   =   FindSubString(sData, "~", nPrev);

            XPS_AlterXP(OBJECT_SELF, nClass, nXP, TRUE, FALSE);
            XPS_AlterAbsorbXP(OBJECT_SELF, nClass, nAbsorb, TRUE);

            nClass++;
        }
    }
}
