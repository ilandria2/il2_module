// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) System init                           |
// | File    || vg_s5_bcs_init.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 18-07-2009                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system BCS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_bcs_core_c"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_BCS_PREFIX;
    string  sName       =   SYSTEM_BCS_NAME;
    string  sVersion    =   SYSTEM_BCS_VERSION;
    string  sAuthor     =   SYSTEM_BCS_AUTHOR;
    string  sDate       =   SYSTEM_BCS_DATE;
    string  sUpDate     =   SYSTEM_BCS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_bcs_info");
//    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_<short>_data_c");
//    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_<short>_data_s");
//    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_<short>_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_PHYSICAL_ATTACKED) + "_SCRIPT", "vg_s1_bcs_determ", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_D_ON_PHYSICAL_ATTACKED) + "_SCRIPT", "vg_s1_bcs_determ", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_P_ON_PHYSICAL_ATTACKED) + "_SCRIPT", "vg_s1_bcs_determ", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACQUIRE_ITEM) + "_SCRIPT", "vg_s1_bcs_upgrad", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_UNEQUIP_ITEM) + "_SCRIPT", "vg_s1_bcs_unarmd", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_bcs_refbab", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_EQUIP_ITEM) + "_SCRIPT", "vg_s1_bcs_setbab", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_bcs_stmcap", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_SPAWN) + "_SCRIPT", "vg_s1_bcs_walksp", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_bcs_stmreg", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_bcs_regpow", -2);

//  ----------------------------------------------------------------------------
//  Inicializacni kod

//..

//  Inicializacni kod
//  ----------------------------------------------------------------------------

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
