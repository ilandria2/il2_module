// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Load groups script               |
// | File    || vg_s1_tis_groups.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Spawnovaci systemu dynamickych objektu (soucast systemu TIS)
    Pri nacteni modulu se pusti tenhle skript, kt. nacte vsechny spawn WP a
    podle jejich promennych typu "GROUP" se oblasti, ve kt. jsou tyhle WP,
    zeskupi a nastavi pro spravne fungovani respawn systemu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    logentry("[" + SYSTEM_TIS_PREFIX + "] Setting up spawn point master objects");

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");
    int     n, bGroup, nTotal, bArea;
    object  oMaster, oSpawn, oArea;
    string  sGroup, sResRef;

    n       =   0;
    oSpawn  =   GetObjectByTag(TIS_TAG_TIS_SPAWN_POINT, n);

    while   (GetIsObjectValid(oSpawn))
    {
        sGroup  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP");
        bGroup  =   gli(oSystem, SYSTEM_TIS_PREFIX + "_GROUP_" + sGroup);
        oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + upper(sGroup));

        if  (!bGroup)
        {
            nTotal  =   0;
            sResRef =   gls(oMaster, SYSTEM_TIS_PREFIX + "_RESREF", ++nTotal);

            while   (sResRef    !=  "")
            sResRef =   gls(oMaster, SYSTEM_TIS_PREFIX + "_RESREF", ++nTotal);
            nTotal  -=  1;

            sli(oMaster, SYSTEM_TIS_PREFIX + "_RESREF_S_ROWS", nTotal);
            sls(oSystem, SYSTEM_TIS_PREFIX + "_GROUP", sGroup, -2);
            sli(oSystem, SYSTEM_TIS_PREFIX + "_GROUP_" + sGroup, TRUE);
        }

        if  (GetIsObjectValid(oMaster))
        {
            oArea   =   GetArea(oSpawn);
            bArea   =   gli(oMaster, SYSTEM_TIS_PREFIX + "_AREA_" + GetTag(oArea));

            if  (!bArea)
            {
                sli(oMaster, SYSTEM_TIS_PREFIX + "_AREA_" + GetTag(oArea), TRUE);
                slo(oMaster, SYSTEM_TIS_PREFIX + "_AREA", oArea, -2);
            }

            bGroup  =   gli(oArea, SYSTEM_TIS_PREFIX + "_GROUP_" + sGroup);

            if  (!bGroup)
            {
                sls(oArea, SYSTEM_TIS_PREFIX + "_GROUP", sGroup, -2);
                sli(oArea, SYSTEM_TIS_PREFIX + "_GROUP_" + sGroup, TRUE);
            }

            slo(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN", oSpawn, -2);
        }

        else
        logentry("[" + SYSTEM_TIS_PREFIX + "] Error: No master WP for spawn group [" + sGroup + "] area [" + GetTag(GetArea(oSpawn)) + "] pos [" + itos(ftoi(GetPosition(oSpawn).x)) + ", " + itos(ftoi(GetPosition(oSpawn).y)) + ", " + itos(ftoi(GetPosition(oSpawn).z)) + "]");

        oSpawn  =   GetObjectByTag(TIS_TAG_TIS_SPAWN_POINT, ++n);
    }

    logentry("[" + SYSTEM_TIS_PREFIX + "] Spawn point master objects set up. Total [" + itos(n) + "] spawn points found.");
}

