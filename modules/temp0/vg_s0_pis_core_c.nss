// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Core C                           |
// | File    || vg_s0_pis_core_c.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Hlavni knihovna funkci systemu PIS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_pis_core_t"
#include    "nwnx_names"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || PIS_ProgressAction                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Sets the progress indicator to nPercent % and refreshes the portrait based
//  indicators.
//
// -----------------------------------------------------------------------------
void PIS_ProgressAction(object oPC, int nPercent);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_RefreshIndicatorIcons                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Obnovi indikatory typu "effect icons" u hrace oPC
//
// -----------------------------------------------------------------------------
void    PIS_RefreshIndicatorIcons(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_RefreshIndicator                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Obnovi indikator pro hrace oPC
//  ** oPC + oPlayer    = T>S, S>T
//  ** oPC + oPC + bAll = *>S
//  ** oPC + bAll       = *>S, S>*
//  ** oPC              = S>S, DM>S
//
//  !! funkce vyuziva GetFirstPC() a GetNextPC(), proto je nutne pouzit spozdeni
//  pri pouziti tehle funkce ve smycce
//
// -----------------------------------------------------------------------------
void    PIS_RefreshIndicator(object oPC, object oPlayer = OBJECT_INVALID, int bAll = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || PIS_RefreshIndicatorPortrait                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Obnovi portret obsahujici indikatory
//
// -----------------------------------------------------------------------------
void    PIS_RefreshIndicatorPortrait(object oPC);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                             PIS_ProgressAction                            |
// +---------------------------------------------------------------------------+

void PIS_ProgressAction(object oPC, int nPercent)
{
    if (!GetIsPC(oPC)) return;

    sli(oPC, SYSTEM_PIS_PREFIX + "_PROGRESS_ACTION", nPercent < 0 ? 0 : nPercent > 100 ? 100 : nPercent);
    PIS_RefreshIndicatorPortrait(oPC);
}



// +---------------------------------------------------------------------------+
// |                         PIS_RefreshIndicatorIcons                         |
// +---------------------------------------------------------------------------+



void    ApplyEffect(object oPC)
{
    //DelayCommand(0.01f, ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(PIS_GetHealthIndicator(oPC)), oPC));
    DelayCommand(0.01f, ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(PIS_GetHungerIndicator(oPC)), oPC));
    DelayCommand(0.01f, ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(PIS_GetFatigueIndicator(oPC)), oPC));
    DelayCommand(0.01f, ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(PIS_GetThirstIndicator(oPC)), oPC));
    //DelayCommand(0.01f, ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(PIS_GetWeightIndicator(oPC)), oPC));
}


void    PIS_RefreshIndicatorIcons(object oPC)
{
    if  (!GetIsPC(oPC)) return;

    effect  eEffect =   GetFirstEffect(oPC);
    int     nType;

    while   (GetIsEffectValid(eEffect))
    {
        if  (GetEffectSubType(eEffect)  ==  SUBTYPE_SUPERNATURAL)
        {
            nType   =   GetEffectType(eEffect);

            if  (nType  ==  EFFECT_TYPE_ABILITY_DECREASE
            ||  nType   ==  EFFECT_TYPE_ABILITY_INCREASE
            ||  nType   ==  EFFECT_TYPE_SAVING_THROW_INCREASE
            ||  nType   ==  EFFECT_TYPE_SAVING_THROW_DECREASE
            ||  nType   ==  EFFECT_TYPE_SPELL_FAILURE
            ||  nType   ==  EFFECT_TYPE_SPELLLEVELABSORPTION
            ||  nType   ==  EFFECT_TYPE_SKILL_DECREASE
            ||  nType   ==  EFFECT_TYPE_SKILL_INCREASE
            ||  nType   ==  EFFECT_TYPE_SPELL_RESISTANCE_DECREASE
            ||  nType   ==  EFFECT_TYPE_SPELL_RESISTANCE_INCREASE)
            DelayCommand(0.1f, RemoveEffect(oPC, eEffect));
        }

        //msg(oPC, "effect: " + itos(nType) + " sub: " + itos(GetEffectSubType(eEffect)));
        eEffect =   GetNextEffect(oPC);
    }

    DelayCommand(0.15f, ApplyEffect(oPC));
}



// +---------------------------------------------------------------------------+
// |                           PIS_RefreshIndicator                            |
// +---------------------------------------------------------------------------+



void    PIS_RefreshIndicator(object oPC, object oPlayer = OBJECT_INVALID, int bAll = TRUE)
{
    if  (!GetIsPC(oPC)) return;

    if  ((oPC   ==  oPlayer
    ||  oPlayer ==  OBJECT_INVALID)
    &&  !bAll)
    {
        SetDynamicName(oPC, oPC, PIS_GetMergedIndicator(oPC, oPC));
        PIS_RefreshIndicatorPortrait(oPC);
        //PIS_RefreshIndicatorIcons(oPC);
    }

    else

    //  ** oPC + oPlayer    = S>T, T<S
    if  (GetIsPC(oPlayer)
    &&  oPC != oPlayer
    &&  !bAll)
    {
        SetDynamicName(oPC, oPlayer, PIS_GetMergedIndicator(oPC, oPlayer));
        SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));
    }

    else

    if  (!bAll) return;

    //  jine pripady maji loop vsech hracu
    else
    {
        int bPC     =   oPC == oPlayer;
        oPlayer     =   GetFirstPC();

        while   (GetIsObjectValid(oPlayer))
        {
            if  (bAll)
            {
                //  ** oPC + oPC + bAll = *>S
                if  (bPC)
                SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));

                //  ** oPC + bAll       = *>S, S>*
                else
                {
                    //  kdyz nalezeny hrac ze smycky je oPC pak staci nastavit indikator pouze 1x
                    if  (oPC    !=  oPlayer)
                    SetDynamicName(oPC, oPlayer, PIS_GetMergedIndicator(oPC, oPlayer));
                    SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));

                    if  (oPC    ==  oPlayer)
                    PIS_RefreshIndicatorPortrait(oPC);
                }
            }

            oPlayer =   GetNextPC();
        }
    }
}

//stary pouzivajici text indikatory pro health, hunger, thirst, inventory, fatigue
/*void    PIS_RefreshIndicator(object oPC, object oPlayer = OBJECT_INVALID, int bAll = TRUE)
{
    if  (!GetIsPC(oPC)) return;

    //  ** oPC + oPlayer    = S>T, T<S
    if  (GetIsPC(oPlayer)
    &&  oPC != oPlayer
    &&  !bAll)
    {
        SetDynamicName(oPC, oPlayer, PIS_GetMergedIndicator(oPC, oPlayer));
        SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));
    }

    //  jine pripady maji loop vsech hracu
    else
    {
        int bPC     =   oPC == oPlayer;
        oPlayer     =   GetFirstPC();

        while   (GetIsObjectValid(oPlayer))
        {
            if  (bAll)
            {
                //  ** oPC + oPC + bAll = *>S
                if  (bPC)
                SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));

                //  ** oPC + bAll       = *>S, S>*
                else
                {
                    //  kdyz nalezeny hrac ze smycky je oPC pak staci nastavit indikator pouze 1x
                    if  (oPC    !=  oPlayer)
                    SetDynamicName(oPC, oPlayer, PIS_GetMergedIndicator(oPC, oPlayer));
                    SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));
                }
            }

            //  ** oPC = S>S, DM>S
            else
            {
                if  (GetIsDM(oPlayer)
                ||  oPC ==  oPlayer)
                SetDynamicName(oPlayer, oPC, PIS_GetMergedIndicator(oPlayer, oPC));
            }

            oPlayer =   GetNextPC();
        }
    }
}
*/



// +---------------------------------------------------------------------------+
// |                         PIS_RefreshIndicatorPortrait                      |
// +---------------------------------------------------------------------------+
int     ValueToLevel3(int nValue)
{
    int nLevel;

    nLevel  =   (nValue >=  81) ?   2   :
                (nValue >=  31) ?   1   :   0;

    return  nLevel;
}

int     ValueToLevel5(int nValue)
{
    int nLevel;

    nLevel  =   (nValue <   25) ?   0   :
                (nValue <   50) ?   1   :
                (nValue <   75) ?   2   :
                (nValue <   100) ?  3   :   4;

    return  nLevel;
}

int     ValueToLevel10(int nValue)
{
    int nLevel;

    nLevel  =   (nValue <   10) ?   0   :
                (nValue <   20) ?   1   :
                (nValue <   30) ?   2   :
                (nValue <   40) ?   3   :
                (nValue <   50) ?   4   :
                (nValue <   60) ?   5   :
                (nValue <   70) ?   6   :
                (nValue <   80) ?   7   :
                (nValue <   90) ?   8   :   9;

    return  nLevel;
}


void    PIS_RefreshIndicatorPortrait(object oPC)
{
    int nHunger, nLiquid, nEnergy, nStamina, nPower, nProgress;
    string portrait;

    nHunger =   100 - abs(ftoi(itof(gli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER")) / FDS_DEADLY_EDGE_HUNGER * 100));
    nLiquid =   100 - abs(ftoi(itof(gli(oPC, SYSTEM_FDS_PREFIX + "_THIRST")) / FDS_DEADLY_EDGE_THIRST * 100));
    nEnergy =   100 - abs(ftoi(itof(gli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE")) / RS_FATIGUE_EDGE_FSLEEP * 100));
    nStamina=   ftoi(itof(gli(oPC, SYSTEM_BCS_PREFIX + "_STAMINA")) / gli(oPC, SYSTEM_BCS_PREFIX + "_STAMINA_MAX") * 100);
    nPower =  ftoi(itof(gli(oPC, SYSTEM_BCS_PREFIX + "_POWER")) / gli(oPC, SYSTEM_BCS_PREFIX + "_POWER_MAX") * 100);
    nProgress=  gli(oPC, SYSTEM_PIS_PREFIX + "_PROGRESS_ACTION");
    nHunger =   ValueToLevel3(nHunger);
    nLiquid =   ValueToLevel3(nLiquid);
    nEnergy =   ValueToLevel3(nEnergy);
    nStamina=   ValueToLevel10(nStamina);
    nPower=  ValueToLevel5(nPower);
    nProgress=  ValueToLevel5(nProgress);
    portrait =  "il2ind_x_" + itos(nStamina) + itos(nPower) + itos(nHunger) + itos(nLiquid) + itos(nEnergy) + itos(nProgress);

    SetPortraitResRef(oPC, portrait);
}

