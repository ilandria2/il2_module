#include "aps_include"
#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_sys_core_o"
#include "vg_s0_ets_const"
#include "vg_s0_ids_core_c"

void main()
{
    object source = OBJECT_SELF;
    string temp = gls(source, SYSTEM_SYS_PREFIX + "_DB_SAVE_PARAM", -1, SYS_M_DELETE);
    string actionName = GetSubString(temp, 0, FindSubString(temp, "&") - 1);

    if (actionName != "ADD" && actionName != "DELETE" && actionName != "SELL" && actionName != "MONEY" &&
        actionName != "TAKE" && actionName != "DELETE_ITEMS" && actionName != "STORE_ITEMS" && actionName != "TRANSACTION")
    {
        logentry("[" + ETS + "] Invalid action name: " + actionName + " when saving data.");
        return;
    }

    // Persist new store object
    if (actionName == "ADD")
    {
        if (GetObjectType(source) != OBJECT_TYPE_STORE)
        {
            logentry("[" + ETS + "] ERROR: Invalid object type when persisting/removing store object: " + GetTag(source));
            return;
        }

        int id = gli(source, ETS_ + "STORE_ID");

        if (id >= 100)
        {
            logentry("[" + ETS + "] ERROR: Unable to persist existing store with id: " + itos(id));
            return;
        }

        string sql = "SELECT MAX(store_id) FROM ets_store";
        SQLExecDirect(sql);

        if (SQLFetch())
            id = stoi(SQLGetData(1)) + 1;
        if (id < 100) id = 100;

        string resref = GetResRef(source);
        string name = GetName(source);
        string owner = gls(source, ETS_ + "STORE_OWNER");
        string loc = APSLocationToString(GetLocation(source));
        sql = "INSERT INTO ets_store (store_id, store_resref, store_name, store_loc, store_owner) VALUES (" +
            itos(id) + ",'" + resref + "','" + name + "','" + loc + "','" + owner + "')";

        SQLExecDirect(sql);
        sli(source, ETS_ + "STORE_ID", id);
        sli(__ETS(), ETS_ + "STORE", id, -2);
        slo(__ETS(), ETS_ + "STORE", source, id);
        logentry("[" + ETS + "] Persisted new store object with id " + itos(id));
    }

    // Remove existing store object
    else if (actionName == "DELETE")
    {
        if (GetObjectType(source) != OBJECT_TYPE_STORE)
        {
            logentry("[" + ETS + "] ERROR: Invalid object type when persisting/removing store object: " + GetTag(source));
            return;
        }

        int id = gli(source, ETS_ + "STORE_ID");

        if (id < 100)
        {
            logentry("[" + ETS + "] ERROR: Invalid store_id when attempting to remove it from database: " + itos(id));
            return;
        }

        string sql = "DELETE FROM ets_store WHERE store_id = " + itos(id);

        SQLExecDirect(sql);
        sli(source, ETS_ + "STORE_ID", -1, -1, SYS_M_DELETE);
        logentry("[" + ETS + "] Removed persisted store with id " + itos(id));
    }

    else if (actionName == "SELL")
    {
        object store = GetItemPossessor(source);
        object player = glo(store, ETS_ + "TARGET");

        if (!GetIsObjectValid(store) || GetObjectType(store) != OBJECT_TYPE_STORE)
        {
            logentry("[" + ETS + "] Invalid store for item " + GetTag(source));
            return;
        }

        int storeId = gli(store, ETS_ + "STORE_ID");
        string sql = "SELECT MAX(store_item_id) FROM ets_store_items";
        SQLExecDirect(sql);

        int itemId;
        if (SQLFetch())
            itemId = stoi(SQLGetData(1)) + 1;
        if (itemId < 100) itemId = 100;

        int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
        string item_tag = GetTag(source);
        string item_tech = GetDescription(source, FALSE, FALSE);
        int item_oid = GetTechPart(item_tech, TECH_PART_OBJECT_ID);
        int item_stack = GetItemStackSize(source);
        int item_weight = GetWeight(source);
        int item_dbcur = IDS_GetItemValue(source, FALSE);
        int item_dbmax = IDS_GetItemValue(source, TRUE);
        string item_name = SQLEncodeSpecialChars(GetName(source));
        string item_desc = SQLEncodeSpecialChars(GetDescription(source));

        sql = "INSERT INTO ets_store_items (store_item_id, store_id, player_id, item_oid, item_tag, item_tech, item_stack, item_weight, item_dbcur" +
            ",item_dbmax, item_name, item_desc) VALUES " +
            "(" + itos(itemId) + "," + itos(storeId) + "," + itos(playerId) + "," + itos(item_oid) + ",'" + item_tag + "','" + item_tech + "'," + itos(item_stack) +
            "," + itos(item_weight) + "," + itos(item_dbcur) + "," + itos(item_dbmax) + ",'" + item_name + "','" + item_desc + "')";

        SQLExecDirect(sql);
        sli(source, ETS_ + "STORE_ITEM", itemId);
    }

    else if (actionName == "TAKE")
    {
        int itemId = gli(source, ETS_ + "STORE_ITEM");

        if (itemId < 100)
        {
            logentry("[" + ETS + "] ERROR:  Attempted to remove item with invalid item ID.");
            return;
        }

        string sql = "DELETE FROM ets_store_items WHERE store_item_id = " + itos(itemId);

        SQLExecDirect(sql);
        sli(source, ETS_ + "STORE_ITEM", -1, 0, SYS_M_DELETE);
        logentry("[" + ETS + "] Removed item ID " + itos(itemId) + " from database.");
    }

    else if (actionName == "DELETE_ITEMS")
    {
        string itemIds = gls(source, ETS_ + "ITEM_IDS", -1, SYS_M_DELETE);
        string sql = "DELETE FROM ets_store_items WHERE store_item_id IN (" + itemIds + ")";

        SQLExecDirect(sql);
        logentry("[" + ETS + "] Removed item IDs " + itemIds + " from database.");
    }

    else if (actionName == "STORE_ITEMS")
    {
        string itemIds = gls(source, ETS_ + "ITEM_IDS", -1, SYS_M_DELETE);
        string sql = "UPDATE ets_store_items SET player_id = NULL WHERE store_item_id IN (" + itemIds + ")";

        SQLExecDirect(sql);
        logentry("[" + ETS + "] Removed player_id value from item IDs " + itemIds + " in database.");
    }

    else if (actionName == "MONEY")
    {
        //TODO: adjust status of money of the store (source)
    }

    else if (actionName == "TRANSACTION")
    {
        if (GetObjectType(source) != OBJECT_TYPE_STORE)
        {
            logentry("[" + ETS + "] ERROR: Invalid object type when creating transaction.");
            return;
        }

        int store_id = gli(source, ETS_ + "STORE_ID");

        if (store_id < 100)
        {
            logentry("[" + ETS + "] ERROR: Uninitialized merchant object: " + GetTag(source));
            return;
        }

        SQLExecDirect("SELECT MAX(transaction_id) FROM ets_store_transaction");

        int id = 0;
        if (SQLFetch())
            id = stoi(SQLGetData(1)) + 1;
        if (id < 100)
            id = 100;

        int items_sold = stoi(GetNthSubString(temp, 1));
        int items_bought = stoi(GetNthSubString(temp, 2));
        int balance_cleared = stoi(GetNthSubString(temp, 3));
        int balance_req = stoi(GetNthSubString(temp, 4));
        object player = glo(source, ETS_ + "TARGET");
        int player_id = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        SQLExecDirect("INSERT INTO ets_store_transaction VALUES (" +
            itos(id) + "," + itos(store_id) + "," + itos(player_id) + "," + itos(items_bought) + "," + itos(items_sold) + "," +
            itos(balance_cleared) + "," + itos(balance_req) + ",default,default)");

        logentry("[" + ETS + "] Created new transaction ID id " + itos(id));
    }
}
