// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Module event script                    |
// | File    || vg_s1_scs_manamx.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 30-04-2008                                                     |
// | Updated || 15-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacte perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_core_c"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC         =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");
    int     nManaMaxOld =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX");
    int     nManaMaxNew =   SCS_CalculateMaxMana(oPC);

    if  (nManaMaxNew    >   0)
    {
        sli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX", nManaMaxNew);
        SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_MAX_&" + itos(nManaMaxOld) + "&_&" + itos(nManaMaxNew) + "&");
    }
}
