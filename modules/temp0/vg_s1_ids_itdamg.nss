// OnDamaged event of placed item - if the placeable's HP is lower than it was supposed to have,
// get its ID, remove from DB and destroy it

#include "vg_s0_ids_core_c"

void main()
{
    IDS_HandleDamagedEventForPlacedItem(OBJECT_SELF);
}
