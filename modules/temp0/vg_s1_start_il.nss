// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Player start script                     |
// | File    || vg_s1_start_il.nss                                             |
// +---------++----------------------------------------------------------------+
/*
    Executed when player steps on a trigger in the start area that identifies
    the player's character against his/her persistent data.
*/
// -----------------------------------------------------------------------------

#include "vg_s0_oes_const"
#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "aps_include"

// helper functions

int CheckAndKickIfLoading(object player);


// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void main()
{
    object player = GetEnteringObject();
    if (CheckAndKickIfLoading(player))
        return;

    // skip invalid player
    int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    if (!playerId)
        return;

    // existing player with existing character
    int isRegistered = gli(player, SYSTEM_SYS_PREFIX + "_REGISTERED");
    if (isRegistered)
    {
        logentry("[" + SYSTEM_SYS_PREFIX + "] Logging in existing character: " + itos(playerId) + " for account: " + GetPCPlayerName(player));

        // signal PLAYER_LOGIN event (OES_INIT uses this to signal global load data event)
        slo(GetModule(), "USERDEFINED_EVENT_OBJECT", player);
        SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_LOGIN));
    }

    //  new character
    else
    {
        logentry("Registering new character: " + itos(playerId) + " for account: " + GetPCPlayerName(player));

        // modify starting inventory (wipe inventory and gold, add some clothes and few coins as temp)
        ExecuteScript("vg_s1_start_inv", player);

        // register
        ExecuteScript("vg_s1_register", player);

        // move to start location - TEMP skipping intro
        DelayCommand(0.5f, AssignCommand(player, JumpToLocation(GetLocation(GetWaypointByTag(SYS_TAG_PLAYER_START_PC)))));
    }
}


int CheckAndKickIfLoading(object player)
{
    if (GetIsDM(player) || !GetIsPC(player))
        return TRUE;

    // systems still loading - notify and kick player
    if (gli(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT_STATUS")    != -1)
    {
        //DelayCommand(5.0f, BootPCWithMessage(player, 16821714));
        DelayCommand(5.5f, BootPC(player));
        msg(player, R1 + "Server loading - please retry in 5 minutes", TRUE, TRUE);
        return TRUE;
    }

    return FALSE;
}
