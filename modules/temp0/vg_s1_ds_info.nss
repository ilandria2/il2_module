// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Sys info                                     |
// | File    || vg_s1_ds_info.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 29-07-2009                                                     |
// | Updated || 29-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu DS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ds_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     bFloat  =   TRUE;
    string  sTemp, sCase, sMessage;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_DS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "RETURN")
    {
        sMessage    =   C_NEGA + "Your characte returned from the sphere of death and lost a part of its experience.";
        //sMessage    +=  R1 + "\n( !!! ): " + W2 + "Je proti pravidl�m, aby si pamatovala cokoliv od doby 1 hodiny p�ed svou smrti a� po n�vrat ze sf�ry smrti. Opisov�n�, jak vyp�d� sf�ra smrti a pod. je trestn�.";
    }

    else

    if  (sCase  ==  "JUMP")
    {
        sMessage    =   C_NEGA + "Your character died and wakes up in the sphere of death.";
        //sMessage    +=  R1 + "\n( !!! ) " + W2 + "Je proti pravidl�m, aby si pamatovala cokoliv od doby 1 hodiny p�ed svou smrti a� po n�vrat ze sf�ry smrti. Opisov�n�, jak vyp�d� sf�ra smrti a pod. je trestn�.";
    }

    else

    if  (sCase  ==  "DIED")
    {
        sMessage    =   C_NEGA + "*You woke up injured*";
    }

    else

    if  (sCase  ==  "BLEEDING")
    {
        sMessage    =   C_NEGA + "Your character bleeds.";
    }

    else

    if  (sCase  ==  "BLEEDING_STOP")
    {
        sMessage    =   C_POSI + "Your character stopped bleeding.";
    }

    else

    if  (sCase  ==  "BLEED_TO_DEATH")
    {
        sMessage    =   C_NEGA + "Your character bled to dead.";
    }

    else

    if  (sCase  ==  "RESURRECTED")
    {
        sMessage    =   C_NEGA + "Your characte returned from the sphere of death and lost a part of its experience.";
        //sMessage    +=  R1 + "\n( !!! ) " + W2 + "Je proti pravidl�m, aby si pamatovala cokoliv od doby 1 hodiny p�ed svou smrti a� po n�vrat ze sf�ry smrti. Opisov�n�, jak vyp�d� sf�ra smrti a pod. je trestn�.";
    }

    else

    if  (sCase  ==  "ALREADY_DIED")
    {
        sMessage    =   C_NORM + "The physical body of your character lies dead in the world hence can't die again.";
    }

    else

    if  (sCase  ==  "SYSTEM_SKILLS")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_DS_PREFIX + "_OBJECT");
        object  oTarget =   glo(OBJECT_SELF, SYSTEM_DS_PREFIX + "_SYSTEM_SKILLS_OBJECT", -1, SYS_M_DELETE);
        object  oCorpse =   glo(oTarget, SYSTEM_DS_PREFIX + "_CORPSE");
        object  oArea   =   GetArea(oCorpse);
        string  sArea   =   (!GetIsObjectValid(oArea))  ?   "Does not exist"    :   GetName(oArea);

        sMessage    =   C_NORM + "Corpse in the area: " + C_BOLD + sArea;
        bFloat      =   FALSE;
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}
