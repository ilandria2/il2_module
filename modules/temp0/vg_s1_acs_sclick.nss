// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Store Click script                 |
// | File    || vg_s1_acs_sclose.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    store item purchase/sell skript
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_acs_core_c"



void    Disturbed(object oPC, int bIndex)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    int     nMode   =   gli(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_STORE_MODE");
    int     RID, RQTY, RS1, RS2, RS3, RS4, RS5, COST, nItems, nMoney;
    object  oItem;

    RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RID");
    RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RQTY");
    RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC1");
    RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC2");
    RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC3");
    RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC4");
    RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC5");
    COST    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_COST");

    //  hrac kupuje predmety od npc
    if  (nMode  ==  ACS_STORE_MODE_BUY)
    {
        nMoney  =   ACS_GetMoney(oPC);

        //  dostatek penez
        if  (nMoney >=  COST)
        {
            oItem   =   ACS_CreateItemUsingRecipe(RID, RQTY, li(), oPC, FALSE, RS1, RS2, RS3, RS4, RS5);

            ACS_GiveMoney(oPC, -COST);
            //slo(oPC, SYSTEM_ACS_PREFIX + "_STORE_BOUGHT", oItem);
            //sli(oPC, SYSTEM_ACS_PREFIX + "_STORE_PRICE", COST);
            SYS_Info(oPC, SYSTEM_ACS_PREFIX, "STORE_BOUGHT");
        }

        //  nedostatek penez
        else
        {
            SYS_Info(oPC, SYSTEM_ACS_PREFIX, "STORE_NOT_ENOUGH_MONEY");
        }
    }

    else

    //  hrac prodava predmety npc
    if  (nMode  ==  ACS_STORE_MODE_SELL)
    {
        //SpawnScriptDebugger();
        nItems  =   ACS_GetTargetHasRecipe(oPC, RID, RS1, RS2, RS3, RS4, RS5);

        //  dostatecna mnozstvi predmetu
        if  (nItems >=  -RQTY)
        {
            ACS_RemoveItemUsingRecipe(RID, -RQTY, oPC, RS1, RS2, RS3, RS4, RS5);
            ACS_GiveMoney(oPC, COST);
            SYS_Info(oPC, SYSTEM_ACS_PREFIX, "STORE_SOLD");
        }

        //  nedostatecne mnozstvi predmetu
        else
        {
            SYS_Info(oPC, SYSTEM_ACS_PREFIX, "STORE_NOT_ENOUGH_ITEMS");
        }
    }

    //  perzistentni ulozeni transakce (statistika)
    sls(oPC, SYSTEM_ACS_PREFIX + "_PSI_TYPE", "XSTORE");
    sli(oPC, SYSTEM_ACS_PREFIX + "_PSI_XSTORE_ID", bIndex);
    ExecuteScript("vg_s1_acs_psi", oPC);
}




// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (GetTag(OBJECT_SELF)    !=  "VG_P_SO_INVENTOR") return;

    object  oGUI    =   OBJECT_SELF;
    object  oPC     =   GetLastDisturbed();
    object  oItem   =   GetInventoryDisturbItem();
    int     nType   =   GetInventoryDisturbType();

    //  pridan predmet do inventare - vraceni zpet do inventare hrace
    if  (nType  ==  INVENTORY_DISTURB_TYPE_ADDED)
    {
        //msg(oPC, "buy: " + GetName(oItem));
        CopyItem(oItem, oPC, TRUE);

        SetPlotFlag(oItem, FALSE);
        Destroy(oItem);
    }

    else

    //  odebran predmet z inventare
    if  (nType  ==  INVENTORY_DISTURB_TYPE_REMOVED)
    {
        string  sTag    =   GetTag(oItem);
        int     bIndex  =   gli(oItem, SYSTEM_ACS_PREFIX + "_XSTORE");

        CopyItem(oItem, oGUI, TRUE);

        SetPlotFlag(oItem, FALSE);
        Destroy(oItem);

        DelayCommand(0.1f, Disturbed(oPC, bIndex));
    }
}

