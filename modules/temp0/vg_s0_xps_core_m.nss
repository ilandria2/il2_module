// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Core M                                 |
// | File    || vg_s0_xps_core_m.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 26-12-2007                                                     |
// | Updated || 31-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu zkusenosti (XPS) typu "Math"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || XPS_GetGainedXPFromPhysicalAttack                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte ziskane zkusenosti z provedenyho fyzickyho utoku
//
// -----------------------------------------------------------------------------
int     XPS_GetGainedXPFromPhysicalAttack(int nLevSource, int nLevTarget, float fCRTarget);



// +---------++----------------------------------------------------------------+
// | Name    || XPS_GetLevelByXP                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte uroven zkusenosti tridy nXPClass podle hodnoty zkusenosti nValue
//
// -----------------------------------------------------------------------------
int     XPS_GetLevelByXP(int nValue, int nXPClass);



// +---------++----------------------------------------------------------------+
// | Name    || XPS_GetXPByLevel                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte zkusenosti tridy nXPClass podle urovne zkusenosti nLevel
//
// -----------------------------------------------------------------------------
int     XPS_GetXPByLevel(int nLevel, int nXPClass);



// +---------++----------------------------------------------------------------+
// | Name    || XPS_AlterXP                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Upravi zkusenosti tridy nXPClass hraci oPC o hodnotu nValue
//
// -----------------------------------------------------------------------------
void    XPS_AlterXP(object oPC, int nXPClass, int nValue, int bSet = FALSE, int bMessage = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || XPS_AbsorbModeSequence                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Aplikuje ucinky sekvence vstrebavaciho modu na hrace oPC
//
// -----------------------------------------------------------------------------
void    XPS_AbsorbModeSequence(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || XPS_AlterAbsorbXP                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Upravi zkusenosti na vstrebavani tridy nXPClass hraci oPC o hodnotu nValue
//
// -----------------------------------------------------------------------------
void    XPS_AlterAbsorbXP(object oPC, int nXPClass, int nValue, int bSet = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || XPS_AlterLevel                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Upravi uroven zkusenosti tridy nXPClass hraci oPC o hodnotu nValue
//
// -----------------------------------------------------------------------------
void    XPS_AlterLevel(object oPC, int nXPClass, int nValue, int bSet = FALSE, int bMessage = TRUE);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                      XPS_GetGainedXPFromPhysicalAttack                    |
// +---------------------------------------------------------------------------+



int     XPS_GetGainedXPFromPhysicalAttack(int nLevSource, int nLevTarget, float fCRTarget)
{
    int     nResult;

 /*   if  (nLevTarget >   0)
    {
        float   A, B, C, D;

        if  ((nLevTarget - nLevSource)  >   0)
        {
            C   =   pow(XPS_M_XP_KOEFICIENT, itof(nLevTarget - nLevSource));
            D   =   1.0f;
        }

        else
        {
            C   =   itof(XPS_M_DELIMITER - (nLevSource - nLevTarget));
            D   =   itof(XPS_M_DELIMITER);
        }

        A       =   itof(XPS_M_XP_BASE);
        B       =   pow(XPS_M_XP_INCREMENT, itof(nLevSource - 1));
        nResult =   ftoi((A * B * C) / D);
        nResult +=  (nResult    >   0)  ?   ftoi(fCRTarget) :   0;
    }
 */
    return nResult;
}



// +---------------------------------------------------------------------------+
// |                          XPS_GetLevelByXP                                 |
// +---------------------------------------------------------------------------+



int     XPS_GetLevelByXP(int nValue, int nXPClass)
{
    if  (nValue <=  0)  return  1;

    int     nResult, nRow, nTemp, nLast;

    switch  (nXPClass)
    {
        case    XPS_M_CLASS_XPS_PLAYER_CHARACTER:   nLast   =   40; break;
        default:                                    nLast   =   50; break;
    }

    nRow    =   1;
    nTemp   =   XPS_GetXPByLevel(nRow, nXPClass);

    while   (nRow   <   nLast)
    {
        if  (nTemp  >   nValue)
        {
            nResult =   nRow - 1;
            break;
        }

        nRow++;
        nTemp   =   XPS_GetXPByLevel(nRow, nXPClass);
    }

    nResult =   (nRow   >=  nLast)  ?   nLast   :   nResult;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                             XPS_GetXPByLevel                              |
// +---------------------------------------------------------------------------+



int     XPS_GetXPByLevel(int nLevel, int nXPClass)
{
    if  (nLevel <=  0)  return  0;

    int     nResult, nLast;
    string  sClass;

    switch  (nXPClass)
    {
        case    XPS_M_CLASS_XPS_PLAYER_CHARACTER:   nLast   =   40; sClass  =   "PC";   break;
        default:                                    nLast   =   50; sClass  =   "N";    break;
    }

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT");

    nLevel  =   (nLevel >   nLast)  ?   nLast   :   nLevel;
    nResult =   gli(oSystem, SYSTEM_XPS_PREFIX + "_CLASS_" + sClass + "_LEVEL", nLevel);

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                               XPS_AlterXP                                 |
// +---------------------------------------------------------------------------+



void    XPS_AlterXP(object oPC, int nXPClass, int nValue, int bSet = FALSE, int bMessage = TRUE)
{
    if  (!GetIsObjectValid(oPC))    return; //  neexistuje
    if  (!GetIsPC(oPC))             return; //  neni hrac
    if  (GetIsDM(oPC))              return; //  je dm

    int     nXP, nLevel, nNextXP, nPrevXP, nNewXP, nNewLev;

    nXP     =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_XP");
    nLevel  =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_LEVEL");

    //  prepocet xp
    if  (!nLevel)
    {
        nLevel  =   XPS_GetLevelByXP(nXP, nXPClass);

        sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_LEVEL", nLevel);
    }

    nNewLev =   nLevel;
    nNextXP =   XPS_GetXPByLevel(nLevel + 1, nXPClass);
    nPrevXP =   XPS_GetXPByLevel(nLevel, nXPClass);
    nNewXP  =   (!bSet * nXP) + nValue;
    nNewXP  =   (nNewXP <   0)  ?   0   :   nNewXP;

    //  Nove XP = tomu co jiz ma
    if  (nNewXP ==  nXP)    return;

    //  Zmena urovne
    if
    (
        nNewXP <   nPrevXP
    ||  nNewXP >=  nNextXP
    )
    {
        nNewLev =   XPS_GetLevelByXP(nNewXP, nXPClass);
        nPrevXP =   XPS_GetXPByLevel(nNewLev, nXPClass);
        nNextXP =   XPS_GetXPByLevel(nNewLev + 1, nXPClass);
    }

    /*
    //  Kontrola podminek pro zmenu zkusenosti
    sls(oPC, SYSTEM_XPS_PREFIX + "_ALTER_XP_CONDITION_PARAMETERS", "&" + itos(nXP) + "&_&" + itos(nNewXP) + "&_&" + itos(nLevel) + "&_&" + itos(nNewLev) + "&_&" + itos(nXPClass) + "&");
    ExecuteScript(XPS_NCS_ALTER_XP_CONDITION, oPC);

    int bCondition  =   gli(oPC, SYSTEM_XPS_PREFIX + "_ALTER_XP_CONDITION_RESULT", -1, SYS_M_DELETE);

    if  (!bCondition)   return;
    */

    //  Zmena urovne
    if  (nNewLev    !=  nLevel)
    {
        //  Zmena zkusenosti
        if  (!nXPClass) SetXP(oPC, nPrevXP);

        sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_LEVEL", nNewLev);

        if  (bMessage)  SYS_Info(oPC, SYSTEM_XPS_PREFIX, "NEW_LEVEL_&" + itos(nLevel) + "&_&" + itos(nNewLev) + "&_&" + itos(nXPClass) + "&");
    }

    sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_XP", nNewXP);

    if  (bMessage && XPS_B_ENABLE_NORMAL_XP_NOTIFICATION)   SYS_Info(oPC, SYSTEM_XPS_PREFIX, "ALTER_XP_&" + itos(nXP) + "&_&" + itos(nNewXP) + "&_&" + itos(nXPClass) + "&");
}



// +---------------------------------------------------------------------------+
// |                          XPS_AbsorbModeSequence                           |
// +---------------------------------------------------------------------------+



void    XPS_AbsorbModeSequence(object oPC)
{
    int nRow    =   1;
    int nLast   =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID_I_ROWS");
    int nXPClass, nValue, nLevel, nAlter, n, nTotal;

    //  projde vsechny ulozene tridy zkusenosti, do kt. se nahromadili nejake xp
    while   (nRow   <=  nLast)
    {
        nXPClass    =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID", nRow);
        nValue      =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB");
        nLevel      =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_LEVEL");
        nAlter      =   ((XPS_M_XP_ABSORB_SEQUENCE_MULTIPLIER * nLevel) / 2) + 1 + Random(XPS_M_XP_ABSORB_SEQUENCE_MULTIPLIER * nLevel);
        nAlter      +=  ftoi(itof((nAlter) / 100) * GetAbilityScore(oPC, ABILITY_INTELLIGENCE));

        //  vyprazdneni 1 tridy zkusenosti
        if  (nAlter >=  nValue)
        {
            int nNext;

            nAlter  =   nValue;
            n       =   nRow;

            //  presunuti stavajicich trid zkusenosti na misto vyprazdnene tridy
            while   (n  <   nLast)
            {
                nNext   =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID", n + 1);

                sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID", nNext, n);
                sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nNext) + "_ABSORB_ROW", n);

                n++;
            }

            //  smazani posledni tridy zkusenosti (byla presunuta na predposledni)
            sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID", -1, n, SYS_M_DELETE);
            sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nNext) + "_ABSORB_ROW", -1, -1, SYS_M_DELETE);
            sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB_ROW", -1, -1, SYS_M_DELETE);

            //  snizeni poctu trid zkusenosti
            nLast--;
        }

        //  snizeni hodnoty "XP na vstrebani"
        sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB", nAlter, -1, SYS_M_DECREMENT);
        sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_SUM_ABSORB", nAlter, -1, SYS_M_DECREMENT);

        //  zvyseni hodnoty "ziskane XP"
        XPS_AlterXP(oPC, nXPClass, nAlter);

        nTotal  +=  nAlter;
        nRow++;
    }

//    if  (nTotal >   0)  XPS_AlterXP(oPC, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nTotal / 4);
}



// +---------------------------------------------------------------------------+
// |                             XPS_AlterAbsorbXP                             |
// +---------------------------------------------------------------------------+



void    XPS_AlterAbsorbXP(object oPC, int nXPClass, int nValue, int bSet = FALSE)
{
    if  (!XPS_B_ENABLE_ABSORB_MODE)
    {
        //  bSet nesmi byt TRUE, jinak je normalni XP bude nahrazeno tim
        //  XP, ktere se melo vstrebat
        XPS_AlterXP(oPC, nXPClass, nValue, FALSE);
        return;
    }

    if  (!GetIsObjectValid(oPC))    return; //  neexistuje
    if  (!GetIsPC(oPC))             return; //  neni hrac
    if  (GetIsDM(oPC))              return; //  je dm

    int nLevel  =   GetHitDice(oPC);
    int nXP     =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB");
    int nSumXP  =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_SUM_ABSORB");
    int nSumMax =   XPS_M_ABSORB_VALUE_BASE + (nLevel * XPS_M_ABSORB_VALUE_PER_LEVEL);
    int nNewXP;

    if  (nSumXP >=  nSumMax)
    {
        SYS_Info(oPC, SYSTEM_XPS_PREFIX, "ABSORB_FIRST");
        return;
    }

    //  hromadeni xp na lvl 1 je zakazane - hrac musi ziskat xp od DM za RP
    if  (nLevel ==  1)
    {
        //SYS_Info(oPC, SYSTEM_XPS_PREFIX, "CONDITION_LEVEL_ONE_PERMITTED");
        return;
    }

    nNewXP  =   (!bSet * nXP) + nValue;
    nNewXP  =   (nNewXP <   0)  ?   0   :   nNewXP;

    if  ((nSumXP + nNewXP - nXP)  >=  nSumMax)
    {
        nNewXP  =   nSumMax - nSumXP + nXP;

        SYS_Info(oPC, SYSTEM_XPS_PREFIX, "ABSORB_FIRST");
    }

    int bAbsorb =   gli(oPC, SYSTEM_XPS_PREFIX + "_ABSORB_MODE");

    if  (bAbsorb)
    {
        sli(oPC, SYSTEM_XPS_PREFIX + "_ABSORB_MODE", FALSE);

        if  (XPS_B_ENABLE_ABSORB_MODE)
        SYS_Info(oPC, SYSTEM_XPS_PREFIX, "EXITING_ABSORB_MODE");
    }

    bAbsorb =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB_ROW");

    if  (!bAbsorb)
    {
        bAbsorb =   -2;

        sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB_ROW", gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID_I_ROWS") + 1);
    }

    sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_ABSORB_ID", nXPClass, bAbsorb);
    sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_ABSORB", nNewXP);
    sli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_SUM_ABSORB", nNewXP - nXP, -1, SYS_M_INCREMENT);
    sli(oPC, SYSTEM_XPS_PREFIX + "_ABSORB_HEARTBEATS", -1, -1, SYS_M_DELETE);
}



// +---------------------------------------------------------------------------+
// |                                XPS_AlterLevel                             |
// +---------------------------------------------------------------------------+



void    XPS_AlterLevel(object oPC, int nXPClass, int nValue, int bSet = FALSE, int bMessage = TRUE)
{
    if  (!GetIsObjectValid(oPC))    return; //  neexistuje
    if  (!GetIsPC(oPC))             return; //  neni hrac
    if  (GetIsDM(oPC))              return; //  je dm

    int nLevel  =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_LEVEL");
    int nNewLev =   (bSet)  ?   nValue  :   (nLevel + nValue);
    int nNewXP  =   XPS_GetXPByLevel(nNewLev, nXPClass);

    XPS_AlterXP(oPC, nXPClass, nNewXP, TRUE, bMessage);
}
