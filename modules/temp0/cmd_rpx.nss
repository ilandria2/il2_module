// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_rpx.nss                                                    |
// | Author  || VirgiL                                                         |
// | Created || 15-12-2009                                                     |
// | Updated || 15-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "RPX"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_xps_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "RPX";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    //int     nValue  =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oPlayer =   CCS_GetConvertedObject(sLine, 1, FALSE);
    string  sMessage;

    if  (!GetIsObjectValid(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� objekt";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Nen� to hr��";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (GetIsDM(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Vybran� hr�� je DM - jim nelze nastavit RPX";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int bRPX    =   gli(oPlayer, SYSTEM_XPS_PREFIX + "_RPX");

    if  (bRPX)
    {
        sli(oPlayer, SYSTEM_XPS_PREFIX + "_RPX", -1, -1, SYS_M_DELETE);

        sMessage    =   Y1 + "( ? ) " + W2 + "Deaktivace RPX u hr��e [" + G2 + GetPCPlayerName(oPlayer) + " / " + GetName(oPlayer) + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
    }

    else

    if  (!bRPX)
    {
        int nNewValue   =   5 + 2 * GetHitDice(oPlayer);

        sli(oPlayer, SYSTEM_XPS_PREFIX + "_RPX", nNewValue);

        sMessage    =   Y1 + "( ? ) " + W2 + "Nastavov�n� m�du RPX u hr��e [" + G2 + GetPCPlayerName(oPlayer) + " / " + GetName(oPlayer) + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
    }

    else
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� hodnota m�du RPX";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
