// Text Interaction System (TIS) Interaction script - RELOCATE

#include "vg_s0_tis_inter"
#include "vg_s0_ple_core"

const int ACTION_TYPE  = 15;

//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    return TRUE;
}

//////// ACTION'S BODY
void Do()
{
    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        if (gli(this.Player, "i_ple"))SpawnScriptDebugger();
        PLE_StartLoop(this.Player, this.Object);
    }

    // delayed sequence
    else
    {
        // ......

        // last sequence
        if (TRUE)
            this.DelaySequence = 0.0;
    }
}



/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

