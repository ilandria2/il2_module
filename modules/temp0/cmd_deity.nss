// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_deity.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "DEITY"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "DEITY";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string      sDeity  =   CCS_GetConvertedString(sLine, 1, FALSE);
    object      oTarget =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string      sMessage, sCurrent;

    if  (!GetIsObjectValid(oTarget))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, TRUE);
        return;
    }

    if  (GetObjectType(oTarget) !=  OBJECT_TYPE_CREATURE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type - target [" + R2 + GetTag(oTarget) + " / " + GetName(oTarget) + W2 + "] " + R2 + "is not a creature";

        msg(oPC, sMessage, TRUE);
        return;
    }

    sCurrent    =   GetDeity(oTarget);

    if  (upper(sDeity)  ==  "GET")
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "DEITY of creature [" + Y2 + GetPCPlayerName(oTarget) + " / " + GetName(oTarget) + W2 + "] is [" + Y1 + sCurrent + W2 + "]";
    }

    else
    {
        SetDeity(oTarget, sDeity);

        sMessage    =   Y1 + "( ? ) " + W2 + "Change of DEITY on creature [" + Y2 + GetTag(oTarget) + " - " + GetName(oTarget) + W2 + "]:\n" +
                        Y2 + "from: '" + W2 + sCurrent + "\n" +
                        Y2 + "to: '" + W2 + sDeity + "\n";
    }

    msg(oPC, sMessage, TRUE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
