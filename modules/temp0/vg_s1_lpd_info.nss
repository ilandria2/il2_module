// dialogue system information script

#include    "vg_s0_lpd_core_c"
#include    "x0_i0_position"

void ShowMenu(object player);

void main()
{
    string sTemp, sCase, sMessage;
    int bFloat = TRUE;
    object player = OBJECT_SELF;

    sTemp = gls(player, SYSTEM_LPD_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase = GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if(gli(player,"lpd_info"))SpawnScriptDebugger();

    if  (sCase  ==  "MENU_LIST")
    {
        AssignCommand(player, ClearAllActions(TRUE));
        AssignCommand(player, DelayCommand(0.1f, ActionDoCommand(ShowMenu(player))));
    }

    else if (sCase == "NO_TARGET")
        sMessage = C_BOLD + "Noone nearby to converse with";

    else if (sCase == "START_CONV")
        sMessage = C_BOLD + "Use the " + C_INTE + "/hi" + C_BOLD + " channel command to begin conversation";

    else if (sCase == "ABORTED")
        sMessage = C_BOLD + "Dialogue aborted.";

    else if (sCase == "STARTED")
        sMessage = C_BOLD + "Dialogue started";

    else if (sCase == "ENDED")
        sMessage = C_BOLD + "Dialogue ended";

    else

    if  (sCase  ==  "OCCUPIED")
    sMessage    =   C_BOLD + "The target is busy";

    else

    if  (sCase  ==  "SECOND_DIALOG")
    sMessage    =   C_BOLD + "First end the active conversation";

    else

    if  (sCase  ==  "NOT_IN_G")
    sMessage    =   C_BOLD + "You are not in any dialogue.";

    msg(player, sMessage, bFloat, FALSE);
}



void ShowMenu(object player)
{
    if (gli(player, "lpd_menu")) SpawnScriptDebugger();

    object oSource = LPD_GetConversator(player);
    object oDialog = LPD_GetDialog(oSource);
    int n = 1;
    int nLast = gli(oSource, SYSTEM_LPD_PREFIX + "_SOURCE_S_ROWS");

    string message = C_BOLD + "Your dialogue options:";
    while (n <= nLast)
    {
        string node = gls(oSource, SYSTEM_LPD_PREFIX + "_SOURCE", n);
        string sText = LPD_GetNodeText(oDialog, node);
        string sKeyword = LPD_GetNodeKeyword(oDialog, node);

        if (left(sText, 1) == "$")
            sText = gls(player, sub(sText, 1, len(sText) - 1));

        message += "\n" + C_INTE + "/" + sKeyword + C_BOLD + " (" + sText + ")";
        n++;
    }

    message += "\n" + C_INTE + "/bye" + C_BOLD + " <ends the dialogue>";

    msg(player, message, FALSE, FALSE);
}

