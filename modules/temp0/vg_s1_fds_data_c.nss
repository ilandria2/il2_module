// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) Persistent data load script        |
// | File    || vg_s1_fds_data_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_fds_const"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;

    sLog    =   "[" + SYSTEM_FDS_PREFIX + "] Checking table [fds_stomach]";
    sQuerry =   "DESCRIBE fds_stomach";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_FDS_PREFIX + "] Table [fds_stomach] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_FDS_PREFIX + "] Table [fds_stomach] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE fds_stomach " +
        "(" +
            "PlayerID INT(6) NOT NULL PRIMARY KEY," +
            "Hunger VARCHAR(3) DEFAULT NULL," +
            "Thirst VARCHAR(3) DEFAULT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE fds_stomach";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_FDS_PREFIX + "] Table [fds_stomach] created successfully"; else
        sLog    =   "[" + SYSTEM_FDS_PREFIX + "] Table [fds_stomach] error in creation";

        logentry(sLog);
    }
}
