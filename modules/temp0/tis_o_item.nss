// verifies conditions for ITEM interactive object detection

#include "vg_s0_tis_const"
#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"

void main()
{
    object oPC = OBJECT_SELF;
    int bResult = TRUE;

    sli(oPC, SYS_VAR_RETURN, bResult);
}

