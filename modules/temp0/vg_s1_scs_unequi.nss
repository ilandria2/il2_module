// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) OnPlayerUnEquipItem event script       |
// | File    || vg_s1_scs_unequi.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-05-2009                                                     |
// | Updated || 16-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pokud slozeny predmet obsahuje vlastnosti typu "bonus (obnoveni) many"
    pak se nastavi prislusne promenne udavajici aktualni uroven techto bonusu,
    ktere se nactou v matematickych funkcich na prepocet many
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_core_m"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object          oPC     =   GetPCItemLastUnequippedBy();
    object          oItem   =   GetPCItemLastUnequipped();
    itemproperty    ipTemp  =   GetFirstItemProperty(oItem);

    while   (GetIsItemPropertyValid(ipTemp))
    {
        int     nType   =   GetItemPropertyType(ipTemp);

        if  (nType  ==  SCS_IPRP_TYPE_ADDITIONAL)
        {
            int     nSubType    =   GetItemPropertySubType(ipTemp);
            int     nValue      =   GetItemPropertyCostTableValue(ipTemp);
            string  sLabel      =   Get2DAString("iprp_addcost", "Label", nValue);
            int     nNew, bCalc;
            string  sType;

            nValue  =   stoi(GetSubString(sLabel, FindSubString(sLabel, "_") + 1, 5));

            switch  (nSubType)
            {
                case    SCS_IPRP_SUBTYPE_BONUS_MANA_V:          sType   =   "MAX_MANA_BONUS_VALUE";     bCalc   =   TRUE;   break;
                case    SCS_IPRP_SUBTYPE_BONUS_MANA_P:          sType   =   "MAX_MANA_BONUS_PERCENT";   bCalc   =   TRUE;   break;
                case    SCS_IPRP_SUBTYPE_BONUS_REGEN_MANA_V:    sType   =   "REGEN_MANA_BONUS_VALUE";   break;
                case    SCS_IPRP_SUBTYPE_BONUS_REGEN_MANA_P:    sType   =   "REGEN_MANA_BONUS_PERCENT"; break;
            }

            int     nOld    =   gli(oPC, SYSTEM_SCS_PREFIX + "_" + sType);

            nNew    =   nOld - nValue;
            nNew    =   (nNew   <   0)  ?   0   :   nNew;

            sli(oPC, SYSTEM_SCS_PREFIX + "_" + sType, nValue, -1, SYS_M_DECREMENT);
            SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_BONUS_&" + itos(nSubType) + "&_&" + itos(nOld) + "&_&" + itos(nNew) + "&");

            if  (bCalc)
            {
                int     nManaMax    =   SCS_CalculateMaxMana(oPC);
                int     nMana       =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA");

                if  (nMana  >   nManaMax)
                {
                    sli(oPC, SYSTEM_SCS_PREFIX + "_MANA", nManaMax);
                    SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&" + itos(nMana) + "&_&" + itos(nManaMax) + "&");
                }
                sli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX", nManaMax);
            }
        }

        ipTemp  =   GetNextItemProperty(oItem);
    }
}
