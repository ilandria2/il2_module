/*

all events are mainly controlled by OES (object event) system


==== MODULE_HEARTBEAT (approx every 6 seconds):

    // 10x heartbeat = 1x custom loop event

    if  (gli(oSystem, SYSTEM_OES_PREFIX + "_HEARTBEAT") ==  OES_CUSTOM_LOOP_HEARTBEATS)
    {
        sli(oSystem, SYSTEM_OES_PREFIX + "_HEARTBEAT", 0);
        OES_ActionEvent(OES_EVENT_O_ON_CUSTOM_LOOP);
    }

    else
        sli(oSystem, SYSTEM_OES_PREFIX + "_HEARTBEAT", 1, -1, SYS_M_INCREMENT);


==== MODULE_LOAD:
     sls(GetModule(), "NWNX!INIT", "1");
     SQLInit();
     SQLExecDirect("set names cp1250");
     AssignCommand(OBJECT_SELF, ExecuteScript("vg_s1_oes_loadsy", oSelf));


==== loadsy:

system load order:
1. oes
2. sys
3. tis ....

then for each system:
    oSystem =   CreateObject(OBJECT_TYPE_WAYPOINT, SYS_RSRF_SYSTEM_OBJECT, lCreate, FALSE, "SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    ExecuteScript(GetStringLowerCase("vg_s1_" + sSystem + "_init"), GetModule());

!! every system_init script optionally executes:
    CheckPersistentTables(sSystem);


==== oes_init:

    sls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CUSTOM_LOOP) + "_SCRIPT", "vg_s1_oes_savedt", -2);
    sls(oSystem, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_oes_loaddt", -2);


==== sys_init:

    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
        CheckPersistentTables(sSystem);

    // calendar hours - so CUSTOM_LOOP can be triggered exactly every 1 minute
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_sys_time", -2);

    // trigered first - validates player's identity and triggers player_relog (not login) event
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CLIENT_ENTER) + "_SCRIPT", "vg_s1_sys_client", -2);

    // trigered when player enters the start area - handles registration or login
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_A_ON_ENTER) + "_SCRIPT", "vg_s1_sys_areent", -2);


    NOTE:
    RELOG event occurs whenever player relogs
    LOGIN event occurs when player connects the server for the first time after it has restarted

================ sys_time

    sli(oSystem, SYSTEM_SYS_PREFIX + "_HBS", 1, -1, SYS_M_INCREMENT);
    sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_HOURS", DateToHours(nYear, nMonth, nDay, nHour));
    SavePersistentData(oSystem, SYSTEM_SYS_PREFIX, "SETTINGS");
    ExecuteScript("vg_s1_sys_party", OBJECT_SELF);


================ sys_client
//TODO: prevent this script from executing when entering standard areas / after login

    // account protection
    if (!CheckPlayerCdKey(player))
        return;

    // init and check player id and account
    int playerId = GetAndCheckPlayerIdAndAccount(player);
    if (!playerId)
        return;

    // EXISTING player charactger
    if (IsRegistered(playerId))
    {
        sli(player, SYSTEM_SYS_PREFIX + "_REGISTERED", TRUE);
        slo(GetModule(), "USERDEFINED_EVENT_OBJECT", player);
        SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_RELOG));
    }


================= sys_areent


    // ignore invalid player
    int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    if (!playerId)
        return;

    object wpInit = GetNearestObjectByTag(SYS_TAG_PLAYER_START_INIT, player);

    if (GetIsObjectValid(wpInit))
    {
        //  debug mode
        if (gli(GetModule(), "DEBUG"))
            ExecuteScript("_test", player);

        //  live mode - move to start_il waypoint
        else
        {
            AssignCommand(player, JumpToLocation(GetLocation(wpInit)));
        }
    }



================== START_IL (from above) trigger script

    object player = GetEnteringObject();
    if (CheckAndKickIfLoading(player))
        return;

    // skip invalid player
    int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    if (!playerId)
        return;

    // existing player with existing character
    int isRegistered = gli(player, SYSTEM_SYS_PREFIX + "_REGISTERED");
    if (isRegistered)
    {
        // signal PLAYER_LOGIN event (OES_INIT uses this to signal global load data event)
        slo(GetModule(), "USERDEFINED_EVENT_OBJECT", player);
        SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_LOGIN));
    }

    //  new character
    else
    {
        // modify starting inventory (wipe inventory and gold, add some clothes and few coins as temp)
        ExecuteScript("vg_s1_start_inv", player);

        // register
        ExecuteScript("vg_s1_register", player);

        // move to start location - TEMP skipping intro
        DelayCommand(0.5f, AssignCommand(player, JumpToLocation(GetLocation(GetWaypointByTag(SYS_TAG_PLAYER_START_PC)))));
    }


================== register:

    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    if (!gli(oSystem, SYSTEM_SYS_PREFIX + "_SQL"))
    {
        SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "REGISTER_FAILURE");
        logentry("[" + SYSTEM_SYS_PREFIX + "]: PlayerID registration failed: NwNX2 not active", TRUE);
    }

    else
    {
        int bExists = gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_KEY_EXISTS");

        if (!bExists)
            SavePersistentData(OBJECT_SELF, SYSTEM_SYS_PREFIX, "KEY"); /////// generates account protection password
        SavePersistentData(OBJECT_SELF, SYSTEM_SYS_PREFIX, "REG"); /////////// sets RegisteredOn among with other fields in sys_players
    }

    // modify character properties (AB, AC)
    DelayCommand(5.0f, ModChar(OBJECT_SELF));

    // signal login event
    slo(GetModule(), "USERDEFINED_EVENT_OBJECT", OBJECT_SELF);
    SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_LOGIN));


*/
