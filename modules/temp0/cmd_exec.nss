// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_exec.nss                                                   |
// | Author  || VirgiL                                                         |
// | Created || 02-08-2009                                                     |
// | Updated || 02-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "EXEC"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "EXEC";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sScript =   CCS_GetConvertedString(sLine, 1, FALSE);
    object  oObject =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;

    if  (sScript    ==  "")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid script name";

        msg(oPC, sMessage);
        return;
    }

    if  (!GetIsObjectValid(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage);
        return;
    }

    ExecuteScript(sScript, oObject);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
