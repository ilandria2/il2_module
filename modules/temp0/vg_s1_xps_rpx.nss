// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Role playing XP awards                 |
// | File    || vg_s1_xps_rpx.nss                                              |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 06-06-2009                                                     |
// | Updated || 06-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    V pravidelnych sekvencich navysi zkusenosti postavy vsem hracum, ktery byli
    DMkem oznaceni jako "RPX"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"
#include    "vg_s0_ccs_core_t"



void    ResetRPX()
{
    object  oTemp   =   GetFirstPC();
    int     nRPX;

    while   (GetIsObjectValid(oTemp))
    {
        nRPX    =   gli(oTemp, SYSTEM_XPS_PREFIX + "_RPX");

        if  (nRPX   >   0)
        sli(oTemp, SYSTEM_XPS_PREFIX + "_RPX", -1, -1, SYS_M_DELETE);

        oTemp   =   GetNextPC();
    }
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    return;
    object  oXPS    =   GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT");
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");
    int     nLast   =   gli(oXPS, SYSTEM_XPS_PREFIX + "_RPX_HB");

    if  (nHBs - nLast   <   XPS_M_RPX_HEARTBEATS)   return;

    object  oPC     =   GetFirstPC();
    int     bPMDM, bRPX, nRPX;

    while   (GetIsObjectValid(oPC))
    {
        nRPX    =   gli(oPC, SYSTEM_XPS_PREFIX + "_RPX");

        //  mod RPX byl pro danyho hrace aktivovan
        if  (nRPX   >   0)
        {
            bRPX    =   TRUE;
            XPS_AlterXP(oPC, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nRPX);
        }

        //  mod RPX nebyl pro danyho hrace aktivovan
        else
        {
            int nLevel  =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(XPS_M_CLASS_XPS_PLAYER_CHARACTER) + "_LEVEL");
            int nValue  =   (nLevel ==  1)  ?   40  :   40 * (nLevel / 4);

            XPS_AlterXP(oPC, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nValue, FALSE, TRUE);
        }

        if  (GetIsDM(oPC)
        ||  CCS_GetHasAccess(oPC, "PM"))
        bPMDM   =   TRUE;
        oPC     =   GetNextPC();
    }

    //  ve hre nezustal ani 1 PM nebo DM
    if  (!bPMDM
    &&  bRPX)
    ResetRPX();
    sli(oXPS, SYSTEM_XPS_PREFIX + "_RPX_HB", nHBs);

    if  (bRPX)
    SYS_Info(oSystem, SYSTEM_XPS_PREFIX, "RPX_INFO");
}
