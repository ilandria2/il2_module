const int FILE_TIME_OF_ACCESS = 0;
const int FILE_TIME_OF_CREATE = 1;
const int FILE_TIME_OF_WRITE = 2;

const int FILE_SEARCH_FOR_FILES = 1;
const int FILE_SEARCH_FOR_DIRECTORIES = 16;

const int FILE_OPEN_MODE_FOR_READING = 1;
const int FILE_OPEN_MODE_FOR_WRITING = 2;

//string spacer = "................................................................................................................................................................................................................................................................";//...............................................................................................................................................................................................................................................................";
               //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456    789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901
               //         1         2         3         4         5         6         7         8         9        10        11        12        13        14        15        16        17        18        19        20        21        22        23        24        25            26        27        28        29        30        31        32        33        34        35        36        37        38        39        40        41        42        43        44        45        46        47        48        49        50        51

struct time
{
int Year,Month,Day,Hour,Minute,Second;
};

////////////////////////////////////////////////////////////////////////////////
//                          function prototypes                               //
////////////////////////////////////////////////////////////////////////////////

//return current unix time (seconds since midnight 1. January 1970)
int GetSystemTime();

//TRUE if file exists, FALSE otherwise
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file,
//path using / backslash and note that partial path start in NWN root
//example of full path "C:/NeverWinterNights/NWN/modules
//example of partial path "modules"
int FileExists(string filename, string path="");

//returs TRUE if file was succesfully deleted
//in case that file don't exist, or file is opened at that time, this will return
//FALSE and won't delete given file
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileDeleteFile(string filename, string path="");

//returs TRUE if directory was succesfully deleted, FALSE otherwise
//dirname - directory name eg. "1"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileDeleteDirectory(string dirname, string path="");

//returns MD5 for current file
//filename - file name eg. "readme.txt"
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
string FileMD5(string filename, string path="");

//close opened file
int FileClose();

//write to the opened file sString and return TRUE/FALSE
int FileWrite(string sString);

//returns file size in bytes
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileSize(string filename, string path="");

//open file for reading, writing or both (const are "flags" and can be put into
//this function both with | )
//returns TRUE if file is opened, FALSE otherwise (already opened/don't exists...)
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileOpen(string filename, string path="", int nMode = FILE_OPEN_MODE_FOR_READING);

//this will copy file to elsewhere and returns TRUE if file was succesfully copied
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
//newpath - full or partial path to the new directory where you want to file be copied
//newname - when "" (default), then the default file name will be used
//rewrite_if_exists - TRUE or FALSE, TRUE can be quite usefull for backups
int FileCopy(string filename, string path, string newpath, string newname="", int rewrite_if_exists=FALSE);

//reads 256 characters from the file, or less when encounter new line
//returns "" when encounter EOF
string FileRead();

//return unix time (see GetSystemTime) when the file was last changed/opened/accessed or 0 if the file is in use
//path - full path to a file
//nTime - FILE_TIME_OF_*
//note - FILE_TIME_OF_ACCESS will return current time, this is rather bug, however could be usefull
//filename - file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileTime(string filename, string path="", int nTime=FILE_TIME_OF_WRITE);

//this will create a new file in specified path and with given name
//returns TRUE if creating will be sucesfull, FALSE otherwise
//if file with same name is already there, nothing happen
//filename - desired file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileCreateFile(string filename, string path="");

//this will create a new file in specified path and with given name
//returns TRUE if creating will be sucesfull, FALSE otherwise if file with same name is already there, nothing happen
//dirname - directory name eg. "1"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
int FileCreateDirectory(string dirname, string path="");

//return name of first file in given directory
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
//filter - with "*" (default) you will get every file in this directory
//       - with "*.XYZ" you can filter file by extension
//mode - SEARCH_FOR_*
//     - flags, can be used both with "|"
string FileGetFirst(string path="", string filter="*", int mode=FILE_SEARCH_FOR_FILES);

//return name of next file in given directory
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
//filter - with "*" (default) you will get every file in this directory
//       - with "*.XYZ" you can filter file by extension
//mode - SEARCH_FOR_*
//     - flags, can be used both with "|"
string FileGetNext(string path="", string filter="*", int mode=FILE_SEARCH_FOR_FILES);

//this will rename existing file in specified path to given new name
//returns TRUE if creating will be sucesfull, FALSE otherwise
//filename - desired file name eg. "readme.txt"
//path - full or partial path to the directory which should contain this file for more info look at FileExists function
//newname - desired new file name eg "test.txt"
int FileRenameFile(string filename, string path, string newname);

//using GetSystemTime, this will search through all files in PC account directory and
//return the newly changed
//in order this function get return real newest bic, you have to save character in OnClientEnter
//and call this function with some delay (like 3.0), as ExportSingleCharacter is not instant
//then I would recommend to save the bic into database, so you will have to do this only for new characters
//Note: this is nwscriptwise function using other File functions
//Note: do not work in OnClientExit (but thats unlogical to use it there anyway)
string GetNewestBic(object oPC);

////////////////////////////////////////////////////////////////////////////////
//                          function definition                               //
////////////////////////////////////////////////////////////////////////////////

int GetSystemTime()
{
SetLocalString(GetModule(),"NWNX!FILES!GETSYSTEMTIME","..........");
string s = GetLocalString(GetModule(),"NWNX!FILES!GETSYSTEMTIME");
return StringToInt(s);
}

string GetNewestBic(object oPC)
{
string path = "servervault/"+GetPCPlayerName(oPC);
string filter = "*.bic";
string newest, file = FileGetFirst(path,filter);
int time,highest;
 while(file != "")
 {
 time = FileTime(file,path,FILE_TIME_OF_WRITE);
  if(time > highest)
  {
  highest = time;
  newest = file;
  }
 file = FileGetNext(path,filter);
 }
return newest;
}

int FileExists(string filename, string path="")
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!EXISTS",path+filename);
path = GetLocalString(o,"NWNX!FILES!EXISTS");
DeleteLocalString(o,"NWNX!FILES!EXISTS");
return StringToInt(path);
}

int FileDeleteFile(string filename, string path="")
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!DELETE",path+filename);
path = GetLocalString(o,"NWNX!FILES!DELETE");
DeleteLocalString(o,"NWNX!FILES!DELETE");
return StringToInt(path);
}

int FileDeleteDirectory(string dirname, string path="")
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!DDELETE",path+dirname);
path = GetLocalString(o,"NWNX!FILES!DDELETE");
DeleteLocalString(o,"NWNX!FILES!DDELETE");
return StringToInt(path);
}

string FileMD5(string filename, string path="")
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!MD5",path+filename+"|.........................................................");
path = GetLocalString(o,"NWNX!FILES!MD5");
DeleteLocalString(o,"NWNX!FILES!MD5");
return path;
}

int FileClose()
{
object o = GetModule();
SetLocalString(o,"NWNX!FILES!CLOSE",".");
string s = GetLocalString(o,"NWNX!FILES!CLOSE");
DeleteLocalString(o,"NWNX!FILES!CLOSE");
return StringToInt(s);
}

int FileWrite(string sString)
{
object o = GetModule();
SetLocalString(o,"NWNX!FILES!WRITE",sString);
sString = GetLocalString(o,"NWNX!FILES!WRITE");
DeleteLocalString(o,"NWNX!FILES!WRITE");
return StringToInt(sString);
}

int FileSize(string filename, string path="")
{
 if(GetStringLength(path)>0 && GetStringRight(path,1)!="/")
 {
 path+="/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!SIZE",path+filename+"|............................");
path = GetLocalString(o,"NWNX!FILES!SIZE");
DeleteLocalString(o,"NWNX!FILES!SIZE");
return StringToInt(path);
}

int FileOpen(string filename, string path="", int nMode = FILE_OPEN_MODE_FOR_READING)
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!OPEN",path+filename+"|"+(nMode&FILE_OPEN_MODE_FOR_READING ? "r" : "")+(nMode&FILE_OPEN_MODE_FOR_WRITING ? "w" : ""));
path = GetLocalString(o,"NWNX!FILES!OPEN");
DeleteLocalString(o,"NWNX!FILES!OPEN");
return StringToInt(path);
}

int FileCopy(string filename, string path, string newpath, string newname="", int rewrite_if_exists=FALSE)
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
 if(GetStringLength(newpath) > 0 && GetStringRight(newpath,1) != "/")
 {
 newpath+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!COPY",path+filename+"|"+newpath+(newname == "" ? filename : newname)+"|"+IntToString(rewrite_if_exists));
path = GetLocalString(o,"NWNX!FILES!COPY");
DeleteLocalString(o,"NWNX!FILES!COPY");
return StringToInt(path);
}

string FileRead()
{
object o = GetModule();
SetLocalString(o,"NWNX!FILES!READ","................................................................................................................................................................................................................................................................");
string s = GetLocalString(o,"NWNX!FILES!READ");
DeleteLocalString(o,"NWNX!FILES!READ");
return s;
}

int FileTime(string filename, string path="", int nTime=FILE_TIME_OF_WRITE)
{
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
object o = GetModule();
SetLocalString(o,"NWNX!FILES!TIME",IntToString(nTime)+"|"+path+filename+"|......................................");
path = GetLocalString(o,"NWNX!FILES!TIME");
DeleteLocalString(o,"NWNX!FILES!TIME");
return StringToInt(path);
}

int FileCreateFile(string filename, string path="")
{
object o = GetModule();
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
SetLocalString(o,"NWNX!FILES!CREATE",path+filename);
string s = GetLocalString(o,"NWNX!FILES!CREATE");
DeleteLocalString(o,"NWNX!FILES!CREATE");
return StringToInt(s);
}

int FileRenameFile(string filename, string path, string newname)
{
object o = GetModule();
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
SetLocalString(o,"NWNX!FILES!RENAME",path+filename+"|"+path+newname);
string s = GetLocalString(o,"NWNX!FILES!RENAME");
DeleteLocalString(o,"NWNX!FILES!RENAME");
return StringToInt(s);
}

int FileCreateDirectory(string dirname, string path="")
{
object o = GetModule();
 if(GetStringLength(path)>0 && GetStringRight(path,1)!="/")
 {
 path+="/";
 }
SetLocalString(o,"NWNX!FILES!DCREATE",path+dirname);
string s = GetLocalString(o,"NWNX!FILES!DCREATE");
DeleteLocalString(o,"NWNX!FILES!DCREATE");
return StringToInt(s);
}

string FileGetFirst(string path="", string filter="*", int mode=FILE_SEARCH_FOR_FILES)
{
object o = GetModule();
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
SetLocalString(o,"NWNX!FILES!FIRST",path+filter+"|"+IntToString(mode)+"|..................................................");
path = GetLocalString(o,"NWNX!FILES!FIRST");
DeleteLocalString(o,"NWNX!FILES!FIRST");
return path;
}

string FileGetNext(string path="", string filter="*", int mode=FILE_SEARCH_FOR_FILES)
{
object o = GetModule();
 if(GetStringLength(path) > 0 && GetStringRight(path,1) != "/")
 {
 path+= "/";
 }
SetLocalString(o,"NWNX!FILES!NEXT",path+filter+"|"+IntToString(mode)+"|..................................................");
path = GetLocalString(o,"NWNX!FILES!NEXT");
DeleteLocalString(o,"NWNX!FILES!NEXT");
return path;
}


