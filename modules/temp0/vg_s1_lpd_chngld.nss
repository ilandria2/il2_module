// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialogs (LPD) Dialog script for changing Gold    |
// | File    || vg_s1_lpd_chngld.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Zmeni hracovi zlato o zadanej hodnote
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   LPD_GetPC();
    string  sCount  =   LPD_GetSpokenText(FALSE, FALSE);
    int     nCount  =   stoi(sCount);

    ActionGold(nCount, oPC, SYS_M_INCREMENT);
}
