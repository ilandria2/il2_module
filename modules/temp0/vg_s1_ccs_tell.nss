// +---------++----------------------------------------------------------------+
// | Name    || Console Command System (CCS) Tell script                       |
// | File    || vg_s1_ccs_tell.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Posle TELL zpravu vybranemu cily
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_c"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ccs_const"
#include    "nwnx_chat"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    int     nSpell  =   GetSpellId();

    if  (nSpell ==  SPELL_TIS_TARGET)
    {
        object  oTarget =   GetSpellTargetObject();

        if  (oPC    ==  oTarget)
        {
            SYS_Info(oPC, SYSTEM_CCS_PREFIX, "TARGET_UNSET");
            sls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "", -1, SYS_M_DELETE);
            return;
        }

        if  (!GetIsPC(oTarget)
        ||  GetDistanceBetween(oPC, oTarget)    >   32.0f)
        {
            SYS_Info(oPC, SYSTEM_CCS_PREFIX, "INVALID_TELL");
            return;
        }

        string  sMessage;

        sMessage    =   GetNthSubString(gls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", -1, SYS_M_DELETE), 2);
        sMessage    =   ConvertTokens(sMessage, oPC, FALSE, TRUE, FALSE, FALSE);

        msg(oPC, Y2 + "[PM] " + sMessage, TRUE, FALSE);
        msg(oTarget, G2 + "[PM] " + sMessage, TRUE, FALSE);
        logentry("[" + SYSTEM_CCS_PREFIX + "] [PM] od [" + GetPCPlayerName(oPC) + "] pro [" + GetPCPlayerName(oTarget) + "]: " + sMessage, TRUE);
    }
}

