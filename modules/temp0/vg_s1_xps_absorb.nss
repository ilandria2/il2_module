// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Absorb XP script                       |
// | File    || vg_s1_xps_absorb.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 07-06-2009                                                     |
// | Updated || 07-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kdyz hrac neprovede agresivni interakci na zvyseni zkuenosti na vstrebani
    urcitou dobu, pak se zacnou zkusenosti na vstrebani vstrebavat v pravidelne
    sekvenci
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    EnterAbsorbMode(object oPC)
{
    int bAbsorb =   gli(oPC, SYSTEM_XPS_PREFIX + "_ABSORB_MODE");

    //  Vstoupeni do vstrebavaciho modu
    if  (!bAbsorb)
    {
        sli(oPC, SYSTEM_XPS_PREFIX + "_ABSORB_MODE", TRUE);

        if  (XPS_B_ENABLE_ABSORB_MODE)
        SYS_Info(oPC, SYSTEM_XPS_PREFIX, "ENTERING_ABSORB_MODE");
    }

    XPS_AbsorbModeSequence(oPC);
}



void    main()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    object  oXPS    =   GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT");
    int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");
    int     nLast   =   gli(oXPS, SYSTEM_XPS_PREFIX + "_SEQ_HB");

    if  (nHBs - nLast   <   XPS_M_ABSORB_HEARTBEATS)    return;

    object  oPC =   GetFirstPC();
    float   fDelay;

    while   (GetIsObjectValid(oPC))
    {
        if  (!GetIsDM(oPC))
        {
            /*string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

            if  (sArea  !=  "S"
            &&  sArea   !=  "X")
            {*/
                nLast   =   gli(oPC, SYSTEM_XPS_PREFIX + "_SEQ_HB");

                if  (nHBs - nLast   >=  XPS_M_ABSORB_SEQUENCES)
                {
                    fDelay  +=  ran(0.0f, 5.0f, 2);

                    sli(oPC, SYSTEM_XPS_PREFIX + "_SEQ_HB", nHBs);
                    DelayCommand(fDelay, EnterAbsorbMode(oPC));
                }
            //}
        }

        oPC =   GetNextPC();
    }

    sli(oSystem, SYSTEM_XPS_PREFIX + "_SEQ_HB", nHBs);
}
