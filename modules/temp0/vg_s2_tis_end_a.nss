// Text Interaction System (TIS) - End menu - abort script

#include "vg_s0_tis_core_c"

void main()
{
    object oPC = OBJECT_SELF;
    object user = TIS_GetUser(oPC);

    TIS_ClearActionList(oPC);
    TIS_ClearObjectList(oPC);

    if (GetIsObjectValid(user))
        TIS_SetUser(user, OBJECT_INVALID);
    TIS_SetUser(oPC, OBJECT_INVALID);

    sli(oPC, TIS_ + "CHECK_RESULT", -1, -1, SYS_M_DELETE);  //  temporary condition check result
    sll(oPC, TIS_ + "SEQLOC", li(), -1, SYS_M_DELETE);      //  temporary location at the beginning of an interaction
}
