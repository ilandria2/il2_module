// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_time.nss                                                   |
// | Author  || VirgiL                                                         |
// | Created || 29-12-2009                                                     |
// | Updated || 29-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "TIME"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "TIME";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nHour   =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     nMinute =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    int     nDay    =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    int     nMonth  =   CCS_GetConvertedInteger(sLine, 4, FALSE);
    int     nYear   =   CCS_GetConvertedInteger(sLine, 5, FALSE);
    string  sMessage;

    nHour   =   (nHour      <   -1) ?   0       :   (nHour      >   23)     ?   23      :   nHour;
    nMinute =   (nMinute    <   -1) ?   0       :   (nMinute    >   59)     ?   59      :   nMinute;
    nDay    =   (nDay       <   -1) ?   1       :   (nDay       >   28)     ?   28      :   nDay;
    nMonth  =   (nMonth     <   -1) ?   1       :   (nMonth     >   12)     ?   12      :   nMonth;
    nYear   =   (nYear      <   -1) ?   1200    :   (nYear      >   1500)   ?   1500    :   nYear;

    int nCurHour    =   GetTimeHour();
    int nCurMinute  =   GetTimeMinute();
    int nCurDay     =   GetCalendarDay();
    int nCurMonth   =   GetCalendarMonth();
    int nCurYear    =   GetCalendarYear();

    if  (nHour  ==  -1
    &&  nMinute ==  -1
    &&  nDay    ==  -1
    &&  nMonth  ==  -1
    &&  nYear   ==  -1)
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Current date is [" + Y2 + itos(nCurDay) + "-" + itos(nCurMonth) + "-" + itos(nCurYear) + " " + itos(nCurHour) + ":" + itos(nCurMinute) + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int nNewHour    =   (nHour      ==  -1) ?   nCurHour    :   nHour;
    int nNewMinute  =   (nMinute    ==  -1) ?   nCurMinute  :   nMinute;
    int nNewDay     =   (nDay       ==  -1) ?   nCurDay     :   nDay;
    int nNewMonth   =   (nMonth     ==  -1) ?   nCurMonth   :   nMonth;
    int nNewYear    =   (nYear      ==  -1) ?   nCurYear    :   nYear;

    //  navrat do minulosti nefunguje
    if  (DateToHours(nNewYear, nNewMonth, nNewDay, nNewHour)
    <   DateToHours(nCurYear, nCurMonth, nCurDay, nCurHour))
    {
        nNewDay +=  1;
        sMessage    =   Y1 + "( ? ) " + W2 + "Date was additionally modified by " + Y1 + "+1 day" + W2 + " (Because you've attempted to change the date to the past)";

        msg(oPC, sMessage, FALSE, FALSE);
        //return;
    }

    SetCalendar(nNewYear, nNewMonth, nNewDay);
    SetTime(nNewHour, nNewMinute, GetTimeSecond(), GetTimeMillisecond());

    int     nAftHour    =   GetTimeHour();
    int     nAftMinute  =   GetTimeMinute();
    int     nAftDay     =   GetCalendarDay();
    int     nAftMonth   =   GetCalendarMonth();
    int     nAftYear    =   GetCalendarYear();
    string  sAftHour    =   (nAftHour   <   10) ?   "0" + itos(nAftHour)    :   itos(nAftHour);
    string  sAftMinute  =   (nAftMinute <   10) ?   "0" + itos(nAftMinute)  :   itos(nAftMinute);
    string  sAftDay     =   (nAftDay    <   10) ?   "0" + itos(nAftDay)     :   itos(nAftDay);
    string  sAftMonth   =   (nAftMonth  <   10) ?   "0" + itos(nAftMonth)   :   itos(nAftMonth);
    string  sAftYear    =   (nAftYear   <   10) ?   "0" + itos(nAftYear)    :   itos(nAftYear);

    sMessage    =   Y1 + "( ? ) " + W2 + "The time was changed:" +
                    "\nfrom [" + Y2 + itos(nCurDay) + "-" + itos(nCurMonth) + "-" + itos(nCurYear) + " " + itos(nCurHour) + ":" + itos(nCurMinute) + W2 + "]" +
                    "\nto [" + G2 + itos(nAftDay) + "-" + itos(nAftMonth) + "-" + itos(nAftYear) + " " + itos(nAftHour) + ":" + itos(nAftMinute) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
