#include "aps_include"
#include "vg_s0_ets_const"
#include "vg_s0_sys_core_o"
#include "vg_s0_acs_core_c"

void FetchResultsAsync(int playerId, int count = 0);

// ETS data load script
void main()
{
    int playerId = gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

    string query =
        "select store_item_id, item_oid, item_tag, item_tech, item_stack, item_weight, item_dbcur, item_dbmax " +
        "from ets_store_items " +
        "where player_id = " + itos(playerId);

    SQLExecDirect(query);
    FetchResultsAsync(playerId);
}

void FetchResultsAsync(int playerId, int count = 0)
{
    if (gli(OBJECT_SELF, "etscart")) SpawnScriptDebugger();

    if (SQLFetch() == SQL_SUCCESS)
    {
        int store_item_id = stoi(SQLGetData(1));
        int item_oid = stoi(SQLGetData(2));
        string item_tag = SQLGetData(3);
        string item_tech = SQLGetData(4);
        int item_stack = stoi(SQLGetData(5));
        int item_weight = stoi(SQLGetData(6));
        int item_dbcur = stoi(SQLGetData(7));
        int item_dbmax = stoi(SQLGetData(8));
        string item_resref = left(item_tag, 16);
        int item_recipe = GetTechPart(item_tech, TECH_PART_RECIPE_ID);
        int item_type = ResRefToBaseItemType(item_resref);
        int isWeighted = IsWeightBasedItemType(item_type);
        int amount = !isWeighted ? item_stack : ACS_GetItemStackSize(item_weight, item_resref);
        object item = ACS_CreateItemUsingRecipe(item_recipe, amount, li(), OBJECT_SELF, FALSE);

        if (!GetIsObjectValid(item))
            logentry("[" + ETS + "] Unable to load cart item object id " + itos(item_oid) + " [" + item_tag + "] for player [" + itos(playerId) + "]");
        else
        {
            IDS_UpdateItem(OBJECT_INVALID, item, item_dbcur, item_dbmax, FALSE);
            SetTechPart(item, TECH_PART_OBJECT_ID, item_oid);
            sli(OBJECT_SELF, ETS_ + "DELETE_STORE_ITEM", store_item_id, -2);

            count++;
        }
    }

    else
    {
        int takenItems = gli(OBJECT_SELF, ETS_ + "DELETE_STORE_ITEM_I_ROWS");
        int row;
        string itemsToDelete;
        for (row = 1; row <= takenItems; row++)
        {
            int itemId = gli(OBJECT_SELF, ETS_ + "DELETE_STORE_ITEM", row, SYS_M_DELETE);

            if (row > 1)
                itemsToDelete += ",";
            itemsToDelete += itos(itemId);
        }

        SQLExecDirect("DELETE FROM ets_store_items WHERE store_item_id IN (" + itemsToDelete + ")");
        logentry("[" + ETS + "] " + itos(count) + " cart items loaded for player " + itos(playerId));
        sli(OBJECT_SELF, ETS_ + "LOADING_DATA", FALSE);
        return;
    }

    sli(OBJECT_SELF, ETS_ + "LOADING_DATA", TRUE);
    DelayCommand(0.001f, FetchResultsAsync(playerId, count));
}
