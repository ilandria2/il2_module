// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Item equip script                       |
// | File    || vg_s1_sys_itmequ.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Zablokuje equipnuti stacknutych predmetu do leve ruky
    Nastavi promennou "equipped slot"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oItem   =   GetPCItemLastEquipped();
    object  oPC     =   GetPCItemLastEquippedBy();

    //  nastaveni promenne slotu
    int     n       =   0;
    object  oTemp   =   GetItemInSlot(n, oPC);

    while   (n  <=  NUM_INVENTORY_SLOTS)
    {
        if  (oItem  ==  oTemp)  break;

        oTemp   =   GetItemInSlot(++n, oPC);
    }

    //  slot = leva ruka
    if  (n  ==  INVENTORY_SLOT_LEFTHAND)
    {
        //  equipnuti stacknutyho predmetu
        if  (GetItemStackSize(oItem)    >   1)
        {
            SYS_Info(oPC, SYSTEM_SYS_PREFIX, "SPLIT_&" + GetName(oItem) + "&");
            DelayCommand(0.2f, AssignCommand(oPC, ClearAllActions(TRUE)));
            DelayCommand(0.3f, AssignCommand(oPC, ActionUnequipItem(oItem)));
            return;
        }
    }

    sli(oItem, SYSTEM_SYS_PREFIX + "_INV_SLOT", 100 + n);
}

