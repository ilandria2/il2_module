// +---------++----------------------------------------------------------------+
// | Name    || Generic User Interface (GUI) System init                       |
// | File    || vg_s1_gui_init.nss                                             |
// +---------++----------------------------------------------------------------+

#include "vg_s0_gui_core"
#include "vg_s0_sys_core_d"
#include "vg_s0_oes_const"

void main()
{
    string sName       = SYSTEM_GUI_NAME;
    string sVersion    = SYSTEM_GUI_VERSION;
    string sAuthor     = SYSTEM_GUI_AUTHOR;
    string sDate       = SYSTEM_GUI_DATE;
    string sUpDate     = SYSTEM_GUI_UPDATE;

    sli(__GUI(), "SYSTEM" + _GUI_ + "STATE", 2);
    logentry("[" + GUI + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", GUI, -2);
    sli(__GUI(), "SYSTEM" + _GUI_ + "ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(__GUI(), "SYSTEM" + _GUI_ + "NAME", sName);
    sls(__GUI(), "SYSTEM" + _GUI_ + "PREFIX", GUI);
    sls(__GUI(), "SYSTEM" + _GUI_ + "VERSION", sVersion);
    sls(__GUI(), "SYSTEM" + _GUI_ + "AUTHOR", sAuthor);
    sls(__GUI(), "SYSTEM" + _GUI_ + "DATE", sDate);
    sls(__GUI(), "SYSTEM" + _GUI_ + "UPDATE", sUpDate);

    sls(__GUI(), GUI_ + "NCS_SYSINFO_RESREF", "vg_s1_gui_info");
//    sls(__GUI(), GUI_ + "NCS_DB_CHECK_RESREF", "vg_s1_gui_data_c");
//    sls(__GUI(), GUI_ + "NCS_DB_SAVE_RESREF", "vg_s1_gui_data_s");
//    sls(__GUI(), GUI_ + "NCS_DB_LOAD_RESREF", "vg_s1_gui_data_l");
//    sli(__GUI(), GUI_ + "DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(__GUI(), GUI_ + "DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(__GUI(), GUI_ + "DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(__GUI(), GUI_ + "DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(GUI);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_*) + "_SCRIPT", "<event_script>", -2);

    ////////////// 1. init gui layouts
    // load list of guis
    sls(__GUI(), GUI_ + "TYPE", "INTERACT", -2);
    //sls(GUI_ + "TYPE", "READ_BOOK", -2);
    //sls(GUI_ + "TYPE", "VIEW_SHELF", -2);
    //sls(GUI_ + "TYPE", "PICK_POCKET", -2);
    //sls(GUI_ + "TYPE", "PICK_LOCK", -2);
    //sls(GUI_ + "TYPE", "BREW_POTION", -2);
    //sls(GUI_ + "TYPE", "COOK_MEAL", -2);
    //sls(GUI_ + "TYPE", "TEND_WOUNDS", -2);
    //sls(GUI_ + "TYPE", "START_FIRE", -2);
    //sls(GUI_ + "TYPE", "DISARM_TRAP", -2);
    //sls(GUI_ + "TYPE", "HAGGLE", -2);

    // init each gui listed above
    int countGuiType = gli(__GUI(), GUI_ + "TYPE_S_ROWS");
    int row = 0;
    for (row = 1; row <= countGuiType; row++)
    {
        vector vCreate = Vector(1.0f + row, 2.0f, 1.0f);
        location lCreate = Location(GetObjectByTag(SYS_TAG_SYSTEMS_AREA), vCreate, 0.0f);
        object gui = CreateObject(OBJECT_TYPE_WAYPOINT, GUI_OBJECT_RESREF, lCreate, FALSE, GUI_ + itos(row));
        string guiType = GUI_GetType(row);

        GUI_SetTypeId(guiType, row);
        logentry("[" + GUI + "] Initializing " + itos(row) + "(" + guiType + ")");
        ExecuteScript(GUI_ + "def_" + guiType, OBJECT_SELF);
    }

    sli(__GUI(), "SYSTEM" + _GUI_ + "STATE", TRUE);
    logentry("[" + GUI + "] init done", TRUE);
}

