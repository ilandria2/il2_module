// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Modify player character script          |
// | File    || vg_s1_sys_modchr.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*

*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    //ExecuteScript("vg_s1_sys_creitm", oPC);
}

