/*

1. init all gui types:
- set a list of GUI_TYPE string array (INTERACT, READ_BOOK, etc.)
- for each gui_type string array
- create waypoint GUI_OBJECT_RESREF in systems area with GUI_<GuiTypeId> tag
- exec gui_def_<GuiType>
-------------------------------------------

2. init INTERACT gui type example:
- GUI_LayoutAddObject()
-- GUI_SetLayoutObjectWidth();
-- GUI_SetLayoutObjectHeight();
-- GUI_SetLayoutObjectPosX();
-- GUI_SetLayoutObjectPosY();
-- GUI_SetLayoutObjectLayer();
-- GUI_SetLayoutObjectVfxId();
-- GUI_SetLayoutObjectHasLayout();

- GUI_LayoutAddGridObjects()
-- repeats the above for nColumn * nRow objects representing a grid
-------------------------------------------


3. on menu start called via (eg. GUI_StartMenu(oPlayer, "INTERACT"); )
- from old to new:
-- GUI_StopMenu()
-- GUI_SetMenuName()
-- GUI_RenderMenuSequence(oPlayer, menu)

- from old to old:
-- GUI_RenderMenuSequence(oPlayer, menu, -1)
-------------------------------------------


4. on GUI_RenderMenuSequence(player, menu, id)
- if GUI_GetMenuName(oPlayer) == ""
-- exit // manually/force stopped

- seqId = GUI_GetSequence(oPlayer)
- if (seqId > id && id != -1)
-- exit // other sequence was triggered before loop manually and this sequence becomes obsolete

- GUI_SetSequence(oPlayer, seqId);

- if id == -1
-- baseLayer = GUI_GetMaxLayerId(GUI_GetDefinitionObject(GUI_GetTypeId(menu))) + 1;
-- id = GUI_GetSequence(oPlayer);
- else
-- baseLayer = GUI_GetMaxLayerId(pc) + 1;

- GUI_SetMaxLayerId(pc, baseLayer);

-   // get list of objects defined by menu
--      // first determine whether there is a cached or default VFX id associated to the guiObject
--      // no cached VFX id
----        // no defautl vfx id - skip rendering this object
--      // render object by VFX id
--      // object doesn't have clickable layout thus no display name
--      // get existing layout object
----        // create layout object (clickable placeable)
--      // get and set display name for the layout object

-------------------------------------------


5. on GUI_StopMenu()
-   // get list of objects defined by menu
--      GUI_DestroyLayoutObject(objects)
--------------------------------------------

*/
