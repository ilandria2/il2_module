// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_revealmap.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 29-12-2009                                                     |
// | Updated || 29-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "REVEALMAP"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "REVEALMAP";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     bExplored   =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     bNotify     =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    object  oPlayer     =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neni to hrac
    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "You must select a player character";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    bExplored   =   (bExplored  ==  TRUE)   ?   TRUE    :   FALSE;
    bNotify     =   (bNotify    ==  TRUE)   ?   TRUE    :   FALSE;

    ExploreAreaForPlayer(GetArea(oPC), oPC, bExplored);

    if  (bExplored)
    sMessage    =   Y1 + "( ? ) " + G2 + "Revealing " + W2 + "this area for player character [" + G2 + GetPCPlayerName(oPlayer) + " / " + GetName(oPlayer) + W2 + "]";

    else
    sMessage    =   Y1 + "( ? ) " + R2 + "Hiding " + W2 + "this area for player character [" + G2 + GetPCPlayerName(oPlayer) + " / " + GetName(oPlayer) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

    if  (bNotify
    &&  oPlayer !=  oPC)
    {
        if  (bExplored)
        sMessage    =   G1 + "( ! ) " + W2 + "For you the map for this area is now " + G2 + "revealed";

        else
        sMessage    =   R1 + "( ! ) " + W2 + "For you the map for this area is now " + R2 + "hidden";

        msg(oPlayer, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
