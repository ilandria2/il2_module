// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_spawn.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SPAWN"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_tis_core_c"
#include    "nwnx_funcs"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    CreateObjectVoid(int nType, string sResRef, location lLoc, int bAppear=FALSE, string sNewTag="")
{
    CreateObject(nType, sResRef, lLoc, bAppear, sNewTag);
}



void    main()
{
    string      sCmd    =   "SPAWN";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nSpawn  =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     nCount  =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    string  sLoc    =   CCS_GetConvertedString(sLine, 3, FALSE);
    location lLoc   =   CCS_GetConvertedLocation(sLine, 3, FALSE);
    string  sMessage;

    nCount  =   (nCount < 1) ? 1 : nCount;

    //  neplatne ID < 100
    if  (nSpawn <   100)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Resref ID must be >= 100";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    //  neplatna lokace
    if  (!GetIsObjectValid(GetAreaFromLocation(lLoc)))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid location";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    /*

    ;spawn 104;             --- spawne objekt s resrefem #104 na vybrane pozici (target)
    ;spawn 104;1;a;         --- spawne objekt s resrefem #104 na nahodne pozici v aktualni oblasti
    ;spawn 104;1;a6;        --- spawne objekt s resrefem #104 na nahodne pozici v oblasti #6
    ;spawn 104;7;a23;       --- spawne objekt s resrefem #104 na nahodne pozici v oblasti #23 (x7)

    //GetGroundHeightFromLocation()

    */

    object  oTIS;
    string  sResRef, sName, sType;
    int     n, nType, bLoc;

    oTIS    =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");
    sType   =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPRF_" + itos(nSpawn) + "_TYPE");
    sResRef =   gls(oTIS, SYSTEM_TIS_PREFIX + "_SPRF_" + itos(nSpawn) + "_RESREF");
    sName   =   gls(oTIS, SYSTEM_TIS_PREFIX + "_RF_" + upper(sResRef) + "_NAME");
    nType   =   (sType  ==  "C") ? OBJECT_TYPE_CREATURE : OBJECT_TYPE_PLACEABLE;
    bLoc    =   TestStringAgainstPattern("A*n|A", upper(sLoc));

    //  spawnuti v targetem-urcene pozici
    if  (!bLoc)
    {
        object oCreate = CreateObject(nType, sResRef, lLoc);

        if  (GetIsObjectValid(oCreate))
        sMessage    =   G1 + "( ! ) " + W2 + "Spawning resref " + G2 + itos(nSpawn) + "-" + sResRef + " (" + sName + ")" + W2 + " at selected location:" + G1 + " Success";

        else
        sMessage    =   R1 + "( ! ) " + W2 + "Spawning resref " + R2 + itos(nSpawn) + "-" + sResRef + " (" + sName + ")" + W2 + " at selected location:" + G1 + " Failure";

        msg(oPC, sMessage, TRUE, FALSE);
    }

    //  spawnuti ve vybrane oblasti na nahodne pozici
    else
    {
        sMessage    =   G1 + "( ! ) " + W2 + "Spawning " + G2 + itos(nCount) + "x" + W2 + " resref " + G2 + itos(nSpawn) + "-" + sResRef + " (" + sName + ")" + W2 + " in area [" + G2 + GetTag(GetAreaFromLocation(lLoc)) + " (" + GetName(GetAreaFromLocation(lLoc)) + G2 + ")]" + W2 + " at random location...";

        msg(oPC, sMessage, TRUE, FALSE);

        //  zacykleni nCount-krat
        while   (++n    <=  nCount)
            DelayCommand(n * 0.1f, CreateObjectVoid(nType, sResRef, lLoc));

        //sMessage    =   G1 + "( ! ) " + W2 + "Dokon�en spawn " + G2 + itos(nCount) + "x" + W2 + " resrefu " + G2 + itos(nSpawn) + "-" + sResRef + " (" + sName + ")" + W2 + " v oblasti [" + G2 + GetTag(GetAreaFromLocation(lLoc)) + " (" + GetName(GetAreaFromLocation(lLoc)) + G2 + ")]" + W2 + " na n�hodn� pozici";

        //DelayCommand((n - 1) * 0.1f, msg(oPC, sMessage, TRUE, FALSE));
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

