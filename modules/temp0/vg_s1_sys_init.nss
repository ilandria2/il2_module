// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) System init                             |
// | File    || vg_s1_sys_init.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 12-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system SYS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_m"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_oes_const"
#include    "x2_inc_switches"
#include    "aps_include"
#include    "nwnx_funcs"
#include    "nwnx_chat"
#include "nwnx_files"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+


void    CreateObjectVoid(int nObjectType, string sTemplate, location lLocation, int bUseAppearAnimation=FALSE, string sNewTag="")
{
    CreateObject(nObjectType, sTemplate, lLocation, bUseAppearAnimation, sNewTag);
}



void    LightSources(string sSource, string sLight, vector vOff, float fForward)
{
    object  oSource;
    vector  vPos;
    int     n;

    oSource =   GetObjectByTag(sSource, n = 0);

    while   (GetIsObjectValid(oSource))
    {
        vPos    =   GetPosition(oSource);
        vPos    =   vPos + vOff;
        vPos    =   GetAwayVector(vPos, fForward, GetFacing(oSource)-180.0f);

        AssignCommand(OBJECT_SELF, CreateObjectVoid(OBJECT_TYPE_PLACEABLE, sLight, Location(GetArea(oSource), vPos, ran(0.0f, 360.0f, 0))));

        oSource     =   GetObjectByTag(sSource, ++n);
    }
}

void Load2daTables();

void    main()
{
    string  sSystem     =   SYSTEM_SYS_PREFIX;
    string  sName       =   SYSTEM_SYS_NAME;
    string  sVersion    =   SYSTEM_SYS_VERSION;
    string  sAuthor     =   SYSTEM_SYS_AUTHOR;
    string  sDate       =   SYSTEM_SYS_DATE;
    string  sUpDate     =   SYSTEM_SYS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "NWNX!ODBC!FETCH", sSystem);

    int bSQL, bOS;

    if  (gls(GetModule(), "NWNX!ODBC!FETCH")    !=  sSystem)
    {
        //SpawnScriptDebugger();
        SQLExecDirect("SHOW VARIABLES WHERE Variable_name = 'version_compile_os'");
        SQLFetch();

        string sData   =   SQLGetData(2);
        bOS     =   (TestStringAgainstPattern("**WIN**", upper(sData)))    ?   SQL_COMPILE_VERSION_WINDOWS :   SQL_COMPILE_VERSION_UNIX;
        bSQL    =   TRUE;
    }

    sli(oSystem, sSystem + "_OS", bOS);
    sli(oSystem, sSystem + "_SQL", bSQL);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(oSystem, "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_sys_info");
    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_sys_data_c");
    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_sys_data_s");
    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_sys_data_l");
    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
        CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_sys_time", -2);
    //sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACQUIRE_ITEM) + "_SCRIPT", "vg_s1_sys_creitm", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_UNACQUIRE_ITEM) + "_SCRIPT", "vg_s1_sys_delsto", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CLIENT_ENTER) + "_SCRIPT", "vg_s1_sys_client", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_EQUIP_ITEM) + "_SCRIPT", "vg_s1_sys_itmequ", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_UNEQUIP_ITEM) + "_SCRIPT", "vg_s1_sys_itmune", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_sys_modchr", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_USER_DEFINED) + "_SCRIPT", "vg_s1_sys_runmod", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_SPAWN) + "_SCRIPT", "vg_s1_sys_npcnam", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_A_ON_ENTER) + "_SCRIPT", "vg_s1_sys_areent", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CLIENT_ENTER) + "_SCRIPT", "chat_enter", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_CLIENT_LEAVE) + "_SCRIPT", "chat_exit", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_DEATH) + "_SCRIPT", "vg_s1_sys_death", -2);

//  ----------------------------------------------------------------------------
//  Inicializacni kod

    sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_HOURS", DateToHours(GetCalendarYear(), GetCalendarMonth(), GetCalendarDay(), GetTimeHour()));

    //  nacteni posledniho PlayerID z db do lokalni promenne pro algorytmus
    //  newPlayerID = lastPlayerID + 1
    if  (bSQL)
    {
        SQLExecDirect("SELECT MAX(PlayerID) FROM sys_players");

        //  uspech
        if  (SQLFetch() ==  SQL_SUCCESS)
        {
            int nLastID =   stoi(SQLGetData(1)); //+ SYS_PLAYER_ID_BASE; // removed due to MAX(col) in sql

            sli(oSystem, SYSTEM_SYS_PREFIX + "_PLAYER_ID_LAST", nLastID);
            logentry("[" + SYSTEM_SYS_PREFIX + "] setting up last PlayerID SUCCESS [" + itos(nLastID) + "]", TRUE);
        }

        //  nejaka chyba (napr. prazdna tabulka)
        else
        {
            logentry("[" + SYSTEM_SYS_PREFIX + "] setting up last PlayerID FAILURE", TRUE);
        }

        LoadPersistentData(OBJECT_SELF, SYSTEM_SYS_PREFIX, "SETTINGS");
    }

    NWNXChat_Init();
    //NWNXFuncs_Init();

    //  Vlastni mody:

    //  Mod chuze (default)
    sli(oSystem, SYSTEM_SYS_PREFIX + "_MODE_ID", SYS_MODE_WALK, -1);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_SCRIPT", "walk", SYS_MODE_WALK);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_NAME", "walking", SYS_MODE_WALK);

    //  Mod behu
    sli(oSystem, SYSTEM_SYS_PREFIX + "_MODE_ID", SYS_MODE_RUN, SPELL_SYS_MODE_RUN);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_SCRIPT", "run", SYS_MODE_RUN);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_NAME", "running", SYS_MODE_RUN);
    sls(oSystem, SYSTEM_SYS_PREFIX + "_MODE_NAME2", "sprinting", SYS_MODE_RUN);

    //  Osvetleni
    LightSources("VG_P_VE_TORCH_01", "vg_p_ve_firem_01", Vector(0.0f, 0.0f, 3.12f), 0.15f);

    //  Seznam oblasti

    int a;
    object oArea;
    if (bOS == SQL_COMPILE_VERSION_UNIX)
    {
        oArea   =   GetFirstArea();

        while   (GetIsObjectValid(oArea))
        {
            slo(oSystem, SYSTEM_SYS_PREFIX + "_AREA", oArea, -2);
            sli(oArea, SYSTEM_SYS_PREFIX + "_AREA", ++a);
            logentry("[" + SYSTEM_SYS_PREFIX + "] adding area: " + GetTag(oArea));

            oArea   =   GetNextArea();
        }
    }
    else
    {
        string file = FileGetFirst("./modules/temp0", "*.git");
        while (file != "")
        {
            string areaTag = upper(left(file, len(file) - 4));
            logentry("[" + SYSTEM_SYS_PREFIX + "] adding area: " + areaTag);
            oArea = GetObjectByTag(areaTag);
            slo(oSystem, SYSTEM_SYS_PREFIX + "_AREA", oArea, -2);
            sli(oArea, SYSTEM_SYS_PREFIX + "_AREA", ++a);
            file = FileGetNext("./modules/temp0", "*.git");
        }
    }

    logentry("[" + SYSTEM_SYS_PREFIX + "] areas: " + itos(a));

    // 2da tables
    Load2daTables();


//  Inicializacni kod
//  ----------------------------------------------------------------------------

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}



void Load2daTables()
{
}

