// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_<cmd>.nss                                                  |
// | Author  || <AUTHOR>                                                       |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "DREFIND"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "DREFIND";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    object  oObj1   =   CCS_GetConvertedObject(sLine, 1, FALSE);
    object  oObj2   =   CCS_GetConvertedObject(sLine, 2, FALSE);
    int     bAll    =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    string  sMessage;

    PIS_RefreshIndicator(oObj1, oObj2, bAll);

    return;

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

