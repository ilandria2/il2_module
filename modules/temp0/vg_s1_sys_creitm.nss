// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Creature Items setup script             |
// | File    || vg_s1_sys_creitm.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 02-01-2010                                                     |
// | Updated || 02-01-2010                                                     |
// +---------++----------------------------------------------------------------+
/*
    Auto-equipne ziskane vrozene predmety
*/
// -----------------------------------------------------------------------------



#include    "x2_inc_itemprop"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_oes_const"



void    DestroyCreatureItems(object oCreature)
{
    //SpawnScriptDebugger();
    object  oItem   =   GetFirstItemInInventory(oCreature);
    int     nBase;

    while   (GetIsObjectValid(oItem))
    {
        nBase   =   GetBaseItemType(oItem);

        if  (nBase  ==  BASE_ITEM_CREATUREITEM
        ||  nBase   ==  BASE_ITEM_CBLUDGWEAPON
        ||  nBase   ==  BASE_ITEM_CPIERCWEAPON
        ||  nBase   ==  BASE_ITEM_CSLASHWEAPON
        ||  nBase   ==  BASE_ITEM_CSLSHPRCWEAP)
        {
            SetPlotFlag(oItem, FALSE);
            DestroyObject(oItem);
        }

        oItem   =   GetNextItemInInventory(oCreature);
    }
}



void    AddCreatureItems(object oPC)
{
    //SpawnScriptDebugger();
    //SetCutsceneMode(oPC, FALSE, FALSE);
    object          oClawR, oClawL, oSkin, oItem, oBite;
    itemproperty    ipProf, ipAdd;

    ipProf  =   ItemPropertyBonusFeat(IP_CONST_FEAT_WEAPON_PROF_CREATURE);
    ipAdd   =   ItemPropertyOnHitCastSpell(141, 1);

    SetCommandable(TRUE, oPC);
    AssignCommand(oPC, ClearAllActions(TRUE));

    oSkin   =   GetItemInSlot(INVENTORY_SLOT_CARMOUR, oPC);

    //  SKIN
    if  (!GetIsObjectValid(oSkin))
    {
        //oSkin   =   CreateItemOnObject("vg_i_cp_skin0000", oPC, 1);
        oSkin   =   CreateItemOnObject("x2_it_emptyskin", oPC, 1);

        IPSafeAddItemProperty(oSkin, ipProf, 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);
        DelayCommand(0.1f, AssignCommand(oPC, ActionEquipItem(oSkin, INVENTORY_SLOT_CARMOUR)));
    }

    oClawR  =   GetItemInSlot(INVENTORY_SLOT_CWEAPON_R, oPC);

    //  CLAW - RIGHT
    if  (!GetIsObjectValid(oClawR))
    {
        oClawR  =   CreateItemOnObject("vg_i_ct_claw0000", oPC, 1);

        DelayCommand(0.1f, IPSafeAddItemProperty(oClawR, ipAdd, 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE));
        DelayCommand(0.2f, AssignCommand(oPC, ActionEquipItem(oClawR, INVENTORY_SLOT_CWEAPON_R)));
    }

    oClawL  =   GetItemInSlot(INVENTORY_SLOT_CWEAPON_L, oPC);

    //  CLAW - LEFT
    if  (!GetIsObjectValid(oClawL))
    {
        oClawL  =   CreateItemOnObject("vg_i_ct_claw0000", oPC, 1);

        DelayCommand(0.1f, IPSafeAddItemProperty(oClawL, ipAdd, 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE));
        DelayCommand(0.2f, AssignCommand(oPC, ActionEquipItem(oClawL, INVENTORY_SLOT_CWEAPON_L)));
    }

    oBite   =   GetItemInSlot(INVENTORY_SLOT_CWEAPON_B, oPC);

    //  CLAW - BITE
    if  (!GetIsObjectValid(oBite))
    {
        oBite   =   CreateItemOnObject("vg_i_ct_claw0000", oPC, 1);

        DelayCommand(0.1f, IPSafeAddItemProperty(oBite, ipAdd, 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE));
        DelayCommand(0.2f, AssignCommand(oPC, ActionEquipItem(oBite, INVENTORY_SLOT_CWEAPON_B)));
    }

    DelayCommand(0.5f, DestroyCreatureItems(oPC));
    //DelayCommand(0.6f, SetCutsceneMode(oPC, FALSE, TRUE));
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    //DelayCommand(1.5f, AddCreatureItems(oPC));
    AddCreatureItems(OBJECT_SELF);
}
