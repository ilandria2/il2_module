// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Core O                                  |
// | File    || vg_s0_sys_core_o.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 10-05-2009                                                     |
// | Updated || 10-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Knihovna systemovych funkci Ilandrie 2 kategorie "Object"
*/
// -----------------------------------------------------------------------------

#include    "vg_s0_sys_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "x2_inc_itemprop"
#include    "nwnx_structs"

// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+

// Returns CHECK_PARENT_RESULT_* constant indicating the current parent-child
// relationship between childItem and parentItem
int CheckParentObject(object childItem, object parentItem, int bAlsoLog = TRUE);

// Returns the tech description's partType part value
int GetTechPart(string techDesc, int partType, int nth = 0);

// Replaces the tech description's partType part value by newValue
string ReplaceTechPart(string techDesc, int partType, int newValue, int nth = 0);

// Sets the teh description's partType part value to value
void SetTechPart(object oItem, int partType, int value, int nth = 0);

// Gets the item from oInventory with the object id set to objectId
// If oInventory is an item without its own inventory then its possesor is used
object GetItemByObjectId(object oInventory, int objectId);

// Returns the object id of childItem's parent or 0 if does not have one
int GetParentObject(object childItem);

// Binds the two items in a parent-child relationship
void SetParentObject(object childItem, object parentItem);

// Returns the object id of nth child id of the parentItem
int GetNthChildObjectId(object parentItem, int nth = 0);

// Returns the amount of child objects (items) that the oItem item does possess
int GetChildObjectCount(object parentItem);

// Sets a unique persistent id of an object
// If it is an item then also stores it into the item's unidentified description
// If id = -1 then calls NewObjectId() to be used instead.
void SetObjectId(object oObject, int id = -1);

// Gets a unique persistent id of an object
// Returns 0 if not exists
// If being 0 and it is an item then tries to parse it from the item's unidentified description
int GetObjectId(object oObject);

// Gets a new unique object id
int NewObjectId();

// Gets a latest object id that was last time saved
int LastObjectId();

// Calls DestroyObject()
// ** If oObject is item possessed by player then saves its resRef on player before destroying it
void Destroy(object oObject, float fDelay = 0.0f);

// Gets the cost value of the first property of the itemPropType type of the oItem item
// ** returns -1 if not valid item or desired property not found
int GetItemPropertyCostValue(object oItem, int itemPropType);

// Returns the INVENTORY_SLOT_* constant where oItem is currently equipped on
int GetItemEquippedSlot(object oCreature, object oItem);

// Converts inventorySlot constant into constant for bit comparison
int ConvertInventorySlotConstant(int inventorySlot);

// Returns TRUE if the baseItem is equippable on inventorySlot
int IsBaseItemEquippableOn(int baseItem, int inventorySlot);

// Returns INVENTORY_SLOT_* constant that oCreature is able to equip the base item type
int FindSlotForBaseItem(object oCreature, int nType);

// +---------++----------------------------------------------------------------+
// | Name    || ResRefToBaseItemType                                           |
// +---------++----------------------------------------------------------------+
//  Converts item resref to base item type
// -----------------------------------------------------------------------------
int ResRefToBaseItemType(string resRef);

// +---------++----------------------------------------------------------------+
// | Name    || GetBaseItemSize                                                |
// +---------++----------------------------------------------------------------+
//  Gets the size of the base item type (width x height)
// -----------------------------------------------------------------------------
int GetBaseItemSize(int nType);

// Determines whether the itemType is a weight based or quantity based itemType
int IsWeightBasedItemType(int itemType);

// +---------++----------------------------------------------------------------+
// | Name    || IsWeightBasedItem                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//  Determines whether the item is a weight based or quantity based item
// -----------------------------------------------------------------------------
int IsWeightBasedItem(object oItem);

// +---------++----------------------------------------------------------------+
// | Name    || IsSystemItem                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//  Determines whether the item is a system item
//  Returns -1 if oItem is not an item or is OBJECT_INVALID
// -----------------------------------------------------------------------------
int IsSystemItem(object oItem, int nType = -1);

// +---------++----------------------------------------------------------------+
// | Name    || CreateItemOnObjectAndDeleteIfUnable                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//  Creates item on target's inventory and deletes it if it can't posses it
// -----------------------------------------------------------------------------
object CreateItemOnObjectAndDeleteIfUnable(object oPC, string resRef);

// +---------++----------------------------------------------------------------+
// | Name    || li                                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Funkce ktera vraci neplatni lokaci
//
// -----------------------------------------------------------------------------
location    li();

// Returns empty vector
vector vi();



// +---------++----------------------------------------------------------------+
// | Name    || IPS_SetTag                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zmeni TAG objektu oObject (rozlisuje OS)
//
// -----------------------------------------------------------------------------
void    IPS_SetTag(object oObject, string sTag);



/*
// +---------++----------------------------------------------------------------+
// | Name    || IPS_GetItemPrice                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati IL2 cenu predmetu oItem
//
// -----------------------------------------------------------------------------
int     IPS_GetItemPrice(object oItem);
*/



// +---------++----------------------------------------------------------------+
// | Name    || IPS_GetItemWeight                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati vahu predmetu oItem
//
// -----------------------------------------------------------------------------
int     IPS_GetItemWeight(object oItem, int bProp = FALSE);



/*
// +---------++----------------------------------------------------------------+
// | Name    || IPS_SetItemPrice                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nastavi cenu predmetu oItem na hodnotu nPrice [n]
//
// -----------------------------------------------------------------------------
void    IPS_SetItemPrice(object oItem, int nPrice);
*/



// +---------++----------------------------------------------------------------+
// | Name    || IPS_SetItemWeight                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nastavi vahu predmetu oItem na hodnotu nWeight [g]
//
// -----------------------------------------------------------------------------
void    IPS_SetItemWeight(object oItem, int nWeight);



// +---------++----------------------------------------------------------------+
// | Name    || DuplicateCreature                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zduplikuje bytost oTarget  s novou lokaci lLocation s novym tagem sNewTag
//
// -----------------------------------------------------------------------------
object  DuplicateCreature(object oTarget, location lLocation, int bInventory = TRUE, int bEquip = TRUE, int bHeal = TRUE, string sNewTag = "");



// +---------++----------------------------------------------------------------+
// | Name    || GetDefinedObject                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati objekt dle kategorie nDefType a parametru definice sDefinition
//
// -----------------------------------------------------------------------------
object  GetDefinedObject(int nDefType, string sDefinition, object oSource = OBJECT_SELF);



// +---------++----------------------------------------------------------------+
// | Name    || GetItemcount                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Spocita predmety u oSource dle definice nDefType a parametru sDefinition
//
// -----------------------------------------------------------------------------
int     GetItemCount(int nDefType, string sDefinition, int nSlot, object oSource);



// +---------++----------------------------------------------------------------+
// | Name    || CompareItem                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Porovna predmet oItem dle definice nDefType a parametru definice sDefinition
//
// -----------------------------------------------------------------------------
int     CompareItem(int nDefType, string sDefinition, object oItem);



// +---------++----------------------------------------------------------------+
// | Name    || GetItemEquipableInSlot                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE, pokud lze predmet oItem vybavit v slotu nEqSlot v inventari
//
// -----------------------------------------------------------------------------
int     GetItemEquipableInSlot(object oItem, int nEqSlot = EQUIPABLE_SLOT_MAIN_HAND);



/*
// +---------++----------------------------------------------------------------+
// | Name    || GetIsWieldingWeapon                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zjisti, zda bytost oCreature ma vybaveny predmet typu WEAPON_WIELD_*
//
// -----------------------------------------------------------------------------
int     GetIsWieldingWeapon(object oCreature = OBJECT_SELF, int nWeaponWield = WEAPON_WIELD_STANDARD_ONE_HANDED);
*/



// +---------++----------------------------------------------------------------+
// | Name    || GetHasEquippedWeaponType                                       |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud bytost oCreature ma vybaveny predmet s urcitym WeaponType
//
// -----------------------------------------------------------------------------
int     GetHasEquippedWeaponType(object oCreature = OBJECT_SELF, int nWeaponType = WEAPON_TYPE_SLASHING);



// +---------++----------------------------------------------------------------+
// | Name    || GetItemWeaponType                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati typ zbrane oItem
//
// -----------------------------------------------------------------------------
int     GetItemWeaponType(object oItem);



// +---------++----------------------------------------------------------------+
// | Name    || GetItemWeaponSize                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati velikost zbrane oItem
//
// -----------------------------------------------------------------------------
int     GetItemWeaponSize(object oItem);



// +---------++----------------------------------------------------------------+
// | Name    || ItemPropertyDirect                                             |
// | Author  || FunkySwerve                                                    |
// +---------++----------------------------------------------------------------+
//
//  Creates a custom item property with elementary definition
//
// -----------------------------------------------------------------------------
itemproperty    ItemPropertyDirect(int nType, int nSubType, int nCostTable=-1, int nCostValue=-1, int nParamTable=-1, int nParamValue=-1);



// +---------++----------------------------------------------------------------+
// | Name    || CreateObjectOffset                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Ta sama funkce jako CreateObject no s posunutou pozici lLocation o hodnoty
//  X, Y a Z ulozene v LVAR "X", "Y" a "Z"
//
// -----------------------------------------------------------------------------
object  CreateObjectOffset(int nObjectType, string sTemplate, location lLocation, int bUseAppearAnimation=FALSE, string sNewTag="");



// +---------------------------------------------------------------------------+
// |                        I M P L E M E N T A T I O N                        |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                             GetItemByObjectId                             |
// +---------------------------------------------------------------------------+

// helper functions
object _getItemByObjectIdVar(object oInventory, int objectId) { return GetLocalObject(oInventory, SYSTEM_SYS_PREFIX + "_OBJECT_" + itos(objectId)); }
void _deleteItemByObjectIdVar(object oInventory, int objectId) { DeleteLocalObject(oInventory, SYSTEM_SYS_PREFIX + "_OBJECT_" + itos(objectId)); }
void _setItemByObjectIdVar(object oInventory, int objectId, object item) { SetLocalObject(oInventory, SYSTEM_SYS_PREFIX + "_OBJECT_" + itos(objectId), item); }

object GetItemByObjectId(object oInventory, int objectId)
{
    if (GetObjectType(oInventory) == OBJECT_TYPE_ITEM &&
    !GetHasInventory(oInventory))
        oInventory = GetItemPossessor(oInventory);

    if (!GetHasInventory(oInventory)) return OBJECT_INVALID;
    if (objectId <= 0) return OBJECT_INVALID;

    // find item by var reference
    object item = _getItemByObjectIdVar(oInventory, objectId);
    if (GetIsObjectValid(item))
    {
        object owner = GetItemPossessor(item);
        if (owner == oInventory && GetObjectId(item) == objectId) return item;

        _setItemByObjectIdVar(owner, objectId, item);
        _deleteItemByObjectIdVar(oInventory, objectId);
    }

    int id = 0;

    // get item by equip slots
    if (GetObjectType(oInventory) == OBJECT_TYPE_CREATURE)
    {
        int slot = 0;
        for (; slot < NUM_INVENTORY_SLOTS; slot++)
        {
            item = GetItemInSlot(slot, oInventory);
            id = GetObjectId(item);

            if (id == objectId)
            {
                _setItemByObjectIdVar(oInventory, objectId, item);
                return item;
            }
        }
    }

    // get item by inventory slots
    item = GetFirstItemInInventory(oInventory);
    while (GetIsObjectValid(item))
    {
        id = GetObjectId(item);
        if (id == objectId)
        {
            _setItemByObjectIdVar(oInventory, objectId, item);
            return item;
        }

        item = GetNextItemInInventory(oInventory);
    }

    _deleteItemByObjectIdVar(oInventory, objectId);
    return OBJECT_INVALID;
}



// +---------------------------------------------------------------------------+
// |                               CheckParentObject                           |
// +---------------------------------------------------------------------------+

// helper function
int CheckParentObject(object childItem, object parentItem, int bAlsoLog = TRUE)
{
    int childId = GetObjectId(childItem);
    int parentId = GetObjectId(parentItem);
    if (!GetIsObjectValid(parentItem)
    || !GetIsObjectValid(childItem))
    {
        if (bAlsoLog)
            logentry("[" + SYSTEM_SYS_PREFIX + "] unable to create parent-child relationship between objects [" + itos(parentId) + " - " + GetTag(parentItem) + "] and [" + itos(childId) + " - " + GetTag(childItem) + "] - invalid object(s)");
        return CHECK_PARENT_RESULT_INVALID_OBJECT;
    }

    if (GetObjectType(parentItem) != OBJECT_TYPE_ITEM
    || GetObjectType(childItem) != OBJECT_TYPE_ITEM)
    {
        if (bAlsoLog)
            logentry("[" + SYSTEM_SYS_PREFIX + "] unable to create parent-child relationship between objects [" + itos(parentId) + " - " + GetTag(parentItem) + "] and [" + itos(childId) + " - " + GetTag(childItem) + "] - invalid object type(s)");
        return CHECK_PARENT_RESULT_INVALID_OBJECT_TYPE;
    }

    if (!childId || !parentId)
    {
        if (bAlsoLog)
            logentry("[" + SYSTEM_SYS_PREFIX + "] unable to create parent-child relationship between items [" + itos(parentId) + " - " + GetTag(parentItem) + "] and [" + itos(childId) + " - " + GetTag(childItem) + "] - invalid object id(s)");
        return CHECK_PARENT_RESULT_INVALID_OBJECT_ID;
    }

    string descChild = GetDescription(childItem, FALSE, FALSE);
    int childParentId = GetTechPart(descChild, TECH_PART_PARENT_OBJECT_ID);
    if (childParentId)
    {
        if (bAlsoLog)
            logentry("[" + SYSTEM_SYS_PREFIX + "] unable to create parent-child relationship between items [" + itos(parentId) + " - " + GetTag(parentItem) + "] and [" + itos(childId) + " - " + GetTag(childItem) + "] - the child item is already a child of some parent item.");
        return CHECK_PARENT_RESULT_CHILD_HAS_PARENT;
    }

    string descParent = GetDescription(parentItem, FALSE, FALSE);
    int parentParentId = GetTechPart(descParent, TECH_PART_PARENT_OBJECT_ID);
    if (parentParentId == childId)
    {
        if (bAlsoLog)
            logentry("[" + SYSTEM_SYS_PREFIX + "] unable to create parent-child relationship between items [" + itos(parentId) + " - " + GetTag(parentItem) + "] and [" + itos(childId) + " - " + GetTag(childItem) + "] - the child item is already a parent of the parent item.");
        return CHECK_PARENT_RESULT_PARENT_IS_CHILD;
    }

    return CHECK_PARENT_RESULT_IS_VALID;
}



// +---------------------------------------------------------------------------+
// |                               SetParentObject                             |
// +---------------------------------------------------------------------------+

int SetParentObject(object childItem, object parentItem)
{
    if (gli(GetFirstPC(), "SetParent"))SpawnScriptDebugger();

    // invalid state of child or parent item - ignore
    int checkResult = CheckParentObject(childItem, parentItem);
    if (checkResult) return checkResult;

    int parentId = GetObjectId(parentItem);
    int childId = GetObjectId(childItem);
    string techChild = GetDescription(childItem, FALSE, FALSE);
    string techParent = GetDescription(parentItem, FALSE, FALSE);
    int children = GetTechPart(techParent, TECH_PART_CHILDREN_COUNT);
    int childRecipe = GetTechPart(techChild, TECH_PART_RECIPE_ID);
    int parentRecipe = GetTechPart(techParent, TECH_PART_RECIPE_ID);

    SetTechPart(parentItem, TECH_PART_NTH_CHILD_OBJECT_ID, childId, children);
    SetTechPart(parentItem, TECH_PART_NTH_CHILD_RECIPE_ID, childRecipe, children);
    SetTechPart(parentItem, TECH_PART_CHILDREN_COUNT, ++children);
    SetTechPart(childItem, TECH_PART_PARENT_OBJECT_ID, parentId);
    SetTechPart(childItem, TECH_PART_PARENT_RECIPE_ID, parentRecipe);
    //todo: add recipe spec/amount after every recipe id

    ExecuteScript("vg_s1_acs_rsf2nm", parentItem);
    ExecuteScript("vg_s1_acs_rsf2nm", childItem);

    return CHECK_PARENT_RESULT_IS_VALID;
}



// +---------------------------------------------------------------------------+
// |                             GetNthChildObjectId                           |
// +---------------------------------------------------------------------------+

int GetNthChildObjectId(object parentItem, int nth)
{
    return gli(parentItem, SYSTEM_SYS_PREFIX + "_CHILD", nth);
}



// +---------------------------------------------------------------------------+
// |                             GetChildObjectCount                           |
// +---------------------------------------------------------------------------+

int GetChildObjectCount(object parentItem)
{
    return gli(parentItem, SYSTEM_SYS_PREFIX + "_CHILD_I_ROWS");
}



// +---------------------------------------------------------------------------+
// |                              GetParentObjectId                            |
// +---------------------------------------------------------------------------+

int GetParentObjectId(object childItem)
{
    return gli(childItem, SYSTEM_SYS_PREFIX + "_PARENT");
}



// +---------------------------------------------------------------------------+
// |                           TechPart helper functions                       |
// +---------------------------------------------------------------------------+

void InitializeItemFromTech(object oItem)
{
    string techDesc = GetDescription(oItem, FALSE, FALSE);
    int id = GetTechPart(techDesc, TECH_PART_OBJECT_ID);
    int parentId = GetTechPart(techDesc, TECH_PART_PARENT_OBJECT_ID);
    int childCount = GetTechPart(techDesc, TECH_PART_CHILDREN_COUNT);

    sli(oItem, SYSTEM_SYS_PREFIX + "_OBJECT_ID", id == 0 ? NewObjectId() : id);
    sli(oItem, SYSTEM_SYS_PREFIX + "_PARENT", parentId);
    sli(oItem, SYSTEM_SYS_PREFIX + "_CHILD_I_ROWS", -1, -1, SYS_M_DELETE);

    int n, childId;
    for (;n < childCount; n++)
    {
        childId = GetTechPart(techDesc, TECH_PART_NTH_CHILD_OBJECT_ID, n);
        sli(oItem, SYSTEM_SYS_PREFIX + "_CHILD", childId, -2);
    }
}

int GetTechPartDigits(int partType)
{
    int digits = OBJECT_ID_DIGITS;

    switch (partType)
    {
    case TECH_PART_RECIPE_SPEC:
        digits = 10;
        break;

    case TECH_PART_CHILDREN_COUNT:
        digits = 2;
        break;
    }

    return digits;
}

int GetTechPartPosition(int partType, int nth = 0)
{
    int partStart;

    switch (partType)
    {
    case TECH_PART_OBJECT_ID:
        partStart = 0;
        break;

    case TECH_PART_RECIPE_ID:
        partStart = 6;
        break;

    case TECH_PART_RECIPE_SPEC:
        partStart = 12;
        break;

    case TECH_PART_DURABILITY_CURRENT:
        partStart = 22;
        break;

    case TECH_PART_DURABILITY_MAX:
        partStart = 28;
        break;

    case TECH_PART_PARENT_OBJECT_ID:
        partStart = 34;
        break;

    case TECH_PART_PARENT_RECIPE_ID:
        partStart = 40;
        break;

    case TECH_PART_CHILDREN_COUNT:
        partStart = 46;
        break;

    case TECH_PART_NTH_CHILD_OBJECT_ID:
        partStart = 48 + nth * OBJECT_ID_DIGITS * 2;
        break;

    case TECH_PART_NTH_CHILD_RECIPE_ID:
        partStart = 54 + nth * OBJECT_ID_DIGITS * 2;
        break;
    }

    return partStart;
}



// +---------------------------------------------------------------------------+
// |                                 GetTechPart                               |
// +---------------------------------------------------------------------------+

int GetTechPart(string techDesc, int partType, int nth = 0)
{
    int partStart = GetTechPartPosition(partType, nth);
    int digits = GetTechPartDigits(partType);
    return stoi(sub(techDesc, partStart, digits));
}



// +---------------------------------------------------------------------------+
// |                                ReplaceTechPart                            |
// +---------------------------------------------------------------------------+

string ReplaceTechPart(string techDesc, int partType, int newValue, int nth = 0)
{
    string part;
    int partStart = GetTechPartPosition(partType, nth);
    int digits = GetTechPartDigits(partType);
    int d;
    for (d = 0; d < digits; d++)
        part += " ";
    part = right(part + itos(newValue), digits);
    string partL = sub(techDesc, 0, partStart);
    string partR = sub(techDesc, partStart + digits, 99999);
    for (d = len(partL); d < partStart; d++)
        partL += " ";
    techDesc = partL + part + partR;

    return techDesc;
}



// +---------------------------------------------------------------------------+
// |                                  SetTechPart                              |
// +---------------------------------------------------------------------------+

void SetTechPart(object oItem, int partType, int value, int nth = 0)
{
    string techDesc = GetDescription(oItem, FALSE, FALSE);
    techDesc = ReplaceTechPart(techDesc, partType, value, nth);
    SetDescription(oItem, techDesc, FALSE);
}



// +---------------------------------------------------------------------------+
// |                                SetObjectId                                |
// +---------------------------------------------------------------------------+

void SetObjectId(object oObject, int id = -1)
{
    if (!GetIsObjectValid(oObject)) return;
    if (id < 0) id = NewObjectId();
    if (id > 999999) id = 999999;
    if (GetObjectType(oObject) == OBJECT_TYPE_ITEM)
        SetTechPart(oObject, TECH_PART_OBJECT_ID, id);
    SetLocalInt(oObject, SYSTEM_SYS_PREFIX + "_OBJECT_ID", id);
}



// +---------------------------------------------------------------------------+
// |                                GetObjectId                                |
// +---------------------------------------------------------------------------+

int GetObjectId(object oObject)
{
    if (!GetIsObjectValid(oObject)) return 0;
    int id = GetLocalInt(oObject, SYSTEM_SYS_PREFIX + "_OBJECT_ID");

    if (id <= 0)
    {
        id = 0;
        if (GetObjectType(oObject) == OBJECT_TYPE_ITEM)
        {
            InitializeItemFromTech(oObject);
            return GetLocalInt(oObject, SYSTEM_SYS_PREFIX + "_OBJECT_ID");
        }
    }

    return id;
}



// +---------------------------------------------------------------------------+
// |                                 NewObjectId                               |
// +---------------------------------------------------------------------------+

int NewObjectId()
{
    int lastId = LastObjectId();
    lastId += 1;
    SetLocalInt(
        GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"),
        SYSTEM_SYS_PREFIX + "_LAST_OBJECT_ID", lastId);
    return lastId;
}



// +---------------------------------------------------------------------------+
// |                                LastObjectId                               |
// +---------------------------------------------------------------------------+

int LastObjectId()
{
    return GetLocalInt(
        GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"),
        SYSTEM_SYS_PREFIX + "_LAST_OBJECT_ID");
}



// +---------------------------------------------------------------------------+
// |                                   Destroy                                 |
// +---------------------------------------------------------------------------+

void Destroy(object oObject, float fDelay = 0.0f)
{
    if (!GetIsObjectValid(oObject)) return;
    if (GetObjectType(oObject) == OBJECT_TYPE_ITEM)
    {
        object oPossessor = GetItemPossessor(oObject);
        if (GetIsPC(oPossessor) && !GetIsDM(oPossessor))
        {
            SetLocalString(oPossessor, SYSTEM_SYS_PREFIX + "_DESTROYED", GetResRef(oObject));
            SetLocalObject(oPossessor, SYSTEM_SYS_PREFIX + "_DESTROYED", oObject);
        }
    }

    DestroyObject(oObject, fDelay);
}



// +---------------------------------------------------------------------------+
// |                          GetItemPropertyCostValue                         |
// +---------------------------------------------------------------------------+

int GetItemPropertyCostValue(object oItem, int itemPropType)
{
    if (!GetIsObjectValid(oItem)
    || GetObjectType(oItem) != OBJECT_TYPE_ITEM) return -1;

    itemproperty ip = GetFirstItemProperty(oItem);

    while (GetIsItemPropertyValid(ip))
    {
        if (GetItemPropertyType(ip) == itemPropType)
            return GetItemPropertyCostTableValue(ip);

        ip = GetNextItemProperty(oItem);
    }

    return -1;
}



// +---------------------------------------------------------------------------+
// |                             GetItemEquippedSlot                           |
// +---------------------------------------------------------------------------+

int GetItemEquippedSlot(object oCreature, object oItem)
{
    int slot;
    for (slot = 0; slot < NUM_INVENTORY_SLOTS; slot++)
        if (GetItemInSlot(slot, oCreature) == oItem) return slot;

    return -1;
}



// +---------------------------------------------------------------------------+
// |                         ConvertInventorySlotConstant                      |
// +---------------------------------------------------------------------------+

int ConvertInventorySlotConstant(int inventorySlot)
{
    switch (inventorySlot)
    {
        case INVENTORY_SLOT_HEAD: return 0x00001;
        case INVENTORY_SLOT_CHEST: return 0x00002;
        case INVENTORY_SLOT_BOOTS: return 0x00004;
        case INVENTORY_SLOT_ARMS: return 0x00008;
        case INVENTORY_SLOT_RIGHTHAND:
        case INVENTORY_SLOT_LEFTHAND: return 0x00030;
        case INVENTORY_SLOT_CLOAK: return 0x00040;
        case INVENTORY_SLOT_RIGHTRING:
        case INVENTORY_SLOT_LEFTRING: return 0x00180;
        case INVENTORY_SLOT_NECK: return 0x00200;
        case INVENTORY_SLOT_BELT: return 0x00400;
        case INVENTORY_SLOT_ARROWS: return 0x00800;
        case INVENTORY_SLOT_BULLETS: return 0x01000;
        case INVENTORY_SLOT_BOLTS: return 0x02000;
        default: return 0x00000;
    }

    return 0x00000;
}



// +---------------------------------------------------------------------------+
// |                            IsBaseItemEquippableOn                         |
// +---------------------------------------------------------------------------+

int IsBaseItemEquippableOn(int baseItem, int inventorySlot)
{
    int slots = htoi(GetLocalString(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_BITM_" + itos(baseItem) + "_SLOTS"));
    int slot = ConvertInventorySlotConstant(inventorySlot);
        return slot & slots;
    return FALSE;
}



// +---------------------------------------------------------------------------+
// |                             FindSlotForBaseItem                           |
// +---------------------------------------------------------------------------+

int FindSlotForBaseItem(object oCreature, int nType)
{
    int slot;
    for (slot = 0; slot < NUM_INVENTORY_SLOTS; slot++)
    {
        if (GetIsObjectValid(GetItemInSlot(slot, oCreature))) continue;

        // check the hands as last slots - primary use body as target slots
        if (slot == INVENTORY_SLOT_RIGHTHAND
        || slot == INVENTORY_SLOT_LEFTHAND) continue;

        if (IsBaseItemEquippableOn(nType, slot)) return slot;
    }

    slot = INVENTORY_SLOT_RIGHTHAND;
    if (!GetIsObjectValid(GetItemInSlot(slot, oCreature))
    && IsBaseItemEquippableOn(nType, slot)) return slot;

    slot = INVENTORY_SLOT_LEFTHAND;
    if (!GetIsObjectValid(GetItemInSlot(slot, oCreature))
    && IsBaseItemEquippableOn(nType, slot)) return slot;

    return -1;
}



// +---------------------------------------------------------------------------+
// |                              ResRefToBaseItemType                         |
// +---------------------------------------------------------------------------+

int ResRefToBaseItemType(string resRef)
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int nType = GetLocalInt(oSystem, SYSTEM_SYS_PREFIX + "_ITRF_" + upper(resRef) + "_ID");

    if (!nType)
    {
        object oArea = GetObjectByTag(SYS_TAG_SYSTEMS_AREA);
        object oTemp = CreateObject(OBJECT_TYPE_ITEM, resRef, Location(oArea, Vector(), 0.0f));

        if (GetIsObjectValid(oTemp))
        {
            nType = GetBaseItemType(oTemp);

            logentry("[" + SYSTEM_SYS_PREFIX + "] Setting ItemResRef [" + upper(resRef) + "] to BaseItemType [" + itos(nType) + "]");
            SetLocalInt(oSystem, SYSTEM_SYS_PREFIX + "_ITRF_" + upper(resRef) + "_ID", nType);

            int invBonus = GetItemPropertyCostValue(oTemp, ITEM_PROPERTY_INVCAP_BONUS) + 1;
            if (invBonus > 0)
            {
                logentry("[" + SYSTEM_SYS_PREFIX + "] Setting ItemResRef [" + upper(resRef) + "] to InventoryCapacityBonus [" + itos(invBonus) + "]");
                SetLocalInt(oSystem, SYSTEM_SYS_PREFIX + "_ITRF_" + upper(resRef) + "_INVBONUS", invBonus);
            }
            SetPlotFlag(oTemp, FALSE);
            DestroyObject(oTemp);
        }
        else
        {
            nType = BASE_ITEM_INVALID;
            logentry("[" + SYSTEM_SYS_PREFIX + "] Unable to create item by resRef [" + resRef + "].. Returning BASE_ITEM_INVALID (" + itos(BASE_ITEM_INVALID) + ")");
        }
    }

    return nType;
}



// +---------------------------------------------------------------------------+
// |                               GetBaseItemSize                             |
// +---------------------------------------------------------------------------+

int GetBaseItemSize(int nType)
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int width = GetLocalInt(oSystem, SYSTEM_SYS_PREFIX + "_BITM_" + itos(nType) + "_WIDTH");
    int height = GetLocalInt(oSystem, SYSTEM_SYS_PREFIX + "_BITM_" + itos(nType) + "_HEIGHT");

    return width * height;
}



// +---------------------------------------------------------------------------+
// |                              IsWeightBasedItem                            |
// +---------------------------------------------------------------------------+

int IsWeightBasedItemType(int itemType)
{
    return stoi(Get2DAString("baseitems", "ILRStackSize", itemType)) == 0;
}

int IsWeightBasedItem(object oItem)
{
    if (!GetIsObjectValid(oItem)
    || (GetObjectType(oItem) != OBJECT_TYPE_ITEM))
        return FALSE;

    return IsWeightBasedItemType(GetBaseItemType(oItem));
}



// +---------------------------------------------------------------------------+
// |                                IsSystemItem                               |
// +---------------------------------------------------------------------------+

int IsSystemItem(object oItem, int nType = -1)
{
    if (!GetIsObjectValid(oItem))
    {
        if (nType == -1) return -1;
    }
    else if (GetObjectType(oItem) != OBJECT_TYPE_ITEM) return -1;

    if (nType == -1)
        nType = GetBaseItemType(oItem);

    if (nType == BASE_ITEM_INVNETORY_BLOCK_SLOT
    || nType == BASE_ITEM_INVNETORY_BLOCK_BAR
    || nType == BASE_ITEM_INVNETORY_BLOCK_TAB
    || nType == BASE_ITEM_DIALOGOPTION)
        return TRUE;
    return FALSE;
}



// +---------------------------------------------------------------------------+
// |                   CreateItemOnObjectAndDeleteIfUnable                     |
// +---------------------------------------------------------------------------+

object CreateItemOnObjectAndDeleteIfUnable(object oPC, string resRef)
{
    if (!GetHasInventory(oPC)) return OBJECT_INVALID;

    object oResult = CreateItemOnObject(resRef, oPC);

    if (GetItemPossessor(oResult) != oPC)
    {
        SetPlotFlag(oResult, FALSE);
        DestroyObject(oResult);
        return OBJECT_INVALID;
    }

    else return oResult;
}


// +---------------------------------------------------------------------------+
// |                                    li                                     |
// +---------------------------------------------------------------------------+



location    li(){return Location(OBJECT_INVALID, Vector(0.0f, 0.0f, 0.0f), 0.0f);}


vector vi(){ return Vector(); }


// +---------------------------------------------------------------------------+
// |                               IPS_SetTag                                  |
// +---------------------------------------------------------------------------+



void    IPS_SetTag(object oObject, string sTag)
{
    int bOS =   GetLocalInt(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_OS");

    if  (bOS    ==  SQL_COMPILE_VERSION_WINDOWS)
    {
        SetLocalString(oObject, "NWNX!FUNCS!SETTAG", sTag);
        DeleteLocalString(oObject, "NWNX!FUNCS!SETTAG");
    }

    else

    if  (bOS    ==  SQL_COMPILE_VERSION_UNIX)
    {
        SetLocalString(oObject, "NWNX!FUNCS!SETTAG", sTag);
    }
}


/*
// +---------------------------------------------------------------------------+
// |                             IPS_GetItemPrice                              |
// +---------------------------------------------------------------------------+



int     IPS_GetItemPrice(object oItem)
{
    itemproperty    ip;
    int             nValue;

    ip  =   GetFirstItemProperty(oItem);

    while   (GetIsItemPropertyValid(ip))
    {
        if  (GetItemPropertyType(ip)    ==  ITEM_PROPERTY_IPS_PRICE)
        nValue  +=  stoi(Get2DAString("ips_price", "Value", GetItemPropertySubType(ip)));
        ip      =   GetNextItemProperty(oItem);
    }

    return  nValue;
}
*/


/*
// +---------------------------------------------------------------------------+
// |                              IPS_SetItemPrice                             |
// +---------------------------------------------------------------------------+



void    IPS_SetItemPrice(object oItem, int nPrice)
{
    IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_IPS_PRICE, nPrice % 1000), 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);

    if  (nPrice >   999)
    IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_IPS_PRICE, 999 + (nPrice % 1000000) / 1000), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);

    if  (nPrice >   999999)
    IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_IPS_PRICE, 1998 + (nPrice % 1000000000) / 1000000), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);

    if  (nPrice >   999999999)
    //IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_IPS_PRICE, (nPrice % 1000000000000) / 1000000000, 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);
    IPSafeAddItemProperty(oItem, ItemPropertyDirect(ITEM_PROPERTY_IPS_PRICE, 2998), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);   //max 1B
}
*/



// +---------------------------------------------------------------------------+
// |                             IPS_GetItemWeight                             |
// +---------------------------------------------------------------------------+



int     IPS_GetItemWeight(object oItem, int bProp = FALSE)
{
    int nResult;

    //  navrati aktualni vahu
    if  (!bProp)    nResult =   GetWeight(oItem) / 10;

    else
    {
        //  zjisti vahu z iprp + vaha z base_item 2da
        int             nParam, nValue;
        itemproperty    ip;

        ip  =   GetFirstItemProperty(oItem);

        while   (GetIsItemPropertyValid(ip))
        {
            if  (GetItemPropertyType(ip)    ==  ITEM_PROPERTY_WEIGHT_INCREASE)
            {
                nParam  =   GetItemPropertyParam1Value(ip);

                if  (nParam >   0)
                {
                    nValue  =   (nParam - 1) / 9;
                    nResult +=  (nParam - nValue * 9) * ftoi(pow(10.0f, itof(nValue)));
                }
            }

            ip  =   GetNextItemProperty(oItem);
        }

        nResult +=  stoi(Get2DAString("baseitems", "TenthLBS", GetBaseItemType(oItem))) / 10;
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                              IPS_SetItemWeight                            |
// +---------------------------------------------------------------------------+



void    IPS_SetItemWeight(object oItem, int nWeight)
{
    int n;

    n   =   nWeight % 10;
    //n   -=  (n  >   0)  ?   1   :   0;

    IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_REPLACE_EXISTING, TRUE, TRUE);

    if  (nWeight    >   9)
    {
        n   =   nWeight % 100;

        if  (n / 10 >   0)
        {
            n   =   (n  >   0)  ?   9 + n / 10  :   n;

            IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);
        }
    }

    if  (nWeight    >   99)
    {
        n   =   nWeight % 1000;

        if  (n / 100    >   0)
        {
            n   =   (n  >   0)  ?   18 + n / 100  :   n;

            IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);
        }
    }

    if  (nWeight    >   999)
    {
        n   =   nWeight % 10000;

        if  (n / 1000   >   0)
        {
            n   =   (n  >   0)  ?   27 + n / 1000  :   n;

            IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);
        }
    }

    if  (nWeight    >   9999)
    {
        n   =   nWeight % 100000;

        if  (n / 10000  >   0)
        {
            n   =   (n  >   0)  ?   36 + n / 10000 :   n;

            IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);
        }
    }

    if  (nWeight    >   99999)
    {
        n   =   nWeight % 1000000;

        if  (n / 100000 >   0)
        {
            n   =   (n  >   0)  ?   45 + n / 100000 :   n;

            IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);
        }
    }

    if  (nWeight    >   999999)
    /*{
        n   =   nWeight % 1000;
        n   =   (n  >   0)  ?   18 + n / 100  :   n;

        IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(n), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE);
    }*/

    IPSafeAddItemProperty(oItem, ItemPropertyWeightIncrease(55), 0.0f, X2_IP_ADDPROP_POLICY_IGNORE_EXISTING, TRUE, TRUE); //max 1t

    if (IsWeightBasedItem(oItem))
        SetTechPart(oItem, TECH_PART_RECIPE_SPEC, nWeight);
}



// +---------------------------------------------------------------------------+
// |                             ItemPropertyDirect                            |
// +---------------------------------------------------------------------------+



itemproperty    ItemPropertyDirect(int nType, int nSubType, int nCostTable=-1, int nCostValue=-1, int nParamTable=-1, int nParamValue=-1)
{
    itemproperty ip = ItemPropertyAdditional(0);

    SetItemPropertyInteger(ip, 0, nType);
    SetItemPropertyInteger(ip, 1, nSubType);
    if (nCostTable != -1) SetItemPropertyInteger(ip, 2, nCostTable);
    if (nCostValue != -1) SetItemPropertyInteger(ip, 3, nCostValue);
    if (nParamTable != -1) SetItemPropertyInteger(ip, 4, nParamTable);
    if (nParamValue != -1) SetItemPropertyInteger(ip, 5, nParamValue);

    return ip;
}



// +---------------------------------------------------------------------------+
// |                             DuplicateCreature                             |
// +---------------------------------------------------------------------------+



object  DuplicateCreature(object oTarget, location lLocation, int bInventory = TRUE, int bEquip = TRUE, int bHeal = TRUE, string sNewTag = "")
{
    if  (!GetIsObjectValid(oTarget))    return  OBJECT_INVALID;

    object  oResult, oItem, oGold;
    int     i;

    i       =   0;
    oResult =   CopyObject(oTarget, lLocation, OBJECT_INVALID, sNewTag);
    oItem   =   GetItemInSlot(i, oResult);

    while   (i  <   NUM_INVENTORY_SLOTS)
    {
        if  (!bEquip)
        {
            SetPlotFlag(oItem, FALSE);
            DestroyObject(oItem);
        }

        else    SetDroppableFlag(oItem, FALSE);

        i++;
        oItem   =   GetItemInSlot(i, oResult);
    }

    oItem   =   GetFirstItemInInventory(oResult);

    while   (GetIsObjectValid(oItem))
    {
        if  (!bInventory)
        {
            SetPlotFlag(oItem, FALSE);
            DestroyObject(oItem);
        }

        else    SetDroppableFlag(oItem, FALSE);

        oItem   =   GetNextItemInInventory(oResult);
    }

    if  (bHeal)
    {
        DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oResult)), oResult));
    }

    AssignCommand(oResult, TakeGoldFromCreature(GetGold(oResult), oResult, TRUE));
    AssignCommand(oResult, SetIsDestroyable(FALSE, FALSE, TRUE));

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                             GetDefinedObject                              |
// +---------------------------------------------------------------------------+



object  GetDefinedObject(int nDefType, string sDefinition, object oSource = OBJECT_SELF)
{
    object  oResult, oTemp;
    string  sName;

    switch  (nDefType)
    {
        case    SYS_O_DEFINITION_TYPE_VARIABLE:
            oResult =   GetLocalObject(oSource, sDefinition);
        break;

        case    SYS_O_DEFINITION_TYPE_SELF:
            oResult =   OBJECT_SELF;
        break;

        case    SYS_O_DEFINITION_TYPE_TAG:
            oResult =   GetObjectByTag(sDefinition);
        break;

        case    SYS_O_DEFINITION_TYPE_PLAYERNAME:
            oTemp   =   GetFirstPC();
            sName   =   GetName(oTemp);

            while   (GetIsObjectValid(oTemp))
            {
                if  (TestStringAgainstPattern(sDefinition, sName))
                {
                    oResult =   oTemp;
                    break;
                }

                oTemp   =   GetNextPC();
                sName   =   GetName(oTemp);
            }
        break;

        case    SYS_O_DEFINITION_TYPE_SOURCE:
            oResult =   oSource;
        break;
    }

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                              GetItemCount                                 |
// +---------------------------------------------------------------------------+



int     GetItemCount(int nDefType, string sDefinition, int nSlot, object oSource)
{
    int     nResult, nDefItemType, nItemType, i, bItem;
    object  oItem;
    string  sValue;

    if  (nSlot  <   NUM_INVENTORY_SLOTS)
    {
        oItem   =   GetItemInSlot(nSlot, oSource);
        bItem   =   CompareItem(nDefType, sDefinition, oItem);

        if  (bItem  ==  TRUE)
        {
            nResult =   GetItemStackSize(oItem);
        }
    }

    else

    if  (nSlot  ==  SYS_O_ITEM_SLOT_INVENTORY)
    {
        oItem   =   GetFirstItemInInventory(oSource);

        while   (oItem  !=  OBJECT_INVALID)
        {
            bItem   =   CompareItem(nDefType, sDefinition, oItem);

            if  (bItem  ==  TRUE)
            {
                nResult +=  GetItemStackSize(oItem);
            }

            oItem   =   GetNextItemInInventory(oSource);
        }
    }

    else

    if  (nSlot  ==  SYS_O_ITEM_SLOT_EQUIPPED)
    {
        i       =   0;

        while   (i  <   NUM_INVENTORY_SLOTS)
        {
            oItem   =   GetItemInSlot(i, oSource);
            bItem   =   CompareItem(nDefType, sDefinition, oItem);

            if  (bItem  ==  TRUE)
            {
                nResult +=  GetItemStackSize(oItem);
            }

            i++;
        }
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                               CompareItem                                 |
// +---------------------------------------------------------------------------+



int     CompareItem(int nDefType, string sDefinition, object oItem)
{
    int bResult;

    switch  (nDefType)
    {
        case    SYS_O_ITEM_DEFINITION_BY_TYPE:
            bResult =   GetBaseItemType(oItem)  ==  StringToInt(sDefinition);
        break;

        case    SYS_O_ITEM_DEFINITION_BY_TAG:
            bResult =   GetTag(oItem)   ==  sDefinition;
        break;

        case    SYS_O_ITEM_DEFINITION_BY_RESREF:
            bResult =   GetResRef(oItem)    ==  sDefinition;
        break;

        case    SYS_O_ITEM_DEFINITION_BY_NAME:
            bResult =   GetName(oItem)  ==  sDefinition;
        break;
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                           GetHasEquippedWeaponType                        |
// +---------------------------------------------------------------------------+



int     GetHasEquippedWeaponType(object oCreature = OBJECT_SELF, int nWeaponType = WEAPON_TYPE_SLASHING)
{
    int bResult =   FALSE;

    object  oRight  =   GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oCreature);
    object  oLeft   =   GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oCreature);

    if  (!GetIsObjectValid(oRight)
    &&  !GetIsObjectValid(oLeft))   return  FALSE;

    int nRight  =   StringToInt(Get2DAString("baseitems", "WeaponType", GetBaseItemType(oRight)));
    int nLeft   =   StringToInt(Get2DAString("baseitems", "WeaponType", GetBaseItemType(oLeft)));

    if  (nRight ==  nWeaponType
    ||  nLeft   ==  nWeaponType)
    bResult =   TRUE;

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                            GetItemWeaponType                              |
// +---------------------------------------------------------------------------+



int     GetItemWeaponType(object oItem)
{
    if  (!GetIsObjectValid(oItem)
    ||  GetObjectType(oItem)    !=  OBJECT_TYPE_ITEM)   return  FALSE;

    int nResult =   StringToInt(Get2DAString("baseitems", "WeaponType", GetBaseItemType(oItem)));

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                              GetItemWeaponSize                            |
// +---------------------------------------------------------------------------+



int     GetItemWeaponSize(object oItem)
{
    if  (!GetIsObjectValid(oItem)
    ||  GetObjectType(oItem)    !=  OBJECT_TYPE_ITEM)   return  FALSE;

    int nResult =   StringToInt(Get2DAString("baseitems", "WeaponSize", GetBaseItemType(oItem)));

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                          GetItemEquipableInSlot                           |
// +---------------------------------------------------------------------------+



int     GetItemEquipableInSlot(object oItem, int nEqSlot = EQUIPABLE_SLOT_MAIN_HAND)
{
    if  (!GetIsObjectValid(oItem)
    ||  GetObjectType(oItem)    !=  OBJECT_TYPE_ITEM)   return  FALSE;

    int nSlot   =   htoi(Get2DAString("baseitems", "EquipableSlots", GetBaseItemType(oItem)));
    int bResult =   (nEqSlot    ==  EQUIPABLE_SLOT_GET) ?   nSlot   :   (nSlot & nEqSlot);

    return  bResult;
}



/*
// +---------------------------------------------------------------------------+
// |                              GetIsWieldingWeapon                          |
// +---------------------------------------------------------------------------+



int     GetIsWieldingWeapon(object oCreature = OBJECT_SELF, int nWeaponWield = WEAPON_WIELD_STANDARD_ONE_HANDED)
{
    int     bResult =   FALSE;
    int     nSlot   =   0;
    object  oLeft   =   GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oCreature);
    object  oRight  =   GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oCreature);

    bResult =   StringToInt(Get2DAString("baseitems", "WeaponWield", GetBaseItemType(oLeft)))   ==  nWeaponWield
    ||          StringToInt(Get2DAString("baseitems", "WeaponWield", GetBaseItemType(oRight)))  ==  nWeaponWield;

    return  bResult;
}
*/



// +---------------------------------------------------------------------------+
// |                               CreateObjectOffset                          |
// +---------------------------------------------------------------------------+



object  CreateObjectOffset(int nObjectType, string sTemplate, location lLocation, int bUseAppearAnimation=FALSE, string sNewTag="")
{
//    lLocation   =   Location(
//                        GetAreaFromLocation(lLocation),
//                        Vector(glf(
    return  CreateObject(nObjectType, sTemplate, lLocation, bUseAppearAnimation, sNewTag);
}
