#include "vg_s0_gui_core"

///// GUI controls definition by the system using the GUI

const int _GUI_TYPE = GUI_TYPE_INTERACT;

void main()
{
/*
    //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, width, height, posX,  posY, layer, vfxId,                      hasLayout       # row id
    GUI_LayoutAddObject(_GUI_TYPE,       9.0,   6.0,    0.0,   0.0,  0,     GUI_VFX_INTERACT_WINDOW,    "");            // 1. window
    GUI_LayoutAddObject(_GUI_TYPE,       0.5,   0.5,    3.5,   2.0,  1,     GUI_VFX_CLOSE_BUTTON,       "gui_lyt_close");   // 2. close button
    GUI_LayoutAddObject(_GUI_TYPE,       3.83,  4.59,   2.015, 0.025,1,     GUI_VFX_EMPTY,              "");            // 3. scenario image
    GUI_LayoutAddObject(_GUI_TYPE,       3.50,  0.99,   -2.15, 1.825,1,     GUI_VFX_EMPTY,              "");            // 4. title

    //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, width, height, distX, distY, rows, cols, margin, layer, hasLayout
    GUI_LayoutAddGridObjects(_GUI_TYPE,  1.0,   1.0,    0.6,   0.73,  3,    3,    0.15f,  1,     "gui_lyt_1_choice");         // 5+ objects or 9 actions grid
*/

// simple gui

    //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, width, height, posX,  posY,   layer, vfxId,                        hasLayout             # row id
    GUI_LayoutAddObject(_GUI_TYPE,       7.0,   1.0,    0.0,   0.0,    0,     GUI_VFX_INTERACT_RADIAL_MENU, "");                  // 1. radial menu
    GUI_LayoutAddObject(_GUI_TYPE,       0.5,   0.5,    0.0,   1.10,   1,     GUI_VFX_CLOSE_BUTTON,         "gui_lyt_close");     // 2. close button

    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    0.00,  0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle
    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    1.00,  0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle
    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    -1.00, 0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle
    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    2.00,  0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle
    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    -2.00, 0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle
    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    3.00,  0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle
    GUI_LayoutAddObject(_GUI_TYPE,       1.0,   1.0,    -3.00, 0.00,   1,     GUI_VFX_EMPTY,                "gui_lyt_1_choice");  // 3. radial choice top middle

}

