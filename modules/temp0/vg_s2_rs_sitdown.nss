// +---------++----------------------------------------------------------------+
// | Name    || Rest System (TIS) Sit down script                              |
// | File    || vg_s2_rs_sitdwn.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Hrac, ktery vybral moznost "Sednout si" v rest menu (LPD rozhovor) si sedne
    na nejblizsi volne misto na sezeni
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_const"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   LPD_GetPC();
    object  oSit, oTemp;
    int     n;

    oTemp   =   GetNearestObject(OBJECT_TYPE_PLACEABLE, oPC, ++n);

    while   (GetIsObjectValid(oTemp))
    {
        //  vyhledat mista na sezeni pouze v okruhu 5ti metru
        if  (GetDistanceBetween(oPC, oTemp) >   5.0f)   break;

        //  pouze nepouzitelne objekty
        if  (!GetUseableFlag(oTemp))
        {
            //  objekt musi mit nastavenou promennout "RS_SIT" na "1" (TRUE)
            if  (gli(oTemp, SYSTEM_RS_PREFIX + "_SIT"))
            {
                //  misto na sezeni nesmi byt obsazeno
                if  (!GetIsObjectValid(GetSittingCreature(oTemp)))
                {
                    oSit    =   oTemp;
                    break;
                }
            }
        }

        oTemp   =   GetNearestObject(OBJECT_TYPE_PLACEABLE, oPC, ++n);
    }

    string  sMessage;

    if  (!GetIsObjectValid(oSit))
    {
        sMessage    =   C_NEGA + "Your character did not find any free sitplace nearby";

        msg(oPC, sMessage, TRUE, FALSE);
//        sls(oPC, SYSTEM_LPD_PREFIX + "_TALK", sMessage, -2);
        AssignCommand(oPC, PlayAnimation(ANIMATION_FIREFORGET_PAUSE_SCRATCH_HEAD, 1.0f, 0.0f));
    }

    else
    {
        sMessage    =   ConvertTokens("*Jde si sednout*", oPC);

        AssignCommand(oPC, SpeakString(sMessage));
        AssignCommand(oPC, ActionSit(oTemp));
    }
}

