// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_access.nss                                                 |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 27-06-2009                                                     |
// | Updated || 27-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "ACCESS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "ACCESS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sGroup      =   GetStringUpperCase(CCS_GetConvertedString(sLine, 1, FALSE));
    string  sUsedPwd    =   CCS_GetConvertedString(sLine, 2, FALSE);
    string  sTruePwd    =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_PASSWORD");
    int     nId         =   gli(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_ID");
    int     bAccess     =   CCS_GetHasAccess(oPC, sGroup, FALSE);
    string  sMessage;

    //  Neinicializovana skupina
    if  (!nId)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid command group [" + R1 + sGroup + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  Jiz pristupna skupina
    if  (bAccess)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "You already have access to the command group [" + R1 + sGroup + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  Chybne heslo
    if  (sUsedPwd   !=  sTruePwd)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid password [" + R1 + sUsedPwd + W2 + "] for command group [" + G1 + sGroup + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    CCS_GrantAccess(oPC, sGroup, FALSE);
    SYS_Info(oPC, SYSTEM_CCS_PREFIX, "ACCESS_GRANTED_&" + sGroup + "&");

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
