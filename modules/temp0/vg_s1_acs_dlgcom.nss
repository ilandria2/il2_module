// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Common dialog script               |
// | File    || vg_s1_acs_dlgcom.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Parametrizovany skript pro obecni rozhovor
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_acs_core_c"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+


void    ExamineGUI(object oPC)
{
    AssignCommand(oPC, ClearAllActions(TRUE));
    AssignCommand(oPC, ActionExamine(glo(oPC, SYSTEM_LPD_PREFIX + "_GUI")));
}

void    pricename(object oItem, int COST, string Color1, string Color2, string sign)
{
    string  sName   =   Color1 + ((sign == "-") ? "Buy" : "Sell") + "\n" + GetName(oItem) + Color2 + "\nfor " + Color1 + itos(COST) + " coins";
//    string  sName   =   Color2 + "price: " + Color1 + sign + itos(COST) + " coins" + Color2 + " for:\n" + GetName(oItem);

    SetName(oItem, sName);
}


void    main()
{
    //SpawnScriptDebugger();
    //int nlpn = GetListenPatternNumber();
    //object ospk = GetLastSpeaker();
    //msg(GetFirstPC(), "lpn: " + itos(GetListenPatternNumber()));
    object  oNPC    =   LPD_GetSource();

    if (GetIsPC(oNPC) || !GetIsObjectValid(oNPC))
        return;

    object customDialog = LPD_GetDialog(oNPC);

    if (GetIsObjectValid(customDialog))
        return;

    string  sParam  =   gls(OBJECT_SELF, SYSTEM_LPD_PREFIX + "_SCRIPT_PARAMETER", -1, SYS_M_DELETE);

    if  (gls(oNPC, SYSTEM_LPD_PREFIX + "_DIALOG")   ==  "")
    {
        sParam  =   "INIT";

        sls(oNPC, SYSTEM_LPD_PREFIX + "_DIALOG", ACS_TAG_DIALOG_COMMON);
    }

    if  (sParam ==  "") return;

    else

    if  (sParam ==  "INIT"
    &&  gli(oNPC, SYSTEM_ACS_PREFIX + "_INIT"))
    {
        sli(oNPC, SYS_VAR_RETURN, TRUE);
        return;
    }

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oPC     =   LPD_GetPC();
    string  sTag    =   GetTag(oNPC);
    string  sGameDT =   GetCalendarDate();
    int     bResult =   FALSE;
    int     bIndex  =   0;
    int     n, nID, nQty;

    //  nacteni promennych pro NPC
    if  (sParam ==  "INIT")
    {
        bResult =   TRUE;

        //  1 - Nastavi prani (WISH) + zakladni texty
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sTag + "_WISH");

        if  (bIndex)
        {
            sls(oNPC, "T_GREET", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_GREET"));
            sls(oNPC, "T_INFO_SELF", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_INFO_S"));
            sls(oNPC, "T_INFO_AREA", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_INFO_A"));
            sls(oNPC, "T_WISH_NONE", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_N"));

            if  (gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_ITEM_RID", 1)    >=  100)
            {
                sls(oNPC, "T_WISH_DONE", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_D"));
                sls(oNPC, "T_WISH_DETAIL", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_I"));
                sls(oNPC, "T_MESSAGE", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_M"));
                sli(oNPC, SYSTEM_ACS_PREFIX + "_WISH", bIndex);
            }
        }

        //  2 - Nastavi zakazky (CARGO)
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sTag + "_CARGO", n = 1);

        while   (bIndex)
        {
            if  (gls(oNPC, "T_CARGO_NONE")  ==  "") sls(oNPC, "T_CARGO_NONE", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_N"));
            if  (gls(oNPC, "T_CARGO_SHORT") ==  "") sls(oNPC, "T_CARGO_SHORT", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_S"));
            if  (gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_ITEM_RID", 1)    >=  100
            &&  cd(gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_DATE_END"), sGameDT, SYS_M_HIGHER_OR_EQUAL)
            &&  !gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_COMPLETE"))
            {
                sls(oNPC, "M_CARGO", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_DISPLAY"), -2);
                sls(oNPC, "T_MESSAGE", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_M"), -2);
                sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", bIndex, -2);
            }

            bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sTag + "_CARGO", ++n);
        }

        //  3 - nastavi eventy u NPC
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_TAG_" + sTag + "_EVENT");

        if  (bIndex)
        {
            sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT", bIndex);
            sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1);
            sls(oNPC, "T_EVENT_NONE", gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_T_NONE"));
            sls(oNPC, "T_EVENT_PROGRESS", gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_T_PROGRESS"));
            sls(oNPC, "T_EVENT_DETAIL", gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_T_DETAIL"));
            sls(oNPC, "T_EVENT_DONE", gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_T_DONE"));
        }

        //  4 - nastavi uceni u NPC
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + sTag + "_TDID");

        if  (bIndex)
        {
            sli(oNPC, SYSTEM_ACS_PREFIX + "_TEACHER", bIndex);
            sls(oNPC, "M_TEACH", gls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(bIndex) + "_MREQUEST"));
            sls(oNPC, "T_TEACH", gls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(bIndex) + "_TTEACH"));
            sls(oNPC, "T_TEACH_REQ", gls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(bIndex) + "_TREQUIRE"));
        }

        //  5 - nastavi xchange obchod u NPC
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + sTag + "_ID", n = 1);

        while   (bIndex)
        {
            sli(oNPC, SYSTEM_ACS_PREFIX + "_XITEM", bIndex, -2);
            sls(oNPC, "M_XITEM_" + itos(n), gls(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + itos(bIndex) + "_MENU"));

            bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + sTag + "_ID", ++n);
        }

        //  6 - nastavi obchod u NPC
        nID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SD_" + sTag + "_ID");
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_STORE_" + itos(nID) + "_SI", n = 1);

        while   (bIndex)
        {
            nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RQTY");

            if  (nQty   >   0)
            sli(oNPC, SYSTEM_ACS_PREFIX + "_XBUY", bIndex, -2);

            else

            if  (nQty   <   0)
            sli(oNPC, SYSTEM_ACS_PREFIX + "_XSELL", bIndex, -2);

            bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_STORE_" + itos(nID) + "_SI", ++n);
        }

        //  promenna "dokoncena inicializace"
        sli(oNPC, SYSTEM_ACS_PREFIX + "_NPC_INIT", TRUE);
    }

    else

    if  (sParam ==  "XSTORE_BUY")
    {
        SpawnScriptDebugger();
        object  oStore, oItem;
        vector  vPos;
        string  sPort;
        int RID, RQTY, RS1, RS2, RS3, RS4, RS5, COST;

        vPos    =   GetPosition(oPC);
        oStore  =   CreateObject(OBJECT_TYPE_PLACEABLE, "vg_p_so_inventor", Location(GetArea(oPC), GetAwayVector(Vector(vPos.x, vPos.y, vPos.z - 0.5f), 0.1f, GetFacing(oPC)), GetFacing(oPC)));

        //  nacteni predmetu, ktere npc prodava
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XBUY", n = 1);

        while   (bIndex)
        {
            RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RID");
            RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RQTY");
            RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC1");
            RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC2");
            RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC3");
            RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC4");
            RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC5");
            COST    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_COST");
            oItem   =   ACS_CreateItemUsingRecipe(RID, RQTY, li(), oStore, FALSE, RS1, RS2, RS3, RS4, RS5);

            DelayCommand(0.15f, pricename(oItem, COST, R1, R2, "-"));
            sli(oItem, SYSTEM_ACS_PREFIX + "_XSTORE", bIndex);
            SetPlotFlag(oItem, TRUE);

            bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XBUY", ++n);
        }

        //  nastaveni vzhledu kontejneru (obchodu) - nazev, portret
        sPort   =   (sPort  ==  "") ?   GetPortraitResRef(oPC)  :   sPort;

        DelayCommand(0.1f, SetName(oStore, W2 + "NPC " + R1 + "sells" + W2 + " these items:"));
        slo(oPC, SYSTEM_ACS_PREFIX + "_XSTORE", oStore);
        sli(oStore, SYSTEM_ACS_PREFIX + "_STORE_MODE", ACS_STORE_MODE_BUY);

        if  (sPort  !=  "")
        SetPortraitResRef(oStore, sPort);
        AssignCommand(oPC, ClearAllActions());
        SetUseableFlag(oStore, TRUE);
        DelayCommand(0.3, AssignCommand(oPC, ActionInteractObject(oStore)));
    }

    else

    if  (sParam ==  "XSTORE_SELL")
    {
        object  oStore, oItem;
        vector  vPos;
        string  sPort;
        int RID, RQTY, RS1, RS2, RS3, RS4, RS5, COST;

        vPos    =   GetPosition(oPC);
        oStore  =   CreateObject(OBJECT_TYPE_PLACEABLE, "vg_p_so_inventor", Location(GetArea(oPC), GetAwayVector(Vector(vPos.x, vPos.y, vPos.z - 0.5f), 0.1f, GetFacing(oPC)), GetFacing(oPC)));

        //  nacteni predmetu, ktere npc prodava
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XSELL", n = 1);

        while   (bIndex)
        {
            RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RID");
            RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RQTY");
            RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC1");
            RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC2");
            RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC3");
            RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC4");
            RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC5");
            COST    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_COST");
            oItem   =   ACS_CreateItemUsingRecipe(RID, -RQTY, li(), oStore, FALSE, RS1, RS2, RS3, RS4, RS5);

            DelayCommand(0.15f, pricename(oItem, COST, G1, G2, "+"));
            sli(oItem, SYSTEM_ACS_PREFIX + "_XSTORE", bIndex);
            SetPlotFlag(oItem, TRUE);

            bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XSELL", ++n);
        }

        //  nastaveni vzhledu kontejneru (obchodu) - nazev, portret
        sPort   =   (sPort  ==  "") ?   GetPortraitResRef(oPC)  :   sPort;

        DelayCommand(0.1f, SetName(oStore, W2 + "NPC " + G1 + "buys" + W2 + " these items:"));
        slo(oPC, SYSTEM_ACS_PREFIX + "_XSTORE", oStore);
        sli(oStore, SYSTEM_ACS_PREFIX + "_STORE_MODE", ACS_STORE_MODE_SELL);

        if  (sPort  !=  "")
        SetPortraitResRef(oStore, sPort);
        AssignCommand(oPC, ClearAllActions());
        SetUseableFlag(oStore, TRUE);
        DelayCommand(0.3, AssignCommand(oPC, ActionInteractObject(oStore)));
    }

    else

    if  (sParam ==  "XSTORE_INFO")
    {
        string  sInfo, sText;

        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XITEM", n = 1);

        while   (bIndex)
        {
            sInfo   =   gls(oSystem, SYSTEM_ACS_PREFIX + "_XSD_" + itos(bIndex) + "_INFO");
            sText   +=  ConvertTokens(sInfo, oNPC, FALSE) + "\n\n";
            bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XITEM", ++n);
        }

        //sText   =   C_INTE + "Tady je m� nab�dka:\n" + sText;

        sls(oNPC, SYSTEM_LPD_PREFIX + "_TALK", sText, -2);
        DelayCommand(0.5f, ExamineGUI(oPC));
    }

    else

    if  (sParam ==  "XSTORE_FINISH")
    {
        int XSI, RID, RQTY, RS1, RS2, RS3, RS4, RS5, nAvail;

        bResult =   TRUE;
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XITEM");            //  selected exchange store item Row
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_XITEM", bIndex);    //  selected exchange store item ID
        XSI     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSTORE_" + itos(bIndex) + "_XSI", n = 1);

        //  kontrola podminek obchodu
        while   (XSI)
        {
            RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RQTY");

            if  (RQTY   <   0)
            {
                RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RID");
                RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC1");
                RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC2");
                RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC3");
                RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC4");
                RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC5");
                nAvail  =   ACS_GetTargetHasRecipe(oPC, RID, RS1, RS2, RS3, RS4, RS5);

                if  (nAvail <   -RQTY)
                {
                    bResult =   FALSE;
                    break;
                }
            }

            XSI     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSTORE_" + itos(bIndex) + "_XSI", ++n);
        }

        //  podminky obchodu splneny - dokonceni obchodu
        if  (bResult)
        {
            XSI     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSTORE_" + itos(bIndex) + "_XSI", n = 1);

            while   (XSI)
            {
                RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RQTY");
                RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RID");
                RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC1");
                RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC2");
                RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC3");
                RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC4");
                RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSI_" + itos(XSI) + "_RSPEC5");

                //  hrac prijde o predmet podle receptu RID
                if  (RQTY   <   0)
                {
                    ACS_RemoveItemUsingRecipe(RID, -RQTY, oPC, RS1, RS2, RS3, RS4, RS5);
                }

                //  zisk pro hrace
                else
                {
                    ACS_CreateItemUsingRecipe(RID, RQTY, li(), oPC, FALSE, RS1, RS2, RS3, RS4, RS5);
                }

                XSI     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_XSTORE_" + itos(bIndex) + "_XSI", ++n);
            }

            //  statistika
            sls(oPC, SYSTEM_ACS_PREFIX + "_PSI_TYPE", "XSTORE");
            sli(oPC, SYSTEM_ACS_PREFIX + "_PSI_XSTORE_ID", bIndex);
            ExecuteScript("vg_s1_acs_psi", oPC);

            //  notifikace
            SYS_Info(oPC, SYSTEM_ACS_PREFIX, "XSTORE_FINISH");
        }
    }

    else

    if  (sParam ==  "TEACH_REQ")
    {
        int TDID, RID, RQTY, RS1, RS2, RS3, RS4, RS5, nAvail, n;

        bResult =   TRUE;
        TDID    =   gli(oNPC, SYSTEM_ACS_PREFIX + "_TEACHER");
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_TRID", n = 1);

        while   (bIndex >=  100)
        {
            RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RID");
            RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RQTY");
            RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC1");
            RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC2");
            RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC3");
            RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC4");
            RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC5");
            nAvail  =   ACS_GetTargetHasRecipe(oPC, RID, RS1, RS2, RS3, RS4, RS5);

            if  (nAvail <   -RQTY)
            {
                bResult =   FALSE;
                break;
            }

            bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_TRID", ++n);
        }

        bResult =   !bResult;
    }
    else

    if  (sParam ==  "TAUGHT")
    {
        int     TDID, nValue, nVarValue;
        string  sVarName;

        TDID        =   gli(oNPC, SYSTEM_ACS_PREFIX + "_TEACHER");
        sVarName    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_VARNAME");
        nVarValue   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_VARVALUE");
        nValue      =   gli(oPC, sVarName);
        bResult     =   nValue < nVarValue;
    }

    else

    if  (sParam ==  "TEACH")
    {
        int TDID, RID, RQTY, RS1, RS2, RS3, RS4, RS5, n;

        TDID    =   gli(oNPC, SYSTEM_ACS_PREFIX + "_TEACHER");
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_TRID", n = 1);

        while   (bIndex >=  100)
        {
            RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RID");
            RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RQTY");
            RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC1");
            RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC2");
            RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC3");
            RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC4");
            RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TRI_" + itos(bIndex) + "_RSPEC5");

            ACS_RemoveItemUsingRecipe(RID, -RQTY, oPC, RS1, RS2, RS3, RS4, RS5);

            bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_TRID", ++n);
        }

        string  sVarName    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_VARNAME");
        int     nVarValue   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_TEACHER_" + itos(TDID) + "_VARVALUE");

        sli(oPC, sVarName, nVarValue);
        spi(oPC, sVarName, nVarValue);

        //  notifikace
        SYS_Info(oPC, SYSTEM_ACS_PREFIX, "TEACH");
    }

    else

    if  (sParam ==  "WISH_PASSED")
    {
        bResult =   ACS_GetTargetPassedCargo(oPC, gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sTag + "_WISH"));
    }

    else

    if  (sParam ==  "WISH_DONE")
    {
        bIndex  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + sTag + "_WISH");
        bResult =   ACS_GetTargetPassedCargo(oPC, bIndex);

        if  (bResult    >   0)
        ACS_PassCargoForTarget(oPC, bIndex, bResult, FALSE);
    }

    else

    if  (sParam ==  "CARGO_DETAIL")
    {
        bResult =   TRUE;
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO");
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", bIndex);

        sls(oNPC, "T_CARGO_DETAIL", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_I"));
        sls(oNPC, "T_CARGO_DONE", gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_T_CARGO_D"));
    }

    else

    if  (sParam ==  "CARGO_PASSED")
    {
        int bCargo;

        bCargo  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO");
        bCargo  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", bCargo);

        bResult =   ACS_GetTargetPassedCargo(oPC, bCargo);
    }

    else

    if  (sParam ==  "CARGO_DONE")
    {
        int bCargo  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO");
        int nCount  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO_I_ROWS");
        int x       =   bCargo;

        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", bCargo);
        bResult =   ACS_GetTargetPassedCargo(oPC, bIndex);

        if  (bResult    >   0)
        ACS_PassCargoForTarget(oPC, bIndex, bResult, TRUE);

        //  odstrani cargo ze seznamu
        while   (x  <   nCount)
        {
            sls(oNPC, "M_CARGO", gls(oNPC, "M_CARGO", x + 1), x);
            sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", x + 1), x);
            x++;
        }

        sls(oNPC, "M_CARGO", "", x, SYS_M_DELETE);
        sli(oNPC, "M_CARGO_S_ROWS", nCount - 1);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", -1, x, SYS_M_DELETE);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO_I_ROWS", --nCount);
    }

    else

    if  (sParam ==  "EXPIRE")
    {
        int bExpire, nCount, x;

        n       =   0;
        bResult =   TRUE;
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", ++n);
        nCount  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO_I_ROWS");

        while   (bIndex >=  100)
        {
            bExpire =   cd(gls(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bIndex) + "_DATE_END"), sGameDT, SYS_M_LOWER);

            if  (bExpire)
            {
                x   =   n--;

                while   (x  <   nCount)
                sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", x + 1), x++);
                sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", -1, x, SYS_M_DELETE);
                sli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO_I_ROWS", --nCount);
            }

            bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_CARGO", ++n);
        }

        if  (!nCount)
        {
            //bResult =   FALSE;
        }
    }

    else

    if  (sParam ==  "RA_ENTER")
    {
        bIndex  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT");

        int     nPC     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_PLAYERS");
        int     nMin    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_PCMIN");
        int     nMax    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_PCMAX");
        string  sTag    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_AREATAG");
        object  oStart  =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_START");
        object  oNPCLoc =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_NPCLOC");
        object  oOwner;
        int     bFull;

        nPC     +=  1;
        bFull   =   (nPC    <   nMin)   ?   -1  :
                    (nPC    ==  nMax)   ?   1   :   0;

        if  (nPC    ==  1)
        {
            oOwner  =   DuplicateCreature(oNPC, GetLocation(oNPCLoc), FALSE, TRUE, TRUE);

            SetPlotFlag(oOwner, TRUE);
            //ChangeToStandardFaction(oOwner, STANDARD_FACTION_COMMONER);
            SetStandardFactionReputation(STANDARD_FACTION_HOSTILE, 50, oOwner);
            ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectVisualEffect(VFX_DUR_GHOSTLY_VISAGE), oOwner);
            sls(oOwner, "LPD_DIALOG", ACS_TAG_DIALOG_EVENT_OWNER);
            sls(oOwner, "T_EVENT_DONE", gls(oNPC, "T_EVENT_DONE"));
            sls(oOwner, "M_WAVE_NEXT", "Begin!");
            slo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_OWNER", oOwner);
        }

        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_FULL", bFull);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_WAVE_READY", !bFull);
        sli(oOwner, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1);
        sli(oOwner, SYSTEM_ACS_PREFIX + "_EVENT_FULL", bFull);
        sli(oOwner, SYSTEM_ACS_PREFIX + "_WAVE_READY", !bFull);
        sli(oStart, SYSTEM_ACS_PREFIX + "_EVENT", bIndex);
        slo(oStart, SYSTEM_ACS_PREFIX + "_NPC", oNPC);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_PLAYERS", nPC);
        AssignCommand(oPC, JumpToLocation(GetLocation(oStart)));
    }

    bResult =   (bResult    !=  0)  ?   TRUE    :   FALSE;

    sli(OBJECT_SELF, SYS_VAR_RETURN, bResult);
}

