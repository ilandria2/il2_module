// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_debug.nss                                                  |
// | Author  || VirgiL                                                         |
// | Created || 28-12-2009                                                     |
// | Updated || 28-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "DEBUG"
*/
// -----------------------------------------------------------------------------

#include    "vg_s0_ccs_core_t"
#include    "vg_s0_acs_core_c"
#include    "vg_s0_tis_core_c"
#include    "nwnx_funcs"
#include    "nwnx_funcsext"
#include "nwnx_funcs_w"
#include "aps_include"
#include "vg_s0_ids_core_c"
#include "x0_i0_position"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void    RemoveSpecificEffect(object oPC, int nType)
{
    effect  eTemp   =   GetFirstEffect(oPC);

    while   (GetIsEffectValid(eTemp))
    {
        msg(oPC, "effect list: " + itos(GetEffectType(eTemp)), TRUE);

        if  (GetEffectType(eTemp)   ==  nType)
        {
            RemoveEffect(oPC, eTemp);
            msg(oPC, "removing effect " + itos(nType));
            return;
        }

        eTemp   =   GetNextEffect(oPC);
    }
}



void    ResetFeats(object oPC)
{
    int n       =   0;
    int nFeats  =   GetTotalKnownFeats(oPC);
    int nFeat;

    //  smazat vsechny featy
    for (n = nFeats - 1; n >= 0; n--)
    {
        nFeat   =   GetKnownFeat(oPC, n);

        RemoveKnownFeat(oPC, nFeat);
    }

    //  pridat zacatecni featy
    AddKnownFeat(oPC, FEAT_TIS_TARGET, -1);
    AddKnownFeat(oPC, FEAT_TIS_INTERACTION_MENU, -1);
    AddKnownFeat(oPC, FEAT_SYS_MODE_RUN, -1);
    //AddKnownFeat(oPC, FEAT_BCS_ADVANCED_HIT_STRONG, -1);
    //AddKnownFeat(oPC, FEAT_BCS_ADVANCED_HIT_MIGHTY, -1);

    msg(oPC, "Feats reset", TRUE);
}


float calc_orient(location loc_pc, location loc_target)
{
    float orient = 0.0;
    vector pos = GetPositionFromLocation(loc_pc);
    vector posTarget = GetPositionFromLocation(loc_target);
    float distTarget = GetDistanceBetweenLocations(loc_pc, loc_target);
    float baseOrient =
        posTarget.x>=pos.x && posTarget.y>=pos.y ? 0.0 :
        posTarget.x>=pos.x && posTarget.y<pos.y ? 270.0 :
        posTarget.x<pos.x && posTarget.y<pos.y ? 180.0 : 90.0;
    orient = asin(fabs(posTarget.x - pos.x)/distTarget);
    orient = (baseOrient == 0.0 || baseOrient == 180.0) ? 90.0 - orient : orient;
    orient = baseOrient + orient;
        /*
        msg(oPC,
            C2 + "----------------------\n" +
            "\nposPC: X=" + ftos(pos.x) +
            "\nposPC: Y=" + ftos(pos.y) +
            "\nposTG: X=" + ftos(posTarget.x) +
            "\nposTG: Y=" + ftos(posTarget.y) +
            "\ndistTG: " + ftos(distTarget) +
            "\nbase=" + itos(ftoi(baseOrient)) +
            "\norientCur: " + ftos(GetFacing(oPC)) +
            "\norientNew: " + ftos(orient));
        */
    return orient;
}

void    main()
{
    string      sCmd    =   "DEBUG";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sParam  =   CCS_GetConvertedString(sLine, 1, FALSE);
    int     nParam  =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    float   fParam  =   CCS_GetConvertedFloat(sLine, 3, FALSE);
    object  oParam  =   CCS_GetConvertedObject(sLine, 4, FALSE);
    location    lParam  =   CCS_GetConvertedLocation(sLine, 5, FALSE, TRUE);
    string  sMessage;

    sParam  =   upper(sParam);

    if (gli(oPC, "cmddbg")) SpawnScriptDebugger();

    if (sParam == "SETPRICE")
    {
        NWNXFuncs_SetItemValue(oParam, nParam);
    }

    else if (sParam == "GETPRICE")
    {
        int value = NWNXFuncs_GetItemValue(oParam, ITEM_VALUE_IDENTIFIED);
        msg(oPC, "Item " + GetTag(oParam) + " value: " + itos(value));
    }

    else if (sParam == "RESETCART" || sParam == "ACCEPTCART")
    {
        sls(oPC, "debug_param", sParam);
        ExecuteScript("debug_cart", oPC);
    }

    else if (sParam == "ADJFAC")
    {
        AssignCommand(oPC, SetFacing(calc_orient(GetLocation(oPC), lParam)));
    }

    else if (sParam == "ADJPOS")
    {
        object plc = glo(oPC, "PLC");
        if (!GetIsObjectValid(plc))
            return;
        location locPlc = GetLocation(plc);
        vector posTarget = GetPositionFromLocation(lParam);
        vector posPlc = GetPositionFromLocation(locPlc);
        float orTarget = 0.0f;
        switch (nParam)
        {
            case 0: // xy position
                posTarget.z = posPlc.z;
                orTarget = GetFacingFromLocation(locPlc);
                break;
            case 1: // z position
                posTarget = posPlc;
                posTarget.z = fParam;
                orTarget = GetFacingFromLocation(locPlc);
                break;
            case 2: // orientation
                posTarget = posPlc;
                orTarget = calc_orient(locPlc, lParam);
                break;
        }

        string plcResRef = GetResRef(plc);
        object plcTarget = CreateObject(OBJECT_TYPE_PLACEABLE, plcResRef, Location(GetArea(oPC), posTarget, orTarget));

        if (!GetIsObjectValid(plcTarget))
        {
            msg(oPC, "Unable to duplicate plc");
            return;
        }

        SetHardness(GetHardness(plc), plcTarget);
        int damage = GetMaxHitPoints(plc) - GetCurrentHitPoints(plc);
        if (damage > 0)
            ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(damage, DAMAGE_TYPE_MAGICAL, DAMAGE_POWER_ENERGY), plcTarget);

        // copy vars
        struct LocalVariable lvar = GetFirstLocalVariable(plc);
        while (GetIsVariableValid(lvar))
        {
            //msg(oPC, C2 + "Copying var [" + lvar.name + "]");
            switch (lvar.type)
            {
                case VARIABLE_TYPE_FLOAT:
                    slf(plcTarget, lvar.name, glf(plc, lvar.name));
                    break;
                case VARIABLE_TYPE_INT:
                    sli(plcTarget, lvar.name, gli(plc, lvar.name));
                    break;
                case VARIABLE_TYPE_STRING:
                    sls(plcTarget, lvar.name, gls(plc, lvar.name));
                    break;
                case VARIABLE_TYPE_OBJECT:
                    slo(plcTarget, lvar.name, glo(plc, lvar.name));
                    break;
                case VARIABLE_TYPE_LOCATION:
                    sll(plcTarget, lvar.name, gll(plc, lvar.name));
                    break;
            }
            lvar = GetNextLocalVariable(lvar);
        }

        SetName(plcTarget, GetName(plc));
        SetDescription(plcTarget, GetDescription(plc));
        slo(oPC, "PLC", plcTarget);
        Destroy(plc, 0.1);
    }

    else if (sParam == "HPCUR")
    {
        int hp = GetCurrentHitPoints(oParam);
        msg(oPC, "hp: " + itos(hp));
    }

    else if (sParam == "HPMAX")
    {
        int hp = GetMaxHitPoints(oParam);
        msg(oPC, "Maxhp: " + itos(hp));
    }

    else if (sParam == "TBAR2")
    {
        NWNXFuncs_StartTimingbar(oPC, 5000, 1);
    }

    else if (sParam == "GETARMORAPPR")
    {
        string appr = C1 + "Armor appearance:" + C2;

        int model = 0;

        for (model = 0; model < ITEM_APPR_ARMOR_NUM_MODELS; model++)
        {
            int value = GetItemAppearance(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, model);
            string name = IDS_ModelToString(ITEM_APPR_TYPE_ARMOR_MODEL, model);
            appr += "\n  " + name + " = " + itos(value);
        }

        appr += C1 + "\nArmor color:" + C2;

        for (model = 0; model < ITEM_APPR_ARMOR_NUM_COLORS; model++)
        {
            int value = GetItemAppearance(oParam, ITEM_APPR_TYPE_ARMOR_COLOR, model);
            string name = IDS_ModelToString(ITEM_APPR_TYPE_ARMOR_COLOR, model);
            appr += "\n  " + name + " = " + itos(value);
        }

        appr += _C;
        msg(oPC, appr);
    }

    else if (sParam == "ADDARMORAPPR")
    {
        int value = GetItemAppearance(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, nParam);
        object copy = CopyItemAndModify(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, nParam, ++value, TRUE);
        string model = IDS_ModelToString(ITEM_APPR_TYPE_ARMOR_MODEL, nParam);
        Destroy(oParam);
        DelayCommand(0.2, AssignCommand(oPC, ActionEquipItem(copy, INVENTORY_SLOT_CHEST)));
        msg(oPC, "new armor appr: " + model + " = " + itos(value));
    }

    else if (sParam == "SUBARMORAPPR")
    {
        int value = GetItemAppearance(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, nParam);
        object copy = CopyItemAndModify(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, nParam, --value, TRUE);
        string model = IDS_ModelToString(ITEM_APPR_TYPE_ARMOR_MODEL, nParam);
        Destroy(oParam);
        DelayCommand(0.2, AssignCommand(oPC, ActionEquipItem(copy, INVENTORY_SLOT_CHEST)));
        msg(oPC, "new armor appr: " + model + " = " + itos(value));
    }

    else if (sParam == "ADDARMORAPPRX")
    {
        int value = GetItemAppearance(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, nParam);
        SetItemAppearance(oParam, nParam, ++value);
        msg(oPC, "new armor apprX: model " + itos(nParam) + " value " + itos(value));
    }

    else if (sParam == "SUBARMORAPPRX")
    {
        int value = GetItemAppearance(oParam, ITEM_APPR_TYPE_ARMOR_MODEL, nParam);
        SetItemAppearance(oParam, nParam, --value);
        msg(oPC, "new armor apprX: model " + itos(nParam) + " value " + itos(value));
    }

    else if (sParam == "SETQTY")
    {
        ACS_SetWeightedItemQuantity(oParam, nParam);
    }

    else if (sParam == "GETQTY")
    {
        int qty = ACS_GetItemStackSizeForItem(oParam);
        msg(oPC, "qty : " + itos(qty));
    }

    else if (sParam == "OBJVIS")
    {
        msg(oPC, "Visibility of " + GetTag(oParam) + " set to " + itos(nParam));
        NWNXFuncs_SetVisibility(oPC, oParam, nParam);
    }

    else if (sParam == "BIND")
    {
        if (!GetIsObjectValid(oParam)) return;
        if (oParam == oPC)
        {
            slo(oPC, "dbgParent", OBJECT_INVALID, -1, SYS_M_DELETE);
            msg(oPC, "Parent unset");
            return;
        }

        if (GetObjectType(oParam) != OBJECT_TYPE_ITEM) return;

        object parent = glo(oPC, "dbgParent");
        if (!GetIsObjectValid(parent))
        {
            slo(oPC, "dbgParent", oParam);
            msg(oPC, "Parent: " + GetName(oParam));
        }
        else
        {
            slo(oPC, "dbgParent", OBJECT_INVALID, -1, SYS_M_DELETE);
            SetParentObject(oParam, parent);
            msg(oPC, "Parent: " + GetName(parent) + " \nChild: " + GetName(oParam));
            string parentDesc = GetDescription(parent, FALSE, FALSE);
            string childDesc = GetDescription(oParam, FALSE, FALSE);
            parentDesc = sub(parentDesc, 0, 6) + " | " + sub(parentDesc, 6, 6) + " | " + sub(parentDesc, 12, 10) + " | " + sub(parentDesc, 22, 6) + " | " + sub(parentDesc, 28, 6) + " | " + sub(parentDesc, 34, 6) + " | " + sub(parentDesc, 40, 6) + " | " + sub(parentDesc, 46, 2) + " | " + sub(parentDesc, 48, 6) + " | " + sub(parentDesc, 54, 6) + " | " + sub(parentDesc, 60, 9999);
            childDesc = sub(childDesc, 0, 6) + " | " + sub(childDesc, 6, 6) + " | " + sub(childDesc, 12, 10) + " | " + sub(childDesc, 22, 6) + " | " + sub(childDesc, 28, 6) + " | " + sub(childDesc, 34, 6) + " | " + sub(childDesc, 40, 6) + " | " + sub(childDesc, 46, 2) + " | " + sub(childDesc, 48, 6) + " | " + sub(childDesc, 54, 6) + " | " + sub(childDesc, 60, 9999);
            msg(oPC, "ParentD: " + parentDesc);
            msg(oPC, "ChildD: " + childDesc);
        }
    }
    else if (sParam == "OTOS")
    {
        msg(oPC, "Object [" + GetName(oParam) + " / " + GetPCPlayerName(oParam) + " / " + GetTag(oParam) + "] to string: " + ObjectToString(oParam));
    }

    else if (sParam == "PGBAR_S")
    {
        msg(oPC, "Starting timing bar for " + GetPCPlayerName(oParam) + " for " + itos(nParam) + " seconds...");
        AssignCommand(oParam, PlayAnimation(ANIMATION_LOOPING_GET_LOW, 1.0f, itof(nParam) + 5.0f));
        TimingBarStart(oParam, nParam, FALSE, "debug_timebar", "debug_timebar_x");
    }

    else if (sParam == "PGBAR_X")
    {
        msg(oPC, "Cancelling timing bar for " + GetPCPlayerName(oParam) + "...");
        TimingBarCancel(oParam);
    }

    else if (sParam == "VFXEXT")
    {
        msg(oPC, "Applying " + itos(nParam) + " effect for PC " + GetPCPlayerName(oParam) + " at custom position...");
        AreaVisualEffectForPC(oParam, nParam, GetPositionFromLocation(lParam));
    }

    else if (sParam == "EQUIP")
    {
        AssignCommand(oPC, ActionEquipItem(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC), INVENTORY_SLOT_RIGHTHAND));
    }

    else

    if (sParam == "EVENT_LOG")
    {
        object oOES = GetObjectByTag("SYSTEM_OES_OBJECT");
        int bLog = gli(oOES, "OES_EVENT_LOG", -1, SYS_M_DELETE);

        if (!bLog)
            sli(oOES, "OES_EVENT_LOG", TRUE);
    }

    else

    if  (sParam ==  "EX")
    {
        location    loc =   GetLocation(oPC);
        vector      vec =   GetPositionFromLocation(loc);
        object  plc   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

        SetDescription(plc, "abc123");
        AssignCommand(oPC, ActionExamine(plc));
        DelayCommand(3.0f, DestroyObject(plc));
    }

    else

    if  (sParam ==  "PATTERN")
    {
        string  pattern  =   gls(oPC, "pattern");
        string  str  =   gls(oPC, "string");

        msg(oPC, "String " + str + " against pattern " + pattern + " = " + itos(TestStringAgainstPattern(pattern, str)));
    }

    else

    if  (sParam ==  "EFF")
    {
        //hunger
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityIncrease(ABILITY_CHARISMA, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityDecrease(ABILITY_CHARISMA, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSavingThrowIncrease(SAVING_THROW_WILL, 1, SAVING_THROW_TYPE_NONE), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSavingThrowDecrease(SAVING_THROW_ALL, 1, SAVING_THROW_TYPE_NONE), oPC);

        //health
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityIncrease(ABILITY_WISDOM, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityDecrease(ABILITY_WISDOM, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSavingThrowIncrease(SAVING_THROW_FORT, 1, SAVING_THROW_TYPE_NONE), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSpellFailure(1), oPC);

        //thirst
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityIncrease(ABILITY_INTELLIGENCE, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityDecrease(ABILITY_INTELLIGENCE, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSavingThrowIncrease(SAVING_THROW_REFLEX, 1, SAVING_THROW_TYPE_NONE), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSpellLevelAbsorption(1), oPC);

        //fatigue
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityIncrease(ABILITY_DEXTERITY, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityDecrease(ABILITY_DEXTERITY, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSkillIncrease(0, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSkillDecrease(0, 1), oPC);

        //inventory
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityIncrease(ABILITY_CONSTITUTION, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAbilityDecrease(ABILITY_CONSTITUTION, 1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSpellResistanceIncrease(1), oPC);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectSpellResistanceDecrease(1), oPC);
    }

    else

    if  (sParam ==  "FEATS")
    ResetFeats(oParam);

    else

    if  (sParam ==  "ADDFEAT")
    {
        AddKnownFeat(oPC, nParam);
        msg(oPC, "AddFeat: " + itos(nParam));
    }

    else

    if  (sParam ==  "TESTAREA")
    AssignCommand(oParam, JumpToLocation(GetLocation(GetObjectByTag("VG_W_WP_NORMAL01_I_00_TESTARE_01"))));

    else

    if (sParam == "SIT")
    {
        object  oParam  =   CCS_GetConvertedObject(sLine, 2, FALSE);
        AssignCommand(oPC, ActionSit(oParam));
    }

    else

    if  (sParam ==  "SP")
    {
        ExecuteScript("debug_exec", OBJECT_SELF);
    }

    else

    if  (sParam ==  "USG")
    {
        TIS_UpdateSpawnGroup("P_PLANTS");
    }

    else

    if  (sParam ==  "BOOST")
    {
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectModifyAttacks(2), oParam);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAttackIncrease(30), oParam);
        SetMaxHitPointsByLevel(oParam, 1, 200);
        SetCurrentHitPoints(oParam, 300);
        SetACNaturalBase(oParam, 25);
        ACS_CreateItemUsingRecipe(100, 1, li(), oParam, TRUE, 1, 1, 1, 1, 1); //  1x drevsek
        ACS_CreateItemUsingRecipe(112, 1, li(), oParam, TRUE, 1, 1, 1, 1, 1); //  1x krumpac
        ACS_CreateItemUsingRecipe(198, 1, li(), oParam, TRUE, 1, 1, 1, 1, 1); //  1x kosak
        ACS_CreateItemUsingRecipe(200, 1, li(), oParam, TRUE, 1, 1, 1, 1, 1); //  1x stahovaci nuz
        ACS_CreateItemUsingRecipe(113, 3, li(), oParam, TRUE, 1, 1, 1, 1, 1); //  4x kresadlo
        ACS_CreateItemUsingRecipe(128, 5, li(), oParam, TRUE, 1, 1, 1, 1, 1); //  5x pochoden
        ACS_GiveMoney(oParam, 600); //  600 penez
        //sli(oParam, "ROCK_LEVEL", 2);
        //sli(oParam, "TREE_LEVEL", 2);
        //sli(oParam, "PLANT_LEVEL", 2);
        //sli(oParam, "MUSHROOM_LEVEL", 2);
        //sli(oParam, "BODY_LEVEL", 2);
    }

    else

    if  (sParam ==  "PORTRAIT")
    {
        msg(oPC, "Portrait ID: " + itos(GetPortraitId(oParam)) + " ResRef: " + GetPortraitResRef(oParam), TRUE, FALSE);
    }

    else

    if  (sParam ==  "INTER")
    {
        if  (!GetIsObjectValid(oParam)) return;

        if  (GetIsOpen(oParam)) return;

        SetUseableFlag(oParam, TRUE);
        ActionInteractObject(oParam);
        ActionDoCommand(SetUseableFlag(oParam, FALSE));
        DelayCommand(1.0f, ActionExamine(GetFirstItemInInventory(oPC)));
    }

    else

    if  (sParam ==  "STORE")
    {
        object  oStore  =   GetNearestObject(OBJECT_TYPE_STORE, oPC, 1);

        if  (!GetIsObjectValid(oStore))
        {
            msg(oPC, "No stores nearby...", TRUE);
            return;
        }

        msg(oPC, "Opening store: " + GetTag(oStore), TRUE, FALSE);
        OpenStore(oStore, oPC);
    }

    else

    if  (sParam ==  "DUMP")
    {
        msg(oPC, "Object [" + GetTag(oParam) + "] Vars:\n" + DumpLocalVariables(oParam));
    }

    else

    if  (sParam ==  "WEIGHT")
    {
        int nPerc   =   100 - abs(ftoi(itof(GetWeight(oPC) / 10) / (GetAbilityScore(oPC, ABILITY_STRENGTH) * 3000) * 100));
        int nAbil = GetAbilityScore(oPC, ABILITY_STRENGTH) * 3000;
        int nWeig = GetWeight(oPC) / 10;
        int nResul = abs(ftoi(itof(nWeig) / nAbil * 100));
        float x = itof(GetWeight(oPC) / 10) / (GetAbilityScore(oPC, ABILITY_STRENGTH) * 3000);

        msg(oPC, "perc: " + itos(nPerc) + " abil: " + itos(nAbil) + " weig: " + itos(nWeig) + " res: " + itos(nResul) + " x: " + ftos(x));
    }

    else

    if  (sParam ==  "ANIM")
    {
        AssignCommand(oPC, ActionPlayAnimation(nParam, 1.0f, (fParam == 0.0f) ? 5.0f : fParam));
    }

    else

    if  (sParam ==  "SB")
    {
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectModifyAttacks(3), oPC);
    }

    else

    if  (sParam ==  "RB")
    {
        RemoveSpecificEffect(oPC, nParam);
    }

    else

    if  (sParam ==  "RFIND")
    {
        SpawnScriptDebugger();
        ACS_GetTargetHasRecipe(oPC, 123);
    }

    else

    if  (sParam ==  "HASINV")
    {
        msg(oPC, "Target [" + GetTag(oParam) + "] has inventory: " + itos(GetHasInventory(oParam)));
    }

    else

    if  (sParam ==  "AREATRANS")
    {
        msg(oPC, "AREATRANS");

        object  oTriger;
        int     n;
        int     c;
        vector  pos;

        oTriger =   GetObjectByTag("VG_T_AT_TRANS_01", n = 0);

        while   (GetIsObjectValid(oTriger))
        {
            if  (!GetIsObjectValid(GetTransitionTarget(oTriger)))
            {
                pos         =   GetPosition(oTriger);
                sMessage    =   R1 + "AT UNSET: " + W2 + "#" + itos(++c) + " - " + Y1 + GetTag(GetArea(oTriger)) + R1 + " [" + W2 + itos(ftoi(pos.x)) + R1 + ", " + W2 + itos(ftoi(pos.y)) + R1 + "]";
                msg(oPC, sMessage, FALSE, FALSE);
            }

            oTriger =   GetObjectByTag("VG_T_AT_TRANS_01", ++n);
        }
    }


    /*
    msg(oPC, "Target: " + GetTag(CCS_GetConvertedObject(sLine, 1, FALSE)));

    object  c   =   CreateObject(OBJECT_TYPE_CREATURE, "test_panter", CCS_GetConvertedLocation(sLine));

    DelayCommand(fParam, AssignCommand(c, ActionMoveToLocation(GetLocation(oPC))));
    DestroyObject(c, fParam + 2.0f);

    PlaySound("il2_event_done");
    */

    /*
    SpawnScriptDebugger();
    object  oObj    =   ACS_CreateItemUsingRecipeOnObject(100, oPC, 3, 2);
    object  o1      =   ACS_FindItemUsingRecipeOnObject(100, oPC, 3, 1);
    object  o2      =   ACS_FindItemUsingRecipeOnObject(100, oPC, 3, 2);

    msg(oPC, "Created [" + GetTag(oObj) + "]");
    msg(oPC, "Find1 [" + GetTag(o1) + "]");
    msg(oPC, "Find2 [" + GetTag(o2) + "]");
    msg(oPC, "Desc: " + GetDescription(oObj, FALSE, FALSE));
    */

    //msg(oPC, "<c�00>Text <c0�0>Text <c00�>Text</c> Text </c>Text </c>Text </c>Text");
    //msg(oPC, "<c�00>Text <c0�0>Text </c><c00�>Text</c> Text </c>Text </c>Text </c>Text");

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
