#include "vg_s0_sys_core_t"

void main()
{
    object store = OBJECT_SELF;
    object player = GetLastClosedBy();

    msg(player, "Closing store: " + GetTag(store));
}
