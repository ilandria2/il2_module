// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channels init                    |
// | File    || vg_s1_ccs_chanel.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 10-03-2010                                                     |
// | Updated || 10-03-2010                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript pro inicializaci kanalu mluvy
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sChannel, sDescription;

//  ----------------------------------------------------------------------------
//  Kanal "HELP"

    sChannel        =   "BUG";
    sDescription    =   "Inserts a bug entry to the database";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "HELP";
    sDescription    =   "Inserts a help-request entry to the database and sends a message to all DM / PM in the game";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "DESCSELF";
    sDescription    =   "Changes the description of your character";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "DESC";
    sDescription    =   "Changes the appearance indicator of your character (max 40 chars)";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "PM";
    sDescription    =   "Monitored role playing (RP) personal message (PM)";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "TALK";
    sDescription    =   "Normal talk within 15 meters";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "K";
    sDescription    =   "Shout within 30 meters (Alternative: Type <space> + <text>)";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "NAME";
    sDescription    =   "Introduces your character to some other player character under a specific name\n'/name 0' deletes one name\n'/name h' or '/name hide' hides the identity of your character)";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "AFK";
    sDescription    =   "Switches the AFK (Away From Keyboard) mode (ON/OFF)";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "HI";
    sDescription    =   "Starts the conversation with the nearest NPC";

    CCS_InitChannel(sChannel, sDescription);

    sChannel        =   "BYE";
    sDescription    =   "Ends the current conversation";

    CCS_InitChannel(sChannel, sDescription);

//  Kanal "HELP"
//  ----------------------------------------------------------------------------
}
