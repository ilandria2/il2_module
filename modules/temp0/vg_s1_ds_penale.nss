// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Penalize player script                       |
// | File    || vg_s1_ds_penale.nss                                            |
// | Author  || VirgiL                                                         |
// | Created || 01-07-2008                                                     |
// | Updated || 30-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery da postih hraci pri navratu ze sferu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_core_c"
#include    "vg_s0_xps_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    int     nLevel  =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(XPS_M_CLASS_XPS_PLAYER_CHARACTER) + "_LEVEL");
    int     nLevXP  =   XPS_GetXPByLevel(nLevel, XPS_M_CLASS_XPS_PLAYER_CHARACTER);
    int     nLev2XP =   XPS_GetXPByLevel(nLevel + 1, XPS_M_CLASS_XPS_PLAYER_CHARACTER);
    int     nValue  =   ftoi((itof(nLev2XP - nLevXP) / 100) * DS_XP_PERCENT_PENALTY_XP_CHARACTER);

    XPS_AlterXP(oPC, XPS_M_CLASS_XPS_PLAYER_CHARACTER, -nValue);

    /*
    effect  eApply;

    //eApply  =   EffectMovementSpeedDecrease(50);
    eApply  =   EffectLinkEffects(EffectAttackDecrease(10), eApply);
    eApply  =   EffectLinkEffects(EffectACDecrease(10), eApply);
    eApply  =   EffectLinkEffects(EffectSpellFailure(50), eApply);

    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, MagicalEffect(eApply), oPC, 900.0f);

    if  (GetHitDice(oPC)    >=  DS_PENALE_LEVEL_LIMIT)
    {
        //int     nXP     =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(XPS_M_CLASS_XPS_PLAYER_CHARACTER) + "_XP");
        int     bType   =   gli(oPC, SYSTEM_DS_PREFIX + "_KILLER", -1, SYS_M_DELETE);
        int     nXPType =   XPS_M_CLASS_XPS_PLAYER_CHARACTER;
        int     nLevel, nLevXP, nLev2XP, nValue;
        float   fConst;

        while   (nXPType    <=  XPS_M_CLASS_LAST)
        {
            if  (nXPType    ==  XPS_M_CLASS_XPS_PLAYER_CHARACTER)
            fConst  =   DS_XP_PERCENT_PENALTY_XP_CHARACTER;

            else

            if  (nXPType    <=  XPS_M_CLASS_SCS_LAST)
            fConst  =   DS_XP_PERCENT_PENALTY_XP_SCS;

            else

            if  (nXPType    <=  XPS_M_CLASS_BCS_LAST)
            fConst  =   DS_XP_PERCENT_PENALTY_XP_BCS;

            fConst  *=  (bType  ==  2)  ?    DS_XP_PERCENT_PENALTY_KILLER_PC    :   1.0f;

            nLevel  =   gli(oPC, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPType) + "_LEVEL");
            nLevXP  =   XPS_GetXPByLevel(nLevel, nXPType);
            nLev2XP =   XPS_GetXPByLevel(nLevel + 1, nXPType);
            nValue  =   ftoi((itof(/*nXP#/nLev2XP - nLevXP) / 100) * fConst);

            XPS_AlterXP(oPC, nXPType, -nValue, FALSE, TRUE);

            nXPType++;
        }
    }
    */
}
