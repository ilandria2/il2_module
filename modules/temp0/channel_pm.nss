// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_pm.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "PM"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include "vg_s0_sys_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "PM";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    SYS_Info(oPC, SYSTEM_CCS_PREFIX, "USE_TELL");
    SetTargetParameters(oPC, "vg_s1_ccs_tell", sText);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
