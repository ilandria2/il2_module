// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_kick.nss                                                   |
// | Author  || VirgiL                                                         |
// | Created || 23-07-2009                                                     |
// | Updated || 23-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "KICK"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "KICK";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    object  oPlayer =   CCS_GetConvertedObject(sLine, 1, FALSE);
    string  sMessage;

    if  (!GetIsObjectValid(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type - must be a player character";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "Player " + G2 + GetName(oPlayer) + " (" + GetPCPlayerName(oPlayer)+ ")" + W2 + " was kicked from the game server";

    BootPC(oPlayer);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
