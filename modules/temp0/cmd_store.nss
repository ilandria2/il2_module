// STORE console command script

#include "vg_s0_ccs_core_t"
#include "vg_s0_ets_core"
#include "vg_s0_sys_core_d"

void main()
{
    string sCmd = "STORE";
    string sLine = CCS_GetCommandLine(TRUE);
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int bLog = gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object player = OBJECT_SELF;

    if (bLog)
    {
        int nId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

    string sAction = CCS_GetConvertedString(sLine, 1, FALSE);
    string sResRef = CCS_GetConvertedString(sLine, 2, FALSE);
    string sOwner = CCS_GetConvertedString(sLine, 3, FALSE);
    location loc = CCS_GetConvertedLocation(sLine, 4, FALSE);
    sAction = upper(sAction);

    if (sAction != "ADD" && sAction != "DELETE" && sAction != "OPEN")
    {
        msg(player, R1 + "Invalid Action parameter value - Use either ADD or DELETE", TRUE);
        return;
    }

    if (sAction == "OPEN")
    {
        object store = GetNearestObjectToLocation(OBJECT_TYPE_STORE, GetLocation(player));

        if (!GetIsObjectValid(store) || GetDistanceBetween(player, store) > 10.0f)
        {
            msg(player, Y1 + "No valid store object within 10 meters from your position.");
            return;
        }

        OpenStore(store, player);
    }

    if (sResRef == "")
    {
        msg(player, R1 + "No template ResRef specified.", TRUE);
        return;
    }

    if (sOwner == "")
    {
        msg(player, R1 + "No owner Tag specified.", TRUE);
        return;
    }

    if (!GetIsObjectValid(GetAreaFromLocation(loc)))
    {
        msg(player, R1 + "Invalid target location.", TRUE);
        return;
    }

    if (sAction == "ADD")
    {
        object store = CreateObject(OBJECT_TYPE_STORE, sResRef, loc);

        if (!GetIsObjectValid(store))
        {
            msg(player, R1 + "Error creating store object by ResRef: " + R2 + sResRef, TRUE);
            return;
        }

        sls(store, ETS_ + "STORE_OWNER", sOwner);
        SavePersistentData(store, ETS, "ADD");

        int storeId = gli(store, ETS_ + "STORE_ID");

        msg(player, G1 + "Successfully placed store " + G2 + GetName(store) + G1 + " [" + G2 + GetTag(store) + G1 + "] ID [" + G2 + itos(storeId) + G1 + "] at target location.");
        logentry("[" + ETS + "] Store " + sResRef + " was successfully created at " + LocationToString(loc));
    }

    else if (sAction == "DELETE")
    {
        int storeId = CCS_GetConvertedInteger(sLine, 2, FALSE);
        object store = glo(__ETS(), ETS_ + "STORE", storeId);

        if (!GetIsObjectValid(store))
        {
            SYS_Info(player, ETS, "INVALID_STORE_ID");
            return;
        }

        SavePersistentData(store, ETS, "DELETE");
        SetPlotFlag(store, FALSE);
        DestroyObject(store, 0.1f);

        msg(player, Y1 + "Successfully deleted store ID " + Y2 + itos(storeId));
    }
}

