// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Cast event script                      |
// | File    || vg_s3_scs_spcast.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-05-2008                                                     |
// | Updated || 14-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosti "SCS CastSpell"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_const"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_scs_core_m"
#include    "x2_inc_switches"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{   //SpawnScriptDebugger();
    int nSpell  =   SCS_GetSpellId();

    //  kouzla iprp_onhit
    object  oItem   =   GetSpellCastItem();
    object  oTarget =   GetSpellTargetObject();

    if  (oItem  !=  OBJECT_INVALID)
    {
        int bLoop   =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_ON_HIT_SPELL_LOOP");

        if  (!bLoop)
        {
            itemproperty    ipTemp  =   GetFirstItemProperty(oItem);
            int     nType, nSubType, nItemSpell, bInit, bHarmful;
            string  sImpact;

            while   (GetIsItemPropertyValid(ipTemp))
            {
                nType   =   GetItemPropertyType(ipTemp);

                if  (nType  ==  ITEM_PROPERTY_ONHITCASTSPELL)
                {
                    //  prve OnHit kouzlo funguje samo, netreba ho manualne poustet
                    if  (!bInit)
                    {
                        bInit   =   TRUE;

                        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_ON_HIT_SPELL_LOOP", TRUE);
                    }

                    else
                    {
                        nSubType    =   GetItemPropertySubType(ipTemp);
                        nItemSpell  =   stoi(Get2DAString("iprp_onhitspell", "SpellIndex", nSubType));
                        bHarmful    =   stoi(Get2DAString("spells", "HostileSetting", nItemSpell));
                        sImpact     =   Get2DAString("spells", "ImpactScript", nItemSpell);

                        ExecuteScript(sImpact, OBJECT_SELF);
                        SignalEvent(oTarget, EventSpellCastAt(OBJECT_SELF, nItemSpell, bHarmful));
                    }
                }

                ipTemp  =   GetNextItemProperty(oItem);
            }

            sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_ON_HIT_SPELL_LOOP", -1, -1, SYS_M_DELETE);
        }

        return;
    }

    //  kouzla DOMAIN_POWERS
    if  (TestStringAgainstPattern("**DOMAIN**", Get2DAString("feat", "Label", stoi(Get2DAString("spells", "FeatID", nSpell))))) return;

    //  DMko muze kouzlit bez omezeni
    if  (GetIsDM(OBJECT_SELF))  return;

    location    lTarget     =   GetSpellTargetLocation();
    int         nConjMode   =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONJURE_MODE");
    int         nMana       =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA");
    int         nManaUsed   =   SCS_CalculateUsedMana(OBJECT_SELF, nSpell);
    int         nManaMax    =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA_MAX");
    string      sSpellName  =   GetStringByStrRef(stoi(Get2DAString("spells", "Name", nSpell)));

    ExecuteScript(SCS_NCS_CONDITION_RESREF, OBJECT_SELF);

    int     bCondition  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", -1, SYS_M_DELETE);
    int     bMana       =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT_MANA", -1, SYS_M_DELETE);

    //  nesplnil nejakou podminku (mana, znalost kouzla, ...)
    if  (!bCondition)
    {
        SetModuleOverrideSpellScriptFinished();
        DelayCommand(0.5f, AssignCommand(OBJECT_SELF, ClearAllActions(TRUE)));
    }

    else

    //  selhani kouzla
    if  (bCondition ==  2)
    {
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_FAILURE_PARAMETERS", nSpell);
        ExecuteScript(SCS_NCS_SPELL_FAILURE_RESREF, OBJECT_SELF);
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_FAILURE");
        SetModuleOverrideSpellScriptFinished();
    }

    else

    //  mysticke selhani kouzla
    if  (bCondition ==  3)
    {
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_FAILURE_M_PARAMETERS", nSpell);
        ExecuteScript(SCS_NCS_SPELL_FAILURE_M_RESREF, OBJECT_SELF);
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_FAILURE_M");
        SetModuleOverrideSpellScriptFinished();
    }

    else

    if  (nConjMode  !=  SCS_CONJURE_MODE_NORMAL)
    {
        SetModuleOverrideSpellScriptFinished();
    }

    else    ExecuteScript(SCS_NCS_LEARN_SCHOOL_RESREF, OBJECT_SELF);

    if  (bMana)
    {
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA", nManaUsed, -1, SYS_M_DECREMENT);
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&" + itos(nMana) + "&_&" + itos(nMana - nManaUsed) + "&");
    }

    //  externe zapamatovani kouzleni
    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_CASTS", 1, -1, SYS_M_INCREMENT);
}
