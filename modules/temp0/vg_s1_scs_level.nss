// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Module event script                    |
// | File    || vg_s1_scs_level.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 30-04-2008                                                     |
// | Updated || 15-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Prepocte maximalni uroven many a nastavi ji hraci
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC         =   GetPCLevellingUp();
    int     nManaMaxOld =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX");
    int     nManaMaxNew =   SCS_CalculateMaxMana(oPC);

    sli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX", nManaMaxNew);
    SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_MAX_&" + itos(nManaMaxOld) + "&_&" + itos(nManaMaxNew) + "&");
}
