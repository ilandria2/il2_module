// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) Count items                    |
// | File    || vg_s1_lts_count.nss                                            |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Spocita predmety ve vsech zdrojech pokladu z vyhrazene oblasti
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



int CountItems(object oInventory)
{
    if  (!GetIsObjectValid(oInventory)
    ||  !GetHasInventory(oInventory))   return  -1;

    object  oTemp   =   GetFirstItemInInventory(oInventory);
    int     nResult;

    while   (GetIsObjectValid(oTemp))
    {
        nResult++;
        oTemp   =   GetNextItemInInventory(oInventory);
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oArea   =   GetObjectByTag(LTS_TAG_TREASURE_AREA);
    object  oObject =   GetFirstObjectInArea(oArea);
    string  sTag    =   left(GetTag(oObject), 16);

    while   (GetIsObjectValid(oObject))
    {
        if  (sTag   ==  LTS_TAG_TREASURE_CONTAINER)
        {
            int bComposed   =   gli(oObject, SYSTEM_LTS_PREFIX + "_COMPOSED_TREASURE");

            //  skladany zdroj poklad
            if  (bComposed)
            {
                int     nNth    =   1;
                string  sPart   =   gls(oObject, SYSTEM_LTS_PREFIX + "_COMPOSED_PART", nNth);
                object  oPart   =   GetNearestObjectByTag(LTS_TAG_TREASURE_CONTAINER + "_" + sPart, oObject);
                int     nTotal;

                while   (GetIsObjectValid(oPart))
                {
                    int nCount  =   CountItems(oPart);

                    sli(oObject, SYSTEM_LTS_PREFIX + "_COMPOSED_PART_" + itos(nNth) + "_COUNT", nCount);

                    nTotal  +=  nCount;
                    sPart   =   gls(oObject, SYSTEM_LTS_PREFIX + "_COMPOSED_PART", ++nNth);
                    oPart   =   GetNearestObjectByTag(LTS_TAG_TREASURE_CONTAINER + "_" + sPart, oObject);
                }

                sli(oObject, SYSTEM_LTS_PREFIX + "_ITEMS_COUNT", nTotal);
            }

            //  obycejny zdroj pokladu
            else
            {
                int nCount  =   CountItems(oObject);

                sli(oObject, SYSTEM_LTS_PREFIX + "_ITEMS_COUNT", nCount);
            }
        }

        oObject =   GetNextObjectInArea(oArea);
        sTag    =   left(GetTag(oObject), 16);
    }
}
