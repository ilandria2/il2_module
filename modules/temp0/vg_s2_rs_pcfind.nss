// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) - PC Finder script                            |
// | File    || vg_s2_rs_pcfind.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Manipulace se seznamem "Mam zajem o..." pro danyho hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"
#include    "vg_s0_rs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oSource =   OBJECT_SELF;
    object  oPC     =   LPD_GetPC();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_RS_PREFIX + "_OBJECT");
    string  sParam  =   gls(oSource, SYSTEM_LPD_PREFIX + "_SCRIPT_PARAMETER", -1, SYS_M_DELETE);
    string  sMessage;

    if(gli(oPC, "rs_pcfind"))SpawnScriptDebugger();
    sParam  =   GetStringUpperCase(sParam);

    //  zobrazit seznam zajemcu
    if  (sParam ==  "DISPLAY")
    {
        //  promazani promennych
        string  sPart;
        int     nMode, nCount, n, nRows;

        /*
        nCount  =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART_S_ROWS");
        n       =   1;

        while   (n  <=  nCount)
        {
            nMode   =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_MODE", n);
            sPart   =   gls(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART", n);

            sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_M_" + itos(nMode) + "_A_" + sPart, -1, -1, SYS_M_DELETE);

            n++;
        }

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_MODE_I_ROWS", -1, -1, SYS_M_DELETE);
        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART_S_ROWS", -1, -1, SYS_M_DELETE);
        */

        //  nacteni zajmu od hracu a ulozeni do promennych
        object  oTemp   =   GetFirstPC();
        object  oArea;

        while   (GetIsObjectValid(oTemp))
        {
            oArea   =   GetArea(oTemp);
            sPart   =   GetTag(oArea);
            nMode   =   gli(oTemp, SYSTEM_RS_PREFIX + "_PCFIND_MODE");
            nCount  =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_M_" + itos(nMode) + "_A_" + sPart);

            if  (!nCount)
            {
                sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_MODE", nMode, -2);
                sls(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART", sPart, -2);
            }

            sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_M_" + itos(nMode) + "_A_" + sPart, nCount + 1);

            oTemp   =   GetNextPC();
        }

        //  nastaveni zpravy
        string  sM_RP, sM_Act, sM_Qst, sM_Xpd, sCtg;

        n       =   1;
        nRows   =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART_S_ROWS");

        while   (n  <=  nRows)
        {
            nMode   =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_MODE", n);
            sPart   =   gls(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART", n);
            nCount  =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_M_" + itos(nMode) + "_A_" + sPart);
            oArea   =   GetObjectByTag(sPart);

            sM_RP   +=  (nMode  !=  RS_PCFIND_MODE_ROLEPLAY)    ?   ""  :   "\n  " + GetName(oArea) + C_NORM + " (" + C_BOLD + "x" + itos(nCount) + C_NORM + ")";
            sM_Act  +=  (nMode  !=  RS_PCFIND_MODE_ACTION)      ?   ""  :   "\n  " + GetName(oArea) + C_NORM + " (" + C_BOLD + "x" + itos(nCount) + C_NORM + ")";
            sM_Qst  +=  (nMode  !=  RS_PCFIND_MODE_QUEST)       ?   ""  :   "\n  " + GetName(oArea) + C_NORM + " (" + C_BOLD + "x" + itos(nCount) + C_NORM + ")";
            sM_Xpd  +=  (nMode  !=  RS_PCFIND_MODE_EXPEDITION)  ?   ""  :   "\n  " + GetName(oArea) + C_NORM + " (" + C_BOLD + "x" + itos(nCount) + C_NORM + ")";

            n++;
        }

        sMessage    =   "";
        nMode       =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_MODE");

        sCtg        =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_ROLEPLAY);
        sMessage    +=  (sM_RP  ==  "") ?   ""  :   C_INTE + sCtg;
        sMessage    +=  (nMode  !=  RS_PCFIND_MODE_ROLEPLAY)    ?   ""  :   C_NORM + " (Z�jem tv� postavy)";
        sMessage    +=  (sM_RP  ==  "") ?   ""  :   ":" + sM_RP + "\n\n";

        sCtg        =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_ACTION);
        sMessage    +=  (sM_Act ==  "") ?   ""  :   C_INTE + sCtg;
        sMessage    +=  (nMode  !=  RS_PCFIND_MODE_ACTION)  ?   ""  :   C_NORM + " (Z�jem tv� postavy)";
        sMessage    +=  (sM_Act ==  "") ?   ""  :   ":" + sM_Act + "\n\n";

        sCtg        =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_QUEST);
        sMessage    +=  (sM_Qst ==  "") ?   ""  :   C_INTE + sCtg;
        sMessage    +=  (nMode  !=  RS_PCFIND_MODE_QUEST)   ?   ""  :   C_NORM + " (Z�jem tv� postavy)";
        sMessage    +=  (sM_Qst ==  "") ?   ""  :   ":" + sM_Qst + "\n\n";

        sCtg        =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_EXPEDITION);
        sMessage    +=  (sM_Xpd ==  "") ?   ""  :   C_INTE + sCtg;
        sMessage    +=  (nMode  !=  RS_PCFIND_MODE_EXPEDITION)  ?   ""  :   C_NORM + " (Z�jem tv� postavy)";
        sMessage    +=  (sM_Xpd ==  "") ?   ""  :   ":" + sM_Xpd + "\n\n";

        sMessage    =   (sMessage   !=  "") ?   sMessage    :   C_NORM + "Seznam z�jm� je " + C_BOLD + "pr�zdn�";

        //  promazani promennych
        nCount  =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART_S_ROWS");
        n       =   1;

        while   (n  <=  nCount)
        {
            nMode   =   gli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_MODE", n);
            sPart   =   gls(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART", n);

            sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_M_" + itos(nMode) + "_A_" + sPart, -1, -1, SYS_M_DELETE);

            n++;
        }

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_MODE_I_ROWS", -1, -1, SYS_M_DELETE);
        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_G_PART_S_ROWS", -1, -1, SYS_M_DELETE);
    }

    else

    //  nastavit na RP
    if  (sParam ==  "SET_RP")
    {
        string  sCtg    =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_ROLEPLAY);

        sMessage    =   C_NORM + "Tv� postava byla p�ips�na do kategorie " + C_BOLD + sCtg;

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_ROLEPLAY);
    }

    else

    //  nastavit na Akce
    if  (sParam ==  "SET_ACT")
    {
        string  sCtg    =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_ACTION);

        sMessage    =   C_NORM + "Tv� postava byla p�ips�na do kategorie " + C_BOLD + sCtg;

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_ACTION);
    }

    else

    //  nastavit na Quest
    if  (sParam ==  "SET_QST")
    {
        string  sCtg    =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_QUEST);

        sMessage    =   C_NORM + "Tv� postava byla p�ips�na do kategorie " + C_BOLD + sCtg;

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_QUEST);
    }

    else

    //  nastavit na Vyprava
    if  (sParam ==  "SET_XPD")
    {
        string  sCtg    =   gls(oSystem, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_EXPEDITION);

        sMessage    =   C_NORM + "Tv� postava byla p�ips�na do kategorie " + C_BOLD + sCtg;

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_EXPEDITION);
    }

    else

    //  odepsat
    if  (sParam ==  "REMOVE")
    {
        sMessage    =   C_NORM + "Tv� postava byla " + C_BOLD + "odeps�na" + C_NORM + " ze seznamu z�jm�.";

        sli(oPC, SYSTEM_RS_PREFIX + "_PCFIND_MODE", RS_PCFIND_MODE_NONE);
    }

    if  (sMessage   !=  "")
    sls(oPC, SYSTEM_LPD_PREFIX + "_TALK", sMessage, -2);
}
