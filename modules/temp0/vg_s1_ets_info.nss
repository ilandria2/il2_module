// Exchange Trade Stores - Information script

#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_ets_core"

void main()
{
    string sTemp = gls(OBJECT_SELF, ETS_ + "NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    string sCase = GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);
    string sMessage;
    int bFloat;

    if (sCase == "INVALID_STORE_ID")
    {
        sMessage = R1 + "Invalid Store ID";
        bFloat = TRUE;
    }

    else if (sCase == "RECIPE_FAILED")
    {
        sMessage = R1 + "Error when creating item using recipe.";
        bFloat = TRUE;
    }

    else if (sCase == "BALANCE")
    {
        object player = OBJECT_SELF;
        object store = glo(player, ETS_ + "TARGET");
        int boughtItems = gli(player, ETS_ + "CART_O_ROWS");
        int soldItems = gli(store, ETS_ + "CART_O_ROWS");
        int balance = ETS_GetBalance(player, store);
        string color = balance < 0 ? R1 : balance > 0 ? G1 : Y1;

        sMessage = W2 + "Balance: " + color + itos(balance) + " coins";
    }

    else if (sCase == "NOT_ENOUGH_MONEY")
    {
        int money = stoi(GetNthSubString(sTemp, 1));
        int balance = stoi(GetNthSubString(sTemp, 2));
        int confirm = gli(OBJECT_SELF, ETS_ + "CONFIRM_MONEY");

        if (confirm)
            sMessage = R1 + "Store does not have " + Y1 + itos(balance) + " coins\n" + W2 + "Accept only " + Y1 + itos(money) + " coins" + W2 + "?";
        else
            sMessage = R1 + "You have " + Y1 + itos(money) + " coins" + R1 + " but you need " + Y1 + itos(balance) + R1 + " coins.";

        bFloat = TRUE;
    }

    else if (sCase == "TRANSACTION_ACCEPTED")
    {
        bFloat = TRUE;
        sMessage = G1 + "Transaction accepted!";
    }

    else if (sCase == "TRANSACTION_CANCELED")
    {
        bFloat = TRUE;
        sMessage = Y1 + "Transaction canceled!";
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}

