// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_damage.nss                                                 |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "DAMAGE"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "DAMAGE";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nAmount =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     nType   =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    int     nPower  =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    object  oTarget =   CCS_GetConvertedObject(sLine, 4, FALSE);
    string  sMessage;

    //  neplatny cil
    if  (!GetIsObjectValid(oTarget))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid target";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatne mnozstvi
    if  (nAmount    <=  0)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid amount of damage";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny typ
    if  (nType      <=  0)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid damage type";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatna sila
    if  (nPower     <   0)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid damage power";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "Applied [" + G2 + itos(nAmount) + W2 + "] damage of type [" + G2 + itos(nType) + W2 + "] to object [" + G2 + GetName(oTarget) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(nAmount, nType, nPower), oTarget);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
