// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Core T                           |
// | File    || vg_s0_ccs_core_t.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-08-2008                                                     |
// | Updated || 17-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu zkusenosti (CCS) typu "Text"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "x0_i0_position"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetIsChannel                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud retezec sLine je kanal mluvy
//
//  //channel text_mluvy    =   text
//  /channel text_mluvy     =   channel
//  channel text_mluvy      =   text
//
// -----------------------------------------------------------------------------
int     CCS_GetIsChannel(string sLine);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetChannel                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati retezec kanalu mluvy z retezce sLine:
//
//  /channel text_mluvy...
//  _^^^^^^^______________
//
// -----------------------------------------------------------------------------
string  CCS_GetChannel(string sLine);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetChannelText                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati retezec textu mluvy z retezce sLine:
//
//  /channel text_mluvy...
//  _________^^^^^^^^^^^^^
//
// -----------------------------------------------------------------------------
string  CCS_GetChannelText(string sLine);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetIsCommand                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud retezec sLine je prikaz
//
//  //command param1;param2     =   text
//  /command param1;param2      =   command
//  command param1;param2       =   text
//
// -----------------------------------------------------------------------------
int     CCS_GetIsCommand(string sLine);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetCommand                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati retezec prikazu z retezce sLine:
//
//  /command param1;param2;...
//  _^^^^^^^__________________
//
// -----------------------------------------------------------------------------
string  CCS_GetCommand(string sLine);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetParameters                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati retezec parametru z retezce sLine:
//
//  /command param1;param2;...
//  _________^^^^^^^^^^^^^^^^^
//
// -----------------------------------------------------------------------------
string  CCS_GetParameters(string sLine);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetNthParameters                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati n-ty parametr z retezce sLine:
//
//  /command param1;param2;...
//  ________________^^^^^^____
//
// -----------------------------------------------------------------------------
string  CCS_GetNthParameter(string sLine, int nNth = 1, int bMerged = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetHasAccess                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud hrac oPC ma pristup k prikazu nebo skupine sGroup
//
// -----------------------------------------------------------------------------
int     CCS_GetHasAccess(object oPC, string sGroup, int bCmd = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_ValidateParameters                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud jsou platne vsechny parametry v retezci sLine pro oPC
//
// -----------------------------------------------------------------------------
int     CCS_ValidateParameters(string sLine, object oPC = OBJECT_SELF);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_ValidateParameter                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati:
//  -1  - sLine neni prikazovy radek
//  0   - spravny
//  1   - nNth < pocet povinnych parametru
//  2   - nNth > pocet povinnych a nepovinnych parametru
//  3   - spatny datovy typ
//  4   - spravny, integracia #TARGET
//
// -----------------------------------------------------------------------------
int     CCS_ValidateParameter(string sLine, int nNth = 1);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_ValidateDataType                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud se datovy typ parametru sParameter zhoduje s nDataType
//
// -----------------------------------------------------------------------------
int     CCS_ValidateDataType(string sParameter, int nDataType);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_InitCommand                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Pro skupinu sGroup inicializuje prikaz sCmd s popisem sDescription
//
// -----------------------------------------------------------------------------
void    CCS_InitCommand(string sGroup, string sCmd, string sDescription, int bLog = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_DefineParameter                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Pro prikaz sCmd zadefinuje parametr sName typu nType na hodnotu sValue
//  s popisem sDescription
//  ** pokud je sValue zadefinovano, pak se stava dany parametr nepovinnym
//  ** po nepovinnym parametru nemuze nasledovat povinny parametr
//
// -----------------------------------------------------------------------------
void    CCS_DefineParameter(string sCmd, string sName, int nType = CCS_DATATYPE_INTEGER, string sValue = "", string sDescription = "");



// +---------++----------------------------------------------------------------+
// | Name    || CCS_InitChannel                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Inicializuje kanal mluvy sChannel s popisem sDescription
//
// -----------------------------------------------------------------------------
void    CCS_InitChannel(string sChannel, string sDescription);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_InitGroup                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Inicializuje skupinu sGroup pod heslem sPassword
//  ** pokud sPassword neni zadano, pak se skupina sGroup stava verejnou
//
// -----------------------------------------------------------------------------
void    CCS_InitGroup(string sGroup, string sPassword = "");



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GrantAccess                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prida pristup hraci oPC pro skupinu sGroup
//
// -----------------------------------------------------------------------------
void    CCS_GrantAccess(object oPC, string sGroup, int bCmd = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetTargetObject                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati objekt cile pri provadeni prikazu
//
// -----------------------------------------------------------------------------
object  CCS_GetTargetObject();



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetTargetLocation                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati objekt cile pri provadeni prikazu
//
// -----------------------------------------------------------------------------
location    CCS_GetTargetLocation();



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetCommandLine                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati docasne lozene parametry prikazu
//
// -----------------------------------------------------------------------------
string  CCS_GetCommandLine(int bMerged = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetConvertedString                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati konverznu hodnotu nNth-teho parametru prikazoveho radku sLine typu
//  "string"
//
// -----------------------------------------------------------------------------
string  CCS_GetConvertedString(string sLine, int nNth = 1, int bMerged = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetConvertedInteger                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati konverznu hodnotu nNth-teho parametru prikazoveho radku sLine typu
//  "int"
//
// -----------------------------------------------------------------------------
int     CCS_GetConvertedInteger(string sLine, int nNth = 1, int bMerged = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetConvertedFloat                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati konverznu hodnotu nNth-teho parametru prikazoveho radku sLine typu
//  "float"
//
// -----------------------------------------------------------------------------
float   CCS_GetConvertedFloat(string sLine, int nNth = 1, int bMerged = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetConvertedObject                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati konverznu hodnotu nNth-teho parametru prikazoveho radku sLine typu
//  "object"
//
// -----------------------------------------------------------------------------
object  CCS_GetConvertedObject(string sLine, int nNth = 1, int bMerged = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_GetConvertedLocation                                       |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati konverznu hodnotu nNth-teho parametru prikazoveho radku sLine typu
//  "location"
//
// -----------------------------------------------------------------------------
location    CCS_GetConvertedLocation(string sLine, int nNth = 1, int bMerged = FALSE, int ignoreObj = FALSE);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                             CCS_GetIsChannel                              |
// +---------------------------------------------------------------------------+



int     CCS_GetIsChannel(string sLine)
{
    if
    (
        TestStringAgainstPattern(CCS_CHANNEL_IDENTIFIER + "**", sLine)  ==  TRUE
    &&  TestStringAgainstPattern("//**", sLine) ==  FALSE
    )

    return  TRUE;
    return  FALSE;
}



// +---------------------------------------------------------------------------+
// |                             CCS_GetChannel                                |
// +---------------------------------------------------------------------------+



string  CCS_GetChannel(string sLine)
{
    if  (CCS_GetIsChannel(sLine))
    {
        int nSpace  =   FindSubString(sLine, " ");

        return  GetStringUpperCase(GetSubString(sLine, FindSubString(sLine, CCS_CHANNEL_IDENTIFIER, 0) + 1, nSpace - 1));
    }

    return  "";
}



// +---------------------------------------------------------------------------+
// |                           CCS_GetChannelText                              |
// +---------------------------------------------------------------------------+



string  CCS_GetChannelText(string sLine)
{
    if  (CCS_GetIsChannel(sLine))
    {
        int nSpace  =   FindSubString(sLine, " ");

        if  (nSpace ==  -1)
        return  "";
        return  GetSubString(sLine, nSpace + 1, GetStringLength(sLine));
    }

    return  "";
}



// +---------------------------------------------------------------------------+
// |                             CCS_GetIsCommand                              |
// +---------------------------------------------------------------------------+



int     CCS_GetIsCommand(string sLine)
{
    if
    (
        TestStringAgainstPattern(CCS_COMMAND_IDENTIFIER + "**", sLine)  ==  TRUE
    &&  TestStringAgainstPattern("//**", sLine) ==  FALSE
    )

    return  TRUE;
    return  FALSE;
}



// +---------------------------------------------------------------------------+
// |                              CCS_GetCommand                               |
// +---------------------------------------------------------------------------+



string  CCS_GetCommand(string sLine)
{
    if  (CCS_GetIsCommand(sLine))
    {
        int nSpace  =   FindSubString(sLine, " ");
        int nCmd    =   FindSubString(sLine, CCS_COMMAND_IDENTIFIER, 0);
        int nSep    =   FindSubString(sLine, CCS_COMMAND_SEPARATOR, nCmd + 1);

        if  (nSpace ==  -1
        &&  nSep    !=  -1)
        return  upper(GetSubString(sLine, nCmd + 1, nSep - 1));
        return  upper(GetSubString(sLine, nCmd + 1, nSpace - 1));
    }

    return  "";
}



// +---------------------------------------------------------------------------+
// |                            CCS_GetParameters                              |
// +---------------------------------------------------------------------------+



string  CCS_GetParameters(string sLine)
{
    if  (CCS_GetIsCommand(sLine))
    {
        int nSpace  =   FindSubString(sLine, " ");

        if  (nSpace ==  -1)
        return  "";
        return  GetSubString(sLine, nSpace + 1, GetStringLength(sLine));
    }

    return  "";
}



// +---------------------------------------------------------------------------+
// |                           CCS_GetNthParameter                             |
// +---------------------------------------------------------------------------+



string  CCS_GetNthParameter(string sLine, int nNth = 1, int bMerged = FALSE)
{
    string  sCmd    =   CCS_GetCommand(sLine);

    if  (sCmd   ==  "") return  "";

    string  sParams =   CCS_GetParameters(sLine);
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     nMax    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
    int     nUsed   =   (sParams    ==  "") ?   0   :   GetCharsCount(CCS_COMMAND_SEPARATOR, sParams);

    if  (nNth   >   nUsed)
    {
        if  (nNth   <=  nMax)
        {
            if  (!bMerged)  return  "";
            else            return  gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nNth) + "_VALUE");
        }

        else
        {
            return  "";
        }
    }

    if  (nNth   <   1)  nNth    =   1;

    int     n, nFind, nStart, nLength;
    string  sRight, sResult;

    nLength =   len(sParams);

    while   (n  <   nNth)
    {
        nFind   =   FindSubString(sParams, CCS_COMMAND_SEPARATOR, nStart);
        sRight  =   sub(sParams, nStart, nLength - nStart);

        if  (nFind  >=  0)  n++;

        else

        if  (nFind  ==  -1) return  "";
        if  (n  ==  nNth)
        {
            sResult =   GetSubString(sParams, nStart, nFind - nStart);
            break;
        }

        nStart  =   nFind + len(CCS_COMMAND_SEPARATOR);
    }

    if  (!bMerged)  return  sResult;

    if  (sResult    ==  "")
    {
        sResult =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nNth) + "_VALUE");
    }

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                             CCS_GetHasAccess                              |
// +---------------------------------------------------------------------------+



int     CCS_GetHasAccess(object oPC, string sGroup, int bCmd = FALSE)
{
    string  sType   =   (bCmd)  ?   "CMD"   :   "GROUP";

    return  gli(oPC, SYSTEM_CCS_PREFIX + "_" + sType + "_" + sGroup + "_ACCESS");
}



// +---------------------------------------------------------------------------+
// |                           CCS_ValidateParameters                          |
// +---------------------------------------------------------------------------+



int     CCS_ValidateParameters(string sLine, object oPC = OBJECT_SELF)
{
    string  sCmd    =   CCS_GetCommand(sLine);

    if  (sCmd   ==  "") return  -1;

    string  sParams =   CCS_GetParameters(sLine);
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     nMax    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
    int     nUsed   =   (sParams    ==  "") ?   0   :   GetCharsCount(CCS_COMMAND_SEPARATOR, sParams);
    int     nLast   =   (nMax   >   nUsed)  ?   nMax    :   nUsed;
    int     n       =   1;
    int     bResult =   TRUE;
    int     bTarget =   FALSE;
    int     bValid;

    while   (n  <=  nLast)
    {
        bValid  =   CCS_ValidateParameter(sLine, n);

        switch  (bValid)
        {
            case    -1: return  -1;                                                                                                     break;
            case    0:  sls(oPC, SYSTEM_CCS_PREFIX + "_INFO", "PARAMETER_VALUE_&"           + itos(n) + "&", -2);                       break;
            case    1:  sls(oPC, SYSTEM_CCS_PREFIX + "_INFO", "NOT_ENOUGH_PARAMETERS_&"     + itos(n) + "&", -2);   bResult =   FALSE;  break;
            case    2:  sls(oPC, SYSTEM_CCS_PREFIX + "_INFO", "MAX_PARAMETERS_OVERRUN_&"    + itos(n) + "&", -2);   bResult =   FALSE;  break;
            case    3:  sls(oPC, SYSTEM_CCS_PREFIX + "_INFO", "WRONG_DATATYPE_&"            + itos(n) + "&", -2);   bResult =   FALSE;  break;
            case    4:  sls(oPC, SYSTEM_CCS_PREFIX + "_INFO", "PARAMETER_VALUE_&"           + itos(n) + "&", -2);   bTarget =   TRUE;   break;
        }

        n++;
    }

    SYS_Info(oPC, SYSTEM_CCS_PREFIX, "PARAMETERS_VALIDATION_&" + sLine + "&_&" + itos(bResult) + "&");

    if  (bResult    &&  bTarget)    bResult =   2;

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                         CCS_ValidateParameter                             |
// +---------------------------------------------------------------------------+



int     CCS_ValidateParameter(string sLine, int nNth = 1)
{
    string  sCmd    =   CCS_GetCommand(sLine);

    //  chybny parametr: neni to prikazovy radek
    if  (sCmd   ==  "")
    return  -1;

    string  sParams =   CCS_GetParameters(sLine);
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     nMin    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MIN");
    int     nMax    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
    int     nUsed   =   (sParams    ==  "") ?   0   :   GetCharsCount(CCS_COMMAND_SEPARATOR, sParams);

    //  chybny parametr: n-ty parametr je povinny a byl ignorovan
    if  (nNth   >   nUsed
    &&  nNth    <=  nMin)
    return  1;

    //  chybny parametr: n-ty parametr je vetsi nez pocet povinnych a
    //  nepovinnych parametru
    if  (nNth   >   nMax
    &&  nUsed   >   0)
    return  2;

    string  sParam      =   CCS_GetNthParameter(sLine, nNth, TRUE);
    int     nDataType   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nNth) + "_TYPE");
    int     bDataType   =   CCS_ValidateDataType(sParam, nDataType);

    //  chybny parametr: pouzita hodnota n-teho parametru ma spatny datovy typ
    if  (!bDataType)    return  3;

    //  validni parametr: n-ty parametr se rovna tokenu "TARGET" - integrace
    //  kouzla SYS_SPELL_TARGET
    if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_TARGET, sParam))
    return  4;

    //  validni parametr
    return  0;
}




// +---------------------------------------------------------------------------+
// |                         CCS_ValidateDataType                              |
// +---------------------------------------------------------------------------+



int     CCS_ValidateDataType(string sParameter, int nDataType)
{
    int bResult;

    switch  (nDataType)
    {
        case    CCS_DATATYPE_INTEGER:   bResult =   TestStringAgainstPattern("(-*n|*n)|"    + CCS_COMMAND_VARIABLE_IDENTIFIER + "**|" + CCS_COMMAND_TOKENS, sParameter);                                            break;
        case    CCS_DATATYPE_FLOAT:     bResult =   TestStringAgainstPattern("(-*n|*n).*n|" + CCS_COMMAND_VARIABLE_IDENTIFIER + "**|" + CCS_COMMAND_TOKENS, sParameter);                                            break;
        case    CCS_DATATYPE_STRING:    bResult =   TRUE;                                                                                                                                                           break;
        default:                        bResult =   TestStringAgainstPattern("obj|"         + CCS_COMMAND_VARIABLE_IDENTIFIER + "**|" + CCS_COMMAND_TOKENS + "|" + CCS_COMMAND_TAG_IDENTIFIER + "**", sParameter);  break;
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                              CCS_InitCommand                              |
// +---------------------------------------------------------------------------+



void    CCS_InitCommand(string sGroup, string sCmd, string sDescription, int bLog = FALSE)
{
    sGroup  =   GetStringUpperCase(FindAndReplace(sGroup, " ", ""));

    //  neinicializuje prikaz pro neplatnou skupinu
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     nId     =   gli(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_ID");

    if  (!nId)  return;

    //  inicializace prikazu
    sCmd    =   FindAndReplace(sCmd, " ", "");

    sls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_CMD", sCmd, -2);
    sli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_INIT", TRUE);
    sls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_DESCRIPTION", sDescription);
    sli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG", bLog);
}



// +---------------------------------------------------------------------------+
// |                            CCS_DefineParameter                            |
// +---------------------------------------------------------------------------+



void    CCS_DefineParameter(string sCmd, string sName, int nType = CCS_DATATYPE_INTEGER, string sValue = "", string sDescription ="")
{
    sCmd    =   GetStringUpperCase(FindAndReplace(sCmd, " ", ""));
    sName   =   FindAndReplace(sName, " ", "");

    //  nedefinuje povinny parametr po nepovinnem
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     nMin    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MIN");
    int     nMax    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");

    if
    (
        nMin    <   nMax
    &&  sValue  ==  ""
    )   return;

    //  Definice parametru
    sls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nMax + 1) + "_NAME", sName);
    sli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nMax + 1) + "_TYPE", nType);
    sls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nMax + 1) + "_VALUE", sValue);
    sls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(nMax + 1) + "_DESCRIPTION", sDescription);

    //  Zvyseni promennych udavajici pocet povinnych a nepovinnych parametru
    sli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX", nMax + 1);

    if  (sValue ==  "")
    sli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MIN", nMax + 1);
}



// +---------------------------------------------------------------------------+
// |                              CCS_InitChannel                              |
// +---------------------------------------------------------------------------+



void    CCS_InitChannel(string sChannel, string sDescription)
{
    sChannel    =   GetStringUpperCase(FindAndReplace(sChannel, " ", ""));

    //  neinicializuje jiz existujici kanal mluvy
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     bInit   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_" + sChannel + "_INIT");

    if  (bInit  !=  0)  return;

    //  inicializace kanalu mluvy
    int nLast   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_S_ROWS");

    sls(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL", sChannel, -2);
    sls(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_" + sChannel + "_DESCRIPTION", sDescription);
    sli(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_" + sChannel + "_INIT", TRUE);
}



// +---------------------------------------------------------------------------+
// |                              CCS_InitGroup                                |
// +---------------------------------------------------------------------------+



void    CCS_InitGroup(string sGroup, string sPassword = "")
{
    sGroup  =   GetStringUpperCase(FindAndReplace(sGroup, " ", ""));

    //  neinicializuje jiz existujici skupinu
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     bInit   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_ID");

    if  (bInit  !=  0)  return;

    //  inicializace skupiny
    int nLast   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_S_ROWS");

    sls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", sGroup, -2);
    sli(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_ID", nLast + 1);
    sls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_PASSWORD", sPassword);
}



// +---------------------------------------------------------------------------+
// |                             CCS_GrantAccess                               |
// +---------------------------------------------------------------------------+



void    CCS_GrantAccess(object oPC, string sGroup, int bCmd = FALSE)
{
    if  (!GetIsPC(oPC)) return;

    sGroup  =   GetStringUpperCase(FindAndReplace(sGroup, " ", ""));

    string  sType   =   (bCmd)  ?   "CMD"   :   "GROUP";
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");

    sli(oPC, SYSTEM_CCS_PREFIX + "_" + sType + "_" + sGroup + "_ACCESS", TRUE);

    if  (!bCmd)
    {
        int     nId     =   1;
        string  sCmd    =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_CMD", nId);

        while   (sCmd   !=  "")
        {
            CCS_GrantAccess(oPC, sCmd, TRUE);

            nId++;
            sCmd    =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_CMD", nId);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                          CCS_GetTargetObject                              |
// +---------------------------------------------------------------------------+



object  CCS_GetTargetObject()
{
    if  (GetSpellId()   ==  SPELL_TIS_TARGET)
    return  GetSpellTargetObject();
    return  OBJECT_SELF;
}



// +---------------------------------------------------------------------------+
// |                           CCS_GetTargetLocation                           |
// +---------------------------------------------------------------------------+



location    CCS_GetTargetLocation()
{
    if  (GetSpellId()   ==  SPELL_TIS_TARGET)
    return  GetSpellTargetLocation();
    return  GetLocation(OBJECT_SELF);
}



// +---------------------------------------------------------------------------+
// |                            CCS_GetCommandLine                             |
// +---------------------------------------------------------------------------+



string  CCS_GetCommandLine(int bMerged = TRUE)
{
    string  sResult;

    if  (GetPCChatMessage() ==  "") sResult =   GetNthSubString(gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS"), 2, "&");
    else                            sResult =   gls(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_COMMAND_PARAMETERS");

    if  (!bMerged)  return  sResult;

    object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    string  sCmd        =   CCS_GetCommand(sResult);
    string  sParams     =   CCS_GetParameters(sResult);
    int     nParams     =   GetCharsCount(CCS_COMMAND_SEPARATOR, sParams);
    int     nLast       =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
    int     n           =   1;

    nParams =   (nParams    >   nLast)  ?   nParams :   nLast;
    sParams =   "";

    while   (n  <=  nParams)
    {
        sParams +=  CCS_GetNthParameter(sResult, n, TRUE) + CCS_COMMAND_SEPARATOR;
        n++;
    }

    sResult =   CCS_COMMAND_IDENTIFIER + sCmd + " " + sParams;

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                          CCS_GetConvertedString                           |
// +---------------------------------------------------------------------------+



string  CCS_GetConvertedString(string sLine, int nNth = 1, int bMerged = FALSE)
{
    string  sValue  =   CCS_GetNthParameter(sLine, nNth, bMerged);
    string  sChar   =   left(sValue, 1);
    string  sPost   =   GetStringUpperCase(right(sValue, GetStringLength(sValue) - 1));
    string  sResult =   sValue;

    if  (sChar  ==  CCS_COMMAND_VARIABLE_IDENTIFIER)    sResult =   gls(OBJECT_SELF, sPost);

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                         CCS_GetConvertedInteger                           |
// +---------------------------------------------------------------------------+



int     CCS_GetConvertedInteger(string sLine, int nNth = 1, int bMerged = FALSE)
{
    string  sValue  =   CCS_GetNthParameter(sLine, nNth, bMerged);
    string  sChar   =   left(sValue, 1);
    string  sPost   =   GetStringUpperCase(right(sValue, GetStringLength(sValue) - 1));
    int     nResult =   stoi(sValue);

    if  (sChar  ==  CCS_COMMAND_VARIABLE_IDENTIFIER)    nResult =   gli(OBJECT_SELF, sPost);

    return  nResult;
}




// +---------------------------------------------------------------------------+
// |                         CCS_GetConvertedFloat                             |
// +---------------------------------------------------------------------------+



float   CCS_GetConvertedFloat(string sLine, int nNth = 1, int bMerged = FALSE)
{
    string  sValue  =   CCS_GetNthParameter(sLine, nNth, bMerged);
    string  sChar   =   left(sValue, 1);
    string  sPost   =   GetStringUpperCase(right(sValue, GetStringLength(sValue) - 1));
    float   fResult =   stof(sValue);

    if  (sChar  ==  CCS_COMMAND_VARIABLE_IDENTIFIER)    fResult =   glf(OBJECT_SELF, sPost);

    return  fResult;
}




// +---------------------------------------------------------------------------+
// |                         CCS_GetConvertedObject                            |
// +---------------------------------------------------------------------------+



object  CCS_GetConvertedObject(string sLine, int nNth = 1, int bMerged = FALSE)
{
    string  sValue  =   upper(CCS_GetNthParameter(sLine, nNth, bMerged));
    string  sChar   =   left(sValue, 1);
    string  sPost   =   GetStringUpperCase(right(sValue, GetStringLength(sValue) - 1));
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object  oSYS    =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    object  oResult;

    if  (sChar  ==  CCS_COMMAND_VARIABLE_IDENTIFIER)    oResult =   glo(OBJECT_SELF, sPost);    else
    if  (sChar  ==  CCS_COMMAND_TAG_IDENTIFIER)         oResult =   GetObjectByTag(sPost);
    else
    {
        oResult =
        (TestStringAgainstPattern(CCS_COMMAND_TOKENS_TARGET, sValue))   ?   CCS_GetTargetObject()   :
        (TestStringAgainstPattern(CCS_COMMAND_TOKENS_SELF,   sValue))   ?   OBJECT_SELF             :
        (TestStringAgainstPattern(CCS_COMMAND_TOKENS_MODULE, sValue))   ?   GetModule()             :
        OBJECT_INVALID;

        //  A|AS|A<ID>
        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_AREA, sValue))
        {
            //SpawnScriptDebugger();
            if  (sValue ==  "A")    oResult =   GetArea(OBJECT_SELF);                                   else
            if  (sValue ==  "AS")   oResult =   GetArea(GetWaypointByTag(SYS_TAG_PLAYER_START_PC));     else
                                    oResult =   glo(oSYS, SYSTEM_SYS_PREFIX + "_AREA", stoi(GetSubString(sValue, 1, 4)));
        }

        else

        //  O<SPAWN_OBJECT_ID>
        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_SPAWN_OBJECT, sValue))
        {
            oResult =   glo(OBJECT_SELF, "SP" + GetSubString(sValue, 1, 4));
        }

        else

        //  P|P<PC_ID>
        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_PLAYER, sValue))
        {
            if  (sValue ==  "P")    oResult =   OBJECT_SELF;

            else
            {
                int     nRow;
                int     nPlayer =   stoi(GetSubString(sValue, 1, 4));
                object  oTemp   =   GetFirstPC();

                while   (GetIsObjectValid(oTemp))
                {
                    if  (nPlayer    ==  nRow)
                    {
                        oResult =   oTemp;
                        break;
                    }

                    nRow++;
                    oTemp   =   GetNextPC();
                }
            }
        }

        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_TARGET, sValue)
        &&  !GetIsObjectValid(oResult))
        {
            location    lTarget =   CCS_GetTargetLocation();

            int rth = 1;
            oResult = GetNearestObjectToLocation(OBJECT_TYPE_ALL, lTarget, rth);

            if (GetIsObjectValid(oResult))
            {
                location lResult = GetLocation(oResult);

                // nearest object is more than minimum distance away
                if (GetDistanceBetweenLocations(lTarget, lResult) > 0.3)
                {
                    vector posTarget = GetPositionFromLocation(lTarget);
                    vector posResult = GetPositionFromLocation(lResult);
                    while (GetIsObjectValid(oResult))
                    {
                        location locTargetResult = Location(GetArea(oResult), Vector(posResult.x, posResult.y, posTarget.z), 0.0);

                        // nearest object is more than minimum distance in X and Y away (ignoring Z)
                        float distTargetResult = GetDistanceBetweenLocations(lTarget, locTargetResult);
                        if (distTargetResult <= 0.3)
                            break;
                        else if (distTargetResult > 5.0f)
                        {
                            oResult = OBJECT_INVALID;
                            break;
                        }

                        oResult = GetNearestObjectToLocation(OBJECT_TYPE_ALL, lTarget, ++rth);
                        posResult = GetPositionFromLocation(GetLocation(oResult));
                    }
                }
            }
        }
    }

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                         CCS_GetConvertedLocation                          |
// +---------------------------------------------------------------------------+



location    CCS_GetConvertedLocation(string sLine, int nNth = 1, int bMerged = FALSE, int ignoreObj = FALSE)
{
    if (!ignoreObj)
    {
        object oConverted = CCS_GetConvertedObject(sLine, nNth, bMerged);

        if  (GetIsObjectValid(oConverted)
        &&  GetIsObjectValid(GetArea(oConverted))
        &&  GetArea(oConverted) !=  oConverted)
        return GetLocation(oConverted);
    }

    string  sValue  =   upper(CCS_GetNthParameter(sLine, nNth, bMerged));
    string  sChar   =   left(sValue, 1);
    string  sPost   =   GetStringUpperCase(right(sValue, GetStringLength(sValue) - 1));
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object  oSYS    =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    location    lResult;

    if  (sChar  ==  CCS_COMMAND_VARIABLE_IDENTIFIER)    lResult =   gll(OBJECT_SELF, sPost);    else
    if  (sChar  ==  CCS_COMMAND_TAG_IDENTIFIER)         lResult =   GetLocation(GetObjectByTag(sPost));
    else
    {
        lResult =
        (TestStringAgainstPattern(CCS_COMMAND_TOKENS_TARGET, sValue))   ?   CCS_GetTargetLocation()     :
        (TestStringAgainstPattern(CCS_COMMAND_TOKENS_SELF,   sValue))   ?   GetLocation(OBJECT_SELF)    :
        (TestStringAgainstPattern(CCS_COMMAND_TOKENS_MODULE, sValue))   ?   GetLocation(GetModule())    :
        Location(OBJECT_INVALID, Vector(), 0.0f);




        //  A|AS|A<ID>
        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_AREA, sValue))
        {
            SpawnScriptDebugger();
            object  oResult;

            if  (sValue ==  "A")    oResult =   GetArea(OBJECT_SELF);                                   else
            if  (sValue ==  "AS")   oResult =   GetArea(GetWaypointByTag(SYS_TAG_PLAYER_START_PC));     else
                                    oResult =   glo(oSYS, SYSTEM_SYS_PREFIX + "_AREA", stoi(GetSubString(sValue, 1, 4)));

            lResult =   Location(oResult, Vector(itof(GetAreaSize(AREA_WIDTH, oResult) * 10) / 2 - 1, itof(GetAreaSize(AREA_HEIGHT, oResult) * 10 - 1) / 2 - 1, 0.0f), GetFacing(OBJECT_SELF));
        }

        else

        //  O<SPAWNED_OBJECT_ID>
        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_SPAWN_OBJECT, sValue))
        {
            lResult =   GetLocation(glo(OBJECT_SELF, "SP" + GetSubString(sValue, 1, 4)));
        }

        else

        //  P|P<PC_ID>
        if  (TestStringAgainstPattern(CCS_COMMAND_TOKENS_PLAYER, sValue))
        {
            if  (sValue ==  "P")    lResult =   GetLocation(OBJECT_SELF);

            else
            {
                int     nRow;
                int     nPlayer =   stoi(GetSubString(sValue, 1, 4));
                object  oTemp   =   GetFirstPC();

                while   (GetIsObjectValid(oTemp))
                {
                    if  (nPlayer    ==  nRow)
                    {
                        lResult =   GetLocation(oTemp);
                        break;
                    }

                    nRow++;
                    oTemp   =   GetNextPC();
                }
            }
        }
    }

    return  lResult;
}
