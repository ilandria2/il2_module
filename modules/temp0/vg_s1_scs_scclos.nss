// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Close summoning circle                 |
// | File    || vg_s1_scs_scclo.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 14-05-2009                                                     |
// | Updated || 14-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Znici vyvolavaci kruh s vizualnim efektem
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "inc_draw"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void Recompute(object oArea)
{
    DelayCommand(6.0f, RecomputeStaticLighting(oArea));
}



void    main()
{
    int     i       =   0;
    int     nTotal  =   gli(OBJECT_SELF, "storetotal");
    effect  eVisual =   EffectVisualEffect(VFX_IMP_MAGIC_PROTECTION);
    float   fDelay  =   0.0f;
    float   fInc    =   1.0f/(IntToFloat(nTotal)/3.0f);
    object  oNode   =   glo(OBJECT_SELF, "store"+IntToString(i));
    object  oArea   =   GetArea(OBJECT_SELF);

    //AssignCommand(GetModule(), Recompute(oArea));

    while   (oNode  !=  OBJECT_INVALID)
    {
        DelayCommand(fDelay, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, eVisual, GetLocation(oNode)));

        i++;
        fDelay  +=  fInc;
        oNode   =   glo(OBJECT_SELF, "store"+IntToString(i));
    }

    DelayCommand(1.0f, GroupDestroyObject(OBJECT_SELF, 0.0f, 2.5f, FALSE));
}

