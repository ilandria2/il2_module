// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_pltinfo.nss                                                |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-12-2009                                                     |
// | Updated || 05-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "PLTINFO"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "PLTINFO";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nPltID  =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    string  sType   =   CCS_GetConvertedString(sLine, 2, FALSE);
    string  sMessage;

    //  neplatne ID
    if  (nPltID <   -1)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� ID";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sType   =   GetStringUpperCase(sType);

    int nObjType    =   (sType  ==  "C")    ?   OBJECT_TYPE_CREATURE    :
                        (sType  ==  "D")    ?   OBJECT_TYPE_DOOR        :
                        (sType  ==  "I")    ?   OBJECT_TYPE_ITEM        :
                        (sType  ==  "M")    ?   OBJECT_TYPE_STORE       :
                        (sType  ==  "E")    ?   OBJECT_TYPE_ENCOUNTER   :
                        (sType  ==  "T")    ?   OBJECT_TYPE_TRIGGER     :
                        (sType  ==  "W")    ?   OBJECT_TYPE_WAYPOINT    :   OBJECT_TYPE_INVALID;

    if  (nObjType   ==  OBJECT_TYPE_INVALID)
    {
        sMessage    ==  R1 + "( ! ) " + W2 + "Neplatn� typ objektu";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  seznam vsech navrhu ze sloupce UT*
    if  (nPltID ==  -1)
    {
        int     nRow;
        string  sTAG    =   Get2DAString("ccs_pltinfo", "UT" + sType, nRow);
        string  sResRefs;

        while   (sTAG   !=  "")
        {
            sResRefs    +=  "\n" + W2 + "#" + itos(nRow) + " - " + G2 + sTAG;
            sTAG        =   Get2DAString("ccs_pltinfo", "UT" + sType, ++nRow);
        }

        sMessage    =   Y1 + "( ? ) " + W2 + "Seznam n�vrh� typu [" + G2 + sType + W2 + "]";
        sMessage    +=  "\n" + W2 + "~~~~~~~~~~~~~~~~";
        sMessage    +=  sResRefs;

        msg(oPC, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
