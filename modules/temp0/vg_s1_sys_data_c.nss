// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Persistent data check script            |
// | File    || vg_s1_sys_data_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-09-2009                                                     |
// | Updated || 05-09-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro system SYS
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;

    //  sys_players tabble (one player account : N player characters)
    sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Checking table [sys_players]";
    sQuerry =   "DESCRIBE sys_players";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_players] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_players] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE sys_players " +
        "(" +
            "PlayerID INT(6) NOT NULL," +
            "PlayerACC VARCHAR(32) DEFAULT NULL," +
            "PlayerNAME VARCHAR(64) DEFAULT NULL," +
            "PlayerPOS VARCHAR(200) DEFAULT NULL," +
            "CharHeadType SMALLINT DEFAULT NULL," +
            "CharSkinColor SMALLINT DEFAULT NULL," +
            "CharHairColor SMALLINT DEFAULT NULL," +
            "CharGender SMALLINT DEFAULT NULL," +
            "CharPhenotype SMALLINT DEFAULT NULL," +
            "CharRace SMALLINT DEFAULT NULL," +
            "CharSoundSet VARCHAR(16) DEFAULT NULL," +
            "CharDescription TEXT DEFAULT NULL," +
            "CharAge SMALLINT DEFAULT NULL," +
            "CharHitPoints SMALLINT DEFAULT NULL," +
            "CharHitPointsMax SMALLINT DEFAULT NULL," +
            "RegisteredOn DATETIME DEFAULT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE sys_players";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_players] created successfully"; else
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_players] not created - error";

        logentry(sLog);
    }

    //  sys_abilities table (one player character : N abilities)
    sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Checking table [sys_abilities]";
    sQuerry =   "DESCRIBE sys_abilities";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_abilities] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_abilities] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE sys_abilities " +
        "(" +
            "PlayerID INT(6) NOT NULL," +
            "CharHeadType INT(6) NOT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE sys_abilities";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_abilities] created successfully"; else
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_abilities] not created - error";

        logentry(sLog);
    }

    //  sys_keys table
    sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Checking table [sys_keys]";
    sQuerry =   "DESCRIBE sys_keys";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_keys] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_keys] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE sys_keys " +
        "(" +
            "PlayerACC VARCHAR(32) NOT NULL," +
            "PlayerCD VARCHAR(12) NOT NULL," +
            "PlayerPWD VARCHAR(32) NOT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE sys_keys";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_keys] created successfully";    else
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_keys] not created - error";

        logentry(sLog);
    }

    //  sys_settings table
    sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Checking table [sys_settings]";
    sQuerry =   "DESCRIBE sys_settings";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_settings] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_settings] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE sys_settings " +
        "(" +
            "GameDate datetime NULL," +
            "LastObjectId int NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE sys_settings";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        {
            sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_settings] created successfully";
            sQuerry =   "INSERT INTO sys_settings values('" + itos(GAMEDATE_YEAR_START) + "-01-01 00:00:00', 0, NOW())";

            SQLExecDirect(sQuerry);
        }

        else
        {
            sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_settings] not created - error";
        }

        logentry(sLog);
    }

    //  sys_vars table
    sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Checking table [sys_vars]";
    sQuerry =   "DESCRIBE sys_vars";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_vars] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_vars] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE sys_vars " +
        "(" +
            "ObjType SMALLINT NOT NULL," +
            "ObjDef VARCHAR(32) NOT NULL," +
            "VarType SMALLINT NOT NULL," +
            "VarName VARCHAR(32) NOT NULL," +
            "VarValue TEXT NOT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "ALTER TABLE sys_vars ADD CONSTRAINT pk_obj PRIMARY KEY (ObjType, ObjDef, VarType, VarName)";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE sys_vars";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_vars] created successfully";    else
        sLog    =   "[" + SYSTEM_SYS_PREFIX + "] Table [sys_vars] not created - error";

        logentry(sLog);
    }
}
