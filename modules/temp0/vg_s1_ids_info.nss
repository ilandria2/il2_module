// +---------++----------------------------------------------------------------+
// | Name    || Item Durability System (IDS) Sys info                          |
// | File    || vg_s1_ids_info.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu IDS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_pis_core_t"
#include    "vg_s0_ids_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void DoIndicateInventory(object oPC)
{
    sli(oPC, IDS_ + "INDICATOR", -1, -1, SYS_M_DELETE);
    string status = PIS_GetInventoryIndicator(oPC);

    msg(oPC, status, TRUE, FALSE);
}

void IndicateInventory(object oPC)
{
    if (gli(oPC, IDS_ + "INDICATOR")) return;
    sli(oPC, IDS_ + "INDICATOR", TRUE);

    DelayCommand(0.5, DoIndicateInventory(oPC));
}

void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat  =   TRUE;

    sTemp   =   gls(OBJECT_SELF, IDS_ + "NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "TINDERBOX_NA")
    {
        sMessage    =   C_BOLD + "You need a tinderbox.";
    }

    else

    if (sCase == "ITEM_DROPPED")
    {
        string sName = GetNthSubString(sTemp, 1);
        sMessage = C_BOLD + "Item dropped\n" + C_NORM + sName;
    }

    else if (sCase == "ITEMS_DROPPED")
    {
        sMessage = C_BOLD + "You have dropped some items.";
    }

    else

    if  (sCase  ==  "NOT_ENOUGH_SLOTS")
    {
        sMessage    =   C_BOLD + "Not enough space in inventory!";
    }

    else

    if  (sCase  ==  "SLOTS_STATUS")
    {
        IndicateInventory(OBJECT_SELF);
        return;
    }

    else

    if  (sCase  ==  "STATUS")
    {
        string  sName   =   GetNthSubString(sTemp, 1);
        int     nStatus =   stoi(GetNthSubString(sTemp, 2));

        nStatus =   (nStatus    <   0)      ?   0   :
                    (nStatus    >   100)    ?   100 :   nStatus;

        string  sDiag   =   GetStringByStrRef(stoi(Get2DAString("ips_durstat", "Name", nStatus)));

        sMessage    =   (!nStatus)  ?   C_BOLD + "Item '" + sName + C_BOLD + "' lost its durability and was destroyed" :
                                        sDiag + C_NORM + " " + sName;// + " (" + C_BOLD + itos(nStatus) + C_NORM + "%)";
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}

