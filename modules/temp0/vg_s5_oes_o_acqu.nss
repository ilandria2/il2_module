// +---------++----------------------------------------------------------------+
// | Name    || Object Events System (OES) Module event script                 |
// | File    || vg_s5_oes_o_acqu.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-12-2007                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Udalost modulu "OnAcquireItem"
*/
// -----------------------------------------------------------------------------



#include    "x2_inc_switches"
#include    "vg_s0_oes_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
     object oItem = GetModuleItemAcquired();
     // * Generic Item Script Execution Code
     // * If MODULE_SWITCH_EXECUTE_TAGBASED_SCRIPTS is set to TRUE on the module,
     // * it will execute a script that has the same name as the item's tag
     // * inside this script you can manage scripts for all events by checking against
     // * GetUserDefinedItemEventNumber(). See x2_it_example.nss
     if (GetModuleSwitchValue(MODULE_SWITCH_ENABLE_TAGBASED_SCRIPTS) == TRUE)
     {
        SetUserDefinedItemEventNumber(X2_ITEM_EVENT_ACQUIRE);
        int nRet =   ExecuteScriptAndReturnInt(GetUserDefinedItemEventScriptName(oItem),OBJECT_SELF);
        if (nRet == X2_EXECUTE_SCRIPT_END)
        {
           return;
        }

     }

    OES_ActionEvent(OES_EVENT_O_ON_ACQUIRE_ITEM);
}
