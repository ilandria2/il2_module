// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Objekt death event script        |
// | File    || vg_s1_tis_dspawn.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Spawnovaci systemu dynamickych objektu (soucast systemu TIS)
    Pokud byl prave-zniceny objekt vytvoren spawn systemem, bude mu resetnout
    cas spawnu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    //SpawnScriptDebugger();
    object  oTIS    =   glo(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_TISOBJ");

    oTIS    =   (!GetIsObjectValid(oTIS))   ?   OBJECT_SELF :   oTIS;

    string  sGroup  =   gls(oTIS, SYSTEM_TIS_PREFIX + "_GROUP");

    //  znicenej objekt byl vytvoren spawn systemem
    if  (sGroup !=  "")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
        object  oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sGroup);
        int     nRow    =   gli(oTIS, SYSTEM_TIS_PREFIX + "_ROW");
        int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");

        sli(oMaster, SYSTEM_TIS_PREFIX + "_HBS", nHBs, nRow);
    }

    //  objekty krome bytosti
    if  (GetObjectType(OBJECT_SELF) ==  OBJECT_TYPE_CREATURE)   return;

    //  znicenej objekt (PLC) odhaloval interakcni bod (WP)
    if  (GetIsObjectValid(oTIS)
    &&  oTIS    !=  OBJECT_SELF)
    {
        //logentry("tisobj_destroy_before");
        DestroyObject(oTIS);
        //logentry("tisobj_destroy_after");
    }
}

