// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Check object - rock object       |
// | File    || tis_o_rock.nss                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Skript pro detekci interakcniho objektu typu ROCK
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    object  oTIS    =   glo(oPC, SYSTEM_TIS_PREFIX + "_OBJECT", -1, SYS_M_DELETE);      //  vybrany TIS objekt
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");      //  TIS system objekt
    string  sSub    =   left(GetTag(oTIS), 16);                                         //  TAG
    string  sOT     =   itos(gli(oSystem, SYSTEM_TIS_PREFIX + "_TAG_" + sSub + "_OT")); //  TAG.TypeID
    string  sOS     =   itos(gli(oSystem, SYSTEM_TIS_PREFIX + "_TAG_" + sSub + "_OS")); //  TAG.SubTypeID
    string  sTech   =   gls(oSystem, SYSTEM_TIS_PREFIX + "_TAG_" + sSub + "_TECH");     //  TAG.Tech
    int     bResult =   FALSE;



//  ----------------------------------------------------------------------------
//  Kod detekce objektu

    int nLevel;

    nLevel  =   gli(oSystem, SYSTEM_TIS_PREFIX + "_SET_OS_" + sOS + "_LEVEL");
    bResult =   gli(oPC, sTech + "_LEVEL");
    bResult =   bResult >= nLevel;
    bResult =   bResult && gli(oSystem, SYSTEM_TIS_PREFIX + "_SET_OS_" + sOS + "_AS_108_RECIPE") >= 100;

//  Kod detekce objektu
//  ----------------------------------------------------------------------------



    sli(oPC, SYS_VAR_RETURN, bResult);
}

