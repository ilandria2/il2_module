// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Set Dynamic Name script          |
// | File    || vg_s1_pis_setnam.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Tento skript se pusti kdyz hrac pouzije kanal /NAME a nasledne pouzije
    ovladaci prvek target
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_pis_core_c"
#include    "vg_s0_sys_core_d"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    int     nSpell  =   GetSpellId();

    if  (nSpell ==  SPELL_TIS_TARGET)
    {
        object  oTarget =   GetSpellTargetObject();

        if  (oPC    ==  oTarget)
        {
            SYS_Info(oPC, SYSTEM_PIS_PREFIX, "TARGET_UNSET");
            sls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "", -1, SYS_M_DELETE);
            return;
        }

        if  (!GetIsPC(oTarget)
        ||  GetDistanceBetween(oPC, oTarget)    >   32.0f)
        {
            SYS_Info(oPC, SYSTEM_PIS_PREFIX, "VALID_TARGET");
            return;
        }

        string  sMessage, sName;
        int     nTarget;

        //sName       =   GetNthSubString(gls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", -1, SYS_M_DELETE), 2);
        sName       =   GetNthSubString(gls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS"), 2);
        nTarget     =   gli(oTarget, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        //  smazani jmena
        if  (sName  ==  "0")
        {
            sMessage    =   C_INTE + "Jm�no pro vybranou postavu smaz�no";

            sls(oPC, SYSTEM_PIS_PREFIX + "_PC_" + itos(nTarget) + "_NAME", "", -1, SYS_M_DELETE);
        }

        //  nastaveni jmena
        else
        {
            sMessage    =   C_POSI + "Jm�no pro vybranou postavu nastaveno";

            sls(oPC, SYSTEM_PIS_PREFIX + "_PC_" + itos(nTarget) + "_NAME", sName);
            logentry("[" + SYSTEM_PIS_PREFIX + "] Dynamic name set by [" + GetPCPlayerName(oPC) + "] for [" + GetPCPlayerName(oTarget) + "] to [" + sName + "]", TRUE);
        }

        SavePersistentData(oPC, SYSTEM_PIS_PREFIX, itos(nTarget));
        PIS_RefreshIndicator(oPC, oTarget, FALSE);
        msg(oPC, sMessage, TRUE, FALSE);
    }
}

