// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) - Walk mode script                    |
// | File    || mode_walk.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Skript pro mod "Chuze"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    int     nMode   =   gli(oPC, SYSTEM_SYS_PREFIX + "_MODE");

    //  preruseni - nelze prerusit
    if  (nMode  <   0)  return;

    //  Mod chuze je aktivni - nic se nestane
    if  (nMode  ==  SYS_MODE_WALK)  return;

    SwitchRunState(oPC, FALSE, 30);
    sli(oPC, SYSTEM_SYS_PREFIX + "_MODE", SYS_MODE_WALK);
    SYS_Info(oPC, SYSTEM_SYS_PREFIX, "MODE_SWITCH_&1&_&" + itos(nMode) + "&");
}

