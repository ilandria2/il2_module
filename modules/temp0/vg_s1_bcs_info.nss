// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Sys info                              |
// | File    || vg_s1_bcs_info.nss                                             |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 18-07-2009                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu BCS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_bcs_core_c"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if (sCase   == "USE_ABILITY")
    {
        int nFeat = stoi(GetNthSubString(sTemp, 1));
        int bSecondary = stoi(GetNthSubString(sTemp, 2));

        if (nFeat == FEAT_BCS_ADVANCED_HIT_STRONG)
        {
            if (!bSecondary)
                sMessage = C_BOLD + "*Strong hit!*";
            else
                sMessage = C_BOLD + "*Mighty hit!*";
        }

        bFloat = TRUE;
    }

    else

    if  (sCase  ==  "COOLDOWN")
    {
        sMessage    =   R1 + "-- Wait for the Cooldown --";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "NO_ATTACK")
    {
        sMessage    =   R1 + "-- You have to be in a combat first --";
        bFloat      =   FALSE;
    }

    else

    if  (sCase  ==  "WEAPON_SIZE_2_X")
    {
        sMessage    =   R1 + "-- You need to equip a medium weapon in the right hand --";
        bFloat      =   FALSE;
    }

    else

    if  (sCase  ==  "WEAPON_SIZE_3P_X")
    {
        sMessage    =   R1 + "-- You need to equip a large weapon in the right hand --";
        bFloat      =   FALSE;
    }

    else

    if  (sCase  ==  "STAMINA")
    {
        PIS_RefreshIndicatorPortrait(OBJECT_SELF);
        /*
        int nValFrom    =   stoi(GetNthSubString(sTemp, 1));
        int nValCur     =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_STAMINA");
        int nValMax     =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_STAMINA_MAX");

        sMessage    +=  GetDiagram(nValCur, nValMax, nValFrom, FALSE) + C_NORM + " Stamina";
        bFloat      =   FALSE;
        */
    }

    else

    if  (sCase  ==  "STAMINA_X")
    {
        sMessage    =   C_NEGA + "Not enough stamina!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "POWER_X")
    {
        sMessage    =   C_NEGA + "Not enough power!";
        bFloat      =   TRUE;
    }

    else

    if  (sCase  ==  "STAMINA_F")
    {
        sMessage    =   C_POSI + "Stamina regenerated!";
        bFloat      =   FALSE;
    }

    else

    if  (sCase  ==  "POWER_F")
    {
        sMessage    =   C_POSI + "Power regenerated!";
        bFloat      =   FALSE;
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}
