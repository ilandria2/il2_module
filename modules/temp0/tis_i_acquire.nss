// Text Interaction System (TIS) Interaction script - ACQUIRE

//this = TIS_ConstructInteraction();
#include "vg_s0_tis_inter"
#include "vg_s0_acs_core_c"

const int ACTION_TYPE  = 100;

// helper functions
void ApplyAudioVideoOnCorpse();
void ApplyExamineAudio();

//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    string sMessage;
    int bResult = FALSE;
    int nItem = (GetObjectType(this.Object) != OBJECT_TYPE_CREATURE) ? BASE_ITEM_HERBSICKLE : BASE_ITEM_SKINKNIFE;

    // need to have the right tool equipped
    if (GetBaseItemType(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, this.Player)) != nItem)
    {
        sMessage = (nItem == BASE_ITEM_HERBSICKLE) ? "herb sickle" : "skinning knife";
        sMessage = C_NORM + "You need to equip a " + C_INTE + sMessage + C_NORM + " in your right hand";
    }

    // need to have the off-hand free
    else if (GetIsObjectValid(GetItemInSlot(INVENTORY_SLOT_LEFTHAND, this.Player)))
        sMessage = C_NORM + "Make your " + C_INTE + "left hand free";

    else
    {
        object user = TIS_GetUser(this.Object);
        if (GetIsObjectValid(user) && user != this.Player)
            sMessage = C_BOLD + "Someone else is using it.";

        // success
        else
            return TRUE;
    }

    if (!bResult)
    {
        sMessage = ConvertTokens(sMessage, this.Player, FALSE, FALSE, FALSE, FALSE);
        msg(this.Player, sMessage, TRUE, FALSE);
    }

    return bResult;
}

//////// ACTION'S BODY
void Do()
{
//struct TIS_Interaction _this = this; //debug
    string  sMessage;

    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        sMessage = "Cuts something";

        // plants & mushrooms audio effects
        if (this.ObjectTech == "PLANT" || this.ObjectTech == "MUSHROOM")
        {
            sMessage = "Takes something";
            DelayCommand(1.0f, ApplyExamineAudio());
        }

        // body audio effects
        else if (this.ObjectTech == "BODY")
            DelayCommand(2.0f, AssignCommand(this.Player, ApplyAudioVideoOnCorpse()));

        // sequence based action
        this.DelaySequence = TIS_INTERACT_SEQUENCE_DELAY;

        // show time bar and play animation for the time being
        AssignCommand(this.Player, SpeakString(ConvertTokens("*" + sMessage + "*", this.Player, FALSE, TRUE, FALSE, FALSE)));
        AssignCommand(this.Player, PlayAnimation(ANIMATION_LOOPING_GET_LOW, 1.0f, this.DelaySequence));
    }

    // delayed sequence
    else
    {
        int nRecipe = gli(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_AS_" + itos(this.ActionSubType) + "_RECIPE");
        int nQty;

        StopAnimation(this.Player);

        // last sequence
        this.DelaySequence = -999.0;

        // plant, mushroom
        if (this.ObjectTech == "PLANT" || this.ObjectTech == "MUSHROOM")
        {
            // quantity based on action sub type (for all objects)
            nQty = gli(__TIS(), TIS_ + "SET_AS_" + itos(this.ActionSubType) + "_QTY");

            SetPlotFlag(this.Object, FALSE);
            DestroyObject(this.Object, 0.1f);
        }

        // body
        else
        {
            string sBodyAction = gls(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_AS_" + itos(this.ActionSubType) + "_TYPE");

            // amount of repetitions this action can be performed
            int nAmount = gli(this.Object, TIS_ + "A_" + itos(this.ActionSubType) + "_AMOUNT");
            if (!nAmount)
                nAmount = gli(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_AS_" + itos(this.ActionSubType) + "_AMOUNT");

            nQty = gli(__TIS(), TIS_ + "SET_AS_" + itos(this.ActionSubType) + "_QTY");
            if (!nQty)
                nQty = gli(__TIS(), TIS_ + "SET_OS_" + itos(this.ObjectSubType) + "_QTY");

            // amount of -1 means depleted
            if (--nAmount == 0)
                --nAmount;
            sli(this.Object, TIS_ + "A_" + itos(this.ActionSubType) + "_AMOUNT", nAmount);

            // last sequence with reopen gui
            if (nAmount > -1)
                this.DelaySequence = 0.0;
        }

        // lower tool's durability
        object oRight = GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, this.Player);
        int DBCUR = IDS_GetItemValue(oRight, FALSE);
        int DBNEW = DBCUR - TIS_DURABILITY_DECREASE_ACQUIRE;

        IDS_UpdateItem(this.Player, oRight, DBNEW, -1, TRUE);

        // finally create item by recipe (with random 75-100% of set quantity)
        nQty = ftoi(nQty * itof(100 - Random(26)) / 100);
        ACS_CreateItemUsingRecipe(nRecipe, nQty, li(), this.Player, TRUE);
    }
}



/////// helper functions
void ApplyExamineAudio()
{
    int nDice;
    string sSound;

    nDice = Random(3);

    switch (nDice)
    {
    case 0: sSound = "as_na_grassmove1"; break;
    case 1: sSound = "as_na_grassmove2"; break;
    case 2: sSound = "as_na_grassmove3"; break;
    }

    AssignCommand(this.Object, PlaySound(sSound));
}

void ApplyAudioVideoOnCorpse2()
{
    vector vPos = GetAwayVector(GetPosition(this.Player), 0.5f, GetFacing(this.Player));
    int nDice;
    int nVFX;
    string sSound;

    int bChance = (Random(100) <= 15) ? TRUE : FALSE;

    if (bChance)
    {
        location lRan = GetRandomLocation(GetArea(this.Player), this.Player, 0.25f);
        vector vRan = GetPositionFromLocation(lRan);
        string sBlood = "vg_p_bf_bloode0" + itos(1 + Random(7));

        lRan = Location(GetArea(this.Player), Vector(vRan.x, vRan.y, vRan.z + 0.01f), itof(Random(360)));

        DelayCommand(30 * 60.0f, DestroyObject(CreateObject(OBJECT_TYPE_PLACEABLE, sBlood, lRan)));
    }

    nDice = Random(7);

    switch (nDice)
    {
    case 0: sSound = "cb_ht_fleshleth1"; break;
    case 1: sSound = "cb_ht_fleshleth2"; break;
    case 2: sSound = "cb_ht_chunk"; break;
    case 3: sSound = "cb_ht_whipleth1"; break;
    case 4: sSound = "cb_ht_whipleth2"; break;
    case 5: sSound = "cb_ht_daggrleth1"; break;
    case 6: sSound = "cb_ht_daggrleth2"; break;
    }

    nDice = Random(3);

    switch (nDice)
    {
    case 0: nVFX = VFX_COM_BLOOD_CRT_RED; break;
    case 1: nVFX = VFX_COM_BLOOD_LRG_RED; break;
    case 2: nVFX = VFX_COM_BLOOD_REG_RED; break;
    }

    AssignCommand(this.Object, PlaySound(sSound));
    ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(nVFX), Location(GetArea(this.Player), Vector(vPos.x, vPos.y, vPos.z + 0.5f), itof(Random(360))));
}


void ApplyAudioVideoOnCorpse()
{
    ApplyAudioVideoOnCorpse2();
    DelayCommand(ran(0.5f, 1.5f), ApplyAudioVideoOnCorpse2());
    DelayCommand(ran(1.5f, 3.0f), ApplyAudioVideoOnCorpse2());
}



/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

