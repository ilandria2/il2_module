// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Module event script                    |
// | File    || vg_s1_scs_death.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 30-04-2008                                                     |
// | Updated || 23-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Vynuluje uroven many zemrele postave hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetLastPlayerDied();

    if  (!GetIsDM(oPC))
    {
        int nMana   =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA");

        if  (nMana  >   0)
        {
            sli(oPC, SYSTEM_SCS_PREFIX + "_MANA", 0);
            SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&" + itos(nMana) + "&_&0&");
        }
    }
}
