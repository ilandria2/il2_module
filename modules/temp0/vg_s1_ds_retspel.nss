// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) VFX Spell script                             |
// | File    || vg_s1_ds_retspel.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    NPC zakouzli fake-kouzlo "Raise Dead" pouze pro vizualni efekt
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   LPD_GetPC();
    object  oNPC    =   OBJECT_SELF;

    AssignCommand(oNPC, ClearAllActions(TRUE));
    AssignCommand(oNPC, ActionCastFakeSpellAtObject(SPELL_RAISE_DEAD, oPC));
}

