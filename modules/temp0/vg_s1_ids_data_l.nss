#include "vg_s0_ids_core_c"
#include "aps_include"

void placeItem(struct IDS_RawItem i)
{
    logentry("[" + IDS + "] loading raw item: ID=" + itos(i.ID) + ", TAG=" + i.TAG + ", " + i.NAME + " in area: " + GetTag(GetAreaFromLocation(i.LOC)) + ", weight: " + itos(i.WEIGHT) + ", x" + itos(i.STACK) + ", dbcur: " + itos(i.DBCUR) + ", dbmax: " + itos(i.DBMAX) + "...");
    object placed_item = IDS_CreatePlacedItemRaw(i);
}

void FinishLoadingForArea(string areaTag)
{
    object placed_list = IDS_GetPlacedItemList(areaTag);
    if (!GetIsObjectValid(placed_list))
        logentry("[" + IDS + "] unable to get placed items list by area tag: " + areaTag);
    else
    {
        logentry("[" + IDS + "] finished loading persisted placed items for area: " + areaTag);
        sli(placed_list, IDS_ + "LOADED", TRUE);
    }
}

void main()
{
    sli(__IDS(), IDS_ + "LOADING_DATA", TRUE);
    //string param = gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_LOAD_PARAM", -1, SYS_M_DELETE);
    string query =
        "SELECT item_oid, item_type, item_tag, item_tech, item_stack, item_weight, item_dbcur, item_dbmax, item_loc, item_name, item_desc " +
        "FROM ids_placed_items " +
        "ORDER BY mid(item_loc, 7, 16) asc";

    SQLExecDirect(query);

    int counter = 0;
    while (SQLFetch() == SQL_SUCCESS)
    {
        struct IDS_RawItem rawItem;
        rawItem.ID = stoi(SQLGetData(1));
        rawItem.TYPE = stoi(SQLGetData(2));
        rawItem.TAG = SQLGetData(3);
        rawItem.TECH = SQLGetData(4);
        rawItem.STACK = stoi(SQLGetData(5));
        rawItem.WEIGHT = stoi(SQLGetData(6));
        rawItem.DBCUR = stoi(SQLGetData(7));
        rawItem.DBMAX = stoi(SQLGetData(8));
        rawItem.LOC = APSStringToLocation(SQLGetData(9));
        rawItem.NAME = SQLDecodeSpecialChars(SQLGetData(10));
        rawItem.DESC = SQLDecodeSpecialChars(SQLGetData(11));

        DelayCommand(0.01 * counter++, placeItem(rawItem));
    }

    if (!counter)
        logentry("[" + IDS + "] There are no placed items to be loaded.");

    object SysObj = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int row;
    object area = glo(SysObj, SYSTEM_SYS_PREFIX + "_AREA", ++row);

    while (GetIsObjectValid(area))
    {
        DelayCommand(1.0 + 0.01 * counter, FinishLoadingForArea(GetTag(area)));
        area = glo(SysObj, SYSTEM_SYS_PREFIX + "_AREA", ++row);
    }

    DelayCommand(1.5 + 0.01 * counter, sli(__IDS(), IDS_ + "LOADING_DATA", FALSE));
}
