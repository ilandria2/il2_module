// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Inventory Viewer - Disturbed     |
// | File    || vg_s1_ccs_inv_di.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 14-12-2009                                                     |
// | Updated || 14-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Provedene zmeny na kontejneru odzrkadli i u inventare vybraneho objektu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_o"
#include    "vg_s0_ccs_const"



void    VerifyEquipped(int nSlot, object oItem, object oSrcItem, object oCreature)
{
    object  oNewItem    =   GetItemInSlot(nSlot, oCreature);

    if  (oNewItem   !=  oItem)
    {
        string  sMessage    =   R1 + "( ! ) " + W2 + "C�li se nepoda�ilo vybavit p�edm�t [" + R2 + GetName(oItem) + W2 + "], proto z�st�v� v jeho obecn�m invent��i";

        msg(OBJECT_SELF, sMessage, TRUE, FALSE);
        SetPlotFlag(oSrcItem, FALSE);
        Destroy(oSrcItem);
    }
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nType       =   GetInventoryDisturbType();
    object  oContainer  =   OBJECT_SELF;
    object  oItem       =   GetInventoryDisturbItem();
    object  oPC         =   GetLastDisturbed();
    object  oPoss       =   GetItemPossessor(oItem);
    object  oTarget     =   glo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_TARGET");
    string  sType       =   gls(oContainer, SYSTEM_CCS_PREFIX + "_INVENTORY_TYPE");
    string  sMessage;
    object  oCopy;

    switch  (nType)
    {
        //  pridani predmetu
        case    INVENTORY_DISTURB_TYPE_ADDED:
        {
            //  objekt, ktereho inventar je prohledavan jiz neexistuje a proto
            //  treba znicit neviditelny kontejner
            if  (!GetIsObjectValid(oTarget))
            {
                AssignCommand(oPC, ClearAllActions(TRUE));
                AssignCommand(oPC, ActionTakeItem(oItem, oContainer));
                AssignCommand(oPC, ActionInteractObject(oContainer));

                sMessage    =   R1 + "( !!! ) " + W2 + "Objekt, kter�ho invent�� je prohled�van ji� neexistuje a proto t�eba zni�it neviditeln� kontejner";

                msg(oPC, sMessage, TRUE, FALSE);
                return;
            }

            int nNewSlot    =   -1;
            int bEquip      =   FALSE;
            int bValid      =   FALSE;
            int nBaseItem   =   GetBaseItemType(oItem);

            //  kdyz se prohlizi inventar typu "EQUIPPED" (vybava) / "NATURAL"
            //  (predmety bytosti), pak si danej predmet hrac nasledne vybavi na
            //  patricny slot v intentari
            if  (sType  ==  "EQUIPPED")
            {
                bEquip  =   TRUE;

                switch  (nBaseItem)
                {
                    case    BASE_ITEM_AMULET:   nNewSlot    =   INVENTORY_SLOT_NECK;        break;
                    case    BASE_ITEM_ARMOR:    nNewSlot    =   INVENTORY_SLOT_CHEST;       break;
                    case    BASE_ITEM_ARROW:    nNewSlot    =   INVENTORY_SLOT_ARROWS;      break;
                    case    BASE_ITEM_BELT:     nNewSlot    =   INVENTORY_SLOT_BELT;        break;
                    case    BASE_ITEM_BOLT:     nNewSlot    =   INVENTORY_SLOT_BOLTS;       break;
                    case    BASE_ITEM_BOOTS:    nNewSlot    =   INVENTORY_SLOT_BOOTS;       break;
                    case    BASE_ITEM_BRACER:
                    case    BASE_ITEM_GLOVES:   nNewSlot    =   INVENTORY_SLOT_ARMS;        break;
                    case    BASE_ITEM_BULLET:   nNewSlot    =   INVENTORY_SLOT_BULLETS;     break;
                    case    BASE_ITEM_CLOAK:    nNewSlot    =   INVENTORY_SLOT_CLOAK;       break;
                    case    BASE_ITEM_HELMET:   nNewSlot    =   INVENTORY_SLOT_HEAD;        break;
                    case    BASE_ITEM_TORCH:    nNewSlot    =   INVENTORY_SLOT_LEFTHAND;    break;
                    case    BASE_ITEM_GRENADE:  nNewSlot    =   INVENTORY_SLOT_RIGHTHAND;   break;
                    case    BASE_ITEM_SMALLSHIELD:
                    case    BASE_ITEM_LARGESHIELD:
                    case    BASE_ITEM_TOWERSHIELD:  nNewSlot    =   INVENTORY_SLOT_LEFTHAND;    break;
                    case    BASE_ITEM_RING:
                    {
                        object  oRing;

                        nNewSlot    =   INVENTORY_SLOT_RIGHTRING;
                        oRing       =   GetItemInSlot(nNewSlot, oTarget);

                        if  (!GetIsObjectValid(oRing))  break;

                        nNewSlot    =   INVENTORY_SLOT_LEFTRING;
                        oRing       =   GetItemInSlot(nNewSlot, oTarget);

                        if  (!GetIsObjectValid(oRing))  break;

                        //  cil jiz ma equipnute obidva slotu prstenu, takze
                        //  se vybere pravy. pokud chce mit levy, musi mu ho
                        //  nejprve de-equipnout
                        nNewSlot    =   INVENTORY_SLOT_RIGHTRING;
                        sMessage    =   Y1 + "( ? ) " + W2 + "C�l ji� m� obsazen� obidva sloty pro prsten, proto prsten na prav� ruce bude br�n jako v�choz�. Kdy� chce� pou��t levou ruku, mus� nejd��ve c�li jej� prsten z lev� ruky sundat.";

                        msg(oPC, sMessage, TRUE, FALSE);
                    }
                    break;

                    default:
                    {
                        string  s2da    =   Get2DAString("baseitems", "WeaponType", nBaseItem);

                        //  jedna se o zbran
                        if  (stoi(s2da) >   0)
                        {
                            object  oWield;

                            nNewSlot    =   INVENTORY_SLOT_RIGHTHAND;
                            oWield      =   GetItemInSlot(nNewSlot, oTarget);

                            if  (!GetIsObjectValid(oWield)) break;

                            nNewSlot    =   INVENTORY_SLOT_LEFTHAND;
                            oWield      =   GetItemInSlot(nNewSlot, oTarget);

                            if  (!GetIsObjectValid(oWield))
                            {
                                string  sEquip  =   Get2DAString("baseitems", "EquipableSlots", nBaseItem);
                                int     bSlot   =   stoi(sEquip) & stoi("0x00020");

                                if  (bSlot)    break;

                                nNewSlot    =   INVENTORY_SLOT_RIGHTHAND;
                                break;
                            }

                            nNewSlot    =   INVENTORY_SLOT_RIGHTHAND;
                            break;
                        }

                        //  neni to zbran
                        else
                        {

                        }
                    }
                    break;
                }
            }

            else

            if  (sType  ==  "NATURAL")
            {
                bEquip  =   TRUE;

                string  sEquip  =   Get2DAString("baseitems", "EquipableSlots", nBaseItem);
                int     bCArmor =   stoi(sEquip) & stoi("0x20000");

                //  kuze bytosti
                if  (bCArmor)
                {
                    object  oWield;

                    nNewSlot    =   INVENTORY_SLOT_CARMOUR;
                    oWield      =   GetItemInSlot(nNewSlot, oTarget);

                    if  (GetIsObjectValid(oWield))
                    {
                        nNewSlot    =   -1;
                        sMessage    =   R1 + "( ! ) " + W2 + "C�l se nem��e vybavit t�mhle p�edm�tem, proto�e ji� nem� voln� slot";

                        msg(oPC, sMessage, TRUE, FALSE);
                    }
                }

                //  zbran bytosti
                else
                {
                    int bCWeap  =   stoi(sEquip) & stoi("0x1C000");

                    if  (bCWeap)
                    {
                        object  oWield;

                        nNewSlot    =   INVENTORY_SLOT_CWEAPON_R;
                        oWield      =   GetItemInSlot(nNewSlot, oTarget);

                        if  (GetIsObjectValid(oWield))
                        {
                            nNewSlot    =   INVENTORY_SLOT_CWEAPON_L;
                            oWield  =   GetItemInSlot(nNewSlot, oTarget);

                            if  (GetIsObjectValid(oWield))
                            {
                                nNewSlot    =   INVENTORY_SLOT_CWEAPON_B;
                                oWield  =   GetItemInSlot(nNewSlot, oTarget);

                                //  neni volny slot
                                if  (GetIsObjectValid(oWield))
                                {
                                    nNewSlot    =   -1;
                                    sMessage    =   R1 + "( ! ) " + W2 + "C�l se nem��e vybavit t�mhle p�edm�tem, proto�e ji� nem� voln� slot";

                                    msg(oPC, sMessage, TRUE, FALSE);
                                }
                            }
                        }
                    }
                }
            }

            //  kdyz se ma dany predmet equipnout
            if  (bEquip)
            {
                //  pro dany typ predmetu nebylo mozne najit vhodny slot
                if  (nNewSlot   ==  -1)
                {
                    bValid      =   FALSE;
                    sMessage    =   R1 + "( ! ) " + W2 + "C�l nem��e vybavit tenhle p�edm�t";

                    object  oTaken  =   CopyItem(oItem, oPC, TRUE);

                    SetDroppableFlag(oTaken, TRUE);
                    SetPlotFlag(oTaken, FALSE);
                    SetPlotFlag(oItem, FALSE);
                    Destroy(oItem);
                }

                //  pro dany typ predmetu byl nalezen novy slot v inventari
                else
                {
                    bValid  =   TRUE;
                }

            }

            //  jedna se o obecny inventar
            else
            {
                bValid  =   TRUE;
            }

            //  zrcadleni predmetu do inventare cile
            if  (bValid)
            {
                if  (gli(GetModule(), "TEST"))
                SpawnScriptDebugger();
                oCopy   =   CopyItem(oItem, oTarget, TRUE);

                slo(oItem, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY", oCopy);
                SetDroppableFlag(oItem, FALSE);

                //  vybavit a overit vybavu, pokud se jednalo o EQUIPPED / NATURAL inventar
                if  (bEquip)
                {
                    DelayCommand(0.5f, AssignCommand(oTarget, ActionEquipItem(oCopy, nNewSlot)));
                    DelayCommand(0.7f, AssignCommand(oPC, VerifyEquipped(nNewSlot, oCopy, oItem, oTarget)));
                }

                //  oznami hracovi zisk predmetu
                if  (GetIsPC(oTarget))
                {
                    string  sName   =   GetIdentified(oCopy)    ?   GetName(oCopy)  :   GetStringByStrRef(stoi(Get2DAString("baseitems", "Name", GetBaseItemType(oCopy)))) + " (Neur�eno)";

                    sMessage    =   Y1 + "( ? ) " + W2 + "Z�skal(a) jsi p�edm�t [" + Y2 + sName + W2 + "] od PM";

                    msg(oTarget, sMessage, TRUE, FALSE);
                }
            }
        }
        break;

        //  odebrani predmetu
        case    INVENTORY_DISTURB_TYPE_REMOVED:
        {
            //  objekt, ktereho inventar je prohledavan jiz neexistuje a proto
            //  treba znicit neviditelny kontejner
            if  (!GetIsObjectValid(oTarget))
            {
                AssignCommand(oPC, ClearAllActions(TRUE));
                AssignCommand(oPC, ActionInteractObject(oContainer));
                SetPlotFlag(oItem, FALSE);
                Destroy(oItem);

                sMessage    =   R1 + "( !!! ) " + W2 + "Objekt, kter�ho invent�� je prohled�van ji� neexistuje a proto t�eba zni�it neviditeln� kontejner";

                msg(oPC, sMessage, TRUE, FALSE);
                return;
            }

            oCopy   =   glo(oItem, SYSTEM_CCS_PREFIX + "_INVENTORY_COPY");

            //  predmet, ktery PMko upravilo jiz neexistuje a proto bude jeho
            //  kopie (oItem) znicena
            if  (!GetIsObjectValid(oCopy))
            {
                SetPlotFlag(oItem, FALSE);
                Destroy(oItem);

                sMessage    =   R1 + "( ! ) " + W2 + "Origin�l p�edm�tu [" + R2 + GetName(oItem) + W2 + "] ji� neexistuje a proto byla jeho kopie zni�ena";

                msg(oPC, sMessage, TRUE, FALSE);
                return;
            }

            oPoss   =   GetItemPossessor(oCopy);

            //  predmet, ktery PMko upravilo se jiz nenachazi v inventari objektu,
            //  ktery je prohledavan - proto musi byt odebran i z neviditelneho
            //  kontejneru kvuli synchronizaci
            if  (oPoss  !=  oTarget)
            {
                SetPlotFlag(oItem, FALSE);
                Destroy(oItem);

                sMessage    =   R1 + "( ! ) " + W2 + "Origin�l p�edm�tu [" + R2 + GetName(oItem) + W2 + "] se ji� nenach�z� v invent��i vybran�ho objektu, kter� je prohled�v�n a proto byla jeho kopie zni�ena";

                msg(oPC, sMessage, TRUE, FALSE);
                return;
            }

            //  zniceni originalu, ktery je nahrazen kopii z neviditelneho
            //  kontejneru a zaslani zpravy cilu, kdyz je jedna o hrace
            SetPlotFlag(oCopy, FALSE);
            Destroy(oCopy);

            if  (GetIsPC(oTarget))
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Byl ti odebr�n p�edm�t [" + Y2 + GetName(oItem) + W2 + "] jedn�m PM";

                msg(oTarget, sMessage, TRUE, FALSE);
            }

            //  prepnuti "DROPPABLE" a "PLOT" znacky, protoze pri zrcadleni
            //  originalu se vypnuli kvuli ochrane
            SetDroppableFlag(oItem, TRUE);
            SetPlotFlag(oItem, FALSE);
        }
        break;
    }
}
