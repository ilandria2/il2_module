// +---------++----------------------------------------------------------------+
// | Name    || Exchange Trade Store (ETS) Core                                |
// | File    || vg_s0_ets_core.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 31-03-2018                                                     |
// | Updated || 31-03-2018                                                     |
// +---------++----------------------------------------------------------------+

#include "vg_s0_ets_const"
#include "vg_s0_sys_core_d"
#include "vg_s0_acs_core_c"

// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// handles the interaction with an item that just occured between source and target
// type values: "SELL" and "BUY"
void ETS_InteractWithStoreItem(string type, object source, object target, object item);

// resets the items in the carts of both the player and the store he was interacting with
void ETS_ResetCarts(object player, object store);

// accepts the items in the carts by both the player and the store he was interacting with
void ETS_AcceptCarts(object player, object store);

// gets the current balance of pending transaction between player and store
int ETS_GetBalance(object player, object store);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



void ETS_InteractWithStoreItem(string type, object source, object target, object item)
{
    object player;

    if (type == "BUY")
        player = target;

    else if (type == "SELL")
        player = source;

    int itemCart = gli(item, ETS_ + "CART");

    // sold/bought item - add to target's cart list
    if (!itemCart)
    {
        int carts = gli(target, ETS_ + "CART_O_ROWS");

        slo(target, ETS_ + "CART", item, -2);
        sli(item, ETS_ + "CART", ++carts);

        if (type == "SELL")
            SavePersistentData(item, ETS, type);

        else if (type == "BUY")
            SetStolenFlag(item, TRUE);

        msg(player, C1 + "DBG: " + type + " " + GetTag(item) + " itemCart: " + itos(carts));
    }

    // returned/taken item - remove from source's cart list
    else
    {
        int carts = gli(source, ETS_ + "CART_O_ROWS");
        int row;
        for (row = itemCart; row < carts; row++)
        {
            object nextItemInCart = glo(source, ETS_ + "CART", row + 1);

            if (!GetIsObjectValid(nextItemInCart))
                logentry("[" + ETS + "] During " + type + " invalid item in source's cart on row " + itos(row + 1));

            slo(source, ETS_ + "CART", nextItemInCart, row);
            sli(nextItemInCart, ETS_ + "CART", row);
        }

        msg(player, C1 + "DBG: " + type + " BACK " + GetTag(item) + " itemCart: " + itos(itemCart) + " / " + itos(carts));
        sli(item, ETS_ + "CART", -1, -1, SYS_M_DELETE);
        slo(source, ETS_ + "CART", OBJECT_INVALID, row, SYS_M_DELETE);
        sli(source, ETS_ + "CART_O_ROWS", --carts);

        if (type == "BUY")
            SavePersistentData(item, ETS, "TAKE");

        else if (type == "SELL")
            SetStolenFlag(item, FALSE);
    }

    SYS_Info(player, ETS, "BALANCE");
}



void _resetCarts(object player, object store, int accept)
{
    if (!GetIsObjectValid(player) || !GetIsPC(player))
    {
        logentry("[" + ETS + "] Invalid player object when resetting carts: " + GetTag(player));
        return;
    }

    if (!GetIsObjectValid(store))
    {
        logentry("[" + ETS + "] Invalid store object when resetting carts.");
        return;
    }

    int row;

    // reset store carts
    int storeCarts = gli(store, ETS_ + "CART_O_ROWS");
    string itemIds;

    for (row = 1; row <= storeCarts; row++)
    {
        object item = glo(store, ETS_ + "CART", row, SYS_M_DELETE);

        if (!GetIsObjectValid(item))
            continue;

        if (GetItemPossessor(item) != store)
        {
            logentry("[" + ETS + "] Invalid item possessor when resetting store cart items: " + GetTag(GetItemPossessor(item)));
            continue;
        }

        if (!accept)
        {
            object copy = CopyItem(item, player, TRUE);

            if (!GetIsObjectValid(copy))
            {
                logentry("[" + ETS + "] Unable to move item object when resetting store cart items: " + GetTag(item));
                continue;
            }

            msg(player, C1 + "DBG: Deleting store cart item: " + GetTag(item));
            SetPlotFlag(item, FALSE);
            DestroyObject(item, 0.01f);
        }

        int itemId = gli(item, ETS_ + "STORE_ITEM");

        if (row > 1)
            itemIds += ",";
        itemIds += itos(itemId);
    }

    if (itemIds != "")
    {
        string saveType = accept ? "STORE_ITEMS" : "DELETE_ITEMS";

        sls(store, ETS_ + "ITEM_IDS", itemIds);
        SavePersistentData(store, ETS, saveType);
    }

    sli(store, ETS_ + "CART_O_ROWS", -1, -1, SYS_M_DELETE);

    // reset player carts
    int playerCarts = gli(player, ETS_ + "CART_O_ROWS");
    itemIds = "";

    for (row = 1; row <= playerCarts; row++)
    {
        object item = glo(player, ETS_ + "CART", row, SYS_M_DELETE);

        if (!GetIsObjectValid(item))
            continue;

        if (GetItemPossessor(item) != player)
        {
            logentry("[" + ETS + "] Invalid item possessor when resetting player cart items: " + GetTag(GetItemPossessor(item)));
            continue;
        }

        if (!accept)
        {
            object copy = CopyItem(item, store, TRUE);

            if (!GetIsObjectValid(copy))
            {
                logentry("[" + ETS + "] Unable to move item object when resetting player cart items: " + GetTag(item));
                continue;
            }

            SetStolenFlag(copy, FALSE);

            msg(player, C1 + "DBG: Deleting player cart item: " + GetTag(item));
            SetPlotFlag(item, FALSE);
            DestroyObject(item, 0.01f);
        }

        else
        {
            int itemId = gli(item, ETS_ + "STORE_ITEM");

            if (row > 1)
                itemIds += ",";
            itemIds += itos(itemId);
        }

        SetStolenFlag(item, FALSE);
    }

    if (itemIds != "" && accept)
    {
        sls(store, ETS_ + "ITEM_IDS", itemIds);
        SavePersistentData(store, ETS, "DELETE_ITEMS");
    }

    sli(player, ETS_ + "CART_O_ROWS", -1, -1, SYS_M_DELETE);
    slo(player, ETS_ + "TARGET", OBJECT_INVALID);
    slo(store, ETS_ + "TARGET", OBJECT_INVALID);

    string info = accept ? "TRANSACTION_ACCEPTED" : "TRANSACTION_CANCELED";
    SYS_Info(player, ETS, info);
}

void ETS_ResetCarts(object player, object store)
{
    _resetCarts(player, store, FALSE);
}

int _checkStoreParties(object player, object store)
{
    if (!GetIsObjectValid(player))
    {
        logentry("[" + ETS + "] Player object is invalid.");
        return FALSE;
    }

    if (!GetIsObjectValid(store))
    {
        logentry("[" + ETS + "] Store object is invalid.");
        return FALSE;
    }

    if (!GetIsPC(player))
    {
        logentry("[" + ETS + "] Player object is not a player.");
        return FALSE;
    }

    if (GetObjectType(store) != OBJECT_TYPE_STORE)
    {
        logentry("[" + ETS + "] Store object is not a store.");
        return FALSE;
    }

    return TRUE;
}

int _checkMoney(object player, object store, int balance)
{
    // player owes to store
    if (balance < 0)
    {
        balance = -balance;
        int money = ACS_GetMoney(player);

        if (money < balance)
        {
            SYS_Info(player, ETS, "NOT_ENOUGH_MONEY_&" + itos(money) + "&&" + itos(balance) + "&");
            return FALSE;
        }
    }

    // store owes to player
    else if (balance > 0)
    {
        int confirmed = gli(player, ETS_ + "CONFIRM_MONEY", -1, SYS_M_DELETE);

        // store doesn't have enough money but player confirmed he/she is ok with that
        if (confirmed)
            return 2;

        int money = ACS_GetMoney(store);

        if (money < balance)
        {
            sli(player, ETS_ + "CONFIRM_MONEY", TRUE);
            SYS_Info(player, ETS, "NOT_ENOUGH_MONEY_&" + itos(money) + "&&" + itos(balance) + "&");
            return FALSE;
        }
    }

    return TRUE;
}

void ETS_AcceptCarts(object player, object store)
{
    if (!_checkStoreParties(player, store))
        return;

    int playerCarts = gli(player, ETS_ + "CART_O_ROWS");
    int storeCarts = gli(store, ETS_ + "CART_O_ROWS");

    if (!playerCarts && !storeCarts)
        return;

    int balance = ETS_GetBalance(player, store);
    int haveMoney = _checkMoney(player, store, balance);

    if (!haveMoney)
        return;

    int balance_req = balance;

    if (balance != 0)
    {
        // store owes to player but doesnt have enough and player confirms he/she is ok with that
        // then only total amount what the store currently has will be transferred
        if (balance > 0 && haveMoney == 2)
            balance = ACS_GetMoney(store);

        if (balance != 0)
        {
            ACS_GiveMoney(player, balance);
            ACS_GiveMoney(store, -balance);
            SavePersistentData(store, ETS, "MONEY");
        }
    }

    SavePersistentData(store, ETS, "TRANSACTION_&" + itos(playerCarts) + "&&" + itos(storeCarts) + "&&" + itos(balance) + "&&" + itos(balance_req) + "&");
    _resetCarts(player, store, TRUE);
}

int ETS_GetBalance(object player, object store)
{
    //TODO: implement real logic
    int playerCarts = gli(player, ETS_ + "CART_O_ROWS");
    int storeCarts = gli(store, ETS_ + "CART_O_ROWS");

    return 10 * storeCarts - 10 * playerCarts;
}
