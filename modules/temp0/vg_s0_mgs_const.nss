// +---------++----------------------------------------------------------------+
// | Name    || Music Generator System (MGS) Constants                         |
// | File    || vg_s0_mgs_const.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 04-09-2010                                                     |
// | Updated || 04-09-2010                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu MGS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_MGS_NAME                 =   "Music Generator System";
const string    SYSTEM_MGS_PREFIX               =   "MGS";
const string    SYSTEM_MGS_VERSION              =   "1.00";
const string    SYSTEM_MGS_AUTHOR               =   "VirgiL";
const string    SYSTEM_MGS_DATE                 =   "04-09-2010";
const string    SYSTEM_MGS_UPDATE               =   "04-09-2010";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const int       MGS_HEARTBEATS_MUSIC            =   50;

