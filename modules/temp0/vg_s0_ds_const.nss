// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Constants                                    |
// | File    || vg_s0_ds_const.nss                                             |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 02-07-2008                                                     |
// | Updated || 29-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu DS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_DS_NAME                      =   "Death System";
const string    SYSTEM_DS_PREFIX                    =   "DS";
const string    SYSTEM_DS_VERSION                   =   "2.00";
const string    SYSTEM_DS_AUTHOR                    =   "VirgiL";
const string    SYSTEM_DS_DATE                      =   "02-07-2008";
const string    SYSTEM_DS_UPDATE                    =   "29-07-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



//  database
/*
const string    DS_DB_TABLE_CORPSES                 =   "ds_corpses";
const string    DS_DB_COLUMN_CORPSE                 =   "lCorpse";
const string    DS_DB_COLUMN_PLAYER_ID              =   "idPlayer";
const string    DS_DB_COLUMN_KILLER                 =   "bKiller";
*/

//  scripts
const string    DS_RETURN_PENALIZATION              =   "vg_s1_ds_penale";

//  coefs
const float     DS_XP_PERCENT_PENALTY_XP_CHARACTER  =   50.0f;  // postava
const float     DS_XP_PERCENT_PENALTY_XP_BCS        =   25.0f;  // boj
const float     DS_XP_PERCENT_PENALTY_XP_SCS        =   25.0f;  // kouzla
const float     DS_XP_PERCENT_PENALTY_XP_ACS        =   0.0f;   // kraft
const float     DS_XP_PERCENT_PENALTY_KILLER_PC     =   0.1f;   // smrt hracem
const int       DS_PENALE_LEVEL_LIMIT               =   3;

const float     DS_BLEEDING_SEQUENCE_TIME           =   12.0f;

//  tags
const string    DS_TAG_DEATH_CORPSE                 =   "VG_P_S1_DSCORPSE";
const string    DS_TAG_SPHERE_RETURN                =   "VG_W_S1_SPHERE_R";
const string    DS_TAG_SPHERE_JUMP                  =   "VG_W_S1_SPHERE_D";
const string    DS_DIE_WAYPOINT                     =   "VG_W_S1_DS_DEAD";

