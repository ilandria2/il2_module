// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Refresh BAB skript                    |
// | File    || vg_s1_bcs_refbab.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Po prihlaseni hrace obnovi BAB
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    BCS_RefreshBAB(oPC);
}

