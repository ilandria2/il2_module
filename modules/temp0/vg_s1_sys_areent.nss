// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Remember region script                  |
// | File    || vg_s1_sys_areent.nss                                           |
// +---------++----------------------------------------------------------------+

#include    "vg_s0_sys_core_d"
#include    "vg_s0_lpd_core_c"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void main()
{
    object player = GetEnteringObject();

    RemoveFromParty(player);

    if (!GetIsPC(player)) return;
    if (GetIsDM(player)) return;

    // ignore invalid player
    int playerId = gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    if (!playerId)
        return;

    object wpInit = GetNearestObjectByTag(SYS_TAG_PLAYER_START_INIT, player);

    if (GetIsObjectValid(wpInit))
    {
        //  debug mode
        if (gli(GetModule(), "DEBUG"))
            ExecuteScript("_test", player);

        //  live mode - move to start_il waypoint
        else
        {
            AssignCommand(player, JumpToLocation(GetLocation(wpInit)));
        }
    }
}
