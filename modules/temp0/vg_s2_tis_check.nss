// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) - Check skript                   |
// | File    || vg_s2_tis_check.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Overi podminky TIS vlaken
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"
#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void    main()
{
    int     bResult =   TRUE;
    object  oSource =   OBJECT_SELF;
    object  oPC     =   LPD_GetPC();
    string  sParam  =   gls(oSource, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if(gli(oPC, "tis_debug"))SpawnScriptDebugger();
    sParam  =   upper(sParam);



//  ----------------------------------------------------------------------------
//  ACT -- Detekce cinnosti

    if  (sParam ==  "ACT")
    {
        bResult = TIS_CheckCurrentObjectAndDetectActions(oPC);
    }

//  ACT -- Detekce cinnosti
//  ----------------------------------------------------------------------------

    else

//  ----------------------------------------------------------------------------
//  COA -- Detekce aktualni cinnosti

    if  (sParam ==  "COA")
    {
        bResult = TIS_CheckCurrentObjectAndAction(oPC);
    }

//  COA -- Detekce aktualni cinnosti
//  ----------------------------------------------------------------------------



    sli(oSource, SYS_VAR_RETURN, bResult);
}
