// Text Interaction System (TIS) Interaction script - TAKE

#include "vg_s0_tis_inter"
#include "vg_s0_acs_core_c"

const int ACTION_TYPE  = 107;

//////// ACTION'S SPECIAL CONDITIONS
int Check()
{
    return TRUE;
}

//////// ACTION'S BODY
void Do()
{
    if (gli(this.Player, "ids_convert"))SpawnScriptDebugger();
    // instant sequence
    if  (this.DelaySequence == 0.0)
    {
        this.DelaySequence = 2.0f;

        AssignCommand(this.Player, ClearAllActions());
        DelayCommand(0.05, AssignCommand(this.Player, ActionSpeakString("*Picks up something*")));
        DelayCommand(0.10, AssignCommand(this.Player, ActionPlayAnimation(ANIMATION_LOOPING_GET_LOW, 1.0, 2.0)));
    }

    // delayed sequence
    else
    {
        // last sequence
        StopAnimation(this.Player);

        string itemTag = gls(this.Object, IDS_ + "ITEM_TAG");
        string itemTech = gls(this.Object, IDS_ + "ITEM_TECH");
        int weight = gli(this.Object, IDS_ + "ITEM_WEIGHT");
        int durMax = gli(this.Object, IDS_ + "ITEM_DBMAX");
        int durCur = gli(this.Object, IDS_ + "ITEM_DBCUR");
        int stack = gli(this.Object, IDS_ + "ITEM_STACK");
        int itemType = gli(this.Object, IDS_ + "ITEM_TYPE");

        int itemOid = GetTechPart(itemTech, TECH_PART_OBJECT_ID);
        int recipe = GetTechPart(itemTech, TECH_PART_RECIPE_ID);
        int isWeighted = IsWeightBasedItemType(itemType);
        int amount = stack;

        if (isWeighted)
            amount = ACS_GetItemStackSize(weight, left(itemTag, 16));

        object item = ACS_CreateItemUsingRecipe(recipe, amount, li(), this.Player, TRUE);

        if (GetIsObjectValid(item))
        {
            IDS_UpdateItem(this.Player, item, durCur, durMax, FALSE);
            SetTechPart(item, TECH_PART_OBJECT_ID, itemOid);
            IDS_CheckPlacedItemsInArea(GetTag(GetArea(this.Player)));
            Destroy(this.Object, 0.1);
        }

        // last sequence
        this.DelaySequence = -999.0;
    }
}

/////// main handler
void main()
{
    if (this.Player == OBJECT_INVALID)
        this = TIS_ConstructInteraction(ACTION_TYPE);

    string param = gls(this.Player, SYS_VAR_PARAMETER, -1, SYS_M_DELETE);

    if (param == "")
        TIS_HandleInteraction(this);
    else if (param == TIS_PARAMETER_CHECK)
        sli(this.Player, SYS_VAR_RETURN, Check());
    else if (param == TIS_PARAMETER_DO)
    {
        Do();

        if (this.DelaySequence == 0.0)
            GUI_StopMenu(this.Player);

        slf(this.Player, TIS_ + "SEQ_DELAY", this.DelaySequence);
    }
}

