
#include "vg_s0_gui_core"
#include "vg_s0_tis_core_c"

//////////////// 4. vg_s1_tis_info
///// update GUI variables to reflect the current state of the system using the GUI


void main()
{
    object player = OBJECT_SELF;
    int objCount = gli(player, TIS_ + "OBJECT_O_ROWS");

    if (!objCount)
        return;

    if (gli(player, "render"))SpawnScriptDebugger();

    int gridRow = 1;

    int objUsed = gli(player, TIS_ + "OBJECT_USED");
    object objCurrent = glo(player, TIS_ + "OBJECT", objUsed);
    string objTag = GetTag(objCurrent);
    int objType = gli(__TIS(), TIS_ + "TAG_" + objTag + "_OT");
    int objSubType = gli(__TIS(), TIS_ + "TAG_" + objTag + "_OS");

    // object is selected - show actions
    if (objUsed)
    {
        int actCount = gli(player, TIS_ + "ACTION_I_ROWS");
        if (actCount == 0)
            SYS_Info(player, TIS, "NO_ACTIONS");

        for (gridRow = 0; gridRow < actCount; gridRow++)
        {
            int actSubType = gli(player, TIS_ + "ACTION", gridRow + 1);
            int vfxAction = gli(__TIS(), TIS_ + "AS_" + itos(actSubType) + "_VFX");
            string nameAction = gls(player, TIS_ + "N_A" + itos(gridRow + 1));
            GUI_MenuSetObjectVfxId(player, GUI_LAYOUT_INTERACT_GRID + gridRow, vfxAction);
            GUI_MenuSetObjectName(player, GUI_LAYOUT_INTERACT_GRID + gridRow, nameAction);
        }

        string nameObject = gls(player, TIS_ + "N_O" + itos(objUsed));
        GUI_MenuSetObjectName(player, GUI_LAYOUT_CLOSE_BUTTON, nameObject + C_NORM + "\nClose");
    }

    // object selection needed - show objects
    else
    {
        for (gridRow = 0; gridRow < objCount; gridRow++)
        {
            objCurrent = glo(player, TIS_ + "OBJECT", gridRow + 1);
            objTag = GetTag(objCurrent);
            objType = gli(__TIS(), TIS_ + "TAG_" + objTag + "_OT");
            objSubType = gli(__TIS(), TIS_ + "TAG_" + objTag + "_OS");
            int vfxObject = gli(__TIS(), TIS_ + "OT_" + itos(objType) + "_VFX");
            string nameObject = gls(player, TIS_ + "N_O" + itos(gridRow + 1));
            GUI_MenuSetObjectVfxId(player, GUI_LAYOUT_INTERACT_GRID + gridRow, vfxObject);
            GUI_MenuSetObjectName(player, GUI_LAYOUT_INTERACT_GRID + gridRow, nameObject);
        }

        GUI_MenuSetObjectName(player, GUI_LAYOUT_CLOSE_BUTTON, C_BOLD + "Interaction menu" + C_NORM + "\nClose");
    }

    // reset unused grid objects
    for (; gridRow < GUI_LAYOUT_INTERACT_GRID_OBJECTS; gridRow++)
    {
        GUI_DisableLayoutObject(player, GUI_LAYOUT_INTERACT_GRID + gridRow);
        GUI_MenuSetObjectVfxId(player, GUI_LAYOUT_INTERACT_GRID + gridRow, GUI_VFX_EMPTY);
        GUI_MenuSetObjectName(player, GUI_LAYOUT_INTERACT_GRID + gridRow, "");
    }
}


