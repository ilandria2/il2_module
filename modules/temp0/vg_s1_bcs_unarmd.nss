// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Unarmed BAB skript                    |
// | File    || vg_s1_bcs_unarmed.nss                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pri unequipnuti zbrane navysi BAB (base attack bonus) hracske postave pro
    neozbrojeny boj
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetPCItemLastUnequippedBy();

    BCS_RefreshBAB(oPC);
}

