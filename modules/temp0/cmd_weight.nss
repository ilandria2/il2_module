// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_weight.nss                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "WEIGHT"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_sys_core_o"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "WEIGHT";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nWeight =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oItem   =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;

    SpawnScriptDebugger();
    if  (GetObjectType(oItem)   !=  OBJECT_TYPE_ITEM)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type - must be an item";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    if  (nWeight    >   0)
    {
        sMessage    =   G1 + "( ! ) " + W2 + "Changing the weight of an item [" + G2 + GetTag(oItem) + " / " + GetName(oItem) + W2 + "] to: " + itos(nWeight);

        IPS_SetItemWeight(oItem, nWeight);
    }

    else
    sMessage    =   Y1 + "( ? ) " + W2 + "Weight of an item [" + G2 + GetTag(oItem) + " / " + GetName(oItem) + W2 + "] is: W:" + itos(IPS_GetItemWeight(oItem, FALSE)) + " P:" + itos(IPS_GetItemWeight(oItem, TRUE));

    msg(oPC, sMessage, TRUE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

