// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Delete sold item script                 |
// | File    || vg_s1_sys_delsto.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kdyz hrac proda predmet a dany obchod ma nastavenouo promennou
    "SYS_BUY_ONLY" na TRUE, pak prodanej predmet bude znicen a nelze ho odkoupit
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oItem   =   GetModuleItemLost();
    object  oPoss   =   GetItemPossessor(oItem);

    if  (GetObjectType(oPoss)   ==  OBJECT_TYPE_STORE)
    {
        int bBuyOnly    =   gli(oPoss, SYSTEM_SYS_PREFIX + "_BUY_ONLY");

        if  (bBuyOnly)
        {
            SetPlotFlag(oItem, FALSE);
            DestroyObject(oItem, 0.05f);
        }
    }
}

