// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_gold.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "GOLD"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "GOLD";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nAmount     =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     bSet        =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    object  oCreature   =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oCreature))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� bytost";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatne mnozstvi
    if  (nAmount    <=  0)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� mno�stv�";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int nGold   =   GetGold(oCreature);
    int nValue  =   ei(nGold, nAmount, bSet);

    if  (nValue >   nGold)  GiveGoldToCreature(oCreature, abs(nValue - nGold)); else
    if  (nValue <   nGold)  TakeGoldFromCreature(abs(nValue - nGold), oCreature, TRUE);

    nGold       =   GetGold(oCreature);
    sMessage    =   Y1 + "( ? ) " + W2 + "Bytost [" + G2 + GetName(oCreature) + W2 + "] zlato [" + itos(nGold) + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
