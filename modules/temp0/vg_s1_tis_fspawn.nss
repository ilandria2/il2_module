// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Force spawn groups script        |
// | File    || vg_s1_tis_fspawn.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Spawnovaci systemu dynamickych objektu (soucast systemu TIS)
    Vynucene spawne vsechny skupiny spawnpointu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    update(string sGroup)
{
    object  oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + upper(sGroup));

    TIS_UpdateSpawnGroup(sGroup, TRUE);
    //sli(oMaster, SYSTEM_TIS_PREFIX + "_FSPAWN", TRUE);
}



void    main()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");
    string  sGroup;
    int     nRows, n;

    nRows   =   gli(oSystem, SYSTEM_TIS_PREFIX + "_GROUP_S_ROWS");
    n       =   1;

    while   (n  <=  nRows)
    {
        sGroup  =   gls(oSystem, SYSTEM_TIS_PREFIX + "_GROUP", n);

        DelayCommand((n++ - 1) * 0.1f, update(sGroup));
    }
}

