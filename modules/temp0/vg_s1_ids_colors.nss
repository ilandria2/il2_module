#include "vg_s0_oes_const"
#include "vg_s0_ids_core_c"

void main()
{
    if (GetUserDefinedEventNumber() != OES_EVENT_NUMBER_O_PLAYER_LOGIN)
        return;

    object player = glo(GetModule(), "USERDEFINED_EVENT_OBJECT");
    if (!GetIsPC(player))
        return;

    object armor = GetItemInSlot(INVENTORY_SLOT_CHEST, player);
    int c;
    for (c = 0; c < ITEM_APPR_ARMOR_NUM_COLORS; c++)
        sli(player, SYSTEM_IDS_PREFIX + "_LAST_COLOR", GetItemAppearance(armor, ITEM_APPR_TYPE_ARMOR_COLOR, c), c);
}
