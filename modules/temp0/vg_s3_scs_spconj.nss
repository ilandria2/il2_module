// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Conjure event script                   |
// | File    || vg_s3_scs_spconj.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-05-2008                                                     |
// | Updated || 14-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosti "SCS ConjureSpell"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_const"
#include    "vg_s0_scs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int         nSpellCast  =   GetSpellId();
    object      oTarget     =   GetSpellTargetObject();
    location    lTarget     =   GetSpellTargetLocation();

    //  NPC
    if  (!GetIsPC(OBJECT_SELF))
    {
        if  (oTarget    !=  OBJECT_INVALID) AssignCommand(OBJECT_SELF, ActionCastSpellAtObject(nSpellCast - SCS_FIRST_CONJURE_SPELL, oTarget, METAMAGIC_NONE, TRUE));
        else                                AssignCommand(OBJECT_SELF, ActionCastSpellAtLocation(nSpellCast - SCS_FIRST_CONJURE_SPELL, lTarget, METAMAGIC_NONE, TRUE));
    }

    //  Hrac
    else
    {   //SpawnScriptDebugger();
        object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

        //  N-te kliknuti na SCS kouzlo (vyber metamagie)
        //if  (nSpellCast !=  SYS_SPELL_TARGET)
        if  (TRUE)
        {
            int nCurrent    =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SUB");
            int nSpell      =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL");
            int nSpellGroup =   (nSpellCast <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpellCast - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
            int nSpellOrig  =   (nSpellCast <   SCS_FIRST_CONJURE_SPELL)    ?   nSpellCast  :   nSpellCast - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));
            int nLast       =   5;
            int nStart      =   (nCurrent   ==  nLast)  ?   0   :   nCurrent;
            int n           =   (nSpell !=  nSpellCast) ?   0   :   nStart + 1;
            int nEnd        =   nLast;
            int nNew;
            string  s2da;

            while   (n  <=  nEnd)
            {
                s2da    =   Get2DAString("spells", "SubRadSpell" + itos(n), nSpellOrig);

                if  (s2da   !=  "")
                {
                    nNew    =   n;
                    break;
                }

                if  (n  ==  nLast)
                {
                    n       =   1;
                    nEnd    =   nStart;
                }

                else    n++;
            }

            nSpell  =   nSpellCast;

            sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", nSpellCast);
            sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SUB", nNew);
            sls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "&" + SCS_NCS_SPELL_EVENT_CONJURE + "&_&" + itos(nSpell) + "&");
            SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_SWITCH_&" + itos(nSpell) + "&_&" + itos(nNew)+"&");
        }

        //  pouziti featu Target (kontrola podminek a kouzleni)
        else
        {
            if  (!GetIsDM(OBJECT_SELF)
            &&  !gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT"))
            {
                ExecuteScript(SCS_NCS_CONDITION_RESREF, OBJECT_SELF);

                int bResult =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT", -1, SYS_M_DELETE);

                if  (!bResult)  return;
            }

            int nAction =   GetCurrentAction(OBJECT_SELF);

            if  (nAction    !=  ACTION_CASTSPELL
            &&  nAction     !=  ACTION_ITEMCASTSPELL
            &&  nAction     !=  ACTION_INVALID)
            {
                AssignCommand(OBJECT_SELF, ClearAllActions(TRUE));
            }

            string  sParameters =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS");
            int     nSub        =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SUB");
            int     nSpell      =   stoi(GetNthSubString(sParameters, 2));
            int     nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
            int     nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));
            int     nMeta       =   gli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_META", nSpellGroup);
            int     nInnate     =   stoi(Get2DAString("spells", "Innate", nSpellOrig));
            int     nAdd        =   (nInnate    <=  3)  ?   0   :   (nInnate    <=  6)  ?   1   :   2;
            int     nFeat, bMeta;

            bMeta   =   SCS_GetSpellHasMetamagicType(METAMAGIC_QUICKEN, nSpellOrig, TRUE);

            if  (bMeta)
            {
                nFeat   =   FEAT_EPIC_AUTOMATIC_QUICKEN_1 + nAdd;
                nMeta   +=  (GetHasFeat(nFeat, OBJECT_SELF))    ?   METAMAGIC_QUICKEN   :   METAMAGIC_NONE;
            }

            bMeta   =   SCS_GetSpellHasMetamagicType(METAMAGIC_SILENT, nSpellOrig, TRUE);

            if  (bMeta)
            {
                nFeat   =   FEAT_EPIC_AUTOMATIC_SILENT_SPELL_1 + nAdd;
                nMeta   +=  (GetHasFeat(nFeat, OBJECT_SELF))    ?   METAMAGIC_SILENT    :   METAMAGIC_NONE;
            }

            bMeta   =   SCS_GetSpellHasMetamagicType(METAMAGIC_STILL, nSpellOrig, TRUE);

            if  (bMeta)
            {
                nFeat   =   FEAT_EPIC_AUTOMATIC_STILL_SPELL_1 + nAdd;
                nMeta   +=  (GetHasFeat(nFeat, OBJECT_SELF))    ?   METAMAGIC_STILL     :   METAMAGIC_NONE;
            }

            //  subradialni kouzlo
            if  (nSub   >   0)
            {
                nSpellOrig  =   stoi(Get2DAString("spells", "SubRadSpell" + itos(nSub), nSpellOrig));
            }

            if  (oTarget    !=  OBJECT_INVALID) AssignCommand(OBJECT_SELF, SCS_CastSpellAtObject(nSpellOrig, oTarget, nMeta));
            else                                AssignCommand(OBJECT_SELF, SCS_CastSpellAtLocation(nSpellOrig, lTarget, nMeta));
        }
    }
}
