// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_invis.nss                                                  |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "INVIS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "INVIS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    object  oCreature   =   CCS_GetConvertedObject(sLine, 1, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oCreature))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neni to bytost
    if  (GetObjectType(oCreature)   !=  OBJECT_TYPE_CREATURE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int bInvis      =   gli(oCreature, SYSTEM_CCS_PREFIX + "_DM_INVISIBLE");

    if  (!bInvis)
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Creature [" + G2 + GetName(oCreature) + W2 + "] becomes [" + R2 + "invisible" + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectCutsceneGhost()), oCreature);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectVisualEffect(VFX_DUR_CUTSCENE_INVISIBILITY)), oCreature);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, SupernaturalEffect(EffectInvisibility(INVISIBILITY_TYPE_IMPROVED)), oCreature);
        sli(oCreature, SYSTEM_CCS_PREFIX + "_DM_INVISIBLE", TRUE);
    }

    else
    {
        effect  eEffect     =   GetFirstEffect(oCreature);
        int     nType       =   GetEffectType(eEffect);
        int     nSubType    =   GetEffectSubType(eEffect);

        while   (GetIsEffectValid(eEffect))
        {
            if  ((nType     ==  EFFECT_TYPE_CUTSCENEGHOST
            ||  nType       ==  EFFECT_TYPE_VISUALEFFECT
            ||  nType       ==  EFFECT_TYPE_IMPROVEDINVISIBILITY)
            &&  nSubType    ==  SUBTYPE_SUPERNATURAL)
            {
                RemoveEffect(oCreature, eEffect);
            }

            eEffect     =   GetNextEffect(oCreature);
            nType       =   GetEffectType(eEffect);
            nSubType    =   GetEffectSubType(eEffect);
        }

        sMessage    =   Y1 + "( ? ) " + W2 + "Creature [" + G2 + GetName(oCreature) + W2 + "] becomes [" + G2 + "visible" + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        sli(oCreature, SYSTEM_CCS_PREFIX + "_DM_INVISIBLE", FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
