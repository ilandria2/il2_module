// Target ability impact script handler for PLE system

#include "vg_s0_ple_core"

void main()
{
    object player = OBJECT_SELF;
    if (!PLE_GetLoopFlag(player)) return;

    object target = PLE_GetTarget(player);
    if (!GetIsObjectValid(target)) return;

    location locTarget = GetSpellTargetLocation();
    int mode = PLE_GetAdjustMode(player);
    if (mode != PLE_MODE_ORIENTATION && GetDistanceBetweenLocations(locTarget, GetLocation(player)) > PLE_INTERACT_DISTANCE)
    {
        SYS_Info(player, PLE, "MAX_DIST");
        return;
    }

    if (gli(player, "ple_target"))SpawnScriptDebugger();

    location locPlc = GetLocation(target);
    vector posTarget = GetPositionFromLocation(locTarget);
    vector posPlc = GetPositionFromLocation(locPlc);
    float orTarget = GetFacing(target);

    switch (mode)
    {
        case PLE_MODE_TERRAIN:
            break;

        case PLE_MODE_SIDEWAYS:
            posTarget.z = posPlc.z;
            break;

        case PLE_MODE_ORIENTATION:
            posTarget = posPlc;
            orTarget = PLE_CalculateOrientation(locPlc, locTarget);
            DelayCommand(0.5, SYS_Info(player, PLE, "TARGET_ORIENT"));
            break;
    }

    PLE_RelocateObject(player, target, Location(GetArea(player), posTarget, orTarget));
}
