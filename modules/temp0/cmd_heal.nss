// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_heal.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 27-06-2009                                                     |
// | Updated || 27-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "HEAL"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_ds_core_c"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "HEAL";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nHitPoints  =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oHealed     =   CCS_GetConvertedObject(sLine, 2, FALSE);

    //  Neplatny cil
    if  (!GetIsObjectValid(oHealed))
    {
        string  sMessage    =   R1 + "( ! ) " + W2 + "Invalid creature";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (nHitPoints ==  -1)
    {
        effect  eTemp   =   GetFirstEffect(oHealed);
        int     nType   =   GetEffectType(eTemp);

        while   (GetIsEffectValid(eTemp))
        {
            if  (nType  !=  EFFECT_TYPE_ABILITY_DECREASE
            &&  nType   !=  EFFECT_TYPE_AC_DECREASE
            &&  nType   !=  EFFECT_TYPE_ARCANE_SPELL_FAILURE
            &&  nType   !=  EFFECT_TYPE_ATTACK_DECREASE
            &&  nType   !=  EFFECT_TYPE_CURSE
            &&  nType   !=  EFFECT_TYPE_DAMAGE_DECREASE
            &&  nType   !=  EFFECT_TYPE_DAMAGE_IMMUNITY_DECREASE
            &&  nType   !=  EFFECT_TYPE_MISS_CHANCE
            &&  nType   !=  EFFECT_TYPE_MOVEMENT_SPEED_DECREASE
            &&  nType   !=  EFFECT_TYPE_NEGATIVELEVEL
            &&  nType   !=  EFFECT_TYPE_POISON
            &&  nType   !=  EFFECT_TYPE_SAVING_THROW_DECREASE
            &&  nType   !=  EFFECT_TYPE_SKILL_DECREASE
            &&  nType   !=  EFFECT_TYPE_SPELL_RESISTANCE_DECREASE
            &&  nType   !=  EFFECT_TYPE_TURN_RESISTANCE_DECREASE
            &&  nType   !=  EFFECT_TYPE_NEGATIVELEVEL
            &&  nType   !=  EFFECT_TYPE_SLOW)
            RemoveEffect(oHealed, eTemp);

            eTemp   =   GetNextEffect(oHealed);
            nType   =   GetEffectType(eTemp);
        }

        return;
    }

    if  (GetIsDead(oHealed))
    {
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), oHealed);
        slo(GetModule(), "USERDEFINED_EVENT_OBJECT", oHealed);
        SignalEvent(GetModule(), EventUserDefined(OES_EVENT_O_ON_PLAYER_RESURRECT));

        //nHitPoints  =   GetMaxHitPoints(oHealed);
    }

    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(nHitPoints), oHealed);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
