// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) ReturnFromSphere script                      |
// | File    || vg_s1_ds_return.nss                                            |
// | Author  || VirgiL                                                         |
// | Created || 01-07-2008                                                     |
// | Updated || 30-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery prenese hrace ze sfery smrti do sveta
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_core_c"
#include    "vg_s0_lpd_core_c"
#include    "inc_draw"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   LPD_GetPC();

    DrawSpring(DURATION_TYPE_INSTANT, VFX_IMP_DEATH_L, GetLocation(oPC), 6.0f, 0.0f, 8.0f, 0.0f, 0.0f, 15, 14.0f, 3.0f, 0.0f, "z");
    DelayCommand(3.0f, DrawLineFromCenter(DURATION_TYPE_INSTANT, VFX_IMP_DEATH_L, GetLocation(oPC), 10.0f, 0.0f, 0.0f, 10, 0.5f, "x"));
    DelayCommand(4.0f, DS_ReturnFromSphere(oPC));
    DelayCommand(4.0f, SYS_Info(oPC, SYSTEM_DS_PREFIX, "RETURN"));
    DelayCommand(3.0f, ExecuteScript(DS_RETURN_PENALIZATION, oPC));


    /*
    object  oPC         =   (GetIsPC(OBJECT_SELF))  ?   OBJECT_SELF :
                            (GetObjectType(OBJECT_SELF) ==  OBJECT_TYPE_CREATURE)   ?   LPD_GetPC() :   GetLastUsedBy();
    object  oCorpse     =   glo(oPC, SYSTEM_DS_PREFIX + "_CORPSE");
    int     nAlignment  =   GetAlignmentGoodEvil(oPC);
    int     nVFX        =   (nAlignment ==  ALIGNMENT_GOOD) ?   VFX_IMP_KNOCK   :
                            (nAlignment ==  ALIGNMENT_EVIL) ?   VFX_IMP_REDUCE_ABILITY_SCORE    :   VFX_IMP_HOLY_AID;

    DrawSpring(DURATION_TYPE_INSTANT, nVFX, GetLocation(oPC), 6.0f, 0.0f, 8.0f, 0.0f, 0.0f, 30, 14.0f, 3.0f, 0.0f, "z");
    DelayCommand(3.0f, DrawLineFromCenter(DURATION_TYPE_INSTANT, nVFX, GetLocation(oPC), 10.0f, 0.0f, 0.0f, 10, 0.5f, "x"));

    if  (GetIsObjectValid(oCorpse))
    {
        DelayCommand(4.0f, DS_ReturnFromSphere(oPC));
        DelayCommand(4.0f, SYS_Info(oPC, SYSTEM_DS_PREFIX, "RETURN"));

        DelayCommand(3.0f, ExecuteScript(DS_RETURN_PENALIZATION, oPC));
    }
    */
}
