// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_appearance.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SETAPP"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SETAPP";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nModel      =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    string  sPartName   =   CCS_GetConvertedString(sLine, 2, FALSE);
    object  oCreature   =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oCreature))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny typ objektu
    if  (GetObjectType(oCreature)   !=  OBJECT_TYPE_CREATURE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "The target must be a " + R1 + "creature";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny model
    if  (nModel <   0)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid model";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    sPartName   =   GetStringUpperCase(sPartName);

    int bPart   =   (sPartName  ==  "RFOOT")        ?   CREATURE_PART_RIGHT_FOOT        :
                    (sPartName  ==  "LFOOT")        ?   CREATURE_PART_LEFT_FOOT         :
                    (sPartName  ==  "RSHIN")        ?   CREATURE_PART_RIGHT_SHIN        :
                    (sPartName  ==  "LSHIN")        ?   CREATURE_PART_LEFT_SHIN         :
                    (sPartName  ==  "RTHIG")        ?   CREATURE_PART_RIGHT_THIGH       :
                    (sPartName  ==  "LTHIG")        ?   CREATURE_PART_LEFT_THIGH        :
                    (sPartName  ==  "PELVIS")       ?   CREATURE_PART_PELVIS            :
                    (sPartName  ==  "TORSO")        ?   CREATURE_PART_TORSO             :
                    (sPartName  ==  "BELT")         ?   CREATURE_PART_BELT              :
                    (sPartName  ==  "NECK")         ?   CREATURE_PART_NECK              :
                    (sPartName  ==  "RFOREARM")     ?   CREATURE_PART_RIGHT_FOREARM     :
                    (sPartName  ==  "LFOREARM")     ?   CREATURE_PART_LEFT_FOREARM      :
                    (sPartName  ==  "RBICEP")       ?   CREATURE_PART_RIGHT_BICEP       :
                    (sPartName  ==  "LBICEP")       ?   CREATURE_PART_LEFT_BICEP        :
                    (sPartName  ==  "RSHOULDER")    ?   CREATURE_PART_RIGHT_SHOULDER    :
                    (sPartName  ==  "LSHOULDER")    ?   CREATURE_PART_LEFT_SHOULDER     :
                    (sPartName  ==  "RHAND")        ?   CREATURE_PART_RIGHT_HAND        :
                    (sPartName  ==  "LHAND")        ?   CREATURE_PART_LEFT_HAND         :
                    (sPartName  ==  "HEAD")         ?   CREATURE_PART_HEAD              :
                    (sPartName  ==  "TAIL")         ?   -1  :
                    (sPartName  ==  "WINGS")        ?   -2  :   255;

    int nOld;

    //  telo
    if  (bPart  ==  255)
    {
        nOld        =   GetAppearanceType(oCreature);
        sMessage    =   Y1 + "( ? ) " + W2 + "Changing the creature body appearance [" + G2 + GetName(oCreature) + ", " + GetTag(oCreature) + W2 + "] from [" + G2 + itos(nOld) + " - " + Get2DAString("appearance", "LABEL", nOld) + W2 + "] to [" + G1 + itos(nModel) + " - " + Get2DAString("appearance", "LABEL", nModel) + W2 + "]";

        SetCreatureAppearanceType(oCreature, nModel);
    }

    else

    //  chvost
    if  (bPart  ==  -1)
    {
        nOld        =   GetCreatureTailType(oCreature);
        sMessage    =   Y1 + "( ? ) " + W2 + "Changing the creature tail appearance [" + G2 + GetName(oCreature) + ", " + GetTag(oCreature) + W2 + "] from [" + G2 + itos(nOld) + " - " + Get2DAString("tailmodel", "LABEL", nOld) + W2 + "] to [" + G1 + itos(nModel) + " - " + Get2DAString("tailmodel", "LABEL", nModel) + W2 + "]";

        SetCreatureTailType(nModel, oCreature);
    }

    else

    //  kridla
    if  (bPart  ==  -2)
    {
        nOld        =   GetCreatureWingType(oCreature);
        sMessage    =   Y1 + "( ? ) " + W2 + "Changing the creature wings appearance [" + G2 + GetName(oCreature) + ", " + GetTag(oCreature) + W2 + "] from [" + G2 + itos(nOld) + W2 + "] to [" + G1 + itos(nModel) + W2 + "]";

        SetCreatureWingType(nModel, oCreature);
    }

    //  cast tela X
    else
    {
        nOld    =   GetCreatureBodyPart(bPart, oCreature);
        sMessage    =   Y1 + "( ? ) " + W2 + "Changing the part appearance [" + G2 + sPartName + W2 + "] of the creatrure [" + G2 + GetName(oCreature) + ", " + GetTag(oCreature) + W2 + "] from [" + G2 + itos(nOld) + W2 + "] to [" + G1 + itos(nModel) + W2 + "]";

        SetCreatureBodyPart(bPart, nModel, oCreature);
    }

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
