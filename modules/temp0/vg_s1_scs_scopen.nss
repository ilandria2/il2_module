// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Summoning circle                       |
// | File    || vg_s1_scs_scopen.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Vytvari vyvolavaci kruh pro specifickou skolu kouzel
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_scs_const"
#include    "inc_draw"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    location    lCenter =   gll(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_LOCATION", -1, SYS_M_DELETE);
    int         nSchool =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", -1, SYS_M_DELETE);
    object      oSchool;
    int         nVFX, nRandom;
    string      sTemplate;

    nVFX    =   VFX_DUR_GHOST_TRANSPARENT;

    switch  (nSchool)
    {
        case    0:  //  General (None)

        break;

        case    1:  //  Abjuration

            DrawRhodoneaSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_ACID, lCenter, 4.0f, 0.0f, 0.0f, 0.5f, 0.0f, 40, 2.0f, 2.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_GR";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlaceRhodoneaSpring(sTemplate, lCenter, 4.0f, 0.0f, 0.0f, 0.5f, 40, 2.0f, 2.0f, 0.0f, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 0.0f, 2.0f, 0.0f);

        break;

        case    2:  //  Conjuration

            DrawRhodoneaSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_HOLY, lCenter, 3.0f, 0.0f, 0.0f, 3.0f, 0.0f, 30, 0.5f, 2.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_YE";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlaceRhodoneaSpring(sTemplate, lCenter, 3.0f, 0.0f, 0.0f, 3.0, 25, 0.5, 2.0, 0.0, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 0.0, 2.0, 0.0);

        break;

        case    3:  //  Divination

            DrawStarSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_FIRE, lCenter, 4.0f, 3.0f, 4.0f, 3.0f, 0.0f, 0.0f, 8, 0.0f, 35, 1.0f, 2.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_OR";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlaceStarSpring(sTemplate, lCenter, 4.0f, 3.0f, 4.0f, 3.0f, 0.0f, 0.0f, 8, 35, 1.0f, 2.0f, 0.0f, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 0.0f, 2.0f, 0.0f);

        break;

        case    4:  //  Enchantment

            DrawSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_ELECTRICITY, lCenter, 4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 30, 14.0f, 2.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_CY";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlaceSpring(sTemplate, lCenter, 4.0f, 0.0, 0.0, 0.0, 30, 14.0, 2.0, 0.0, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 3.0, 2.0, 0.0);

        break;

        case    5:  //  Evocation

            nRandom     =   4 + Random(4);
            DrawStarSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_MIND, lCenter, 4.0f, 2.0f, 4.0f, 2.0f, 0.0f, 0.0f, nRandom, 0.0f, 30, 1.0f, 1.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_BU";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlaceStarSpring(sTemplate, lCenter, 4.0f, 2.0f, 4.0f, 2.0f, 0.0f, 0.0f, nRandom, 30, 1.0, 1.0, 0.0, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 3.0, 2.0, 0.0);

        break;

        case    6:  //  Illusion

            DrawSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_SONIC, lCenter, 3.0f, 3.0f, 0.0f, 0.0f, 0.0f, 15, 1.0f, 1.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_WH";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlaceSpring(sTemplate, lCenter, 3.0f, 3.0f, 0.0, 0.0, 15, 1.0, 1.0, 0.0, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 3.0, 2.0, 0.0);

        break;

        case    7:  //  Necromancy

            DrawPentaclicSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_EVIL, lCenter, 4.0f, 4.0f, 0.0f, 0.0f, 0.0f, 30, 1.0f, 2.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_RE";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlacePentaclicSpring(sTemplate, lCenter, 4.0f, 4.0f, 0.0f, 0.0f, 30, 1.0, 2.0, 0.0, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 3.0, 2.0, 0.0);

        break;

        case    8:  //  Transmutation

            nRandom     =   3 + Random(3);
            DrawPolygonalSpring(DURATION_TYPE_INSTANT, VFX_IMP_HEAD_ODD, lCenter, 3.0f, 3.0f, 0.0f, 0.0f, nRandom, 0.0f, 15, 1.0f, 2.0f, 0.0f, "z");

            //sTemplate   =   "VG_P_VE_SHAFT_PU";
            sTemplate   =   "invisobj";
            oSchool     =   ObjectPlacePolygonalSpring(sTemplate, lCenter, 3.0f, 3.0f, 0.0f, 0.0f, nRandom, 15, 1.0, 2.0, 0.0, "z", DURATION_TYPE_PERMANENT, VFX_DUR_BLUR, 3.0, 2.0, 0.0);

        break;
    }

    sTemplate   =   "";
    nVFX        =   -1;

    slo(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_OBJECT", oSchool);
    //DelayCommand(3.0f, RecomputeStaticLighting(GetAreaFromLocation(lCenter)));
}
