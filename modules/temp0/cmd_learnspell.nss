// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_learnspell.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 08-12-2009                                                     |
// | Updated || 08-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "LEARNSPELL"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_scs_const"
#include    "vg_s0_xps_core_m"



int GetRowFromSpellId(int nSpell)
{
    int     nRow, nID;
    string  s2da    =   Get2DAString("scs_spellbooks_a", "Spell_ID", nRow);

    while   (s2da   !=  "")
    {
        nID =   stoi(s2da);

        if  (nID    ==  nSpell) return  nRow;

        s2da    =   Get2DAString("scs_spellbooks_a", "Spell_ID", ++nRow);
    }

    return  -1;
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "LEARNSPELL";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nSpell      =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    int     bValidate   =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    int     bXP         =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    int     bMsg        =   CCS_GetConvertedInteger(sLine, 4, FALSE);
    object  oPlayer     =   CCS_GetConvertedObject(sLine, 5, FALSE);
    string  sMessage;

    //  neplatne ID kouzla
    if  (nSpell <   -1)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� ID kouzla";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny objekt
    if  (!GetIsObjectValid(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� objekt";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neni to hrac
    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Nen� to hr��ska postava";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  seznam kouzel
    if  (nSpell ==  -1)
    {
        int nRow, nID, nInnate;
        string  sName, sSchool, s2da, sColor;

        s2da    =   Get2DAString("scs_spellbooks_a", "Spell_ID", nRow);

        sMessage    =   Y1 + "( ? ) " + W2 + "Seznam v�ech dostupn�ch kouzel:";
        sMessage    +=  "~~~~~~~~~~~~~~~~~~~~~~";

        msg(oPC, sMessage);

        while   (s2da   !=  "")
        {
            nID         =   stoi(s2da);
            nInnate     =   stoi(Get2DAString("scs_spellbooks_a", "Innate", nRow));
            sName       =   Get2DAString("scs_spellbooks_a", "Label", nRow);
            sSchool     =   Get2DAString("scs_spellbooks_a", "School", nRow);
            sColor      =   (sSchool    ==  "A")    ?   G1  :
                            (sSchool    ==  "C")    ?   Y1  :
                            (sSchool    ==  "D")    ?   O1  :
                            (sSchool    ==  "E")    ?   C1  :
                            (sSchool    ==  "I")    ?   W1  :
                            (sSchool    ==  "N")    ?   R1  :
                            (sSchool    ==  "T")    ?   P1  :
                            (sSchool    ==  "V")    ?   B1  :   W2;

            sMessage    =   W2 + "#" + itos(nID) + " - " + C2 + sName + W2 + " (" + sColor + sSchool + itos(nInnate) + W2 + ")";
            s2da        =   Get2DAString("scs_spellbooks_a", "Spell_ID", ++nRow);

            msg(oPC, sMessage);
        }
    }

    //  pokus o nauceni kouzla
    else
    {
        int bKnown  =   gli(oPlayer, SYSTEM_SCS_PREFIX + "_SPELL", nSpell);

        // jiz zname kouzlo
        if  (bKnown)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Hr��ska postava [" + R2 + GetPCPlayerName(oPlayer) + " / " + GetName(oPlayer) + W2 + "] ji� zn� tohle kouzlo";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        //  nezname kouzlo
        else
        {
            int nRow    =   GetRowFromSpellId(nSpell);

            //  neplatne ID kouzla
            if  (nRow   ==  -1)
            {
                sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� ID kouzla";

                msg(oPC, sMessage, FALSE, FALSE);
                return;
            }

            int bValid  =   TRUE;

            //  kontrola urovne sesilatele
            if  (bValidate)
            {
                int nFeat   =   SCS_FIRST_CONJURE_FEAT + nSpell;
                int bValid  =   GetHasFeat(SCS_FIRST_CONJURE_FEAT + nSpell, oPlayer) || (nSpell >= SCS_FIRST_CONJURE_ABILITY);

                //  nedostatek urovne sesilatele
                if  (!bValid)
                {
                    SYS_Info(oPC, SYSTEM_SCS_PREFIX, "LEARN_SPELL_FAILED_&" + itos(nSpell) + "&");
                    return;
                }
            }

            //  nauceni se kouzla
            if  (bValid)
            {
                sli(oPlayer, SYSTEM_SCS_PREFIX + "_SPELL", TRUE, nSpell);
                sli(oPlayer, SYSTEM_SCS_PREFIX + "_SPELL_ROW", nSpell, -2);

                int     nInnate     =   stoi(Get2DAString("scs_spellbooks_a", "Innate", nRow));
                string  sName       =   Get2DAString("scs_spellbooks_a", "Label", nRow);
                string  sSchool     =   Get2DAString("scs_spellbooks_a", "School", nRow);
                int     nClass      =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT"), SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_" + sSchool);
                string  sColor      =   (sSchool    ==  "A")    ?   G1  :
                                        (sSchool    ==  "C")    ?   Y1  :
                                        (sSchool    ==  "D")    ?   O1  :
                                        (sSchool    ==  "E")    ?   C1  :
                                        (sSchool    ==  "I")    ?   W1  :
                                        (sSchool    ==  "N")    ?   R1  :
                                        (sSchool    ==  "T")    ?   P1  :
                                        (sSchool    ==  "V")    ?   B1  :   W2;

                sMessage    =   G1 + "( ! ) " + W2 + "Hr��ska postava [" + G2 + GetPCPlayerName(oPlayer) + " / " + GetName(oPlayer) + W2 + "] nyn� ovl�d� kouzlo [" + B2 + sName + " (" + sColor + sSchool + itos(nInnate) + B2 + ")" + W2 + "]";

                msg(oPC, sMessage, FALSE, FALSE);

                //  oznameni hracovi
                if  (bMsg)  SYS_Info(oPlayer, SYSTEM_SCS_PREFIX, "LEARN_SPELL_SUCCESS_&" + itos(nSpell) + "&");

                //  ziskani zkusenosti
                if  (bXP)
                {
                    int nGainedXP   =   10 + Random(30 * nInnate);

                    XPS_AlterAbsorbXP(oPlayer, nClass, nGainedXP);
                    XPS_AlterAbsorbXP(oPlayer, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nGainedXP / 2);
                }
            }
        }
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
