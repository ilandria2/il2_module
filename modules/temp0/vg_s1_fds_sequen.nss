// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) HungerThirst sequence script       |
// | File    || vg_s1_fds_sequen.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 06-07-2009                                                     |
// | Updated || 06-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pusti sekvenci hladu a zizne
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_fds_core_c"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    HungerThirstSequence(object oPC)
{
    if  (!GetIsObjectValid(oPC))    return;
    if  (!GetIsPC(oPC))             return;
    if  (GetIsDM(oPC))              return;
    if  (GetIsDead(oPC))            return;

    //int nHoursMod   =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_CALENDAR_HOURS");
    int nHunger     =   gli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER");
    int nThirst     =   gli(oPC, SYSTEM_FDS_PREFIX + "_THIRST");
    int nFood, nLiquid;

    //  Procento vyuziti celkovej nosnosti
    //SpawnScriptDebugger();
    float   fValue  =   100 * (IntToFloat(GetWeight(oPC)) / 10) / (GetAbilityScore(oPC, ABILITY_STRENGTH) * 3000);
    int     nWeight =   FloatToInt(fValue);
    int     nValue;

    nWeight /=  25;

    /*
    //  Pocet provedenych utoku za 1 sekvenci
    int nALast  =   gli(oPC, SYSTEM_FDS_PREFIX + "_ATTACKS_LAST");
    int nANew   =   gli(oPC, "BCS_PHYSICAL_ATTACKS");
    int nAttack =   nANew - nALast;

    sli(oPC, SYSTEM_FDS_PREFIX + "_ATTACKS_LAST", nANew);

    nAttack /=  25;

    //  Pocet vykuzleni za 1 sekvenci
    int nCLast      =   gli(oPC, SYSTEM_FDS_PREFIX + "_CASTS_LAST");
    int nCNew       =   gli(oPC, "SCS_SPELL_CASTS");
    int nConjure    =   nCNew - nCLast;

    sli(oPC, SYSTEM_FDS_PREFIX + "_CASTS_LAST", nCNew);

    nConjure    /=  25;

    int nValue  =   nWeight + nAttack + nConjure + FDS_HUNGER_THIRST_SEQUENCE_INC;
    */

    nValue  =   nWeight /*+ nAttack + nConjure*/ + 1;
    nValue  =   (nValue <   0)  ?   0   :   nValue;
    nHunger +=  nValue;
    nThirst +=  nValue;

    sli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER", nHunger);
    sli(oPC, SYSTEM_FDS_PREFIX + "_THIRST", nThirst);

    //  hladina hladu dosahuje nebo presahuje smrtici hranici
    if  (nHunger    >=  FDS_DEADLY_EDGE_HUNGER)
    {
        sli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER", nHunger / 2);
        SetPlotFlag(oPC, FALSE);
        AssignCommand(oPC, ClearAllActions(TRUE));
        DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(TRUE, TRUE), oPC));
        SYS_Info(oPC, SYSTEM_FDS_PREFIX, "HUNGER_D");
        return;
    }

    else

    //  hladina zizne dosahuje nebo presahuje smrtici hranici
    if  (nThirst    >=  FDS_DEADLY_EDGE_THIRST)
    {
        sli(oPC, SYSTEM_FDS_PREFIX + "_THIRST", nThirst / 2);
        SetPlotFlag(oPC, FALSE);
        AssignCommand(oPC, ClearAllActions(TRUE));
        DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(TRUE, TRUE), oPC));
        SYS_Info(oPC, SYSTEM_FDS_PREFIX, "THIRST_D");
        return;
    }

    //  kriticka hranice hladu
    if  (nHunger    >=  FDS_DEADLY_EDGE_HUNGER - 10)
    {
        SYS_Info(oPC, SYSTEM_FDS_PREFIX, "HUNGER_C");
    }

    //  kriticka hranice zizne
    if  (nThirst    >=  FDS_DEADLY_EDGE_THIRST - 10)
    {
        SYS_Info(oPC, SYSTEM_FDS_PREFIX, "THIRST_C");
    }
}



void    main()
{
    object  oPC =   GetFirstPC();
    float   fDelay;

    while   (GetIsObjectValid(oPC))
    {
        if  (!GetIsDM(oPC))
        {
            /*
            string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

            if  (sArea  !=  "S"
            &&  sArea   !=  "X")
            { */
                fDelay  +=  ran(0.0f, 10.0f, 2);

                DelayCommand(fDelay, HungerThirstSequence(oPC));
            //}
        }

        oPC =   GetNextPC();
    }
}
