// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Set BAB skript                        |
// | File    || vg_s1_bcs_setbab.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pri equipnuti zbrane navysi BAB (base attack bonus) hracske postave podle
    kategorie zbrane
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetPCItemLastEquippedBy();
    object  oItem   =   GetPCItemLastEquipped();

    //  predmet byl equipnuty mimo pravou / levou ruku
    if  (GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oPC)   !=  oItem
    &&  GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oPC)     !=  oItem)  return;

    BCS_RefreshBAB(oPC);
}

