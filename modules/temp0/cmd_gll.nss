// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_gll.nss                                                    |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 29-06-2009                                                     |
// | Updated || 29-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "GLL"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "GLL";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sVarName    =   CCS_GetConvertedString(sLine, 1, FALSE);
    int     nRow        =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    object  oObject     =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny nazev promenne
    if  (sVarName   ==  "")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid variable name";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny radek
    if  (nRow   <   -2)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid row";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    location    lValue  =   gll(oObject, sVarName, nRow);
    int     nNewRow =   nRow;
    string  sTag    =   GetTag(oObject);
    vector  v       =   GetPositionFromLocation(lValue);
    string  sPost;

    if  (nRow   !=  -1)
    {
        nNewRow =   (nRow   ==  -2) ?   gli(oObject, sVarName + "_L_ROWS")  :   nRow;
        sPost   =   "_" + itos(nNewRow);
    }

    sTag        =   (sTag       ==  "") ?   "#N/A"  :   sTag;
    sMessage    =   Y1 + "( ? ) " + W2 + "Variable value [" + G2 + sVarName + sPost + W2 + "] on object [" + G2 + "Tag: " + sTag + ", Name: " + GetName(oObject) + W2 + "] is [" + G2 + GetName(GetAreaFromLocation(lValue)) + ", " + itos(ftoi(v.x)) + "x " + itos(ftoi(v.y)) + "y " + itos(ftoi(v.z)) + "z, " + ftos(GetFacingFromLocation(lValue)) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
