/*  VirgiL
    2018-03-26
    Exchange Trade System (ETS) concept

    About:
    Core concept of a trade system that should allow players to buy and sell goods by exchanging items between the merchant.

    Concept:
    x thanks to the ACS system the natural item properties such as weight and durability are already calculated using a
      recipe based system persisted in the database where the game loads the metadata and dynamically sets specific values
    x for trading purposes, ETS system introduces a new BaseCost item property which will indicate raw value of any tradable item
    - the original store GUI window is used to simulate and perform transactions as per the following example:
        - player can open the store window by Interaction or talking to NPC and selecting the Trade option
        x as the standard shop does when it opens, this will too show all public items that the merchant offers
        x because the transaction is hardcoded to be based on the nwn gold piece values, merchant has infinite amount of gold pieces and
          the same applies for player
        x player simply drags or double-clicks on items on the store window which are then added to the players inventory
        x player then receives a combat log message indicating the current Balance to be paid off either by player or the merchant
          For example: "Total: -273 coins" (if player has to pay merchant) or "Total: +273 coins" (if merchant has to pay player)
        - The amount mentioned in the balance is the sum of copper coins that should be less than or as close as possible to zero
        x Player doesn't need to have any coins in the inventory but must possess some items to bargain with
        x The coins are also items so player can drag them as normal items thus making the balance manually equal to zero
        x Player can close and reopen the window and the state of items and balance will still be memorized
        - To accept the transaction, the player has to select the "Accept" option in merchant conversation
        - To cancel the transaction, the player can either walk away, stay idle ot select the "Cancel" option in merchant conversation
    - That means, player blocks the store for anyone else while shopping until he either accepts or cancels the transaction
    x The system reuses the already implemented item-type restriction system which specifies a black/white list of baseitem types
    - Because of the item type restrictions, the items in the palette should have unique base item types if it should be possible to
      to exclude/include them in the list. For example, we have baseItem type for Longsword, which is unique, but for Tents, we have
      "Miscelaneous large" which also contains many other items with different usage type and thus, marking the whole group in the list
      would automatically imply that also other items not related to Tents would be white/black listed.
    x Finally, the list of items in the store is not infinite and is dynamically loaded from the database.
    x When player sells something or buys something, the state of these items are reflected back to the database
    x All store objects are pre-defined in the module using the toolset but are dynamically placed from within the game using console commands
    x To find existing stores based on search pattern, use ;stores <pattern>; command or ;stores <store_id>;
    x To place a new instance of a store on a specific location and persist it afterwards, use ;store add;<resref>;<ownerTag>; command
    x To remove an existing instance of a store, use ;store remove;<store_id>;

    Merchant interactions with items:
    x SELL: Player gives an item he owns to the merchant
    x RETURN: Player gives a previously bought item to the merchant
    x BUY: Player takes an item from the merchant
    x TAKE: Player takes a previously sold item from the merchant
    - ACCEPT: Player selected "Accept" option in merchant conversation
    - CANCEL: Player walks away, stays idle or selects the "Cancel" option in merchant conversation
    x LOGIN: Player logs in with items persisted in one of the merchant's cart

    Details of interactions:

    SELL:
    x item is added into ETS_PLAYER_CART list on Merchant
    x item is added to ets_store_items with playerId field set
    x player receives a combat log message in format like: "Balance: 123 coins"

    RETURN:
    x item is removed from ETS_PLAYER_CART list on Player
    x item stolen flag removed
    x player receives a combat log message in format like: "Balance: 0 coins"

    BUY:
    x item is added into ETS_PLAYER_CART list on Player
    x item is flagged as stolen
    x player receives a combat log message in format like: "Balance: -123 coins"

    TAKE:
    x item is removed from ETS_PLAYER_CART list on Merchant
    x item is removed from ets_store_items in database
    x player receives a combat log message in format like: "Balance: 0 coins"

    CANCEL:
    x items in ETS_CART list on Player are RETURNED to Merchant
    x items in ETS_CART list on Merchant are TAKEN back by Player
    x both lists are deleted
    - shop is unblocked and is free for other players
    - npcs says generic thank you for your visit message

    ACCEPT:
    - when the balance is:
        - < 0 (player owes to NPC) then checks player's money - if the amount is < |balance| then error
        - > 0 (NPC owes to player) then checks merchant's money - if the amount is < |balance| then warning with confirmation
    - if player was asked to confirm, he has to accept again and transaction will succeed (player will be underpaid / not paid)
    - balance is cleared - based on whether it's < or > then 0 one party receives the money and the other losts it
    - when balance was higher than the NPC could afford and player confirmed it then only the possible amount would be transfered
    - items in ETS_CART list on Player are removed from the database
    - items in ETS_CART list on Merchant are added to the database
    - both lists are deleted
    - shop is unblocked and is free for other players
    - npcs says generic thank you for your visit message
    - transaction (ets_store_transaction) is created in database summarizing:
        - count of items purchased
        - count of items sold
        - balance (positive/negative) cleared
        - player id
        - merchant id
        - date time

    LOGIN:
    x items in ets_store_items filtered by playerId column are added to player's inventory and removed from database
    x items flagged as stolen are removed from player's inventory
    (the two above scenarios occur when there were items in the one's cart
    before the transaction was accepted or canceled (ie. server crashed)


*/

