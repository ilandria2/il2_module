#include    "vg_s0_lpd_core_c"

void    main()
{
    object  oPC =   LPD_GetPC();

    LPD_PauseConversation(oPC);
    DelayCommand(3.0f, LPD_ResumeConversation(oPC));
}
