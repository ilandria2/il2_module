// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) OnDisturbed                    |
// | File    || vg_s1_lts_modify.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosi "OnDisturbed" pro zdroj poklad
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLeft   =   left(GetTag(OBJECT_SELF), 16);

    if  (sLeft  ==  "VG_P_TC_LTS_CONT")
    {
        int bComposed   =   gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_COMPOSED_TREASURE");

        //  nelze modifikovat skladany zdroj pokladu
        if  (bComposed)
        {
            SYS_Info(GetLastDisturbed(), SYSTEM_LTS_PREFIX, "COMPOSED_MODIFY");
            return;
        }

        int     nDist   =   GetInventoryDisturbType();
        int     nType   =   (nDist  ==  INVENTORY_DISTURB_TYPE_ADDED)   ?   SYS_M_INCREMENT :   SYS_M_DECREMENT;
        string  sName   =   GetName(GetInventoryDisturbItem());
        string  sType   =   GetSubString(GetTag(OBJECT_SELF), 17, 16);

        sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_ITEMS_COUNT", 1, -1, nType);
        SYS_Info(GetLastDisturbed(), SYSTEM_LTS_PREFIX, "SOURCE_MODIFY_&" + itos(nType) + "&_&" + sType + "&_&" + sName + "&");
    }
}
