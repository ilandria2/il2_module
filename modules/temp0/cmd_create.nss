// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_create.nss                                                 |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "CREATE"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "CREATE";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string      sType   =   GetStringUpperCase(CCS_GetConvertedString(sLine, 1, FALSE));
    string      sResRef =   CCS_GetConvertedString(sLine, 2, FALSE);
    location    lCreate =   CCS_GetConvertedLocation(sLine, 3, FALSE);
    int         bAnim   =   CCS_GetConvertedInteger(sLine, 4, FALSE);
    string      sNewTag =   CCS_GetConvertedString(sLine, 5, FALSE);
    int         nType   =   (sType  ==  "C")    ?   OBJECT_TYPE_CREATURE    :
                            (sType  ==  "I")    ?   OBJECT_TYPE_ITEM        :
                            (sType  ==  "P")    ?   OBJECT_TYPE_PLACEABLE   :
                            (sType  ==  "M")    ?   OBJECT_TYPE_STORE       :
                            (sType  ==  "W")    ?   OBJECT_TYPE_WAYPOINT    :
                                                    OBJECT_TYPE_INVALID;

    sNewTag =   (sNewTag    ==  "EMPTY")    ?   ""  :   sNewTag;

    object  oCreate =   CreateObject(nType, sResRef, lCreate, nType, sNewTag);

    //  Chyba pri vytvareni
    if  (!GetIsObjectValid(oCreate))
    {
        string  sMessage    =   R1 + "( ! ) " + W2 + "Unable to create object of type [" + G2 + sType + W2 + "] from a resref [" + G2 + sResRef + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
