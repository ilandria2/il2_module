// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Inventory Viewer - Closed        |
// | File    || vg_s1_ccs_inv_cl.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 14-12-2009                                                     |
// | Updated || 14-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Smaze obsah sveho inventare a znici sebe
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ccs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oTemp   =   GetFirstItemInInventory();
    object  oPC     =   GetLastClosedBy();
    object  oTarget =   glo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_TARGET");
    int     nCount;
    string  sMessage;

    while   (GetIsObjectValid(oTemp))
    {
        SetPlotFlag(oTemp, FALSE);
        DestroyObject(oTemp);

        //nCount  +=  GetItemStackSize(oTemp);
        nCount++;
        oTemp   =   GetNextItemInInventory();
    }

    slo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_CONTAINER", OBJECT_INVALID, -1, SYS_M_DELETE);
    slo(oPC, SYSTEM_CCS_PREFIX + "_INVENTORY_TARGET", OBJECT_INVALID, -1, SYS_M_DELETE);
    slo(oTarget, SYSTEM_CCS_PREFIX + "_INVENTORY_VIEWER", OBJECT_INVALID, -1, SYS_M_DELETE);
    SetPlotFlag(OBJECT_SELF, FALSE);
    DestroyObject(OBJECT_SELF);

    sMessage    =   Y1 + "( ? ) " + W2 + "Smaz�no " + G2 + itos(nCount) + W2 + " p�edm�t� z kontejneru [" + G2 + GetName(OBJECT_SELF) + W2 + "]";

    msg(oPC, sMessage, TRUE, FALSE);
}
