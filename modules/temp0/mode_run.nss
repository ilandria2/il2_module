// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) - Run mode script                     |
// | File    || mode_run.nss                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Skript pro mod "Beh"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nMode = gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_MODE");

    //  Preruseni
    if  (nMode  <   0)
    {
        BCS_RegDrainStamina(OBJECT_SELF, 1);
    }

    //  Aktivace
    else
    {
        int bSecondary = gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_USE_SECONDARY", -1, SYS_M_DELETE);
        int nStamina    =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_STAMINA");
        int nValNeeded  =   BCS_CalculateRegDrainedStaminaValue(OBJECT_SELF, bSecondary ? -2 : -1);

        //  nedostatek staminy
        if  (nStamina   <   -nValNeeded)
        {
            SYS_Info(OBJECT_SELF, SYSTEM_BCS_PREFIX, "STAMINA_X");
            return;
        }

        SwitchRunState(OBJECT_SELF, bSecondary ? 2 : 1);
        sli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_MODE_SECONDARY", bSecondary);
        sli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_MODE", SYS_MODE_RUN);
        SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "MODE_SWITCH_&1&_&" + itos(nMode) + "&_&" + itos(bSecondary) + "&");
        BCS_RegDrainStamina(OBJECT_SELF, bSecondary ? -2 : -1);
    }
}

