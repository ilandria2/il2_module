// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Commands init                    |
// | File    || vg_s1_ccs_cmd.nss                                              |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 27-06-2009                                                     |
// | Updated || 27-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript pro inicializaci skupin a prikazu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sGroup, sPassword, sCmd, sDescription;

//  ----------------------------------------------------------------------------
//  Skupina "PUBLIC"

    sGroup      =   "PUBLIC";
    sPassword   =   "";

    CCS_InitGroup(sGroup, sPassword);

    //  ACCESS
    sCmd            =   "ACCESS";
    sDescription    =   "Now, for testing purposes, all players automatically have access to all commands";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Group", CCS_DATATYPE_STRING, "", "Name of the command group (PUBLIC, PM, SA)");
    CCS_DefineParameter(sCmd, "Password", CCS_DATATYPE_STRING, "", "Password");

    //  LIST
    sCmd            =   "LIST";
    sDescription    =   "Shows a list of available commands";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);

    /*
    //  QUIT
    sCmd            =   "QUIT";
    sDescription    =   "Odpojen� se ze hry (kick)";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    */

    //  SPEAK
    sCmd            =   "SPEAK";
    sDescription    =   "Forced talk";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Text", CCS_DATATYPE_STRING, "", "What should be said");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "Who should say something");
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_STRING, "TALK", "In which channel the message should be said");

    /*
    //  SUPPORT
    sCmd            =   "SUPPORT";
    sDescription    =   "Vytvo�� ��dost o podporu ze strany DM/PM";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Znacka", CCS_DATATYPE_STRING, "", "Stru�n� ozna�en� probl�mu");
    CCS_DefineParameter(sCmd, "Popis", CCS_DATATYPE_STRING, "", "Podrobn� popis probl�mu");
    */

//  Skupina "PUBLIC"
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Skupina "PM"

    sGroup      =   "PM";
    sPassword   =   "";

    CCS_InitGroup(sGroup, sPassword);

    //  ALIGNMENT
    sCmd            =   "ALIGNMENT";
    sDescription    =   "Changes the alignment of the creature";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_INTEGER, "", "Alignment type (0 = all, 1 = neutral, 2 = law, 3 = chaos, 4 = good, 5 = evil)");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_INTEGER, "", "Value by which the alignment should be modified");
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "Creature to who the alignment should be changed");

    //  AREAINFO
    sCmd            =   "AREAS";
    sDescription    =   "Shows the information of the areas";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "AreaID", CCS_DATATYPE_INTEGER, "-1", "Shows the area information:\n1+ = detailed information about the selected area\n-1 = list of all areas\n-2 = list of all areas occupied by at least 1 player character");

    //  CAST
    //sCmd            =   "CAST";
    //sDescription    =   "Se�le na c�l vybrane kouzlo ve vybran� metamagii";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    //CCS_DefineParameter(sCmd, "Kouzlo", CCS_DATATYPE_STRING, "", "Pattern nebo ID kouzla (100 / light, fireball)");
    //CCS_DefineParameter(sCmd, "Metamagie", CCS_DATATYPE_STRING, "N", "Metamagie: [N]one [E]mpower e[X]tend [M]aximize [Q]uicken [S]ilent s[T]ill (Q, S a T se daji spojit)");
    //CCS_DefineParameter(sCmd, "Instant", CCS_DATATYPE_INTEGER, "0", "Kdy� 1, pak se ses�l�n� p�esko�� a kouzlo se se�le okam�it�");
    //CCS_DefineParameter(sCmd, "Cil", CCS_DATATYPE_LOCATION, "T", "C�l, na kter� se m� seslat kouzlo");

    //  CREATE
    sCmd            =   "CREATE";
    sDescription    =   "Creates an object using the selected resref";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Typee", CCS_DATATYPE_STRING, "", "Object type: C, E, I, M, P, T, W");
    CCS_DefineParameter(sCmd, "ResRef", CCS_DATATYPE_STRING, "", "Object resref");
    CCS_DefineParameter(sCmd, "Location", CCS_DATATYPE_LOCATION, "T", "Object location");
    CCS_DefineParameter(sCmd, "AppearAnim", CCS_DATATYPE_INTEGER, "0", "Use special 'appear' animation after spawn?");
    CCS_DefineParameter(sCmd, "NewTag", CCS_DATATYPE_STRING, "EMPTY", "New TAG of the object");

    //  DAMAGE
    sCmd            =   "DAMAGE";
    sDescription    =   "Applies specified amount of damage amount, type and power to the selected object";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "", "Amount of damage to be applied");
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_INTEGER, "8", "Damage type: 1 bludgeoning, 2 piercing, 4 slashing, 8 magical, 16 acid, 32 cold, 64 divine, 128 electricity, 256 fire, 512 negative, 1024 positive, 2048 sonic");
    CCS_DefineParameter(sCmd, "Power", CCS_DATATYPE_INTEGER, "0", "Damage power: 0 = +0, 1 = +1, ..., 5 = +5, 6 = energy, 7 = +6, 8 = +7, ..., 21 = +20");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "To what object should be the damage applied");

    //  DEITY
    sCmd            =   "DEITY";
    sDescription    =   "Changes the 'Deity' property on the selected creature";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Deity", CCS_DATATYPE_STRING, "GET", "The value to be set. If you specify 'GET' this command returns the current setting");
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "Creature to which we should apply the object");

    //  DESCRIPTION
    sCmd            =   "DESCRIPTION";
    sDescription    =   "Changes the object description";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Description", CCS_DATATYPE_STRING, "GET", "The text value of the description (empty = reset to the original one)");
    CCS_DefineParameter(sCmd, "Identified", CCS_DATATYPE_INTEGER, "1", "At the item-type objects we distinguish between identified (1) and unidentified (0) description");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "Object which's description should be changed");

    //  DETECT
    sCmd            =   "DETECT";
    sDescription    =   "Simulates a TIS object detection near the selected player character";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Player", CCS_DATATYPE_OBJECT, "T", "The selected player character");

    //  FLAG
    sCmd            =   "FLAG";
    sDescription    =   "Changes / resets / retrieves the current 'flag' setting on selected object";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Flag", CCS_DATATYPE_STRING, "PLOT", "Flag types: IDENTIFIED, LOCKED, DROPPABLE, INFINITE, CURSED, PICKPOCKETABLE, PLOT, STOLEN, USEABLE, IMMORTAL, DESTROYABLE, CUTSCENE, COMMANDABLE");
    CCS_DefineParameter(sCmd, "Bit", CCS_DATATYPE_INTEGER, "-1", "Value -1=GET, 0=UNSET, 1=SET");
    CCS_DefineParameter(sCmd, "Param", CCS_DATATYPE_INTEGER, "0", "If Flag = CUTSCENE, then this parameter determines whether left-clicking will or not be enabled while being in the cutscene mode");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected object");

    //  GETAPP
    sCmd            =   "GETAPP";
    sDescription    =   "Shows the current settings of the selected creature's appearance / finds a list of appearance names that match the specified pattern";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_STRING, "t", "C�lov� bytost / kl��ov� slovo vzhled�");

    //  GETSTATUS
    sCmd            =   "GETSTATUS";
    sDescription    =   "Displays the current status of the selected creature's properties/needs";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "Selected target");
    CCS_DefineParameter(sCmd, "Property", CCS_DATATYPE_STRING, "ALL", "Property type - all/liquid/food/stamina/hp/weight/skills/energy/power/progress");

    //  GOLD
    sCmd            =   "GOLD";
    sDescription    =   "Uprav� mno�stv� zla��k� u vybran� bytosti";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "", "Mno�stv� zla��k� na �pravu");
    CCS_DefineParameter(sCmd, "Operace", CCS_DATATYPE_INTEGER, "1", "-1 = odebrat, 0 = nastavit, 1 = pridat");
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "Creature se zla��ky na upraven�");

    //  HEAL
    sCmd            =   "HEAL";
    sDescription    =   "Heals the selected target";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "20", "The amount of damage to be healed: 0+ heal, -1 = dispel");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected creature to be healed");

    /*  odstaveno - potrebne informace jsou v denniku...
    //  HELP
    sCmd            =   "HELP";
    sDescription    =   "Zobraz� v�echna t�mata n�pov�dy ilandrie 2";

    //CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    */

    //  INVENTORY
    sCmd            =   "INVENTORY";
    sDescription    =   "Opens the inventory of the selected object for viewing/eidting";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_STRING, "GENERIC", "Inventory type to view: GENERIC, EQUIPPED, NATURAL");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected object");

    //  INVIS
    sCmd            =   "INVIS";
    sDescription    =   "Switches the invisibility mode for the selected creature";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "The selected creature");

    //  ITEM
    sCmd            =   "ITEM";
    sDescription    =   "Creates an item for the selected creature/location based on the specified recipe/resref";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Recipe", CCS_DATATYPE_STRING, "", "Numeric value = ID of the recipe, text value = resref of an item from the item palette");
    CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "1", "Amount of items to be created");
    CCS_DefineParameter(sCmd, "Specification", CCS_DATATYPE_STRING, "11111", "Recipe specification (see command ;ITEMS; for more information)");
    CCS_DefineParameter(sCmd, "Target", CCS_DATATYPE_OBJECT, "T", "The selected target / location");

    //  ITEMS
    sCmd            =   "ITEMS";
    sDescription    =   "Shows a list of recipe names that match the selected pattern";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Pattern", CCS_DATATYPE_STRING, "ALL", "Pattern of the recipes to be shown (box, 317, torch, ..., ALL)");

    //  JUMP
    sCmd            =   "JUMP";
    sDescription    =   "Moves the selected creature to the selected location";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Location", CCS_DATATYPE_LOCATION, "", "The selected location");
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "The selected creature");

    //  KICK
    sCmd            =   "KICK";
    sDescription    =   "Kicks a player character";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Player", CCS_DATATYPE_OBJECT, "T", "The player character to be kicked");

    //  KILL
    sCmd            =   "KILL";
    sDescription    =   "Kills/detroys the selected target";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_STRING, "DEATH", "Death type: DEATH, DESTROY");
    CCS_DefineParameter(sCmd, "Plot", CCS_DATATYPE_INTEGER, "1", "1 = turns off the 'PLOT' and 'IMMORTAL' flags before attempt to destroy");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected target to be killed/destroyed");

    //  LEARNSPELL
    //sCmd            =   "LEARNSPELL";
    //sDescription    =   "Nau�� hr��skou postavu nov�mu kouzlu";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    //CCS_DefineParameter(sCmd, "Kouzlo", CCS_DATATYPE_INTEGER, "-1", "ID kouzla pro nau�en�");
    //CCS_DefineParameter(sCmd, "Kontrola?", CCS_DATATYPE_INTEGER, "1", "M� se zkontrolovat, zda hr��ska postava m� dostatek �rovn� sesilatele?");
    //CCS_DefineParameter(sCmd, "XP?", CCS_DATATYPE_INTEGER, "1", "M� hr��ska postava z�skat zku�enosti?");
    //CCS_DefineParameter(sCmd, "Oznamit?", CCS_DATATYPE_INTEGER, "1", "M� b�t hr�� informov�n o nau�en�?");
    //CCS_DefineParameter(sCmd, "Hrac", CCS_DATATYPE_OBJECT, "T", "Hr��ska postava, kter� se m� dan� kouzlo nau�it");

    //  LEVEL
    //sCmd            =   "LEVEL";
    //sDescription    =   "Uprav� �rove� zku�enost� vybran� bytosti o ur�it� mno�stv�";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);                   er
    //CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "", "Mno�stv� �rov�� zku�enost�");
    //CCS_DefineParameter(sCmd, "Trida", CCS_DATATYPE_INTEGER, "0", "T��da zku�enost�: -1 pro seznam");
    //CCS_DefineParameter(sCmd, "Nastavit", CCS_DATATYPE_INTEGER, "0", "1 / 0");
    //CCS_DefineParameter(sCmd, "Hrac", CCS_DATATYPE_OBJECT, "T", "Komu se m� upravit �rove� zku�enosti");

    //  MESSAGES
    sCmd            =   "MESSAGES";
    sDescription    =   "Turns the system messages ON/OFF for the selected player character";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Player", CCS_DATATYPE_OBJECT, "T", "The selected player character");

    //  MONEY
    sCmd            =   "MONEY";
    sDescription    =   "Gives/takes money from the selected object";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "0", "Amount of money (0 = shows the current amount)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected object");

    //  MUSIC
    sCmd            =   "MUSIC";
    sDescription    =   "Changes the music settings for the current area";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_STRING, "N", "Music type - D (day), N (night), B (battle)");
    CCS_DefineParameter(sCmd, "Number", CCS_DATATYPE_INTEGER, "-50", "Track number (negative number = middle of the list)");
    CCS_DefineParameter(sCmd, "Play", CCS_DATATYPE_INTEGER, "100", "1 = play, 0 = stop, -1 = change (if the 2nd parameter was negative, this defines how many rows to show near the middle)");

    //  NAME
    sCmd            =   "NAME";
    sDescription    =   "Renames the selected object";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "RESET", "New name (RESET = resets the name to the original value)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected object to be renamed");

    //  PLAYERINFO
    sCmd            =   "PLAYERS";
    sDescription    =   "Displays information about the selected player";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Player", CCS_DATATYPE_INTEGER, "-1", "Player number (-1 = list of all players)");

    //  PLTINFO
    //sCmd            =   "PLTINFO";
    //sDescription    =   "NEDOKONCENO - Zobraz� informace o dan�m n�vrhu z palety";

    //CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    //CCS_DefineParameter(sCmd, "PltID", CCS_DATATYPE_INTEGER, "-1", "ID n�vrhu (-1 pro seznam v�ech n�vrh�)");
    //CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_STRING, "E", "Type n�vrhu (C = bytost, D = dvere, E = setkani, I = predmet, M = obchod, P = umistnitelny, T = spoust, W = kontrolni bod, S = zvuk)");

    //  PRICE
    //sCmd            =   "PRICE";
    //sDescription    =   "Zmeni cenu predmetu";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    //CCS_DefineParameter(sCmd, "Cena", CCS_DATATYPE_INTEGER, "", "Nova cena");
    //CCS_DefineParameter(sCmd, "Predmet", CCS_DATATYPE_OBJECT, "T", "Predmetu, kteremu cena ma byt zmenena");

    //  RETURN
    //sCmd            =   "RETURN";
    //sDescription    =   "Vrat� hr��skou postavu ze sf�ry smrti";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    //CCS_DefineParameter(sCmd, "Posith", CCS_DATATYPE_INTEGER, "0", "S postihem na XP?");
    //CCS_DefineParameter(sCmd, "Hrac", CCS_DATATYPE_OBJECT, "T", "Hr��ska postava k vr�cen�");

    //  REVEALMAP
    //sCmd            =   "REVEALMAP";
    //sDescription    =   "Odhal� mapu oblasti, v kter� se nach�z� pro vybran�ho hr��e";

    //CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    //CCS_DefineParameter(sCmd, "Odhalit?", CCS_DATATYPE_INTEGER, "1", "1 = odhalit, 0 = zahalit");
    //CCS_DefineParameter(sCmd, "Oznamit?", CCS_DATATYPE_INTEGER, "1", "1 = ozn�mit");
    //CCS_DefineParameter(sCmd, "Hrac", CCS_DATATYPE_OBJECT, "T", "Hr��, kter�mu m� b�t oblast odhalena/zahalena");

    //  RPX
    //sCmd            =   "RPX";
    //sDescription    =   "Nastav� hodnotu RolePlaying-Experience pro vybran�ho hr��e";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    //CCS_DefineParameter(sCmd, "Hodnota", CCS_DATATYPE_INTEGER, "40", "Hodnota RPX, kter� m� b�t nastavena");
    //CCS_DefineParameter(sCmd, "Hrac", CCS_DATATYPE_OBJECT, "T", "Hr��, kter�mu se m� nastavit hodnota RPX");

    //  SETAPP
    sCmd            =   "SETAPP";
    sDescription    =   "Changes the appearance of the selected creature";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Model", CCS_DATATYPE_INTEGER, "0", "A new model number to be set");
    CCS_DefineParameter(sCmd, "Part", CCS_DATATYPE_STRING, "BODY", "A name of the body part to be changed: BODY, TAIL, WINGS, HEAD, NECK, TORSO, PELVIS, xBICEP, xFOREARM, xHAND, xTHIGH, xSHIN, xFOOT - instead of 'x' put 'L' (left) or 'R' (right)");
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "The selected creature");

    //  SETSTATUS
    sCmd            =   "SETSTATUS";
    sDescription    =   "Changes the properties for the selected creature";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Property", CCS_DATATYPE_STRING, "", "Property type - all/liquid/food/stamina/energy/tree/rock/plant/body");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_STRING, "95%", "The new value - specify a numeric or percentage value");
    CCS_DefineParameter(sCmd, "Creature", CCS_DATATYPE_OBJECT, "T", "The selected creature");

    //  SPAWN
    sCmd            =   "SPAWN";
    sDescription    =   "Spawns an object on the selected location";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "ResRefID", CCS_DATATYPE_INTEGER, "", "The number of the resref - use the ;SPAWNS; command to find one");
    CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "1", "The amount of objects to be spawned (Only if the selected location is area)");
    CCS_DefineParameter(sCmd, "Location", CCS_DATATYPE_LOCATION, "T", "The selected location (choose via target (T) or via specific area object (A23, A7, ...))");

    //  SPAWNS
    sCmd            =   "SPAWNS";
    sDescription    =   "Shows the name of master-spawn object that match the specified pattern / contain an object to be spawned which matches this pattern / information about the specific master-spawn object";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Pattern", CCS_DATATYPE_STRING, "ALL", "The specified pattern");

    //  SPAWNSHOW
    sCmd            =   "SPAWNSHOW";
    sDescription    =   "Highlights all spawned hidden interactive placeable objects in the current area for the time of 5 minutes";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);

    // STORES
    sCmd            =   "STORES";
    sDescription    =   "Lists and filters all persisted stores/merchants.";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Pattern", CCS_DATATYPE_STRING, "ALL", "The specified pattern");

    // STORE
    sCmd            =   "STORE";
    sDescription    =   "Creates new or deletes existing persisted stores/merchants.";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Action", CCS_DATATYPE_STRING, "", "ADD / DELETE");
    CCS_DefineParameter(sCmd, "ResRef", CCS_DATATYPE_STRING, "", "Template ResRef from palette if ADD or StoreID if DELETE");
    CCS_DefineParameter(sCmd, "Owner", CCS_DATATYPE_STRING, "T", "NPC Owner Tag from palette if ADD, otherwise ignored");
    CCS_DefineParameter(sCmd, "Location", CCS_DATATYPE_LOCATION, "T", "Target location if ADD, otherwise ignored");

    //  TIME
    sCmd            =   "TIME";
    sDescription    =   "Changes / shows the current date and time";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Hour", CCS_DATATYPE_INTEGER, "-1", "from 0 to 23");
    CCS_DefineParameter(sCmd, "Minute", CCS_DATATYPE_INTEGER, "-1", "from 0 to 59");
    CCS_DefineParameter(sCmd, "Day", CCS_DATATYPE_INTEGER, "-1", "from 1 to 28");
    CCS_DefineParameter(sCmd, "Month", CCS_DATATYPE_INTEGER, "-1", "from 1 to 12");
    CCS_DefineParameter(sCmd, "Year", CCS_DATATYPE_INTEGER, "-1", "from 1200 to 1500");

    //  VFX
    sCmd            =   "VFX";
    sDescription    =   "Applies a specific visual effect on the selected location";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Id", CCS_DATATYPE_INTEGER, "", "Id of the visual effect");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected object");
    CCS_DefineParameter(sCmd, "Location", CCS_DATATYPE_LOCATION, "T", "The selected location");
    CCS_DefineParameter(sCmd, "Duration", CCS_DATATYPE_FLOAT, "0.0", "The duration");

    //  WEATHER
    sCmd            =   "WEATHER";
    sDescription    =   "Changes the weather for the selected area";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Type", CCS_DATATYPE_INTEGER, "-1", "The new weather type (-1 = default, 0 = clear, 1 = storm, 2 = snow");
    CCS_DefineParameter(sCmd, "Area", CCS_DATATYPE_OBJECT, "A", "The selected area");

    //  WEIGHT
    sCmd            =   "WEIGHT";
    sDescription    =   "Changes the weight of the selected item";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_INTEGER, "", "The new weight");
    CCS_DefineParameter(sCmd, "Item", CCS_DATATYPE_OBJECT, "T", "The selected item");

    //  XP
    //sCmd            =   "XP";
    //sDescription    =   "Uprav� zku�enosti vybran� bytosti o ur�it� mno�stv�";

    //CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    //CCS_DefineParameter(sCmd, "Amount", CCS_DATATYPE_INTEGER, "", "Mno�stv� zku�enost�");
    //CCS_DefineParameter(sCmd, "Trida", CCS_DATATYPE_INTEGER, "0", "T��da zku�enost�: -1 pro seznam");
    //CCS_DefineParameter(sCmd, "Nastavit", CCS_DATATYPE_INTEGER, "0", "Hfromnota 0 nebo 1");
    //CCS_DefineParameter(sCmd, "Hrac", CCS_DATATYPE_OBJECT, "T", "Komu se m�j� upravit zku�enosti");

//  Skupina "PM"
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Skupina "SA"

    sGroup      =   "SA";
    sPassword   =   "";

    CCS_InitGroup(sGroup, sPassword);

    //  DEBUG
    sCmd            =   "DEBUG";
    sDescription    =   "Debug command (for development purposes)";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "String", CCS_DATATYPE_STRING, "X", "Parameter of data type 'string'");
    CCS_DefineParameter(sCmd, "Integer", CCS_DATATYPE_INTEGER, "-1", "Parameter of data type 'int'");
    CCS_DefineParameter(sCmd, "Float", CCS_DATATYPE_FLOAT, "-1.0", "Parameter of data type 'float'");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "X", "Parameter of data type 'object'");
    CCS_DefineParameter(sCmd, "Location", CCS_DATATYPE_LOCATION, "X", "Parameter of data type 'location'");

    //  DEBUG
    sCmd            =   "DNAME";
    sDescription    =   "Set dynamic name";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "X", "Dynamic name");
    CCS_DefineParameter(sCmd, "Object1", CCS_DATATYPE_OBJECT, "s", "Object 1");
    CCS_DefineParameter(sCmd, "Object2", CCS_DATATYPE_OBJECT, "t", "Object 2");

    //  DINIT
    sCmd            =   "DINIT";
    sDescription    =   "Dynamic name init player list";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "t", "Object 1");

    //  DGETIND
    sCmd            =   "DGETIND";
    sDescription    =   "Get merged indicator for players pc1 and pc2";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "PC1", CCS_DATATYPE_OBJECT, "s", "Player 1");
    CCS_DefineParameter(sCmd, "PC2", CCS_DATATYPE_OBJECT, "t", "Player 2");

    //  DREFIND
    sCmd            =   "DREFIND";
    sDescription    =   "Refresh merged indicator for players pc1 and pc2";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "PC1", CCS_DATATYPE_OBJECT, "s", "Player 1");
    CCS_DefineParameter(sCmd, "PC2", CCS_DATATYPE_OBJECT, "t", "Player 2");
    CCS_DefineParameter(sCmd, "All", CCS_DATATYPE_INTEGER, "1", "All");

    //  EXEC
    sCmd            =   "EXEC";
    sDescription    =   "Executes a specified module-script on the server for the selected object";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Script", CCS_DATATYPE_STRING, "", "The name of the script to be executed");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "S", "The selected object");

    //  GLF
    sCmd            =   "GLF";
    sDescription    =   "Navr�t� hfromnotu lok�ln� prom�nn� of data type FLOAT";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  GLI
    sCmd            =   "GLI";
    sDescription    =   "Navr�t� hfromnotu lok�ln� prom�nn� of data type INTEGER";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  GLL
    sCmd            =   "GLL";
    sDescription    =   "Navr�t� hfromnotu lok�ln� prom�nn� of data type LOCATION";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  GLO
    sCmd            =   "GLO";
    sDescription    =   "Navr�t� hfromnotu lok�ln� prom�nn� of data type OBJECT";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  GLS
    sCmd            =   "GLS";
    sDescription    =   "Navr�t� hfromnotu lok�ln� prom�nn� of data type STRING";

    CCS_InitCommand(sGroup, sCmd, sDescription, FALSE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  RESTART
    sCmd            =   "RESTART";
    sDescription    =   "Starts the server restart sequence";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Interval", CCS_DATATYPE_INTEGER, "5", "After how many minutes the server should get restarted (0 = immediately, -1 = deactivates the started sequence)");

    //  SLF
    sCmd            =   "SLF";
    sDescription    =   "Sets the specified local variable of data type FLOAT";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_FLOAT, "", "The new value of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Operation", CCS_DATATYPE_INTEGER, itos(SYS_M_SET), "The math operation used: -3 square_root, -2 division, -1 exclusion, 0 set, 1 addition, 2 multiplication, 3 power");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  SLI
    sCmd            =   "SLI";
    sDescription    =   "Sets the specified local variable of data type INTEGER";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_INTEGER, "", "The new value of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Operation", CCS_DATATYPE_INTEGER, itos(SYS_M_SET), "The math operation used: -3 square_root, -2 division, -1 exclusion, 0 set, 1 addition, 2 multiplication, 3 power");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  SLL
    sCmd            =   "SLL";
    sDescription    =   "Sets the specified local variable of data type LOCATION";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_LOCATION, "", "The new value of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Operation", CCS_DATATYPE_INTEGER, itos(SYS_M_SET), "The math operation used: -3 square_root, -2 division, -1 exclusion, 0 set, 1 addition, 2 multiplication, 3 power");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  SLO
    sCmd            =   "SLO";
    sDescription    =   "Sets the specified local variable of data type OBJECT";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_OBJECT, "", "The new value of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Operation", CCS_DATATYPE_INTEGER, itos(SYS_M_SET), "The math operation used: -3 square_root, -2 division, -1 exclusion, 0 set, 1 addition, 2 multiplication, 3 power");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  SLS
    sCmd            =   "SLS";
    sDescription    =   "Sets the specified local variable of data type STRING";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Name", CCS_DATATYPE_STRING, "", "A case-sensitive name of the variable");
    CCS_DefineParameter(sCmd, "Value", CCS_DATATYPE_STRING, "", "The new value of the variable");
    CCS_DefineParameter(sCmd, "Row", CCS_DATATYPE_INTEGER, "-1", "Row in the variable name: -2 = last + 1 (format: VARNAME_ROW, ex: VARNAME_1, VARNAME_2, ...)");
    CCS_DefineParameter(sCmd, "Operation", CCS_DATATYPE_INTEGER, itos(SYS_M_SET), "The math operation used: -3 square_root, -2 division, -1 exclusion, 0 set, 1 addition, 2 multiplication, 3 power");
    CCS_DefineParameter(sCmd, "Object", CCS_DATATYPE_OBJECT, "T", "The selected obejct");

    //  SQL
    sCmd            =   "SQL";
    sDescription    =   "Runs a SQL query on the server (shows results if any rows returned)";

    CCS_InitCommand(sGroup, sCmd, sDescription, TRUE);
    CCS_DefineParameter(sCmd, "Query", CCS_DATATYPE_STRING, "", "The specified SQL query");

//  Skupina "SA"
//  ----------------------------------------------------------------------------
}
