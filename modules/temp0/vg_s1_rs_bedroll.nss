// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Unpack bedroll script                         |
// | File    || vg_s1_rs_bedroll.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-08-2009                                                     |
// | Updated || 16-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Rozbali deku na spani (smazani predmet z inventare a vytvoreni pouzitelneho
    umisstnitelnyho objektu, ktery pri pouziti udela pravej opak)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_core_c"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    /*
    object  oPC     =   GetItemActivator();
    object  oItem   =   GetItemActivated();
    string  sTag    =   GetTag(oItem);
    string  sPart   =   GetSubString(sTag, 8, 6);

    //  VG_I_MI_RS_BED_0
    //  ________^^^^^^__
    if  (sPart  ==  "RS_BED")
    {
        itemproperty    ipTemp  =   GetFirstItemProperty(oItem);
        int             nType, nSubType, nBonus;

        while   (GetIsItemPropertyValid(ipTemp))
        {
            nType   =   GetItemPropertyType(ipTemp);

            if  (nType  ==  ITEM_PROPERTY_ADDITIONAL)
            {
                nSubType    =   GetItemPropertySubType(ipTemp);

                if  (nSubType   ==  RS_IPRP_BEDROLL)
                {
                    string  sBonus  =   Get2DAString("iprp_addcost", "Label", GetItemPropertyCostTableValue(ipTemp));

                    if  (left(sBonus, 6)    ==  "Bonus_")
                    nBonus  =  stoi(GetSubString(sBonus, 6, GetStringLength(sBonus) - 6));

                    object  oBedroll    =   CreateObject(OBJECT_TYPE_PLACEABLE, RS_RSRF_BEDROLL, GetLocation(oPC), FALSE, "");

                    DelayCommand(0.3f, AssignCommand(oBedroll, PlaySound("as_sw_clothop1")));
                    AssignCommand(oPC, ActionPlayAnimation(ANIMATION_LOOPING_GET_LOW, 0.5f, 5.0f));
                    sli(oBedroll, SYSTEM_RS_PREFIX + "_BEDROLL", nBonus);
                    sls(oBedroll, SYSTEM_OES_PREFIX + "_P_USED_0_NAME", "GIVE");
                    sls(oBedroll, SYSTEM_OES_PREFIX + "_P_USED_0_VALUE", GetResRef(oItem));
                    sli(oBedroll, SYSTEM_OES_PREFIX + "_P_USED_0_VALUE", 1);
                    sls(oBedroll, SYSTEM_OES_PREFIX + "_P_USED_1_NAME", "DESTROY");
                    SetPlotFlag(oItem, FALSE);
                    Destroy(oItem);
                    SYS_Info(oPC, SYSTEM_RS_PREFIX, "UNPACK");
                    break;
                }
            }

            ipTemp  =   GetNextItemProperty(oItem);
        }

    }
    */
}
