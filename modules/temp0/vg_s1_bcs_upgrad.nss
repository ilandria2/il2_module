// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) OnModuleItemAcquired event script     |
// | File    || vg_s1_bcs_upgrad.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-01-2008                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Prida iprp na ziskanou zbran hraci
*/
// -----------------------------------------------------------------------------



#include    "x2_inc_itemprop"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetModuleItemAcquiredBy();

    if  (!GetIsPC(oPC)) return;

    object  oAcquired   =   GetModuleItemAcquired();
    int     bWeapon     =   stoi(Get2DAString("baseitems", "WeaponType", GetBaseItemType(oAcquired)))   >   0;

    if  (!bWeapon)  return;

    IPSafeAddItemProperty(oAcquired, ItemPropertyOnHitCastSpell(141, 1));
}
