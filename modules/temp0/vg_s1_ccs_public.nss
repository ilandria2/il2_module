// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Module event script              |
// | File    || vg_s1_ccs_public.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 18-06-2009                                                     |
// | Updated || 18-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosti "OnPlayerLogin" pro system "CCS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    if  (!GetIsPC(oPC)) return;

    int nId;
    string  sGroup, sPassword;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     bDM     =   GetIsDM(oPC);

    nId     =   1;
    sGroup  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", nId);

    while   (sGroup !=  "")
    {
        sPassword   =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_PASSWORD");

        if  (sPassword  ==  ""
        ||  bDM)    CCS_GrantAccess(oPC, sGroup);

        nId++;
        sGroup  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", nId);
    }
}
