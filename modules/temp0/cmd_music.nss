// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_music.nss                                                  |
// | Author  || VirgiL                                                         |
// | Created || 22-07-2009                                                     |
// | Updated || 22-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "MUSIC"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



string  GetTrackName(int nRow)
{
    string  s2da    =   Get2DAString("ambientmusic", "Description", nRow);

    if  (s2da   !=  "") return  GetStringByStrRef(stoi(s2da));

    s2da    =   Get2DAString("ambientmusic", "DisplayName", nRow);

    if  (s2da   !=  "") return  s2da;

    s2da    =   Get2DAString("ambientmusic", "Resource", nRow);

    return  s2da;
}



void    main()
{
    string      sCmd    =   "MUSIC";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sType   =   GetStringUpperCase(CCS_GetConvertedString(sLine, 1, FALSE));
    int     nTrack  =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    int     bPlay   =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    object  oArea   =   GetArea(oPC);
    string  sMessage;

    //  zmena hudby
    if  (nTrack >=  0)
    {
        //  den
        if  (sType  ==  "D")
        {
            int     nOld    =   MusicBackgroundGetDayTrack(oArea);
            string  sOld    =   GetTrackName(nOld);
            string  sNew    =   GetTrackName(nTrack);

            sMessage    =   Y1 + "( ? ) " + W2 + "Changing " + G2 + "day music" + W2 + " from [" + G2 + "#" + itos(nOld) + " - " + sOld + W2 + "] to [" + G2 + "#" + itos(nTrack) + " - " + sNew + W2 + "]";

            msg(oPC, sMessage, FALSE, FALSE);
            MusicBackgroundChangeDay(oArea, nTrack);

            if  (bPlay  ==  TRUE)
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Stopping " + R2 + "battle music" + W2 + ", starting " + G2 + "background music" + W2 + "...";

                msg(oPC, sMessage, FALSE, FALSE);
                MusicBattleStop(oArea);
                MusicBackgroundPlay(oArea);
            }

            else

            if  (bPlay  ==  FALSE)
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Stopping " + R2 + "background music" + W2 + "...";

                msg(oPC, sMessage, FALSE, FALSE);
                MusicBackgroundStop(oArea);
            }
        }

        else

        //  noc
        if  (sType  ==  "N")
        {
            int     nOld    =   MusicBackgroundGetNightTrack(oArea);
            string  sOld    =   GetTrackName(nOld);
            string  sNew    =   GetTrackName(nTrack);

            sMessage    =   Y1 + "( ? ) " + W2 + "Changing " + G2 + "night music" + W2 + " from [" + G2 + "#" + itos(nOld) + " - " + sOld + W2 + "] to [" + G2 + "#" + itos(nTrack) + " - " + sNew + W2 + "]";

            msg(oPC, sMessage, FALSE, FALSE);
            MusicBackgroundChangeNight(oArea, nTrack);

            if  (bPlay  ==  TRUE)
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Stopping " + R2 + "battle music" + W2 + ", starting " + G2 + "background music" + W2 + "...";

                msg(oPC, sMessage, FALSE, FALSE);
                MusicBattleStop(oArea);
                MusicBackgroundPlay(oArea);
            }

            else

            if  (bPlay  ==  FALSE)
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Stopping " + R2 + "background music" + W2 + "...";

                msg(oPC, sMessage, FALSE, FALSE);
                MusicBackgroundStop(oArea);
            }
        }

        else

        //  boj
        if  (sType  ==  "B")
        {
            int     nOld    =   MusicBackgroundGetBattleTrack(oArea);
            string  sOld    =   GetTrackName(nOld);
            string  sNew    =   GetTrackName(nTrack);

            sMessage    =   Y1 + "( ? ) " + W2 + "Changing " + G2 + "battle music" + W2 + " from [" + G2 + "#" + itos(nOld) + " - " + sOld + W2 + "] to [" + G2 + "#" + itos(nTrack) + " - " + sNew + W2 + "]";

            msg(oPC, sMessage, FALSE, FALSE);
            MusicBattleChange(oArea, nTrack);

            if  (bPlay  ==  TRUE)
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Stopping " + R2 + "background music" + W2 + ", starting " + G2 + "battle music" + W2 + "...";

                msg(oPC, sMessage, FALSE, FALSE);
                MusicBackgroundStop(oArea);
                MusicBattlePlay(oArea);
            }

            else

            if  (bPlay  ==  FALSE)
            {
                sMessage    =   Y1 + "( ? ) " + W2 + "Stopping " + R2 + "battle music" + W2 + "...";

                msg(oPC, sMessage, FALSE, FALSE);
                MusicBattleStop(oArea);
            }
        }
    }

    //  nacteni seznamu z 2da
    else
    {
        nTrack  =   -nTrack;

        int     nDay    =   MusicBackgroundGetDayTrack(oArea);
        int     nNight  =   MusicBackgroundGetNightTrack(oArea);
        int     nBattle =   MusicBackgroundGetBattleTrack(oArea);
        int     nLow    =   (bPlay  <=  0)      ?   0   :   nTrack - (bPlay / 2);
        int     nHigh   =   (bPlay  >=  800)    ?   100 :   nTrack + (bPlay / 2);
        int     nRow    =   nLow;
        string  sTrack  =   GetTrackName(nRow);
        string  sState;

        int bOne;

        sMessage    =   Y1 + "( ? ) " + W2 + "List of musics within a range from " + G2 + itos(nLow) + W2 + " to " + G2 + itos(nHigh) + W2 + ":";
        sMessage    +=  G2 + "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";

        msg(oPC, sMessage, FALSE, FALSE);

        while   (nRow   <=  nHigh)
        {
            sMessage    =  G2 + "#" + itos(nRow) + W2 + " - " + sTrack;

            if  (nRow   ==  nDay)
            {
                bOne    =   TRUE;
                sState  +=  W2 + "[" + B2 + "D" + W2 + "]";
            }

            if  (nRow   ==  nNight)
            {
                bOne    =   TRUE;
                sState  +=  W2 + "[" + V2 + "N" + W2 + "]";
            }

            if  (nRow   ==  nBattle)
            {
                bOne    =   TRUE;
                sState  +=  W2 + "[" + R2 + "B" + W2 + "]";
            }

            if  (bOne)
            {
                sMessage    +=  " " + sState;
                sState      =   "";
                bOne        =   FALSE;
            }

            msg(oPC, sMessage, FALSE, FALSE);

            sTrack  =   GetTrackName(++nRow);
        }

        sMessage    =   G2 + "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
        sMessage    +=  Y1 + "\n( ? ) " + W2 + "Current music settings in the area:";
        sMessage    +=  G2 + "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
        sMessage    +=  W2 + "\nBackground music (day) [" + G2 + "#" + itos(nDay) + " - " + GetTrackName(nDay) + W2 + "]";
        sMessage    +=  W2 + "\nBackground music (night) [" + G2 + "#" + itos(nNight) + " - " + GetTrackName(nNight) + W2 + "]";
        sMessage    +=  W2 + "\nBattle music [" + G2 + "#" + itos(nBattle) + " - " + GetTrackName(nBattle) + W2 + "]";
        sMessage    +=  G2 + "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";

        msg(oPC, sMessage, FALSE, FALSE);
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
