// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft Skills (BCS) Stamina capacity script               |
// | File    || vg_s1_bcs_stmcap.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Prepocte a nastavi kapacitu staminy pro prihlasujiciho se hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC     =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");
    int     nValMax =   BCS_CalculateStaminaCapacity(oPC);
    int     nPowMax = BCS_CalculatePowerCapacity(oPC);

    sli(oPC, SYSTEM_BCS_PREFIX + "_STAMINA", nValMax);
    sli(oPC, SYSTEM_BCS_PREFIX + "_POWER", nPowMax);

    SetCustomActionMode(oPC, SYS_MODE_WALK, FALSE);
}

