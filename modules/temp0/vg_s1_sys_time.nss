// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Time saver script                       |
// | File    || vg_s1_sys_time.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 06-07-2009                                                     |
// | Updated || 06-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Uklada cas do promenne pro kontrolu "zmenu stavu o 1 hodinu" v systemech
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_m"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_d"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int     nValue  =   GetTimeHour();
    int     bDate;

    sli(oSystem, SYSTEM_SYS_PREFIX + "_HBS", 1, -1, SYS_M_INCREMENT);

    if  (nValue ==  gli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_HOUR"))
    {
        nValue  =   GetCalendarDay();

        if  (nValue ==  gli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_DAY"))
        {
            nValue  =   GetCalendarMonth();

            if  (nValue ==  gli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_MONTH"))
            {
                nValue  =   GetCalendarYear();

                if  (nValue ==  gli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_YEAR"))
                {
                    return;
                }

                else    bDate   =   TRUE;
            }

            else    bDate   =   TRUE;
        }

        else    bDate   =   TRUE;
    }

    else    bDate   =   TRUE;

    if  (bDate)
    {
        int nYear   =   GetCalendarYear();
        int nMonth  =   GetCalendarMonth();
        int nDay    =   GetCalendarDay();
        int nHour   =   GetTimeHour();

        sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_YEAR", nYear);
        sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_MONTH", nMonth);
        sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_DAY", nDay);
        sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_HOUR", nHour);
        sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_HOURS", DateToHours(nYear, nMonth, nDay, nHour));

        SavePersistentData(oSystem, SYSTEM_SYS_PREFIX, "SETTINGS");
        //SQLExecDirect("SELECT NOW()");
    }

    ExecuteScript("vg_s1_sys_party", OBJECT_SELF);
}
