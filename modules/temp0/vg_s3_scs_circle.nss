// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Impact script                          |
// | File    || vg_s3_scs_circle.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nastavuje promennou urcujici vybranou skolu pro vyvolavaci kruh a take
    vytvari samotnej vyvolavaci kruh dle definovane promenne
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_core_c"
#include    "vg_s0_scs_const"
#include    "x2_inc_spellhook"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nSpell  =   GetSpellId();

    if  (nSpell ==  SCS_SPELL_SUMMONING_CIRCLE)
    {
        int     nLast       =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT"), SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_S_ROWS");
        int     nCurrent    =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL");
        int     nEnd        =   nLast;
        int     nNew, i, bKnown;

        i   =   nCurrent + 1;

        while   (i  <=  nEnd)
        {
            bKnown  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", nSpell + i);

            if  (bKnown)    break;
            if  (i  >=  nLast)
            {
                i       =   0;
                nEnd    =   nCurrent;
            }

            i++;
        }

        nNew    =   (bKnown)    ?   i   :   0;

        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", nNew);
        sls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "&" + SCS_NCS_SPELL_SUMMONING_CIRCLE + "&_&" + itos(nNew) + "&");
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "CIRCLE_SWITCH_&" + itos(nNew)+"&");
    }

    else

    //if  (nSpell ==  SYS_SPELL_TARGET)
    if  (TRUE)
    {
        location    lTarget     =   GetSpellTargetLocation();
        string      sParameter  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS");
        int         nSchool     =   stoi(GetNthSubString(sParameter, 2));

        if  (!nSchool)  return;

        ExecuteScript(SCS_NCS_CONDITION_RESREF, OBJECT_SELF);

        int bSpell  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_CONDITION_RESULT");

        if  (bSpell)    AssignCommand(OBJECT_SELF, ActionCastSpellAtLocation(SCS_SPELL_SUMMONING_CIRCLE + nSchool, lTarget, METAMAGIC_ANY, TRUE));
    }

    else
    {
        if (!X2PreSpellCastCode())  return;

        location    lTarget     =   GetSpellTargetLocation();
        object      oCircle;

        oCircle =   SCS_GetFirstSummoningCircle(SHAPE_SPHERE, 5.0f, lTarget);

        if  (GetIsObjectValid(oCircle)) SCS_CloseSummoningCircle(oCircle);
        else                            SCS_OpenSummoningCircle(nSpell - SCS_SPELL_SUMMONING_CIRCLE, lTarget);
    }
}
