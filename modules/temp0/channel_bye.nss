// script handler of the channel "BYE"

#include    "vg_s0_ccs_core_t"
#include    "vg_s0_lpd_core_c"

void main()
{
    string sChannel = "BYE";
    string sLine = GetPCChatMessage();
    object sSystem = GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object oPC = OBJECT_SELF;
    string sText = CCS_GetChannelText(sLine);

    if (gli(oPC, "ccs_bye"))SpawnScriptDebugger();

    object G_PC = LPD_GetConversator(oPC);

    SetPCChatMessage(sText);
    if (GetIsObjectValid(G_PC))
        LPD_EndConversation(oPC, FALSE);
}

