// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_name.nss                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "NAME"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"
#include "vg_s0_sys_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "NAME";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    //  zrusi dynamicke jmeno u vybraneho cile
    if  (upper(sText)   ==  "H"
    ||  upper(sText)    ==  "HIDE")
    {
        sli(oPC, SYSTEM_PIS_PREFIX + "_HIDDEN", !gli(oPC, SYSTEM_PIS_PREFIX + "_HIDDEN"));
        PIS_RefreshIndicator(oPC, OBJECT_INVALID, TRUE);
        SYS_Info(oPC, SYSTEM_PIS_PREFIX, "HIDE_SWITCH");
        return;
    }

    //  target mode
    SYS_Info(oPC, SYSTEM_PIS_PREFIX, "USE_NAME");
    SetTargetParameters(oPC, "vg_s1_pis_setnam", sText);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
