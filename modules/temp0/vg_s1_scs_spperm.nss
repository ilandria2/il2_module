// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Check permitted script                 |
// | File    || vg_s1_scs_spperm.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 14-05-2009                                                     |
// | Updated || 14-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skontroluje jestli seslane kouzlo neni blokovane nejakyma systemama
    (napriklad, ze nelze kouzlit ve sferach atd..)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_c"
#include    "vg_s0_scs_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     bPermitted;
    int     bAreaSwitch =   gli(GetArea(OBJECT_SELF), SYSTEM_SCS_PREFIX + "_SPELL_CAST_DISABLED");

    if  (bAreaSwitch)
    {
        SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "SPELL_PERMITTED_AREASWITCH");
        bPermitted  =   TRUE;
    }
    /*
    string  sParameters =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS");
    int     nSpell      =   stoi(GetNthSubString(sParameters, 2)) - SCS_FIRST_CONJURE_SPELL;*/

    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_PERMITTED_RESULT", !bPermitted);
}
