// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_sls.nss                                                    |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 29-06-2009                                                     |
// | Updated || 29-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SLS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SLS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sVarName    =   CCS_GetConvertedString(sLine, 1, FALSE);
    string  sValue      =   CCS_GetConvertedString(sLine, 2, FALSE);
    int     nRow        =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    int     bOperation  =   CCS_GetConvertedInteger(sLine, 4, FALSE);
    object  oObject     =   CCS_GetConvertedObject(sLine, 5, FALSE);
    string  sMessage;

    //  neplatny objekt
    if  (!GetIsObjectValid(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny nazev promenne
    if  (sVarName   ==  "")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid variable name";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny radek
    if  (nRow   <   -2)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid row";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int     nLast       =   gli(oObject, sVarName + "_S_ROWS");

    //  mazani radku kdyz zadne radky neexistuji
    if  (!nLast
    &&  nRow        !=  -1
    &&  bOperation  ==  SYS_M_DELETE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Cannot delete a row which is unset";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  mazani radku vyssiho nez je posledni
    if  (nRow       >   nLast
    &&  bOperation  ==  SYS_M_DELETE)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Cannot delete a row which is higher than the last one";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int nNewRow =   nRow;

    if  (nRow   ==  -2)
    {
        nNewRow =   nLast;

        if  (bOperation !=  SYS_M_DELETE)
        nNewRow++;
    }

    string  sNewValue   =   sValue;
    string  sPost       =   (nNewRow    !=  -1) ?   "_" + itos(nNewRow) :   "";

    if  (bOperation !=  SYS_M_DELETE)
    sMessage    =   Y1 + "( ? ) " + W2 + "Setting variable [" + G2 + sVarName + sPost + W2 + "] on object [" + G2 + GetName(oObject) + W2 + "] to value [" + G2 + sValue + W2 + "]";   else
    sMessage    =   Y1 + "( ? ) " + W2 + "Deleting variable [" + G2 + sVarName + sPost + W2 + "] on object [" + G2 + GetName(oObject) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);
    sls(oObject, sVarName, sValue, nRow, bOperation);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
