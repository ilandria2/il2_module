// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Corpse Create script                         |
// | File    || vg_s1_ds_corpsec.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 07-11-2008                                                     |
// | Updated || 07-11-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery vytvorit mrtvolu hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_const"
#include    "vg_s0_oes_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_d"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    if  (!GetIsPC(oPC)) return;

    DelayCommand(2.0f, LoadPersistentData(oPC, SYSTEM_DS_PREFIX, "LOGIN"));
}
