// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Core                               |
// | File    || vg_s1_acs_core_c                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu ACS typu C
*/
// -----------------------------------------------------------------------------

#include    "vg_s0_acs_const"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_ids_core_c"
#include    "x0_i0_position"

// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+

// Gets recipe item description
string ACS_GetRecipeDescription(int recipeId);

// Modifies the weight-based item's recipe quantity by value
void ACS_ModifyWeightedItemQuantity(object item, int value);

// Sets the weight-based item's recipe quantity to amount
void ACS_SetWeightedItemQuantity(object item, int amount);

// Gets the formatted weight-based indicator for a recipe
string ACS_GetWeightedRecipeName(int recipe, int weight);

// Gets the display name of the recipe
string ACS_GetRecipeName(int recipeId, int bNoColor = FALSE);

// Gets the item recipe id associated to the resRef
int ACS_GetRecipeForResRef(string resRef);

// Gets the bModifier modifier of the nRecipe item recipe
// use ACS_MODIFIER_* constants
float ACS_GetRecipeModifier(int nRecipe, int bModifier);

// Gets the current stack amount for weighted item
// i.e. Fogflower 97g, recipe mod weight = 1.3, roundDown(97 / 1.3) = 74 qty
int ACS_GetItemStackSizeForItem(object oItem);

// Gets the current stack amount for a combination of weight and weighted item's resRef
int ACS_GetItemStackSize(int weight, string resRef);

// Gets the display version of a weight based recipe item
string ACS_GetWeightedRecipeName(int recipe, int weight);

// Gets the display version of a weight based recipe item's stack size
string ACS_GetWeightedRecipeNameStack(int recipe, int weight);

// +---------++----------------------------------------------------------------+
// | Name    || ACS_RefreshName                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Obnovi nazev predmetu na zaklade jeho vahy ve formatu:
//  Nazev (#,#j)
//
// -----------------------------------------------------------------------------
void    ACS_RefreshNameAndDescription(object oItem);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_RefreshMoney                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Na zaklade UnIdentified description u predmetu "Mesec na penize" se obnovi
//  Identified description (viditelny popis) udavajici pocet minci a vaha
//
// -----------------------------------------------------------------------------
void    ACS_RefreshMoney(object oItem);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_GetMoney                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati pocet penez
//
// -----------------------------------------------------------------------------
int     ACS_GetMoney(object oTarget);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_GiveMoney                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prida objektu oTarget nAmount penez
//  ** Zaporni hodnota promenne nAmount znamena "Take/Vzit"
//
// -----------------------------------------------------------------------------
void    ACS_GiveMoney(object oTarget, int nAmount = 0);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_FindItemUsingRecipeOnObject                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati predmet podle receptu nRecipe s upresnenim nSpec# u objektu oTarget
//
// -----------------------------------------------------------------------------
object  ACS_FindItemUsingRecipeOnObject(int nRecipe, object oTarget = OBJECT_SELF, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1, int nNth = 1);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_CreateItemUsingRecipe                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Vytvori predmet podle receptu nRecipe s upresnenim nSpec# u objektu oTarget
//  Pokud neni oTarget definovan, pak predmet vytvori v lokaci lLocation
//  ** Recept -1 = recept ACS_RECIPE_MONEY (default 187) = penize, kde promenne
//  ** nSpec# udavaji vysku konkretni meny, kterou ma mesec penez obsahovat
//
// -----------------------------------------------------------------------------
object  ACS_CreateItemUsingRecipe(int nRecipe, int nAmount, location lLocation, object oTarget, int bExist = TRUE, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_RemoveItemUsingRecipe                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Odebrani urceneho objemu nAmount predmetu podle receptu nRecipe u oTarget
//
// -----------------------------------------------------------------------------
void    ACS_RemoveItemUsingRecipe(int nRecipe, int nAmount, object oTarget = OBJECT_SELF, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_GetTargetPassedCargo                                       |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati hodnotu udavajici kolik krat objekt oObject splni kriteria
//  zakazky/prani bCargo
//
// -----------------------------------------------------------------------------
int     ACS_GetTargetPassedCargo(object oObject, int bCargo);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_PassCargoForTarget                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Vezme objektu oObject predmety se zaporni qty a prida predmety s pozitivni
//  qty definovane v zakazce/prani bCargo (nTimes-krat)
//
// -----------------------------------------------------------------------------
void    ACS_PassCargoForTarget(object oObject, int bCargo, int nTimes = 1, int bDB = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_GetTargetHasRecipe                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati kolik predmetu objekt oObject vlastni podle receptu nRecipe
//  a upresneni nSpec#
//
// -----------------------------------------------------------------------------
int     ACS_GetTargetHasRecipe(object oObject, int nRecipe, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1);



// +---------++----------------------------------------------------------------+
// | Name    || ACS_StartEvent                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zacne event bIndex od zadavatele oNPC
//
// -----------------------------------------------------------------------------
void    ACS_StartEvent(object oNPC, int bIndex);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+

// +---------------------------------------------------------------------------+
// |                              ACS_GetRecipeName                            |
// +---------------------------------------------------------------------------+

string ACS_GetRecipeName(int recipeId, int bNoColor = FALSE)
{
    string part = "_DISPLAY";
    if (bNoColor) part += "_NOCOLOR";
    return gls(GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT"), SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(recipeId) + part);
}

string ACS_GetRecipeDescription(int recipeId)
{
    return C_NORM + "There is nothing special about this item." + _C;
}


// +---------------------------------------------------------------------------+
// |                            ACS_GetRecipeForResRef                         |
// +---------------------------------------------------------------------------+

int ACS_GetRecipeForResRef(string resRef)
{
    return gli(GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT"), SYSTEM_ACS_PREFIX + "_RESREF_" + lower(resRef) + "_RID");
}



// +---------------------------------------------------------------------------+
// |                            ACS_GetRecipeModifier                          |
// +---------------------------------------------------------------------------+

float ACS_GetRecipeModifier(int nRecipe, int bModifier)
{
    string modName = bModifier == ACS_MODIFIER_WEIGHT ? "MODWEIGHT" : bModifier == ACS_MODIFIER_DURABILITY ? "MODDURAB" : "MODCOST";
    return glf(GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT"), SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_" + modName);
}



// +---------------------------------------------------------------------------+
// |                            ACS_GetItemStackSize                           |
// +---------------------------------------------------------------------------+

int ACS_GetItemStackSizeForItem(object oItem)
{
    return ACS_GetItemStackSize(IPS_GetItemWeight(oItem), GetResRef(oItem));
}

int ACS_GetItemStackSize(int weight, string resRef)
{
    return ftoi(weight / ACS_GetRecipeModifier(ACS_GetRecipeForResRef(resRef), ACS_MODIFIER_WEIGHT));
}



// +---------------------------------------------------------------------------+
// |                              ACS_StartEvent                               |
// +---------------------------------------------------------------------------+



void    EventMessage(int bIndex, string sMessage)
{
    if  (sMessage   ==  "") return;

    int     n;
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oArea   =   GetObjectByTag(gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_AREATAG"));
    object  oPC     =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, GetFirstObjectInArea(oArea), n = 1);

    while   (GetIsObjectValid(oPC))
    {
        msg(oPC, sMessage, TRUE, FALSE);

        oPC =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, GetFirstObjectInArea(oArea), ++n);
    }
}



void    LoopEvent(object oNPC, int bIndex)
{
    SpawnScriptDebugger();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    object  oOwner  =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_OWNER");
    object  oStart  =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_START");
    int     nLength =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_LENGTH");
    int     nLast   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WAVES");
    int     nWave   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_WAVE");
    int     nTime   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_TIME") + 10;
    int     n;
    string  sMessage;

    //  konec eventu - nezustal zde zadnej zivej hrac - smazani zivych spawnutych bytosti
    object  oPC =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, oStart, 1, CREATURE_TYPE_IS_ALIVE, TRUE);

    if  (!GetIsObjectValid(oPC))
    {
        object  oCreature   =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_NOT_PC, oStart, n = 1, CREATURE_TYPE_IS_ALIVE, TRUE);

        while   (GetIsObjectValid(oCreature))
        {
            DestroyObject(oCreature);

            oCreature   =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_NOT_PC, oStart, ++n, CREATURE_TYPE_IS_ALIVE, TRUE);
        }

        SetPlotFlag(oOwner, FALSE);
        DestroyObject(oOwner);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 1);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_FULL", -1);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_WAVE_READY", 0);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_WAVE", 1);
        sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_TIME", 0);
        return;
    }

    //  n-te kolo
    if  (nWave  <   nLast)
    {
        //  zacatek noveho kola
        if  (nTime / nLength)
        {
            sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_WAVE", ++nWave);
            sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_TIME", nTime = 0);
        }
    }

    //  zacalo nove kolo
    if  (nTime  <=  10)
    {
        string  sResRef =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WV_" + itos(nWave) + "_RESREF", n = 1);
        int     nAmount =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WV_" + itos(nWave) + "_AMOUNT", n);
        int     nWP     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WAYPOINT_O_ROWS");
        object  oWP     =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WAYPOINT", 1 + Random(nWP));
        int     c;
        object  oCreate;
        location    lLoc;
        string  sSound;

        sSound      =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WV_" + itos(nWave) + "_SOUND");
        sMessage    =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WV_" + itos(nWave) + "_TEXT");
        sMessage    =   ConvertTokens(sMessage, oOwner, TRUE);

        AssignCommand(oOwner, SpeakString(sMessage));
        AssignCommand(oOwner, PlaySound(sSound));
        sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 0);
        sli(oNPC, SYSTEM_ACS_PREFIX + "_WAVE_READY", 0);
        sli(oOwner, SYSTEM_ACS_PREFIX + "_EVENT_READY", 0);
        sli(oOwner, SYSTEM_ACS_PREFIX + "_WAVE_READY", 0);

        //  spawn vsech bytosti pro nWave kolo (nahodny spawn point)
        while   (sResRef    !=  "")
        {
            //  n-krat spawnout danou bytost
            while   (++c    <=  nAmount)
            {
                oWP     =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WAYPOINT", 1 + Random(nWP));
                oCreate =   CreateObject(OBJECT_TYPE_CREATURE, sResRef, GetLocation(oWP), FALSE);
                //lLoc    =   Location(GetArea(oStart), GetAwayVector(GetPosition(oStart), 5.0f, GetAngleBetweenLocations(GetLocation(oStart), GetLocation(oWP))), GetFacing(oWP));

                ChangeToStandardFaction(oCreate, STANDARD_FACTION_HOSTILE);
                //DelayCommand(0.75f, AssignCommand(oCreate, ActionMoveToLocation(lLoc, TRUE)));
                DelayCommand(0.60, AssignCommand(oCreate, ClearAllActions(TRUE)));
                DelayCommand(1.50, AssignCommand(oCreate, ActionRandomWalk()));
                //DelayCommand(0.75, AssignCommand(oCreate, ActionAttack(GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, oOwner, 1, CREATURE_TYPE_IS_ALIVE, TRUE), FALSE)));
            }

            sResRef =   gls(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WV_" + itos(nWave) + "_RESREF", ++n);
            nAmount =   gli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_WV_" + itos(nWave) + "_AMOUNT", n);
            c       =   0;
        }
    }

    if  (nTime  >   10)
    {
        //  zadne zive nepratelske NPC v oblasti nezustalo
        object  oCreature   =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_NOT_PC, oOwner, 1, CREATURE_TYPE_IS_ALIVE, TRUE, CREATURE_TYPE_REPUTATION, REPUTATION_TYPE_ENEMY);

        if  (!GetIsObjectValid(oCreature))
        {
            if  (nWave  ==  nLast)
            {
                sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 2);
                sli(oOwner, SYSTEM_ACS_PREFIX + "_EVENT_READY", 2);

                AssignCommand(oOwner, PlaySound("gui_quest_done"));
                return;
            }

            else
            {
                sli(oNPC, SYSTEM_ACS_PREFIX + "_WAVE_READY", 1);
                sli(oOwner, SYSTEM_ACS_PREFIX + "_WAVE_READY", 1);
            }
        }
    }

    sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_TIME", (!nTime) ? 10 : nTime);
    DelayCommand(10.0f, LoopEvent(oNPC, bIndex));
}



void    ACS_StartEvent(object oNPC, int bIndex)
{
    if  (!GetIsObjectValid(oNPC)
    ||  bIndex  <   100)    return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    int     bReady  =   gli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY");

    if  (!bReady)   return;

    object  oOwner  =   glo(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_OWNER");

    sli(oNPC, SYSTEM_ACS_PREFIX + "_EVENT_READY", 0);
    sli(oNPC, SYSTEM_ACS_PREFIX + "_WAVE_READY", 0);
    sls(oOwner, "M_WAVE_NEXT", "Continue!");
    sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_WAVE", 1);
    sli(oSystem, SYSTEM_ACS_PREFIX + "_EVENT_" + itos(bIndex) + "_P_TIME", 0);
    DelayCommand(10.0f, LoopEvent(oNPC, bIndex));
}



// +---------------------------------------------------------------------------+
// |                              ACS_RefreshMoney                             |
// +---------------------------------------------------------------------------+



void    ACS_RefreshMoney(object oItem)
{
    //SpawnScriptDebugger();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    string  sSpec   =   GetDescription(oItem, FALSE, FALSE);
    int     nValue  =   (sSpec  ==  "") ?   0   :   stoi(sub(sSpec, 0, 6));
    int     nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(ACS_RECIPE_MONEY) + "_REL_GROUP", 1);
    float   fWeight;
    string  sName, sDesc;

    fWeight =   glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nGroup) + "_MODWEIGHT") * nValue;
    sDesc   =   gls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nGroup) + "_DISPLAY");
    sName   =   Y2 + "Bag of coins:\n" + sDesc + Y1 + " x" + itos(nValue);
    sDesc   =   W2 + "The bag has:\n" + sDesc + Y1 + " x" + itos(nValue);

    SetName(oItem, sName);
    SetDescription(oItem, sDesc, TRUE);
    IPS_SetItemWeight(oItem, ftoi(fWeight));
}



// +---------------------------------------------------------------------------+
// |                               ACS_GetMoney                                |
// +---------------------------------------------------------------------------+



int     ACS_GetMoney(object oTarget)
{
    if  (!GetIsObjectValid(oTarget))    return  0;

    int nResult;

    if  (GetBaseItemType(oTarget)   ==  BASE_ITEM_MONEY)
    {
        nResult =   stoi(sub(GetDescription(oTarget, FALSE, FALSE), 0, 6));

        return  nResult;
    }

    if  (!GetHasInventory(oTarget))     return  0;

    int     n       =   0;
    object  oItem;

    oItem   =   ACS_FindItemUsingRecipeOnObject(ACS_RECIPE_MONEY, oTarget, -1, -1, -1, -1, -1, ++n);

    while   (GetIsObjectValid(oItem))
    {
        nResult +=  stoi(sub(GetDescription(oItem, FALSE, FALSE), 0, 6));
        oItem   =   ACS_FindItemUsingRecipeOnObject(ACS_RECIPE_MONEY, oTarget, -1, -1, -1, -1, -1, ++n);
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                               ACS_GiveMoney                               |
// +---------------------------------------------------------------------------+



void    ACS_GiveMoney(object oTarget, int nAmount = 0)
{
    if  (!GetIsObjectValid(oTarget))    return;

    if  (GetBaseItemType(oTarget)   ==  BASE_ITEM_MONEY)
    {
        int     nValue  =   stoi(sub(GetDescription(oTarget, FALSE, FALSE), 0, 6));
        string  sSpec   =   right("000000" + itos(nAmount + nValue), 6);

        SetDescription(oTarget, sSpec, FALSE);
        ACS_RefreshMoney(oTarget);
        return;
    }

    if  (!GetHasInventory(oTarget))     return;
    if  (!nAmount)                      return;
    if  (nAmount    >   0)  ACS_CreateItemUsingRecipe(ACS_RECIPE_MONEY, nAmount, li(), oTarget, TRUE, 0, 0, 0, 0, 0);

    else
    {
        int     n, nValue;
        string  sSpec;
        object  oMoney  =   ACS_FindItemUsingRecipeOnObject(ACS_RECIPE_MONEY, oTarget, -1, -1, -1, -1, -1, ++n);

        while   (GetIsObjectValid(oMoney))
        {
            sSpec   =   GetDescription(oMoney, FALSE, FALSE);
            nValue  =   stoi(sub(sSpec, 0, 6));

            if  (nValue <=  -nAmount)
            {
                nValue  =   0;
                nAmount +=  nValue;

                SetPlotFlag(oMoney, FALSE);
                Destroy(oMoney);
            }

            else
            {
                nValue  +=  nAmount;
                nAmount =   0;
                sSpec   =   right("000000" + itos(nValue), 6) + sub(sSpec, 6, len(sSpec) - 6);

                SetDescription(oMoney, sSpec, FALSE);
                ACS_RefreshMoney(oMoney);
            }

            if  (!nAmount)  break;

            oMoney  =   ACS_FindItemUsingRecipeOnObject(ACS_RECIPE_MONEY, oTarget, -1, -1, -1, -1, -1, ++n);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                       ACS_FindItemUsingRecipeOnObject                     |
// +---------------------------------------------------------------------------+



object  ACS_FindItemUsingRecipeOnObject(int nRecipe, object oTarget = OBJECT_SELF, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1, int nNth = 1)
{
    if  (nRecipe    <   100
    &&  nRecipe     >   0)              return  OBJECT_INVALID;
    if  (!GetIsObjectValid(oTarget))    return  OBJECT_INVALID;
    if  (!GetHasInventory(oTarget))     return  OBJECT_INVALID;

    //  pro rychlejsi pouziti, recept s cislem -1 reprezentuje recept ACS_RECIPE_MONEY (penize)
    nRecipe =   (nRecipe    ==  -1) ?   ACS_RECIPE_MONEY    :   nRecipe;

    if  (nRecipe    !=  ACS_RECIPE_MONEY)
    {
        nSpec1  =   (!nSpec1)   ?   1   :   nSpec1;
        nSpec2  =   (!nSpec2)   ?   1   :   nSpec2;
        nSpec3  =   (!nSpec3)   ?   1   :   nSpec3;
        nSpec4  =   (!nSpec4)   ?   1   :   nSpec4;
        nSpec5  =   (!nSpec5)   ?   1   :   nSpec5;
    }

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    string  sResRef =   upper(gls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_RESREF"));

    //  recept nema zadefinovany zadny ressref
    if  (sResRef    ==  "")
    {
        logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to find an item using recipe ID [" + itos(nRecipe) + "]: Blank ResRef specified.");
        return  OBJECT_INVALID;
    }

    string  sTag, sSpec;
    int     nSpec, nGroup, n, nXth;
    object  oItem   =   (nNth   <   0)  ?   GetNextItemInInventory(oTarget) :   GetFirstItemInInventory(oTarget);

    while   (GetIsObjectValid(oItem))
    {
        sTag    =   GetTag(oItem);

        if  (sTag   ==  sResRef)
        {
            ++nXth;

            if  (nRecipe    ==  ACS_RECIPE_MONEY
            &&  nXth        >=  abs(nNth))   break;

            //  skupina 1
            nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

            if  (!nGroup)
            if  (nXth   >=  abs(nNth))   break;
            if  (nGroup >   0)
            {
                nSpec   =   stoi(sub(GetDescription(oItem, FALSE, FALSE), 0, 2));

                if  (nSpec  ==  nSpec1)
                {
                    //  skupina 2
                    nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                    if  (!nGroup)
                    if  (nXth   >=  abs(nNth))   break;
                    if  (nGroup >   0)
                    {
                        nSpec   =   stoi(sub(GetDescription(oItem, FALSE, FALSE), 2, 2));

                        if  (nSpec  ==  nSpec2)
                        {
                            //  skupina 3
                            nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                            if  (!nGroup)
                            if  (nXth   >=  abs(nNth))   break;
                            if  (nGroup >   0)
                            {
                                nSpec   =   stoi(sub(GetDescription(oItem, FALSE, FALSE), 4, 2));

                                if  (nSpec  ==  nSpec3)
                                {
                                    //  skupina 4
                                    nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                                    if  (!nGroup)
                                    if  (nXth   >=  abs(nNth))   break;
                                    if  (nGroup >   0)
                                    {
                                        nSpec   =   stoi(sub(GetDescription(oItem, FALSE, FALSE), 6, 2));

                                        if  (nSpec  ==  nSpec4)
                                        {
                                            //  skupina 5
                                            nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                                            if  (!nGroup)
                                            if  (nXth   >=  abs(nNth))   break;
                                            if  (nGroup >   0)
                                            {
                                                nSpec   =   stoi(sub(GetDescription(oItem, FALSE, FALSE), 8, 2));

                                                if  (nSpec  ==  nSpec5)
                                                if  (nXth   >=  abs(nNth))   break;
                                            }   //  skupina 5
                                        }
                                    }   //  skupina 4
                                }
                            }   //  skupina 3
                        }
                    }   //  skupina 2
                }
            }   //  skupina 1
        }

        oItem   =   GetNextItemInInventory(oTarget);
    }

    return  oItem;
}



// +---------------------------------------------------------------------------+
// |                        Create recipe help funcitons                       |
// +---------------------------------------------------------------------------+

void ACS_ModifyWeightedItemQuantity(object item, int value)
{
    int recipe = ACS_GetRecipeForResRef(GetResRef(item));
    float fWeight = ACS_GetRecipeModifier(recipe, ACS_MODIFIER_WEIGHT);
    int qty = ACS_GetItemStackSizeForItem(item);
    qty += value;
    if (qty < 1)
        qty = 1;

    int weight = ftoi(fWeight * qty);
    if (weight < 1)
        weight = 1;

    IPS_SetItemWeight(item, weight);
    ACS_RefreshNameAndDescription(item);
}


void ACS_SetWeightedItemQuantity(object item, int amount)
{
    int recipe = ACS_GetRecipeForResRef(GetResRef(item));
    float fWeight = ACS_GetRecipeModifier(recipe, ACS_MODIFIER_WEIGHT);
    int weight = ftoi(fWeight * amount);

    if (weight < 1)
        weight = 1;

    IPS_SetItemWeight(item, weight);
    ACS_RefreshNameAndDescription(item);
}


object  ACS_FinishRecipeForItem(int nRecipe, object oItem, int nAmount, int nSpec1, int nSpec2, int nSpec3, int nSpec4, int nSpec5, int bExist, int bWeight)
{
    //  vlastnosti receptu mimo skupin
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    float   fWeight =   glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_MODWEIGHT");
    float   fDurab  =   glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_MODDURAB");
    string  sSpec, sDesc;
    int     n, nGroup, nValue;

    //  penize (skupina)
    if  (nRecipe    ==  ACS_RECIPE_MONEY)
    {
        fWeight =   0.0f;
        fDurab  =   0.0f;
        sSpec   =   GetDescription(oItem, FALSE, FALSE);
        nValue  =   stoi(sub(sSpec, 0, 6));
        sSpec   =   right("000000" + itos(nSpec1 + nValue), 6);
        /*sSpec   +=  right("000000" + itos(nSpec2), 6);
        sSpec   +=  right("000000" + itos(nSpec3), 6);
        sSpec   +=  right("000000" + itos(nSpec4), 6);
        sSpec   +=  right("000000" + itos(nSpec5), 6);*/

        SetDescription(oItem, sSpec, FALSE);
        ACS_RefreshMoney(oItem);
    }

    else
    {
        //  vlastnosti skupin
        int nChild, nQty;

        //  skupina 1
        nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

        if  (nGroup >   0)
        {
            nChild  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", nSpec1);

            if  (nChild <   100)
            {
                logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Invalid " + itos(nSpec1) + ".child [" + itos(nChild) + "] of " + itos(n) + ".group [" + itos(nGroup) + "]");
                return  OBJECT_INVALID;
            }

            nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_QTY", n);
            fWeight +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODWEIGHT");
            fDurab  +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODDURAB");
            sSpec   +=  right("0" + itos(nSpec1), 2);

            //  skupina 2
            nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

            if  (nGroup >=  100)
            {
                nChild  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", nSpec2);

                if  (nChild <   100)
                {
                    logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Invalid " + itos(nSpec2) + ".child [" + itos(nChild) + "] of " + itos(n) + ".group [" + itos(nGroup) + "]");
                    return  OBJECT_INVALID;
                }

                nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_QTY", n);
                fWeight +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODWEIGHT");
                fDurab  +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODDURAB");
                sSpec   +=  right("0" + itos(nSpec2), 2);

                //  skupina 3
                nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                if  (nGroup >=  100)
                {
                    nChild  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", nSpec3);

                    if  (nChild <   100)
                    {
                        logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Invalid " + itos(nSpec3) + ".child [" + itos(nChild) + "] of " + itos(n) + ".group [" + itos(nGroup) + "]");
                        return  OBJECT_INVALID;
                    }

                    nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_QTY", n);
                    fWeight +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODWEIGHT");
                    fDurab  +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODDURAB");
                    sSpec   +=  right("0" + itos(nSpec3), 2);

                    //  skupina 4
                    nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                    if  (nGroup >=  100)
                    {
                        nChild  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", nSpec4);

                        if  (nChild <   100)
                        {
                            logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Invalid " + itos(nSpec4) + ".child [" + itos(nChild) + "] of " + itos(n) + ".group [" + itos(nGroup) + "]");
                            return  OBJECT_INVALID;
                        }

                        nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_QTY", n);
                        fWeight +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODWEIGHT");
                        fDurab  +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODDURAB");
                        sSpec   +=  right("0" + itos(nSpec4), 2);

                        //  skupina 5
                        nGroup  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_GROUP", ++n);

                        if  (nGroup >=  100)
                        {
                            nChild  =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_GROUP_" + itos(nGroup) + "_CHILD", nSpec5);

                            if  (nChild <   100)
                            {
                                logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Invalid " + itos(nSpec5) + ".child [" + itos(nChild) + "] of " + itos(n) + ".group [" + itos(nGroup) + "]");
                                return  OBJECT_INVALID;
                            }

                            nQty    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_REL_QTY", n);
                            fWeight +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODWEIGHT");
                            fDurab  +=  nQty * glf(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nChild) + "_MODDURAB");
                            sSpec   +=  right("0" + itos(nSpec5), 2);
                        }
                    }
                }
            }
        }

        nAmount =   (!bWeight)  ?   ftoi(fWeight)   :   ftoi(fWeight * nAmount);
        nAmount +=  (!bExist)   ?   0               :   IPS_GetItemWeight(oItem, FALSE);

        n = len(sSpec);
        while (n < 10)
        {
            sSpec += "00";
            n += 2;
        }

        if  (!bWeight)
        IDS_UpdateItem(GetItemPossessor(oItem), oItem, ftoi(fDurab), ftoi(fDurab), FALSE);  //  durabilita pouze pro nove predmety a predmety, ktere se nestackuji
        IPS_SetItemWeight(oItem, nAmount);
        string tech = "";
        tech = ReplaceTechPart(tech, TECH_PART_RECIPE_SPEC, stoi(sSpec));
        tech = ReplaceTechPart(tech, TECH_PART_RECIPE_ID, nRecipe);
        SetDescription(oItem, tech, FALSE);
        SetObjectId(oItem);
    }

    return  oItem;
}



// +---------------------------------------------------------------------------+
// |                          ACS_CreateItemUsingRecipe                        |
// +---------------------------------------------------------------------------+

void ACS_InitStackSize(object oItem, int nAmount, object oTarget, location lLocation)
{
    int nStack = stoi(Get2DAString("baseitems", "Stacking", GetBaseItemType(oItem)));

    //  pocet podle stacku
    if  (nStack > 1)
        SetItemStackSize(oItem, GetItemStackSize(oItem) + nAmount);

    //  pocet podle poctu
    else while (nAmount-- > 1)
        CopyObject(oItem, lLocation, oTarget);
}

void ACS_InitRecipeItem(int bWeight, int bExist, object oItem, int nRecipe, int nAmount, location lLocation, object oTarget, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1)
{
    if (gli(GetFirstPC(), "acs_rcr"))SpawnScriptDebugger();

    //  aplikovani specifickych vlastnosti receptu na predmet
    ACS_FinishRecipeForItem(nRecipe, oItem, nAmount, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5, bExist, bWeight);
    ACS_RefreshNameAndDescription(oItem);

    if (lLocation != li())
        IDS_ItemToInteractiveObject(oItem);

    if  (!bWeight)
        ACS_InitStackSize(oItem, nAmount, oTarget, lLocation);
}

object  ACS_CreateItemUsingRecipe(int nRecipe, int nAmount, location lLocation, object oTarget, int bExist = TRUE, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1)
{
    //  neplatni ID receptu
    if  (nRecipe    <   100)    return  OBJECT_INVALID;
    if  (nAmount    <   1)      return  OBJECT_INVALID;

    //  penize
    if  (nRecipe    ==  ACS_RECIPE_MONEY)
    {
        oTarget =   CreateItemOnObject("vg_i_cu_money_01", oTarget);

        string  sSpec   =   right("000000" + itos(nAmount), 6);

        SetDescription(oTarget, sSpec, FALSE);
        ACS_RefreshMoney(oTarget);
        return  OBJECT_INVALID;
    }

    int bLoc;

    //  neplatni cil
    if  (!GetIsObjectValid(oTarget)
    ||  !GetHasInventory(oTarget))
    {
        if  (!GetIsObjectValid(GetAreaFromLocation(lLocation))) return  OBJECT_INVALID;

        bLoc    =   TRUE;
    }

    //  zadna specifikace = default to 1
    nSpec1  =   (!nSpec1)   ?   1   :   nSpec1;
    nSpec2  =   (!nSpec2)   ?   1   :   nSpec2;
    nSpec3  =   (!nSpec3)   ?   1   :   nSpec3;
    nSpec4  =   (!nSpec4)   ?   1   :   nSpec4;
    nSpec5  =   (!nSpec5)   ?   1   :   nSpec5;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    string  sResRef =   gls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nRecipe) + "_RESREF");

    //  recept nema zadefinovany zadny ressref
    if  (sResRef    ==  "")
    {
        logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Blank ResRef specified.");
        return  OBJECT_INVALID;
    }

    object  oItem;
    int     nStack, bWeight;
    if (gli(oTarget, "recipeitem"))SpawnScriptDebugger();

    //  vytvoreni receptu pro cil
    if  (!bLoc)
    {
        // only check for existing item if specified by parameter
        if (bExist)
        {
            oItem   =   GetItemPossessedBy(oTarget, upper(sResRef));
            bExist  =   GetIsObjectValid(oItem);
        }

        // existing item not found - create new
        if  (!bExist) oItem = CreateItemOnObject(sResRef, oTarget, 1, "");
        bWeight =   IsWeightBasedItem(oItem);

        // flag item as not to be merged with existing
        if (!bExist && bWeight)
            sli(oItem, SYSTEM_ACS_PREFIX + "_SKIP_MERGE", TRUE);

        // count-based item and one already exists
        if  (!bWeight && bExist) oItem = CreateItemOnObject(sResRef, oTarget, 1, "");
    }

    //  vytvoreni receptu na lokaci
    else
    {
        oItem   =   CreateObject(OBJECT_TYPE_ITEM, sResRef, lLocation, FALSE, "");
        bWeight =   IsWeightBasedItem(oItem);
    }

    //  doslo k chybe pri vytvareni predmetu
    if  (!GetIsObjectValid(oItem))
    {
        logentry("[" + SYSTEM_ACS_PREFIX + "] Error: Attempt to create an item using recipe ID [" + itos(nRecipe) + "]: Unable to create item from ResRef [" + sResRef + "].");
        return  OBJECT_INVALID;
    }

    ACS_InitRecipeItem(bWeight, bExist, oItem, nRecipe, nAmount, lLocation, oTarget, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5);
    return  oItem;
}



void    ACS_CreateItemUsingRecipeVoid(int nRecipe, int nAmount, location lLocation, object oTarget, int bExist = TRUE, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1)
{
    ACS_CreateItemUsingRecipe(nRecipe, nAmount, lLocation, oTarget, bExist, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5);
}



// +---------------------------------------------------------------------------+
// |                          ACS_RemoveItemUsingRecipe                        |
// +---------------------------------------------------------------------------+



void    ACS_RemoveItemUsingRecipe(int nRecipe, int nAmount, object oTarget = OBJECT_SELF, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1)
{
    //  neplatni ID receptu
    if  (nRecipe    <   100)    return;
    if  (nAmount    <   1)      return;

    //  penize
    if  (nRecipe    ==  ACS_RECIPE_MONEY)
    {
        ACS_GiveMoney(oTarget, -nAmount);//xxxxxxxxxxxx
        return;
    }

    //  neplatni cil
    if  (!GetIsObjectValid(oTarget)
    ||  !GetHasInventory(oTarget))  return;

    //  zadna specifikace = specifikace 1
    if  (nRecipe    !=  ACS_RECIPE_MONEY)
    {
        nSpec1  =   (!nSpec1)   ?   1   :   nSpec1;
        nSpec2  =   (!nSpec2)   ?   1   :   nSpec2;
        nSpec3  =   (!nSpec3)   ?   1   :   nSpec3;
        nSpec4  =   (!nSpec4)   ?   1   :   nSpec4;
        nSpec5  =   (!nSpec5)   ?   1   :   nSpec5;
    }



//  ----------------------------------------------------------------------------
//  Odebrani predmetu

    object  oItem;
    int     n, nCount, nNew, bWeight;

    oItem   =   ACS_FindItemUsingRecipeOnObject(nRecipe, oTarget, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5, n = 1);
    bWeight =   IsWeightBasedItem(oItem);

    //  prohleda vsechny predmety v inventari objektu oTarget
    while   (GetIsObjectValid(oItem))
    {
        nCount  =   (bWeight)   ?   IPS_GetItemWeight(oItem)    :   1;      //  objem nalezeneho predmetu
        nNew    =   (nCount >   nAmount)    ?   nCount - nAmount    :   0;  //  snizeni objemu podle nAmount
        nAmount -=  nCount - nNew;

        //  nalezen mensi objem nez pozadovany ke snizeni - predmet bude znicen
        if  (!nNew)
        {
            SetPlotFlag(oItem, FALSE);
            Destroy(oItem);
        }

        //  nalezen vetsi objem nez pozadovany ke snizeni - objem nalezeneho predmetu bude snizen
        else
        {
            if  (bWeight)
            IPS_SetItemWeight(oItem, nNew);
            ACS_RefreshNameAndDescription(oItem);
        }

        //  konec snizovani objemu
        if  (nAmount    <=  0)  break;

        oItem   =   ACS_FindItemUsingRecipeOnObject(nRecipe, oTarget, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5, -abs(++n));
    }

//  Odebrani predmetu
//  ----------------------------------------------------------------------------

}



// +---------------------------------------------------------------------------+
// |                          ACS_GetTargetPassedCargo                         |
// +---------------------------------------------------------------------------+



int     ACS_GetTargetPassedCargo(object oObject, int bCargo)
{
    if  (!GetIsObjectValid(oObject)
    ||  !GetHasInventory(oObject)
    ||  bCargo  <   100)    return  0;

    int     nResult =   999999999;
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    int     bRID, nRQty, n, r, qty, bWeight;
    object  oItem;

    bRID    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RID", ++r);

    //  projde kazdy predmet pro bCargo
    while   (bRID   >=  100)
    {
        nRQty   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RQTY", r);

        if  (!nRQty)    return  0;

        //  pouze zkontrolovat predmety, ktere NPC potrebuje a ne ty co da za odmenu
        if  (nRQty  <   0)
        {
            qty =   ACS_GetTargetHasRecipe(oObject, bRID,
                        gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC1", r),
                        gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC2", r),
                        gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC3", r),
                        gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC4", r),
                        gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC5", r)
                    );
            n   =   qty / abs(nRQty);

            if  (!n)    return  0;

            nResult =   (n  <   nResult)    ?   n   :   nResult;
        }

        bRID    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RID", ++r);
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                           ACS_PassCargoForTarget                          |
// +---------------------------------------------------------------------------+



void    ACS_PassCargoForTarget(object oObject, int bCargo, int nTimes = 1, int bDB = FALSE)
{
    if  (!GetIsObjectValid(oObject)
    ||  !GetHasInventory(oObject)
    ||  bCargo  <   100
    ||  !nTimes)    return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");

    int     bRID, nRQty, n, r, qty, bWeight, nAmount;
    object  oItem;

    bRID    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RID", ++r);

    //  projde kazdy predmet pro bCargo
    while   (bRID   >=  100)
    {
        nRQty   =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RQTY", r);

        if  (!nRQty)    return;

        nAmount =   nTimes * nRQty;

        /*xxxxxxxxxxxxxx
        ACS_GiveRecipeToTarget(oObject, bRID, nAmount,
            gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC1", r),
            gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC2", r),
            gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC3", r),
            gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC4", r),
            gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RSPEC5", r)
        );
        */

        bRID    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_CARGO_" + itos(bCargo) + "_ITEM_RID", ++r);
    }

    if  (bDB)
    {
        sli(oObject, SYSTEM_ACS_PREFIX + "_COMPLETE_CARGO", bCargo);
        SavePersistentData(oObject, SYSTEM_ACS_PREFIX, "COMPLETE_CARGO");
    }
}



// +---------------------------------------------------------------------------+
// |                           ACS_GetTargetHasRecipe                          |
// +---------------------------------------------------------------------------+



int     ACS_GetTargetHasRecipe(object oObject, int nRecipe, int nSpec1 = 1, int nSpec2 = 1, int nSpec3 = 1, int nSpec4 = 1, int nSpec5 = 1)
{
    if  (!GetIsObjectValid(oObject)
    ||  !GetHasInventory(oObject)
    ||  nRecipe <   100)    return  0;

    int nResult;

    if  (nRecipe    ==  ACS_RECIPE_MONEY)
    nResult =   ACS_GetMoney(oObject);

    else
    {
        int     n, bWeight;
        object  oItem;

        if  (nRecipe    !=  ACS_RECIPE_MONEY)
        {
            nSpec1  =   (!nSpec1)   ?   1   :   nSpec1;
            nSpec2  =   (!nSpec2)   ?   1   :   nSpec2;
            nSpec3  =   (!nSpec3)   ?   1   :   nSpec3;
            nSpec4  =   (!nSpec4)   ?   1   :   nSpec4;
            nSpec5  =   (!nSpec5)   ?   1   :   nSpec5;
        }

        oItem   =   ACS_FindItemUsingRecipeOnObject(nRecipe, oObject, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5, n = 1);
        bWeight =   IsWeightBasedItem(oItem);

        //  projde vsechny predmety podle receptu nRecipe a spocita jejich pocet/vahu
        while   (GetIsObjectValid(oItem))
        {
            //if  (!GetPlotFlag(oItem))
            nResult +=  (bWeight)   ?   IPS_GetItemWeight(oItem)    :   1;
            oItem   =   ACS_FindItemUsingRecipeOnObject(nRecipe, oObject, nSpec1, nSpec2, nSpec3, nSpec4, nSpec5, -abs(++n));
        }
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                              ACS_RefreshName                              |
// +---------------------------------------------------------------------------+

string ACS_GetWeightedRecipeName(int recipe, int weight)
{
    string sName = "";
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    int nGroup = gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_CHILD_" + itos(recipe) + "_GROUP");
    string sWeight = itos(weight);

    if (!nGroup)
        nGroup = gli(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(recipe) + "_REL_GROUP", 1);

    if (!nGroup) return "";
    if (weight < 1000)
    {
        sName = gls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nGroup) + "_UNIT1U");
        sWeight = sWeight + " " + sName;
    }

    else if (weight < 1000000)
    {
        sName = gls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nGroup) + "_UNIT1K");
        weight = len(sWeight);
        sWeight = sub(sWeight, 0, weight - 3) + "," + sub(sWeight, weight - 3, 1) + " " + sName;
    }

    else
    {
        sName = gls(oSystem, SYSTEM_ACS_PREFIX + "_RECIPE_" + itos(nGroup) + "_UNIT1M");
        weight = len(sWeight);
        sWeight = sub(sWeight, 0, weight - 6) + "," + sub(sWeight, weight - 6, 1) + " " + sName;
    }

    return sWeight;
}

string ACS_GetQuantityString(int amount) { return itos(amount) + "x"; }
string ACS_GetWeightedRecipeNameStack(int recipe, int weight)
{
    float fWeight = ACS_GetRecipeModifier(recipe, ACS_MODIFIER_WEIGHT);
    int qty = ftoi(weight / fWeight);

    if (qty < 1)
        qty = 1;

    return ACS_GetQuantityString(qty);
}

void ACS_RefreshNameAndDescriptionDelayed(object oItem)
{
if  (gli(GetFirstPC(), "acs_stack"))SpawnScriptDebugger();
    int bWeight = IsWeightBasedItem(oItem);
    int nRecipe = ACS_GetRecipeForResRef(GetResRef(oItem));
    if (nRecipe == ACS_RECIPE_MONEY)
        return;

    string sName = ACS_GetRecipeName(nRecipe, FALSE);
    string sDesc = ACS_GetRecipeDescription(nRecipe);
    if (bWeight)
    {
        int weight = IPS_GetItemWeight(oItem);
        string sWeight = ACS_GetWeightedRecipeName(nRecipe, weight);
        sName += Y2 + "\n (" + sWeight + ")";
        sDesc += "\n\n" + C_BOLD + "Item quantity: " + Y2 + ACS_GetWeightedRecipeNameStack(nRecipe, weight) + _C;
    }

    SetName(oItem, sName);
    SetDescription(oItem, sDesc, TRUE);
}


void ACS_RefreshNameAndDescription(object oItem)
{
    DelayCommand(0.1f, ACS_RefreshNameAndDescriptionDelayed(oItem));
}

