// +---------++----------------------------------------------------------------+
// | Name    || Placeable Location Editor (PLE) Sys info                       |
// | File    || vg_s1_ple_info.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//  Information messages for PLE system
// -----------------------------------------------------------------------------

#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ple_core"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat;

    sTemp   =   gls(OBJECT_SELF, PLE_ + "NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "LOOP_START")
    {
        object target = PLE_GetTarget(OBJECT_SELF);
        sMessage = C1 + "Relocating " + GetName(target);
        sMessage += "\n" + C2 + "Type numbers to " + C1 + "offset height";
        sMessage += "\n" + C2 + "Type numbers ending with A to " + C1 + "set angle";
        sMessage += "\n" + C1 + "Interact ability: " + C1 + "toggles between 3 modes";
        bFloat = TRUE;
    }

    else if (sCase  ==  "LOOP_END")
    {
        sMessage = C1 + "Relocation finished";
        bFloat = TRUE;
    }

    else if (sCase == "TARGET_ORIENT")
    {
        sMessage = C1 + "Angle: " + C2 + itos(ftoi(GetFacing(PLE_GetTarget(OBJECT_SELF)))) + " degrees";
        bFloat = TRUE;
    }

    else if (sCase == "MODE")
    {
        int mode = PLE_GetAdjustMode(OBJECT_SELF);
        sMessage = C1 + "Target ability: " + C2;

        switch (mode)
        {
            case PLE_MODE_TERRAIN:
                sMessage += "moves on walkable terrain";
                break;

            case PLE_MODE_SIDEWAYS:
                sMessage += "moves sideways";
                break;

            case PLE_MODE_ORIENTATION:
                sMessage += "changes angle";
                break;
        }
    }

    else if (sCase == "MAX_DIST")
    {
        sMessage = R1 + "Exceeded maximum distance (" + itos(ftoi(PLE_INTERACT_DISTANCE)) + ")";
        bFloat = TRUE;
    }

    else if (sCase == "INVALID_CMD")
    {
        sMessage = R1 + "Invalid number command";
        bFloat = TRUE;
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}

