// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) Core C                         |
// | File    || vg_s0_lts_core_c.nss                                          |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu LTS typu "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || LTS_SpawnTreasure                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Spawne nAmount nahodnych predmetu z inventare oSource do inventare oTarget
//
// -----------------------------------------------------------------------------
void    LTS_SpawnTreasure(object oTarget, object oSource, int bProggress = FALSE, int nAmount = 0, int nGoldChance = 50, int nGoldMin = 10, int nGoldMax = 50);



// +---------++----------------------------------------------------------------+
// | Name    || LTS_SpawnTreasureObject                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Spawne nAmount nahodnych predmetu z inventare oSource do inventare oTarget
//
// -----------------------------------------------------------------------------
object  LTS_SpawnTreasureObject(object oTarget, object oSource, int bProggress = FALSE, int nAmount = 0, int nGoldChance = 50, int nGoldMin = 10, int nGoldMax = 50, string sNewTag = "");



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                             LTS_SpawnTreasure                             |
// +---------------------------------------------------------------------------+



void    LTS_SpawnTreasure(object oTarget, object oSource, int bProggress = FALSE, int nAmount = 0, int nGoldChance = 50, int nGoldMin = 10, int nGoldMax = 50)
{
    LTS_SpawnTreasureObject(oTarget, oSource, bProggress, nAmount, nGoldChance, nGoldMin, nGoldMax, "");
}



// +---------------------------------------------------------------------------+
// |                          LTS_SpawnTreasureObject                          |
// +---------------------------------------------------------------------------+



object  LTS_SpawnTreasureObject(object oTarget, object oSource, int bProggress = FALSE, int nAmount = 0, int nGoldChance = 50, int nGoldMin = 10, int nGoldMax = 50, string sNewTag = "")
{
    if  (!GetIsObjectValid(oTarget))    return  OBJECT_INVALID;
    if  (!GetIsObjectValid(oSource))    return  OBJECT_INVALID;

    object  oResult;
    int     nItems  =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_COUNT");
    int     i       =   0;

    nAmount =   (!nAmount)  ?   1 + Random(nItems)  :   nAmount;

    //  i-nahodne vybratych perdmetu z inventare oSource
    while   (i++    <   nAmount)
    {
        int     nNth        =   0;
        int     nRandom     =   Random(nItems);
        int     bComposed   =   gli(oSource, SYSTEM_LTS_PREFIX + "_COMPOSED_TREASURE");
        object  oContainer  =   oSource;

        //  hledani vygenerovaneho predmetu v x poskladanych kontejnerech
        if  (bComposed)
        {
            int     nBase   =   0;
            int     nPart   =   1;
            int     nCount  =   gli(oSource, SYSTEM_LTS_PREFIX + "_COMPOSED_PART_" + itos(nPart) + "_COUNT");
            string  sPart   =   gls(oSource, SYSTEM_LTS_PREFIX + "_COMPOSED_PART", nPart);

            while   (nCount >   0)
            {
                if  ((nCount + nBase)   >   nRandom)    break;

                nBase   +=  nCount;
                nCount  =   gli(oSource, SYSTEM_LTS_PREFIX + "_COMPOSED_PART_" + itos(++nPart) + "_COUNT");
                sPart   =   gls(oSource, SYSTEM_LTS_PREFIX + "_COMPOSED_PART", nPart);
            }

            oContainer  =   GetObjectByTag(LTS_TAG_TREASURE_CONTAINER + "_" + sPart);
            nRandom     -=  nBase;
        }

        object  oTemp   =   GetFirstItemInInventory(oContainer);

        while   (nNth++ <   nRandom)    oTemp   =   GetNextItemInInventory(oContainer);

        int     nStack  =   GetItemStackSize(oTemp);
        object  oCopy   =   CopyObject(oTemp, GetLocation(oTarget), oTarget, sNewTag);

        //  zruseni stavu identifikace predmetu
        if  (GetIsItemPropertyValid(GetFirstItemProperty(oCopy)))
        SetIdentified(oCopy, FALSE);

        //  nahodny pocet kdyz se jedna o predmety se "zasobama" (munice a pod.)
        if  (nStack >   1)  SetItemStackSize(oCopy, 1 + Random(nStack));

        oResult =   oCopy;
    }

    i   =   Random(100);

    //  Spawnuti zlata
    if  (i  <   nGoldChance)
    {
        //  zaklad
        nAmount =   nGoldMin + Random(1 + nGoldMax - nGoldMin);

        //  bonus za proggress v dungeonu
        if  (bProggress)
        {
            object  oLTS    =   GetObjectByTag("SYSTEM_" + SYSTEM_LTS_PREFIX + "_OBJECT");
            string  sDung   =   GetSubString(GetTag(GetArea(oTarget)), 6, 10);
            int     nTime   =   gli(oLTS, SYSTEM_LTS_PREFIX + "_DUNGEON_" + sDung + "_TIME", -1, SYS_M_DELETE);
            float   fMod    =   itof(nTime) / LTS_TIME_CONSTANT;

            fMod    =   (fMod   >   LTS_TIME_MOD_BONUS_LIMIT)   ?   LTS_TIME_MOD_BONUS_LIMIT    :   fMod;

            int     nBonus  =   ftoi(nAmount * fMod);

            //AssignCommand(oTarget, SpeakString("Amount [" + itos(nAmount) + "] Time [" + itos(nTime) + "] Mod [" + ftos(fMod) + "] Bonus [" + itos(nBonus) + "] NewAmount [" + itos(nAmount + nBonus) + "]"));

            nAmount +=  nBonus;
        }

        CreateItemOnObject("nw_it_gold001", oTarget, nAmount);
    }

    return  oResult;
}
