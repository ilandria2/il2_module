#include "vg_s0_sys_core_t"

void main()
{
    object item = GetModuleItemAcquired();
    object player = GetModuleItemAcquiredBy();
    object from = GetModuleItemAcquiredFrom();

    msg(player, "acq item: " + GetTag(item) + " source: " + GetTag(from));
}
