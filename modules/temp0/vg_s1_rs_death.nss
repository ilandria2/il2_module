// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) OnPlayerDeath script                          |
// | File    || vg_s1_rs_death.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    "Zobudi" zemreleho hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_core_c"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetLastPlayerDied();

    if  (!GetIsPC(oPC)) return;

    int     nEdge   =   RS_FATIGUE_EDGE_FSLEEP - 15;

    sli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE", nEdge);
}
