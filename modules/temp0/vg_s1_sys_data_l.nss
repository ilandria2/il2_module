// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Persistent data load script             |
// | File    || vg_s1_sys_data_l.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-09-2009                                                     |
// | Updated || 05-09-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacteni perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    slv(int nType, object oObject, string sVarName, string sValue)
{
    if  (nType  ==  SYS_V_STRING)   sls(oObject, sVarName, sValue);         else
    if  (nType  ==  SYS_V_INTEGER)  sli(oObject, sVarName, stoi(sValue));   else
    if  (nType  ==  SYS_V_FLOAT)    slf(oObject, sVarName, stof(sValue));
}

void    main()
{
    string  sQuerry;
    string  sParam  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_LOAD_PARAM", -1, SYS_M_DELETE);

    //  datum a cas
    if  (sParam ==  "SETTINGS")
    {
        sQuerry =   "SELECT Year(GameDate) as Y, Month(GameDate) as M, Day(GameDate) as D, Hour(GameDate) as H, GameDate, LastObjectId FROM sys_settings";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        {
            object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
            int     nYear   =   stoi(SQLGetData(1));
            int     nMonth  =   stoi(SQLGetData(2));
            int     nDay    =   stoi(SQLGetData(3));
            int     nHour   =   stoi(SQLGetData(4));
            int lastId = stoi(SQLGetData(6));
            lastId += 1000;

            SetCalendar(nYear, nMonth, nDay);
            SetTime(nHour, 0, 0, 0);
            sli(oSystem, SYSTEM_SYS_PREFIX + "_LAST_OBJECT_ID", lastId);

            logentry("[" + SYSTEM_SYS_PREFIX + "] Loading persistent calendar: " + SQLGetData(5), TRUE);
            logentry("[" + SYSTEM_SYS_PREFIX + "] Loading Last object ID: " + itos(lastId), TRUE);

            sli(oSystem, SYSTEM_SYS_PREFIX + "_CALENDAR_DAY", (nYear - GAMEDATE_YEAR_START - 1) * 336 + (nMonth - 1) * 28 + nDay);
        }
    }

    //  default (player enter)
    else
    {
        int nID =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        sQuerry =   "SELECT PlayerPOS " +
                    "FROM sys_players " +
                    "WHERE PlayerID = '" + itos(nID) + "'";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        {
            location    lLoc    =   APSStringToLocation(SQLGetData(1));
            object areaLoc = GetAreaFromLocation(lLoc);

            // move to default start location if invalid position was saved or if the position was saved in the player init (loading) area
            if  (!GetIsObjectValid(areaLoc) || GetIsObjectValid(GetNearestObjectByTag(SYS_TAG_PLAYER_START_INIT, GetFirstObjectInArea(areaLoc))))
                lLoc = GetLocation(GetWaypointByTag(SYS_TAG_PLAYER_START_PC));

            DelayCommand(0.5f, AssignCommand(OBJECT_SELF, JumpToLocation(lLoc)));
            DelayCommand(0.5f, SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "RELOCATION"));
        }

        else
        {
            DelayCommand(0.5f, JumpToLocation(GetLocation(GetWaypointByTag(SYS_TAG_PLAYER_START_PC))));
        }

        sQuerry =   "SELECT VarType, VarName, VarValue " +
                    "FROM sys_vars " +
                    "WHERE ObjType = -1 AND ObjDef = '" + itos(nID) + "'";

        SQLExecDirect(sQuerry);

        string  sVarName, sVarValue;
        int     nVarType;

        while   (SQLFetch() ==  SQL_SUCCESS)
        {
            nVarType    =   stoi(SQLGetData(1));
            sVarName    =   SQLGetData(2);
            sVarValue   =   SQLGetData(3);

            slv(nVarType, OBJECT_SELF, sVarName, sVarValue);
        }
    }
}
