#include "vg_s0_ids_core_c"
#include "vg_s0_sys_core_d"

object SysObj = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");


void main()
{
    int nLoop = gli(__IDS(), IDS_ + "PLACED_LOOP");

    sli(__IDS(), IDS_ + "PLACED_LOOP", ++nLoop);

    if (nLoop < IDS_SEQ_CUSTOM_LOOPS_PLACED)
        return;

    if (gli(GetFirstPC(), "itsave"))SpawnScriptDebugger();
    int area_count = gli(SysObj, SYSTEM_SYS_PREFIX + "_AREA_O_ROWS");
    int row;

    // loop through all areas
    for (row = 1; row <= area_count; row++)
    {
        object area = glo(SysObj, SYSTEM_SYS_PREFIX + "_AREA", row);
        DelayCommand(row * 0.1, IDS_CheckPlacedItemsInArea(GetTag(area), FALSE));
    }

    sli(__IDS(), IDS_ + "PLACED_LOOP", -1, -1, SYS_M_DELETE);
}
