// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) NPC Age script                          |
// | File    || vg_s1_sys_npcage.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Nastavi jmeno podle veku NPC kt. zbadalo nejakou hracskou postavu
*/
// -----------------------------------------------------------------------------



#include    "nwnx_names"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetLastPerceived();

    if  (!GetIsPC(oPC))             return;
    if  (!GetLastPerceptionSeen())  return;

    SetDynamicName(OBJECT_SELF, oPC, GetDiagram(1 + Random(100), 100));
}

