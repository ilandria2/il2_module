// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Dialog script for changing XP          |
// | File    || vg_s1_xps_change.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Zmeni hracovi zkusenosti o hodnotu, kterou zada - pro testovani
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   LPD_GetPC();
    string  sCount  =   LPD_GetSpokenText(FALSE, FALSE);
    string  sClass  =   gls(oPC, SYSTEM_LPD_PREFIX + "_SCRIPT_PARAMETER");
    int     nClass  =   stoi(sClass);
    int     nCount  =   stoi(sCount);

    XPS_AlterLevel(oPC, nClass, nCount);
}
