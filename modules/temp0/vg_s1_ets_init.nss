// +---------++----------------------------------------------------------------+
// | Name    || Exchange Trade Store (ETS) System init                         |
// | File    || vg_s1_ets_init.nss                                             |
// +---------++----------------------------------------------------------------+

#include "vg_s0_sys_core_v"
#include "vg_s0_sys_core_t"
#include "vg_s0_sys_core_d"
#include "vg_s0_oes_const"
#include "vg_s0_ets_const"

void main()
{
    string sName       = SYSTEM_ETS_NAME;
    string sVersion    = SYSTEM_ETS_VERSION;
    string sAuthor     = SYSTEM_ETS_AUTHOR;
    string sDate       = SYSTEM_ETS_DATE;
    string sUpDate     = SYSTEM_ETS_UPDATE;

    sli(__ETS(), "SYSTEM" + _ETS_ + "STATE", 2);
    logentry("[" + ETS + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", ETS, -2);
    sli(__ETS(), "SYSTEM" + _ETS_ + "ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(__ETS(), "SYSTEM" + _ETS_ + "NAME", sName);
    sls(__ETS(), "SYSTEM" + _ETS_ + "PREFIX", ETS);
    sls(__ETS(), "SYSTEM" + _ETS_ + "VERSION", sVersion);
    sls(__ETS(), "SYSTEM" + _ETS_ + "AUTHOR", sAuthor);
    sls(__ETS(), "SYSTEM" + _ETS_ + "DATE", sDate);
    sls(__ETS(), "SYSTEM" + _ETS_ + "UPDATE", sUpDate);

    sls(__ETS(), ETS_ + "NCS_SYSINFO_RESREF", "vg_s1_ets_info");
    sls(__ETS(), ETS_ + "NCS_DB_CHECK_RESREF", "vg_s1_ETS_data_c");
    sls(__ETS(), ETS_ + "NCS_DB_SAVE_RESREF", "vg_s1_ETS_data_s");
    sls(__ETS(), ETS_ + "NCS_DB_LOAD_RESREF", "vg_s1_ETS_data_l");
//    sli(__ETS(), ETS_ + "DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(__ETS(), ETS_ + "DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(__ETS(), ETS_ + "DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(__ETS(), ETS_ + "DB_CHECK_USE_GLOBAL_SCRIPT"))
        CheckPersistentTables(ETS);

    object oOES = GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_M_ON_OPEN_STORE) + "_SCRIPT", "vg_s1_ets_open", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_M_ON_CLOSE_STORE) + "_SCRIPT", "vg_s1_ets_close", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_UNACQUIRE_ITEM) + "_SCRIPT", "vg_s1_ets_itmsel", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_ACQUIRE_ITEM) + "_SCRIPT", "vg_s1_ets_itmbuy", -2);

/*
    // <INIT_CODE>
*/
    sli(__ETS(), "SYSTEM" + _ETS_ + "STATE", TRUE);
    logentry("[" + ETS + "] init done", TRUE);
}

