// +---------++----------------------------------------------------------------+
// | Name    || BattleCraft System (BCS) Spell script - AdvancedHit            |
// | File    || vg_s3_bcs_advhit.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Aplikuje instantni damage
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_bcs_core_c"
#include    "vg_s0_pis_core_c"
#include    "vg_s0_rs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    DelayAbility(OBJECT_SELF, "vg_s1_bcs_advhit");
}

