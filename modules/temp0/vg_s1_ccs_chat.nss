// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Module event script              |
// | File    || vg_s1_ccs_chat.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-08-2008                                                     |
// | Updated || 18-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosti "OnPlayerChar" pro system "CCS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_c"
#include "vg_s0_ple_core"
#include    "nwnx_chat"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sMessage    =   GetPCChatMessage();
    object  oPC         =   GetPCChatSpeaker();

    RemoveFromParty(oPC);

    if  (sMessage   ==  "") return;

    // ignore chat and handle numeric commands by PLE system
    else if (PLE_GetLoopFlag(oPC))
    {
        PLE_HandleChatCommand(oPC, sMessage);
        SetPCChatMessage("");
    }

    //  odnastaveni targetu
    else if  (sMessage   ==  ";;")
    {
        sls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "", -1, SYS_M_DELETE);
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "TARGET_UNSET");
        SetPCChatMessage("");
    }

    else

    //  zaslani seznamu kanalu mluvy
    if  (sMessage   ==  "/")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNELS");
        SetPCChatMessage("");
    }

    else

    //  jedna se o prikaz
    if  (CCS_GetIsCommand(sMessage))
    {
        CCS_PerformCommand(sMessage, oPC);
        SetPCChatMessage("");
    }

    else

    //  jedna se o kanal mluvy
    if  (CCS_GetIsChannel(sMessage))
    {
        CCS_PerformChannelChat(sMessage, oPC);

        // block message if it wasn't modified from within the channel script handler
        if (GetPCChatMessage() == sMessage)
            SetPCChatMessage("");
    }

    //  jedna se o standardni kanal mluvy
    else
    {
        if  (GetIsDM(oPC)
        &&  GetPCChatVolume()   ==  TALKVOLUME_SILENT_SHOUT)
        return;

        //  krik
        if  (left(sMessage, 1)  ==  " ")
        {
            sMessage    =   ConvertTokens(sMessage, oPC, FALSE);
            sMessage    =   C_BOLD + "[Shout] " + sMessage;

            SetPCChatVolume(TALKVOLUME_TALK);
            SetPCChatMessage(sMessage);
            logentry("[" + SYSTEM_CCS_PREFIX + "] Shout from [" + GetPCPlayerName(oPC) + "]: " + sMessage, TRUE);
        }

        else

        //  sepot
        if  (right(sMessage, 1) ==  " ")
        {
            sMessage    =   ConvertTokens(sMessage, oPC, FALSE);

            SetPCChatVolume(TALKVOLUME_WHISPER);
            SetPCChatMessage(sMessage);
        }

        //  vlastni mluva (15 metru)
        else
        {
            int     n;
            object  oCreature;

            sMessage    =   ConvertTokens(sMessage, oPC, FALSE);
            oCreature   =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, oPC, ++n);

            while   (GetIsObjectValid(oCreature))
            {
                if  (GetDistanceBetween(oCreature, oPC) >   15.0f)  break;

                NWNXChat_SendMessage(oPC, CHAT_CHANNEL_TALK, sMessage, oCreature);

                oCreature   =   GetNearestCreature(CREATURE_TYPE_PLAYER_CHAR, PLAYER_CHAR_IS_PC, oPC, ++n);
            }

            if  (n == 1)
            NWNXChat_SendMessage(oPC, CHAT_CHANNEL_TALK, sMessage, oPC);

            if (!gli(oPC, "ccs_msg"))
            SetPCChatMessage("");
        }
    }
}
