// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Refresh loop script              |
// | File    || vg_s1_pis_loop.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pri kazde sekvenci hromadne obnovi indikatory vsem hracum
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+


void    refresh(object  oPC)
{
    PIS_RefreshIndicator(oPC, OBJECT_INVALID, FALSE);
}


void    main()
{
    object  oPC =   GetFirstPC();

    while   (GetIsObjectValid(oPC))
    {
        DelayCommand(ran(1.0f, 5.0f), refresh(oPC));

        oPC =   GetNextPC();
    }
}

