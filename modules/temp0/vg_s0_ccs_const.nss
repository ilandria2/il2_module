// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Constants                        |
// | File    || vg_s0_ccs_const.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-08-2008                                                     |
// | Updated || 17-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu CCS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_CCS_NAME                     =   "Console Commands System";
const string    SYSTEM_CCS_PREFIX                   =   "CCS";
const string    SYSTEM_CCS_VERSION                  =   "1.00";
const string    SYSTEM_CCS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_CCS_DATE                     =   "19-08-2008";
const string    SYSTEM_CCS_UPDATE                   =   "17-06-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    CCS_COMMAND_PREFIX                  =   "cmd_";
const string    CCS_COMMAND_SEPARATOR               =   ";";
const string    CCS_COMMAND_VARIABLE_IDENTIFIER     =   "$";
const string    CCS_COMMAND_TAG_IDENTIFIER          =   "=";
const string    CCS_COMMAND_IDENTIFIER              =   ";";
const string    CCS_COMMAND_TOKENS_TARGET           =   "T";
const string    CCS_COMMAND_TOKENS_SELF             =   "S";
const string    CCS_COMMAND_TOKENS_MODULE           =   "M";
const string    CCS_COMMAND_TOKENS_AREA             =   "A(-*n|*n)|AS|A";
const string    CCS_COMMAND_TOKENS_PLAYER           =   "P(-*n|*n)|P";
const string    CCS_COMMAND_TOKENS_SPAWN_OBJECT     =   "O(-*n|*n)|O";
const string    CCS_COMMAND_TOKENS_INVALID          =   "X";
const string    CCS_COMMAND_TOKENS                  =   "T|S|M|A(-*n|*n)|A|AS|P(-*n|*n)|P|O(-*n|*n)|O|X";

const string    CCS_CHANNEL_IDENTIFIER              =   "/";
const string    CCS_CHANNEL_PREFIX                  =   "channel_";

const int       CCS_DATATYPE_INTEGER                =   0;
const int       CCS_DATATYPE_FLOAT                  =   1;
const int       CCS_DATATYPE_STRING                 =   2;
const int       CCS_DATATYPE_LOCATION               =   3;
const int       CCS_DATATYPE_OBJECT                 =   4;
