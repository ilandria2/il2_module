// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Player registration script              |
// | File    || vg_s1_register.nss                                             |
// -----------------------------------------------------------------------------

#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_oes_const"
#include    "nwnx_funcs"
#include    "aps_include"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void ModChar(object oPC)
{
    SetACNaturalBase(oPC, 50);
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectAttackIncrease(35, ATTACK_BONUS_MISC), oPC);
    SetPortraitResRef(oPC, "il2_");
}



void main()
{
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");

    if (!gli(oSystem, SYSTEM_SYS_PREFIX + "_SQL"))
    {
        SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "REGISTER_FAILURE");
        logentry("[" + SYSTEM_SYS_PREFIX + "]: PlayerID registration failed: NwNX2 not active", TRUE);
    }

    else
    {
        int bExists = gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_KEY_EXISTS");

        if (!bExists)
            SavePersistentData(OBJECT_SELF, SYSTEM_SYS_PREFIX, "KEY");
        SavePersistentData(OBJECT_SELF, SYSTEM_SYS_PREFIX, "REG");
    }

    // modify character properties
    DelayCommand(5.0f, ModChar(OBJECT_SELF));

    // signal login event
    slo(GetModule(), "USERDEFINED_EVENT_OBJECT", OBJECT_SELF);
    SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_LOGIN));
}
