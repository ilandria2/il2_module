// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialog System (LPD) System init                  |
// | File    || vg_s1_lpd_init.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 09-07-2009                                                     |
// | Updated || 09-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system LPD
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_lpd_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_LPD_PREFIX;
    string  sName       =   SYSTEM_LPD_NAME;
    string  sVersion    =   SYSTEM_LPD_VERSION;
    string  sAuthor     =   SYSTEM_LPD_AUTHOR;
    string  sDate       =   SYSTEM_LPD_DATE;
    string  sUpDate     =   SYSTEM_LPD_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_lpd_info");
//    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_<short>_data_c");
//    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_<short>_data_s");
//    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_<short>_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_CHAT) + "_SCRIPT", "vg_s1_lpd_pcmsg", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_PLAYER_DEATH) + "_SCRIPT", "vg_s1_lpd_dthabo", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_C_ON_CONVERSATION) + "_SCRIPT", "vg_s1_lpd_conver", -2);
    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_A_ON_ENTER) + "_SCRIPT", "vg_s1_lpd_arechn", -2);

/*

    // <INIT_CODE>

*/


    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
