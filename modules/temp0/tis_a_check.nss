// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Check interactions script        |
// | File    || tis_c_interact.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Globalni overeni podminek detekce interakci
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_core_c"
#include "vg_s0_ids_const"



// Checks whether the player oPC fulfills the requirements to perform the nAction
// (Interaction.SubType) on the interaction object oTIS
int CheckAction(object oPC, object oTIS, int nAction);



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    object  oTIS    =   glo(oPC, TIS_ + "OBJECT", -1, SYS_M_DELETE);          //  vybrany TIS objekt
    object  oDebug  =   glo(oPC, TIS_ + "DEBUG_OBJECT", -1, SYS_M_DELETE);    //  debug object
    string  sSub    =   left(GetTag(oTIS), 16);                               //  TAG
    string  sOT     =   itos(gli(__TIS(), TIS_ + "TAG_" + sSub + "_OT"));     //  TAG.TypeID
    int     nAction =   gli(oPC, TIS_ + "ACTION_CHECK");                      //  p.c. cinnosti
    int     bResult =   FALSE;

    /**/__dbgmsg(FALSE, oDebug, "< a_check: STARTED >");
    if (gli(oPC, "detact"))SpawnScriptDebugger();

    //  Detect all actions
    if (nAction == -1)
    {
        /**/__dbgmsg(FALSE, oDebug, "< a_check: Detecting all actions >");
        int nLast = gli(__TIS(), TIS_ + "OT_" + sOT + "_AS_I_ROWS");
        int n, nIID;

        /**/__dbgmsg(FALSE, oDebug, "< a_check: Possible object interactions with " + sOT + ": " + itos(nLast) + " >");
        //  loops through all actions related to a specific object type
        for (n = 1; n <= nLast; n++)
        {
            nAction = gli(__TIS(), TIS_ + "OT_" + sOT + "_AS", n);
            /**/__dbgmsg(FALSE, oDebug, "< a_check: Action #" + itos(n) + ": " + itos(nAction) + " >");

            bResult = CheckAction(oPC, oTIS, nAction);
            /**/__dbgmsg(FALSE, oDebug, "< a_check: Detected: " + itos(bResult) + " >");

            // success
            if (bResult)
                sli(oPC, TIS_ + "ACTION", nAction, -2);  //  append action to the list of detected actions
        }
    }

    //  Detect a single action
    else
    {
        /**/__dbgmsg(FALSE, oDebug, "< a_check: Detecting single action... >");
        nAction = gli(oPC, TIS_ + "ACTION", nAction); //  p.c. cinnost -> cinnost

        /**/__dbgmsg(FALSE, oDebug, "< a_check: Action ID: " + itos(nAction) + " >");
        bResult = CheckAction(oPC, oTIS, nAction);                  //  overi podminky konkretni cinnosti

        /**/__dbgmsg(FALSE, oDebug, "< a_check: Detected: " + itos(bResult) + " >");
    }

    sli(oPC, SYS_VAR_RETURN, bResult);
    /**/__dbgmsg(FALSE, oDebug, "< a_check: ENDED >");
}



// +---------------------------------------------------------------------------+
// |                            Main help functions                            |
// +---------------------------------------------------------------------------+



int CheckAction(object oPC, object oTIS, int nAction)
{
    string  sSub    =   left(GetTag(oTIS), 16);
    string  sOS     =   itos(gli(__TIS(), TIS_ + "TAG_" + sSub + "_OS"));
    int     nInter  =   gli(__TIS(), TIS_ + "AS_" + itos(nAction) + "_IID");
    int     nType   =   GetObjectType(oTIS);
    int     bResult =   FALSE;
    int     bBit;

    switch  (nInter)
    {
    case    100:            //  ACQUIRE
        bResult = gli(oTIS, TIS_ + "A_" + itos(nAction) + "_AMOUNT") != -1;
        bResult = bResult && gli(__TIS(), TIS_ + "SET_OS_" + sOS + "_AS_" + itos(nAction) + "_RECIPE") >= 100;
        break;

    case    101:            //  MINE
        bResult =   TRUE;
        break;

    case    102:            //  LOCK_UNLOCK
        bResult =   GetLocked(oTIS) || GetLockLockable(oTIS);
        break;

    case    103:            //  TRAP//xxx
        bResult =   FALSE;
        break;

    case    104:            //  SWITCH//xxx
        bResult =   FALSE;
        break;

    case    105:            //  ENTER//xxx
        bResult =   FALSE;
        break;

    case    106:            //  OPEN_CLOSE
        bResult =   nType == !GetLocked(oTIS) && (OBJECT_TYPE_DOOR || GetIsOpen(oTIS) || (nType == OBJECT_TYPE_PLACEABLE && GetHasInventory(oTIS)));
        break;

    case    107:            //  TAKE
        bResult =   sSub == IDS_TAG_PLACED_ITEM;
        break;

    case    108:            //  IGNITE//xxx-doplnit zapalovani objektu
        bResult =   FALSE;//nType == OBJECT_TYPE_CREATURE && GetIsDead(oTIS);
        break;

    case    109:            //  SLEEP//xxx
        bResult =   FALSE;
        break;

    case    110:            //  PACK_BUILD//xxx
        bResult =   FALSE;
        break;

    case    111:            //  DRINK//xxx
        bResult =   FALSE;
        break;

    case    112:            //  EXAMINE
        bResult =   FALSE;
        break;

    case    113:            //  SIT_DOWN
        bResult =   TRUE;
        break;

    case    114:            //  BASH
        //SpawnScriptDebugger();
        bBit    =   OBJECT_TYPE_CREATURE + OBJECT_TYPE_DOOR + OBJECT_TYPE_PLACEABLE;
        bBit    =   nType & bBit;
        bResult =   nType == bBit && GetCurrentHitPoints(oTIS) > -11 && !GetPlotFlag(oTIS);
        break;

    case    115:            // RELOCATE
        bResult = TRUE;
        break;

    default:                //  <else>
        bResult =   FALSE;
        break;
    }

    return  bResult;
}

