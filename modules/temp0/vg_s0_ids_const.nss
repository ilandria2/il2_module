// +---------++----------------------------------------------------------------+
// | Name    || Item Durability System (IDS) Constants                         |
// | File    || vg_s0_ids_const.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu IDS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_IDS_NAME                 =   "Item Durability System";
const string    SYSTEM_IDS_PREFIX               =   "IDS";
const string    SYSTEM_IDS_VERSION              =   "0.9";
const string    SYSTEM_IDS_AUTHOR               =   "VirgiL";
const string    SYSTEM_IDS_DATE                 =   "04-09-2011";
const string    SYSTEM_IDS_UPDATE               =   "04-09-2011";
string          IDS                             =   SYSTEM_IDS_PREFIX;
string          IDS_                            =   IDS + "_";
string          _IDS                            =   "_" + IDS;
string          _IDS_                           =   "_" + IDS + "_";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    IDS_TAG_TINDER_BOX              =   "VG_I_MI_ACS_TDBX";
const string    IDS_TAG_INVENTORY_SIZE_INDICATOR=   "VG_I_MI_IDS_INVS";
const string    IDS_TAG_ITEM_NAKED_BODY         =   "VG_I_CC_BLANK_MA";
const string    IDS_TAG_PLACED_ITEM             = "VG_P_S4_PLACEDIT";
const string    IDS_TAG_PLACED_ITEMS_AREA_OBJECT= "VG_W_S1_IDS_LIST";

const int       IDS_PLACED_ITEM_STATE_DEFAULT   = 0;
const int       IDS_PLACED_ITEM_STATE_SPAWNED   = 1;
const int       IDS_PLACED_ITEM_STATE_DESTROYED = 2;
const int       IDS_PLACED_ITEM_STATE_CHANGED   = 3;
const int       IDS_PLACEABLE_MAX_HITPOINTS     = 10000;
const int       IDS_ITEM_DURABILITY_HP_RATIO    = 10;  // 10 durability = 1 hitpoint
const int       IDS_ITEM_DURABILITY_HD_RATIO    = 150; // 150 durability = 1 hardness
const int       IDS_NUM_COMPOSED_CLOTHES_SLOTS  = 3;
const int       IDS_SEQ_CUSTOM_LOOPS_DURAB      = 5;
const int       IDS_SEQ_CUSTOM_LOOPS_PLACED     = 10;
const int       IDS_TINDER_USE_ATTEMPTS         = 10;
const int       IDS_INVENTORY_ITEMS_LIMIT_BASE  = 5;

//const int       IDS_PENALTY_DAMAGE_FIRE         =   2;
//const int       IDS_PENALTY_LIGHT               =   1;
const int       IDS_PENALTY_LIGHT_START         =   25;
const int       IDS_PENALTY_TINDER_BOX          =   2;
const int       IDS_PENALTY_LOOP_TORSO          =   20;
const int       IDS_PENALTY_LOOP_RIGHTHAND      =   15;
const int       IDS_PENALTY_LOOP_LEFTHAND       =   10;
const int       IDS_PENALTY_LOOP_TORCH          =   200;
const int       IDS_PENALTY_LOOP_INVENTORY      =   1;



// +---------------------------------------------------------------------------+
// |                             F U N C T I O N S                             |
// +---------------------------------------------------------------------------+

object __ids;
object __IDS();
object __IDS()
{
    if (!GetIsObjectValid(__ids))
        __ids = GetObjectByTag("SYSTEM" + _IDS_ + "OBJECT");

    return __ids;
}

