// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Constants                               |
// | File    || vg_s0_sys_const.nss                                            |
// +---------++----------------------------------------------------------------+

// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+

const string    SYSTEM_SYS_NAME                     =   "System Functions";
const string    SYSTEM_SYS_PREFIX                   =   "SYS";
const string    SYSTEM_SYS_VERSION                  =   "1.00";
const string    SYSTEM_SYS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_SYS_DATE                     =   "09-05-2009";
const string    SYSTEM_SYS_UPDATE                   =   "09-05-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+

const int       SQL_COMPILE_VERSION_WINDOWS         =   10;
const int       SQL_COMPILE_VERSION_UNIX            =   11;

const int       GAMEDATE_YEAR_START                 =   1000;

const int       BASE_ITEM_TINDERBOX                 =   500;
const int       BASE_ITEM_FOOD                      =   501;
const int       BASE_ITEM_LIQUID                    =   502;
const int       BASE_ITEM_WOODSMAN_AXE              =   503;
const int       BASE_ITEM_INVNETORY_BLOCK_TAB       =   504;
const int       BASE_ITEM_MONEY                     =   519;
const int       BASE_ITEM_PICKAXE                   =   520;
const int       BASE_ITEM_HERBSICKLE                =   521;
const int       BASE_ITEM_SKINKNIFE                 =   522;
const int       BASE_ITEM_DIALOGOPTION              =   523;
const int       BASE_ITEM_INVNETORY_BLOCK_SLOT      =   524;
const int       BASE_ITEM_BAG                       =   525;
const int       BASE_ITEM_INVNETORY_BLOCK_BAR       =   526;

const int       ITEM_PROPERTY_IPS_PRICE             =   209;
const int       ITEM_PROPERTY_CURRENT               =   201;
const int       ITEM_PROPERTY_MAX                   =   202;
const int       ITEM_PROPERTY_STATUS                =   203;
const int       ITEM_PROPERTY_LIQUID                =   204;
const int       ITEM_PROPERTY_FOOD                  =   205;
const int       ITEM_PROPERTY_USE_SELF              =   206;
const int       ITEM_PROPERTY_USE_TARGET            =   207;
const int       ITEM_PROPERTY_SPELL                 =   208;
const int       ITEM_PROPERTY_INVCAP_BONUS          =   210;

const string    SYS_TAG_SYSTEMS_AREA                =   "VG_AX_00_SYSAREA";
const string    SYS_TAG_PLAYER_START_PC             =   "VG_W_WP_START_PC";
const string    SYS_TAG_PLAYER_START_TS             =   "VG_W_WP_START_TS";
const string    SYS_TAG_PLAYER_START_INIT           =   "VG_W_WP_START_IL";
const string    SYS_TAG_PLAYER_INTRO_INIT           =   "VG_W_WP_START_IN";
const string    SYS_TAG_DIALOG_INVALID_PC           =   "VG_P_S1_INVALIDC";
const string    SYS_RSRF_SYSTEM_OBJECT              =   "VG_W_S1_SYSTEM_O";
const string    SYS_RSRF_USERDEFINED_SPELL_SCRIPT   =   "vg_s3_scs_spcast";

const int       SYS_PLAYER_ID_BASE                  =   100000;

const int       EFFECT_TYPE_CUTSCENE_DOMINATED      =   85;
const int       EFFECT_TYPE_DAMAGE                  =   86;
const int       EFFECT_TYPE_DEATH                   =   87;
const int       EFFECT_TYPE_APPEAR                  =   88;
const int       EFFECT_TYPE_DISAPPEAR               =   89;
const int       EFFECT_TYPE_HEAL                    =   90;
const int       EFFECT_TYPE_HP_CHANGE_WHEN_DYING    =   91;
const int       EFFECT_TYPE_KNOCKDOWN               =   92;
const int       EFFECT_TYPE_LINK_EFFECTS            =   93;
const int       EFFECT_TYPE_MODIFY_ATTACKS          =   94;
const int       EFFECT_TYPE_SUMMON_CREATURE         =   95;

const int       WEAPON_TYPE_PIERCING                =   1;
const int       WEAPON_TYPE_BLUDGEONING             =   2;
const int       WEAPON_TYPE_SLASHING                =   3;
const int       WEAPON_TYPE_SLASHING_PIERCING       =   4;
const int       WEAPON_TYPE_BLUDGEONING_PIERCING    =   5;

const int       WEAPON_SIZE_TINY                    =   1;
const int       WEAPON_SIZE_SMALL                   =   2;
const int       WEAPON_SIZE_MEDIUM                  =   3;
const int       WEAPON_SIZE_LARGE                   =   4;

const int       EQUIPABLE_SLOT_GET                  =   -1;
const int       EQUIPABLE_SLOT_NONE                 =   0x00000;
const int       EQUIPABLE_SLOT_HELMET               =   0x00001;
const int       EQUIPABLE_SLOT_ARMOR                =   0x00002;
const int       EQUIPABLE_SLOT_BOOTS                =   0x00004;
const int       EQUIPABLE_SLOT_GLOVES               =   0x00008;
const int       EQUIPABLE_SLOT_MAIN_HAND            =   0x00010;
const int       EQUIPABLE_SLOT_OFF_HAND             =   0x00020;
const int       EQUIPABLE_SLOT_CLOAK                =   0x00040;
const int       EQUIPABLE_SLOT_RINGS                =   0x00180;
const int       EQUIPABLE_SLOT_AMULET               =   0x00200;
const int       EQUIPABLE_SLOT_BELT                 =   0x00400;
const int       EQUIPABLE_SLOT_ARROW                =   0x00800;
const int       EQUIPABLE_SLOT_BULLET               =   0x01000;
const int       EQUIPABLE_SLOT_BOLT                 =   0x02000;
const int       EQUIPABLE_SLOT_CREEATURE_WEAPONS    =   0x1C000;
const int       EQUIPABLE_SLOT_CREATURE_ARMOR       =   0x20000;

/*
const int       WEAPON_WIELD_STANDARD_ONE_HANDED    =   0;
const int       WEAPON_WIELD_NOT_WIELDABLE          =   1;
const int       WEAPON_WIELD_TWO_HANDED             =   4;
const int       WEAPON_WIELD_BOW                    =   5;
const int       WEAPON_WIELD_CROSSBOW               =   6;
const int       WEAPON_WIELD_SHIELD                 =   7;
const int       WEAPON_WIELD_DOUBLE_SIDED           =   8;
const int       WEAPON_WIELD_CREATURE_WEAPON        =   9;
const int       WEAPON_WIELD_DART_SLING             =   10;
const int       WEAPON_WIELD_SHURIKEN_THROWING_AXE  =   11;
*/

const int       SYS_V_INTEGER                       =   0;
const int       SYS_V_FLOAT                         =   1;
const int       SYS_V_STRING                        =   2;
const int       SYS_V_LOCATION                      =   3;
const int       SYS_V_OBJECT                        =   4;

const int       SYS_M_SQUARE_ROOT                   =   -3;
const int       SYS_M_DIVIDE                        =   -2;
const int       SYS_M_DECREMENT                     =   -1;
const int       SYS_M_SET                           =   0;
const int       SYS_M_INCREMENT                     =   1;
const int       SYS_M_MULTIPLE                      =   2;
const int       SYS_M_POWER                         =   3;
const int       SYS_M_DELETE                        =   255;

const int       SYS_M_LOWER                         =   -2;
const int       SYS_M_LOWER_OR_EQUAL                =   -1;
const int       SYS_M_EQUAL                         =   0;
const int       SYS_M_HIGHER_OR_EQUAL               =   1;
const int       SYS_M_HIGHER                        =   2;
const int       SYS_M_NOT_EQUAL                     =   255;

const int       SYS_M_THROW_CATEGORY_DICE           =   0;
const int       SYS_M_THROW_CATEGORY_SKILL          =   1;
const int       SYS_M_THROW_CATEGORY_ABILITY        =   2;
const int       SYS_M_THROW_CATEGORY_BAB            =   3;
const int       SYS_M_THROW_CATEGORY_AC             =   4;
const int       SYS_M_THROW_CATEGORY_SR             =   5;
const int       SYS_M_THROW_CATEGORY_SAVING_THROW   =   6;

const int       SYS_C_EFFECT_CREATOR_ANY            =   0;
const int       SYS_C_EFFECT_CREATOR_SELF           =   1;
const int       SYS_C_EFFECT_CREATOR_NOT_SELF       =   2;
const int       SYS_C_EFFECT_CREATOR_IS_ALIVE       =   3;
const int       SYS_C_EFFECT_CREATOR_IS_NOT_ALIVE   =   4;
const int       SYS_C_EFFECT_CREATOR_IS_INVALID     =   5;

const int       SYS_O_DEFINITION_TYPE_VARIABLE      =   0;
const int       SYS_O_DEFINITION_TYPE_SELF          =   1;
const int       SYS_O_DEFINITION_TYPE_TAG           =   2;
const int       SYS_O_DEFINITION_TYPE_PLAYERNAME    =   3;
const int       SYS_O_DEFINITION_TYPE_SOURCE        =   4;

const int       SYS_O_ITEM_SLOT_EQUIPPED            =   18;
const int       SYS_O_ITEM_SLOT_INVENTORY           =   19;
const int       SYS_O_ITEM_DEFINITION_BY_TYPE       =   0;
const int       SYS_O_ITEM_DEFINITION_BY_TAG        =   1;
const int       SYS_O_ITEM_DEFINITION_BY_NAME       =   2;
const int       SYS_O_ITEM_DEFINITION_BY_RESREF     =   3;

//  SYS jmena promennych
const string    SYS_VAR_TARGET_OBJECT               =   "SYS_TARGET_OBJECT";
const string    SYS_VAR_JOURNAL_PREFIX              =   "NW_JOURNAL_ENTRY";
const string    SYS_VAR_RETURN                      =   "SYS_RETURN";
const string    SYS_VAR_THROW_CATEGORY              =   "SYS_THROW_CATEGORY";
const string    SYS_VAR_THROW_TYPE                  =   "SYS_THROW_TYPE";
const string    SYS_VAR_PARAMETER                   =   "SYS_PARAMETER";

//  schopnosti
const int       FEAT_TIS_TARGET                     =   1600;   //  Target
const int       FEAT_TIS_INTERACTION_MENU           =   1601;   //  Interact
const int       FEAT_SYS_MODE_RUN                   =   1625;   //  Walk / Run / Sprint
const int       FEAT_BCS_ADVANCED_HIT_STRONG        =   1700;   //  Advanced hit
const int       FEAT_BCS_ADVANCED_HIT_MIGHTY        =   1701;   //  Mocny Uder

const int       SPELL_TIS_TARGET                    =   FEAT_TIS_TARGET;
const int       SPELL_TIS_INTERACTION_MENU          =   FEAT_TIS_INTERACTION_MENU;

const int       SPELL_SYS_MODE_RUN                  =   FEAT_SYS_MODE_RUN;

const int       SPELL_BCS_ADVANCED_HIT_STRONG       =   1700;   //  Advanced hit

//  mody
const float     SYS_MODE_LOOP_DELAY                 =   0.3;
const string    SYS_MODE_SCRIPT_PREFIX              =   "mode_";

const int       SYS_MODE_WALK                       =   1;
const int       SYS_MODE_RUN                        =   2;

//  party-remove
const int       SYS_HB_PARTY_REMOVE                 =   3;

//  tech description
const int TECH_PART_OBJECT_ID = 0;
const int TECH_PART_RECIPE_ID = 1;
const int TECH_PART_RECIPE_SPEC = 2;
const int TECH_PART_DURABILITY_CURRENT = 3;
const int TECH_PART_DURABILITY_MAX = 4;
const int TECH_PART_PARENT_OBJECT_ID = 5;
const int TECH_PART_PARENT_RECIPE_ID = 6;
const int TECH_PART_CHILDREN_COUNT = 7;
const int TECH_PART_NTH_CHILD_OBJECT_ID = 8;
const int TECH_PART_NTH_CHILD_RECIPE_ID = 9;
const int OBJECT_ID_DIGITS = 6;

// parent-child relationship checking
const int CHECK_PARENT_RESULT_IS_VALID = 0;
const int CHECK_PARENT_RESULT_INVALID_OBJECT = 1;
const int CHECK_PARENT_RESULT_INVALID_OBJECT_TYPE = 2;
const int CHECK_PARENT_RESULT_INVALID_OBJECT_ID = 3;
const int CHECK_PARENT_RESULT_CHILD_HAS_PARENT = 4;
const int CHECK_PARENT_RESULT_PARENT_IS_CHILD = 5;

//  other
const int       SYS_MOVEMENT_BONUS_RUN              =   0;
const int       SYS_MOVEMENT_BONUS_SPRINT           =   49;

