// +---------++----------------------------------------------------------------+
// | Name    || Object Event System (OES) Load systems script                  |
// | File    || vg_s1_oes_loadsy.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-05-2009                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript pro inicializaci vsech systemu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_oes_const"
//#include    "vg_s0_scs_const"
#include    "vg_s0_xps_const"
#include    "vg_s0_ccs_const"
#include    "vg_s0_lpd_const"
#include    "vg_s0_bcs_const"
#include    "vg_s0_lts_const"
#include    "vg_s0_ds_const"
#include    "vg_s0_fds_const"
#include    "vg_s0_rs_const"
#include    "vg_s0_mgs_const"
#include    "vg_s0_tis_const"
#include    "vg_s0_ids_const"
#include    "vg_s0_acs_const"
#include    "vg_s0_pis_const"
#include "vg_s0_gui_const"
#include "vg_s0_ple_const"
#include "vg_s0_ets_const"


//  system init loop

void    SystemsInitLoop(int nNth = 1)
{
    string  sSystem =   gls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", nNth);

    if  (sSystem    ==  "")
    {
        //  initialization finished
        sli(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT_STATUS", -1);
        logentry("[" + SYSTEM_SYS_PREFIX + "] Systems inicialization finished");
        return;
    }

    object  oSystem =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");
    int     bInit   =   gli(oSystem, "SYSTEM_" + sSystem + "_STATE");

    nNth    +=  (bInit  ==  TRUE)   ?   1   :   0;

    if  (!bInit)
    {
        int         nCount  =   GetLocalInt(GetModule(), "SYSTEM_S_ROWS");
        vector      vCreate =   Vector(1.0f + (nCount * 1.0f), 1.0f, 1.0f);
        location    lCreate =   Location(GetObjectByTag(SYS_TAG_SYSTEMS_AREA), vCreate, 0.0f);

        oSystem =   CreateObject(OBJECT_TYPE_WAYPOINT, SYS_RSRF_SYSTEM_OBJECT, lCreate, FALSE, "SYSTEM_" + sSystem + "_OBJECT");

        sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
        ExecuteScript(GetStringLowerCase("vg_s1_" + sSystem + "_init"), GetModule());
    }

    DelayCommand(0.25f, SystemsInitLoop(nNth));
}


//  system init

void    Systems_Init()
{
    if  (gli(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT_STATUS")    !=  0)  return;

    //  initialization started
    sli(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT_STATUS", 1);
    logentry("[" + SYSTEM_SYS_PREFIX + "] Systems inicialization started");
    SystemsInitLoop();
}



// +---------------------------------------------------------------------------+
// |                                  M A I N                                  |
// +---------------------------------------------------------------------------+



void    main()
{
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_OES_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_SYS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", TIS, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_FDS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_RS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_LTS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", ETS, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_ACS_PREFIX, -2);
    //sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_SCS_PREFIX, -2);
    //sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_XPS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_LPD_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_CCS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_BCS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_DS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_MGS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", IDS, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", SYSTEM_PIS_PREFIX, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", GUI, -2);
    sls(GetModule(), SYSTEM_OES_PREFIX + "_SYSTEM_INIT", PLE, -2);

    Systems_Init();
}
