// +---------++----------------------------------------------------------------+
// | Name    || Battle Craft System (BCS) Regenerate power script              |
// | File    || vg_s1_bcs_regpow.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Loops through all players and performs the power regeneration sequence
*/
// -----------------------------------------------------------------------------

#include "vg_s0_bcs_core_c"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void    main()
{
    object oPC = GetFirstPC();

    while (GetIsObjectValid(oPC))
    {
        DelayCommand(ran(0.1f, 0.3f, 2), BCS_RegeneratePowerSequence(oPC));

        oPC = GetNextPC();
    }
}

