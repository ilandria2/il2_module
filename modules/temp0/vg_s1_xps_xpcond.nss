// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Check condition script                 |
// | File    || vg_s1_xps_spcond.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 06-06-2009                                                     |
// | Updated || 06-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skontroluje zda hrac splna vsechny podminky pro upravu zkusenosti
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sParameters =   gls(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_ALTER_XP_CONDITION_PARAMETERS", -1, SYS_M_DELETE);
    int     nOldXP      =   stoi(GetNthSubString(sParameters, 1));
    int     nNewXP      =   stoi(GetNthSubString(sParameters, 2));
    int     nOldLevel   =   stoi(GetNthSubString(sParameters, 3));
    int     nNewLevel   =   stoi(GetNthSubString(sParameters, 4));
    int     nXPClass    =   stoi(GetNthSubString(sParameters, 5));
    int     bResult     =   TRUE;

    sli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_ALTER_XP_CONDITION_RESULT", bResult);
}
