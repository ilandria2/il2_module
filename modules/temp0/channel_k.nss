// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_k.nss                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "K" (Krik)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "K";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    sText   =   ConvertTokens(sText, oPC, FALSE);
    sText   =   C_BOLD + "[Shout] " + sText;

    AssignCommand(oPC, SpeakString(sText, TALKVOLUME_TALK));
    logentry("[" + SYSTEM_CCS_PREFIX + "] Shout from [" + GetPCPlayerName(oPC) + "]: " + sText, TRUE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

