// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) System info                      |
// | File    || vg_s1_ccs_info.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-06-2009                                                     |
// | Updated || 17-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu CCS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ccs_core_t"



void    msgex(object oPC, string sMessage)
{
    location    loc =   GetLocation(oPC);
    vector      vec =   GetPositionFromLocation(loc);
    object  oExam   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

    SetDescription(oExam, sMessage);
    AssignCommand(oPC, ClearAllActions(TRUE));
    AssignCommand(oPC, ActionExamine(oExam));
    DelayCommand(2.0f, DestroyObject(oExam));
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");

    sTemp   =   gls(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "INVALID_CHANNEL")
    {
        string  sChannel    =   GetNthSubString(sTemp);

        sMessage    =   C_NEGA + "Invalid channel [" + CCS_CHANNEL_IDENTIFIER + sChannel + "]";
    }

    else

    if  (sCase  ==  "INVALID_TELL")
    {
        sMessage    =   C_NEGA + "You must select a foreign visible player character";
    }

    else

    if  (sCase  ==  "INVALID_CMD")
    {
        string  sCmd    =   GetNthSubString(sTemp);

        sMessage    =   C_NEGA + "Invalid command [" + CCS_COMMAND_IDENTIFIER + sCmd + "]";
    }

    else

    if  (sCase  ==  "USE_LIST_CMD")
    {
        sMessage    =   C_NORM + "Type " + C_BOLD + CCS_COMMAND_IDENTIFIER + "List" + C_NORM + " to show the list of available commands";
        sMessage    +=  "\n" + Y2 + "If you don't put the " + Y1 + "semicolon" + Y2 + " (" + Y1 + CCS_COMMAND_SEPARATOR + Y2 + ") at the end of the command line then this window will appear again (command's description)";
    }

    else

    if  (sCase  ==  "INSUFFICIENT_PRIVILEGES")
    {
        string  sCmd    =   GetNthSubString(sTemp);

        sMessage    =   C_NEGA + "Access denied [" + CCS_COMMAND_IDENTIFIER + sCmd + "]";
    }

    else

    if  (sCase  ==  "ACCESS_GRANTED")
    {
        string  sGroup  =   GetNthSubString(sTemp);

        sMessage    =   C_POSI + "Access granted for command group [" + sGroup + "]";
    }

    else

    if  (sCase  ==  "CMD_INFO")
    {
        string  sCmd            =   GetNthSubString(sTemp);
        string  sDescription    =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_DESCRIPTION");
        //int     nMin            =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MIN");
        int     nLast           =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
        int     n               =   1;
        int     nDataType;
        string  sParameters, sDataType, sName, sValue, sParamDesc;

        while   (n  <=  nLast)
        {
            sName       =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_NAME");
            nDataType   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_TYPE");
            sValue      =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_VALUE");
            sParamDesc  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_DESCRIPTION");
            sDataType   =   gls(oSystem, SYSTEM_CCS_PREFIX + "_DATATYPE", nDataType + 1);
            sValue      =   (sValue !=  "") ?   W1 + "(default: " + Y2 + sValue + W1 + ")"  :   sValue;

            switch  (nDataType)
            {
                case    CCS_DATATYPE_INTEGER:   sDataType   =   "Number";           break;
                case    CCS_DATATYPE_FLOAT:     sDataType   =   "Decimal number";   break;
                case    CCS_DATATYPE_STRING:    sDataType   =   "Text";             break;
                case    CCS_DATATYPE_OBJECT:    sDataType   =   "Object";           break;
                case    CCS_DATATYPE_LOCATION:  sDataType   =   "Location";         break;
            }

            sParameters +=  "\n\n" + Y2 + itos(n) + W2 + " - " + sName + W1 + " [" + sDataType + "] " + sValue + "\n" + W1 + sParamDesc;
            n++;
        }

        sMessage    =   C_INTE + "Information about the used command";
        sMessage    +=  "\n" + Y1 + "----------------------------------------\n";
        sMessage    +=  "\n" + Y1 + "Command: " + Y2 + sCmd;
        sMessage    +=  "\n" + Y1 + "Description: " + Y2 + sDescription;
        sMessage    +=  "\n" + Y1 + "Parameters: " + Y2 + sParameters;
        sMessage    +=  "\n\n" + Y2 + "If you don't put the " + Y1 + "semicolon" + Y2 + " (" + Y1 + CCS_COMMAND_SEPARATOR + Y2 + ") at the end of the command line then this window will appear again (command's description)";

        msgex(OBJECT_SELF, sMessage);
        return;
    }

    else

    if  (sCase  ==  "PARAMETERS_VALIDATION")
    {
        string  sString     =   GetNthSubString(sTemp);
        string  sCmd        =   CCS_GetCommand(sString);
        string  sParameters =   CCS_GetParameters(sString);
        int     bResult     =   stoi(GetNthSubString(sTemp, 2));
        int     nLast       =   gli(OBJECT_SELF,  SYSTEM_CCS_PREFIX + "_INFO_S_ROWS");
        int     n           =   1;

        if  (bResult)
        {
            while   (n  <=  nLast)
            {
                sls(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_INFO", "", n, SYS_M_DELETE);

                n++;
            }

            sli(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_INFO_S_ROWS", -1, -1, SYS_M_DELETE);

            return;
        }

        else
        {
            string  sDescription    =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_DESCRIPTION");
            int     nMin            =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MIN");
            int     nMax            =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
            string  sParams, sDataType, sName, sValue, sParamDesc, sProblem, sType, sParam, sUsed;
            int     nDataType;

            //SpawnScriptDebugger();
            while   (n  <=  nLast)
            {
                sName       =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_NAME");
                nDataType   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_TYPE");
                sValue      =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_VALUE");
                sParamDesc  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_DESCRIPTION");
                sDataType   =   gls(oSystem, SYSTEM_CCS_PREFIX + "_DATATYPE", nDataType + 1);
                sType       =   gls(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_INFO", n, SYS_M_DELETE);
                sCase       =   GetSubString(sType, 0, FindSubString(sType, "&") - 1);
                sParam      =   CCS_GetNthParameter(CCS_COMMAND_IDENTIFIER + sCmd + " " + sParameters, n);
                sUsed       =   (sParam ==  "") ?   sValue  :   sParam;
                sUsed       =   (sUsed  ==  "") ?   "<Empty>" :   sUsed;
                sValue      =   (sValue !=  "") ?   W1 + "(default: " + Y2 + sValue + W1 + ")"  :   sValue;

                switch  (nDataType)
                {
                    case    CCS_DATATYPE_INTEGER:   sDataType   =   "Number";           break;
                    case    CCS_DATATYPE_FLOAT:     sDataType   =   "Decimal number";   break;
                    case    CCS_DATATYPE_STRING:    sDataType   =   "Text";             break;
                    case    CCS_DATATYPE_OBJECT:    sDataType   =   "Object";           break;
                    case    CCS_DATATYPE_LOCATION:  sDataType   =   "Location";         break;
                }

                if  (sCase  ==  "MAX_PARAMETERS_OVERRUN")
                {
                    sParams +=  "\n\n" + Y2 + itos(n) + W2 + " - " + Y2 + sUsed;
                    sParams +=  "\n" + R1 + "Error: " + R2 + "This parameter is additive";
                }

                else
                {
                    sParams +=  "\n\n" + Y2 + itos(n) + W2 + " - " + sName + W1 + " [" + sDataType + "] " + sValue + "\n" + W1 + sParamDesc;
                    sParams +=  "\n" + W2 + "Value: " + Y2 + sUsed;

                    if  (sCase  ==  "WRONG_DATATYPE")
                    {
                        sParams +=  "\n" + R1 + "Error: " + R2 + "This parameter expects " + R1 + sDataType;
                    }

                    else

                    if  (sCase  ==  "NOT_ENOUGH_PARAMETERS")
                    {
                        sParams +=  "\n" + R1 + "Error: " + R2 + "This parameter is mandatory and expects " + R1 + sDataType;
                    }
                }

                n++;
            }

            sMessage    =   C_INTE + "Information about the used command";
            sMessage    +=  "\n" + Y1 + "----------------------------------------\n";
            sMessage    +=  "\n" + Y1 + "Command: " + Y2 + sCmd;
            sMessage    +=  "\n" + Y1 + "Description: " + Y2 + sDescription;
            sMessage    +=  "\n" + Y1 + "Parameters: " + Y2 + sParameters;
            sMessage    +=  "\n\n" + Y2 + "If you don't put the " + Y1 + "semicolon" + Y2 + " (" + Y1 + CCS_COMMAND_SEPARATOR + Y2 + ") at the end of the command line then this window will appear again (command's description)";

            sli(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_INFO_S_ROWS", -1, -1, SYS_M_DELETE);
            msgex(OBJECT_SELF, sMessage);
            return;
        }
    }

    else

    if  (sCase  ==  "TALK_CUSTOM")
    {

    }

    else

    if  (sCase  ==  "CHANNEL_INFO")
    {
        string  sChannel        =   GetNthSubString(sTemp);
        string  sDescription    =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_" + sChannel + "_DESCRIPTION");

        sMessage    =   C_NORM + "Channel description [" + C_BOLD + CCS_CHANNEL_IDENTIFIER + GetStringUpperCase(sChannel) + C_NORM + "]:\n" + sDescription;
    }

    else

    if  (sCase  ==  "EXECUTE_NORMAL")
    {
        string  sParams     =   "";
        string  sColor      =   C_BOLD;
        string  sString     =   GetNthSubString(sTemp);
        string  sCmd        =   CCS_GetCommand(sString);
        string  sParameters =   CCS_GetParameters(sString);
        string  sValue      =   CCS_GetNthParameter(sString, 1);
        int     nLast       =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
        int     n           =   1;

        if  (sValue ==  "")
        {
            sColor  =   C_NORM;
            sValue  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_VALUE");
        }

        while   (n  <=  nLast)
        {
            sParams +=  sColor + sValue + C_NORM + CCS_COMMAND_SEPARATOR;

            n++;
            sValue  =   CCS_GetNthParameter(sString, n);

            if  (sValue ==  "")
            {
                sColor  =   C_NORM;
                sValue  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_VALUE");
            }

            else
            {
                sColor  =   C_BOLD;
            }
        }

        if  (sParams    ==  "") sParams =   C_BOLD + "No Parameters";

        sMessage    =   "\n" + C_NORM + "Command [" + C_BOLD + CCS_COMMAND_IDENTIFIER + sCmd + C_NORM + "] Parameters [" + sParams + C_NORM + "]";

        msg(OBJECT_SELF, sMessage, TRUE, FALSE);
        return;
    }

    else

    if  (sCase  ==  "EXECUTE_TARGET")
    {
        string  sParams     =   "";
        string  sColor      =   C_BOLD;
        string  sString     =   GetNthSubString(sTemp);
        string  sCmd        =   CCS_GetCommand(sString);
        string  sParameters =   CCS_GetParameters(sString);
        string  sValue      =   CCS_GetNthParameter(sString, 1);
        int     nLast       =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETERS_MAX");
        int     n           =   1;

        if  (sValue ==  "")
        {
            sColor  =   C_NORM;
            sValue  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_VALUE");
        }

        while   (n  <=  nLast)
        {
            sParams +=  sColor + sValue + C_NORM + CCS_COMMAND_SEPARATOR;

            n++;
            sValue  =   CCS_GetNthParameter(sString, n);

            if  (sValue ==  "")
            {
                sColor  =   C_NORM;
                sValue  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_PARAMETER_" + itos(n) + "_VALUE");
            }

            else
            {
                sColor  =   C_BOLD;
            }
        }

        if  (sParams    ==  "") sParams =   C_BOLD + "None";

        sMessage    =   C_INTE + "Target: " + C_NORM + "Command [" + C_BOLD + CCS_COMMAND_IDENTIFIER + sCmd + C_NORM + "] Parameters [" + sParams + C_NORM + "]";

        msg(OBJECT_SELF, sMessage, TRUE, FALSE);
        return;
    }

    else

    if  (sCase  ==  "SYSTEM_SKILLS")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
        object  oTarget =   glo(OBJECT_SELF, SYSTEM_CCS_PREFIX + "_SYSTEM_SKILLS_OBJECT", -1, SYS_M_DELETE);

        sMessage    =   C_NORM + "Access to the command group:";

        int     n       =   1;
        string  sGroup  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", n);

        while   (sGroup !=  "")
        {
            int     bAccess =   CCS_GetHasAccess(oTarget, sGroup);
            string  sColor, sAccess;

            if  (bAccess)
            {
                sColor  =   C_POSI;
                sAccess =   "granted";
            }

            else
            {
                sColor  =   C_NEGA;
                sAccess =   "denied";
            }

            sMessage    +=  "\n" + sColor + "Group [" + sGroup + "]: access [" + sAccess + "]";
            sGroup      =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", ++n);
        }
    }

    else

    if  (sCase  ==  "TARGET_UNSET")
    {
        sMessage    =   C_POSI + "Target ability unset!";
    }

    else

    if  (sCase  ==  "USE_TELL")
    {
        sMessage    =   C_NORM + "Select a target for the " + C_BOLD + "Private Message";
    }

    else

    if  (sCase  ==  "CHANNELS")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
        string  sChan, sDesc, sMessage;
        int     n, nLast;

        nLast   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_S_ROWS");

        for (n = 1; n <= nLast; n++)
        {
            sChan       =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL", n);
            sDesc       =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_" + sChan + "_DESCRIPTION");
            sMessage    +=  "\n" + Y1 + CCS_CHANNEL_IDENTIFIER + sChan + W2 + " - " + sDesc;
        }

        //sMessage    +=  "\n" + Y1 + "/S" + W2 + " - " + "Soukromi";
        sMessage    =   "\n" + W2 + "List of available channels / commands:" + sMessage;

        msgex(OBJECT_SELF, sMessage);
        //msg(OBJECT_SELF, sMessage, TRUE, FALSE);
    }

    else

    if  (sCase  ==  "SHOUT_IS_DISABLED")
    {
        sMessage    =   C_NEGA + "Only DM can use the channel 'SHOUT' hence this message was sent using the 'TALK' channel";
    }

    else

    if  (sCase  ==  "PARTY_IS_DISABLED")
    {
        sMessage    =   C_NEGA + "Only DM can use the channel 'PARTY' hence this message was sent using the 'TALK' channel";
    }

    msg(OBJECT_SELF, sMessage, FALSE, FALSE);
}
