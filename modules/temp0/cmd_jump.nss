// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_jump.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "JUMP"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "JUMP";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    location    lTransition =  CCS_GetConvertedLocation(sLine, 1, FALSE);
    object      oTransition =  CCS_GetConvertedObject(sLine, 1, FALSE);
    object      oCreature   =  CCS_GetConvertedObject(sLine, 2, FALSE);
    string      sMessage;

    //  Neplatna bytost
    if  (!GetIsObjectValid(oCreature))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid creature";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  Neplatna lokace
    if  (!GetIsObjectValid(GetAreaFromLocation(lTransition)))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid location";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    vector  pos =   GetPositionFromLocation(lTransition);

    sMessage    =   Y1 + "( ? ) " + W2 + "Moving [" + G2 + GetName(oCreature) + W2 + "] to location [" + G2 + GetName(GetAreaFromLocation(lTransition)) + ": " + itos(ftoi(pos.x)) + "x " + itos(ftoi(pos.y)) + "y " + itos(ftoi(pos.z)) + "z" + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);
    AssignCommand(oCreature, ClearAllActions(TRUE));
    AssignCommand(oCreature, JumpToLocation(lTransition));

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
