// +---------++----------------------------------------------------------------+
// | Name    || Starting inventory - clear                                     |
// | File    || vg_s1_start_inv.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Smaze inventar a da "startovni" predmety
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_core_c"
#include    "vg_s0_acs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void InvBlock(object oPC)
{
    //msg(oPC, "inv block");
    CreateItemOnObject("vg_i_s1_invblock", oPC);
    CreateItemOnObject("vg_i_s1_invblock", oPC);
    CreateItemOnObject("vg_i_s1_invblock", oPC);
    CreateItemOnObject("vg_i_s1_invblock", oPC);
    CreateItemOnObject("vg_i_s1_invblock", oPC);
    /*CreateItemOnObject("vg_i_s1_invblock", oPC);
    GetFirstItemInInventory(oPC);
    GetNextItemInInventory(oPC);
    GetNextItemInInventory(oPC);
    GetNextItemInInventory(oPC);
    GetNextItemInInventory(oPC);
    DestroyObject(GetNextItemInInventory(oPC));*/
}



void main()
{
    object  oPC =   OBJECT_SELF;

    //  odebrani vsech predmetu vcetne zlata
    object  oItem   =   GetFirstItemInInventory(oPC);

    while   (GetIsObjectValid(oItem))
    {
        SetPlotFlag(oItem, FALSE);
        DestroyObject(oItem);

        oItem   =   GetNextItemInInventory(oPC);
    }

    int nEquip  =   0;

    oItem   =   GetItemInSlot(nEquip, oPC);

    while   (nEquip <=  NUM_INVENTORY_SLOTS)
    {
        if  (GetIsObjectValid(oItem))
        {
            SetPlotFlag(oItem, FALSE);
            DestroyObject(oItem);
        }

        oItem   =   GetItemInSlot(++nEquip, oPC);
    }

    //  nahodni obleceni do zacatku
    oItem   =   ACS_CreateItemUsingRecipe(132 + Random(2), 1, li(), oPC, FALSE);   //  obleceni (vrecovina)
    //oItem   =   LTS_SpawnTreasureObject(oPC, GetObjectByTag(LTS_TAG_TREASURE_CONTAINER + "_START"), FALSE, 1, 0, 0, 0, SYSTEM_SYS_PREFIX + "_INTRO");

    DelayCommand(0.2f, AssignCommand(oPC, ActionEquipItem(oItem, INVENTORY_SLOT_CHEST)));
    TakeGoldFromCreature(GetGold(oPC), oPC, TRUE);
    GiveGoldToCreature(oPC, 999999);
    ACS_GiveMoney(oPC, 250);
    //DelayCommand(0.1f, InvBlock(oPC));
}
