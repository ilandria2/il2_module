#include "vg_s0_ids_core_c"

void main()
{
    object player = GetPCItemLastEquippedBy();
    if (!gli(player, SYSTEM_SYS_PREFIX + "_PLAYER_ID"))
        return;

    IDS_ComposeClothes(player);
}
