// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Regenerate mana heartbeat script       |
// | File    || vg_s1_scs_regene.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Pusti smycku regenerace many
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int nCount  =   gli(oSystem, SYSTEM_SCS_PREFIX + "_MANA_REGEN_HB");

    if  (nCount <   SCS_MANA_REGEN_HEARTBEATS)
    {
        sli(oSystem, SYSTEM_SCS_PREFIX + "_MANA_REGEN_HB", 1, -1, SYS_M_INCREMENT);
        return;
    }

    object  oPC =   GetFirstPC();
    float   fDelay;

    while   (GetIsObjectValid(oPC))
    {
        if  (!GetIsDM(oPC))
        {
            /*string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

            if  (sArea  !=  "S"
            &&  sArea   !=  "X")
            {*/
                fDelay  +=  ran(0.0f, 1.0f, 2);

                DelayCommand(fDelay, SCS_RegenerateManaSequence(oPC));
            //}
        }

        oPC =   GetNextPC();
    }

    sli(oSystem, SYSTEM_SCS_PREFIX + "_MANA_REGEN_HB", -1, -1, SYS_M_DELETE);
}
