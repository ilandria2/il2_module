// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Custom modes script                     |
// | File    || vg_s3_sys_modes.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    - Na ilandrii je a bude nekolik vlastnich modu. Kazdy z nich je prezentovany
    vlastnim featem, no vzdy muze byt aktivni pouze 1.
    - Tenhle skript se pouziva jako impact skript pro vsechny featy typu "Mod",
    ktery na zaklade ID seslaneho kouzla spadajiciho pod aktivovanej feat urci
    nasledni chovani ve hre.
    - Kazdej mod ma sve vlastni ID (1, 2, ..., N) a pro kazde cislo je v init
    skriptech jednotlivych systemu zadefinovan skript, ktery se ma nasledne
    spustit.
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (GetCurrentHitPoints(OBJECT_SELF)   <   0)  return;

    //  track attempts within a short period of time
    object oSystem = GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int nLoop = gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_MODE_IN_LOOP");
    int nID = gli(oSystem, SYSTEM_SYS_PREFIX + "_MODE_ID", GetSpellId());

    DelayCommand(SYS_MODE_LOOP_DELAY, DelayActionMode(OBJECT_SELF, nID));
}

