// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_vfx.nss                                                    |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "VFX"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "VFX";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    SpawnScriptDebugger();
    int         nVfx    =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object      oTarget =   CCS_GetConvertedObject(sLine, 2, FALSE);
    location    lTarget =   CCS_GetConvertedLocation(sLine, 3, FALSE);
    float       fDur    =   CCS_GetConvertedFloat(sLine, 4, FALSE);
    string      sMessage;

    //  neplatny cil
    if  (!GetIsObjectValid(GetAreaFromLocation(lTarget))
    &&  !GetIsObjectValid(oTarget))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid target";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  neplatny efekt
    if  (nVfx   <   0)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid ID";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    int bType   =   (fDur   >   0.0f)   ?   DURATION_TYPE_TEMPORARY :   DURATION_TYPE_INSTANT;

    if  (GetIsObjectValid(oTarget)) ApplyEffectToObject(bType, EffectVisualEffect(nVfx), oTarget, fDur);
    else                            ApplyEffectAtLocation(bType, EffectVisualEffect(nVfx), lTarget, fDur);

    sMessage    =   Y1 + "( ? ) " + W2 + "Applied visual effect [" + G2 + itos(nVfx) + W2 + "] on selected target/location";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
