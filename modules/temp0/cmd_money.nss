// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_money.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "MONEY"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_acs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "MONEY";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nAmount =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oObject =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;

    SpawnScriptDebugger();

    if  (!nAmount)
    {
        nAmount     =   ACS_GetMoney(oObject);
        sMessage    =   Y1 + "( ? ) " + W2 + "Object [" + GetName(oObject) + "/" + GetTag(oObject) + "] have " + Y1 + itos(nAmount) + W2 + " money";
    }

    else
    {
        ACS_GiveMoney(oObject, nAmount);
        sMessage    =   G1 + "( ! ) " + W2 + "Modify of money on object [" + GetName(oObject) + "/" + GetTag(oObject) + "] by amount " + G1 + itos(nAmount);
    }

    msg(oPC, sMessage, TRUE, FALSE);

    return;

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

