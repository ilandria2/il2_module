// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_flag.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "FLAG"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "FLAG";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sFlag   =   upper(CCS_GetConvertedString(sLine, 1, FALSE));
    int     bBit    =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    int     nParam  =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    object  oObject =   CCS_GetConvertedObject(sLine, 4, FALSE);
    string  sMessage;

    //  Neplatny objekt
    if  (!GetIsObjectValid(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (sFlag  ==  "IDENTIFIED")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetIdentified(oObject));

        else
        SetIdentified(oObject, bBit);
    }

    else

    if  (sFlag  ==  "LOCKED")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_DOOR
        &&  GetObjectType(oObject)  !=  OBJECT_TYPE_PLACEABLE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetLocked(oObject));

        else
        SetLocked(oObject, bBit);
    }

    else

    if  (sFlag  ==  "DROPPABLE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetDroppableFlag(oObject));

        else
        SetDroppableFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "INFINITE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetInfiniteFlag(oObject));

        else
        SetInfiniteFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "CURSED")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetItemCursedFlag(oObject));

        else
        SetItemCursedFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "PICKPOCKETABLE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetPickpocketableFlag(oObject));

        else
        SetPickpocketableFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "PLOT")
    {
        if  (bBit   ==  -1)
        sMessage    =   itos(GetPlotFlag(oObject));

        else
        SetPlotFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "STOLEN")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_ITEM)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetStolenFlag(oObject));

        else
        SetStolenFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "USEABLE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_PLACEABLE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetUseableFlag(oObject));

        else
        SetUseableFlag(oObject, bBit);
    }

    else

    if  (sFlag  ==  "IMMORTAL")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_CREATURE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetImmortal(oObject));

        else
        SetImmortal(oObject, bBit);
    }

    else

    if  (sFlag  ==  "DESTROYABLE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_CREATURE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   "N/A";

        else
        AssignCommand(oObject, SetIsDestroyable(bBit));
    }

    else

    if  (sFlag  ==  "CUTSCENE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_CREATURE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetCutsceneMode(oObject));

        else
        SetCutsceneMode(oObject, bBit, nParam);
    }

    else

    if  (sFlag  ==  "COMMANDABLE")
    {
        if  (GetObjectType(oObject) !=  OBJECT_TYPE_CREATURE)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Invalid object type";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        if  (bBit   ==  -1)
        sMessage    =   itos(GetCommandable(oObject));

        else
        SetCommandable(bBit, oObject);
    }

    //  Neplatny typ
    else
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid flag [" + R2 + sFlag + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (sMessage   ==  "N/A")
    sMessage    =   R1 + "( ! ) " + W2 + "Unable to determine the status of the flag [" + R2 + sFlag + W2 + "] due to technical limitations";

    else

    if  (sMessage   !=  "")
    sMessage    =   Y1 + "( ? ) " + W2 + "Status of the flag [" + Y2 + sFlag + W2 + "] on object [" + Y2 + GetTag(oObject) + " / " + (GetName(oObject) + W2 + "] is [" + Y2 + sMessage) + W2 + "]";

    else
    sMessage    =   Y1 + "( ? ) " + W2 + "Flag [" + G2 + sFlag + W2 + "] on object [" + G2 + GetTag(oObject) + " / " + GetName(oObject) + W2 + "] was set to [" + G2 + itos(bBit) + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
