/*  VirgiL
    2018-01-14
    Placeable Location Editor (PLE) analysis

    About:
    The PLE system provides the user with an interface that allows him/her to adjust placeable objects location.

    Concept:
    - A new TIS interaction called "Adjust location" will be made available for predefined interactive placeable TIS objects
    - Optionally the object can be given restrictions that its location is adjustable only by rank X and higher (by default 0+)
    - When this interaction is selected, player will enter "Adjust" mode:
        - A loop that can be cancelled by player's movement
        - Every sequence, the Z-axis and Orientation "rulers" visual effects along with current facing-direction represented by an arrow
          will be rendered only for the player at the target location
        - Player uses these rulers as hints or indicators of relative position in the Z axis and 360 degree angle of target orientation
        - Using the "Interact" ability player switches one of the following Adjust modes
        - Adjust modes:
            - Adjust Terrain - using the Target ability the placeable is relocated at the location clicked
            - Adjust Sideways - same as "Adjust Terrain" except that Z axis is locked at the current value (i.e. "moving plate on a surface/plane")
            - Adjust Orientation - using Target ability, the placeable changes facing direction to the location clicked
          Each consecutive use of the Interact ability switches over to the next mode in row
        - Relocation happens either by using the Target ability or alternatively by issuing numeric chat commands
        - Example chat commands:
            - -27  = will lower the placeable on Z axis by 27 centimeters relative from current position
            - +66  = will raise the placeable on Z axis by 27 centimeters relative from current position
            - 245a = will set orientation of the placeable to 245 degree
            - 176  = will set the placeable position on Z axis to 176 centimeters from player's Z position
*/
