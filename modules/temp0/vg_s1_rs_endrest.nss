// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Interrupd dialog                              |
// | File    || vg_s1_rs_endrest.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 15-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Preruseni odpocinku
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_core_c"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    return;
    object  oPC     =   LPD_GetPC();
    int     nRest   =   gli(oPC, SYSTEM_RS_PREFIX + "_REST_STATE");

    if  (nRest  ==  RS_REST_STATE_SLEEP
    ||  nRest   ==  RS_REST_STATE_FORCE_SLEEP)
    return;

    //RS_Rest(oPC, RS_REST_STATE_INVALID);
}
