// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_price.nss                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "PRICE"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_sys_core_o"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "PRICE";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nPrice  =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oItem   =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;
    return;
    /*
    if  (GetObjectType(oItem)   !=  OBJECT_TYPE_ITEM)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Neni to predmet";

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    if  (nPrice ==  -1)
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Cena predmetu [" + Y2 + GetTag(oItem) + " / " + GetName(oItem) + W2 + "] je: " + Y1 + itos(IPS_GetItemPrice(oItem));

        msg(oPC, sMessage, TRUE, FALSE);
        return;
    }

    sMessage    =   G1 + "( ! ) " + W2 + "Zmena ceny predmetu [" + G2 + GetTag(oItem) + " / " + GetName(oItem) + W2 + "] na: " + G1 + itos(nPrice);

    msg(oPC, sMessage, TRUE, FALSE);
    IPS_SetItemPrice(oItem, nPrice);

    return;
    */

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

