// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Sys Info                               |
// | File    || vg_s1_xps_info.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 06-06-2009                                                     |
// | Updated || 06-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu XPS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"
#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT");

    sTemp   =   gls(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "ALTER_XP")
    {
        int     nOldXP      =   stoi(GetNthSubString(sTemp, 1));
        int     nNewXP      =   stoi(GetNthSubString(sTemp, 2));
        int     nXPClass    =   stoi(GetNthSubString(sTemp, 3));
        int     nLevel      =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nXPClass) + "_LEVEL");
        int     nPrevXP;

        if  (nLevel ==  50) nPrevXP =   XPS_GetXPByLevel(nLevel - 1, nXPClass);
        else                nPrevXP =   XPS_GetXPByLevel(nLevel, nXPClass);

        int     nNextXP     =   XPS_GetXPByLevel(nLevel + 1, nXPClass) - nPrevXP;
        string  sXP         =   gls(oSystem, SYSTEM_XPS_PREFIX + "_CLASS", nXPClass + 1);
        string  sColor      =   (nNewXP >   nOldXP) ?   C_POSI  :
                                (nNewXP <   nOldXP) ?   C_NEGA  :   C_NORM;

        nNewXP      -=  nPrevXP;
        nOldXP      -=  nPrevXP;
        sMessage    =   sColor + "[" + GetDiagram(nNewXP, nNextXP, FALSE, 254, 128, 0) + sColor + "] " + sXP + " / " + itos(nLevel) + " / " + itos(ftoi(itof(nNewXP) / itof(nNextXP) * 100)) + "%";
    }

    else

    if  (sCase  ==  "NEW_LEVEL")
    {
        int     nLast;
        int     nOldLevel   =   stoi(GetNthSubString(sTemp, 1));
        int     nNewLevel   =   stoi(GetNthSubString(sTemp, 2));
        int     nXPClass    =   stoi(GetNthSubString(sTemp, 3));
        string  sXP         =   gls(oSystem, SYSTEM_XPS_PREFIX + "_CLASS", nXPClass + 1);
        string  sChar       =   (nNewLevel  >=  nOldLevel)  ?   "+" :   "-";
        string  sColor      =   (nNewLevel  >   nOldLevel)  ?   C_POSI  :
                                (nNewLevel  <   nOldLevel)  ?   C_NEGA  :   C_NORM;

        switch  (nXPClass)
        {
            case    XPS_M_CLASS_XPS_PLAYER_CHARACTER:   nLast   =   40; break;
            default:                                    nLast   =   50; break;
        }

        sMessage    =   sColor + "[" + GetDiagram(nNewLevel, nLast, nOldLevel, FALSE) + sColor + "] " + sChar + itos(abs(nNewLevel - nOldLevel)) + " �rove� [" + sXP + " / " + itos(nNewLevel) + " / " + itos(nLast) + "]";

        msg(OBJECT_SELF, sMessage, TRUE, FALSE);
        return;
    }

    else

    if  (sCase  ==  "ENTERING_ABSORB_MODE")
    {
        sMessage    =   C_NORM + "P�echod do m�du z�sk�v�n� zku�enost�";
    }

    else

    if  (sCase  ==  "EXITING_ABSORB_MODE")
    {
        sMessage    =   C_NORM + "P�echod do m�du hromad�n� zku�enost�";
    }

    else

    if  (sCase  ==  "ABSORB_FIRST")
    {
        sMessage    =   C_NEGA + "Mus� nejd��ve vst�ebat nahromad�n� zku�enosti abys z�skal dal��";
    }

    else

    if  (sCase  ==  "SYSTEM_SKILLS")
    {
        object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_XPS_PREFIX + "_OBJECT");
        object  oTarget =   glo(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_SYSTEM_SKILLS_OBJECT", -1, SYS_M_DELETE);

        sMessage    =   C_NORM + "�rovn� a zku�enosti t��d:";

        int nClass  =   0;

        while   (nClass <=  XPS_M_CLASS_LAST)
        {
            string  sClass  =   gls(oSystem, SYSTEM_XPS_PREFIX + "_CLASS", nClass + 1);
            int     nLevel  =   gli(oTarget, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nClass) + "_LEVEL");
            int     nXP     =   gli(oTarget, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nClass) + "_XP");
            int     nEdge   =   XPS_GetXPByLevel(nLevel + 1, nClass);
            int     nPrevXP, nCurXP, nCurEd;

            if  (nLevel ==  50) nPrevXP =   XPS_GetXPByLevel(nLevel - 1, nClass);
            else                nPrevXP =   XPS_GetXPByLevel(nLevel, nClass);

            nCurXP  =   nXP - nPrevXP;
            nCurEd  =   nEdge - nPrevXP;

            int nLast;

            switch  (nClass)
            {
                case    XPS_M_CLASS_XPS_PLAYER_CHARACTER:   nLast   =   40; break;
                default:                                    nLast   =   50; break;
            }

            if  (nXP    >   0)
            sMessage    +=  "\n" + C_NORM + "[" + GetDiagram(nCurXP, nCurEd, -2, FALSE, 255, 128, 0) + C_NORM + "] " + sClass + " / " + itos(nLevel) + " / " + itos(nLast) + " (" + itos(ftoi(itof(nCurXP) / itof(nCurEd) * 100)) + "%)";

            ++nClass;
        }
    }

    else

    if  (sCase  ==  "RPX_INFO")
    {
        object  oTemp   =   GetFirstPC();
        string  sPlayers;
        int     nCount;

        while   (GetIsObjectValid(oTemp))
        {
            int nRPX    =   gli(oTemp, SYSTEM_XPS_PREFIX + "_RPX");

            if  (nRPX   >   0)
            {
                nCount++;
                sPlayers    +=  C_BOLD + GetPCPlayerName(oTemp) + " / " + C_NORM + GetName(oTemp) + ", ";
            }

            oTemp   =   GetNextPC();
        }

        sMessage    =   C_NORM + "Seznam hr��� odm��ovan�ch za RP (" + C_BOLD + itos(nCount) + C_NORM + "):";
        sMessage    +=  "\n~~~~~~~~~~~~~~~~~~~~~~";
        sMessage    +=  "\n" + sPlayers;

        oTemp   =   GetFirstPC();

        while   (GetIsObjectValid(oTemp))
        {
            if  (GetIsDM(oTemp)
            ||  CCS_GetHasAccess(oTemp, "PM"))
            msg(oTemp, sMessage, FALSE, FALSE);

            oTemp   =   GetNextPC();
        }
    }

    msg(OBJECT_SELF, sMessage, FALSE, FALSE);
}
