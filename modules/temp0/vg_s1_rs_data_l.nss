// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Persistent data load script                   |
// | File    || vg_s1_rs_data_l.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-08-2009                                                     |
// | Updated || 16-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacte perzistentni data pro hrace systemu RS
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_rs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sQuerry;

    sQuerry =   "SELECT " +
                    "Energy " +
                "FROM " +
                    "rs_fatigue " +
                "WHERE " +
                    "PlayerID = '" + itos(nID) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        int nValue  =   stoi(SQLGetData(1));

        sli(OBJECT_SELF, SYSTEM_RS_PREFIX + "_FATIGUE", nValue);
    }
}
