// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) NPC Name setup                          |
// | File    || vg_s1_sys_npcnam.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 15-09-2009                                                     |
// | Updated || 15-09-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Prejmenuje NPC podle toho o jakou rasu a pohlavi se jedna:
    - Clovek (Muz)
    - Elf (Zena)
    - Skret
    - Ork

    Cilem je RP efekt, kde hrac nebude moct rozlisovat podle "popisku", o jakou
    bytost se jedna, i kdyby to mel byt vyznamny obchodnik nebo tak..

    ** 27-07-2011:
    Odstaveno, zameneno za staticky nazev "Bytost" nebo " " (prazdne)
*/
// -----------------------------------------------------------------------------



//#include    "vg_s0_sys_core_v"
//#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    SetName(OBJECT_SELF, "</c>");

    string  sNobody =   (GetGender(OBJECT_SELF) ==  GENDER_MALE)    ?   "po_nobod02_"    :   "po_nobod01_";

    SetPortraitResRef(OBJECT_SELF, sNobody);

    /*
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int     nRace   =   GetRacialType(OBJECT_SELF);
    string  sName   =   GetStringByStrRef(stoi(Get2DAString("racialtypes", "Name", nRace)));

    if  (GetIsPlayableRacialType(OBJECT_SELF))
    {
        int     nGender =   GetGender(OBJECT_SELF);
        string  sGender =   (nGender    ==  GENDER_MALE)    ?   " (Mu�)"    :
                            (nGender    ==  GENDER_FEMALE)  ?   " (�ena)"   :   "";

        sName   +=  sGender;
    }

    SetName(OBJECT_SELF, sName);
    */
}
