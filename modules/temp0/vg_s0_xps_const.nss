// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Constants                              |
// | File    || vg_s0_xps_const.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 26-12-2007                                                     |
// | Updated || 31-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu XPS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_XPS_NAME                     =   "Experience System";
const string    SYSTEM_XPS_PREFIX                   =   "XPS";
const string    SYSTEM_XPS_VERSION                  =   "2.00";
const string    SYSTEM_XPS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_XPS_DATE                     =   "26-12-2007";
const string    SYSTEM_XPS_UPDATE                   =   "31-05-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



/*
const string    XPS_DB_TABLE_XP                     =   "xps_xp";
const string    XPS_DB_COLUMN_CHARACTER_ID          =   "id_char";
const string    XPS_DB_COLUMN_STRING                =   "value";
*/

const int       XPS_B_ENABLE_ABSORB_MODE            =   FALSE;
const int       XPS_B_ENABLE_NORMAL_XP_NOTIFICATION =   FALSE;

const int       XPS_M_RPX_HEARTBEATS                =   12;
const int       XPS_M_ABSORB_HEARTBEATS             =   3;
const int       XPS_M_ABSORB_SEQUENCES              =   2;
const int       XPS_M_ABSORB_VALUE_BASE             =   500;
const int       XPS_M_ABSORB_VALUE_PER_LEVEL        =   100;
const int       XPS_M_DELIMITER                     =   7;
const int       XPS_M_XP_BASE                       =   10;
const int       XPS_M_XP_COUNT                      =   3;
const int       XPS_M_XP_EDGE                       =   5000;
const int       XPS_M_XP_LEVELS                     =   50;
const int       XPS_M_XP_ABSORB_SEQUENCE_MULTIPLIER =   20;
const int       XPS_M_XP_LEARN_SCHOOL_MULTIPLIER    =   6;
const float     XPS_M_XP_INCREMENT                  =   1.075f;
const float     XPS_M_XP_KOEFICIENT                 =   1.04f;
//const int       XPS_M_AFK_ROUNDS                    =   12;
//const float     XPS_M_AFK_ROUND_DURATION            =   5.0f;
const float     XPS_M_ROUND_DURATION                =   5.5f;

//const string    XPS_NCS_ALTER_XP_CONDITION          =   "vg_s1_xps_xpcond";

//  Tridy zkusenosti
//  XPS
const int       XPS_M_CLASS_XPS_PLAYER_CHARACTER    =   0;
/*
//  SCS
const int       XPS_M_CLASS_SCS_ABJURATION          =   1;
const int       XPS_M_CLASS_SCS_CONJURATION         =   2;
const int       XPS_M_CLASS_SCS_DIVINATION          =   3;
const int       XPS_M_CLASS_SCS_ENCHANTMENT         =   4;
const int       XPS_M_CLASS_SCS_EVOCATION           =   5;
const int       XPS_M_CLASS_SCS_ILLUSION            =   6;
const int       XPS_M_CLASS_SCS_NECROMANCY          =   7;
const int       XPS_M_CLASS_SCS_TRANSMUTATION       =   8;

const int       XPS_M_CLASS_SCS_FIRST               =   XPS_M_CLASS_SCS_ABJURATION;
const int       XPS_M_CLASS_SCS_LAST                =   XPS_M_CLASS_SCS_TRANSMUTATION;

//  BCS
const int       XPS_M_CLASS_BCS_UNARMED             =   9;
const int       XPS_M_CLASS_BCS_SHORT_BLADED        =   10;
const int       XPS_M_CLASS_BCS_LONG_BLADED         =   11;
const int       XPS_M_CLASS_BCS_POLEARM             =   12;
const int       XPS_M_CLASS_BCS_WHIP                =   13;
const int       XPS_M_CLASS_BCS_MACE                =   14;
const int       XPS_M_CLASS_BCS_DOUBLE_SIDED        =   15;
const int       XPS_M_CLASS_BCS_AXE                 =   16;
const int       XPS_M_CLASS_BCS_RANGED              =   17;
const int       XPS_M_CLASS_BCS_FLAIL               =   18;
const int       XPS_M_CLASS_BCS_THROWING            =   19;

const int       XPS_M_CLASS_BCS_FIRST               =   XPS_M_CLASS_BCS_UNARMED;
const int       XPS_M_CLASS_BCS_LAST                =   XPS_M_CLASS_BCS_THROWING;
*/
const int       XPS_M_CLASS_LAST                    =   XPS_M_CLASS_XPS_PLAYER_CHARACTER;

