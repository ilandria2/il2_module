// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_cast.nss                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "CAST"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_scs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "CAST";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string      sSpell  =   CCS_GetConvertedString(sLine, 1, FALSE);
    string      sMeta   =   CCS_GetConvertedString(sLine, 2, FALSE);
    int         bInst   =   CCS_GetConvertedInteger(sLine, 3, FALSE);
    location    lTarget =   CCS_GetConvertedLocation(sLine, 4, FALSE);
    object      oTarget =   CCS_GetConvertedObject(sLine, 4, FALSE);
    int         nSpell  =   -1;
    string      sMessage;

    if  (TestStringAgainstPattern("*n", sSpell))
    {
        nSpell  =   stoi(sSpell);

        if  (nSpell >=  SCS_FIRST_CONJURE_ABILITY)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "Neplatn� ID kouzla [" + R2 + sSpell + W2 + "]";

            msg(oPC, sMessage, TRUE);
            return;
        }
    }

    else
    {
        int     nRow    =   0;
        string  s2da;

        while   (nRow   <   SCS_FIRST_CONJURE_ABILITY)
        {
            s2da    =   Get2DAString("spells", "Label", nRow);

            if  (s2da   !=  "")
            {
                if  (TestStringAgainstPattern(sSpell, s2da))
                {
                    nSpell  =   nRow;
                    break;
                }
            }

            nRow++;
        }
    }
    /*
    if  (nSpell !=  -1)
    {
        if  (Get2DAString("spells", "Label", nSpell + SCS_FIRST_CONJURE_SPELL)  ==  "")
        {
            nSpell  =   -1;
        }
    }*/

    if  (nSpell ==  -1)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Nepoda�ilo se naj�t ��dn� kouzlo podle patternu " + R2 + sSpell;

        msg(oPC, sMessage, TRUE);
        return;
    }

    int nMeta   =   METAMAGIC_NONE;
    string  sMetaUsed;

    sMeta   =   (sMeta  ==  "") ?   "N" :   GetStringUpperCase(sMeta);

    if  (FindSubString(sMeta, "E")  !=  -1)
    {
        if  (SCS_GetSpellHasMetamagicType(METAMAGIC_EMPOWER, nSpell, TRUE))
        {
            nMeta       =   METAMAGIC_EMPOWER;
            sMetaUsed   =   "E";
        }
    }

    else

    if  (FindSubString(sMeta, "X")  !=  -1)
    {
        if  (SCS_GetSpellHasMetamagicType(METAMAGIC_EXTEND, nSpell, TRUE))
        {
            nMeta       =   METAMAGIC_EXTEND;
            sMetaUsed   =   "X";
        }
    }

    else

    if  (FindSubString(sMeta, "M")  !=  -1)
    {
        if  (SCS_GetSpellHasMetamagicType(METAMAGIC_MAXIMIZE, nSpell, TRUE))
        {
            nMeta       =   METAMAGIC_MAXIMIZE;
            sMetaUsed   =   "M";
        }
    }

    else
    {
        nMeta       =   METAMAGIC_NONE;
        sMetaUsed   =   "N";
    }

    if  (FindSubString(sMeta, "Q")  !=  -1)
    {
        if  (SCS_GetSpellHasMetamagicType(METAMAGIC_QUICKEN, nSpell, TRUE))
        {
            nMeta       +=  METAMAGIC_QUICKEN;
            sMetaUsed   +=  "Q";
        }
    }

    if  (FindSubString(sMeta, "S")  !=  -1)
    {
        if  (SCS_GetSpellHasMetamagicType(METAMAGIC_SILENT, nSpell, TRUE))
        {
            nMeta       +=  METAMAGIC_SILENT;
            sMetaUsed   +=  "S";
        }
    }

    if  (FindSubString(sMeta, "T")  !=  -1)
    {
        if  (SCS_GetSpellHasMetamagicType(METAMAGIC_STILL, nSpell, TRUE))
        {
            nMeta       +=  METAMAGIC_STILL;
            sMetaUsed   +=  "T";
        }
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "Kouzlo [" + Y2 + "#" + itos(nSpell) + " - " + Get2DAString("spells", "Label", nSpell) + W2 + "] Meta [" + Y2 + sMetaUsed + W2 + "]";

    msg(oPC, sMessage, TRUE);

    if  (GetIsObjectValid(oTarget))
    AssignCommand(oPC, SCS_CastSpellAtObject(nSpell, oTarget, nMeta, PROJECTILE_PATH_TYPE_DEFAULT, bInst));

    else
    AssignCommand(oPC, SCS_CastSpellAtLocation(nSpell, lTarget, nMeta, PROJECTILE_PATH_TYPE_DEFAULT, bInst));

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
