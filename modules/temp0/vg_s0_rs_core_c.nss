// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Core C                                        |
// | File    || vg_s0_rs_core_c.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 13-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu RS typu "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_m"
#include    "vg_s0_rs_const"
#include    "vg_s0_pis_core_c"
#include    "vg_s0_sys_core_d"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || RS_GetBedrollValue                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati cislo udavajici kvalitu dostupneho a vhodneho spaciho predmetu,
//  ktery hrac oPC pri odpocinku pouziva
//
// -----------------------------------------------------------------------------
int     RS_GetBedrollValue(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || RS_Rest                                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Bytost oCreature zacne odpocivat
//
// -----------------------------------------------------------------------------
void    RS_Rest(object oCreature);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                           RS_GetBedrollValue                              |
// +---------------------------------------------------------------------------+



int     RS_GetBedrollValue(object oPC)
{
    if  (!GetIsObjectValid(oPC))    return  -1;

    int i   =   1;

    object  oTemp   =   GetNearestObject(OBJECT_TYPE_PLACEABLE, oPC, i);

    while   (GetIsObjectValid(oTemp))
    {
        //  zadny bedroll v dosahu
        if  (GetDistanceBetween(oPC, oTemp) >   1.0f)   return  0;

        int nBedroll    =   gli(oTemp, SYSTEM_RS_PREFIX + "_BEDROLL");

        if  (nBedroll   !=  0)  return  nBedroll;

        oTemp   =   GetNearestObject(OBJECT_TYPE_PLACEABLE, oPC, ++i);
    }

    return  0;
}



// +---------------------------------------------------------------------------+
// |                                RS_Rest                                    |
// +---------------------------------------------------------------------------+



void    RS_LoopInterruptCheck(object oPC = OBJECT_SELF, object oHostileDef = OBJECT_INVALID)
{
    int bCheck  =   gli(oPC, SYSTEM_RS_PREFIX + "_INTER_CHECK");

    if  (!bCheck)    return;

    object  oHostile    =   GetLastHostileActor(oPC);

    if  (oHostile   !=  oHostileDef)
    {
        //RS_Rest(oPC, RS_REST_STATE_INVALID);
        return;
    }

    DelayCommand(3.0f, RS_LoopInterruptCheck(oPC, oHostileDef));
}



void    RS_RunInterruptCheck(object oPC = OBJECT_SELF)
{
    int bCheck  =   gli(oPC, SYSTEM_RS_PREFIX + "_INTER_CHECK");

    if  (bCheck)    return;

    sli(oPC, SYSTEM_RS_PREFIX + "_INTER_CHECK", TRUE);

    //  musi se "resetnout" LastHostileActor u postavy hrace aby to spravne
    //  fungovalo
    SetPlotFlag(oPC, TRUE);
    GetLastHostileActor();
    SetPlotFlag(oPC, FALSE);
    RS_LoopInterruptCheck(oPC, GetLastHostileActor(oPC));
}



void    RS_RestLoop(object oCreature)
{
    if  (!GetIsObjectValid(oCreature))  return;

    location    lRest   =   gll(oCreature, SYSTEM_RS_PREFIX + "_RESTLOC");
    location    lCur    =   GetLocation(oCreature);

    //  preruseni odpocinku
    if  (GetIsDead(oCreature)
    ||  GetCurrentHitPoints(oCreature)  <   1
    ||  GetCurrentAction(oCreature) !=  ACTION_INVALID
    ||  lCur  !=  lRest)
    {
        SYS_Info(oCreature, SYSTEM_RS_PREFIX, "REST_CANCELLED");
        sli(oCreature, SYSTEM_RS_PREFIX + "_RESTING", -1, -1, SYS_M_DELETE);
        return;
    }

    int nLoop;

    nLoop   =   gli(oCreature, SYSTEM_RS_PREFIX + "_RESTSEQ");
    nLoop   +=  1;

    //  dokonceni odpocinku
    if  (nLoop  >=  RS_REST_SEQUENCES)
    {
        int nFatigue    =   ftoi(RS_FATIGUE_EDGE_FSLEEP * 0.9);

        if  (!gli(oCreature, SYSTEM_RS_PREFIX + "_FSLEEP", -1, SYS_M_DELETE))
        {
            nFatigue    =   0;

            ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(oCreature) / 100 * RS_ON_RESTED_HP_HEALED_PERCENT), oCreature);
        }

        SYS_Info(oCreature, SYSTEM_RS_PREFIX, "WAKED_UP");
        sli(oCreature, SYSTEM_RS_PREFIX + "_RESTSEQ", -1, -1, SYS_M_DELETE);
        sli(oCreature, SYSTEM_RS_PREFIX + "_RESTING", -1, -1, SYS_M_DELETE);
        sli(oCreature, SYSTEM_RS_PREFIX + "_FATIGUE", nFatigue);
        SavePersistentData(oCreature, SYSTEM_RS_PREFIX);
        PIS_RefreshIndicator(oCreature, OBJECT_INVALID, FALSE);
        AssignCommand(oCreature, ActionPlayAnimation(0, 1.0f, 0.0f));
        AssignCommand(oCreature, ClearAllActions(TRUE));
        return;
    }

    sli(oCreature, SYSTEM_RS_PREFIX + "_RESTSEQ", nLoop);
    DelayCommand(5.0f, RS_RestLoop(oCreature));
}



void    RS_RestLoopStart(object oCreature)
{
    int bResting    =   gli(oCreature, SYSTEM_RS_PREFIX + "_RESTING");

    if  (bResting)  return;

    sli(oCreature, SYSTEM_RS_PREFIX + "_RESTING", TRUE);
    sll(oCreature, SYSTEM_RS_PREFIX + "_RESTLOC", GetLocation(oCreature));

    DelayCommand(5.0f, RS_RestLoop(oCreature));
}



void    RS_Rest(object oCreature)
{
    if  (!GetIsObjectValid(oCreature))                          return;
    if  (GetObjectType(oCreature)   !=  OBJECT_TYPE_CREATURE)   return;
    if  (GetIsDead(oCreature))                                  return;

    int nRest   =   gli(oCreature, SYSTEM_RS_PREFIX + "_RESTING");

    SYS_Info(oCreature, SYSTEM_RS_PREFIX, "RESTING");
    AssignCommand(oCreature, ClearAllActions(TRUE));
    AssignCommand(oCreature, ActionUnequipItem(GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oCreature)));
    AssignCommand(oCreature, ActionUnequipItem(GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oCreature)));
    AssignCommand(oCreature, ActionPlayAnimation(RS_ANIMATION_REST_SLEEP, 0.5f, 9999999.0f));
    DelayCommand(0.9f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_SLEEP), oCreature));
    RS_RestLoopStart(oCreature);
}

