// executed when player sells an item

#include "vg_s0_ets_const"
#include "vg_s0_ets_core"

void main()
{
    object item = GetModuleItemLost();
    object store = GetItemPossessor(item);

    if (GetObjectType(store) != OBJECT_TYPE_STORE) return;
    if (gli(GetModule(), "ets"))SpawnScriptDebugger();

    ETS_InteractWithStoreItem("SELL", GetModuleItemLostBy(), store, item);
}
