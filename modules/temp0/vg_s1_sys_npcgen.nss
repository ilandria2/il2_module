// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Generate commoner script                |
// | File    || vg_s1_sys_npcgen.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 09-09-2009                                                     |
// | Updated || 03-11-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Generuje obyvatele podle regionu, v kterem se NPC zjevi:
    -   vybavu      [Roztrhane hadry, 1gp, ...]
    -   rasu        [clovek]
    -   pohlavi     [muz]
    -   skupinu     [zobrak]
    -   obleceni    [saty]
    -   zlato       [15gp]
    -   vzhled      [hlava, kuze, vlasy]
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    //xxx
    SetPlotFlag(OBJECT_SELF, FALSE);
    DestroyObject(OBJECT_SELF, 1.0f);
    return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    string  sRegion =   GetSubString(GetTag(GetArea(OBJECT_SELF)), 6, 2);

    int     nChance, nLast, nDice, nHigh, nPos, nRow, nValue;

//  ----------------------------------------------------------------------------
//  Skupina NPC

    string  sGroup  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_GROUP");

    //  nahodne
    if  (sGroup ==  "")
    {
        nLast   =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_S_ROWS");

        //  nebyli zadefinovany zadne skupiny NPC pro dany region
        if  (!nLast)
        {
            logentry("[" + SYSTEM_SYS_PREFIX + "] NPC generation error: No groups defined for region [" + sRegion + "]", TRUE);
            return;
        }

        nRow    =   0;
        nPos    =   1;

        while   (nRow   <   nLast)
        {
            nChance =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP_CHANCE", ++nRow);
            nDice   =   d100();

            if  (nDice  <=  nChance
            &&  nDice   >   nHigh)
            {
                nHigh   =   nDice;
                nPos    =   nRow;
            }
        }

        sGroup  =   gls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_GROUP", nPos);
    }

//  Skupina NPC
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Rasa

    int nRace   =   GetRacialType(OBJECT_SELF);

    //  nahodne
    if  (!GetIsPlayableRacialType(OBJECT_SELF))
    {
        nLast   =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_RACE_ID_I_ROWS");

        //  nebyli zadefinovany zadne rasy pro dany region
        if  (!nLast)
        {
            logentry("[" + SYSTEM_SYS_PREFIX + "] NPC generation error: No races defined for region [" + sRegion + "] of group [" + sGroup + "]", TRUE);
            return;
        }

        nPos    =   1;
        nRow    =   0;
        nHigh   =   0;

        while   (nRow   <   nLast)
        {
            nChance =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_RACE_CHANCE", ++nRow);
            nDice   =   d100();

            if  (nDice  <=  nChance
            &&  nDice   >   nHigh)
            {
                nHigh   =   nDice;
                nPos    =   nRow;
            }
        }

        nRace   =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_RACE_ID", nPos);
    }

    string  sRace   =   gls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_2DA_NAME", nRace);

//  Rasa
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Pohlavi

    nValue  =   GetGender(OBJECT_SELF);

    string  sGender;

    //  nahodne
    if  (nValue !=  GENDER_MALE
    &&  nValue  !=  GENDER_FEMALE)
    {
        nChance =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_GENDER_CHANCE");
        nDice   =   d100();
        sGender =   (nDice  <=  nChance)    ?   "F" :   "M";
    }

    else
    {
        sGender =   (nValue ==  GENDER_FEMALE)  ?   "F" :   "M";
    }

//  Pohlavi
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Vytvoreni bytosti podla rasy / pohlavi

    nRow    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_" + sGender + itos(nRace) + "_COUNT");

    //  nebyl zadefinovany pocet zdroju NPC
    if  (!nRow)
    {
        logentry("[" + SYSTEM_SYS_PREFIX + "] NPC generation error: No sources defined for region [" + sRegion + "] gender [" + sGender + "] race [" + itos(nRace) + "]", TRUE);
        return;
    }

    nPos    =   1 + Random(nRow);

    string  sPos    =   itos(nPos);

    sPos    =   (nPos   <   10) ?   "0" + sPos  :   sPos;

    string  sResRef =   gls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_NPC_RACE_" + itos(nRace) + "_PART") + sGender + sPos;
    object  oNPC    =   CreateObject(OBJECT_TYPE_CREATURE, sResRef, GetLocation(OBJECT_SELF));

    SetPlotFlag(OBJECT_SELF, FALSE);
    DestroyObject(OBJECT_SELF, 1.0f);

//  Vytvoreni bytosti podla rasy / pohlavi
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Phenotype

    nChance =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_PHENOTYPE_CHANCE");
    nDice   =   d100();

    int nPhenotype  =   (nDice  <=  nChance)    ?   PHENOTYPE_BIG   :   PHENOTYPE_NORMAL;

    SetPhenoType(nPhenotype, oNPC);

//  Phenotype
//  ----------------------------------------------------------------------------

    string  s2da;

//  ----------------------------------------------------------------------------
//  Vzhled - Hlava

    nRow    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(nRace) + "_" + sGender + "_HEAD");
    nPos    =   Random(nRow);
    s2da    =   Get2DAString("sys_app_head", sRace + "_" + sGender, nPos);

    SetCreatureBodyPart(CREATURE_PART_HEAD, stoi(s2da), oNPC);

//  Vzhled - Hlava
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Vzhled - Barva tela

    nRow    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(nRace) + "_SKIN");
    nPos    =   Random(nRow);
    s2da    =   Get2DAString("sys_app_skin", sRace, nPos);

    SetColor(oNPC, COLOR_CHANNEL_SKIN, stoi(s2da));

//  Vzhled - Barva tela
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Vzhled - Barva vlasu

    nRow    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_RACE_" + itos(nRace) + "_HAIR");
    nPos    =   Random(nRow);
    s2da    =   Get2DAString("sys_app_hair", sRace, nPos);

    SetColor(oNPC, COLOR_CHANNEL_HAIR, stoi(s2da));

//  Vzhled - Barva vlasu
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Inventar - Obleceni

    nRow    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_" + sGender + "_CLOTHES_S_ROWS");
    nPos    =   1 + Random(nRow);

    string  sClothes    =   gls(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_" + sGender + "_CLOTHES", nPos);
    object  oClothes    =   CreateItemOnObject(sClothes, oNPC, 1);

    SetIdentified(oClothes, TRUE);
    SetDroppableFlag(oClothes, FALSE);
    SetPickpocketableFlag(oClothes, FALSE);
    AssignCommand(oNPC, ClearAllActions());
    DelayCommand(0.15f, AssignCommand(oNPC, ActionEquipItem(oClothes, INVENTORY_SLOT_CHEST)));

//  Inventar - Obleceni
//  ----------------------------------------------------------------------------

//  ----------------------------------------------------------------------------
//  Inventar - Zlato

    int nGoldMin    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_GOLD_MIN");
    int nGoldMax    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_REGION_" + sRegion + "_" + sGroup + "_GOLD_MAX");

    nValue  =   nGoldMin + Random(1 + nGoldMax - nGoldMin);

    GiveGoldToCreature(oNPC, nValue);

//  Inventar - Zlato
//  ----------------------------------------------------------------------------
}
