#include "vg_s0_sys_core_t"

void main()
{
    object item = GetModuleItemLost();
    object player = GetModuleItemLostBy();

    msg(player, "lost item: " + GetTag(item) + " new owner: " + GetTag(GetItemPossessor(item)));
}
