// +---------++----------------------------------------------------------------+
// | Name    || Generic User Interface (GUI) Constants                         |
// | File    || vg_s0_gui_const.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+

// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_GUI_NAME                 =   "Generic User Interface";
const string    SYSTEM_GUI_PREFIX               =   "GUI";
const string    SYSTEM_GUI_VERSION              =   "1.0";
const string    SYSTEM_GUI_AUTHOR               =   "VirgiL";
const string    SYSTEM_GUI_DATE                 =   "2017-02-22";
const string    SYSTEM_GUI_UPDATE               =   "2017-02-22";
string          GUI                             =   SYSTEM_GUI_PREFIX;
string          GUI_                            =   GUI + "_";
string          _GUI                            =   "_" + GUI;
string          _GUI_                           =   "_" + GUI + "_";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const float     GUI_CAMERA_ANGLE                = 70.0f;
//const float     GUI_CAMERA_HEIGHT               = 5.0f;
const float     GUI_CAMERA_HEIGHT               = 0.0f;
const float     GUI_CAMERA_DISTANCE             = 10.0f;
const float     GUI_CAMERA_DIRECTION            = DIRECTION_NORTH;
//const float     GUI_HEIGHT                      = 5.5f;
const float     GUI_HEIGHT                      = 2.5f;
const float     GUI_LIFETIME                    = 3.001f;
const float     GUI_LAYER_THICKNESS             = 0.005f;
const float     GUI_LAYOUT_Z_OFFSET             = -1.2f;
const string    GUI_OBJECT_RESREF               = "vg_w_s1_guiobj";

const int GUI_LAYOUT_WINDOW                     = 1; // generic layout id for main window
const int GUI_LAYOUT_CLOSE_BUTTON               = 2; // generic layout id for close button

// GUI objects of type INTERACT
const int GUI_TYPE_INTERACT                     = 1;
const int GUI_LAYOUT_INTERACT_SCENARIO          = 3;
const int GUI_LAYOUT_INTERACT_TITLE             = 4;
const int GUI_LAYOUT_INTERACT_GRID              = 3;
const int GUI_LAYOUT_INTERACT_GRID_OBJECTS      = 7;

// GUI layouts of type <GUI_TYPE>
//const int GUI_TYPE_<GUI_TYPE>                   = 2;
//const int GUI_LAYOUT_<GUI_TYPE>_<LAYOUT1>       = 1;
//const int GUI_LAYOUT_<GUI_TYPE>_<LAYOUT2>       = 2;

const int GUI_VFX_EMPTY                         = -1;
const int GUI_VFX_MISSING                       = 0;
const int GUI_VFX_CLOSE_BUTTON                  = 8199;
const int GUI_VFX_INTERACT_WINDOW               = 8203;
const int GUI_VFX_INTERACT_SC_NOTHING_FOUND     = 8196;
const int GUI_VFX_INTERACT_SC_SELECT_OBJECT     = 8197;
const int GUI_VFX_INTERACT_TITLE_SELECT_OBJECT  = 8200;
const int GUI_VFX_INTERACT_TITLE_NOTHING_FOUND  = 8201;
const int GUI_VFX_INTERACT_TITLE_SELECT_ACTION  = 8204;
const int GUI_VFX_INTERACT_RADIAL_MENU          = 8205;

// system functions
object __gui;
object __GUI();
object __GUI()
{
    if (!GetIsObjectValid(__gui))
        __gui = GetObjectByTag("SYSTEM" + _GUI_ + "OBJECT");

    return __gui;
}

