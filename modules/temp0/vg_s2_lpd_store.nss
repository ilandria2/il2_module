// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialogs (LPD) Open nearest store script          |
// | File    || vg_s2_lpd_store.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Otevre nejblizsi obchod u NPC
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   LPD_GetPC();
    object  oStore  =   GetNearestObject(OBJECT_TYPE_STORE);

    if  (GetDistanceToObject(oStore)    >   3.0f)   return;

    OpenStore(oStore, oPC);
}

