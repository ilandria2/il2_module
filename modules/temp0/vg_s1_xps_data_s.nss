// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Persistent data save script            |
// | File    || vg_s1_xps_data_s.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-06-2009                                                     |
// | Updated || 13-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_xps_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nClass  =   0;
    int     nXP     =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nClass) + "_XP");
    int     nAbsorb =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nClass) + "_ABSORB");
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sQuerry, sXPS;

    while   (nClass <=  XPS_M_CLASS_LAST)
    {
        nClass++;
        sXPS    +=  itos(nXP) + "~" + itos(nAbsorb) + "~";
        nXP     =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nClass) + "_XP");
        nAbsorb =   gli(OBJECT_SELF, SYSTEM_XPS_PREFIX + "_CLASS_" + itos(nClass) + "_ABSORB");
    }

    sQuerry =   "INSERT INTO " +
                    "xps_xp " +
                "VALUES " +
                "('" +
                    itos(nID) + "','" +
                    sXPS + "'," +
                    "NULL" +
                ") ON DUPLICATE KEY UPDATE " +
                    "XPS = '" + sXPS + "'," +
                    "date = NULL";

    if  (sQuerry    !=  "") SQLExecDirect(sQuerry);
}
