// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) Persistent data check script           |
// | File    || vg_s1_xps_data_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 13-06-2009                                                     |
// | Updated || 13-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_xps_const"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;
    int     nTmp;

    sLog    =   "[" + SYSTEM_XPS_PREFIX + "] Checking table [xps_xp]";
    sQuerry =   "DESCRIBE xps_xp";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_XPS_PREFIX + "] Table [xps_xp] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_XPS_PREFIX + "] Table [xps_xp] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE xps_xp " +
        "(" +
            "PlayerID INT(6) NOT NULL PRIMARY KEY," +
            "XPS VARCHAR(900) NOT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE xps_xp";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_XPS_PREFIX + "] Table [xps_xp] created successfully"; else
        sLog    =   "[" + SYSTEM_XPS_PREFIX + "] Table [xps_xp] error in creation";

        logentry(sLog);
    }
}
