// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Core T                                  |
// | File    || vg_s0_sys_core_t.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 09-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Knihovna systemovych funkci Ilandrie 2 kategorie "Text"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_const"
#include    "vg_s0_sys_colors"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || itos                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "IntToString"
//
// -----------------------------------------------------------------------------
string  itos(int nInt);



// +---------++----------------------------------------------------------------+
// | Name    || itof                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "IntToFloat"
//
// -----------------------------------------------------------------------------
float   itof(int nInt);



// +---------++----------------------------------------------------------------+
// | Name    || ftos                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "FloatToString"
//
// -----------------------------------------------------------------------------
string  ftos(float fFloat);



// +---------++----------------------------------------------------------------+
// | Name    || ftoi                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "FloatToInt"
//
// -----------------------------------------------------------------------------
int     ftoi(float fFloat);



// +---------++----------------------------------------------------------------+
// | Name    || stof                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "StringToFloat"
//
// -----------------------------------------------------------------------------
float   stof(string sString);



// +---------++----------------------------------------------------------------+
// | Name    || stoi                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "StringToInt"
//
// -----------------------------------------------------------------------------
int     stoi(string sString);



// +---------++----------------------------------------------------------------+
// | Name    || itoh                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Skracena verze funkce "IntToHexString"
//
// -----------------------------------------------------------------------------
string  itoh(int nInt);



// +---------++----------------------------------------------------------------+
// | Name    || hctoi                                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Konvertuje hexadecimalni znak (0-9, A-F) na decimalni (0-15)
//
// -----------------------------------------------------------------------------
int     hctoi(string sHexChar);



// +---------++----------------------------------------------------------------+
// | Name    || htoi                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Konvertuje hexadecimalni retezec sHex na decimalni cislo
//
// -----------------------------------------------------------------------------
int     htoi(string sHex);



// +---------++----------------------------------------------------------------+
// | Name    || __dbgmsg                                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Universal function for sending debug messages
//
// -----------------------------------------------------------------------------
void __dbgmsg(int bAlsoLog, object oPC, string sMessage);



// +---------++----------------------------------------------------------------+
// | Name    || msg                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce pro posilani serverovych sprav (ne talk!)
//
// -----------------------------------------------------------------------------
void    msg(object oTarget, string sMessage, int bFloat = FALSE, int bFaction = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || log                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce pro zapisovani logu
//
// -----------------------------------------------------------------------------
void    logentry(string sLog, int bTimeStamp = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || FindNthSubString                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati poziciu n-teho substringu v textovem retezci sString
//
// -----------------------------------------------------------------------------
int     FindNthSubString(string sString, string sSubString, int nNth = 1);



// +---------++----------------------------------------------------------------+
// | Name    || GetNthSubString                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati n-ty substring oddeleni znakem sSeparator
//
// -----------------------------------------------------------------------------
string  GetNthSubString(string sString, int nNth = 1, string sSeparator = "&");



// +---------++----------------------------------------------------------------+
// | Name    || GetCharsCount                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati pocet vyskytu znaku sChar v textovym retezci sString
//
// -----------------------------------------------------------------------------
int     GetCharsCount(string sChar, string sString);



// +---------++----------------------------------------------------------------+
// | Name    || GetParity                                                      |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE, pokud cislo nNumber je parne
//
// -----------------------------------------------------------------------------
int     GetParity(int nNumber);



// +---------++----------------------------------------------------------------+
// | Name    || FindAndReplace                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  V textove retezci sString vyhleda sSubString a stridave ho nahradi textovym
//  retezcem
//
// -----------------------------------------------------------------------------
string  FindAndReplace(string sString, string sSubString, string sNewSubString, string sAltSubString = "", int bAll=TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || ConvertRGBTokens                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  V texte sString nahradi tokeny <StartRGBrrrgggbbb>
//
// -----------------------------------------------------------------------------
string  ConvertRGBTokens(string sString);



// +---------++----------------------------------------------------------------+
// | Name    || ConvertTokens                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nahradi tokeny patricnyma hodnotama (podobne jako v dialogu)
//
// -----------------------------------------------------------------------------
string  ConvertTokens(string sText, object oCreature, int bHighlight = TRUE, int bNorm = TRUE, int bCustom = TRUE, int bRGB = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || ConvertPattern                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Upravi patterny ve vice user-friendly formate
//
// -----------------------------------------------------------------------------
string  ConvertPattern(string sText, int bClear = FALSE, string sColorP = C_NORM, string sColorT = C_BOLD);



// +---------++----------------------------------------------------------------+
// | Name    || GetDiagram                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati textovy retezec ve forme diagramu vytvoren pomerem hodnot nValue1/2
//  nFrom (0+) z teto hodnoty se zmeni na nValue1 (> zelena, < cervena, = seda)
//        (-1) barva bude specifikovana pomoci promennych rgb(r,g,b)
//        (-2) Pouziti sede jako zaklad (nepouzije se uz rgb - chovani jako 0+)
//
//  *** Priklad:
//  [||||||----] Diagram (60%)
//
// -----------------------------------------------------------------------------
string  GetDiagram(int nValue1, int nValue2, int nFrom = -2, int bInverted = FALSE, int r = 255, int g = 255, int b = 255);



// +---------++----------------------------------------------------------------+
// | Name    || GetDefinedEffect                                               |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati definovany efekt v sDefinition
//
// -----------------------------------------------------------------------------
effect  GetDefinedEffect(string sDefinition);



// +---------++----------------------------------------------------------------+
// | Name    || rgb                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati barevnej token definovany pomoci 'r', 'g' a 'b'
//
// -----------------------------------------------------------------------------
string  rgb(int r = 255, int g = 255, int b = 255);



// +---------++----------------------------------------------------------------+
// | Name    || left                                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati nCount znaku z leveho konce textovyho retezce sString
//
// -----------------------------------------------------------------------------
string  left(string sString, int nCount);




// +---------++----------------------------------------------------------------+
// | Name    || right                                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati nCount znaku z praveho konce textovyho retezce sString
//
// -----------------------------------------------------------------------------
string  right(string sString, int nCount);



// +---------++----------------------------------------------------------------+
// | Name    || upper                                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkracena verze funkce GetStringUpperCase()
//
// -----------------------------------------------------------------------------
string  upper(string sString);



// +---------++----------------------------------------------------------------+
// | Name    || len                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkracena verze funkce GetStringLength()
//
// -----------------------------------------------------------------------------
int     len(string sString);



// +---------++----------------------------------------------------------------+
// | Name    || sub                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkracena verze funkce GetSubString()
//
// -----------------------------------------------------------------------------
string  sub(string sString, int nStart, int nCount);



// +---------++----------------------------------------------------------------+
// | Name    || lower                                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkracena verze funkce GetStringLowerCase()
//
// -----------------------------------------------------------------------------
string  lower(string sString);



// +---------++----------------------------------------------------------------+
// | Name    || speak                                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Rozsirena funkce SpeakString s kanalama sType:
//  TALK    - normalni mluva
//  SHOUT   - krik do 90 ft
//  ASHOUT  - krik do cele oblasti
//  MSHOUT  - krik do celyho modulu
//  WHISPER - septani
//  PARTY   - druzina
//
// -----------------------------------------------------------------------------
void    speak(string sMessage, string sType = "TALK", int nVolume = -1);



// +---------++----------------------------------------------------------------+
// | Name    || ConvertCustomTokens                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Konvertuje specialni tokeny <CUSTOMnnn> u objektu oObject
//
// -----------------------------------------------------------------------------
string  ConvertCustomTokens(string sText, object oObject = OBJECT_INVALID);



// +---------++----------------------------------------------------------------+
// | Name    || ConvertText                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Odstrani diakritiku z textu sText
//
// -----------------------------------------------------------------------------
string  ConvertText(string sText);



// +---------++----------------------------------------------------------------+
// | Name    || FindAndReplacePart                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Najde v textu sString text mezi 2mi textami sStartSub a sEndSub a nahradi
//  tuto cast s sNewSub
//
// -----------------------------------------------------------------------------
string  FindAndReplacePart(string sString, string sStartSub, string sEndSub, string sNewSub = "", int bAll = TRUE);



// +---------++----------------------------------------------------------------+
// | Name    || GetItemPropertyName                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati jmeno vlastnosti predmetu ipProp
//
// -----------------------------------------------------------------------------
string  GetItemPropertyName(itemproperty ipProp);



// +---------------------------------------------------------------------------+
// |                        I M P L E M E N T A T I O N                        |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                             Convert Functions                             |
// +---------------------------------------------------------------------------+



string  itos(int nInt){return   IntToString(nInt);}
float   itof(int nInt){return   IntToFloat(nInt);}
string  ftos(float fFloat){return   FloatToString(fFloat);}
int     ftoi(float fFloat){return   FloatToInt(fFloat);}
float   stof(string sString){return StringToFloat(sString);}
int     stoi(string sString){return StringToInt(sString);}
string  itoh(int nInt){return   IntToHexString(nInt);}
int     len(string sString){return  GetStringLength(sString);}
string  left(string sString, int nCount){return     GetStringLeft(sString, nCount);}
string  right(string sString, int nCount){return    GetStringRight(sString, nCount);}
string  upper(string sString){return    GetStringUpperCase(sString);}
string  lower(string sString){return    GetStringLowerCase(sString);}
string  sub(string sString, int nStart, int nCount){return  GetSubString(sString, nStart, nCount);}



// +---------------------------------------------------------------------------+
// |                                  hctoi                                    |
// +---------------------------------------------------------------------------+



int     hctoi(string sHexChar)
{
    int     nResult;
    string  sLookUp;

    sHexChar    =   GetStringUpperCase(sHexChar);
    sLookUp     =   "0123456789ABCDEF";
    nResult     =   FindSubString(sLookUp, sHexChar);

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                   htoi                                    |
// +---------------------------------------------------------------------------+



int     htoi(string sHex)
{
    int     nResult, i, nInt;
    string  sLeft, sRight;

    i       =   1;
    sRight  =   GetStringRight(sHex, i);
    sLeft   =   GetStringUpperCase(GetStringLeft(sRight, 1));

    while
    (
        sLeft   !=  "X"
    &&  sLeft   !=  ""
    )
    {
        nInt    =   hctoi(sLeft);
        nResult +=  (nInt * FloatToInt(pow(16.0f, IntToFloat(i - 1))));

        i++;
        sRight  =   GetStringRight(sHex, i);
        sLeft   =   GetStringUpperCase(GetStringLeft(sRight, 1));
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                 __dbgmsg                                    |
// +---------------------------------------------------------------------------+



void __dbgmsg(int bAlsoLog, object oPC, string sMessage)
{
    if (!GetIsObjectValid(oPC)) return;

    if (GetIsPC(oPC))
        msg(oPC, sMessage, FALSE, FALSE);

    if (bAlsoLog)
        logentry("DEBUG> " + sMessage, TRUE);
}



// +---------------------------------------------------------------------------+
// |                                   msg                                     |
// +---------------------------------------------------------------------------+



void    msg(object oTarget, string sMessage, int bFloat = FALSE, int bFaction = TRUE)
{
    if  (bFloat)    FloatingTextStringOnCreature(sMessage, oTarget, bFaction);
    else            SendMessageToPC(oTarget, sMessage);
}



// +---------------------------------------------------------------------------+
// |                                   log                                     |
// +---------------------------------------------------------------------------+



void    logentry(string sLog, int bTimeStamp = TRUE)
{
    if  (bTimeStamp)    WriteTimestampedLogEntry(sLog);
    else                PrintString(sLog);
}



// +---------------------------------------------------------------------------+
// |                            FindNthSubString                               |
// +---------------------------------------------------------------------------+



int     FindNthSubString(string sString, string sSubString, int nNth = 1)
{
    int     i, nFind, nResult, nSub, nStr, nSum;
    string  sTemp;

    nResult =   -1;

    nFind   =   FindSubString(sString, sSubString);
    nSum    =   nFind;
    nSub    =   GetStringLength(sSubString);
    nStr    =   GetStringLength(sString);
    sTemp   =   sString;
    i       =   1;


    while
    (
        nFind   !=  -1
    &&  i       <   nNth
    )
    {
        i++;
        sTemp   =   GetSubString(sTemp, nFind + nSub, nStr);
        nFind   =   FindSubString(sTemp, sSubString);
        nSum    +=  nFind + nSub;
    }

    if  (i  ==  nNth)
    {
        nResult =   nSum;
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                            GetNthSubString                                |
// +---------------------------------------------------------------------------+



string  GetNthSubString(string sString, int nNth = 1, string sSeparator = "&")
{
    int     a, nFind, nStart, nCount, nChars, bParity;
    string  sResult, sTemp;

    nChars  =   GetCharsCount(sSeparator, sString);
    bParity =   GetParity(nChars);
    nFind   =   FindSubString(sString, sSeparator);

    if
    (
        nNth * 2    >   nChars  ||
        nNth        <=  0       ||
        bParity     ==  FALSE
    )
    {
        return  "";
    }

    nStart  =   FindNthSubString(sString, sSeparator, nNth * 2 - 1) + 1;
    nCount  =   FindNthSubString(sString, sSeparator, nNth * 2) - nStart;
    sResult =   GetSubString(sString, nStart, nCount);

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                              GetCharsCount                                |
// +---------------------------------------------------------------------------+



int     GetCharsCount(string sChar, string sString)
{
    int     i, p, nFind;

    nFind   =   FindSubString(sString, sChar);

    while (nFind    !=  -1)
    {
        i++;
        sString =   GetSubString(sString, nFind + 1, GetStringLength(sString));
        nFind   =   FindSubString(sString, sChar);
    }

    return  i;
}



// +---------------------------------------------------------------------------+
// |                               GetParity                                   |
// +---------------------------------------------------------------------------+



int     GetParity(int nNumber)
{
    string  sPair   =   "02468";
    string  sLast   =   GetStringRight(IntToString(nNumber), 1);
    int     bResult =   FindSubString(sPair, sLast) !=  -1;

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                              FindAndReplace                               |
// +---------------------------------------------------------------------------+



string FindAndReplace(string sString, string sSubString, string sNewSubString, string sAltSubString = "", int bAll=TRUE)
{
    string sReturn      =   sString;

    int     nFFirstPos  =   FindSubString(GetStringUpperCase(sString), GetStringUpperCase(sSubString), 0);
    int     nFNextPos   =   GetStringLength(sSubString) + nFFirstPos;
    int     nLength     =   GetStringLength(sString);
    string  sFLeft      =   GetSubString(sString, 0, nFFirstPos);
    string  sFRight     =   GetSubString(sString, nFNextPos, nLength);

    //  len v pripade, ak sa pouziva sAltSubString
    int     bState      =   FALSE;

    //  kontrola bAll - pre zistenie, ci ma nahradit vsetky retazce novymi
    if (bAll    =   TRUE)
    {
        while (nFFirstPos   !=  -1)
        {
            nFNextPos   -=  (sSubString ==  "/n")   ?   2   :   0;

            if (bState  ==  FALSE)
            {
                sReturn     =   sFLeft + sNewSubString + sFRight;
                nFFirstPos  =   FindSubString(GetStringUpperCase(sReturn), GetStringUpperCase(sSubString), nFNextPos + GetStringLength(sNewSubString) - 1);

                if (sAltSubString != "")
                bState      =   TRUE;
            }

            else
            {
                sReturn     =   sFLeft + sAltSubString + sFRight;
                nFFirstPos  =   FindSubString(GetStringUpperCase(sReturn), GetStringUpperCase(sSubString), nFNextPos + GetStringLength(sAltSubString) - 1);
                bState      =   FALSE;
            }

            nFNextPos   =   GetStringLength(sSubString) + nFFirstPos;
            sFLeft      =   GetSubString(sReturn, 0, nFFirstPos);
            sFRight     =   GetSubString(sReturn, nFNextPos, nLength);
        }
    }

    //  kontrola bAll - nahradi len prvy najdeny retazec novym
    else
    {
        return  sFLeft  +   sFRight;
    }

    return sReturn;
}



// +---------------------------------------------------------------------------+
// |                              ConvertTokens                                |
// +---------------------------------------------------------------------------+



string  ConvertTokens(string sText, object oCreature, int bHighlight = TRUE, int bNorm = TRUE, int bCustom = TRUE, int bRGB = TRUE)
{
    int     nVar1, nVar2;
    string  sResult, sSub, sNewSub, sAltSub;

    //  ------------------------------------------------------------------------
    //  <StartAction>

    sSub    =   "<StartAction>";
    sNewSub =   C_POSI;

    if  (bHighlight)    sText   =   FindAndReplace(sText, sSub, sNewSub, "", TRUE);
    else                sText   =   FindAndReplacePart(sText, sSub, "</Start>", "", TRUE);

    //  <StartAction>
    //  ------------------------------------------------------------------------

    //  ------------------------------------------------------------------------
    //  <StartHighlight>

    sSub    =   "<StartHighlight>";
    sNewSub =   C_BOLD;

    if  (bHighlight)    sText   =   FindAndReplace(sText, sSub, sNewSub, "", TRUE);
    else                sText   =   FindAndReplacePart(sText, sSub, "</Start>", "", TRUE);

    //  <StartHighlight>
    //  ------------------------------------------------------------------------

    //  ------------------------------------------------------------------------
    //  <StartCheck>

    sSub    =   "<StartCheck>";
    sNewSub =   C_NEGA;

    if  (bHighlight)    sText   =   FindAndReplace(sText, sSub, sNewSub, "", TRUE);
    else                sText   =   FindAndReplacePart(sText, sSub, "</Start>", "", TRUE);

    //  <StartCheck>
    //  ------------------------------------------------------------------------

    //  ------------------------------------------------------------------------
    //  <StartInter>

    sSub    =   "<StartInter>";
    sNewSub =   C_INTE;

    if  (bHighlight)    sText   =   FindAndReplace(sText, sSub, sNewSub, "", TRUE);
    else                sText   =   FindAndReplacePart(sText, sSub, "</Start>", "", TRUE);

    //  <StartCheck>
    //  ------------------------------------------------------------------------

    //  ------------------------------------------------------------------------
    //  </Start>

    if  (bHighlight)
    {
        sSub    =   "</Start>";
        sNewSub =   _C;

        sText   =   FindAndReplace(sText, sSub, sNewSub, "", TRUE);
    }

    //  </Start>
    //  ------------------------------------------------------------------------

    //  ------------------------------------------------------------------------
    //  Talk standards, custom/rgb tokens

    sText   =   FindAndReplace(sText, "/n", "\n", "", TRUE);
    sText   =   FindAndReplace(sText, "*", C_INTE + "*", "*" + _C, TRUE);
    sText   =   FindAndReplace(sText, "//", C_NEGA + "//", "//" + _C, TRUE);

    if  (bCustom)   sText   =   ConvertCustomTokens(sText, oCreature);
    if  (bRGB)      sText   =   ConvertRGBTokens(sText);
    if  (bNorm)     sText   =   (!TestStringAgainstPattern("**" + C_BOLD + "**", left(sText, 10)))  ?   C_BOLD + sText  :   sText;

    //  Talk standards, custom/rgb tokens
    //  ------------------------------------------------------------------------

    return  sText;
}



// +---------------------------------------------------------------------------+
// |                               GetDiagram                                  |
// +---------------------------------------------------------------------------+



string  GetDiagram(int nValue1, int nValue2, int nFrom = -2, int bInverted = FALSE, int r = 255, int g = 255, int b = 255)
{
    string  sResult, sColorX, sColor;
    int     nProgress, i;

    nValue1 =   (nValue1    <   0)  ?   0   :
                (nValue1    >   nValue2)    ?   nValue2 :   nValue1;
    sColorX =   "<c000>";

    if  (nFrom  ==  -1)
    sColor  =   rgb(r, g, b);

    else

    if  (nFrom  >=  0)
    {
        sColor  =   (nFrom  <   nValue1)    ?   C_POSI      :
                    (nFrom  >   nValue1)    ?   C_NEGA      :   C_NORM;
        sColorX =   (nFrom  <   nValue1)    ?   "<c0d0>"    :
                    (nFrom  >   nValue1)    ?   "<cd00>"    :   sColorX;
    }

    if  (nFrom  ==  -2)
    sColor  =   C_NORM;

    nProgress   =   FloatToInt((IntToFloat(nValue1) / IntToFloat(nValue2)) * 100);

    if  (bInverted)
    nProgress   =   100 - nProgress;
    nProgress   /=  2;

    while   (i  <   nProgress)
    {
        i++;
        sResult +=  "|";
    }

    sResult =   sColor + sResult;
    i       =   nProgress;

    while   (i  <   50)
    {
        if  (i  ==  nProgress)
        sResult +=  sColorX;
        sResult +=  "|";
        i++;
    }

    sResult =   sColor + "[" + sResult + sColor + "]";

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                             GetDefinedEffect                              |
// +---------------------------------------------------------------------------+



effect  GetDefinedEffect(string sDefinition)
{
    effect  eReturn;
    int     nParam1;
    string  sParam1;

    sParam1 =   GetNthSubString(sDefinition, 1);
    nParam1 =   StringToInt(sParam1);

    switch  (nParam1)
    {
        case    EFFECT_TYPE_ABILITY_DECREASE:

            eReturn =   EffectAbilityDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_ABILITY_INCREASE:

            eReturn =   EffectAbilityIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_AC_DECREASE:

            eReturn =   EffectACDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_AC_INCREASE:

            eReturn =   EffectACIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_APPEAR:

            eReturn =   EffectAppear(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_AREA_OF_EFFECT:

            eReturn =   EffectAreaOfEffect(
                            nParam1,
                            GetNthSubString(sDefinition, 2),
                            GetNthSubString(sDefinition, 3),
                            GetNthSubString(sDefinition, 4)
                        );

        break;

        case    EFFECT_TYPE_ATTACK_DECREASE:

            eReturn =   EffectAttackDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_ATTACK_INCREASE:

            eReturn =   EffectAttackIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_BEAM:

            eReturn =   EffectBeam(
                            nParam1,
                            GetLocalObject(
                                OBJECT_SELF,
                                SYS_VAR_TARGET_OBJECT
                            ),
                            StringToInt(GetNthSubString(sDefinition, 4)),
                            StringToInt(GetNthSubString(sDefinition, 5))
                        );

        break;

        case    EFFECT_TYPE_BLINDNESS:

            eReturn =   EffectBlindness();

        break;

        case    EFFECT_TYPE_CONCEALMENT:

            eReturn =   EffectConcealment(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_CONFUSED:

            eReturn =   EffectConfused();

        break;

        case    EFFECT_TYPE_CURSE:

            eReturn =   EffectCurse(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3)),
                            StringToInt(GetNthSubString(sDefinition, 4)),
                            StringToInt(GetNthSubString(sDefinition, 5)),
                            StringToInt(GetNthSubString(sDefinition, 6))
                        );

        break;

        case    EFFECT_TYPE_CUTSCENE_DOMINATED:

            eReturn =   EffectCutsceneDominated();

        break;

        case    EFFECT_TYPE_CUTSCENEGHOST:

            eReturn =   EffectCutsceneGhost();

        break;

        case    EFFECT_TYPE_CUTSCENEIMMOBILIZE:

            eReturn =   EffectCutsceneImmobilize();

        break;

        case    EFFECT_TYPE_CUTSCENE_PARALYZE:

            eReturn =   EffectCutsceneParalyze();

        break;

        case    EFFECT_TYPE_DAMAGE:

            eReturn =   EffectDamage(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_DAMAGE_DECREASE:

            eReturn =   EffectDamageDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_DAMAGE_IMMUNITY_DECREASE:

            eReturn =   EffectDamageImmunityDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_DAMAGE_IMMUNITY_INCREASE:

            eReturn =   EffectDamageImmunityIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_DAMAGE_INCREASE:

            eReturn =   EffectDamageIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_DAMAGE_REDUCTION:

            eReturn =   EffectDamageReduction(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_DAMAGE_RESISTANCE:

            eReturn =   EffectDamageResistance(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_ELEMENTALSHIELD:

            eReturn =   EffectDamageShield(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_DARKNESS:

            eReturn =   EffectDarkness();

        break;

        case    EFFECT_TYPE_DAZED:

            eReturn =   EffectDazed();

        break;

        case    EFFECT_TYPE_DEAF:

            eReturn =   EffectDeaf();

        break;

        case    EFFECT_TYPE_DEATH:

            eReturn =   EffectDeath(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_DISAPPEAR:

            eReturn =   EffectDisappear(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_DISEASE:

            eReturn =   EffectDisease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_DISPELMAGICALL:

            eReturn =   EffectDispelMagicAll(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_DISPELMAGICBEST:

            eReturn =   EffectDispelMagicBest(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_DOMINATED:

            eReturn =   EffectDominated();

        break;

        case    EFFECT_TYPE_ENTANGLE:

            eReturn =   EffectEntangle();

        break;

        case    EFFECT_TYPE_ETHEREAL:

            eReturn =   EffectEthereal();

        break;

        case    EFFECT_TYPE_FRIGHTENED:

            eReturn =   EffectFrightened();

        break;

        case    EFFECT_TYPE_HASTE:

            eReturn =   EffectHaste();

        break;

        case    EFFECT_TYPE_HEAL:

            eReturn =   EffectHeal(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_HP_CHANGE_WHEN_DYING:

            eReturn =   EffectHitPointChangeWhenDying(
                            StringToFloat(sParam1)
                        );

        break;

        case    EFFECT_TYPE_CHARMED:

            eReturn =   EffectCharmed();

        break;

        case    EFFECT_TYPE_IMMUNITY:

            eReturn =   EffectImmunity(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_INVISIBILITY:

            eReturn =   EffectInvisibility(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_KNOCKDOWN:

            eReturn =   EffectKnockdown();

        break;

        case    EFFECT_TYPE_LINK_EFFECTS:

            eReturn =   EffectLinkEffects(
                            GetDefinedEffect(
                                sParam1
                            ),
                            GetDefinedEffect(
                                GetNthSubString(sDefinition, 2)
                            )
                        );

        break;

        case    EFFECT_TYPE_MISS_CHANCE:

            eReturn =   EffectMissChance(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );


        break;

        case    EFFECT_TYPE_MODIFY_ATTACKS:

            eReturn =   EffectModifyAttacks(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_MOVEMENT_SPEED_DECREASE:

            eReturn =   EffectMovementSpeedDecrease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_MOVEMENT_SPEED_INCREASE:

            eReturn =   EffectMovementSpeedIncrease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_NEGATIVELEVEL:

            eReturn =   EffectNegativeLevel(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_PARALYZE:

            eReturn =   EffectParalyze();

        break;

        case    EFFECT_TYPE_PETRIFY:

            eReturn =   EffectPetrify();

        break;

        case    EFFECT_TYPE_POISON:

            eReturn =   EffectPoison(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_POLYMORPH:

            eReturn =   EffectPolymorph(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_REGENERATE:

            eReturn =   EffectRegenerate(
                            nParam1,
                            StringToFloat(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_RESURRECTION:

            eReturn =   EffectResurrection();

        break;

        case    EFFECT_TYPE_SANCTUARY:

            eReturn =   EffectSanctuary(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_SAVING_THROW_DECREASE:

            eReturn =   EffectSavingThrowDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_SAVING_THROW_INCREASE:

            eReturn =   EffectSavingThrowIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

        break;

        case    EFFECT_TYPE_SEEINVISIBLE:

            eReturn =   EffectSeeInvisible();

        break;

        case    EFFECT_TYPE_SILENCE:

            eReturn =   EffectSilence();

        break;

        case    EFFECT_TYPE_SKILL_DECREASE:

            eReturn =   EffectSkillDecrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_SKILL_INCREASE:

            eReturn =   EffectSkillIncrease(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_SLEEP:

            eReturn =   EffectSleep();

        break;

        case    EFFECT_TYPE_SLOW:

            eReturn =   EffectSlow();

        break;

        case    EFFECT_TYPE_SPELL_FAILURE:

            eReturn =   EffectSpellFailure(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;

        case    EFFECT_TYPE_SPELL_IMMUNITY:

            eReturn =   EffectSpellImmunity(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_SPELLLEVELABSORPTION:

            eReturn =   EffectSpellLevelAbsorption(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToInt(GetNthSubString(sDefinition, 3))
                        );

       break;

        case    EFFECT_TYPE_SPELL_RESISTANCE_DECREASE:

            eReturn =   EffectSpellResistanceDecrease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_SPELL_RESISTANCE_INCREASE:

            eReturn =   EffectSpellResistanceIncrease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_STUNNED:

            eReturn =   EffectStunned();

        break;

        case    EFFECT_TYPE_SUMMON_CREATURE:

            eReturn =   EffectSummonCreature(
                            sParam1,
                            StringToInt(GetNthSubString(sDefinition, 2)),
                            StringToFloat(GetNthSubString(sDefinition, 3)),
                            StringToInt(GetNthSubString(sDefinition, 4))
                        );

        break;

        case    EFFECT_TYPE_SWARM:

            eReturn =   EffectSwarm(
                            nParam1,
                            GetNthSubString(sDefinition, 2),
                            GetNthSubString(sDefinition, 3),
                            GetNthSubString(sDefinition, 4),
                            GetNthSubString(sDefinition, 5)
                        );

        break;

        case    EFFECT_TYPE_TEMPORARY_HITPOINTS:

            eReturn =   EffectTemporaryHitpoints(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_TIMESTOP:

            eReturn =   EffectTimeStop();

        break;

        case    EFFECT_TYPE_TRUESEEING:

            eReturn =   EffectTrueSeeing();

        break;

        case    EFFECT_TYPE_TURNED:

            eReturn =   EffectTurned();

        break;

        case    EFFECT_TYPE_TURN_RESISTANCE_DECREASE:

            eReturn =   EffectTurnResistanceDecrease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_TURN_RESISTANCE_INCREASE:

            eReturn =   EffectTurnResistanceIncrease(
                            nParam1
                        );

        break;

        case    EFFECT_TYPE_ULTRAVISION:

            eReturn =   EffectUltravision();

        break;

        case    EFFECT_TYPE_VISUALEFFECT:

            eReturn =   EffectVisualEffect(
                            nParam1,
                            StringToInt(GetNthSubString(sDefinition, 2))
                        );

        break;
    }

    return  eReturn;
}



// +---------------------------------------------------------------------------+
// |                                   rgb                                     |
// +---------------------------------------------------------------------------+



string  rgb(int r = 255, int g = 255, int b = 255)
{
    r               =   (r  >=  255)    ?   254 :
                        (r  <   0)      ?   0   :   r;
    g               =   (g  >=  255)    ?   254 :
                        (g  <   0)      ?   0   :   g;
    b               =   (b  >=  255)    ?   254 :
                        (b  <   0)      ?   0   :   b;
    string  sRed    =   Get2DAString("sys_colors", "Label", r);
    string  sGreen  =   Get2DAString("sys_colors", "Label", g);
    string  sBlue   =   Get2DAString("sys_colors", "Label", b);
    string  sResult =   "</c><c" + sRed + sGreen + sBlue + ">";

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                                    speak                                  |
// +---------------------------------------------------------------------------+



void    speak(string sMessage, string sType = "TALK", int nVolume = -1)
{
    if  (nVolume    !=  -1) SpeakString(sMessage, nVolume);
    else
    {
        if  (sType  ==  "WHISPER")      SpeakString(sMessage, TALKVOLUME_WHISPER);      else
        if  (sType  ==  "SHOUT")        SpeakString(sMessage, TALKVOLUME_SHOUT);        else
        if  (sType  ==  "PARTY")        SpeakString(sMessage, TALKVOLUME_PARTY);        else
                                        SpeakString(sMessage, TALKVOLUME_TALK);
    }
}



// +---------------------------------------------------------------------------+
// |                                ConvertText                                |
// +---------------------------------------------------------------------------+



string  ConvertText(string sText)
{
    if  (sText  ==  "") return  "";

    string  sResult;
    int     pos         =   0;
    int     nlen        =   GetStringLength(sText);
    string  sChar       =   GetSubString(sText, pos, 1);
    string  sLine       =   "�����������������������ͼ��؊���ݎ";
    string  sConverted  =   "aacdeeilnoorstuuyzACDEEILNORSTIUYZ";

    while   (sChar  !=  "")
    {
        int nFind   =   FindSubString(sLine, sChar);

        if  (nFind  !=  -1) sChar   =   GetSubString(sConverted, nFind, 1);

        sResult +=  sChar;
        sChar   =   GetSubString(sText, ++pos, 1);
    }

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                               ConvertPattern                              |
// +---------------------------------------------------------------------------+



string  ConvertPattern(string sText, int bClear = FALSE, string sColorP = C_NORM, string sColorT = C_BOLD)
{
    if  (sText  ==  "") return  "";

    string  sResult;

    sResult =   sText;

    if  (!bClear)
    {
        sResult =   FindAndReplace(sResult, "(", sColorP + "(" + sColorT);
        sResult =   FindAndReplace(sResult, ")", sColorP + ")" + sColorT);
        sResult =   FindAndReplace(sResult, "**", sColorP + "?" + sColorT);
        sResult =   FindAndReplace(sResult, "*w", sColorP + "_" + sColorT);
        sResult =   FindAndReplace(sResult, "*n", sColorP + "#" + sColorT);
        sResult =   FindAndReplace(sResult, "*p", sColorP + "." + sColorT);
        sResult =   FindAndReplace(sResult, "*a", sColorP + "A" + sColorT);
        sResult =   FindAndReplace(sResult, "|", sColorP + " / " + sColorT);
    }

    else
    {
        sResult =   FindAndReplace(sResult, "**", "");
        sResult =   FindAndReplace(sResult, "*w", "");
        sResult =   FindAndReplace(sResult, "*n", "");
        sResult =   FindAndReplace(sResult, "*p", "..");
        sResult =   FindAndReplace(sResult, "*a", "");
        sResult =   FindAndReplace(sResult, "|", " / ");
    }

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                           FindAndReplacePart                              |
// +---------------------------------------------------------------------------+



string  FindAndReplacePart(string sString, string sStartSub, string sEndSub, string sNewSub = "", int bAll = TRUE)
{
    if  (sString    ==  ""
    ||  sStartSub   ==  ""
    ||  sEndSub     ==  "")
    return  "";

    int     nFindStart, nFindEnd, nLenStart, nLenEnd, nLen, nLenNew;
    string  sResult, sNewString, sLeft, sRight;

    sNewString  =   sString;
    nLenStart   =   GetStringLength(sStartSub);
    nLenEnd     =   GetStringLength(sEndSub);
    nLenNew     =   GetStringLength(sNewSub);
    nLen        =   GetStringLength(sNewString);
    nFindStart  =   FindSubString(sNewString, sStartSub, 0);
    nFindEnd    =   FindSubString(sNewString, sEndSub, nFindStart + nLenStart);

    while   (nFindStart !=  -1
    &&      nFindEnd    !=  -1)
    {
        sLeft       =   left(sNewString, nFindStart);
        sRight      =   right(sNewString, nLen - nFindEnd - nLenEnd);
        sNewString  =   sLeft + sNewSub + sRight;
        nLen        =   GetStringLength(sNewString);
        nFindStart  =   FindSubString(sNewString, sStartSub, 0);
        nFindEnd    =   FindSubString(sNewString, sEndSub, nFindStart + nLenStart);

        if  (!bAll) break;
    }

    sResult =   sNewString;

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                           ConvertCustomTokens                             |
// +---------------------------------------------------------------------------+



string  ConvertCustomTokens(string sText, object oObject = OBJECT_INVALID)
{
    if  (sText  ==  "") return  "";

    oObject =   (!GetIsObjectValid(oObject))    ?   GetModule() :   oObject;

    int     nFind, nLen, nLenNew, nToken;
    string  sResult, sNewString, sLeft, sRight, sToken;

    sNewString  =   sText;
    nLen        =   GetStringLength(sNewString);
    nFind       =   FindSubString(sNewString, "<CUSTOM", 0);

    while   (nFind  !=  -1)
    {
        sLeft       =   left(sNewString, nFind);
        sRight      =   right(sNewString, nLen - nFind - 11);
        nToken      =   stoi(GetSubString(sNewString, nFind + 7, 3));
        sToken      =   GetLocalString(oObject, SYSTEM_SYS_PREFIX + "_TOKEN_" + itos(nToken));
        sToken      =   (sToken ==  "") ?   "<CUSTOM" + itos(nToken) + ">"  :   ConvertTokens(sToken, oObject, TRUE);
        sNewString  =   sLeft + sToken + sRight;
        nLen        =   GetStringLength(sNewString);
        nFind       =   FindSubString(sNewString, "<CUSTOM", nLen - GetStringLength(sRight));

        //DeleteLocalString(oObject, SYSTEM_SYS_PREFIX + "_TOKEN_" + itos(nToken));
    }

    sResult =   sNewString;

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                            ConvertRGBTokens                               |
// +---------------------------------------------------------------------------+



string  ConvertRGBTokens(string sString)
{
    if  (sString    ==  "") return  sString;

    int     nFind, nLen, nLenNew, nColorR, nColorG, nColorB;
    string  sResult, sLeft, sRight, sColor;

    sResult =   sString;
    nLen    =   GetStringLength(sResult);
    nFind   =   FindSubString(sResult, "<StartRGB", 0);

    while   (nFind  !=  -1)
    {
        sLeft   =   left(sResult, nFind);
        sRight  =   right(sResult, nLen - nFind - 19);
        nColorR =   stoi(GetSubString(sResult, nFind + 9, 3));
        nColorG =   stoi(GetSubString(sResult, nFind + 12, 3));
        nColorB =   stoi(GetSubString(sResult, nFind + 15, 3));
        sColor  =   rgb(nColorR, nColorG, nColorB);
        sResult =   sLeft + sColor + sRight;
        nLen    =   GetStringLength(sResult);
        nFind   =   FindSubString(sResult, "<StartRGB", nLen - GetStringLength(sRight));
    }

    return  sResult;
}



string  GetItemPropertyName(itemproperty ipProp)
{
    // See http://nwn.bioware.com/developers/Bioware_Aurora_Item_Format.pdf
    // PropertyName : Subtype [CostValue] [Param1: Param1Value]
    string  sName;

    // Note: The above names are given to variables refering to parts of the
    // property description, i.e. PropertyName, Subtype, CostValue, Param1 &
    // Param1Value.

    // The following suffixes are used:
    // 2da - for a variable containing the name of a 2da table
    // Index - for a variable containing a 2da row number
    // StrRef - for a variable refering to a row in dialog.tlk (talk table)

    /* PropertyName
    Look up the StrRef stored in column 0 (should be Name) of itempropdef.2da
    (Table 5.2), at the row indexed by the ItemProperty Struct's PropertyName
    Field (see Table 2.1.3). This StrRef points to the name of the Item
    Property (eg., "Damage Bonus vs. Racial Type", "On Hit", "Light")
    */

    int     nType               =   GetItemPropertyType(ipProp);
    string  sPropertyNameStrRef =   Get2DAString("itempropdef", "Name", nType);

    sName   =   GetStringByStrRef(StringToInt(sPropertyNameStrRef));

    /* Subtype
    In itempropdef.2da, look up the SubTypeResRef column value at the row
    indexed by the ItemProperty Struct's PropertyName Field. If it is ****,
    then there are no subtypes for this Item Property, so there is no subtype
    portion of the Item Property description. Otherwise, the string under this
    column is the ResRef of the subtype table 2da.

    If there is a subtype table, load it and use the ItemProperty Struct's
    Subtype Field as an index into the 2da. Get the StrRef from the name column.
    This StrRef points to the name of the Subtype (eg., "Dragon", "Daze").
    */

    string  sSubType2DA =   Get2DAString("itempropdef", "SubTypeResRef", nType);
    int     nSubType    =   GetItemPropertySubType(ipProp);

    if  (sSubType2DA    !=  "")
    {
        string  sSubTypeStrRef  =   Get2DAString(sSubType2DA, "Name", nSubType);

        sName   +=  " : " + GetStringByStrRef(StringToInt(sSubTypeStrRef));
    }

    /* CostTable Value
    In itempropdef.2da, look up the CostTableResRef number at the row indexed
    by the ItemProperty Struct's PropertyName Field. This should be the same as
    the ItemProperty Struct's CostTable Field.

    Use the CostTableResRef value as an index into iprp_costtable.2da and get
    the string under the Name column. This is the ResRef of the cost table 2da
    to use.

    Load the cost table 2da. Using the ItemProperty Struct's CostValue Field
    as an index into the cost table, get the Name column StrRef. This StrRef
    points to the name of the Cost value
    (eg., "1 Damage", "DC = 14", "Dim (5m)")
    */

    int nCostTableIndex =   GetItemPropertyCostTable(ipProp);

    if  (nCostTableIndex    >   0) // 0 is invalid for a cost table index. It leads to iprp_base1.2da, which is empty.
    {
        string  sCostTable2DA           =   Get2DAString("iprp_costtable", "Name", nCostTableIndex);
        string  sCostTableValueStrRef   =   Get2DAString(sCostTable2DA, "Name", GetItemPropertyCostTableValue(ipProp));

        sName   +=  " " + GetStringByStrRef(StringToInt(sCostTableValueStrRef));
    }


    /* Param
    Get the ResRef of the Param Table.

    If the ItemProperty has a subtype table (see Section 4.3.2), then look for
    a Param1ResRef column in the subtype table. This column contains the param
    table index, and should be identical the Param1 Field of the ItemProperty
    Struct.
    */
    string  sParam1Index    =   "";

    if  (sSubType2DA    !=  "")
    sParam1Index    =   Get2DAString(sSubType2DA, "Param1ResRef", nSubType);

    /*
    If the ItemProperty does not have a subtype table, or the subtype table
    does not have a Param1ResRef column, then look under the Param1ResRef
    column in itempropdef.2da, and use that as the param table index. In this
    case as well, the index should equal the Param1 Field of the ItemProperty
    Struct.
    */
    if  (sParam1Index   ==  "")
    sParam1Index    =   Get2DAString("itempropdef", "Param1ResRef", nType);

    if  (sParam1Index   !=  "")
    {
        /*
        Use the param table index as an index into iprp_paramtable.2da. Look under
        the Name column for a StrRef, and look under the TableResRef column for a
        string. The Name StrRef points to the text for the name of the parameter
        (eg., "Type", "Duration", "Color"). The TableResRef string is the ResRef
        of the param table 2da.
        */
        string  sParam1StrRef   =   Get2DAString("iprp_paramtable", "Name", StringToInt(sParam1Index));
        string  sParam1Value2DA =   Get2DAString("iprp_paramtable", "TableResRef", StringToInt(sParam1Index));

        /*
        Use the ItemProperty Struct's Param1Value as an index into the param table
        found above, and get the StrRef under the "Name" column. This StrRef points
        to the name of the param value (eg., "Acid", "50% / 2 Rounds", "Red")
        */
        int     nParam1ValueIndex   =   GetItemPropertyParam1Value(ipProp);
        string  sParam1ValueStrRef  =   Get2DAString(sParam1Value2DA, "Name", nParam1ValueIndex);
        string  sParam1Value        =   GetStringByStrRef(StringToInt(sParam1ValueStrRef));

        sName   +=  " " + GetStringByStrRef(StringToInt(sParam1StrRef)) + ": " + sParam1Value;
    }

    return sName;
}
