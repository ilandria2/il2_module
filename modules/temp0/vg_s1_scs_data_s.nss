// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Persistent data save script            |
// | File    || vg_s1_scs_data_s.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nRow    =   1;
    int     nLast   =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_ROW_I_ROWS");
    int     nSpell  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_ROW", nRow);
    int     bSaved  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SAVED", nSpell);
    int     nPC     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sQuerry;

    while   (nRow   <=  nLast)
    {
        if  (!bSaved)
        {
            if  (sQuerry    ==  "")
            {
                sQuerry =   "INSERT INTO " + SCS_DB_TABLE_SPELLS + " " +
                "(" +
                    SCS_DB_COLUMN_CHARACTER_ID + "," +
                    SCS_DB_COLUMN_SPELL_ID +
                ") VALUES ";
            }

            if  (right(sQuerry, 1)  ==  ")")    sQuerry +=  ",";

            sQuerry +=
            "(" +
                itos(nPC) + "," +
                itos(nSpell) +
            ")";

            sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SAVED", TRUE, nSpell);
        }

        nRow++;
        nSpell  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_ROW", nRow);
        bSaved  =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SAVED", nSpell);
    }

    if  (sQuerry    !=  "") SQLExecDirect(sQuerry);

    int nMana   =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA");

    sQuerry =   "SELECT " + SCS_DB_COLUMN_MANA_VALUE + " " +
                "FROM " + SCS_DB_TABLE_MANA + " " +
                "WHERE " + SCS_DB_COLUMN_CHARACTER_ID + " = '" + itos(nPC) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_ERROR)
    {
        sQuerry =   "INSERT INTO " + SCS_DB_TABLE_MANA + " " +
        "(" +
            SCS_DB_COLUMN_CHARACTER_ID + "," +
            SCS_DB_COLUMN_MANA_VALUE +
        ") VALUES " +
        "('" +
            itos(nPC) + "','" +
            itos(nMana) +
        "')";

        SQLExecDirect(sQuerry);
    }

    else
    {
        sQuerry =   "UPDATE " + SCS_DB_TABLE_MANA + " " +
                    "SET " + SCS_DB_COLUMN_MANA_VALUE + " = '" + itos(nMana) + "' " +
                    "WHERE " + SCS_DB_COLUMN_CHARACTER_ID + " = '" + itos(nPC) + "'";

        SQLExecDirect(sQuerry);
    }
}
