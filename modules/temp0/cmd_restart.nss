// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_restart.nss                                                |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 05-12-2009                                                     |
// | Updated || 05-12-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "RESTART"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



void    PerformRestart()
{
    string  sModuleName =   GetTag(GetModule());

    StartNewModule(sModuleName);
}



void    PerformRestartSequence()
{
    object  oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     nTime       =   gli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_TIME");

    if  (!nTime)
    {
        PerformRestart();
        return;
    }

    else

    //  zastaveni sekvence
    if  (nTime  ==  -1)
    {
        sli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_SEQUENCE", -1, -1, SYS_M_DELETE);
        sli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_TIME", -1, -1, SYS_M_DELETE);
        return;
    }

    string  sMins       =   (nTime  ==  1)  ?   "minute"    :
                            (nTime  <   5)  ?   "minutes"   :   "minutes";
    string  sMessage    =   R1 + "( !!! ) " + R2 + "In " + R1 + itos(nTime) + " " + R2 + sMins + " the server will restart.";

    AssignCommand(GetModule(), SpeakString(sMessage, TALKVOLUME_SHOUT));

    sli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_TIME", 1, -1, SYS_M_DECREMENT);
    DelayCommand(60.0f, PerformRestartSequence());
}



void    RunRestartSequence()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     bActive =   gli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_SEQUENCE");

    if  (bActive)   return;

    sli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_SEQUENCE", TRUE);
    PerformRestartSequence();
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "RESTART";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     nInterval   =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    string  sMessage;

    //  neplatny interval
    if  (nInterval  <   -1)
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid interval";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  zastaveni sekvence
    if  (nInterval  ==  -1)
    {
        int nTime   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_SEQUENCE");

        //  pokus o zastaveni sekvence, kdyz jeste sekvence nebezi
        if  (!nTime)
        {
            sMessage    =   R1 + "( ! ) " + W2 + "You cannot stop the restart sequence if it did not start yet";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }

        sli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_TIME", nInterval);

        sMessage    =   Y1 + "( ? ) " + W2 + "Restart sequence was " + R2 + "stopped";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    else

    if  (nInterval  >   0)
    {
        int nOldTime    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_TIME");

        sli(oSystem, SYSTEM_CCS_PREFIX + "_RESTART_TIME", nInterval);

        sMessage    =   Y1 + "( ? ) " + W2 + "Restart sequence was changed from [" + Y2 + itos(nOldTime) + W2 + "] to [" + G2 + itos(nInterval) + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        RunRestartSequence();
        return;
    }

    else
    {
        PerformRestart();
        return;
    }

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
