// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Player dying (bleeding) script               |
// | File    || vg_s1_ds_bleed.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 02-12-2010                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery da efekt krvaceni prave umirajici postave
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_ds_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    BleedingLoop(object oPC)
{
    int bBleed  =   gli(oPC, SYSTEM_DS_PREFIX + "_BLEEDING");

    if  (!bBleed
    ||  !GetIsObjectValid(oPC)) return;

    int nHP =   GetCurrentHitPoints(oPC);

    if  (nHP    >   0)
    {
        SYS_Info(oPC, SYSTEM_DS_PREFIX, "BLEEDING_STOP");
        sli(oPC, SYSTEM_DS_PREFIX + "_BLEEDING", FALSE, -1, SYS_M_DELETE);
        return;
    }

    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(1, DAMAGE_TYPE_MAGICAL, DAMAGE_POWER_ENERGY), oPC);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_BLOOD_LRG_RED), oPC);

    nHP =   GetCurrentHitPoints(oPC);

    if  (nHP    <=  -10)
    {
        SYS_Info(oPC, SYSTEM_DS_PREFIX, "BLED_TO_DEATH");
        sli(oPC, SYSTEM_DS_PREFIX + "_BLEEDING", FALSE, -1, SYS_M_DELETE);
    }

    else
    {
        SYS_Info(oPC, SYSTEM_DS_PREFIX, "BLEEDING");

        int nPain;

        if  (d100() <=  40)
        {
            nPain   =   Random(5);
            nPain   =   (nPain  >   2)  ?   VOICE_CHAT_HEALME   :   VOICE_CHAT_HELP;

            DelayCommand(0.5f, PlayVoiceChat(nPain, oPC));
        }

        else
        {
            nPain   =   Random(3);
            nPain   =   (nPain  ==  0)  ?   VOICE_CHAT_PAIN1    :
                        (nPain  ==  1)  ?   VOICE_CHAT_PAIN2    :   VOICE_CHAT_PAIN3;

            PlayVoiceChat(nPain, oPC);
        }

        DelayCommand(DS_BLEEDING_SEQUENCE_TIME, BleedingLoop(oPC));
    }
}



void    StartBleeding(object oPC)
{
    int bBleed  =   gli(oPC, SYSTEM_DS_PREFIX + "_BLEEDING");

    if  (bBleed)    return;

    sli(oPC, SYSTEM_DS_PREFIX + "_BLEEDING", TRUE);
    SYS_Info(oPC, SYSTEM_DS_PREFIX, "BLEEDING");
    DelayCommand(DS_BLEEDING_SEQUENCE_TIME, BleedingLoop(oPC));
}



void    main()
{
    object  oPC =   GetLastPlayerDying();

    //ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectHitPointChangeWhenDying(-0.5f), oPC, 999999.0f);
    StartBleeding(oPC);
}
