// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_kill.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 27-06-2009                                                     |
// | Updated || 27-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "KILL"
*/
// -----------------------------------------------------------------------------

#include "vg_s0_ccs_core_t"
#include "vg_s0_sys_core_o"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void    main()
{
    string      sCmd    =   "KILL";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sType   =   upper(CCS_GetConvertedString(sLine, 1, FALSE));
    int     bPlot   =   CCS_GetConvertedInteger(sLine, 2, FALSE);
    object  oKilled =   CCS_GetConvertedObject(sLine, 3, FALSE);

    //  Neplatny cil
    if  (!GetIsObjectValid(oKilled))
    {
        string  sMessage    =   R1 + "( ! ) " + W2 + "Invalid target";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (bPlot)
    {
        SetImmortal(oKilled, FALSE);
        SetPlotFlag(oKilled, FALSE);
        SetCutsceneMode(oKilled, FALSE, TRUE);
    }

    if (GetObjectType(oKilled) == OBJECT_TYPE_ITEM)
        sType = "DESTROY";

    if  (sType  ==  "DEATH")    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(TRUE, FALSE), oKilled);
    if  (sType  ==  "DESTROY")  Destroy(oKilled);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
