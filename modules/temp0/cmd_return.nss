// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_return.nss                                                 |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 28-06-2009                                                     |
// | Updated || 28-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "RETURN"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_ds_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "RETURN";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    int         bLog    =   gli(GetModule(), SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     bPenale =   CCS_GetConvertedInteger(sLine, 1, FALSE);
    object  oPlayer =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;

    if  (!GetIsPC(oPlayer))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "You must select a player character";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    object  oCorpse =   glo(oPlayer, SYSTEM_DS_PREFIX + "_CORPSE");

    if  (!GetIsObjectValid(oCorpse))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "You cannot return a player character that did not die yet";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (bPenale)
    {
        sMessage    =   Y1 + "( ? ) " + W2 + "Penalizing [" + G1 + GetPCPlayerName(oPlayer) + ", " + GetName(oPlayer) + W2 + "]";

        msg(oPC, sMessage, FALSE, FALSE);
        ExecuteScript(DS_RETURN_PENALIZATION, oPlayer);
    }

    sMessage    =   Y1 + "( ? ) " + W2 + "Returning player character [" + G1 + GetPCPlayerName(oPlayer) + ", " + GetName(oPlayer) + W2 + "] from the sphere of death";

    msg(oPC, sMessage, FALSE, FALSE);
    DS_ReturnFromSphere(oPlayer);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
