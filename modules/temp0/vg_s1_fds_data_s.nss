// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) Persistent data save script        |
// | File    || vg_s1_fds_data_s.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_fds_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!GetIsObjectValid(OBJECT_SELF))    return;

    string  sQuerry;
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    int     nHunger =   gli(OBJECT_SELF, SYSTEM_FDS_PREFIX + "_HUNGER");
    int     nThirst =   gli(OBJECT_SELF, SYSTEM_FDS_PREFIX + "_THIRST");

    sQuerry =   "INSERT INTO " +
                    "fds_stomach " +
                "VALUES " +
                "('" +
                    itos(nID) + "','" +
                    itos(nHunger) + "','" +
                    itos(nThirst) + "'," +
                    "NULL" +
                ") ON DUPLICATE KEY UPDATE " +
                    "Hunger = '" + itos(nHunger) + "'," +
                    "Thirst = '" + itos(nThirst) + "'," +
                    "date = NULL";

    SQLExecDirect(sQuerry);
}
