// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Area exit script                 |
// | File    || vg_s1_tis_exit.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Spawnovaci systemu dynamickych objektu (soucast systemu TIS)
    Snizi pocet hracu z oblasti, z kt. prave odesel hrac (promenna)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_const"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    sli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_PLAYERS", 1, -1, SYS_M_DECREMENT);
}

