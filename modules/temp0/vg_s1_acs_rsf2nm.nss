#include "vg_s0_acs_core_c"

void main()
{
    object item = OBJECT_SELF;
    string techDesc = GetDescription(item, FALSE, FALSE);

    // this name
    int itemRecipe = GetTechPart(techDesc, TECH_PART_RECIPE_ID);
    string itemName = ACS_GetRecipeName(itemRecipe, TRUE);
    itemName = ConvertTokens(itemName, OBJECT_INVALID, FALSE, TRUE, FALSE, FALSE);

    if (IsWeightBasedItem(item))
    {
        string weight = ACS_GetWeightedRecipeName(itemRecipe, IPS_GetItemWeight(item));
        itemName += Y2 + "\n (" + weight + ")";
    }

    // parent name
    int parentRecipe = GetTechPart(techDesc, TECH_PART_PARENT_RECIPE_ID);
    string parentName = ACS_GetRecipeName(parentRecipe, TRUE);

    // child names
    int childCount = GetTechPart(techDesc, TECH_PART_CHILDREN_COUNT);
    int nth = 0;
    string childNames = "";
    for (; nth < childCount; nth++)
    {
        if (nth > 0)
            childNames += "\n";
        int childRecipe = GetTechPart(techDesc, TECH_PART_NTH_CHILD_RECIPE_ID, nth);
        string childName = ACS_GetRecipeName(childRecipe, TRUE);
        childNames += childName;
    }

    // final merged name
    string finalName = itemName;

    if (parentRecipe)
        finalName += "\n\n" + C1 + "IN:\n" + C2 + parentName;

    if (childCount)
        finalName += "\n\n" + C1 + "CONTAINS:\n" + C2 + childNames;

    SetName(item, finalName);
}
