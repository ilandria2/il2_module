// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) Constants                      |
// | File    || vg_s0_lts_const.nss                                           |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu LTS
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_LTS_NAME                     =   "Lootable Treasures System";
const string    SYSTEM_LTS_PREFIX                   =   "LTS";
const string    SYSTEM_LTS_VERSION                  =   "2.00";
const string    SYSTEM_LTS_AUTHOR                   =   "VirgiL";
const string    SYSTEM_LTS_DATE                     =   "01-06-2008";
const string    SYSTEM_LTS_UPDATE                   =   "25-07-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    LTS_TAG_TREASURE_AREA               =   "VG_AX_00_SYSAREA";
const string    LTS_TAG_TREASURE_CONTAINER          =   "VG_P_TC_LTS_CONT";

/*
const string    LTS_PARAMETER_SPAWN_ITEMS_MIN       =   "ITEMS_MIN";
const string    LTS_PARAMETER_SPAWN_ITEMS_MAX       =   "ITEMS_MAX";
const string    LTS_PARAMETER_SPAWN_GOLD_CHANCE     =   "GOLD_CHANCE";
const string    LTS_PARAMETER_SPAWN_GOLD_MIN        =   "GOLD_MIN";
const string    LTS_PARAMETER_SPAWN_GOLD_MAX        =   "GOLD_MAX";
const string    LTS_PARAMETER_SPAWN_CHANCE          =   "SPAWN_CHANCE";
const string    LTS_PARAMETER_SPAWN_TIME            =   "HEARTBEATS";
const string    LTS_PARAMETER_SPAWN_SOURCE          =   "SOURCE";
*/

const int       LTS_DEFAULT_SPAWN_HEARTBEATS        =   2;
const int       LTS_DESPAWN_HEARTBEATS              =   100;
const int       LTS_TIME_CONSTANT                   =   100;
const float     LTS_TIME_MOD_BONUS_LIMIT            =   2.0f;
