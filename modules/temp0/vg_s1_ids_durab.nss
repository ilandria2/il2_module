// +---------++----------------------------------------------------------------+
// | Name    || Item Durability System (IDS) Custom loop durability script     |
// | File    || vg_s1_ids_durab.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Snizi durabilitu predmetu v inventari (zatim jen pochodne):
    - equipnute predmety
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ids_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nLoop   =   gli(__IDS(), IDS_ + "DURAB_LOOP");

    sli(__IDS(), IDS_ + "DURAB_LOOP", ++nLoop);

    //  kod ma nastat pouze pri prekroceni limitu
    if  (nLoop  <   IDS_SEQ_CUSTOM_LOOPS_DURAB)   return;

    object  oPC =   GetFirstPC();
    float   fDelay;

    while   (GetIsObjectValid(oPC))
    {
        if  (!GetIsDM(oPC))
        {
            /*string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

            if  (sArea  !=  "S"
            &&  sArea   !=  "X")
            {*/
                fDelay  +=  ran(0.0f, 5.0f, 2);

                DelayCommand(fDelay, IDS_UpdateItemsInInventory(oPC, TRUE));
            //}
        }

        oPC =   GetNextPC();
    }

    sli(__IDS(), IDS_ + "DURAB_LOOP", -1, -1, SYS_M_DELETE);
}

