// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Persistent data check script                 |
// | File    || vg_s1_ds_data_c.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Overi perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_ds_const"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sLog, sQuerry;

    sLog    =   "[" + SYSTEM_DS_PREFIX + "] Checking table [ds_corpses]";
    sQuerry =   "DESCRIBE ds_corpses";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [ds_corpses] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [ds_corpses] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE ds_corpses " +
        "(" +
            "PlayerID INT(6) NOT NULL PRIMARY KEY," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE ds_corpses";

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [ds_corpses] created successfully"; else
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [ds_corpses] error in creation";

        logentry(sLog);
    }

    /*
    sLog    =   "[" + SYSTEM_DS_PREFIX + "] Checking table [" + DS_DB_TABLE_CORPSES + "]";
    sQuerry =   "DESCRIBE " + DS_DB_TABLE_CORPSES;

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [" + DS_DB_TABLE_CORPSES + "] already exist";

        logentry(sLog);
    }

    else
    {
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [" + DS_DB_TABLE_CORPSES + "] does not exist -> Creating it";

        logentry(sLog);

        sQuerry =   "CREATE TABLE " + DS_DB_TABLE_CORPSES + " " +
        "(" +
            DS_DB_COLUMN_PLAYER_ID + " INT(6) NOT NULL," +
            DS_DB_COLUMN_CORPSE + " VARCHAR(200) DEFAULT NULL," +
            DS_DB_COLUMN_KILLER + " INT(1) DEFAULT NULL," +
            "date TIMESTAMP NOT NULL" +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry =   "DESCRIBE " + DS_DB_TABLE_CORPSES;

        SQLExecDirect(sQuerry);

        if  (SQLFetch() ==  SQL_SUCCESS)
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [" + DS_DB_TABLE_CORPSES + "] created successfully"; else
        sLog    =   "[" + SYSTEM_DS_PREFIX + "] Table [" + DS_DB_TABLE_CORPSES + "] error in creation";

        logentry(sLog);
    }
    */
}
