// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) OnOpen                         |
// | File    || vg_s1_lts_open.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 01-06-2008                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript udalosi "OnOpen" pro poklad
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lts_const"
#include    "vg_s0_lts_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sPost   =   gls(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_SOURCE");

    if  (sPost  ==  "") return;
    if  (gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_ONCE"))     return;

    object  oPC =   glo(OBJECT_SELF, "TIS_OPENED_BY");

    oPC =   (!GetIsObjectValid(oPC))    ?   GetLastOpenedBy()   :   oPC;

    //  neni to hrac
    if  (GetIsDM(oPC)
    ||  (!GetIsPC(oPC)
    &&  !GetIsPC(GetMaster(oPC))))  return;

    if  (gli(oPC, "lts_test"))SpawnScriptDebugger();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    object  oSource =   GetObjectByTag("VG_P_TC_LTS_CONT_" + sPost);
    int     nTimes  =   gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEAT");
    int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");

    //  progress
    if  (nTimes !=  -2)
    {
        int nMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_HEARTBEATS");

        //  jiz dosahlo maximum
        if  (nHBs - nTimes  >=  nMax)
        sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEAT", -2);

        //  poklad otevren pred jeho spawnutim - reset
        else
        {
            sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEAT", nHBs);
            //SYS_Info(oPC, SYSTEM_LTS_PREFIX, "SEQUENCE_RESET");
            return;
        }
    }

    /*
    int     nItems  =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_COUNT");

    //  poklad neni nastaven
    if  (!nItems)
    {
        SYS_Info(oPC, SYSTEM_LTS_PREFIX, "TREASURE_NOT_SET");
        return;
    }*/

    object  oItem   =   GetFirstItemInInventory(OBJECT_SELF);

    //  poklad je jiz spawnut ale nebyl vzat - nic se nestane
    if  (GetIsObjectValid(oItem))   return;

    int nChance =   gli(oSource, SYSTEM_LTS_PREFIX + "_SPAWN_CHANCE");

    nChance =   (!nChance)  ?   100 :   nChance;

    if  (nChance    ==  -1) sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_ONCE", TRUE);

    //  neuspech pri hodu
    if  (Random(100)    >=  nChance)
    {
        sli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_HEARTBEAT", nHBs);
        return;
    }

    int bMain       =   gli(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_MAIN_TREASURE");
    int nItemsMin   =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MIN");
    int nItemsMax   =   gli(oSource, SYSTEM_LTS_PREFIX + "_ITEMS_MAX");
    int nGoldMin    =   gli(oSource, SYSTEM_LTS_PREFIX + "_GOLD_MIN");
    int nGoldMax    =   gli(oSource, SYSTEM_LTS_PREFIX + "_GOLD_MAX");
    int nAmount     =   nItemsMin + (Random(nItemsMax - nItemsMin + 1));

    nChance =   gli(oSource, SYSTEM_LTS_PREFIX + "_GOLD_CHANCE");

    LTS_SpawnTreasure(OBJECT_SELF, oSource, bMain, nAmount, nChance, nGoldMin, nGoldMax);
}
