// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (GUI) Sys info                         |
// | File    || vg_s1_gui_info.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu GUI
*/
// -----------------------------------------------------------------------------

#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_lpd_core_c"
#include    "vg_s0_gui_const"

// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+

void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat;

    object  oSystem =   GetObjectByTag("SYSTEM" + _GUI_ + "OBJECT");

    sTemp   =   gls(OBJECT_SELF, GUI_ + "NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "SYSTEM_SKILLS")
    {

    }

    else if (sCase == "CHECK_RESULT")
    {
        int bResult = stoi(GetNthSubString(sTemp, 1));


        sMessage = ConvertTokens(sMessage, OBJECT_SELF, TRUE, FALSE, FALSE, FALSE);
        bFloat = TRUE;
    }

    msg(OBJECT_SELF, sMessage, bFloat, FALSE);
}

