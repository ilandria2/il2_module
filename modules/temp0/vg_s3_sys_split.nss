// item merge/split spell impact script handler

#include "vg_s0_acs_core_c"
#include "vg_s0_sys_core_c"

void main()
{
    object player = OBJECT_SELF;
    if (gli(player, "split"))SpawnScriptDebugger();

    string splitParam = GetPromptParameter(player);

    // executed via prompt message
    if (splitParam == "SPLIT_AMOUNT")
    {
        string value = GetPromptAnswer(player);
        int amount = stoi(value);

        // invalid, zero or negative number entered
        if (amount <= 0)
        {
            SYS_Info(player, SYSTEM_SYS_PREFIX, "EXCEEDED_MIN");
            return;
        }

        // get the marked item but delete reference to it for next use of the ability
        object mark = glo(player, SYSTEM_SYS_PREFIX + "_MARKED_ITEM", -1, SYS_M_DELETE);
        int maxAmount = ACS_GetItemStackSizeForItem(mark);

        // higher than maximum amount entered
        if (amount >= maxAmount)
        {
            SYS_Info(player, SYSTEM_SYS_PREFIX + "_EXCEEDED_MAX");
            return;
        }

        string tech = GetDescription(mark, FALSE, FALSE);
        int recipe = GetTechPart(tech, TECH_PART_RECIPE_ID);

        // lower marked item's quantity by the specified amount
        ACS_SetWeightedItemQuantity(mark, maxAmount - amount);

        // create new item with the specified amount of quantity
        ACS_CreateItemUsingRecipe(recipe, amount, li(), player, FALSE);

        // notify
        SYS_Info(player, SYSTEM_SYS_PREFIX, "ITEM_SPLIT");
        return;
    }

    object target = GetSpellTargetObject();
    if (!GetIsObjectValid(target) || GetObjectType(target) != OBJECT_TYPE_ITEM)
        return;

    int bWeight = IsWeightBasedItem(target);
    if (!bWeight)
        return;

    object mark = glo(player, SYSTEM_SYS_PREFIX + "_MARKED_ITEM");

    // first time = mark
    if (!GetIsObjectValid(mark))
    {
        slo(player, SYSTEM_SYS_PREFIX + "_MARKED_ITEM", target);
        SYS_Info(player, SYSTEM_SYS_PREFIX, "ITEM_MARKED");
    }

    // second time determine if split/merge
    else
    {
        // marked one item twice = split
        if (mark == target)
            SetPromptScript(player, "vg_s3_sys_split", "SPLIT_AMOUNT", "Enter Item -quantity- in chat message:");

        // marked two different items = merge
        else
        {
            // attempted to merge two different types of items = error
            if (GetBaseItemType(target) != GetBaseItemType(mark))
            {
                SYS_Info(player, SYSTEM_SYS_PREFIX, "MERGE_INCOMPAT");
                return;
            }

            // merge item and notify
            int markQty = ACS_GetItemStackSizeForItem(mark);
            int targetQty = ACS_GetItemStackSizeForItem(target);
            markQty += targetQty;
            ACS_SetWeightedItemQuantity(mark, markQty);
            Destroy(target);
            SYS_Info(player, SYSTEM_SYS_PREFIX, "ITEMS_MERGED");
            slo(player, SYSTEM_SYS_PREFIX + "_MARKED_ITEM", OBJECT_INVALID, -1, SYS_M_DELETE);
        }
    }
}
