// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_list.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-06-2009                                                     |
// | Updated || 27-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "LIST"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



void    msgex(object oPC, string sMessage)
{
    location    loc =   GetLocation(oPC);
    vector      vec =   GetPositionFromLocation(loc);
    object  oExam   =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", Location(GetArea(oPC), Vector(vec.x, vec.y, vec.z - 0.5f), 0.0f));

    SetDescription(oExam, sMessage);
    AssignCommand(oPC, ClearAllActions(TRUE));
    AssignCommand(oPC, ActionExamine(oExam));
    DelayCommand(2.0f, DestroyObject(oExam));
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "LIST";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    int     n           =   1;
    string  sGroup      =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", n);
    string  sText;
    int     bAccess;

    while   (sGroup !=  "")
    {
        bAccess =   CCS_GetHasAccess(oPC, sGroup, FALSE);

        if  (bAccess)
        {
            int     nRow        =   1;
            string  sTemp       =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_CMD", nRow);
            string  sMessage    =   (nRow > 1) ? "\n\n" : "\n" + W2 + "Group [" + G1 + sGroup + W2 + "]\n";
            string  sDesc;
            int     bLog;

            while   (sTemp  !=  "")
            {
                bLog        =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sTemp + "_LOG");
                sDesc       =   gls(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sTemp + "_DESCRIPTION");
                sMessage    +=  W2 + itos(nRow) + "-" + ((bLog) ? R1 : G1) + sTemp + W2 + " - " + sDesc + "\n";

                sTemp   =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP_" + sGroup + "_CMD", ++nRow);
            }

            sText   +=  sMessage;

            //msg(oPC, sMessage, FALSE, FALSE);
        }

        n++;
        sGroup  =   gls(oSystem, SYSTEM_CCS_PREFIX + "_GROUP", n);
    }

    sText   =   C_INTE + "-- List of available commands --" + sText;

    sText   +=  W1 + "<Usage of the commands which have names with " + R1 + "red color" + W1 + " creates a new log entry>";

    msgex(oPC, sText);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
