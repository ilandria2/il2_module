/*
    On MENU ability used:
    - execute script vg_s3_tis_menu
    - start LPD dialog "TIS_MENU"
    - select "Search xxxx" from menu

    On MENU action selected:
    - execute script vg_s2_tis_intera with param "XXXX"
    - if USE_SECONDARY == 0
    -- TIS_PrimaryInteraction(player)
    - else
    -- TIS_DetectObjectsForPC(player);
    -- start LPD dialog "TIS_CORE"


    TIS_CORE dialog schema:

    Root_1 = E2

    Entries (NPC):
    ------------------

    E2:
    - if TIS_OBJECT_O_ROWS < 1 go E11           // nothing found
    - if TIS_OBJECT_O_ROWS = 1 go E4            // actions menu
    - if TIS_OBJECT_O_ROWS > 1 go E3            // objects menu

    E11: Nothing found
    - exit

    E3: Select on of the possible targets:
    - if TIS_OBJECT_O_ROWS >= 1 show R1         // object 1
    - if TIS_OBJECT_O_ROWS >= 2 show R2         // object 2
    - if TIS_OBJECT_O_ROWS >= 3 show R3         // object 3
    - if TIS_OBJECT_O_ROWS >= 4 show R4         // object 4
    - if TIS_OBJECT_O_ROWS >= 5 show R5         // object 5

    E4:
    - if execute vg_s2_tis_check "act" = 1 go E6
    - else go E5

    E5: Your character lost it's target
    - exit

    E6: $CURRENT_OBJECT
    - if TIS_ACTION_1 > 0 show R11
    - if TIS_ACTION_2 > 0 show R12
    - if TIS_ACTION_3 > 0 show R13
    - if TIS_ACTION_4 > 0 show R14
    - if TIS_ACTION_5 > 0 show R15
    - if TIS_ACTION_6 > 0 show R16
    - if TIS_ACTION_7 > 0 show R17
    - if TIS_ACTION_8 > 0 show R18
    - if TIS_ACTION_9 > 0 show R19
    - if TIS_ACTION_10 > 0 show R20

    E7:
    - if execute vg_s2_tis_check "coa" = 1 go E8
    - if TIS_OBJECT_USED = 0 go E2
    - else go E4

    E8:
    - execute vg_s2_tis_action "act"
    - go to E4


    Replies (PC):
    ------------------

    // object 1-5 selected
    R1: $M_O1 - set TIS_OBJECT_USED = 1 and go E4
    R2: $M_O2 - set TIS_OBJECT_USED = 2 and go E4
    R3: $M_O3 - set TIS_OBJECT_USED = 3 and go E4
    R4: $M_O4 - set TIS_OBJECT_USED = 4 and go E4
    R5: $M_O5 - set TIS_OBJECT_USED = 5 and go E4

    // action 1-10 selected
    R11: $M_A1 - set TIS_ACTION_USED = 1 and go E7
    R12: $M_A2 - set TIS_ACTION_USED = 2 and go E7
    R13: $M_A3 - set TIS_ACTION_USED = 3 and go E7
    R14: $M_A4 - set TIS_ACTION_USED = 4 and go E7
    R15: $M_A5 - set TIS_ACTION_USED = 5 and go E7
    R16: $M_A6 - set TIS_ACTION_USED = 6 and go E7
    R17: $M_A7 - set TIS_ACTION_USED = 7 and go E7
    R18: $M_A8 - set TIS_ACTION_USED = 8 and go E7
    R19: $M_A9 - set TIS_ACTION_USED = 9 and go E7
    R20: $M_A10 - set TIS_ACTION_USED = 10 and go E7


*/
