// impact script for the Target ability

#include    "vg_s0_sys_core_c"

void main()
{
    string sScript = GetTargetScript(OBJECT_SELF);

    if (sScript != "")
        ExecuteScript(sScript, OBJECT_SELF);
    else
        SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "READ_DESC");
}
