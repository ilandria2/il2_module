// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Core O                                 |
// | File    || vg_s0_scs_core_o.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu SCS typu "Object"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_const"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || SCS_SummoningCircle                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Polozi vyvolavaci kruh typu nSchool v lokaci lTarget
//
// -----------------------------------------------------------------------------
object  SCS_SummoningCircle(int nSchool, location lTarget);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetFirstSummoningCircle                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1. VK v definovane oblasti lTarget
//
// -----------------------------------------------------------------------------
object  SCS_GetFirstSummoningCircle(int nShape, float fSize, location lTarget, int bLineOfSight = FALSE, int nObjectFilter = OBJECT_TYPE_PLACEABLE, vector vOrigin = [0.0f, 0.0f, 0.0f]);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetNextSummoningCircle                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati dalsi VK v poradi v definovane oblasti lTarget
//
// -----------------------------------------------------------------------------
object  SCS_GetNextSummoningCircle(int nShape, float fSize, location lTarget, int bLineOfSight = FALSE, int nObjectFilter = OBJECT_TYPE_PLACEABLE, vector vOrigin = [0.0f, 0.0f, 0.0f]);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetIsInSummoningCircle                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati 1 pokud se objekt oTarget nachazi v blizkosti VK typu nSchool
//
// -----------------------------------------------------------------------------
int     SCS_GetIsInSummoningCircle(object oTarget, int nSchool);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetSpellCastClass                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati ID povolani ktere bylo pouzito pri sesilani kouzla
//
// -----------------------------------------------------------------------------
int     SCS_GetSpellCastClass();



// +---------++----------------------------------------------------------------+
// | Name    || SCS_GetCasterLevel                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati uroven sesilatele bytosti oCraeture
//
// -----------------------------------------------------------------------------
int     SCS_GetCasterLevel(object oCreature);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                            SCS_SummoningCircle                            |
// +---------------------------------------------------------------------------+



object  SCS_SummoningCircle(int nSchool, location lTarget)
{
    object  oSchool, oResult;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    sll(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_LOCATION", lTarget);
    sli(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", nSchool);
    ExecuteScript(SCS_NCS_PLACE_SUMMONING_CIRCLE, GetModule());

    oSchool     =   glo(oSystem, SYSTEM_SCS_PREFIX + "_CIRCLE_OBJECT", -1, SYS_M_DELETE);
    oResult     =   CreateObject(OBJECT_TYPE_PLACEABLE, "invisobj", lTarget, FALSE, SCS_TAG_SUMMONING_CIRCLE);

    DelayCommand(0.1f, slo(oResult, SYSTEM_SCS_PREFIX + "_CIRCLE_MASTER", OBJECT_SELF));
    DelayCommand(0.1f, slo(oResult, SYSTEM_SCS_PREFIX + "_CIRCLE_OBJECT", oSchool));
    DelayCommand(0.1f, sli(oResult, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL", nSchool));

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                        SCS_GetFirstSummoningCircle                        |
// +---------------------------------------------------------------------------+



object  SCS_GetFirstSummoningCircle(int nShape, float fSize, location lTarget, int bLineOfSight = FALSE, int nObjectFilter = OBJECT_TYPE_PLACEABLE, vector vOrigin = [0.0f, 0.0f, 0.0f])
{
    object  oObject =   GetFirstObjectInShape(nShape, fSize, lTarget, bLineOfSight, nObjectFilter, vOrigin);
    object  oSchool, oResult;

    while   (oObject    !=  OBJECT_INVALID)
    {
        oSchool     =   glo(oObject, SYSTEM_SCS_PREFIX + "_CIRCLE_OBJECT");

        if  (oSchool    !=  OBJECT_INVALID)
        {
            oResult =   oObject;
            break;
        }

        oObject =   GetNextObjectInShape(nShape, fSize, lTarget, bLineOfSight, nObjectFilter, vOrigin);
    }

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                        SCS_GetNextSummoningCircle                         |
// +---------------------------------------------------------------------------+



object  SCS_GetNextSummoningCircle(int nShape, float fSize, location lTarget, int bLineOfSight = FALSE, int nObjectFilter = OBJECT_TYPE_PLACEABLE, vector vOrigin = [0.0f, 0.0f, 0.0f])
{
    object  oObject =   GetNextObjectInShape(nShape, fSize, lTarget, bLineOfSight, nObjectFilter, vOrigin);
    object  oSchool, oResult;

    while   (oObject    !=  OBJECT_INVALID)
    {
        oSchool     =   glo(oObject, SYSTEM_SCS_PREFIX + "_CIRCLE_OBJECT");

        if  (oSchool    !=  OBJECT_INVALID)
        {
            oResult =   oObject;
            break;
        }

        oObject =   GetNextObjectInShape(nShape, fSize, lTarget, bLineOfSight, nObjectFilter, vOrigin);
    }

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                         SCS_GetIsInSummoningCircle                        |
// +---------------------------------------------------------------------------+



int     SCS_GetIsInSummoningCircle(object oTarget, int nSchool)
{
    int     bResult;
    object  oCircle =   SCS_GetFirstSummoningCircle(SHAPE_SPHERE, 5.0f, GetLocation(oTarget));

    while   (GetIsObjectValid(oCircle))
    {
        if  (gli(oCircle, SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL") ==  nSchool)
        {
            bResult =   TRUE;
            break;
        }

        oCircle =   SCS_GetNextSummoningCircle(SHAPE_SPELLCONE, 2.0f, GetLocation(oTarget));
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                           SCS_GetSpellCastClass                           |
// +---------------------------------------------------------------------------+



int     SCS_GetSpellCastClass()
{
    int     nResult, nSpell, nPosition, nClassBit, nClass, nClassLevel;
    string  sClassBit, sMaster;

    if  (GetObjectType(OBJECT_SELF) !=  OBJECT_TYPE_CREATURE)   return  -1;

    nSpell      =   GetSpellId();

    //if  (nSpell ==  SYS_SPELL_TARGET)
    {
        string  sParameter  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS");

        nSpell  =   stoi(GetNthSubString(sParameter, 2));
        nSpell  +=  (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   SCS_SPELL_SUMMONING_CIRCLE  :   0;
    }

    int nSpellGroup =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   -1  :   (nSpell - SCS_FIRST_CONJURE_SPELL) / 800;   //(nSpell - nSpellOrig - SCS_FIRST_CONJURE_SPELL) / 800;
    int nSpellOrig  =   (nSpell <   SCS_FIRST_CONJURE_SPELL)    ?   nSpell  :   nSpell - (SCS_FIRST_CONJURE_SPELL + nSpellGroup * 800); //stoi(Get2DAString("scs_spells_meta", "Original", nSpell));

    nSpell      =   nSpellOrig;
    sMaster     =   Get2DAString("spells", "Master", nSpell);
    nSpell      =   (sMaster    !=  "") ?   StringToInt(sMaster)    :   nSpell;
    nClassBit   =   stoi(Get2DAString("scs_spells", "ClassesBit", nSpell));

    //  1
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");

    nClass      =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", GetClassByPosition(1, OBJECT_SELF));

    if  (nClass & nClassBit)
        nPosition   =   1;

    //  2
    nClass      =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", GetClassByPosition(2, OBJECT_SELF));
    nClassLevel =   GetLevelByPosition(2, OBJECT_SELF);

    if
    (
        (nClass & nClassBit)
    &&  nClassLevel >   GetLevelByPosition(nPosition, OBJECT_SELF)
    )
    {
        nResult     =   nClass;
        nPosition   =   2;
    }

    //  3
    nClass      =   gli(oSystem, SYSTEM_SCS_PREFIX + "_CLASSBIT", GetClassByPosition(3, OBJECT_SELF));
    nClassLevel =   GetLevelByPosition(3, OBJECT_SELF);

    if
    (
        (nClass & nClassBit)
    &&  nClassLevel >   GetLevelByPosition(nPosition, OBJECT_SELF)
    )
    {
        nResult     =   nClass;
        nPosition   =   3;
    }

    nResult =   GetClassByPosition(nPosition, OBJECT_SELF);

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                            SCS_GetCasterLevel                             |
// +---------------------------------------------------------------------------+



int     SCS_GetCasterLevel(object oCreature)
{
    if  (GetIsObjectValid(GetSpellCastItem()))
    return  GetCasterLevel(oCreature);
    return  GetLevelByClass(SCS_GetSpellCastClass(), oCreature);
}
