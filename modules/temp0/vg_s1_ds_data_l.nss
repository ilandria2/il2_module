// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Persistent data load script                  |
// | File    || vg_s1_ds_data_l.nss                                            |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacteni perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_ds_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    Kill(object oKiller, object oVictim)
{
    sli(oVictim, SYSTEM_DS_PREFIX + "_LOGIN_DEATH", TRUE);

    if  (!GetIsObjectValid(oKiller))
    ApplyEffectToObject(DURATION_TYPE_INSTANT, SupernaturalEffect(EffectDeath(FALSE, FALSE)), oVictim);
    else
    AssignCommand(oKiller, ApplyEffectToObject(DURATION_TYPE_INSTANT, SupernaturalEffect(EffectDeath(FALSE, FALSE)), oVictim));
}



void    corpse(object oTarget, location lTarget)
{
    DS_CreateCorpse(oTarget, lTarget);
    DS_JumpToSphere(oTarget, FALSE);
}



void    main()
{
    int     nID     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sParam  =   gls(OBJECT_SELF, SYSTEM_DS_PREFIX + "_DB_LOAD_PARAM", -1, SYS_M_DELETE);
    string  sQuery;

    sParam  =   upper(sParam);

    if  (sParam ==  "LOGIN")
    {
        sQuery  =   "SELECT " +
                        "PlayerID " +
                    "FROM " +
                        "ds_corpses " +
                    "WHERE " +
                        "PlayerID = '" + itos(nID) + "'";

        SQLExecDirect(sQuery);

        if  (SQLFetch() ==  SQL_SUCCESS)
        {
            SetPlotFlag(OBJECT_SELF, FALSE);
            ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDeath(), OBJECT_SELF);
        }
    }

    /*
    string  sQuerry;

    sQuerry =   "SELECT " + DS_DB_COLUMN_CORPSE + "," + DS_DB_COLUMN_KILLER +
                " FROM " + DS_DB_TABLE_CORPSES +
                " WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" + itos(nID) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        location    lData   =   APSStringToLocation(SQLGetData(1));

        //  corpse available - already died
        if  (GetIsObjectValid(GetAreaFromLocation(lData)))
        DelayCommand(5.0f, corpse(OBJECT_SELF, lData));

        //  died but not respawned yet
        else
        {
            sli(OBJECT_SELF, SYSTEM_DS_PREFIX + "_KILLER", stoi(SQLGetData(2)));
            DelayCommand(4.0f, Kill(GetModule(), OBJECT_SELF));
        }
    }
    */
}
