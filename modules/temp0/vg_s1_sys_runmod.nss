// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) RunMode refresh on Resurrect script     |
// | File    || vg_s1_sys_runmod.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Nastavi se mod chuze po oziveni hracske postavy (pri smrti ztrati efekty)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_c"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_O_ON_PLAYER_RESURRECT)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    DelayCommand(1.0f, SetCustomActionMode(oPC, SYS_MODE_RUN));
    DelayCommand(1.1f, SetCustomActionMode(oPC, SYS_MODE_WALK));
}

