// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Resurrection script (database modify)        |
// | File    || vg_s1_ds_resurre.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pri oziveni je nutny smazat zapis v databazi, ktery je tam kvuli ochrane,
    kdyz se postava nalogne po restartu serveru tak je ozivena. To nastaveni
    ma zarucit ze postava bude usmrcena kdyz nastane tenhle stav. Jenze kdyz
    bude ozivena, zapis musi byt smazat, jinak po restartu opet zemre...
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_core_c"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_oes_const"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_O_ON_PLAYER_RESURRECT)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    SYS_Info(oPC, SYSTEM_DS_PREFIX, "DIED");

    /*
    object  oPC =   OBJECT_SELF;

    if  (!GetIsPC(oPC)
    ||  GetIsDM(oPC))   return;

    int     nID =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    string  sSQL;

    sSQL    =   "DELETE FROM " + DS_DB_TABLE_CORPSES + " " +
                "WHERE " + DS_DB_COLUMN_PLAYER_ID + " = '" + itos(nID) + "'";

    SQLExecDirect(sSQL);
    sli(oPC, SYSTEM_DS_PREFIX + "_KILLER", FALSE, -1, SYS_M_DELETE);
    */
}
