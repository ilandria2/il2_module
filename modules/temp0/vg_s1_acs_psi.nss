// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Persistent Statistics Info skript  |
// | File    || vg_s1_acs_psi.nss                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Uklada perzistentni statistiky pro system ACS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_acs_const"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_ACS_PREFIX + "_OBJECT");
    string  sType   =   gls(oPC, SYSTEM_ACS_PREFIX + "_PSI_TYPE");

    //  exchange store
    if  (sType  ==  "XSTORE")
    {
        int     n, PCID, bIndex, XSI, RQTY, RID, RS1, RS2, RS3, RS4, RS5, RS6;
        string  SQL;

        PCID    =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
        bIndex  =   gli(oPC, SYSTEM_ACS_PREFIX + "_PSI_XSTORE_ID");
        RQTY    =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RQTY");
        RID     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RID");
        RS1     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC1");
        RS2     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC2");
        RS3     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC3");
        RS4     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC4");
        RS5     =   gli(oSystem, SYSTEM_ACS_PREFIX + "_SI_" + itos(bIndex) + "_RSPEC5");
        SQL     +=  "(" + itos(PCID) + "," + itos(bIndex) + "," + itos(RID) + "," + itos(RQTY) + "," + itos(RS1) + "," + itos(RS2) + "," + itos(RS3) + "," + itos(RS4) + "," + itos(RS5) + ",default)";

        SQL =   "INSERT INTO psi_xstore VALUES " + SQL;

        SQLExecDirect(SQL);
    }

    //  default
    else
    {

    }
}

