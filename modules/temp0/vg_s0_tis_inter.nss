#include "vg_s0_tis_core_c"
#include "vg_s0_gui_core"
#include "vg_s0_sys_core_c"

// global this of this interaction
struct TIS_Interaction this;

// Checks special conditions
int Check();

// Interaction specific code
void Do();

