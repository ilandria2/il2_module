// +---------++----------------------------------------------------------------+
// | Name    || Object Events System (OES) Constants                           |
// | File    || vg_s0_oes_const.nss                                            |
// | Version || 2.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 26-12-2007                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty pro system OES
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_OES_NAME                     =   "Object Events System";
const string    SYSTEM_OES_PREFIX                   =   "OES";
const string    SYSTEM_OES_VERSION                  =   "2.00";
const string    SYSTEM_OES_AUTHOR                   =   "VirgiL";
const string    SYSTEM_OES_DATE                     =   "26-12-2007";
const string    SYSTEM_OES_UPDATE                   =   "12-05-2009";



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   V A R I A B L E S                      |
// +---------------------------------------------------------------------------+

/*

--+----------------------------------------+------------------------------------
? | name                                   | description                       |
--+----------------------------------------+------------------------------------
s | <SYS>_NCS_SYSINFO_RESREF               | Skript pro posilani sprav od SYS
s |                  _PARAMETERS           | Parametre pro posilani sprav od SYS
s | OES_EVENT_<ID>                         | Nazev udalosti <ID>
--+----------------------------------------+------------------------------------

*/

// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const int       OES_CUSTOM_LOOP_HEARTBEATS          =   10;

const int       OES_EVENT_NUMBER_O_PLAYER_LOGIN     =   1000;
const int       OES_EVENT_NUMBER_O_PLAYER_RELOG     =   1001;

//  events for OBJECT_TYPE_AREA

const int       OES_EVENT_A_ON_ENTER                =   0;
const int       OES_EVENT_A_ON_EXIT                 =   1;
const int       OES_EVENT_A_ON_HEARTBEAT            =   2;
const int       OES_EVENT_A_ON_USER_DEFINED         =   3;

//  events for OBJECT_TYPE_CREATURE

const int       OES_EVENT_C_ON_BLOCKED              =   4;
const int       OES_EVENT_C_ON_COMBAT_ROUND_END     =   5;
const int       OES_EVENT_C_ON_CONVERSATION         =   6;
const int       OES_EVENT_C_ON_DAMAGED              =   7;
const int       OES_EVENT_C_ON_DEATH                =   8;
const int       OES_EVENT_C_ON_DISTURBED            =   9;
const int       OES_EVENT_C_ON_HEARTBEAT            =   10;
const int       OES_EVENT_C_ON_PERCEPTION           =   11;
const int       OES_EVENT_C_ON_PHYSICAL_ATTACKED    =   12;
const int       OES_EVENT_C_ON_RESTED               =   13;
const int       OES_EVENT_C_ON_SPAWN                =   14;
const int       OES_EVENT_C_ON_SPELL_CAST_AT        =   15;
const int       OES_EVENT_C_ON_USER_DEFINED         =   16;

//  events for OBJECT_TYPE_DOOR

const int       OES_EVENT_D_ON_AREA_TRANSITION      =   17;
const int       OES_EVENT_D_ON_CLOSE                =   18;
const int       OES_EVENT_D_ON_DAMAGED              =   19;
const int       OES_EVENT_D_ON_DEATH                =   20;
const int       OES_EVENT_D_ON_FAIL_TO_OPEN         =   21;
const int       OES_EVENT_D_ON_HEARTBEAT            =   22;
const int       OES_EVENT_D_ON_LOCK                 =   23;
const int       OES_EVENT_D_ON_OPEN                 =   24;
const int       OES_EVENT_D_ON_PHYSICAL_ATTACKED    =   25;
const int       OES_EVENT_D_ON_SPELL_CAST_AT        =   26;
const int       OES_EVENT_D_ON_UNLOCK               =   27;
const int       OES_EVENT_D_ON_USER_DEFINED         =   28;

//  events for OBJECT_TYPE_ENCOUNTER

const int       OES_EVENT_E_ON_ENTER                =   29;
const int       OES_EVENT_E_ON_EXHAUSTED            =   30;
const int       OES_EVENT_E_ON_EXIT                 =   31;
const int       OES_EVENT_E_ON_HEARTBEAT            =   32;
const int       OES_EVENT_E_ON_USER_DEFINED         =   33;

//  events for OBJECT_TYPE_MERCHANT

const int       OES_EVENT_M_ON_OPEN_STORE           =   34;
const int       OES_EVENT_M_ON_CLOSE_STORE          =   35;

//  events for OBJECT_TYPE_MODULE

const int       OES_EVENT_O_ON_ACQUIRE_ITEM         =   36;
const int       OES_EVENT_O_ON_ACTIVATE_ITEM        =   37;
const int       OES_EVENT_O_ON_CLIENT_ENTER         =   38;
const int       OES_EVENT_O_ON_CLIENT_LEAVE         =   39;
const int       OES_EVENT_O_ON_PLAYER_LEAVING       =   74;
const int       OES_EVENT_O_ON_CUTSCENE_ABORT       =   40;
const int       OES_EVENT_O_ON_HEARTBEAT            =   41;
const int       OES_EVENT_O_ON_MODULE_LOAD          =   42;
const int       OES_EVENT_O_ON_PLAYER_CHAT          =   43;
const int       OES_EVENT_O_ON_PLAYER_DEATH         =   44;
const int       OES_EVENT_O_ON_PLAYER_DYING         =   45;
const int       OES_EVENT_O_ON_PLAYER_EQUIP_ITEM    =   46;
const int       OES_EVENT_O_ON_PLAYER_LEVEL_UP      =   47;
const int       OES_EVENT_O_ON_PLAYER_RESPAWN       =   48;
const int       OES_EVENT_O_ON_PLAYER_REST          =   49;
const int       OES_EVENT_O_ON_PLAYER_UNEQUIP_ITEM  =   50;
const int       OES_EVENT_O_ON_PLAYER_RESURRECT     =   76;
const int       OES_EVENT_O_ON_UNACQUIRE_ITEM       =   51;
const int       OES_EVENT_O_ON_USER_DEFINED         =   52;
const int       OES_EVENT_O_ON_CUSTOM_LOOP          =   75;

//  events for OBJECT_TYPE_PLACEABLE

const int       OES_EVENT_P_ON_CLICK                =   53;
const int       OES_EVENT_P_ON_CLOSE                =   54;
const int       OES_EVENT_P_ON_DAMAGED              =   55;
const int       OES_EVENT_P_ON_DEATH                =   56;
const int       OES_EVENT_P_ON_DISTURBED            =   57;
const int       OES_EVENT_P_ON_HEARTBEAT            =   58;
const int       OES_EVENT_P_ON_LOCK                 =   59;
const int       OES_EVENT_P_ON_OPEN                 =   60;
const int       OES_EVENT_P_ON_PHYSICAL_ATTACKED    =   61;
const int       OES_EVENT_P_ON_SPELL_CAST_AT        =   62;
const int       OES_EVENT_P_ON_UNLOCK               =   63;
const int       OES_EVENT_P_ON_USED                 =   64;
const int       OES_EVENT_P_ON_USER_DEFINED         =   65;

//  events for OBJECT_TYPE_TRAP

const int       OES_EVENT_T_ON_DISARM               =   66;
const int       OES_EVENT_T_ON_TRAP_TRIGGERED       =   67;

//  events for OBJECT_TYPE_TRIGGER

const int       OES_EVENT_R_ON_AREA_TRANSITION      =   68;
const int       OES_EVENT_R_ON_CLICK                =   69;
const int       OES_EVENT_R_ON_ENTER                =   70;
const int       OES_EVENT_R_ON_EXIT                 =   71;
const int       OES_EVENT_R_ON_HEARTBEAT            =   72;
const int       OES_EVENT_R_ON_USER_DEFINED         =   73;
