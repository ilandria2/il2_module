// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Corpse Destroy script                        |
// | File    || vg_s1_ds_corpsed.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 01-07-2008                                                     |
// | Updated || 30-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Skript ktery odpojenemu hraci znici mrtvolu ve svete
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ds_const"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     i       =   0;
    object  oTarget =   GetObjectByTag(DS_TAG_DEATH_CORPSE, i);

    while   (GetIsObjectValid(oTarget))
    {
        object  oPC     =   glo(oTarget, SYSTEM_DS_PREFIX + "_CORPSE");
        string  sAcc    =   GetPCPlayerName(oPC);

        if  (GetIsObjectValid(oPC)
        &&  sAcc    ==  "")
        {
            SetPlotFlag(oTarget, FALSE);
            AssignCommand(oTarget, SetIsDestroyable(TRUE, FALSE, FALSE));
            DestroyObject(oTarget, 0.1f);
            break;
        }

        oTarget =   GetObjectByTag(DS_TAG_DEATH_CORPSE, ++i);
    }
}
