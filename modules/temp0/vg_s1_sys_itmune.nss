// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Item unequip script                     |
// | File    || vg_s1_sys_itmune.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Smaze promennou "equipped slot"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oItem   =   GetPCItemLastEquipped();

    //  smazani promenne slotu
    sli(oItem, SYSTEM_SYS_PREFIX + "_INV_SLOT", -1, -1, SYS_M_DELETE);
}

