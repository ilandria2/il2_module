// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) Player Judgement script                      |
// | File    || vg_s1_ds_judge.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 27-10-2009                                                     |
// | Updated || 27-10-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Cutscene s LPD rozhovorem hrac <> soudce
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_lpd_core_c"
#include    "inc_draw"



void    ActionCutscene()
{
    object  oPC     =   OBJECT_SELF;
    object  oNPC    =   GetNearestObjectByTag("MN_C_PF_MNBSS06");

    SetCutsceneMode(oPC, TRUE, FALSE);
    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectVisualEffect(VFX_DUR_CUTSCENE_INVISIBILITY), oPC, 8.0f);
    FadeToBlack(oPC, FADE_SPEED_FASTEST);
    DelayCommand(0.2f, SetCameraFacing(GetFacing(oPC) + 1.0f, 20.0f, 0.0f, 0));
    DelayCommand(3.0f, FadeFromBlack(oPC, 0.0001));
    DelayCommand(2.0f, SetCameraFacing(GetFacing(oPC) - 180.0f, 10.0f, 50.0f, 30));
    DelayCommand(8.3f, SetCameraFacing(GetFacing(oPC), 2.0f, 89.0f, 30));
    DelayCommand(1.0f, AssignCommand(oNPC, ClearAllActions(TRUE)));
    DelayCommand(5.0f, AssignCommand(oNPC, ActionCastFakeSpellAtLocation(SPELL_PHANTASMAL_KILLER, GetLocation(oPC), PROJECTILE_PATH_TYPE_HOMING)));
    DelayCommand(6.0f, DrawLineToCenter(DURATION_TYPE_INSTANT, VFX_IMP_DEATH_L, GetLocation(oPC), 10.0f, 0.0f, 0.0f, 10, 0.5f, "x"));
    DelayCommand(6.0f, DrawSpring(DURATION_TYPE_INSTANT, VFX_IMP_DEATH_L, GetLocation(oPC), 0.0f, 6.0f, 0.0f, 8.0f, 0.0f, 30, 14.0f, 3.0f, 0.0f, "z"));
    DelayCommand(7.0f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_FNF_SCREEN_BUMP), GetLocation(oPC)));
    DelayCommand(7.0f, AssignCommand(oPC, ActionPlayAnimation(ANIMATION_LOOPING_DEAD_BACK, 0.5f, 5.0f)));
    DelayCommand(20.0f, StoreCameraFacing());
    DelayCommand(21.0f, SetCutsceneMode(oPC, FALSE, TRUE));
    DelayCommand(21.0f, SetCommandable(FALSE, oPC));
    DelayCommand(22.0f, LPD_StartConversation(oPC, oNPC, OBJECT_INVALID));
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void main()
{
    object  oPC =   GetEnteringObject();

    if  (!GetIsPC(oPC)
    ||  GetIsDM(oPC))   return;

    AssignCommand(GetEnteringObject(), ActionCutscene());
}
