// +---------++----------------------------------------------------------------+
// | Name    || Player Indicator System (PIS) Set Dynamic names script         |
// | File    || vg_s1_pis_names.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Nastavi dynamicky indikator pro hracske postavy kdyz se hrac pripoji
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_oes_const"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    names(object oPC)
{
    InitPlayerNameList(oPC);
    SetNamesEnabled(oPC, TRUE);

    object  oPlayer =   GetFirstPC();

    //  staticke jmeno v player listu
    while   (GetIsObjectValid(oPlayer))
    {
        sls(oPlayer, SYSTEM_PIS_PREFIX + "_NAME", GetName(oPlayer));
        DelayCommand(0.1f, SetDynamicName(oPC, oPlayer, C_NORM + "Dobrodruh"));
        DelayCommand(0.1f, SetDynamicName(oPlayer, oPC, C_NORM + "Dobrodruh"));
        UpdatePlayerList(oPC);
        UpdatePlayerList(oPlayer);

        oPlayer =   GetNextPC();
    }

    DelayCommand(0.3f, PIS_RefreshIndicator(oPC, OBJECT_INVALID, TRUE));
}



void    main()
{
    int nEvent  =   GetUserDefinedEventNumber();

    if  (nEvent !=  OES_EVENT_NUMBER_O_PLAYER_LOGIN)    return;

    object  oPC =   glo(GetModule(), "USERDEFINED_EVENT_OBJECT");

    DelayCommand(0.1f, names(oPC));
}

