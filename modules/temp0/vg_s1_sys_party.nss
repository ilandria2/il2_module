// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Remove from party script                |
// | File    || vg_s1_sys_party.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Every X heartbeats removes all players from their party groups
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int     nHBs    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_HBS");
    int     nStatus =   gli(oSystem, SYSTEM_SYS_PREFIX + "_PARTY_HB");

    if  (nHBs - nStatus >=  SYS_HB_PARTY_REMOVE)
    {
        object  oPC =   GetFirstPC();

        while   (GetIsObjectValid(oPC))
        {
            RemoveFromParty(oPC);

            oPC =   GetNextPC();
        }

        sli(oSystem, SYSTEM_SYS_PREFIX + "_PARTY_HB", nHBs);
    }
}

