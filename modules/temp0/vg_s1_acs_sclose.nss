// +---------++----------------------------------------------------------------+
// | Name    || Advanced Craft System (ACS) Store Close script                 |
// | File    || vg_s1_acs_sclose.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    store close skript
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_acs_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (GetTag(OBJECT_SELF)    !=  "VG_P_SO_INVENTOR") return;

    object  oItem   =   GetFirstItemInInventory(OBJECT_SELF);

    while   (GetIsObjectValid(oItem))
    {
        SetPlotFlag(oItem, FALSE);
        DestroyObject(oItem, 0.1f);

        oItem   =   GetNextItemInInventory(OBJECT_SELF);
    }

    SetPlotFlag(OBJECT_SELF, FALSE);
    DestroyObject(OBJECT_SELF, 0.15f);
}

