// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Core C                           |
// | File    || vg_s0_ccs_core_c.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 19-08-2008                                                     |
// | Updated || 17-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu zkusenosti (CCS) typu "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_const"
#include    "vg_s0_ccs_core_t"
#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || CCS_PerformChannelChat                                         |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Provede mluvu ve vlastnim kanalu mluvy u hrace oPC
//
// -----------------------------------------------------------------------------
void    CCS_PerformChannelChat(string sString, object oPC = OBJECT_SELF);



// +---------++----------------------------------------------------------------+
// | Name    || CCS_PerformCommand                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Provede prikazovy radek sString pro hrace oPC
//
// -----------------------------------------------------------------------------
void    CCS_PerformCommand(string sString, object oPC = OBJECT_SELF);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                           CCS_PerformChannelChat                          |
// +---------------------------------------------------------------------------+



void    CCS_PerformChannelChat(string sString, object oPC = OBJECT_SELF)
{
    //  neni to hrac
    if  (!GetIsPC(oPC)) return;

    string  sChannel    =   CCS_GetChannel(sString);

    //  neni to kanal mluvy
    if  (sChannel   ==  "") return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     bInit   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CHANNEL_" + sChannel + "_INIT");

    //  kanal mluvy neni inicializovan
    if  (!bInit)
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNELS");
        return;
    }

    string  sText   =   CCS_GetChannelText(sString);

    /*
    //  pouzity kanal mluvy bez vlozeni textu - zobrazeni popisu
    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }
    */

    sls(oPC, SYSTEM_CCS_PREFIX + "_CHANNEL_PARAMETERS", sString);
    SYS_Info(oPC, SYSTEM_CCS_PREFIX, "TALK_CUSTOM_&" + sString + "&");
    ExecuteScript(CCS_CHANNEL_PREFIX + sChannel, oPC);
}



// +---------------------------------------------------------------------------+
// |                             CCS_PerformCommand                            |
// +---------------------------------------------------------------------------+



void    CCS_PerformCommand(string sString, object oPC = OBJECT_SELF)
{
    //  neni to hrac
    if  (!GetIsPC(oPC)) return;

    string  sCmd    =   CCS_GetCommand(sString);

    //  neni to prikaz
    if  (sCmd   ==  "") return;

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int     bInit   =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_INIT");

    //  prikaz neni inicializovan
    if  (!bInit)
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "INVALID_CMD_&" + sCmd + "&");
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "USE_LIST_CMD");
        return;
    }

    int bAccess =   CCS_GetHasAccess(oPC, sCmd, TRUE);

    //  pristup zamitnuty
    if  (!bAccess)
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "INSUFFICIENT_PRIVILEGES_&" + sCmd + "&");
        return;
    }

    //  prikazovy radek nebyl ukoncen oddelovacem parametru - definice prikazu
    if  (right(sString, 1)  !=  CCS_COMMAND_SEPARATOR)
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CMD_INFO_&" + sCmd + "&");
        return;
    }

    //  overeni poctu a typy parametru
    int bParameters =   CCS_ValidateParameters(sString, oPC);

    if  (!bParameters)  return;

    //  v prikazovem radku byl vlozen target
    if  (bParameters    ==  2)
    {
        sls(oPC, SYSTEM_SYS_PREFIX + "_TARGET_PARAMETERS", "&" + GetStringLowerCase(CCS_COMMAND_PREFIX + sCmd) + "&_&" + sString + "&");
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "EXECUTE_TARGET_&" + sString + "&");
    }

    else
    {
        sls(oPC, SYSTEM_CCS_PREFIX + "_COMMAND_PARAMETERS", sString);
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "EXECUTE_NORMAL_&" + sString + "&");
        ExecuteScript(GetStringLowerCase(CCS_COMMAND_PREFIX + sCmd), oPC);
    }
}
