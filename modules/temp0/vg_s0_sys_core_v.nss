// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Core V                                  |
// | File    || vg_s0_sys_core_v.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 09-05-2009                                                     |
// | Updated || 09-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Knihovna systemovych funkci Ilandrie 2 kategorie "Variables"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_const"
#include    "vg_s0_sys_core_m"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || sli                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce s manipulaci s lokalni promennou typu 'int'
//
// -----------------------------------------------------------------------------
void    sli(object oObject, string sVarName, int nValue, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || slf                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce s manipulaci s lokalni promennou typu 'float'
//
// -----------------------------------------------------------------------------
void    slf(object oObject, string sVarName, float fValue, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || sls                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce s manipulaci s lokalni promennou typu 'string'
//
// -----------------------------------------------------------------------------
void    sls(object oObject, string sVarName, string sValue, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || slo                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce s manipulaci s lokalni promennou typu 'object'
//
// -----------------------------------------------------------------------------
void    slo(object oObject, string sVarName, object oValue, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || sll                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni funkce s manipulaci s lokalni promennou typu 'location'
//
// -----------------------------------------------------------------------------
void    sll(object oObject, string sVarName, location lValue, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || gli                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni navratova funkce s manipulaci s lokalni promennou typu 'int'
//
// -----------------------------------------------------------------------------
int     gli(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || glf                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni navratova funkce s manipulaci s lokalni promennou typu 'float'
//
// -----------------------------------------------------------------------------
float   glf(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || gls                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni navratova funkce s manipulaci s lokalni promennou typu 'string'
//
// -----------------------------------------------------------------------------
string  gls(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || glo                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni navratova funkce s manipulaci s lokalni promennou typu 'object'
//
// -----------------------------------------------------------------------------
object  glo(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || gll                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Univerzalni navratova funkce s manipulaci s lokalni promennou typu 'location'
//
// -----------------------------------------------------------------------------
location    gll(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET);



// +---------++----------------------------------------------------------------+
// | Name    || SYS_Info                                                       |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Objektu oTarget zobrazi informace systemu sSystem definovane parametrem
//  sParameter
//
// -----------------------------------------------------------------------------
void    SYS_Info(object oTarget, string sSystem = SYSTEM_SYS_PREFIX, string sParameter = "");

// copies local variables from source to target
void CopyVariables(object source, object target);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                                   sli                                     |
// +---------------------------------------------------------------------------+



void    sli(object oObject, string sVarName, int nValue, int nRow = -1, int bType = SYS_M_SET)
{
    int     nNewValue;

    if  (bType  ==  SYS_M_SET)      nNewValue   =   nValue; else
    if  (bType  !=  SYS_M_DELETE)   nNewValue   =   ei(GetLocalInt(oObject, sVarName), nValue, bType);

    //  Radek
    if  (nRow   ==  -1)
    {
        if  (bType  ==  SYS_M_DELETE)   DeleteLocalInt(oObject, sVarName);
        else                            SetLocalInt(oObject, sVarName, nNewValue);
    }

    else
    {
        int     nLastRow    =   GetLocalInt(oObject, sVarName + "_I_ROWS");

        if  (!nLastRow
        &&  nRow    ==  -2
        &&  bType   ==  SYS_M_DELETE)   return;

        int     nNewRow     =   (nRow   ==  -2) ?   nLastRow    :   nRow;

        if  (bType  !=  SYS_M_DELETE)
        {
            nNewRow     +=  (nRow   ==  -2) ?   1   :   0;
            nLastRow    =   (nLastRow   <   nNewRow)    ?   nNewRow :   nLastRow;

            SetLocalInt(oObject, sVarName + "_I_ROWS", nLastRow);
            SetLocalInt(oObject, sVarName + "_" + IntToString(nNewRow), nNewValue);
        }

        else
        {
            DeleteLocalInt(oObject, sVarName + "_" + IntToString(nNewRow));
            SetLocalInt(oObject, sVarName + "_I_ROWS", nLastRow - 1);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                                   slf                                     |
// +---------------------------------------------------------------------------+



void    slf(object oObject, string sVarName, float fValue, int nRow = -1, int bType = SYS_M_SET)
{
    float   fNewValue;

    if  (bType  ==  SYS_M_SET)      fNewValue   =   fValue; else
    if  (bType  !=  SYS_M_DELETE)   fNewValue   =   ef(GetLocalFloat(oObject, sVarName), fValue, bType);

    //  Radek
    if  (nRow   ==  -1)
    {
        if  (bType  ==  SYS_M_DELETE)   DeleteLocalFloat(oObject, sVarName);
        else                            SetLocalFloat(oObject, sVarName, fNewValue);
    }

    else
    {
        int     nLastRow    =   GetLocalInt(oObject, sVarName + "_F_ROWS");

        if  (!nLastRow
        &&  nRow    ==  -2
        &&  bType   ==  SYS_M_DELETE)   return;

        int     nNewRow     =   (nRow   ==  -2) ?   nLastRow    :   nRow;

        if  (bType  !=  SYS_M_DELETE)
        {
            nNewRow     +=  (nRow   ==  -2) ?   1   :   0;
            nLastRow    =   (nLastRow   <   nNewRow)    ?   nNewRow :   nLastRow;

            SetLocalInt(oObject, sVarName + "_F_ROWS", nLastRow);
            SetLocalFloat(oObject, sVarName + "_" + IntToString(nNewRow), fNewValue);
        }

        else
        {
            DeleteLocalFloat(oObject, sVarName + "_" + IntToString(nNewRow));
            SetLocalInt(oObject, sVarName + "_F_ROWS", nLastRow - 1);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                                   sls                                     |
// +---------------------------------------------------------------------------+



void    sls(object oObject, string sVarName, string sValue, int nRow = -1, int bType = SYS_M_SET)
{
    string  sNewValue   =   sValue;

    //  Radek
    if  (nRow   ==  -1)
    {
        if  (bType  ==  SYS_M_DELETE)   DeleteLocalString(oObject, sVarName);
        else                            SetLocalString(oObject, sVarName, sNewValue);
    }

    else
    {
        int     nLastRow    =   GetLocalInt(oObject, sVarName + "_S_ROWS");

        if  (!nLastRow
        &&  nRow    ==  -2
        &&  bType   ==  SYS_M_DELETE)   return;

        int     nNewRow     =   (nRow   ==  -2) ?   nLastRow    :   nRow;

        if  (bType  !=  SYS_M_DELETE)
        {
            nNewRow     +=  (nRow   ==  -2) ?   1   :   0;
            nLastRow    =   (nLastRow   <   nNewRow)    ?   nNewRow :   nLastRow;

            SetLocalInt(oObject, sVarName + "_S_ROWS", nLastRow);
            SetLocalString(oObject, sVarName + "_" + IntToString(nNewRow), sNewValue);
        }

        else
        {
            DeleteLocalString(oObject, sVarName + "_" + IntToString(nNewRow));
            SetLocalInt(oObject, sVarName + "_S_ROWS", nLastRow - 1);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                                   slo                                     |
// +---------------------------------------------------------------------------+



void    slo(object oObject, string sVarName, object oValue, int nRow = -1, int bType = SYS_M_SET)
{
    object  oNewValue   =   oValue;

    //  Radek
    if  (nRow   ==  -1)
    {
        if  (bType  ==  SYS_M_DELETE)   DeleteLocalObject(oObject, sVarName);
        else                            SetLocalObject(oObject, sVarName, oNewValue);
    }

    else
    {
        int     nLastRow    =   GetLocalInt(oObject, sVarName + "_O_ROWS");

        if  (!nLastRow
        &&  nRow    ==  -2
        &&  bType   ==  SYS_M_DELETE)   return;

        int     nNewRow     =   (nRow   ==  -2) ?   nLastRow    :   nRow;

        if  (bType  !=  SYS_M_DELETE)
        {
            nNewRow     +=  (nRow   ==  -2) ?   1   :   0;
            nLastRow    =   (nLastRow   <   nNewRow)    ?   nNewRow :   nLastRow;

            SetLocalInt(oObject, sVarName + "_O_ROWS", nLastRow);
            SetLocalObject(oObject, sVarName + "_" + IntToString(nNewRow), oNewValue);
        }

        else
        {
            DeleteLocalObject(oObject, sVarName + "_" + IntToString(nNewRow));
            SetLocalInt(oObject, sVarName + "_O_ROWS", nLastRow - 1);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                                   sll                                     |
// +---------------------------------------------------------------------------+



void    sll(object oObject, string sVarName, location lValue, int nRow = -1, int bType = SYS_M_SET)
{
    location    lNewValue   =   lValue;

    //  Radek
    if  (nRow   ==  -1)
    {
        if  (bType  ==  SYS_M_DELETE)   DeleteLocalLocation(oObject, sVarName);
        else                            SetLocalLocation(oObject, sVarName, lNewValue);
    }

    else
    {
        int     nLastRow    =   GetLocalInt(oObject, sVarName + "_L_ROWS");

        if  (!nLastRow
        &&  nRow    ==  -2
        &&  bType   ==  SYS_M_DELETE)   return;

        int     nNewRow     =   (nRow   ==  -2) ?   nLastRow    :   nRow;

        if  (bType  !=  SYS_M_DELETE)
        {
            nNewRow     +=  (nRow   ==  -2) ?   1   :   0;
            nLastRow    =   (nLastRow   <   nNewRow)    ?   nNewRow :   nLastRow;

            SetLocalInt(oObject, sVarName + "_L_ROWS", nLastRow);
            SetLocalLocation(oObject, sVarName + "_" + IntToString(nNewRow), lNewValue);
        }

        else
        {
            DeleteLocalLocation(oObject, sVarName + "_" + IntToString(nNewRow));
            SetLocalInt(oObject, sVarName + "_L_ROWS", nLastRow - 1);
        }
    }
}



// +---------------------------------------------------------------------------+
// |                                   gli                                     |
// +---------------------------------------------------------------------------+



int     gli(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET)
{
    int     nResult, nRetRow;

    nRetRow     =   (nRow   ==  -2)     ?   GetLocalInt(oObject, sVarName + "_I_ROWS")  :   nRow;
    sVarName    +=  (nRetRow    !=  -1) ?   "_" + IntToString(nRetRow)  :   "";
    nResult     =   GetLocalInt(oObject, sVarName);

    if  (bType  ==  SYS_M_DELETE)   DeleteLocalInt(oObject, sVarName);

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                   glf                                     |
// +---------------------------------------------------------------------------+



float   glf(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET)
{
    float   fResult;
    int     nRetRow;

    nRetRow     =   (nRow   ==  -2)     ?   GetLocalInt(oObject, sVarName + "_F_ROWS")  :   nRow;
    sVarName    +=  (nRetRow    !=  -1) ?   "_" + IntToString(nRetRow)  :   "";
    fResult     =   GetLocalFloat(oObject, sVarName);

    if  (bType  ==  SYS_M_DELETE)   DeleteLocalFloat(oObject, sVarName);

    return  fResult;
}



// +---------------------------------------------------------------------------+
// |                                   gls                                     |
// +---------------------------------------------------------------------------+



string  gls(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET)
{
    string  sResult;
    int     nRetRow;

    nRetRow     =   (nRow   ==  -2)     ?   GetLocalInt(oObject, sVarName + "_S_ROWS")  :   nRow;
    sVarName    +=  (nRetRow    !=  -1) ?   "_" + IntToString(nRetRow)  :   "";
    sResult     =   GetLocalString(oObject, sVarName);

    if  (bType  ==  SYS_M_DELETE)   DeleteLocalString(oObject, sVarName);

    return  sResult;
}



// +---------------------------------------------------------------------------+
// |                                   glo                                     |
// +---------------------------------------------------------------------------+



object  glo(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET)
{
    object  oResult;
    int     nRetRow;

    nRetRow     =   (nRow   ==  -2) ?   GetLocalInt(oObject, sVarName + "_O_ROWS")  :   nRow;
    sVarName    +=  (nRetRow    !=  -1) ?   "_" + IntToString(nRetRow)  :   "";
    oResult     =   GetLocalObject(oObject, sVarName);

    if  (bType  ==  SYS_M_DELETE)   DeleteLocalObject(oObject, sVarName);

    return  oResult;
}



// +---------------------------------------------------------------------------+
// |                                   gll                                     |
// +---------------------------------------------------------------------------+



location    gll(object oObject, string sVarName, int nRow = -1, int bType = SYS_M_SET)
{
    location    lResult;
    int     nRetRow;

    nRetRow     =   (nRow   ==  -2) ?   GetLocalInt(oObject, sVarName + "_L_ROWS")  :   nRow;
    sVarName    +=  (nRetRow    !=  -1) ?   "_" + IntToString(nRetRow)  :   "";
    lResult     =   GetLocalLocation(oObject, sVarName);

    if  (bType  ==  SYS_M_DELETE)   DeleteLocalLocation(oObject, sVarName);

    return  lResult;
}



// +---------------------------------------------------------------------------+
// |                                 SYS_Info                                  |
// +---------------------------------------------------------------------------+



void    SYS_Info(object oTarget, string sSystem = SYSTEM_SYS_PREFIX, string sParameter = "")
{
    SetLocalString(oTarget, sSystem + "_NCS_SYSINFO_PARAMETERS", sParameter);
    ExecuteScript(GetLocalString(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_NCS_SYSINFO_RESREF"), oTarget);
}

void CopyVariables(object source, object target)
{
    slo(source, SYSTEM_SYS_PREFIX + "_COPY_VARS_TARGET", target);
    ExecuteScript("vg_s1_sys_copyva", source);
}

