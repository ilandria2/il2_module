// +---------++----------------------------------------------------------------+
// | Name    || BattleCraftSystem (BCS) Learn weapon XP script                 |
// | File    || vg_s1_bcs_learwp.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-01-2008                                                     |
// | Updated || 18-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Prepocte a upravi absorbacni zkusenosti hrace kterej je v boji s nejakym
    objektem (fyzicky utok)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_xps_core_m"
#include    "vg_s0_bcs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oTarget =   glo(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_HITS_OBJECT", -1, SYS_M_DELETE);
    string  sTag    =   (GetIsPC(oTarget))  ?   GetPCPlayerName(oTarget)    :   GetTag(oTarget);
    int     nRow    =   1;
    int     nClass  =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS", nRow, SYS_M_DELETE);
    int     nHits, nGainedXP, nTotal;

    while   (nClass >   0)
    {
        nHits       =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_CLASS", nClass, SYS_M_DELETE);
        nGainedXP   =   XPS_GetGainedXPFromPhysicalAttack(GetHitDice(OBJECT_SELF), GetHitDice(oTarget), GetChallengeRating(oTarget));
        nGainedXP   +=  (nHits  >   0)  ?   (1 + Random(nHits)) * (1 + Random(10))  :   0;
        nTotal      +=  nHits;

        XPS_AlterAbsorbXP(OBJECT_SELF, nClass, nGainedXP, FALSE);
        XPS_AlterAbsorbXP(OBJECT_SELF, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nGainedXP / 2);

        nClass  =   gli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS", ++nRow, SYS_M_DELETE);
    }

    sli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_CLASS_I_ROWS", -1, -1, SYS_M_DELETE);
    sli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_" + sTag + "_HITS_I_ROWS", -1, -1, SYS_M_DELETE);
    sli(OBJECT_SELF, SYSTEM_BCS_PREFIX + "_PHYSICAL_ATTACKS", nTotal, -1, SYS_M_INCREMENT);
}
