// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialog System (LPD) Area change                  |
// | File    || vg_s1_lpd_arechn.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Pri zmene oblasti se prerusi LPD rozhovor hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetEnteringObject();

    LPD_EndConversation(oPC, TRUE);
}
