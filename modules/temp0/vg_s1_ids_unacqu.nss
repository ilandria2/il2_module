// handles item unacquired module event to make items replaced by interactive TIS objects and become persistent until
// they are destroyed or picked up by someone else

#include "vg_s0_ids_core_c"

void main()
{
    object creature = GetModuleItemLostBy();
    object item = GetModuleItemLost();

    // for some reason we lost an item - was it destroed maybe? in such case we don't care about it
    if (!GetIsObjectValid(item))
        return;

    IDS_ItemToInteractiveObject(item);
}
