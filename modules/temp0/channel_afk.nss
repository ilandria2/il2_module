// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_afk.nss                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "AFK"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "AFK";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    sli(oPC, SYSTEM_PIS_PREFIX + "_AFK", !gli(oPC, SYSTEM_PIS_PREFIX + "_AFK"));
    PIS_RefreshIndicator(oPC, oPC, TRUE);
    SYS_Info(oPC, SYSTEM_PIS_PREFIX, "AFK");

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
