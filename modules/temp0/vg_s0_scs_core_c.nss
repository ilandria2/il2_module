// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Core C                                 |
// | File    || vg_s0_scs_core_c.nss                                           |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 20-03-2008                                                     |
// | Updated || 13-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Jadro systemu SCS typu "Command"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_scs_const"
#include    "vg_s0_scs_core_o"
#include    "vg_s0_scs_core_m"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || SCS_OpenSummoningCircle                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Otevre vyvolavaci kruh typu nSchool v miste lTarget
//
// -----------------------------------------------------------------------------
void    SCS_OpenSummoningCircle(int nSchool, location lTarget);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CloseSummoningCircle                                       |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zavre vyvolavaci kruh oCircle
//
// -----------------------------------------------------------------------------
void    SCS_CloseSummoningCircle(object oCircle);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_RegenerateManaSequence                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Sekvence pro regeneraci many hrace oPC
//
// -----------------------------------------------------------------------------
void    SCS_RegenerateManaSequence(object oPC);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CastSpellAtObject                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Sesle kouzlo nSpell na objekt oTarget v metamagii nMeta
//
// -----------------------------------------------------------------------------
void    SCS_CastSpellAtObject(int nSpell, object oTarget, int nMeta = METAMAGIC_NONE, int nPath = PROJECTILE_PATH_TYPE_DEFAULT, int bInstant = FALSE);



// +---------++----------------------------------------------------------------+
// | Name    || SCS_CastSpellAtLocation                                        |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Sesle kouzlo nSpell na lokaci lTarget v metamagii nMeta
//
// -----------------------------------------------------------------------------
void    SCS_CastSpellAtLocation(int nSpell, location lTarget, int nMeta = METAMAGIC_NONE, int nPath = PROJECTILE_PATH_TYPE_DEFAULT, int bInstant = FALSE);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                        SCS_OpenSummoningCircle                            |
// +---------------------------------------------------------------------------+



void    SCS_OpenSummoningCircle(int nSchool, location lTarget)
{
    SCS_SummoningCircle(nSchool, lTarget);
}



// +---------------------------------------------------------------------------+
// |                         SCS_CloseSummoningCircle                          |
// +---------------------------------------------------------------------------+



void    SCS_CloseSummoningCircle(object oCircle)
{
    object  oPart   =   glo(oCircle, SYSTEM_SCS_PREFIX + "_CIRCLE_OBJECT");

    ExecuteScript(SCS_NCS_DESTROY_SUMMONING_CIRCLE, oPart);
    SetPlotFlag(oCircle, FALSE);
    DestroyObject(oCircle);
}



// +---------------------------------------------------------------------------+
// |                        SCS_RegenerateManaSequence                         |
// +---------------------------------------------------------------------------+



void    SCS_RegenerateManaSequence(object oPC)
{
    if  (!GetIsObjectValid(oPC))    return;
    if  (GetIsDead(oPC))            return;

    int     nMana, nManaMax, nManaReg;
    string  sMessage;

    nMana       =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA");
    nManaMax    =   gli(oPC, SYSTEM_SCS_PREFIX + "_MANA_MAX");
    nManaReg    =   SCS_CalculateRegeneratedMana(oPC);

    if  ((nMana + nManaReg) >   nManaMax)
    {
        if  (nMana  <   nManaMax)
        {
            sli(oPC, SYSTEM_SCS_PREFIX + "_MANA", nManaMax);
            SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&" + itos(nMana) + "&_&" + itos(nManaMax) + "&");
        }
    }

    else

    if  (nManaReg   >   0)
    {
        if  ((nMana + nManaReg) <=  nManaMax)
        {
            sli(oPC, SYSTEM_SCS_PREFIX + "_MANA", nManaReg, -1, SYS_M_INCREMENT);
            SYS_Info(oPC, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&" + itos(nMana) + "&_&" + itos(nMana + nManaReg) + "&");
        }
    }
}



// +---------------------------------------------------------------------------+
// |                          SCS_CastSpellAtObject                            |
// +---------------------------------------------------------------------------+



void    SCS_CastSpellAtObject(int nSpell, object oTarget, int nMeta = METAMAGIC_NONE, int nPath = PROJECTILE_PATH_TYPE_DEFAULT, int bInstant = FALSE)
{   //SpawnScriptDebugger();
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int nSpellGroup =   gli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 1 + nMeta);
    int nSpellCast  =   SCS_FIRST_CONJURE_SPELL + 800 * nSpellGroup + nSpell;

    ActionCastSpellAtObject(nSpellCast, oTarget, METAMAGIC_NONE, TRUE, -1, nPath, bInstant);
}



// +---------------------------------------------------------------------------+
// |                         SCS_CastSpellAtLocation                           |
// +---------------------------------------------------------------------------+



void    SCS_CastSpellAtLocation(int nSpell, location lTarget, int nMeta = METAMAGIC_NONE, int nPath = PROJECTILE_PATH_TYPE_DEFAULT, int bInstant = FALSE)
{
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT");
    int nSpellGroup =   gli(oSystem, SYSTEM_SCS_PREFIX + "_SPELL_GROUP", 1 + nMeta);
    int nSpellCast  =   SCS_FIRST_CONJURE_SPELL + 800 * nSpellGroup + nSpell;

    ActionCastSpellAtLocation(nSpellCast, lTarget, METAMAGIC_NONE, TRUE, nPath, bInstant);
}
