// +---------++----------------------------------------------------------------+
// | Name    || Color tokens                                                   |
// | File    || vg_s0_sys_colors.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 09-05-2009                                                     |
// | Updated || 09-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Zakladni tokeny barev
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    _C                                  =   "</c>";
const string    _R0                                 =   "</c><c�00>";
const string    _R1                                 =   "</c><c�00>";
const string    _R2                                 =   "</c><c���>";
const string    _G0                                 =   "</c><c0�0>";
const string    _G1                                 =   "</c><c0�0>";
const string    _G2                                 =   "</c><c���>";
const string    _Y0                                 =   "</c><c��0>";
const string    _Y1                                 =   "</c><c��0>";
const string    _Y2                                 =   "</c><c���>";
const string    _B0                                 =   "</c><c00�>";
const string    _B1                                 =   "</c><c00�>";
const string    _B2                                 =   "</c><c0��>";
const string    _C0                                 =   "</c><c0��>";
const string    _C1                                 =   "</c><c0��>";
const string    _C2                                 =   "</c><c���>";
const string    _W0                                 =   "</c><c000>";
const string    _W1                                 =   "</c><c���>";
const string    _W2                                 =   "</c><c���>";
const string    _P0                                 =   "</c><c�0�>";
const string    _P1                                 =   "</c><c�0�>";
const string    _P2                                 =   "</c><c���>";
const string    _O0                                 =   "</c><c�@0>";
const string    _O1                                 =   "</c><c��0>";
const string    _O2                                 =   "</c><c���>";
const string    _V0                                 =   "</c><c@0�>";
const string    _V1                                 =   "</c><c�0�>";
const string    _V2                                 =   "</c><c���>";

const string    R0                                  =   "<c�00>";
const string    R1                                  =   "<c�00>";
const string    R2                                  =   "<c���>";
const string    G0                                  =   "<c0�0>";
const string    G1                                  =   "<c0�0>";
const string    G2                                  =   "<c���>";
const string    Y0                                  =   "<c��0>";
const string    Y1                                  =   "<c��0>";
const string    Y2                                  =   "<c���>";
const string    B0                                  =   "<c00�>";
const string    B1                                  =   "<c00�>";
const string    B2                                  =   "<c0��>";
const string    C0                                  =   "<c0��>";
const string    C1                                  =   "<c0��>";
const string    C2                                  =   "<c���>";
const string    W0                                  =   "<c000>";
const string    W1                                  =   "<c���>";
const string    W2                                  =   "<c���>";
const string    P0                                  =   "<c�0�>";
const string    P1                                  =   "<c�0�>";
const string    P2                                  =   "<c���>";
const string    O0                                  =   "<c�@0>";
const string    O1                                  =   "<c��0>";
const string    O2                                  =   "<c���>";
const string    V0                                  =   "<c@0�>";
const string    V1                                  =   "<c�0�>";
const string    V2                                  =   "<c���>";

const string    C_NORM                              =   "<c���>";
const string    C_BOLD                              =   "<c���>";
const string    C_POSI                              =   "<c0�0>";
const string    C_NEGA                              =   "<c�00>";
const string    C_INTE                              =   "<c��0>";
