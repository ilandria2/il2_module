// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) Persistent data save script                   |
// | File    || vg_s1_acs_data_s.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Ulozi perzistentni data pro hrace systemu ACS
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_acs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    if  (!GetIsObjectValid(OBJECT_SELF))    return;

    string  sParam  =   gls(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_DB_SAVE_PARAM", -1, SYS_M_DELETE);
    string  sQuerry;

    if  (sParam ==  "COMPLETE_CARGO")
    {
        int bCargo  =   gli(OBJECT_SELF, SYSTEM_ACS_PREFIX + "_COMPLETE_CARGO", -1, SYS_M_DELETE);

        sQuerry =   "UPDATE cargo_complete SET Completed = 1, date = NULL WHERE CargoID = " + itos(bCargo);

        SQLExecDirect(sQuerry);
    }
}
