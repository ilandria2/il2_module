// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Portal enter script              |
// | File    || vg_s1_tis_portal.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Hrac, ktery vstoupi do nastaveny spouste pro "Portal" se teleportuje s
    prislusnyma efektama do urcene destinace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_lpd_core_c"
#include    "vg_s0_tis_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetEnteringObject();

    //  portal neaktivni
    int bActive =   gli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_ACTIVE");

    if  (!bActive)  return;

    if  (gli(oPC, "tis_portal"))SpawnScriptDebugger();

    //  pokud je nastaveny LPD dialog, pak se pusti - v nem se musi rozhodnout
    //  co se stane dal
    //  v pripade dialogu vstupujici bytost musi byt hrac, ktery neni v zadnem
    //  jinem rozhovoru (poruseni = nic se nestane)
    string  sDialog =   gls(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_DIALOG");

    if  (sDialog    !=  ""
    &&  GetIsPC(oPC))
    {
        object  oDialog =   GetObjectByTag(sDialog);

        if  (!GetIsObjectValid(oDialog))            return;
        if  (GetIsObjectValid(LPD_GetDialog(oPC)))  return;

        LPD_StartConversation(oPC, oPC, oDialog);
        return;
    }

    //  jenom pro hrace
    int bPCOnly =   gli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_PCONLY");

    if  (bPCOnly    &&  !GetIsPC(oPC)) return;

    //  jenom pro spouste, ktere maji nastavenou promennou "TIS_PORTAL" na TRUE
    int bPortal =   gli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_PORTAL");

    if  (!bPortal)  return;

    //  bytost prave dosahla destinaci
    if  (gli(oPC, SYSTEM_TIS_PREFIX + "_PORTING", -1, SYS_M_DELETE))  return;

    string  sDest   =   gls(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_TARGET");
    object  oWP     =   GetWaypointByTag(sDest);
    string  sMessage;

    //  destinace nepristupna
    if  (!GetIsObjectValid(oWP))
    {
        sMessage    =   C_NORM + "Vstupuje� do port�lu ale " + C_BOLD + "nic se ned�je";

        msg(oPC, sMessage, TRUE, FALSE);
    }

    //  portal
    else
    {
        sMessage    =   ConvertTokens("*Vstupuje do port�lu*", oPC);

        AssignCommand(oPC, ClearAllActions(TRUE));
        SetCommandable(FALSE, oPC);
        sli(oPC, SYSTEM_TIS_PREFIX + "_PORTING", TRUE);
        AssignCommand(oPC, SpeakString(sMessage));
        ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oPC));
        DelayCommand(2.9f, SetCommandable(TRUE, oPC));
        DelayCommand(3.0f, AssignCommand(oPC, JumpToLocation(GetLocation(oWP))));
        DelayCommand(4.5f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_UNSUMMON), GetLocation(oWP)));
    }
}

