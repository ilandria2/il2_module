// +---------++----------------------------------------------------------------+
// | Name    || Experience System (XPS) System init                            |
// | File    || vg_s5_xps_init.nss                                             |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 06-06-2009                                                     |
// | Updated || 06-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system XPS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_xps_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_XPS_PREFIX;
    string  sName       =   SYSTEM_XPS_NAME;
    string  sVersion    =   SYSTEM_XPS_VERSION;
    string  sAuthor     =   SYSTEM_XPS_AUTHOR;
    string  sDate       =   SYSTEM_XPS_DATE;
    string  sUpDate     =   SYSTEM_XPS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_xps_info");
    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_xps_data_c");
    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_xps_data_s");
    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_xps_data_l");
    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_xps_hrbeat", -2);

    //  Tridy zkusenosti
    sls(oSystem, sSystem + "_CLASS", "Postava", XPS_M_CLASS_XPS_PLAYER_CHARACTER + 1);
    /*
    sls(oSystem, sSystem + "_CLASS", "Vym�t�n�", XPS_M_CLASS_SCS_ABJURATION + 1);
    sls(oSystem, sSystem + "_CLASS", "Vyvol�n�", XPS_M_CLASS_SCS_CONJURATION + 1);
    sls(oSystem, sSystem + "_CLASS", "V�t�n�", XPS_M_CLASS_SCS_DIVINATION + 1);
    sls(oSystem, sSystem + "_CLASS", "O�arov�n�", XPS_M_CLASS_SCS_ENCHANTMENT + 1);
    sls(oSystem, sSystem + "_CLASS", "Vz�v�n�", XPS_M_CLASS_SCS_EVOCATION + 1);
    sls(oSystem, sSystem + "_CLASS", "Iluze", XPS_M_CLASS_SCS_ILLUSION + 1);
    sls(oSystem, sSystem + "_CLASS", "Nekromancie", XPS_M_CLASS_SCS_NECROMANCY + 1);
    sls(oSystem, sSystem + "_CLASS", "Prom�ny", XPS_M_CLASS_SCS_TRANSMUTATION + 1);
    sls(oSystem, sSystem + "_CLASS", "Neozbrojen� boj", XPS_M_CLASS_BCS_UNARMED + 1);
    sls(oSystem, sSystem + "_CLASS", "Kr�tk� �epel", XPS_M_CLASS_BCS_SHORT_BLADED + 1);
    sls(oSystem, sSystem + "_CLASS", "Dlouh� �epel", XPS_M_CLASS_BCS_LONG_BLADED + 1);
    sls(oSystem, sSystem + "_CLASS", "D�evcov�", XPS_M_CLASS_BCS_POLEARM + 1);
    sls(oSystem, sSystem + "_CLASS", "Bi�", XPS_M_CLASS_BCS_WHIP + 1);
    sls(oSystem, sSystem + "_CLASS", "Palc�t", XPS_M_CLASS_BCS_MACE + 1);
    sls(oSystem, sSystem + "_CLASS", "Oboustrann�", XPS_M_CLASS_BCS_DOUBLE_SIDED + 1);
    sls(oSystem, sSystem + "_CLASS", "Sekyra", XPS_M_CLASS_BCS_AXE + 1);
    sls(oSystem, sSystem + "_CLASS", "St�eln�", XPS_M_CLASS_BCS_RANGED + 1);
    sls(oSystem, sSystem + "_CLASS", "�et�z", XPS_M_CLASS_BCS_FLAIL + 1);
    sls(oSystem, sSystem + "_CLASS", "Vrhac�", XPS_M_CLASS_BCS_THROWING + 1);

    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_LONG_BLADED, BASE_ITEM_BASTARDSWORD);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_AXE, BASE_ITEM_BATTLEAXE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_UNARMED, BASE_ITEM_CBLUDGWEAPON);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_MACE, BASE_ITEM_CLUB);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_UNARMED, BASE_ITEM_CPIERCWEAPON);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_UNARMED, BASE_ITEM_CSLASHWEAPON);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_UNARMED, BASE_ITEM_CSLSHPRCWEAP);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_SHORT_BLADED, BASE_ITEM_DAGGER);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_THROWING, BASE_ITEM_DART);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_DOUBLE_SIDED, BASE_ITEM_DIREMACE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_DOUBLE_SIDED, BASE_ITEM_DOUBLEAXE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_AXE, BASE_ITEM_DWARVENWARAXE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_UNARMED, BASE_ITEM_GLOVES);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_AXE, BASE_ITEM_GREATAXE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_LONG_BLADED, BASE_ITEM_GREATSWORD);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_THROWING, BASE_ITEM_GRENADE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_POLEARM, BASE_ITEM_HALBERD);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_AXE, BASE_ITEM_HANDAXE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_RANGED, BASE_ITEM_HEAVYCROSSBOW);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_FLAIL, BASE_ITEM_HEAVYFLAIL);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_SHORT_BLADED, BASE_ITEM_KAMA);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_LONG_BLADED, BASE_ITEM_KATANA);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_SHORT_BLADED, BASE_ITEM_KUKRI);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_RANGED, BASE_ITEM_LIGHTCROSSBOW);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_FLAIL, BASE_ITEM_LIGHTFLAIL);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_MACE, BASE_ITEM_LIGHTHAMMER);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_MACE, BASE_ITEM_LIGHTMACE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_RANGED, BASE_ITEM_LONGBOW);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_LONG_BLADED, BASE_ITEM_LONGSWORD);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_MACE, BASE_ITEM_MAGICWAND);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_FLAIL, BASE_ITEM_MORNINGSTAR);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_DOUBLE_SIDED, BASE_ITEM_QUARTERSTAFF);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_LONG_BLADED, BASE_ITEM_RAPIER);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_LONG_BLADED, BASE_ITEM_SCIMITAR);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_POLEARM, BASE_ITEM_SCYTHE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_RANGED, BASE_ITEM_SHORTBOW);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_POLEARM, BASE_ITEM_SHORTSPEAR);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_SHORT_BLADED, BASE_ITEM_SHORTSWORD);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_THROWING, BASE_ITEM_SHURIKEN);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_SHORT_BLADED, BASE_ITEM_SICKLE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_RANGED, BASE_ITEM_SLING);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_THROWING, BASE_ITEM_THROWINGAXE);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_POLEARM, BASE_ITEM_TRIDENT);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_DOUBLE_SIDED, BASE_ITEM_TWOBLADEDSWORD);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_MACE, BASE_ITEM_WARHAMMER);
    sli(oSystem, sSystem + "_BASEITEM_CLASS", XPS_M_CLASS_BCS_WHIP, BASE_ITEM_WHIP);
    */

    //  Hranice zkusenosti urovni vsech trid
    //  Zkusenosti postavy
    int     nRow    =   0;
    string  s2da    =   Get2DAString("exptable", "XP", nRow);

    while   (s2da   !=  "")
    {
        sli(oSystem, SYSTEM_XPS_PREFIX + "_CLASS_PC_LEVEL", stoi(s2da), nRow + 1);

        nRow++;
        s2da    =   Get2DAString("exptable", "XP", nRow);
    }

    //  Zkusenosti BCS / SCS
    nRow    =   0;
    s2da    =   Get2DAString("xps_exptable", "XP", nRow);

    while   (s2da   !=  "")
    {
        sli(oSystem, SYSTEM_XPS_PREFIX + "_CLASS_N_LEVEL", stoi(s2da), nRow + 1);

        nRow++;
        s2da    =   Get2DAString("xps_exptable", "XP", nRow);
    }

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}
