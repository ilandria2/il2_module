// +---------++----------------------------------------------------------------+
// | Name    || Death System (DS) GoToSphere script                            |
// | File    || vg_s1_ds_sphere.nss                                            |
// | Author  || VirgiL                                                         |
// | Created || 27-10-2009                                                     |
// | Updated || 27-10-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Presune hrace do 1 z 3 sfer smrti na zaklade jeho presvedseni (G/N/E)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_lpd_core_c"
#include    "vg_s0_ds_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    /*
    object  oPC         =   LPD_GetPC();
    int     nAlignment  =   GetAlignmentGoodEvil(oPC);
    string  sWaypoint   =   (nAlignment ==  ALIGNMENT_GOOD) ?   DS_TAG_SPHERE_OF_DEATH_GOOD :
                            (nAlignment ==  ALIGNMENT_EVIL) ?   DS_TAG_SPHERE_OF_DEATH_EVIL :   DS_TAG_SPHERE_OF_DEATH_NEUTRAL;

    SetCommandable(TRUE, oPC);
    FadeToBlack(oPC, FADE_SPEED_FASTEST);
    //ExecuteScript(DS_RETURN_PENALIZATION, oPC);
    AssignCommand(oPC, ClearAllActions(TRUE));
    DelayCommand(1.0f, AssignCommand(oPC, ActionJumpToLocation(GetLocation(GetWaypointByTag(sWaypoint)))));
    DelayCommand(5.0f, FadeFromBlack(oPC, FADE_SPEED_FASTEST));
    */
}
