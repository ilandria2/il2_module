// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_setstatus.nss                                              |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SETSTATUS"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_bcs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SETSTATUS";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sType   =   CCS_GetConvertedString(sLine, 1, FALSE);
    string  sAmount =   CCS_GetConvertedString(sLine, 2, FALSE);
    object  oTarget =   CCS_GetConvertedObject(sLine, 3, FALSE);
    string  sInfo;

    if  (!GetIsPC(oTarget) || GetIsDM(oTarget))
    {
        sInfo   =   R2 + "( ! ) " + W2 + "You must select a " + R2 + "player character";

        msg(oPC, sInfo, TRUE, FALSE);
        return;
    }

    sType   =   upper(sType);


    /*
        energy, liquid, food, stamina, hp, weight, skills, all

        //examples - ;GETINFO o="t";s="ALL";
        ;getstatus t;               - info o cieli
        ;getstatus s;               - info o mojej postave
        ;getstatus p2;energy;       - info o RS_FATIGUE u hraca #2

        //examples - ;SETINFO s="";n="80%";o="t";
        ;setstatus energy;137;t;    - nastavit RS_FATIGUE na 137 u cile
        ;setstatus liquid;34%;t;    - nastavit FDS_THIRST na 66% u cile
        ;setstatus all;100%;p3;     - nastavit vsechny potreby na 100% u hraca #3
        ;setstatus all;74;p3;       - nastavit vsechny potreby na 74% u hraca #3 (nastavit na ciselnu hodnotu tu nebude fungovat)

    */

    //SpawnScriptDebugger();
    string  sName, sValue;
    int     nValue, nMax, bAll, bPerc;

    bPerc   =   right(sAmount, 1) == "%";
    bAll    =   sType == "ALL";
    sInfo   =   "\n" + W2 + "Changing the state of the need [" + Y1 + sType + W2 + "] on target [" + Y2 + GetPCPlayerName(oTarget) + W2 + "] to:";

    //  energie
    if  (bAll
    ||  sType   ==  "ENERGY")
    {
        nMax    =   RS_FATIGUE_EDGE_FSLEEP;
        nValue  =   (bPerc) ? ftoi(itof(nMax)/100 * stoi(sAmount)) : stoi(sAmount);
        nValue  =   (nValue > nMax) ? nMax : (nValue < 0) ? 0 : nValue;
        sValue  =   itos(nValue) + " / " + itos(nMax);
        nValue  =   nMax - nValue;
        sName   =   "Energy";
        sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;

        sli(oTarget, SYSTEM_RS_PREFIX + "_FATIGUE", nValue);
    }

    //  hydratace
    if  (bAll
    ||  sType   ==  "LIQUID")
    {
        nMax    =   FDS_DEADLY_EDGE_THIRST;
        nValue  =   (bPerc) ? ftoi(itof(nMax)/100 * stoi(sAmount)) : stoi(sAmount);
        nValue  =   (nValue > nMax) ? nMax : (nValue < 0) ? 0 : nValue;
        sValue  =   itos(nValue) + " / " + itos(nMax);
        nValue  =   nMax - nValue;
        sName   =   "Hydratation";
        sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;

        sli(oTarget, SYSTEM_FDS_PREFIX + "_THIRST", nValue);
    }

    //  jidlo
    if  (bAll
    ||  sType   ==  "FOOD")
    {
        nMax    =   FDS_DEADLY_EDGE_HUNGER;
        nValue  =   (bPerc) ? ftoi(itof(nMax)/100 * stoi(sAmount)) : stoi(sAmount);
        nValue  =   (nValue > nMax) ? nMax : (nValue < 0) ? 0 : nValue;
        sValue  =   itos(nValue) + " / " + itos(nMax);
        nValue  =   nMax - nValue;
        sName   =   "Food";
        sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, TRUE) + C_NORM + " " + sName + " " + sValue;

        sli(oTarget, SYSTEM_FDS_PREFIX + "_HUNGER", nValue);
    }

    //  vydrz/stamina
    if  (bAll
    ||  sType   ==  "STAMINA")
    {
        nMax    =   gli(oTarget, SYSTEM_BCS_PREFIX + "_STAMINA_MAX");
        nValue  =   (bPerc) ? ftoi(itof(nMax)/100 * stoi(sAmount)) : stoi(sAmount);
        nValue  =   (nValue > nMax) ? nMax : (nValue < 0) ? 0 : nValue;
        sValue  =   itos(nValue) + " / " + itos(nMax);
        sName   =   "Stamina";
        sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, FALSE) + C_NORM + " " + sName + " " + sValue;

        sli(oTarget, SYSTEM_BCS_PREFIX + "_STAMINA", nValue);
    }

    //  power
    if  (bAll
    ||  sType   ==  "POWER")
    {
        nMax    =   gli(oTarget, SYSTEM_BCS_PREFIX + "_POWER_MAX");
        nValue  =   (bPerc) ? ftoi(itof(nMax)/100 * stoi(sAmount)) : stoi(sAmount);
        nValue  =   (nValue > nMax) ? nMax : (nValue < 0) ? 0 : nValue;
        sValue  =   itos(nValue) + " / " + itos(nMax);
        sName   =   "Power";
        sInfo   +=  "\n" + C_NORM + GetDiagram(nValue, nMax, -2, FALSE) + C_NORM + " " + sName + " " + sValue;

        sli(oTarget, SYSTEM_BCS_PREFIX + "_POWER", nValue);
    }

    nValue  =   stoi(sAmount);

    sli(oTarget, sType + "_LEVEL", nValue);
    spi(oTarget, sType + "_LEVEL", nValue);

    //  znalosti hornin, stromu, rostlin, houb, zvirat
    if  (sType   ==  "TREE")    sInfo   +=  "\nWoodsman(" + itos(nValue) + ")";   else
    if  (sType   ==  "ROCK")    sInfo   +=  "\nMiner(" + itos(nValue) + ")";       else
    if  (sType   ==  "BODY")    sInfo   +=  "\nHunter(" + itos(nValue) + ")";        else
    if  (sType   ==  "PLANT")   sInfo   +=  "\nAlchemist(" + itos(nValue) + ")";     //else
    //if  (sType   ==  "MUSHROOM")sInfo   +=  "\n(" + itos(nValue) + ")";
    else    sli(oTarget, sType + "_LEVEL", -1, -1, SYS_M_DELETE);

    msg(oPC, sInfo);
    PIS_RefreshIndicator(oTarget, OBJECT_INVALID, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}

