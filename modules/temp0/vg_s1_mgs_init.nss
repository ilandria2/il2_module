// +---------++----------------------------------------------------------------+
// | Name    || Music Generator System (MGS) System init                       |
// | File    || vg_s1_mgs_init.nss                                             |
// +---------++----------------------------------------------------------------+
/*
    Inicializacni skript pro system MGS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_d"
#include    "vg_s0_mgs_const"
#include    "vg_s0_oes_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sSystem     =   SYSTEM_MGS_PREFIX;
    string  sName       =   SYSTEM_MGS_NAME;
    string  sVersion    =   SYSTEM_MGS_VERSION;
    string  sAuthor     =   SYSTEM_MGS_AUTHOR;
    string  sDate       =   SYSTEM_MGS_DATE;
    string  sUpDate     =   SYSTEM_MGS_UPDATE;
    object  oSystem     =   GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT");
    string  sValue;
    int     n;

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", 2);
    logentry("[" + sSystem + "] init start", TRUE);

    sls(GetModule(), "SYSTEM", sSystem, -2);
    sli(oSystem, "SYSTEM_" + sSystem + "_ID", gli(GetModule(), "SYSTEM_S_ROWS"));
    sls(oSystem, "SYSTEM_" + sSystem + "_NAME", sName);
    sls(oSystem, "SYSTEM_" + sSystem + "_PREFIX", sSystem);
    sls(oSystem, "SYSTEM_" + sSystem + "_VERSION", sVersion);
    sls(oSystem, "SYSTEM_" + sSystem + "_AUTHOR", sAuthor);
    sls(oSystem, "SYSTEM_" + sSystem + "_DATE", sDate);
    sls(oSystem, "SYSTEM_" + sSystem + "_UPDATE", sUpDate);

//    sls(oSystem, sSystem + "_NCS_SYSINFO_RESREF", "vg_s1_mgs_info");
//    sls(oSystem, sSystem + "_NCS_DB_CHECK_RESREF", "vg_s1_<short>_data_c");
//    sls(oSystem, sSystem + "_NCS_DB_SAVE_RESREF", "vg_s1_<short>_data_s");
//    sls(oSystem, sSystem + "_NCS_DB_LOAD_RESREF", "vg_s1_<short>_data_l");
//    sli(oSystem, sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_LOAD_USE_GLOBAL_SCRIPT", TRUE);
//    sli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT", TRUE);

    if  (gli(oSystem, sSystem + "_DB_CHECK_USE_GLOBAL_SCRIPT"))
    CheckPersistentTables(sSystem);

    object  oOES    =   GetObjectByTag("SYSTEM_" + SYSTEM_OES_PREFIX + "_OBJECT");

    sls(oOES, SYSTEM_OES_PREFIX + "_EVENT_" + itos(OES_EVENT_O_ON_HEARTBEAT) + "_SCRIPT", "vg_s1_mgs_genert", -2);

/*
    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 1",
        -2
    );

    sls(
        oSystem,
        "SYSTEM_" + sSystem + "_HINT",
        "Hlaska 2",
        -2
    );
*/

    logentry("[" + sSystem + "] setting up music list");

    int     nRow;
    string  s2da, sCtg;

    //  category 'battle'
    nRow    =   0;
    sCtg    =   "battle";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'city'
    nRow    =   0;
    sCtg    =   "city";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'explore'
    nRow    =   0;
    sCtg    =   "explore";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'interior'
    nRow    =   0;
    sCtg    =   "interior";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'dungeon'
    nRow    =   0;
    sCtg    =   "dungeon";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'temple'
    nRow    =   0;
    sCtg    =   "temple";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'tavern'
    nRow    =   0;
    sCtg    =   "tavern";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    //  category 'special'
    nRow    =   0;
    sCtg    =   "special";
    s2da    =   Get2DAString("mus_categories", sCtg, nRow);

    while(s2da  !=  "")
    s2da    =   Get2DAString("mus_categories", sCtg, ++nRow);

    sli(oSystem, sSystem + "_" + GetStringUpperCase(sCtg), nRow);
    logentry("[" + sSystem + "] category '" + GetStringUpperCase(sCtg) + "' with " + itos(nRow) + " tracks");

    logentry("[" + sSystem + "] music list setup finished");

    sli(oSystem, "SYSTEM_" + sSystem + "_STATE", TRUE);
    logentry("[" + sSystem + "] init done", TRUE);
}

