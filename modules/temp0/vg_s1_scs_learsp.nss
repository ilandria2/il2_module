// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Learn spell script                     |
// | File    || vg_s1_scs_learsp.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 16-05-2009                                                     |
// | Updated || 16-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nauci hrace kouzlo, prida zkusenosti
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"
#include    "vg_s0_xps_core_m"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nSpell      =   gli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_LEARN_SPELL_ID", -1, SYS_M_DELETE);
    object  oItem       =   glo(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_LEARN_SPELL_SPBOOK", -1, SYS_M_DELETE);
    string  sSchool     =   Get2DAString("spells", "School", nSpell);
    int     nClass      =   gli(GetObjectByTag("SYSTEM_" + SYSTEM_SCS_PREFIX + "_OBJECT"), SYSTEM_SCS_PREFIX + "_CIRCLE_SCHOOL_" + sSchool);
    int     nInnate     =   stoi(Get2DAString("spells", "Innate", nSpell));
    int     nGainedXP   =   10 + Random(30 * nInnate);

    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEALING_G), OBJECT_SELF);
    AssignCommand(OBJECT_SELF, ActionDoCommand(PlaySound("sce_positive")));
    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", TRUE, nSpell);
    sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_ROW", nSpell, -2);
    SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "LEARN_SPELL_SUCCESS_&" + itos(nSpell) + "&");
    sls(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_ALTER_XP_PARAMETERS", "LEARN_SPELL_&" + itos(nSpell) + "&");
    XPS_AlterAbsorbXP(OBJECT_SELF, nClass, nGainedXP);
    XPS_AlterAbsorbXP(OBJECT_SELF, XPS_M_CLASS_XPS_PLAYER_CHARACTER, nGainedXP / 2);
}
