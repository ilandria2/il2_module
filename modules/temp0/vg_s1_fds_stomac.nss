// +---------++----------------------------------------------------------------+
// | Name    || Foods & Drinks System (FDS) Stomach modify script              |
// | File    || vg_s1_fds_stomac.nss                                           |
// | Author  || VirgiL                                                         |
// | Created || 06-07-2009                                                     |
// | Updated || 06-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kdyz byl pouzit predmet typu "jidlo" nebo "piti", pak se upravuji hodnoty
    jidla a tekutin v zaludku na zaklade vlastnosti pouziteho predmetu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_fds_const"
#include    "vg_s0_pis_core_c"
#include    "vg_s0_sys_core_o"


// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oItem   =   GetPCItemLastEquipped();
    int     nType   =   GetBaseItemType(oItem);

    //  pouze predmety typu "jidlo" a "tekutina"
    if  (nType  !=  BASE_ITEM_FOOD
    &&  nType   !=  BASE_ITEM_LIQUID)   return;

    object  oPC     =   GetPCItemLastEquippedBy();

    //  mimo dm
    if  (GetIsDM(oPC))  return;

    string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

    /*//  mimo specialni oblasti
    if  (sArea  ==  "S"
    ||  sArea   ==  "X")
    {
        SYS_Info(oPC, SYSTEM_FDS_PREFIX, "SPHERE");
        return;
    }*/

    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_FDS_PREFIX + "_OBJECT");
    string  sTag    =   GetTag(oItem);
    int     NID     =   gli(oSystem, SYSTEM_FDS_PREFIX + "_NI_" + sTag + "_NID");
    int     nFood   =   gli(oSystem, SYSTEM_FDS_PREFIX + "_NI_" + itos(NID) + "_FOOD");
    int     nLiquid =   gli(oSystem, SYSTEM_FDS_PREFIX + "_NI_" + itos(NID) + "_LIQUID");

    //SpawnScriptDebugger();
    DelayCommand(0.5f, Destroy(oItem));

    //  zadne vyzivove hodnoty
    if  ((nFood + nLiquid)  ==  0)  return;

    int     nHunger =   gli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER");
    int     nThirst =   gli(oPC, SYSTEM_FDS_PREFIX + "_THIRST");
    string  sMessage, sVerb;

    nHunger -=  nFood;
    nThirst -=  nLiquid;

    //  RP zprava
    sVerb       =   (nType  ==  BASE_ITEM_FOOD) ?   "j�"    :   "pije";
    sMessage    =   ConvertTokens("*N�co " + sVerb + "*", oPC, FALSE, TRUE, FALSE, FALSE);

    if  (nType  ==  BASE_ITEM_LIQUID)
    AssignCommand(oPC, PlayAnimation(ANIMATION_FIREFORGET_DRINK));
    AssignCommand(oPC, SpeakString(sMessage));
    DelayCommand(0.5f, AssignCommand(oPC, PlaySound("gui_potiondrink")));

    //  preplneny zaludek
    if  (nHunger    <   0
    ||  nThirst     <   0)
    {
        int     nDamage;
        vector  vPos;
        location lVFX;

        vPos    =   GetPosition(oPC);
        vPos    =   GetAwayVector(vPos, 0.6f, GetFacing(oPC));
        vPos    =   Vector(vPos.x, vPos.y, vPos.z + 1.6f);
        lVFX    =   Location(GetArea(oPC), vPos, 0.0f);

        nDamage     +=  (nHunger    <   0)  ?   -nHunger    :   0;
        nDamage     +=  (nThirst    <   0)  ?   -nThirst    :   0;
        nHunger     =   (nHunger    <   0)  ?   FDS_CRITICAL_EDGE_HUNGER / 4    :   nHunger;
        nThirst     =   (nThirst    <   0)  ?   FDS_CRITICAL_EDGE_THIRST / 4    :   nThirst;
        sMessage    =   ConvertTokens("*Zvrac�*", oPC, FALSE, TRUE, FALSE, FALSE);

        DelayCommand(1.5f, ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(nDamage, DAMAGE_TYPE_BLUDGEONING, DAMAGE_POWER_ENERGY), oPC));
        DelayCommand(1.5f, AssignCommand(oPC, PlayVoiceChat(VOICE_CHAT_PAIN1 + Random(3))));
        DelayCommand(1.5f, AssignCommand(oPC, SpeakString(sMessage)));
        DelayCommand(1.51f, AssignCommand(oPC, ClearAllActions(TRUE)));
        DelayCommand(1.6f, AssignCommand(oPC, ActionPlayAnimation(ANIMATION_LOOPING_GET_LOW, 0.2f, 4.0f)));
        DelayCommand(3.0f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_BLOOD_LRG_GREEN), lVFX));
        DelayCommand(3.3f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_BLOOD_LRG_GREEN), lVFX));
        DelayCommand(3.6f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_BLOOD_LRG_GREEN), lVFX));
        DelayCommand(3.9f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_BLOOD_LRG_GREEN), lVFX));
        DelayCommand(4.2f, ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_COM_BLOOD_LRG_GREEN), lVFX));
        DelayCommand(5.6f, AssignCommand(oPC, ActionPlayAnimation(ANIMATION_LOOPING_PAUSE_DRUNK, 0.75f, 5.0f)));
    }

    //  zmena hladu a zizne
    sli(oPC, SYSTEM_FDS_PREFIX + "_HUNGER", nHunger);
    sli(oPC, SYSTEM_FDS_PREFIX + "_THIRST", nThirst);
    PIS_RefreshIndicator(oPC, OBJECT_INVALID, FALSE);
    //msg(oPC, "Eating: " + itos(nFood) + " Drinking: " + itos(nLiquid), TRUE);
}
