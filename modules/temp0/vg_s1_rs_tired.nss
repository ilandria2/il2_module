// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) OnClientEnter script                          |
// | File    || vg_s1_rs_tired.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 15-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacteni dat a smycka pro unavu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_core_c"
#include    "vg_s0_sys_core_d"



// +---------------------------------------------------------------------------+
// |                            TiredValueSequence                             |
// +---------------------------------------------------------------------------+



void    TiredValueSequence(object oPC)
{
    if  (!GetIsObjectValid(oPC))    return;
    if  (!GetIsPC(oPC))             return;
    if  (GetIsDM(oPC))              return;
    if  (GetIsDead(oPC))            return;

    int nValue  =   gli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE");

    //  Procento vyuziti celkovej nosnosti
    float   fValue  =   100 * (IntToFloat(GetWeight(oPC)) / 10) / (GetAbilityScore(oPC, ABILITY_STRENGTH) * 3000);
    int     nWeight =   FloatToInt(fValue);
    int     nNewValue;

    nWeight /=  25;

    /*
    //  Pocet provedenych utoku za 1 sekvenci
    int nALast  =   gli(oPC, SYSTEM_RS_PREFIX + "_ATTACKS_LAST");
    int nANew   =   gli(oPC, "BCS_PHYSICAL_ATTACKS");
    int nAttack =   nANew - nALast;

    sli(oPC, SYSTEM_RS_PREFIX + "_ATTACKS_LAST", nANew);

    nAttack /=  25;

    //  Pocet vykuzleni za 1 sekvenci
    int nCLast      =   gli(oPC, SYSTEM_RS_PREFIX + "_CASTS_LAST");
    int nCNew       =   gli(oPC, "SCS_SPELL_CASTS");
    int nConjure    =   nCNew - nCLast;

    sli(oPC, SYSTEM_RS_PREFIX + "_CASTS_LAST", nCNew);

    nConjure    /=  25;

    int nNewValue   =   nValue + nWeight + nAttack + nConjure + nRandom;
    */

    nNewValue   =   nValue + nWeight /*+ nAttack + nConjure*/ + 1;
    nNewValue   =   (nNewValue  <   0)  ?   0   :   nNewValue;

    sli(oPC, SYSTEM_RS_PREFIX + "_FATIGUE", nNewValue);

    //  dosazena kriticka hodnota unavy - nastava vynuceny spanek
    if  (nNewValue  >=  RS_FATIGUE_EDGE_FSLEEP)
    {
        ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectDamage(GetMaxHitPoints(oPC) / 100 * RS_ON_RESTED_HP_HEALED_PERCENT, DAMAGE_TYPE_MAGICAL, DAMAGE_POWER_ENERGY), oPC);
        sli(oPC, SYSTEM_RS_PREFIX + "_RESTSEQ", RS_REST_SEQUENCES / 2 + 1);
        sli(oPC, SYSTEM_RS_PREFIX + "_FSLEEP", TRUE);
        RS_Rest(oPC);

        DelayCommand(0.1f, ApplyEffectToObject(DURATION_TYPE_TEMPORARY, SupernaturalEffect(EffectParalyze()), oPC, (RS_REST_SEQUENCES * 5.0f) / 2 + 1));
    }

    else

    //  notifikacia raz za minutu poslednych 10 minut
    if  (nNewValue  >=  RS_FATIGUE_EDGE_FSLEEP - 10)
    {
        SYS_Info(oPC, SYSTEM_RS_PREFIX, "CRITICAL");
    }
}



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetFirstPC();
    float   fDelay;

    while   (GetIsObjectValid(oPC))
    {
        if  (!GetIsDM(oPC))
        {
            /*string  sArea   =   GetSubString(GetTag(GetArea(oPC)), 4, 1);

            if  (sArea  !=  "S"
            &&  sArea   !=  "X")
            {*/
                fDelay  +=  ran(0.0f, 10.0f, 2);

                DelayCommand(fDelay, TiredValueSequence(oPC));
            //}
        }

        oPC =   GetNextPC();
    }
}
