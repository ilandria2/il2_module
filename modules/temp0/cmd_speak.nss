// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_speak.nss                                                  |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 27-06-2009                                                     |
// | Updated || 27-06-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "SPEAK"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "SPEAK";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText       =   CCS_GetConvertedString(sLine, 1, FALSE);
    object  oSpeaker    =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sType       =   GetStringUpperCase(CCS_GetConvertedString(sLine, 3, FALSE));
    string  sMessage;

    //  Neplatny mluvci
    if  (!GetIsObjectValid(oSpeaker))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid talker";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  Zadna zprava
    if  (sText  ==  "")
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid message";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    if  (oSpeaker           !=  oPC
    &&  GetMaster(oSpeaker) !=  oPC)
    {
        //  Nedostatek prav
        if  (!CCS_GetHasAccess(oPC, "PM")
        &&  !GetIsDM(oPC))
        {
            sMessage    =   R1 + "( ! ) " + W2 + "You cannot force other creatures to speak if you are not DM/PM";

            msg(oPC, sMessage, FALSE, FALSE);
            return;
        }
    }

    AssignCommand(oSpeaker, speak(sText, sType, -1));

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
