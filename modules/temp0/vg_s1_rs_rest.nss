// +---------++----------------------------------------------------------------+
// | Name    || Rest System (RS) OnPlayerRested script                         |
// | File    || vg_s1_rs_rest.nss                                              |
// | Author  || VirgiL                                                         |
// | Created || 04-06-2008                                                     |
// | Updated || 15-08-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Odpocinek
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_rs_core_c"
#include    "vg_s0_lpd_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC =   GetLastPCRested();

    if  (IsInConversation(oPC)) return;
    if  (GetIsDM(oPC)
    ||  GetIsDMPossessed(oPC))
    {
        ForceRest(oPC);
        return;
    }

    int nRestType   =   GetLastRestEventType();

    switch  (nRestType)
    {
    case    REST_EVENTTYPE_REST_CANCELLED:

    //  privatni rozhovor s menu pro vyber "typu" spanku
    {
        object  oG_PC   =   LPD_GetConversator(oPC);

        //  hrac jiz je v rozhovoru (nemuze mit 2)
        if  (GetIsObjectValid(oG_PC))
        LPD_EndConversation(oPC, TRUE);
        LPD_StartConversation(oPC, oPC, GetObjectByTag(RS_TAG_REST_DIALOG));
    }

    break;

    case    REST_EVENTTYPE_REST_STARTED:

        //  Preruseni spanku, aby se v udalosti _REST_CANCELLED pustil LPD
        //  Tohle je zaroven odstaveni originalniho systemu odpocivani
        AssignCommand(oPC, ClearAllActions(TRUE));

    break;
    }
}
