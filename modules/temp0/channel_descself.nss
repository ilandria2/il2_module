// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_descself.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "DESCSELF"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "DESCSELF";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    string  sMessage;

    if  (sText  ==  "0")
    {
        SetDescription(oPC, "");

        sMessage    =   C_INTE + "Description" + C_NORM + " of your character was set to:\n" + GetDescription(oPC);
    }

    else
    {
        sText   =   ConvertTokens(sText, oPC, FALSE);
        sMessage    =   C_INTE + "Description" + C_NORM + " of your character was set to:\n" + sText;

        SetDescription(oPC, sText);
    }

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
