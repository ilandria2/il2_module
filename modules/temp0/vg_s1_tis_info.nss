// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Sys info                         |
// | File    || vg_s1_tis_info.nss                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu TIS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_lpd_core_c"
#include    "vg_s0_tis_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;
    int     bFloat;

    object oPC = OBJECT_SELF;
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_TIS_PREFIX + "_OBJECT");

    sTemp   =   gls(oPC, SYSTEM_TIS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if (sCase == "CHECK_RESULT")
    {
        int bResult = stoi(GetNthSubString(sTemp, 1));

        switch (bResult)
        {
        case TIS_CHECK_RESULT_ACTION_ACTION:
            sMessage = C_BOLD + "Already performing an action";//: " + itos(GetCurrentAction(oPC));
            break;

        case TIS_CHECK_RESULT_ACTION_DIST:
            sMessage = C_BOLD + "Target is too far";
            break;

        case TIS_CHECK_RESULT_ACTION_INVALID:
            sMessage = C_BOLD + "Invalid action";
            break;

        case TIS_CHECK_RESULT_ACTION_OBJECT:
            sMessage = C_BOLD + "Lost selected target";
            break;

        case TIS_CHECK_RESULT_ACTION_VECTOR:
            sMessage = C_BOLD + "Action cancelled by moving";
            break;
        }

        sMessage = ConvertTokens(sMessage, oPC, TRUE, FALSE, FALSE, FALSE);
        bFloat = TRUE;
    }

    else if  (sCase  ==  "NO_OBJECTS")
    {
        sMessage    =   C_BOLD + "Nothing of use here";
        bFloat      =   TRUE;
    }

    else if  (sCase  ==  "NO_ACTIONS")
    {
        sMessage    =   C_BOLD + "Target is not useable anymore.";
        bFloat      =   TRUE;
    }

    if (sMessage != "")
        msg(oPC, sMessage, bFloat, FALSE);
}
