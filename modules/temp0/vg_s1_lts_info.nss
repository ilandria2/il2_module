// +---------++----------------------------------------------------------------+
// | Name    || Lootable Treasures System (LTS) Sys info                       |
// | File    || vg_s1_lts_info.nss                                             |
// | Author  || VirgiL                                                         |
// | Created || 25-07-2009                                                     |
// | Updated || 25-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Informacni skript systemu LTS
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_lts_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string  sTemp, sCase, sMessage;

    sTemp   =   gls(OBJECT_SELF, SYSTEM_LTS_PREFIX + "_NCS_SYSINFO_PARAMETERS", -1, SYS_M_DELETE);
    sCase   =   GetSubString(sTemp, 0, FindSubString(sTemp, "&") - 1);

    if  (sCase  ==  "PHYSICAL_DESTROY_SUCCESS")
    {
        int nDice   =   stoi(GetNthSubString(sTemp, 1));
        int nMod    =   stoi(GetNthSubString(sTemp, 2));
        int nDC     =   stoi(GetNthSubString(sTemp, 3));

        sMessage    =   C_POSI + "Fyzick� zni�en�: �SP�CH (" + itos(nDice) + " + " + itos(nMod) + " = " + itos(nDice + nMod) + " vs DC " + itos(nDC) + ")";
        sMessage    +=  "\nPoklad nebude po�kozen";
    }

    else

    if  (sCase  ==  "PHYSICAL_DESTROY_CRITICAL_FAILURE")
    {
        int nDice   =   stoi(GetNthSubString(sTemp, 1));
        int nMod    =   stoi(GetNthSubString(sTemp, 2));
        int nDC     =   stoi(GetNthSubString(sTemp, 3));

        sMessage    =   C_NEGA + "Fyzick� zni�en�: KRITICK� CHYBA (" + itos(nDice) + " + " + itos(nMod) + " = " + itos(nDice + nMod) + " vs DC " + itos(nDC) + ")";
        sMessage    +=  "\nPoklad bude zni�en";
    }

    else

    if  (sCase  ==  "PHYSICAL_DESTROY_FAILURE")
    {
        int nDice   =   stoi(GetNthSubString(sTemp, 1));
        int nMod    =   stoi(GetNthSubString(sTemp, 2));
        int nDC     =   stoi(GetNthSubString(sTemp, 3));

        sMessage    =   C_NEGA + "Fyzick� zni�en�: CHYBA (" + itos(nDice) + " + " + itos(nMod) + " = " + itos(nDice + nMod) + " vs DC " + itos(nDC) + ")";
        sMessage    +=  "\nPoklad bude po�kozen";
    }

    else

    if  (sCase  ==  "MAGICAL_DESTROY_SUCCESS")
    {
        int nDice   =   stoi(GetNthSubString(sTemp, 1));
        int nMod    =   stoi(GetNthSubString(sTemp, 2));
        int nDC     =   stoi(GetNthSubString(sTemp, 3));

        sMessage    =   C_POSI + "Magick� zni�en�: �SP�CH (" + itos(nDice) + " + " + itos(nMod) + " = " + itos(nDice + nMod) + " vs DC " + itos(nDC) + ")";
        sMessage    +=  "\nPoklad nebude po�kozen";
    }

    else

    if  (sCase  ==  "MAGICAL_DESTROY_CRITICAL_FAILURE")
    {
        int nDice   =   stoi(GetNthSubString(sTemp, 1));
        int nMod    =   stoi(GetNthSubString(sTemp, 2));
        int nDC     =   stoi(GetNthSubString(sTemp, 3));

        sMessage    =   C_NEGA + "Magick� zni�en�: KRITICK� CHYBA (" + itos(nDice) + " + " + itos(nMod) + " = " + itos(nDice + nMod) + " vs DC " + itos(nDC) + ")";
        sMessage    +=  "\nPoklad bude zni�en";
    }

    else

    if  (sCase  ==  "MAGICAL_DESTROY_FAILURE")
    {
        int nDice   =   stoi(GetNthSubString(sTemp, 1));
        int nMod    =   stoi(GetNthSubString(sTemp, 2));
        int nDC     =   stoi(GetNthSubString(sTemp, 3));

        sMessage    =   C_NEGA + "Magick� zni�en�: CHYBA (" + itos(nDice) + " + " + itos(nMod) + " = " + itos(nDice + nMod) + " vs DC " + itos(nDC) + ")";
        sMessage    +=  "\nPoklad bude po�kozen";
    }

    else

    if  (sCase  ==  "TREASURE_NOT_SET")
    {
        sMessage    =   C_NEGA + "Poklad nen� spr�vn� nastaven";
    }

    else

    if  (sCase  ==  "SEQUENCE_RESET")
    {
        sMessage    =   C_NEGA + "Poklad byl odhalen p�ed jeho znovuobjeven�m";
    }

    else

    if  (sCase  ==  "COMPOSED_MODIFY")
    {
        sMessage    =   C_NEGA + "Nelze upravovat skladan� zdroj pokladu";
    }

    else

    if  (sCase  ==  "SOURCE_MODIFY")
    {
        int     bType   =   stoi(GetNthSubString(sTemp, 1));
        string  sType   =   GetNthSubString(sTemp, 2);
        string  sName   =   GetNthSubString(sTemp, 3);

        //  vlozen predmet
        if  (bType  ==  1)
        sMessage    =   C_POSI + "�sp�n� vlo�en� p�edm�tu [" + sName + "] do zdroje pokladu [" + sType + "]";

        else
        sMessage    =   C_POSI + "�sp�n� odebr�n� p�edm�tu [" + sName + "] ze zdroje pokladu [" + sType + "]";
    }

    msg(OBJECT_SELF, sMessage, FALSE, FALSE);
}
