// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Core M                                  |
// | File    || vg_s0_sys_core_m.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 10-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Knihovna systemovych funkci Ilandrie 2 kategorie "Math"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_const"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || GetCalendarDate                                                |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati datum a cas ve formatu jak je v DB (string)
//
// -----------------------------------------------------------------------------
string  GetCalendarDate();



// +---------++----------------------------------------------------------------+
// | Name    || cd                                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Porovna datumy sValue1 a sValue2
//
// -----------------------------------------------------------------------------
int     cd(string sValue1, string sValue2, int bType = SYS_M_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || DayToHours                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte dni na hodiny
//
// -----------------------------------------------------------------------------
int     DayToHours(int nDay);



// +---------++----------------------------------------------------------------+
// | Name    || MonthToHours                                                   |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte mesice na hodiny
//
// -----------------------------------------------------------------------------
int     MonthToHours(int nMonth);



// +---------++----------------------------------------------------------------+
// | Name    || YearToHours                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte roky na hodiny
//
// -----------------------------------------------------------------------------
int     YearToHours(int nYear);



// +---------++----------------------------------------------------------------+
// | Name    || DateToHours                                                    |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Prepocte datum na hodiny
//
// -----------------------------------------------------------------------------
int     DateToHours(int nYear, int nMonth, int nDay, int nHour);



// +---------++----------------------------------------------------------------+
// | Name    || Round                                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zaokruhli cislo nValue
//
// -----------------------------------------------------------------------------
int     Round(int nValue);



// +---------++----------------------------------------------------------------+
// | Name    || ran                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Vypocita nahodne cislo v rozsahu |fMin, fMax| na nDot desetinnych mist
//
// -----------------------------------------------------------------------------
float   ran(float fMin, float fMax, int nDot = 2);



// +---------++----------------------------------------------------------------+
// | Name    || ei                                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  "Calculate I"
//  Vypocita vysledek nNumber a nChange s operaciou bType
//
// -----------------------------------------------------------------------------
int     ei(int nNumber, int nChange, int bType = SYS_M_INCREMENT);



// +---------++----------------------------------------------------------------+
// | Name    || ef                                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  "Calculate F"
//  Vypocita vysledek nNumber a nChange s operaciou bType
//
// -----------------------------------------------------------------------------
float   ef(float fNumber, float fChange, int bType = SYS_M_INCREMENT);



// +---------++----------------------------------------------------------------+
// | Name    || ci                                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Porovna cisla nValue1 a nValue2
//
// -----------------------------------------------------------------------------
int     ci(int nValue1, int nValue2, int bType = SYS_M_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || cf                                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Porovna cisla fValue1 a fValue2
//
// -----------------------------------------------------------------------------
int     cf(float fValue1, float fValue2, int bType = SYS_M_EQUAL);



// +---------++----------------------------------------------------------------+
// | Name    || GetIsInFOV                                                     |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati TRUE pokud objekt oViewer je v toleranci uhla pohledu fFov na objekt
//  oTarget a ve vzdalenosti fMaxDist
//
// -----------------------------------------------------------------------------
int     GetIsInFOV(float fFov, float fMaxDist, object oViewer, object oTarget);



// +---------++----------------------------------------------------------------+
// | Name    || GetAwayVector                                                  |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Navrati vektor fDistance vzdaleny od vOriginal pod uhlem fAngle
//
// -----------------------------------------------------------------------------
vector  GetAwayVector(vector vOriginal, float fDistance, float fAngle);



// +---------------------------------------------------------------------------+
// |                        I M P L E M E N T A T I O N                        |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                             GetCalendarDate                               |
// +---------------------------------------------------------------------------+



string  GetCalendarDate()
{
    return  GetStringRight("0000" + IntToString(GetCalendarYear()), 4) + "-" +
            GetStringRight("00" + IntToString(GetCalendarMonth()), 2) + "-" +
            GetStringRight("00" + IntToString(GetCalendarDay()), 2) + " " +
            GetStringRight("00" + IntToString(GetTimeHour()), 2) + ":" +
            GetStringRight("00" + IntToString(GetTimeMinute()), 2) + ":" +
            GetStringRight("00" + IntToString(GetTimeSecond()), 2) + "." +
            GetStringRight("000" + IntToString(GetTimeMillisecond()), 3);
}



// +---------------------------------------------------------------------------+
// |                                    cd                                     |
// +---------------------------------------------------------------------------+



int     cd(string sValue1, string sValue2, int bType = SYS_M_EQUAL)
{
    int bResult;
    int nValue1 =   StringToInt(GetSubString(sValue1, 0, 4)) * 1000000 + StringToInt(GetSubString(sValue1, 5, 2)) * 10000 + StringToInt(GetSubString(sValue1, 8, 2)) * 100 + StringToInt(GetSubString(sValue1, 11, 2));// + StringToInt(GetSubString(sValue1, 14, 2)) * 100 + StringToInt(GetSubString(sValue1, 17, 2));
    int nValue2 =   StringToInt(GetSubString(sValue2, 0, 4)) * 1000000 + StringToInt(GetSubString(sValue2, 5, 2)) * 10000 + StringToInt(GetSubString(sValue2, 8, 2)) * 100 + StringToInt(GetSubString(sValue2, 11, 2));// + StringToInt(GetSubString(sValue2, 14, 2)) * 100 + StringToInt(GetSubString(sValue2, 17, 2));

    switch  (bType)
    {
        case    SYS_M_LOWER:            bResult =   (nValue1    <   nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_LOWER_OR_EQUAL:   bResult =   (nValue1    <=  nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_EQUAL:            bResult =   (nValue1    ==  nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_HIGHER_OR_EQUAL:  bResult =   (nValue1    >=  nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_HIGHER:           bResult =   (nValue1    >   nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_NOT_EQUAL:        bResult =   (nValue1    !=  nValue2)    ?   TRUE    :   FALSE;  break;
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                               DayToHours                                  |
// +---------------------------------------------------------------------------+



int     DayToHours(int nDay){return  24 * (nDay - 1);}



// +---------------------------------------------------------------------------+
// |                              MonthToHours                                 |
// +---------------------------------------------------------------------------+



int     MonthToHours(int nMonth)
{
    int     nResult, nDays;

    switch  (nMonth)
    {
        case    1:  nDays   =   0;      break;
        case    2:  nDays   =   31;     break;
        case    3:  nDays   =   59;     break;
        case    4:  nDays   =   90;     break;
        case    5:  nDays   =   120;    break;
        case    6:  nDays   =   151;    break;
        case    7:  nDays   =   181;    break;
        case    8:  nDays   =   212;    break;
        case    9:  nDays   =   243;    break;
        case    10: nDays   =   273;    break;
        case    11: nDays   =   304;    break;
        case    12: nDays   =   334;    break;
    }

    nResult =   24 * (nDays * (nMonth));

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                               YearToHours                                 |
// +---------------------------------------------------------------------------+



int     YearToHours(int nYear){return (8760 * (nYear - 1));}



// +---------------------------------------------------------------------------+
// |                               DateToHours                                 |
// +---------------------------------------------------------------------------+



int     DateToHours(int nYear, int nMonth, int nDay, int nHour)
{
    int     nResult, nHoursDay, nHoursMonth, nHoursYear;

    nHoursDay   =   DayToHours(nDay);
    nHoursMonth =   MonthToHours(nMonth);
    nHoursYear  =   YearToHours(nYear);
    nResult     =   nHour + nHoursDay + nHoursMonth + nHoursYear;

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                  Round                                    |
// +---------------------------------------------------------------------------+



int     Round(int nValue)
{
    int     nResult, nLast;
    string  sValue;

    sValue  =   IntToString(nValue);
    nLast   =   StringToInt(GetStringRight(sValue, 1));

    if  (nLast  >=  5)
    {
        nResult =   nValue + (10 - nLast);
    }

    else
    {
        nResult =   nValue - nLast;
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                    ran                                    |
// +---------------------------------------------------------------------------+



float   ran(float fMin, float fMax, int nDot = 2)
{
    int     nTemp;
    float   fResult, fTemp, fNom;

    fNom    =   pow(10.0f, IntToFloat(nDot));
    fTemp   =   (fMax - fMin) * fNom;
    nTemp   =   Random(1 + FloatToInt(fTemp));
    fTemp   =   IntToFloat(nTemp) / fNom;
    fResult =   fMin + fTemp;

    return  fResult;
}



// +---------------------------------------------------------------------------+
// |                                    ei                                     |
// +---------------------------------------------------------------------------+



int     ei(int nNumber, int nChange, int bType = SYS_M_INCREMENT)
{
    int     nResult;

    switch  (bType)
    {
        case    SYS_M_SQUARE_ROOT:  nResult =   FloatToInt(pow(IntToFloat(nNumber), 1 / IntToFloat(nChange)));  break;
        case    SYS_M_DIVIDE:       nResult =   nNumber / nChange;  break;
        case    SYS_M_DECREMENT:    nResult =   nNumber - nChange;  break;
        case    SYS_M_INCREMENT:    nResult =   nNumber + nChange;  break;
        case    SYS_M_MULTIPLE:     nResult =   nNumber * nChange;  break;
        case    SYS_M_POWER:        nResult =   FloatToInt(pow(IntToFloat(nNumber), IntToFloat(nChange)));  break;
        default:                    nResult =   nChange;    break;
    }

    return  nResult;
}



// +---------------------------------------------------------------------------+
// |                                    ef                                     |
// +---------------------------------------------------------------------------+



float   ef(float fNumber, float fChange, int bType = SYS_M_INCREMENT)
{
    float   fResult;

    switch  (bType)
    {
        case    SYS_M_SQUARE_ROOT:  fResult =   pow(fNumber, 1 / fChange);  break;
        case    SYS_M_DIVIDE:       fResult =   fNumber / fChange;  break;
        case    SYS_M_DECREMENT:    fResult =   fNumber - fChange;  break;
        case    SYS_M_INCREMENT:    fResult =   fNumber + fChange;  break;
        case    SYS_M_MULTIPLE:     fResult =   fNumber * fChange;  break;
        case    SYS_M_POWER:        fResult =   pow(fNumber, fChange);  break;
        default:                    fResult =   fChange;    break;
    }

    return  fResult;
}



// +---------------------------------------------------------------------------+
// |                                    ci                                     |
// +---------------------------------------------------------------------------+



int     ci(int nValue1, int nValue2, int bType = SYS_M_EQUAL)
{
    int bResult;

    switch  (bType)
    {
        case    SYS_M_LOWER:            bResult =   (nValue1    <   nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_LOWER_OR_EQUAL:   bResult =   (nValue1    <=  nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_EQUAL:            bResult =   (nValue1    ==  nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_HIGHER_OR_EQUAL:  bResult =   (nValue1    >=  nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_HIGHER:           bResult =   (nValue1    >   nValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_NOT_EQUAL:        bResult =   (nValue1    !=  nValue2)    ?   TRUE    :   FALSE;  break;
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                                    cf                                     |
// +---------------------------------------------------------------------------+



int     cf(float fValue1, float fValue2, int bType = SYS_M_EQUAL)
{
    int bResult;

    switch  (bType)
    {
        case    SYS_M_LOWER:            bResult =   (fValue1    <   fValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_LOWER_OR_EQUAL:   bResult =   (fValue1    <=  fValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_EQUAL:            bResult =   (fValue1    ==  fValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_HIGHER_OR_EQUAL:  bResult =   (fValue1    >=  fValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_HIGHER:           bResult =   (fValue1    >   fValue2)    ?   TRUE    :   FALSE;  break;
        case    SYS_M_NOT_EQUAL:        bResult =   (fValue1    !=  fValue2)    ?   TRUE    :   FALSE;  break;
    }

    return  bResult;
}



// +---------------------------------------------------------------------------+
// |                                  GetIsInFOV                               |
// +---------------------------------------------------------------------------+



int     GetIsInFOV(float fFov, float fMaxDist, object oViewer, object oTarget)
{
    vector  vTarget     =   GetPosition(oTarget);
    vector  vViewer     =   GetPosition(oViewer);

    vTarget =   Vector(vTarget.x, vTarget.y, vViewer.z);

    location lTarget    =   Location(GetArea(oTarget), vTarget, GetFacing(oTarget));
    float   fDistance   =   GetDistanceBetweenLocations(GetLocation(oViewer), lTarget);
    float   fFacing     =   GetFacing(oViewer);
    float   fTolerate   =   fFov / 2.0;
    float   fTolLeft    =   fFacing + fTolerate;
    float   fTolRight   =   fFacing - fTolerate;
    float   fTolDist    =   fDistance / cos(fTolerate);
    vector  vTolLeft    =   GetAwayVector(vViewer, fTolDist, fTolLeft);
    vector  vTolRight   =   GetAwayVector(vViewer, fTolDist, fTolRight);
    vector  vLookPoint  =   GetAwayVector(vViewer, fTolDist, fFacing);
    float   fLPNPC      =   GetDistanceBetweenLocations(Location(GetArea(oViewer), vLookPoint, 0.0f), Location(GetArea(oViewer), vTarget, 0.0f));
    float   fLeft       =   GetDistanceBetweenLocations(Location(GetArea(oViewer), vTolLeft, 0.0f), lTarget);
    float   fRight      =   GetDistanceBetweenLocations(Location(GetArea(oViewer), vTolRight, 0.0f), lTarget);
    int     bResult;

    if
    (
        fLPNPC      <=  fLeft
    &&  fLPNPC      <=  fRight
    &&  fDistance   <=  fMaxDist
    )
            bResult =   TRUE;
    else    bResult =   FALSE;

    return bResult;
}



// +---------------------------------------------------------------------------+
// |                                GetAwayVector                              |
// +---------------------------------------------------------------------------+



vector  GetAwayVector(vector vOriginal, float fDistance, float fAngle)
{
    vector vChanged;

    vChanged.z = vOriginal.z;
    vChanged.x = vOriginal.x + (fDistance * cos(fAngle));

    if  (vChanged.x < 0.0)
    {
        vChanged.x = - vChanged.x;
    }

    vChanged.y = vOriginal.y + (fDistance * sin(fAngle));

    if  (vChanged.y < 0.0)
    {
        vChanged.y = - vChanged.y;
    }

    return vChanged;
}
