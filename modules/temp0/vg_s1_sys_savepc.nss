// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Save Player Character (Manual) Script   |
// | File    || vg_s1_sys_savepc.nss                                           |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Skript kt ulozi data pro specifickou postavu hrace
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_d"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   OBJECT_SELF;
    object  oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT");
    int     bSQL    =   gli(oSystem, SYSTEM_SYS_PREFIX + "_SQL");
    /*
    string  sMessage;

    sMessage    =   (!bSQL) ?
                    R1 + "( ! ) " + W2 + "Nelze nav�zat spojen� s datab�zou - bude ulo�ena pouze tv� postava"
                :   G1 + "( ! ) " + W2 + "Ukladan� dat tv� postavy...";

    msg(oPC, sMessage, TRUE, FALSE);
    */

    int     nSystems    =   gli(GetModule(), "SYSTEM_S_ROWS");
    int     nRow        =   1;
    int     bGlobal;
    string  sSystem;

    while   (nRow   <=  nSystems)
    {
        sSystem =   gls(GetModule(), "SYSTEM", nRow);
        bGlobal =   gli(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_DB_SAVE_USE_GLOBAL_SCRIPT");

        if  (bGlobal)
        SavePersistentData(oPC, sSystem);
        nRow++;
    }

    /*
    if  (bSQL)
    {
        sMessage    =   G1 + "( ! ) " + W2 + "Data tv� postavy �sp�n� ulo�eny";

        msg(oPC, sMessage, TRUE, FALSE);
    }

    sMessage    =   G1 + "Exportov�n� tv� postavy...";

    msg(oPC, sMessage, TRUE, FALSE);
    */
    ExportSingleCharacter(oPC);
    DelayCommand(3.0f, BootPC(oPC));
}

