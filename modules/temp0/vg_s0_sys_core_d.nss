// +---------++----------------------------------------------------------------+
// | Name    || System Functions (SYS) Core D                                  |
// | File    || vg_s0_sys_core_d.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 12-05-2009                                                     |
// | Updated || 12-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Knihovna systemovych funkci Ilandrie 2 kategorie "Data"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_const"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_sys_core_v"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                           D E C L A R A T I O N S                         |
// +---------------------------------------------------------------------------+



// +---------++----------------------------------------------------------------+
// | Name    || CheckPersistentTables                                          |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Zkontroluje / vytvori perzistentni tabulky v databaze pro system sSystem
//
// -----------------------------------------------------------------------------
void    CheckPersistentTables(string sSystem = SYSTEM_SYS_PREFIX, string sParam = "");



// +---------++----------------------------------------------------------------+
// | Name    || SavePersistentData                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Ulozi perzistentni data systemu sSystem pro objekt oObject
//
// -----------------------------------------------------------------------------
void    SavePersistentData(object oObject, string sSystem = SYSTEM_SYS_PREFIX, string sParam = "");



// +---------++----------------------------------------------------------------+
// | Name    || LoadPersistentData                                             |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nacte perzistentni data systemu sSystem pro objekt oObject
//
// -----------------------------------------------------------------------------
void    LoadPersistentData(object oObject, string sSystem = SYSTEM_SYS_PREFIX, string sParam = "");



// +---------++----------------------------------------------------------------+
// | Name    || spi                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nastavi perzistentni promennou sVarName pro objekt oObject na hodnotu nValue
//
// -----------------------------------------------------------------------------
void    spi(object oObject, string sVarName, int nValue = -999999);



// +---------++----------------------------------------------------------------+
// | Name    || spf                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nastavi perzistentni promennou sVarName pro objekt oObject na hodnotu fValue
//
// -----------------------------------------------------------------------------
void    spf(object oObject, string sVarName, float fValue = -999999.0f);



// +---------++----------------------------------------------------------------+
// | Name    || sps                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nastavi perzistentni promennou sVarName pro objekt oObject na hodnotu sValue
//
// -----------------------------------------------------------------------------
void    sps(object oObject, string sVarName, string sValue = "");



// +---------++----------------------------------------------------------------+
// | Name    || spl                                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
//
//  Nastavi perzistentni promennou sVarName pro objekt oObject na hodnotu lValue
//
// -----------------------------------------------------------------------------
void    spl(object oObject, string sVarName, location lValue);



// +---------------------------------------------------------------------------+
// |                         I M P L E M E N T A T I O N                       |
// +---------------------------------------------------------------------------+



// +---------------------------------------------------------------------------+
// |                         CheckPersistentTables                             |
// +---------------------------------------------------------------------------+



void    CheckPersistentTables(string sSystem = SYSTEM_SYS_PREFIX, string sParam = "")
{
    if  (!GetLocalInt(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_SQL"))
    {
        WriteTimestampedLogEntry("[" + sSystem + "]: CheckPersistentTables(): NwNX2 not active");
        return;
    }

    string  sScript =   GetLocalString(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_NCS_DB_CHECK_RESREF");

    if  (sScript    ==  "") return;
    if  (sParam     !=  "")
    SetLocalString(GetModule(), SYSTEM_SYS_PREFIX + "_DB_CHECK_PARAM", sParam);
    ExecuteScript(sScript, GetModule());
}



// +---------------------------------------------------------------------------+
// |                           SavePersistentData                              |
// +---------------------------------------------------------------------------+



void    SavePersistentData(object oObject, string sSystem = SYSTEM_SYS_PREFIX, string sParam = "")
{
    if  (!GetLocalInt(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_SQL"))
    {
        WriteTimestampedLogEntry("[" + sSystem + "]: SavePersistentData(): NwNX2 not active");
        return;
    }

    string  sScript =   GetLocalString(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_NCS_DB_SAVE_RESREF");

    if  (sScript    ==  "") return;
    if  (sParam     !=  "")
    SetLocalString(oObject, SYSTEM_SYS_PREFIX + "_DB_SAVE_PARAM", sParam);
    ExecuteScript(sScript, oObject);
}



// +---------------------------------------------------------------------------+
// |                           LoadPersistentData                              |
// +---------------------------------------------------------------------------+



void    LoadPersistentData(object oObject, string sSystem = SYSTEM_SYS_PREFIX, string sParam = "")
{
    if  (!GetLocalInt(GetObjectByTag("SYSTEM_" + SYSTEM_SYS_PREFIX + "_OBJECT"), SYSTEM_SYS_PREFIX + "_SQL"))
    {
        WriteTimestampedLogEntry("[" + sSystem + "]: LoadPersistentData(): NwNX2 not active");
        return;
    }

    string  sScript =   GetLocalString(GetObjectByTag("SYSTEM_" + sSystem + "_OBJECT"), sSystem + "_NCS_DB_LOAD_RESREF");

    if  (sScript    ==  "") return;
    if  (sParam     !=  "")
    SetLocalString(oObject, SYSTEM_SYS_PREFIX + "_DB_LOAD_PARAM", sParam);
    ExecuteScript(sScript, oObject);
}



// +---------------------------------------------------------------------------+
// |                                  spi                                      |
// +---------------------------------------------------------------------------+



void    spi(object oObject, string sVarName, int nValue = -999999)
{
    if  (GetIsPC(oObject))
    {
        string  SQL;
        int     nPlayer =   gli(oObject, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        SQL =   "INSERT INTO sys_vars VALUES (-1,'" + itos(nPlayer);
        SQL +=  "'," + itos(SYS_V_INTEGER) + ",'" + sVarName + "','" + itos(nValue) + "', DEFAULT)";
        SQL +=  " ON DUPLICATE KEY UPDATE VarValue = '" + itos(nValue) + "'";

        SQLExecDirect(SQL);
    }
}
