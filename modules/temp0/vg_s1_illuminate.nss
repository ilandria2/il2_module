// -----------------------------------------------------------------------------
/*
    Zapne / vypne svetlo na placeablu
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_sys_core_v"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oObject =   OBJECT_SELF;
    int     bState  =   gli(oObject, SYSTEM_SYS_PREFIX + "_ANIMATION_STATE");

    if  (!bState)
    {
        PlayAnimation(ANIMATION_PLACEABLE_ACTIVATE);
        sli(oObject, SYSTEM_SYS_PREFIX + "_ANIMATION_STATE", TRUE);
        //DelayCommand(0.4, SetPlaceableIllumination(oObject, TRUE));
        //DelayCommand(0.5, RecomputeStaticLighting(GetArea(oObject)));

        ApplyEffectToObject(DURATION_TYPE_PERMANENT, EffectVisualEffect(VFX_DUR_LIGHT_YELLOW_5), oObject);
    }
    else
    {
        PlayAnimation(ANIMATION_PLACEABLE_DEACTIVATE);
        sli(oObject, SYSTEM_SYS_PREFIX + "_ANIMATION_STATE", FALSE);
        //DelayCommand(0.4, SetPlaceableIllumination(oObject, FALSE));
        //DelayCommand(0.5, RecomputeStaticLighting(GetArea(oObject)));

        effect  eEffect = GetFirstEffect(oObject);
        while (GetIsEffectValid(eEffect))
        {
            if (GetEffectType(eEffect)  ==  EFFECT_TYPE_VISUALEFFECT)
            RemoveEffect(oObject, eEffect);
            eEffect = GetNextEffect(oObject);
        }
    }
}
