// +---------++----------------------------------------------------------------+
// | Name    || Text Interaction System (TIS) Respawn script                   |
// | File    || vg_s1_tis_spawn.nss                                            |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Spawnovaci systemu dynamickych objektu (soucast systemu TIS)
    Kdyz hrac vleze do oblasti, ve kt. nejsou zadni jini hraci, tenhle skript
    pusti sekvenci spawnu.
    Pote zvysi pocet hracu v oblasti, do kt. prave vstoupil hrac (promenna)
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_tis_core_c"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    object  oPC     =   GetEnteringObject();

    if  (!GetIsPC(oPC)) return;

    object  oSpawn  =   glo(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_SPAWN_SP", 1, SYS_M_DELETE);

    if  (GetIsObjectValid(oSpawn))
    {
        //if  (!gli(oPC, "tis_spawn"))SpawnScriptDebugger();

        object  oMaster;
        string  sResRef, sGroup;
        float   fDist;
        int     n, nRow, nSPRow;

        n   =   1;

        //  projde vsechny spawnpointy oznacene jako "pripravene ke spawnuti bytosti" a spawne bytosti
        while   (GetIsObjectValid(oSpawn))
        {
            sResRef =   gls(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_SPAWN_RESREF", n, SYS_M_DELETE);
            fDist   =   glf(oSpawn, SYSTEM_TIS_PREFIX + "_RADIUS");
            fDist   =   (fDist  ==  0.0f)   ?   TIS_SPAWN_RADIUS    :   fDist;
            fDist   =   ran(0.0f, fDist);
            sGroup  =   gls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP", -1, SYS_M_DELETE);
            nRow    =   gli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW", -1, SYS_M_DELETE);
            oMaster =   GetObjectByTag(TIS_TAG_TIS_SPAWN_MASTER + "_" + sGroup);
            oSpawn  =   CreateObject(OBJECT_TYPE_CREATURE, sResRef, GetRandomLocation(GetArea(oSpawn), oSpawn, fDist));
            nSPRow  =   gli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_GROUP_" + sGroup + "_AREA_SP", -1, SYS_M_DELETE);

            sls(oSpawn, SYSTEM_TIS_PREFIX + "_GROUP", sGroup);
            sli(oSpawn, SYSTEM_TIS_PREFIX + "_ROW", nRow);
            slo(oMaster, SYSTEM_TIS_PREFIX + "_SPAWN_OBJECT", oSpawn, nRow);
            slo(oMaster, SYSTEM_TIS_PREFIX + "_AREA_SP", OBJECT_INVALID, nSPRow);

            oSpawn  =   glo(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_SPAWN_SP", ++n, SYS_M_DELETE);
        }

        sli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_SPAWN_SP_O_ROWS", -1, -1, SYS_M_DELETE);
        sli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_SPAWN_RESREF_S_ROWS", -1, -1, SYS_M_DELETE);
    }

    int nPCs    =   gli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_PLAYERS");

    if  (!nPCs || TRUE)
    {
        int     n       =   0;
        int     nGroups =   gli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_GROUP_S_ROWS");
        string  sGroup;

        while   (++n    <=  nGroups)
        {
            sGroup  =   gls(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_GROUP", n);

            DelayCommand((n - 1) * 0.1f, TIS_UpdateSpawnGroup(sGroup));
        }
    }

    sli(OBJECT_SELF, SYSTEM_TIS_PREFIX + "_PLAYERS", nPCs + 1);
}

