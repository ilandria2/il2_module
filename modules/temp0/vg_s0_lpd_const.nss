// +---------++----------------------------------------------------------------+
// | Name    || ListenPattern Dialog System (LPD) System constants             |
// | File    || vg_s0_lpd_const.nss                                            |
// | Version || 3.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 09-07-2009                                                     |
// | Updated || 09-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Konstanty systemu "LPD"
*/
// -----------------------------------------------------------------------------



// +---------------------------------------------------------------------------+
// |                      S Y S T E M   C O N S T A N T S                      |
// +---------------------------------------------------------------------------+



const string    SYSTEM_LPD_NAME                     =   "ListenPattern Dialog System";
const string    SYSTEM_LPD_PREFIX                   =   "LPD";
const string    SYSTEM_LPD_VERSION                  =   "3.00";
const string    SYSTEM_LPD_AUTHOR                   =   "VirgiL";
const string    SYSTEM_LPD_DATE                     =   "09-07-2009";
const string    SYSTEM_LPD_UPDATE                   =   "09-07-2009";



// +---------------------------------------------------------------------------+
// |                           C O N S T A N T S                               |
// +---------------------------------------------------------------------------+



const string    LPD_STRING_FUNCTION_SCRIPT          =   "EXEC";
const string    LPD_STRING_FUNCTION_LOCAL_VAR       =   "LVAR";

const string LPD_TAG_DIALOG_SOURCE                  = "VG_P_S1_DIALOG01";

//const string    LPD_TAG_DIALOG_OPTION_EXAMINE       =   "VG_I_S1_DLGOPTVI";
const string    LPD_TAG_DIALOG_OPTION_RESPONSE      =   "VG_I_S1_LPD_DFLT";
//const string    LPD_TAG_DIALOG_OPTION_EXIT          =   "VG_I_S1_DLGOPTEX";
const string    LPD_TAG_DIALOG_OPTION_INVALID       =   "VG_I_S1_LPD_MISS";

const float     LPD_DEFAULT_FACING_INTERVAL         =   5.0f;
const float     LPD_DEFAULT_MAX_DISTANCE            =   5.0f;
const int       LPD_DEFAULT_MAX_NOT_RESPONDING      =   10;
