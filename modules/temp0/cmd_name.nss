// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Command code                     |
// | File    || cmd_name.nss                                                   |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-07-2009                                                     |
// | Updated || 17-07-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Kod konzoloveho prikazu "NAME"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sCmd    =   "NAME";
    string      sLine   =   CCS_GetCommandLine(TRUE);
    object      oSystem =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    int         bLog    =   gli(oSystem, SYSTEM_CCS_PREFIX + "_CMD_" + sCmd + "_LOG");
    object      oPC     =   OBJECT_SELF;

    if  (bLog)
    {
        int nId =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        logentry("[" + SYSTEM_CCS_PREFIX + "] PC [" + itos(nId) + "] command line [" + sLine + "]", TRUE);
    }

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sName   =   CCS_GetConvertedString(sLine, 1, FALSE);
    object  oObject =   CCS_GetConvertedObject(sLine, 2, FALSE);
    string  sMessage;

    sName   =   (sName  ==  "RESET")    ?   ""  :   sName;

    //  Neplatny objekt
    if  (!GetIsObjectValid(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Invalid object";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    //  Hrace nelze prejmenovat
    if  (GetIsPC(oObject))
    {
        sMessage    =   R1 + "( ! ) " + W2 + "Cannot rename the player character";

        msg(oPC, sMessage, FALSE, FALSE);
        return;
    }

    string  sOldName    =   GetName(oObject);

    SetName(oObject, sName);

    string  sNewName    =   GetName(oObject);

    sMessage    =   Y1 + "( ? ) " + W2 + "Name of the object [" + G2 + sOldName + W2 + "] was changed to [" + G2 + sNewName + W2 + "]";

    msg(oPC, sMessage, FALSE, FALSE);

//  Kod skriptu
//  ----------------------------------------------------------------------------
}
