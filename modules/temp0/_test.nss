#include    "vg_s0_ccs_core_c"
#include    "vg_s0_xps_core_m"
#include    "vg_s0_oes_const"
#include    "aps_include"

void main()
{
    if  (!gli(OBJECT_SELF, "ONCE"))
    {
        sli(OBJECT_SELF, "ONCE", TRUE);

        int nID =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

        if  (nID    !=  0)  return;

        nID =   stoi(GetDeity(OBJECT_SELF));

        //  stary hrac
        if  (nID    >=  SYS_PLAYER_ID_BASE)
        {
            string  sQuerry =   "SELECT PlayerID " +
                                "FROM sys_players " +
                                "WHERE PlayerID = " + itos(nID);

            SQLExecDirect(sQuerry);

            //  postava ma ID ktere v DB neni
            if  (SQLFetch() ==  SQL_ERROR)
            {
                string  sLog    =   "Invalid player ID [" + itos(nID) + "] on player [" + GetPCPlayerName(OBJECT_SELF) + " / " + GetName(OBJECT_SELF) + "]";

                sli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID", -1);
                logentry(sLog, TRUE);
                SYS_Info(OBJECT_SELF, SYSTEM_SYS_PREFIX, "INVALID_PLAYERID");
                return;
            }

            //  nastaveni PlayerID
            sli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID", nID);

            //  nacteni dat z DB od vsech systemu
            ExecuteScript("vg_s1_oes_loaddt", OBJECT_SELF);

            //  denniky..
            ExecuteScript("vg_s1_start_jrl", OBJECT_SELF);

            //  signalizace udalosti "OnPlayerLogin" - pro pusteni rekurzivy systemu
            slo(GetModule(), "USERDEFINED_EVENT_OBJECT", OBJECT_SELF);
            SignalEvent(GetModule(), EventUserDefined(OES_EVENT_NUMBER_O_PLAYER_LOGIN));
        }

        //  novy hrac
        else
        {
            object  oWP =   GetObjectByTag(SYS_TAG_PLAYER_START_TS);

            //  startovni waypoint v testovacim rezimu
            DelayCommand(0.5f, AssignCommand(OBJECT_SELF, JumpToLocation(GetLocation(oWP))));

            //  startovni uprava inventare
            ExecuteScript("vg_s1_start_inv", OBJECT_SELF);

            //  registrace
            ExecuteScript("vg_s1_register", OBJECT_SELF);

            //  konzolove prikazy
            CCS_GrantAccess(OBJECT_SELF, "PUBLIC");
            CCS_GrantAccess(OBJECT_SELF, "PM");
            CCS_GrantAccess(OBJECT_SELF, "SA");

            //  level 2 pro testovaci rezim
            if  (GetHitDice(OBJECT_SELF)    ==  1)
            {
                XPS_AlterLevel(OBJECT_SELF, XPS_M_CLASS_XPS_PLAYER_CHARACTER, 2);
                msg(OBJECT_SELF, C_BOLD + "[DEBUG]: " + C_NORM + "Level = 1 -> Auto Level-Up");
            }

            msg(OBJECT_SELF, C_BOLD + "[DEBUG]: " + C_NORM + "Console Command Groups access granted");
            msg(OBJECT_SELF, C_BOLD + "[DEBUG]: " + C_NORM + "Moved to TestArea");
        }
    }
}
