#include "aps_include"
#include "vg_s0_ids_core_c"

void main()
{
    string sLog, sQuerry;

    // ids_placed_items table that holds location of dropped items from the world
    sLog = "[" + IDS + "] Checking table [ids_placed_items]";
    sQuerry = "DESCRIBE ids_placed_items";

    logentry(sLog);
    SQLExecDirect(sQuerry);

    if (SQLFetch() == SQL_SUCCESS)
    {
        sLog = "[" + IDS + "] Table [ids_placed_items] already exist";
        logentry(sLog);
    }

    else
    {
        sLog = "[" + IDS + "] Table [ids_placed_items] does not exist -> Creating it";

        logentry(sLog);
        sQuerry = "CREATE TABLE ids_placed_items " +
        "(" +
            "item_oid INT(6) NOT NULL primary key," +
            "item_tag VARCHAR(32) NOT NULL," +
            "item_type INT NOT NULL," +
            "item_tech VARCHAR(100) NOT NULL," +
            "item_stack INT NOT NULL," +
            "item_weight INT NOT NULL," +
            "item_dbcur INT NOT NULL," +
            "item_dbmax INT NOT NULL," +
            "item_loc VARCHAR(200) NOT NULL," +
            "item_name NVARCHAR(100) NOT NULL," +
            "item_desc NVARCHAR(500) NOT NULL," +
            "date TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
            "date_mod TIMESTAMP DEFAULT CURRENT_TIMESTAMP " +
        ")";

        SQLExecDirect(sQuerry);

        sQuerry = "DESCRIBE ids_placed_items";

        SQLExecDirect(sQuerry);

        if (SQLFetch() == SQL_SUCCESS)
        sLog = "[" + IDS + "] Table [ids_placed_items] created successfully"; else
        sLog = "[" + IDS + "] Table [ids_placed_items] not created - error";

        logentry(sLog);
    }


}
