// +---------++----------------------------------------------------------------+
// | Name    || SpellCraft System (SCS) Persistent data load script            |
// | File    || vg_s1_scs_data_l.nss                                           |
// | Version || 1.00                                                           |
// | Author  || VirgiL                                                         |
// | Created || 17-05-2009                                                     |
// | Updated || 17-05-2009                                                     |
// +---------++----------------------------------------------------------------+
/*
    Nacteni perzistentni data pro hrace
*/
// -----------------------------------------------------------------------------



#include    "aps_include"
#include    "vg_s0_sys_core_v"
#include    "vg_s0_sys_core_t"
#include    "vg_s0_scs_const"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    int     nPC     =   gli(OBJECT_SELF, SYSTEM_SYS_PREFIX + "_PLAYER_ID");
    int     nData;
    string  sQuerry;

    sQuerry =   "SELECT " + SCS_DB_COLUMN_SPELL_ID + " FROM " + SCS_DB_TABLE_SPELLS + " WHERE " + SCS_DB_COLUMN_CHARACTER_ID + " = " + itos(nPC);

    SQLExecDirect(sQuerry);

    while   (SQLFetch() ==  SQL_SUCCESS)
    {
        nData   =   stoi(SQLGetData(1));

        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL", TRUE, nData);
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_ROW", nData, -2);
        sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_SPELL_SAVED", TRUE, nData);
    }

    sQuerry =   "SELECT " + SCS_DB_COLUMN_MANA_VALUE + " " +
                "FROM " + SCS_DB_TABLE_MANA + " " +
                "WHERE " + SCS_DB_COLUMN_CHARACTER_ID + " = '" + itos(nPC) + "'";

    SQLExecDirect(sQuerry);

    if  (SQLFetch() ==  SQL_SUCCESS)
    {
        nData   =   stoi(SQLGetData(1));

        DelayCommand(1.0f, sli(OBJECT_SELF, SYSTEM_SCS_PREFIX + "_MANA", nData));
        DelayCommand(1.05f, SYS_Info(OBJECT_SELF, SYSTEM_SCS_PREFIX, "CHANGE_MANA_&0&_&" + itos(nData) + "&"));
    }
}
