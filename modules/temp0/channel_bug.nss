// +---------++----------------------------------------------------------------+
// | Name    || Console Commands System (CCS) Channel code                     |
// | File    || chanel_bug.nss                                                 |
// | Author  || VirgiL                                                         |
// +---------++----------------------------------------------------------------+
/*
    Kod kanalu mluvy "BUG"
*/
// -----------------------------------------------------------------------------



#include    "vg_s0_ccs_core_t"
#include    "vg_s0_pis_core_c"
#include    "aps_include"



// +---------------------------------------------------------------------------+
// |                                M A I N                                    |
// +---------------------------------------------------------------------------+



void    main()
{
    string      sChannel    =   "BUG";
    string      sLine       =   GetPCChatMessage();
    object      oSystem     =   GetObjectByTag("SYSTEM_" + SYSTEM_CCS_PREFIX + "_OBJECT");
    object      oPC         =   OBJECT_SELF;

//  ----------------------------------------------------------------------------
//  Kod skriptu

    string  sText   =   CCS_GetChannelText(sLine);

    if  (sText  ==  "")
    {
        SYS_Info(oPC, SYSTEM_CCS_PREFIX, "CHANNEL_INFO_&" + sChannel + "&");
        return;
    }

    string  sMessage;
    string  sQuery  =   "";
    int     nPCID   =   gli(oPC, SYSTEM_SYS_PREFIX + "_PLAYER_ID");

    sQuery  =   "INSERT INTO psi_pcreports VALUES (" +
                itos(nPCID) + ",'" +
                GetPCPlayerName(oPC) + "','" +
                GetName(oPC) + "','" +
                GetPCIPAddress(oPC) +
                "','BUG','N/A','" +
                SQLEncodeSpecialChars(sText) + "','" +
                LocationToString(GetLocation(oPC)) +
                "','NEW','N/A',now(), now())";

    SQLExecDirect(sQuery);
    logentry("[" + sChannel + " - " + GetPCPlayerName(oPC) + " - " + GetTag(GetArea(oPC)) + " - " + GetName(GetArea(oPC)) + "]: " + sText, TRUE);

    sMessage    =   Y1 + "( ? ) " + W2 + "Added a new bug entry to the database:\n" + Y2 + sText;

    msg(oPC, sMessage, FALSE, FALSE);


//  Kod skriptu
//  ----------------------------------------------------------------------------
}
