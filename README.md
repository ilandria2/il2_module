### What is this repository for? ###

* Contains "master module" source files used to compiled the .mod file (module) for hosting on a neverwinter nights server
* Contains helper documents for development purposes
* Contains archived documents used to develop prior versions - for references / ideas

### How do I get set up? ###

* Copy the /modules/temp0 folder from the source into your nwn folder (delete if any existing is there) - note, the folder temp0 is created whenever you open some module in nwtoolset.exe
* v1.69 version must be installed
* both SoU and HotU expansion packs must be installed


### Who do I talk to? ###

* Repo owner or admin